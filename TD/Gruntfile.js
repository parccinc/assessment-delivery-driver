﻿module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    //Gruntfile.js
    var options = {
        config: {
            src: "gruntTasks/config/*.js",
            
        },
        stagingFolder: "staging",
        releaseFolder: "release",
        pkg: grunt.file.readJSON('package.json'),
    };

    var configs = require('load-grunt-configs')(grunt, options); // load all grunt tasks


    // Define the configuration for all the tasks
    grunt.initConfig(configs);

    grunt.loadTasks('gruntTasks');

};