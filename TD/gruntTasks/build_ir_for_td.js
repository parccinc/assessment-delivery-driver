﻿module.exports = function (grunt) {

    grunt.registerTask('build_ir_for_td', 'Minifies IR from OAT/src and outputs to OAT/dist', function () {

        grunt.option('irdest', 'OAT/src/');
        
        grunt.task.run('minifyir:OAT/src/', 'minifysl:OAT/src/', 'uglify:irBuild', 'clean:cleanOATDist', 'copy:scoringToDist', 'copy:IRBuildToTDDist', 'clean:cleanOATSrcDist', 'clean:cleanBuildOutput');


    });


};
