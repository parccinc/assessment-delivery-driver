﻿module.exports = function (grunt) {

    /*
    The scoring library can only be "minified" from within Item Runner.  Therefore we need to know both the "tag" for the version of Item Runner we
    are going to use as well as the tag for the scoring library repo.  As of 3/17/16 - we copy the files from the 'src' folder of the OAT-parcc-scoring
    repo into a copy of Item Runner, then run the OAT task to minify the scoring library.
    */
    grunt.registerTask('pull_and_build_sl', 'Pulls SL from the repo and builds the minified version --irtagname required, --sltagname required, --irdest and --sldest are optional', function () {
      
        if (!grunt.option('sltagname')) {
            grunt.warn("You must provide a --sltagname to correspond that corresponds to the version of the SL you want from the package-parcc-ads repo. Ex: grunt pull_and_build_sl --sltagname=nightly562")
        }

    

        if (!grunt.option('sldest')) {
            grunt.option('sldest', grunt.option('sltagname'));
        }

        if (!grunt.option('irdest'))
        {
            grunt.option('irdest', grunt.option('sldest'));
           
        }

        grunt.option('irtagname', grunt.option('sldest'));
        grunt.task.run(['cloneScoring', 'copy:initialLibs', 'change_sldest:' + grunt.option('sldest') + "/jssrc/", 'copy:jsForSL', 'auto_install', 'minifysl:' + grunt.option('sldest') + "/jssrc/", 'copy:scoringToDist']); // moved , 'replace:qtirunner', 'replace:themes' to minifyir

    //    grunt.task.run(['cloneScoring', 'change_irdest:' + grunt.option('irdest') + "/jssrc/", 'add_scoring_lib_to_ir', 'auto_install', 'minifysl:' + grunt.option('irdest'), 'copy:scoringToDist']);

        //grunt.log('Scoring Lib was copied to ' + grunt.option('irdest'));
    });
};
