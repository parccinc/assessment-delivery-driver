﻿module.exports = function (grunt) {
    // build tao parcc scripts
    grunt.registerTask('build_user_scripts', 'Runs OATs taoparccscripts task to generate UserScripts files', function (irFolder) {

        if (!irFolder) {
            grunt.warn("You must provide the name of the folder that contains the IR source ex: runIRTasks:nightly435")
        }

        var ir = this.async();

        grunt.util.spawn({
            grunt: true,
            // args: ['requirejs:qtinewrunner', 'requirejs:parccstudenttools', 'requirejs:tei', 'taoparccscripts','sass:taoqtiitem', 'requirejs:qtiscorer'],
            args: ['taoparccscripts'],
            opts: {
                cwd: irFolder + '/tao/views/build'
            }
        }, function (error, result, code) {
            console.log(result.stdout);
            ir();
        });



    });

};