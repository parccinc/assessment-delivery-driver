﻿module.exports.tasks = {
    // Minifies the css files.
    cssmin: {
        options: {
            keepSpecialComments: 0
        },
        // TODO:  Add any NEW css files that should be compressed here!            
        releaseBuild: {
            files: {
                //'<%= releaseFolder %>/styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom-min.css': [
                //    'styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom.css'
                //],
                '<%= releaseFolder %>/styles/td-min.css': [
                    'styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom.css',
                    'styles/ScrollerWidget.css',
                    'styles/LineReaderWidget.css',
                    'styles/jquery.treetable.css',
                    'styles/jquery.treetable.theme.default.css',
                    'styles/td-dbg.css',
                    'styles/td.css',
                ],
                '<%= releaseFolder %>/styles/td-purple-min.css': [
                    'styles/td-purple.css',
                ],
                '<%= releaseFolder %>/styles/td-green-min.css': [
                    'styles/td-green.css',
                ],
                '<%= releaseFolder %>/OAT/dist/assets/ckeditor/contents.css': [
                    'OAT/dist/assets/ckeditor/contents.css',
                ],
                '<%= releaseFolder %>/OAT/dist/assets/ckeditor/skins/tao/scss/dialog.css': [
                    'OAT/dist/assets/ckeditor/skins/tao/scss/dialog.css',
                ],
                '<%= releaseFolder %>/OAT/dist/assets/ckeditor/skins/tao/scss/editor.css': [
                    'OAT/dist/assets/ckeditor/skins/tao/scss/editor.css',
                ]
            }
        },
    },
};