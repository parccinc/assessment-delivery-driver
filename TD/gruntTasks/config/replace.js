﻿"use strict";

module.exports.tasks = {
    replace: {
        qtirunner: {
            options: {
                force: true,
                preserveOrder: true,
                patterns: [
                    {
                        match: /(..\/..\/..\/tao\/views\/css\/font\/)/ig,
                        replacement: 'font/'
                    }
                ]
            },
            files: [
                {
                    expand: true, flatten: false, src: ["<%= grunt.option('irdest') %>/taoQtiItem/views/scss/qti-runner.scss"]
                }
            ]

        },
        themes: {
            options: {
                force: true,
                preserveOrder: true,
                patterns: [
                    {
                        match: /(..\/..\/..\/..\/tao\/views\/css\/font\/)/ig,
                        replacement: '../../font/'
                    }
                ]
            },
            files: [
               {
                   expand: true, flatten: false, src: ["<%= grunt.option('irdest') %>/taoQtiItem/views/scss/themes/default.scss"]
               }
            ]
        },

        releaseBuild: {
            options: {
                force: true,
                preserveOrder: true,
                patterns: [
                    {
                        match: /(\.css)/ig,
                        replacement: '-min.css'
                    }
                ]
            },
            files: [
                {
                    expand: true, flatten: false, src: ['scripts/td-theme.js'], dest: '<%= stagingFolder %>/'
                }
            ]

        },

        releaseIndex: {
            options: {
                force: true,
                preserveOrder: true,
                patterns: [
                    {
                        match: 'version',
                        replacement: '<%= releaseFolder %>'
                    }
                ]
            },
            files: [
                {
                    expand: true, flatten: false, src: ['index-build.html'], dest: '../',
                    rename: function () {
                        return 'index-<%= releaseFolder %>.html';
                    }
                }
            ]
        }
    }
};