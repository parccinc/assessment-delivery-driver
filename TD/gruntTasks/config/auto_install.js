﻿module.exports.tasks = {
    auto_install: {
            local: {},
            subdir: {
                options: {
                        cwd: "<%= grunt.option('irdest') %>/tao/views/build",
                        stdout: true,
                        stderr: true,
                        failOnError: true,
                        npm: true,
                        bower: false
                }
            }
    }
};