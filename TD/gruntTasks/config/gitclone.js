﻿"use strict";

module.exports.tasks = {
    gitclone: {
        cloneIRSrc: {
            options: {
                repository: "https://github.com/Breakthrough-Technologies/OAT-package-parcc-ads.git",
                branch: "<%= grunt.option('irtagname') %>", //"nightly524",
                directory: "<%= grunt.option('irdest') %>"
            }
        },

        clonescoring: {
            options: {
                repository: 'https://github.com/Breakthrough-Technologies/OAT-package-parcc-ads.git', //'https://github.com/Breakthrough-Technologies/OAT-parcc-scoring.git',
                branch: "<%= grunt.option('sltagname') %>",
                directory: "<%= grunt.option('sldest') %>"
            }
        },

        cloneMathJax: {
            options: {
                repository: 'https://github.com/Breakthrough-Technologies/OAT-lib-mathjax.git',
                branch: "master",
                directory: "OAT-lib-mathjax"
            }
        }
    },



};
