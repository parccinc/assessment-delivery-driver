﻿module.exports.tasks = {
    // Minify and obfuscate the javascript.  
    uglify: {
        options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
            //  nameCache: 'tmp/grunt-uglify-cache.json',
            //   mangleProperties: true,
            //   reserveDOMProperties: true,
            //  reserveDOMCache: true,
            //  exceptionsFiles: ['domprops.json'],
            mangle: {
                toplevel: true,
                except: [ // mangle is true for all else besides the specified exceptions
                  'scripts/distribution/jquery-1.8.0.min.js',
                  'scripts/distribution/jquery-ui-1.8.23.custom.min.js',
                  'scripts/distribution/jquery.cookie-1.4.1.min.js',
                  'scripts/distribution/screenfull.min.js',
                  'scripts/distribution/jquery.mousewheel.min.js',
                  'scripts/distribution/jquery.sha256.min.js',
                  'OAT/dist/qtiItemRunner.min.js',
                  'OAT/dist/studenttools.min.js',
                  'OAT/dist/tei.min.js'
                ]
            },
            sourceMap: '<%= includeSourceMap %>',
            compress: {
                drop_console: true,
                drop_debugger: true
            }
        },

        irBuild: {
            options:{
                sourceMap: true,
                mangle: false,
                sourceMapIncludeSources: true
            },
            files: {
            "OAT/src/dist/qtiItemRunnerFull.min.js": [
                    "OAT/src/dist/qtiItemRunner.min.js",
                    "OAT/src/dist/studenttools.min.js",
                    "OAT/src/dist/tei.min.js"
                ]
            }
        },

        releaseBuild: {
            files: {
                // TODO:  Add any NEW files that should be compressed/obfuscated here!
                // this will put all of the files listed in the array below into the file named release/scripts/td-min.js
                '<%= releaseFolder %>/scripts/td-min.js': [
                  //'scripts/td-debug.js', // This is for DEBUGGING only and should never be in the Release versions!
                  'scripts/distribution/jquery-1.8.0.min.js',
                  'scripts/distribution/jquery-ui-1.8.23.custom.min.js',
                  'scripts/distribution/jquery.cookie-1.4.1.min.js',
                  'scripts/distribution/screenfull.min.js',
                  'scripts/distribution/jquery.event.move.js',
                  'scripts/distribution/jquery.event.swipe.js',
                  'scripts/distribution/jquery.mousewheel.min.js',
                  'scripts/distribution/jquery.sha256.min.js',
                  'scripts/distribution/jquery.maskedinput.js',
                  'scripts/distribution/jquery.treetable.js',

                 'scripts/widgets/util/utility.js',
                 'scripts/widgets/util/results.js',
                 'scripts/widgets/selectionengine/engine.js',
                 'scripts/widgets/selectionengine/factory.js',
                 'scripts/widgets/selectionengine/algorithm/linear.js',
                 'scripts/widgets/selectionengine/algorithm/nonlinear.js',
                 'scripts/widgets/selectionengine/algorithm/random.js',
                 'scripts/widgets/selectionengine/algorithm/dcm.js',
                 'scripts/widgets/selectionengine/algorithm/asynchronousdcm.js',
                 'scripts/widgets/scorereport/main.js',
                 'scripts/widgets/scorereport/readingcomp.js',
                 'scripts/widgets/scorereport/readermotivation.js',
                 'scripts/widgets/scorereport/mathcomp.js',
                 'scripts/widgets/scorereport/mathfluency.js',
                 'scripts/widgets/scorereport/general.js',
                 'scripts/distribution/ttsCallbacks.js',

                   '<%= stagingFolder %>/scripts/td-theme.js',
                 'scripts/widgets/ui/_base/FormBaseWidget.js',
                 'scripts/widgets/ContentWidget.js',
                 'scripts/widgets/ui/LoginWidget.js',
                 'scripts/widgets/ui/ItemWidget.js',
                 'scripts/widgets/DataAccessLayerWidget.js',
                 'scripts/widgets/ui/StickyHeaderWidget.js',
                 'scripts/widgets/ui/LogoWidget.js',
                 'scripts/widgets/ui/NotificationsWidget.js',
                 'scripts/widgets/ui/WelcomeWidget.js',
                 'scripts/widgets/ui/ConfirmStudentWidget.js',
                 'scripts/widgets/ui/ConfirmTestWidget.js',
                 'scripts/widgets/ui/StartTestWidget.js',
                 'scripts/widgets/ui/TestInfoWidget.js',
                 'scripts/widgets/ui/FlagWidget.js',
                 'scripts/widgets/ui/ClockWidget.js',
                 'scripts/widgets/ui/ReviewLinkWidget.js',
                 'scripts/widgets/ui/SaveExitWidget.js',
                 'scripts/widgets/ui/CandidateInfoWidget.js',
                 'scripts/widgets/ui/TallyWidget.js',
                 'scripts/widgets/ui/NavigationWidget.js',
                 'scripts/widgets/ui/SaveSubmitWidget.js',
                 'scripts/widgets/ui/SpinnerWidget.js',
                 'scripts/widgets/DataCommWidget.js',
                 'scripts/widgets/ItemInteractionWidget.js',
                 'scripts/widgets/ui/ScrollerWidget.js',
                 'scripts/widgets/ui/LineReaderWidget.js',
                 'scripts/widgets/ui/StickyFooterWidget.js',
                 'scripts/widgets/ui/ToolWidget.js',
                 'scripts/widgets/ui/NotificationDialogWidget.js',
                 'scripts/widgets/ui/TTSToolboxWidget.js',
                 'scripts/widgets/ui/ScoreReportWidget.js',
                 'scripts/widgets/TimerWidget.js',
                 'scripts/widgets/util/utility.js',
                 'scripts/widgets/util/results.js',
                 'scripts/widgets/selectionengine/engine.js',
                 'scripts/widgets/selectionengine/factory.js',
                 'scripts/widgets/selectionengine/algorithm/linear.js',
                 'scripts/widgets/selectionengine/algorithm/nonlinear.js',
                 'scripts/widgets/selectionengine/algorithm/random.js',
                 'scripts/widgets/selectionengine/algorithm/dcm.js',
                 'scripts/widgets/selectionengine/algorithm/asynchronousdcm.js',
                 'scripts/widgets/scorereport/main.js',
                 'scripts/widgets/scorereport/readingcomp.js',
                 'scripts/widgets/scorereport/readermotivation.js',
                 'scripts/widgets/scorereport/mathcomp.js',
                 'scripts/widgets/scorereport/mathfluency.js',
                 'scripts/widgets/scorereport/general.js',
                 'scripts/distribution/ttsCallbacks.js',
                ],

                
                '<%= releaseFolder %>/OAT/vendor/require.js': [
                    'OAT/vendor/require.js'
                ],

                '<%= releaseFolder %>/scripts/main.js': [
                    'scripts/requirejs.main.js'
                ]
            }
        },
    },
};