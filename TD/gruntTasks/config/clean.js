﻿module.exports.tasks = {
    clean: {
        options: {
            force: true,
        },
        cleanIR: ["<%= grunt.option('irdest')%>"],
        cleanSL: ["<%= grunt.option('sldest')%>"],
        cleanIRDist: ["<%= grunt.option('irdest')%>/dist/**"],

        releaseDebug: ["stagingDebug/**", "releaseDebug/**"],
        release: ["staging/**", "release/**"],
        staging: ["staging/**"],
        stagingDebug: ["stagingDebug/**"],

        cleanBuildOutput: ["<%= grunt.option('irdest')%>/tao/views/build/output/**"],

        cleanOATSrcDist: ["OAT/src/dist/**"],

        cleanOATDist: ["OAT/dist"],
    }
};