﻿"use strict";

module.exports.tasks = {
    copy: {
        initialLibs: {
            files: [
              // includes files within path and its sub-directories
              {
                  expand: true, src: ["<%= grunt.option('irtagname') %>/taoQtiItem/views/js/MathJaxFallback.js"], dest: "<%= grunt.option('irtagname') %>/taoQtiItem/views/js/mathjax/",
                  rename: function (dest, src) {
                      // use the source directory to create the file
                      // example with your directory structure
                      //   dest = "dev/js/"
                      //   src = "module1/js/main.js"
                      return dest + "MathJax.js";
                  }
              },
              { expand: true, flatten: true, src: ["<%= grunt.option('irtagname') %>/taoQtiItem/views/js/portableSharedLibraries/**"], dest: "<%= grunt.option('irtagname') %>/taoQtiItem/install/scripts/portableSharedLibraries/" },
              { expand: true, flatten: true, src: ["resources/messages.json"], dest: "<%= grunt.option('irtagname') %>/tao/views/locales/en-US/" },
              // Custom extensions helper that replaces querying for mainfest.php to using the subfolders when finding build tasks.
              {
                expand: true, flatten: true, src: ["resources/extensions.js"], dest: "<%= grunt.option('irdest') %>/tao/views/build/tasks/helpers/"
              }

            ],
        },

        jsForSL: {
            expand: true,
            cwd: "<%= grunt.option('sltagname') %>/",

            src: ['tao/views/**/*.js', 'tao/views/**/*.json', 'tao/views/**/*.css', 'tao/views/**/*.scss', 'tao/views/**/.jshintrc', 'tao/views/**/*.tpl', 'tao/views/js/**/*.png', 'tao/views/js/**/*.jpg', 'tao/views/js/**/*.gif',
                'tao/views/js/lib/mediaelement/flashmediaelement.swf',
                'tao/views/css/font/**', '!**/test/**', '!**/test.js', '!**/locales/**', 'tao/views/locales/**', '!**/templates/**',
                'taoItems/views/js/scoring/api/scorer.js', 'taoQtiItem/views/js/scoring/**', 'taoParcc/views/build/**/*.js', 'taoParcc/views/js/scoring/processor/expressions/operators/custom/**',
                'taoParcc/views/js/lib/**/*.js'
            ],
            dest: "<%= grunt.option('sldest') %>",
            //rename: function (dest, src) {
            //    // use the source directory to create the file
            //    // example with your directory structure
            //    //   dest = 'dev/js/'
            //    //   src = 'module1/js/main.js'
            //    var filePath = src.replace('views/js/', '/'); //.replace('views/css/', '/');

            //    return dest + filePath;
            //}
        },

        jsForIR: {
            expand: true,
            cwd: "<%= grunt.option('irtagname') %>/",

            src: ['taoQtiItem/views/**/*.js', 'taoQtiItem/views/**/*.json', 'taoQtiItem/views/**/*.css', 'taoQtiItem/views/**/*.scss', 'taoQtiItem/views/**/.jshintrc', 'taoQtiItem/views/**/*.tpl', 'taoQtiItem/views/js/**/*.png', 'taoQtiItem/views/js/**/*.jpg', 'taoQtiItem/views/js/**/*.gif',
                'taoParcc/views/**/*.js', 'taoParcc/themes/**/*.js', 'taoParcc/views/**', 'taoParcc/**/*.css', 'taoParcc/**/*.map', 'taoParcc/**/*.scss', 'taoParcc/views/**/.jshintrc', 'taoParcc/views/**/*.tpl', 'taoParcc/views/js/**/*.png', 'taoParcc/views/js/**/*.jpg', 'taoParcc/views/js/**/*.gif',
                'tao/views/**/*.js', 'tao/views/**/*.json', 'tao/views/**/*.css', 'tao/views/**/*.scss', 'tao/views/**/.jshintrc', 'tao/views/**/*.tpl', 'tao/views/js/**/*.png', 'tao/views/js/**/*.jpg', 'tao/views/js/**/*.gif',
                'taoItems/**/*.js', 'taoItems/**/*.json', 'taoItems/**/*.css', 'taoItems/**/*.scss', 'taoItems/**/.jshintrc', 'taoItems/**/manifest.php', 'taoItems/**/*.tpl', 'taoItems/views/js/**/*.png', 'taoItems/views/js/**/*.jpg', 'taoItems/views/js/**/*.gif',
                'parccTei/views/**/*.js', 'parccTei/views/**/*.json', 'parccTei/views/**/*.css', 'parccTei/views/**/*.scss', 'parccTei/views/**/.jshintrc', 'parccTei/views/**/*.tpl', 'parccTei/views/js/**/*.png', 'parccTei/views/js/**/*.jpg', 'parccTei/views/js/**/*.gif',
                'qtiItemPic/views/**/*.js', 'qtiItemPic/views/**/*.json', 'qtiItemPic/views/**/*.css', 'qtiItemPic/views/**/*.scss', 'qtiItemPic/views/**/.jshintrc', 'qtiItemPic/views/**/*.tpl', 'qtiItemPic/views/js/**/*.png', 'qtiItemPic/views/js/**/*.jpg', 'qtiItemPic/views/js/**/*.gif',
                'parccStudentTools/views/**/*.js', 'parccStudentTools/views/**/*.json', 'parccStudentTools/views/**/*.css', 'parccStudentTools/views/**/*.scss', 'parccStudentTools/views/**/.jshintrc', 'parccStudentTools/views/**/*.tpl', 'parccStudentTools/views/js/**/*.png', 'parccStudentTools/views/js/**/*.jpg', 'parccStudentTools/views/js/**/*.gif',
                'tao/views/js/lib/mediaelement/flashmediaelement.swf',
                'tao/views/css/font/**', '!**/test/**', '!**/test.js', '!**/locales/**', 'tao/views/locales/**', '!**/templates/**',
                '!taoItems/views/js/scoring/api/scorer.js', '!taoQtiItem/views/js/scoring/**', '!taoParcc/views/js/scoring/processor/expressions/operators/custom/**'
            ],
            dest: "<%= grunt.option('irdest') %>",
            //rename: function (dest, src) {
            //    // use the source directory to create the file
            //    // example with your directory structure
            //    //   dest = 'dev/js/'
            //    //   src = 'module1/js/main.js'
            //    var filePath = src.replace('views/js/', '/'); //.replace('views/css/', '/');

            //    return dest + filePath;
            //}
        },
        IRToDist: {
            files: [
              // includes files within path and its sub-directories
              {
                  expand: true, flatten: true, src: ["<%= grunt.option('irdest') %>/tao/views/build/output/qtiItemRunner.min*", "<%= grunt.option('irdest') %>/tao/views/build/output/studenttools.min*", "<%= grunt.option('irdest') %>/tao/views/build/output/tei.min*", "<%= grunt.option('irdest') %>/taoQtiItem/views/css/*", "!<%= grunt.option('irdest') %>/taoQtiItem/views/css/qti-runner-orig.css*", "!<%= grunt.option('irdest') %>/taoQtiItem/views/css/item-creator.*"], dest: "<%= grunt.option('irdest') %>/dist/"
              },
               {
                   expand: true, flatten: true, src: ["<%= grunt.option('irdest') %>/taoQtiItem/views/css/themes/*", "!<%= grunt.option('irdest') %>/taoQtiItem/views/css/themes/default-orig*"], dest: "<%= grunt.option('irdest') %>/dist/themes/tao/"
               },

               {
                   expand: true, flatten: false, cwd:"<%= grunt.option('irdest') %>/tao/views/js/lib/ckeditor", src: ["**"], dest: "<%= grunt.option('irdest') %>/dist/assets/ckeditor/"
               },
               {
                   expand: true, flatten: true, src: ["<%= grunt.option('irdest') %>/tao/views/js/lib/mediaelement/flashmediaelement.swf"], dest: "<%= grunt.option('irdest') %>/dist/assets/mediaelement/"
               },
               {
                   expand: true,
                   flatten: false,
                   cwd: "OAT/mathjax/",
                   src: ["**"], dest: "<%= grunt.option('irdest') %>/dist/assets/mathjax"
               }
              
            ],
        },

        IRBuildToTDDist: {
            files: [
                {
                    cwd: "<%= grunt.option('irdest') %>/dist/",
                    expand: true,
                    src: ["**"], dest: "OAT/dist/" //removed references to sources.
                }
                ]
        },

        SLBuildToDist: {
            files: [
              // includes files within path and its sub-directories
              {
                  expand: true, flatten: true, src: ["<%= grunt.option('sldest') %>/dist/qtiScorer.min*"], dest: "OAT/dist/"
              }
            ],
        },

        fonts:{
            expand: true,
            cwd:"<%= grunt.option('irdest') %>/tao/views/css/font/",
            src: ["tao/**", "**/*.eot", "**/*.woff"],
            flatten: false,
            dest: "<%= grunt.option('irdest') %>/dist/font/"
        },
        themes: {
            expand: true,
            cwd: "<%= grunt.option('irdest') %>/taoParcc/themes/",
            src: ["**"],
            dest: "<%= grunt.option('irdest') %>/dist/themes/"
        },
        userScripts: {
            files: [
                 {
                     expand: true, flatten: true, src: ["<%= grunt.option('irdest') %>/tao/views/build/output/userScripts.min.*"], dest: "<%= grunt.option('irdest') %>/dist/userScripts/"
                 }
            ]
        },


        scoringToSrc: {
            expand: true,
            cwd: "<%= grunt.option('sldest') %>",
            src: [
                    'taoItems/views/js/scoring/api/scorer.js',
                    'taoQtiItem/views/js/scoring/**',
                    'taoParcc/views/js/scoring/processor/expressions/operators/custom/**'
                ],
            dest: "<%= grunt.option('irdest') %>",
        },

        scoringToDist: {
            files: [
              // includes files within path and its sub-directories
              {
                  expand: true, flatten: true, src: ["<%= grunt.option('sldest') %>/tao/views/build/output/taoParcc/qtiScorer.min*"], dest: "<%= grunt.option('sldest') %>/dist/"
              }
            ],
        },

        

        customSLBuildScript: {
            files: [
                {
                    expand: true, flatten: true, src: ["resources/bundle.js"], dest: "<%= grunt.option('irdest') %>/taoQtiItem/views/build/grunt"
                }
            ]
        },
        
        IRToOATFolder: {
            files: [
                {
                    expand: true,
                    flatten: false,
                    cwd: "<%= grunt.option('irdest') %>/",
                    src: ["**"], dest: "OAT/src/"
                }
            ]
        },

        release: {
            files: [
                
              // includes files within path and its sub-directories
              //{
              //    expand: true, flatten: false, src: ['OAT/src/dist/**', '!OAT/src/dist/themes/tao-default/**', '!**/*.map'], dest: '<%= releaseFolder %>/',
              //    rename: function (dest, src) {
              //            // use the source directory to create the file
              //            // example with your directory structure
              //            //   dest = 'dev/js/'
              //            //   src = 'module1/js/main.js'
              //            var filePath = src.replace('OAT/src/dist/', 'OAT/dist/'); 

              //            return dest + filePath;
              //        }
              //},
              { expand: true, src: ['images/**'], dest: '<%= releaseFolder %>/' },
              { expand: true, flatten: true, src: ['styles/jquery-ui-1.8.22.custom/images/*'], dest: '<%= releaseFolder %>/styles/images/' },
              {
                  expand: true,
                  flatten: false,
                  cwd: "OAT/",
                  src: ["dist/**", "!dist/qtiItemRunner.min.*", "!dist/studenttools.min.*", "!dist/tei.min.*", "!dist/themes/tao-default/**", "!**/*.map"], dest: "<%= releaseFolder %>/OAT/"
              },
              

              //{ expand: true, src: ['styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom-min.css'], dest: '<%= releaseFolder %>/' },
             // { expand: true, src: ['favicon.ico'], dest: '<%= releaseFolder %>/' },
            ],
        },
        releaseDebug: {
            files: [
                //{ expand: true, flatten: false, src: ['OAT/src/dist/**', '!OAT/src/dist/themes/tao-default/**'], dest: '<%= releaseFolder %>/OAT/dist/' },
                { expand: true, src: ['images/**'], dest: '<%= releaseFolder %>/' },
                { expand: true, flatten: true, src: ['styles/jquery-ui-1.8.22.custom/images/*'], dest: '<%= releaseFolder %>/styles/images/' },
                {
                     expand: true,
                     flatten: false,
                     cwd: "OAT/",
                     src: ["dist/**", "!dist/qtiItemRunner.min.*", "!dist/studenttools.min.*", "!dist/tei.min.*", "!dist/themes/tao-default/**"], dest: "<%= releaseFolder %>/OAT/"
                },
             
            ],
        },

        saveOriginalQtiCss: {
            files: [{
                expand: true,
                
                cwd: "<%= grunt.option('irdest') %>/taoQtiItem/views/",
                dest: "<%= grunt.option('irdest') %>/taoQtiItem/views/",
                src: [
                  "scss/qti-runner.scss", "css/qti-runner.css*", "scss/themes/default.scss", "css/themes/default.css*"
                ],
                rename: function (dest, src) {
                    return dest + src.replace('.scss', '-orig.scss').replace('.css', '-orig.css');
                }
            }],
        },

        resetQtiCss: {
            files: [{
                expand: true,
                cwd: "<%= grunt.option('irdest') %>/taoQtiItem/views/",
                dest: "<%= grunt.option('irdest') %>/taoQtiItem/views/",

                src: [
                  "scss/qti-runner-orig.scss", "css/qti-runner-orig.css*", "scss/themes/default-orig.scss", "css/themes/default-orig.css*"
                ],
                rename: function (dest, src) {
                    return dest + src.replace('-orig.scss', '.scss').replace('-orig.css', '.css');
                }
            }],
        }

        

        //irToDist: {
        //    files: [
        //         { expand: true, src: ['OAT/dist/**', '!OAT/dist/themes/tao-default/**'], dest: '<%= releaseFolder %>/' },
        //    ]
        //}

    }

};
