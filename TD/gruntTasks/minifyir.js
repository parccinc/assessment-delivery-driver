﻿module.exports = function (grunt) {
    /*
    Minifies IR from source.  This task takes the location of the IR source code (OAT/src) in most cases and calls the functions necessary to regenerate
    the minified version of IR.

    Steps:
    1. Clean the "dest" folder where IR will be output
    2. Run auto_install which will install any of the grunt npm packages being used by OAT
    3. Makes a back-up copy of the qtiItemRunner and themes scss and css files because the font file paths are different for local vs minified usage.
    4. Replaces the font file paths in the qtirunner and themes scss files.
    5. Run's OAT's existing grunt tasks (TODO: Replace this?)
    6. Copy the build to the dest folder
    7. Build the "user scripts" folder (added by OAT w/ release 0.7.0)
    8. Restores the backup css and scss files from step 3
    9. Purges the tao/views/js/build/output folder which is used by OAT's grunt tasks.
    */

    grunt.registerTask('minifyir', 'Minifies Item Runner from source.  Requires irFolder to be a path to the folder that has the IR source (ex: minifyir:nighlty532/jssrc/)', function (irFolder) {
        if (!irFolder) {
            grunt.warn("You must provide the directory where the IR source is located ex: grunt minifyir:OAT/src/")
        }

        grunt.option('irdest', irFolder);
        console.log('minifying ir in ' + grunt.option('irdest'));



        grunt.task.run(['clean:cleanIRDist', 'auto_install', 'copy:saveOriginalQtiCss', 'replace:qtirunner', 'replace:themes', 'run_oat_ir_build:' + grunt.option('irdest'),
                        'copy:IRToDist', 'copy:fonts', 'copy:themes',
                        'build_user_scripts:' + grunt.option('irdest'),
                        'copy:userScripts', 'copy:resetQtiCss', 'clean:cleanBuildOutput']);
    });
};


