﻿module.exports = function (grunt) {

    grunt.registerTask('minifysl', 'Minifies Scoring Library from source.  Requires irFolder to be a path to the folder that has the SL source (ex: minifyir:nighlty532/jssrc/)', function (irFolder) {
        if (!irFolder) {
            grunt.warn("You must provide the directory where the IR source is located ex: grunt minifyir:nightly532/jssrc/")
        }

        grunt.option('sldest', irFolder);
        console.log('minifying ir in ' + grunt.option('sldest'));

       // grunt.task.run(["copy:customSLBuildScript", "runoatgrunt:" + irFolder, "copy:scoringToDist", 'clean:cleanBuildOutput']);
       // grunt.task.run(["runoatgrunt:" + irFolder, "copy:scoringToDist", 'clean:cleanBuildOutput']);
        grunt.task.run(["runoatgrunt:" + irFolder]); //, "copy:scoringToDist"]);

        
    });

    grunt.registerTask('runoatgrunt', 'Runs OATs requirejs:qtiscorer.  Assumes the correct build script is present', function (irFolder) {
        if (!irFolder) {
            grunt.warn("You must provide the directory where the IR source is located ex: grunt minifyir:nightly532/jssrc/")
        }

        var cb = this.async(false);
        var scorerTask = grunt.util.spawn({
            grunt: true,
            args: ['requirejs:taoparccqtiscorer'],
            opts: {
                cwd: irFolder + '/tao/views/build'
            }
        }, function (error, result, code) {
            console.log(result.stdout);
            cb();

        });

    });

    
};


