﻿module.exports = function (grunt) {

    grunt.registerTask('build_ir_env', 'Clones IR from the repo --irtagname and copies the applicable files into either the /jssrc/ folder inside of the repo or to a folder specified via the --irdest param. Optionally pass in --sltagname to include the scoring library', function () {
        if (!grunt.option('irtagname')) {
            grunt.warn("You must provide irtagname Ex: grunt buildIR --irtagname=nightly532");
        }

        if (!grunt.option('irdest')) {
            grunt.option('irdest', grunt.option('irtagname'));
        }

        grunt.task.run(['cloneIR', 'copy:initialLibs', 'change_irdest:' + grunt.option('irdest') + "/jssrc/", 'copy:jsForIR']); // moved , 'replace:qtirunner', 'replace:themes' to minifyir


        if (grunt.option('sltagname')) {
            if (grunt.option('sltagname') == grunt.option('irtagname'))
            {
                grunt.task.run('add_scoring_lib_to_ir');
            }
            else {
                grunt.task.run(['cloneScoring', 'add_scoring_lib_to_ir']);
            }
            
        }
    });

    grunt.registerTask('change_irdest', 'changes the irdest option', function (newDest) {
        grunt.log.write("move javascript files into /jssrc/ directory inside of " + grunt.option('irdest'));

        console.log("irdest is currently " + grunt.option('irdest'));

        grunt.option('irdest', newDest);

        console.log("irdest is now " + grunt.option('irdest'));
    });


    grunt.registerTask('change_sldest', 'changes the sldest option', function (newDest) {
        grunt.log.write("move javascript files into /jssrc/ directory inside of " + grunt.option('sldest'));

        console.log(" sldest is currently " + grunt.option('sldest'));

        grunt.option('sldest', newDest);
        grunt.option('irdest', newDest);

        console.log(" sldest is now " + grunt.option('sldest'));
        console.log(" irdest is now " + grunt.option('irdest'));
    });
};


