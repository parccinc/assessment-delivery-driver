﻿module.exports = function (grunt) {

    grunt.registerTask('run_oat_ir_build', 'Runs OAT\'s grunt tasks to create minified IR files', function (irFolder) {
        console.log("Running OAT grunt tasks against " + irFolder);

        if (!irFolder) {
            grunt.warn("You must provide the name of the folder that contains the IR source ex: runIRTasks:nightly435")
        }

        var cb = this.async();
        grunt.util.spawn({
            grunt: true,
            // args: ['requirejs:qtinewrunner', 'requirejs:parccstudenttools', 'requirejs:tei', 'taoparccscripts','sass:taoqtiitem', 'requirejs:qtiscorer'],
            args: ['requirejs:qtinewrunner', 'requirejs:parccstudenttools', 'requirejs:tei', 'sass:taoqtiitem'], //, 'requirejs:qtiscorer'],
            opts: {
                cwd: irFolder + '/tao/views/build'
            }
        }, function (error, result, code) {
            console.log(result.stdout);
            cb();

        });
    });
};


