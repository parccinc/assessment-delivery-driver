﻿module.exports = function (grunt) {

    grunt.registerTask('add_scoring_lib_to_ir', 'Copies a scoring library from the scoring repo. into the IR source so it can be built as a part of IR. --sldest and --irdest params are required', function () {
        if (!grunt.option('sldest')) {
            grunt.warn("You must provide the location of the scoring library by setting --sldest");
        }
        if (!grunt.option('irdest')) {
            grunt.warn("You must provide the location of the ir source by setting --irdest.");
        }

        grunt.task.run(['copy:scoringToSrc']);
    });
};

