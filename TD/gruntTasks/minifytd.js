﻿module.exports = function (grunt) {

    grunt.registerTask('minifytd', 'Minifies Test Driver', function (val, minifyIR) {
        
        grunt.log.writeln("Inside set_global");
        grunt.log.writeln(val);

        grunt.log.writeln("Minify Ir " + minifyIR);

        if (!val)
        {
            val = '';
        }

        grunt.config.set('stagingFolder', 'staging' + val);
        grunt.config.set('releaseFolder', 'release' + val);

        grunt.config.set('includeSourceMap', false);

        if (val == 'Debug') {
            grunt.config.set('includeSourceMap', true);
        }


        var releaseParam = 'release' + val;
        var stagingParam = 'staging' + val


        var stagingFolder = grunt.config.get('stagingFolder');
        var releaseFolder = grunt.config.get('releaseFolder')


        grunt.log.writeln(stagingFolder);
        grunt.log.writeln(releaseFolder);
        grunt.log.writeln(grunt.config.get('includeSourceMap'));

        if (minifyIR == 'true') {
            // First copy OAT folders with existing files, and then uglify js and min css and overwrite files.
            grunt.task.run('clean:' + releaseParam, 'minifyir:OAT/src/', 'minifysl:OAT/src/', 'uglify:irBuild', 'clean:cleanOATDist', 'copy:scoringToDist', 'copy:IRBuildToTDDist', 'copy:SLBuildToDist', 'replace:releaseBuild', 'replace:releaseIndex', 'copy:' + releaseParam, 'uglify:releaseBuild', 'cssmin:releaseBuild', 'clean:' + stagingParam, 'clean:cleanOATSrcDist');

        }
        else {
            // First copy OAT folders with existing files, and then uglify js and min css and overwrite files.
            grunt.task.run('clean:' + releaseParam,  'replace:releaseBuild', 'replace:releaseIndex', 'copy:' + releaseParam, 'uglify:releaseBuild', 'cssmin:releaseBuild', 'clean:' + stagingParam, 'clean:cleanOATSrcDist');

        }
        
            
    });


    //grunt.registerTask('debug', ['minifytd:Debug']); //Pass in Debug parameter. This will cause folder to named "releaseDebug" 
    //grunt.registerTask('release', ['minifytd:']); // Do not pass in a parameter.  This will cause folders to remain "release"
    //grunt.registerTask('all', ['debug', 'release']);
    
};
