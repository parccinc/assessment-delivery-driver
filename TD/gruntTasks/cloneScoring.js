﻿module.exports = function (grunt) {

    grunt.registerTask('cloneScoring', "Clones OAT-parcc-scoring repo at a tag specified by --sltagname parameter.  Requires --sltagname parameter. Optional --sldest to create in a particular folder", function () {
        if (!grunt.option('sltagname')) {
            grunt.warn("You must provide sltagname Ex: grunt buildIR --sltagname=0.3.1");
        }
        if (!grunt.option('sldest')) {
            grunt.option('sldest', grunt.option('sltagname'));
        }

        grunt.task.run(['clean:cleanSL', 'gitclone:clonescoring']);
    });
};


