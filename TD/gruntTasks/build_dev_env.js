﻿module.exports = function (grunt) {

    grunt.registerTask('build_dev_env', 'Calls build_ir_env task to clone IR and optionally SL.  Copies the output to OAT/src for TD development.', function () {
        if (!grunt.option('irtagname')) {
            grunt.warn("You must provide irtagname Ex: grunt buildIR --irtagname=nightly532");
        }

        if (!grunt.option('irdest')) {
            grunt.option('irdest', grunt.option('irtagname'));
        }
        
        grunt.task.run('build_ir_env', 'copy:IRToOATFolder');
    });

   
};


