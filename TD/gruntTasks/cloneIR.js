﻿module.exports = function (grunt) {

    grunt.registerTask('cloneIR', "Clones Item Runner branch.  Requires --irtagname parameter. Optional --irdest to create in a particular folder", function () {
        if (!grunt.option('irtagname')) {
            grunt.warn("You must provide irtagname Ex: grunt buildIR --irtagname=nightly532");
        }
        if (!grunt.option('irdest')) {
            grunt.option('irdest', grunt.option('irtagname'));
        }

        console.log("Cloning IR In " + grunt.option('irdest'));

        grunt.task.run(['clean:cleanIR', 'gitclone:cloneIRSrc']);
    });
};


