﻿module.exports = function (grunt) {

    grunt.registerTask('pull_and_build_ir', 'Pulls IR from the repo and builds the minified version --irtagname required. --irdest optional', function () {
        if (!grunt.option('irtagname')) {
            grunt.warn("You must provide irtagname Ex: grunt buildIR --irtagname=nightly532");
        }

        if (!grunt.option('irdest')) {
            grunt.option('irdest', grunt.option('irtagname'));
        }

        console.log("You have selected " + grunt.option("irtagname") + "and it will be created in " + grunt.option('irdest'));

        grunt.task.run(['build_ir_env', 'change_irdest:' + grunt.option('irdest') + "/jssrc/", 'minifyir:' + grunt.option('irdest') + "/jssrc/"]);
    });
};
