﻿/*
Used to build minified version of TD. This grunt task replaces the pre-existing grunt release, grunt debug, grunt all.
Pass
*/
module.exports = function (grunt) {
    grunt.registerTask('build', 'Creates a minified copy of test driver. Pass in debug, release, or all. Optionally accepts a parameter to reminify IR (will not reminify by default).  If not uses whats in OAT/dist. Example: grunt buildtd:all:false', function (version, reminifyIR) {
        var minifyIR = false;
        if (reminifyIR != null) {
            minifyIR = reminifyIR;
        }

        grunt.log.writeln("Minify IR " + minifyIR);
        if (version == "debug") {
            grunt.task.run('minifytd:Debug:' + minifyIR);
        }
        else if (version == "release") {
            grunt.task.run('minifytd::' + minifyIR);
        }
        else {
            grunt.task.run('minifytd:Debug:' + minifyIR, 'minifytd::' + minifyIR);
        }
    });
};