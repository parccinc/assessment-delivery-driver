﻿var TexthelpSpeechStream = new function () { };

// Adding toolbar to the page
TexthelpSpeechStream.g_bAdded = false;
TexthelpSpeechStream.g_strServer = "parcctoolbar.speechstream.net"; //"s3-us-west-2.amazonaws.com/sstoolbar.speechstream.net"; 
TexthelpSpeechStream.g_strBuild = "202"; 
TexthelpSpeechStream.g_strEnglishVoice = "Vocalizer Expressive Ava Premium High 22kHz";
TexthelpSpeechStream.g_strBookId = null;
TexthelpSpeechStream.g_strPageId = null;

TexthelpSpeechStream.addToolbar = function () {
   
    if (!TexthelpSpeechStream.g_bAdded) {

        
        var elem = document.createElement("script");
        elem.type = "text/javascript";
        elem.src = "//" + TexthelpSpeechStream.g_strServer + "/SpeechStream/v" + TexthelpSpeechStream.g_strBuild + "/texthelpMain.js";
        document.body.appendChild(elem);
        

        TexthelpSpeechStream.g_bAdded = true;
    }
};


function $rw_userParameters() {
    try {
        eba_server = TexthelpSpeechStream.g_strServer;
        eba_server_version = TexthelpSpeechStream.g_strBuild;
        eba_live_servers = ["parccservices.speechstream.net"];
        //eba_cache_servers = ["parcccache.speechstream.net"];  // TR 11/11/2015: Had to comment this out because as long as it was present, TTS was trying to pull questions from cache.
        eba_login_name = "parcc";
        eba_icons = 0;
        eba_no_display_icons = main_icons;
        eba_hidden_bar = true;
        eba_continuous_reading = true;
        eba_ignore_buttons = true;
        eba_language = ENGLISH_US;

        eba_voice = TexthelpSpeechStream.g_strEnglishVoice;
        eba_use_html5 = true;
        eba_ssl_flag = (location.protocol == "https:");
//        eba_inline_img = true;

        eba_cache_mode = false;  // TR 11/11/2015: Setting this to false was not sufficient.  Had to also comment out eba_cache_servers key above.
        eba_cache_live_generation = true;

        eba_custId = "2411";

        eba_book_id = "sampleID";  // TR 11/11/2015: Since cacheing is not enabled above, these values are irrelevant.  If cacheing becomes enabled, these should be changed.
        eba_page_id = "sampleID";
        eba_play_start_point = "page-title";

        eba_inline_img = true;
        eba_apip = true;

        //eba_math = true; // TR 12/3/2015: Setting this to true causes all "-" to be read as minus even in non-math tests.  ex: "to-morrow's" becomes "to minus morrow's"
        // Callbacks
        eba_speech_started_callback = "speechStartedCallback";
        eba_speech_complete_callback = "SpeechCompletedCallback";
        eba_rendering_speech_callback = "SpeechRenderingCallback";
        eba_page_complete_callback = "SpeechPageCompletedCallback";

        eba_speech_word_highlight_callback = function (param) {
            // $rw_event_pause();
            console.log(param + ' ');
            // $rw_event_pause();
        }
    }
    catch (err) {
        alert(err);
    }
}

// add event to load automatically after page load


// Use this to add events to a page without removing existing events in the page.
TexthelpSpeechStream.addEvent = function (obj, eventType, func) {
    if (obj.addEventListener) {
        obj.addEventListener(eventType, func, false);
    }
    else if (obj.attachEvent) {
        obj.attachEvent("on" + eventType, func);
    }
};

// TR: This is not being called anywhere so commenting out for now
//TexthelpSpeechStream.addMathJax = function () {
//    TexthelpSpeechStream.g_strSpeechServer = "ec2-52-0-185-3.compute-1.amazonaws.com";

//    window.MathJax = {
//        tex2jax:
//        {
//            inlineMath: [['$', '$'], ["\\(", "\\)"]],
//            processEscapes: true
//        }
//    };


//    var elem = document.createElement("script");
//    elem.type = "text/javascript";
//    elem.src = "//" + TexthelpSpeechStream.g_strServer + "/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML";

//    document.body.appendChild(elem);
//};

TexthelpSpeechStream.addEvent(window, "load", TexthelpSpeechStream.addToolbar);
//TexthelpSpeechStream.addEvent(document, "ready", TexthelpSpeechStream.addToolbar);

//$(document).on('ready', function () {

//    $("img").on("load", function () {
//        alert('body reloaded!');
//        $rw_tagSentences();
//    });
//});

























/*



// Texthelp Systems, Inc. SpeechStream Toolbar Parameters
function $rw_userParameters()
{
  try
  {
        // +++ User customizable parameters start here. +++ 

        // *****************HIDE THE TOOLBAR***********************
        eba_no_display_icons = play_icon + clicktospeak_icon;  // This makes the features callable in a programmatic way without displaying a toolbar.
        eba_icons = no_bar;                                    // Do not display a toolbar – uses public methods to control speech
        eba_hidden_bar = true;
        // eba_hover_flag = true;
        //eba_initial_speech_on = true;                              // This determines if the toolbar is initially hidden when page loaded


        // eba_bubble_mode = true;
        // eba_bubble_freeze_on_shift_flag = true;
        // eba_continuous_reading = false;


        // *****************END HIDE THE TOOLBAR***********************
        eba_icons = main_icons + selectspeed_icon + pause_icon;  // Display the icons
        eba_server = "customerdemo.speechstream.net"; // TexthelpSpeechStream.g_strDomain;
        eba_speech_server = "customerdemo.speechstream.net";
        eba_login_name = "breakthroughtechnologies";
        eba_cust_id = 2410;
        eba_use_html5 = true;
        eba_ssl_flag = window.location.protocol == "https:";
        eba_voice = "Vocalizer Expressive Ava Premium High 22kHz"; // "ScanSoft Karen_Full_22kHz" "Vocalizer Expressive Milena Premium High 22kHz";
        eba_math = true;

        eba_inline_img = true;
        eba_apip = true;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Customization by Radovan =>
        eba_speed_value = FAST_SPEED; // DEFAULT_SPEED; // 100; // FAST_SPEED; 1 - 100 - 100 being the fastest.
        eba_volume_value = 100; // 1 to 100 - 100 being 100% of the current device volume level

        // eba_reading_age = READING_AGE_4; // This is for speed - value is: 25

        eba_no_flash = false; // rw_sendSocketMessage must be defined if this is set to true

        eba_speech_started_callback = "SpeechStartedCallback"; // Radi, nema parametara
        eba_speech_complete_callback = "SpeechCompletedCallback";
        eba_rendering_speech_callback = "SpeechRenderingCallback";
        eba_page_complete_callback = "SpeechPageCompletedCallback";

        eba_speech_word_highlight_callback = function (param) {
            // $rw_event_pause();
            console.log(param + ' ');
            // $rw_event_pause();
        }

        // eba_speech_range_style = "color:darkred; background-color:lightpink; font-size:2em;"; // this is the same as eba_speech_range_colors
        // eba_speech_word_style = "color:#FFFF00; background:#FF0000; padding: 2px; margin: -2px; border-radius: 4px; text-shadow: 0 3px 8px #2A2A2A;"; // "color:#FFFFFF; background:#0000FF; padding: 2px; margin: -2px; border-radius: 4px; text-shadow: 0 3px 8px #2A2A2A";

        eba_language = ENGLISH_US; // ENGLISH_UK;
        eba_locale = LOCALE_US; // LOCALE_UK;

        eba_speak_selection_by_sentence = true;


        // <= end of customization




        /////////////////////////////////////////
        //eba_initial_speech_on = true;
        //eba_hover_flag = true;
        //eba_translate_source = "English";
        //eba_translate_target = "French";

        //eba_bubble_mode = true;
        //eba_bubble_freeze_on_shift_flag = true;
        //eba_continuous_reading = false;
        // +++ End of user customizable parameters section. +++ 
    }
    catch (err) { }
}
*/

function speechStartedCallback() {
    console.log('Speech has started!');
}

function SpeechCompletedCallback() {
    console.log('Speech has finished!');
}

function SpeechRenderingCallback() {
    console.log('Speech rendering has triggered!');
}

var gSpeechCompletedCallback = undefined;
function SpeechPageCompletedCallback() {
    console.log('Speech page has finished!');
    if (gSpeechCompletedCallback) {
        gSpeechCompletedCallback();
    }
}
