/*!	SWFObject v2.2 <http://code.google.com/p/swfobject/> is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> !*/
var g_strAmp = "&";
var swfobject = function () {
	var D = "undefined",
	r = "object",
	S = "Shockwave Flash",
	W = "ShockwaveFlash.ShockwaveFlash",
	q = "application/x-shockwave-flash",
	R = "SWFObjectExprInst",
	x = "onreadystatechange",
	O = window,
	j = document,
	t = navigator,
	T = false,
	U = [h],
	o = [],
	N = [],
	I = [],
	l,
	Q,
	E,
	B,
	J = false,
	a = false,
	n,
	G,
	m = true,
	M = function () {
		var aa = typeof j.getElementById != D && typeof j.getElementsByTagName != D && typeof j.createElement != D,
		ah = t.userAgent.toLowerCase(),
		Y = t.platform.toLowerCase(),
		ae = Y ? /win/.test(Y) : /win/.test(ah),
		ac = Y ? /mac/.test(Y) : /mac/.test(ah),
		af = /webkit/.test(ah) ? parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false,
		X = ! + "\v1",
		ag = [0, 0, 0],
		ab = null;
		if (typeof t.plugins != D && typeof t.plugins[S] == r) {
			ab = t.plugins[S].description;
			if (ab && !(typeof t.mimeTypes != D && t.mimeTypes[q] && !t.mimeTypes[q].enabledPlugin)) {
				T = true;
				X = false;
				ab = ab.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
				ag[0] = parseInt(ab.replace(/^(.*)\..*$/, "$1"), 10);
				ag[1] = parseInt(ab.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
				ag[2] = /[a-zA-Z]/.test(ab) ? parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0
			}
		} else {
			if (typeof O.ActiveXObject != D) {
				try {
					var ad = new ActiveXObject(W);
					if (ad) {
						ab = ad.GetVariable("$version");
						if (ab) {
							X = true;
							ab = ab.split(" ")[1].split(",");
							ag = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)]
						}
					}
				} catch (Z) {}

			}
		}
		return {
			w3 : aa,
			pv : ag,
			wk : af,
			ie : X,
			win : ae,
			mac : ac
		}
	}
	(),
	k = function () {
		if (!M.w3) {
			return
		}
		if ((typeof j.readyState != D && j.readyState == "complete") || (typeof j.readyState == D && (j.getElementsByTagName("body")[0] || j.body))) {
			f()
		}
		if (!J) {
			if (typeof j.addEventListener != D) {
				j.addEventListener("DOMContentLoaded", f, false)
			}
			if (M.ie && M.win) {
				j.attachEvent(x, function () {
					if (j.readyState == "complete") {
						j.detachEvent(x, arguments.callee);
						f()
					}
				});
				if (O == top) {
					(function () {
						if (J) {
							return
						}
						try {
							j.documentElement.doScroll("left")
						} catch (X) {
							setTimeout(arguments.callee, 0);
							return
						}
						f()
					})()
				}
			}
			if (M.wk) {
				(function () {
					if (J) {
						return
					}
					if (!/loaded|complete/.test(j.readyState)) {
						setTimeout(arguments.callee, 0);
						return
					}
					f()
				})()
			}
			s(f)
		}
	}
	();
	function f() {
		if (J) {
			return
		}
		try {
			var Z = j.getElementsByTagName("body")[0].appendChild(C("span"));
			Z.parentNode.removeChild(Z)
		} catch (aa) {
			return
		}
		J = true;
		var X = U.length;
		for (var Y = 0; Y < X; Y++) {
			U[Y]()
		}
	}
	function K(X) {
		if (J) {
			X()
		} else {
			U[U.length] = X
		}
	}
	function s(Y) {
		if (typeof O.addEventListener != D) {
			O.addEventListener("load", Y, false)
		} else {
			if (typeof j.addEventListener != D) {
				j.addEventListener("load", Y, false)
			} else {
				if (typeof O.attachEvent != D) {
					i(O, "onload", Y)
				} else {
					if (typeof O.onload == "function") {
						var X = O.onload;
						O.onload = function () {
							X();
							Y()
						}
					} else {
						O.onload = Y
					}
				}
			}
		}
	}
	function h() {
		if (T) {
			V()
		} else {
			H()
		}
	}
	function V() {
		var X = j.getElementsByTagName("body")[0];
		var aa = C(r);
		aa.setAttribute("type", q);
		var Z = X.appendChild(aa);
		if (Z) {
			var Y = 0;
			(function () {
				if (typeof Z.GetVariable != D) {
					var ab = Z.GetVariable("$version");
					if (ab) {
						ab = ab.split(" ")[1].split(",");
						M.pv = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)]
					}
				} else {
					if (Y < 10) {
						Y++;
						setTimeout(arguments.callee, 10);
						return
					}
				}
				X.removeChild(aa);
				Z = null;
				H()
			})()
		} else {
			H()
		}
	}
	function H() {
		var ag = o.length;
		if (ag > 0) {
			for (var af = 0; af < ag; af++) {
				var Y = o[af].id;
				var ab = o[af].callbackFn;
				var aa = {
					success : false,
					id : Y
				};
				if (M.pv[0] > 0) {
					var ae = c(Y);
					if (ae) {
						if (F(o[af].swfVersion) && !(M.wk && M.wk < 312)) {
							w(Y, true);
							if (ab) {
								aa.success = true;
								aa.ref = z(Y);
								ab(aa)
							}
						} else {
							if (o[af].expressInstall && A()) {
								var ai = {};
								ai.data = o[af].expressInstall;
								ai.width = ae.getAttribute("width") || "0";
								ai.height = ae.getAttribute("height") || "0";
								if (ae.getAttribute("class")) {
									ai.styleclass = ae.getAttribute("class")
								}
								if (ae.getAttribute("align")) {
									ai.align = ae.getAttribute("align")
								}
								var ah = {};
								var X = ae.getElementsByTagName("param");
								var ac = X.length;
								for (var ad = 0; ad < ac; ad++) {
									if (X[ad].getAttribute("name").toLowerCase() != "movie") {
										ah[X[ad].getAttribute("name")] = X[ad].getAttribute("value")
									}
								}
								P(ai, ah, Y, ab)
							} else {
								p(ae);
								if (ab) {
									ab(aa)
								}
							}
						}
					}
				} else {
					w(Y, true);
					if (ab) {
						var Z = z(Y);
						if (Z && typeof Z.SetVariable != D) {
							aa.success = true;
							aa.ref = Z
						}
						ab(aa)
					}
				}
			}
		}
	}
	function z(aa) {
		var X = null;
		var Y = c(aa);
		if (Y && Y.nodeName == "OBJECT") {
			if (typeof Y.SetVariable != D) {
				X = Y
			} else {
				var Z = Y.getElementsByTagName(r)[0];
				if (Z) {
					X = Z
				}
			}
		}
		return X
	}
	function A() {
		return !a && F("6.0.65") && (M.win || M.mac) && !(M.wk && M.wk < 312)
	}
	function P(aa, ab, X, Z) {
		a = true;
		E = Z || null;
		B = {
			success : false,
			id : X
		};
		var ae = c(X);
		if (ae) {
			if (ae.nodeName == "OBJECT") {
				l = g(ae);
				Q = null
			} else {
				l = ae;
				Q = X
			}
			aa.id = R;
			if (typeof aa.width == D || (!/%$/.test(aa.width) && parseInt(aa.width, 10) < 310)) {
				aa.width = "310"
			}
			if (typeof aa.height == D || (!/%$/.test(aa.height) && parseInt(aa.height, 10) < 137)) {
				aa.height = "137"
			}
			j.title = j.title.slice(0, 47) + " - Flash Player Installation";
			var ad = M.ie && M.win ? "ActiveX" : "PlugIn",
			ac = "MMredirectURL=" + O.location.toString().replace(/&/g, "%26") + g_strAmp + "MMplayerType=" + ad + g_strAmp + "MMdoctitle=" + j.title;
			if (typeof ab.flashvars != D) {
				ab.flashvars += g_strAmp + ac
			} else {
				ab.flashvars = ac
			}
			if (M.ie && M.win && ae.readyState != 4) {
				var Y = C("div");
				X += "SWFObjectNew";
				Y.setAttribute("id", X);
				ae.parentNode.insertBefore(Y, ae);
				ae.style.display = "none";
				(function () {
					if (ae.readyState == 4) {
						ae.parentNode.removeChild(ae)
					} else {
						setTimeout(arguments.callee, 10)
					}
				})()
			}
			u(aa, ab, X)
		}
	}
	function p(Y) {
		if (M.ie && M.win && Y.readyState != 4) {
			var X = C("div");
			Y.parentNode.insertBefore(X, Y);
			X.parentNode.replaceChild(g(Y), X);
			Y.style.display = "none";
			(function () {
				if (Y.readyState == 4) {
					Y.parentNode.removeChild(Y)
				} else {
					setTimeout(arguments.callee, 10)
				}
			})()
		} else {
			Y.parentNode.replaceChild(g(Y), Y)
		}
	}
	function g(ab) {
		var aa = C("div");
		if (M.win && M.ie) {
			aa.innerHTML = ab.innerHTML
		} else {
			var Y = ab.getElementsByTagName(r)[0];
			if (Y) {
				var ad = Y.childNodes;
				if (ad) {
					var X = ad.length;
					for (var Z = 0; Z < X; Z++) {
						if (!(ad[Z].nodeType == 1 && ad[Z].nodeName == "PARAM") && !(ad[Z].nodeType == 8)) {
							aa.appendChild(ad[Z].cloneNode(true))
						}
					}
				}
			}
		}
		return aa
	}
	function u(ai, ag, Y) {
		var X,
		aa = c(Y);
		if (M.wk && M.wk < 312) {
			return X
		}
		if (aa) {
			if (typeof ai.id == D) {
				ai.id = Y
			}
			if (M.ie && M.win) {
				var ah = "";
				for (var ae in ai) {
					if (ai[ae] != Object.prototype[ae]) {
						if (ae.toLowerCase() == "data") {
							ag.movie = ai[ae]
						} else {
							if (ae.toLowerCase() == "styleclass") {
								ah += ' class="' + ai[ae] + '"'
							} else {
								if (ae.toLowerCase() != "classid") {
									ah += " " + ae + '="' + ai[ae] + '"'
								}
							}
						}
					}
				}
				var af = "";
				for (var ad in ag) {
					if (ag[ad] != Object.prototype[ad]) {
						af += '<param name="' + ad + '" value="' + ag[ad] + '"/>'
					}
				}
				aa.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + ah + ">" + af + "</object>";
				N[N.length] = ai.id;
				X = c(ai.id)
			} else {
				var Z = C(r);
				Z.setAttribute("type", q);
				for (var ac in ai) {
					if (ai[ac] != Object.prototype[ac]) {
						if (ac.toLowerCase() == "styleclass") {
							Z.setAttribute("class", ai[ac])
						} else {
							if (ac.toLowerCase() != "classid") {
								Z.setAttribute(ac, ai[ac])
							}
						}
					}
				}
				for (var ab in ag) {
					if (ag[ab] != Object.prototype[ab] && ab.toLowerCase() != "movie") {
						e(Z, ab, ag[ab])
					}
				}
				aa.parentNode.replaceChild(Z, aa);
				X = Z
			}
		}
		return X
	}
	function e(Z, X, Y) {
		var aa = C("param");
		aa.setAttribute("name", X);
		aa.setAttribute("value", Y);
		Z.appendChild(aa)
	}
	function y(Y) {
		var X = c(Y);
		if (X && X.nodeName == "OBJECT") {
			if (M.ie && M.win) {
				X.style.display = "none";
				(function () {
					if (X.readyState == 4) {
						b(Y)
					} else {
						setTimeout(arguments.callee, 10)
					}
				})()
			} else {
				X.parentNode.removeChild(X)
			}
		}
	}
	function b(Z) {
		var Y = c(Z);
		if (Y) {
			for (var X in Y) {
				if (typeof Y[X] == "function") {
					Y[X] = null
				}
			}
			Y.parentNode.removeChild(Y)
		}
	}
	function c(Z) {
		var X = null;
		try {
			X = j.getElementById(Z)
		} catch (Y) {}

		return X
	}
	function C(X) {
		return j.createElement(X)
	}
	function i(Z, X, Y) {
		Z.attachEvent(X, Y);
		I[I.length] = [Z, X, Y]
	}
	function F(Z) {
		var Y = M.pv,
		X = Z.split(".");
		X[0] = parseInt(X[0], 10);
		X[1] = parseInt(X[1], 10) || 0;
		X[2] = parseInt(X[2], 10) || 0;
		return (Y[0] > X[0] || (Y[0] == X[0] && Y[1] > X[1]) || (Y[0] == X[0] && Y[1] == X[1] && Y[2] >= X[2])) ? true : false
	}
	function v(ac, Y, ad, ab) {
		if (M.ie && M.mac) {
			return
		}
		var aa = j.getElementsByTagName("head")[0];
		if (!aa) {
			return
		}
		var X = (ad && typeof ad == "string") ? ad : "screen";
		if (ab) {
			n = null;
			G = null
		}
		if (!n || G != X) {
			var Z = C("style");
			Z.setAttribute("type", "text/css");
			Z.setAttribute("media", X);
			n = aa.appendChild(Z);
			if (M.ie && M.win && typeof j.styleSheets != D && j.styleSheets.length > 0) {
				n = j.styleSheets[j.styleSheets.length - 1]
			}
			G = X
		}
		if (M.ie && M.win) {
			if (n && typeof n.addRule == r) {
				n.addRule(ac, Y)
			}
		} else {
			if (n && typeof j.createTextNode != D) {
				n.appendChild(j.createTextNode(ac + " {" + Y + "}"))
			}
		}
	}
	function w(Z, X) {
		if (!m) {
			return
		}
		var Y = X ? "visible" : "hidden";
		if (J && c(Z)) {
			c(Z).style.visibility = Y
		} else {
			v("#" + Z, "visibility:" + Y)
		}
	}
	function L(Y) {
		var Z = /[\\\"<>\.;]/;
		var X = Z.exec(Y) != null;
		return X && typeof encodeURIComponent != D ? encodeURIComponent(Y) : Y
	}
	var d = function () {
		if (M.ie && M.win) {
			window.attachEvent("onunload", function () {
				var ac = I.length;
				for (var ab = 0; ab < ac; ab++) {
					I[ab][0].detachEvent(I[ab][1], I[ab][2])
				}
				var Z = N.length;
				for (var aa = 0; aa < Z; aa++) {
					y(N[aa])
				}
				for (var Y in M) {
					M[Y] = null
				}
				M = null;
				for (var X in swfobject) {
					swfobject[X] = null
				}
				swfobject = null
			})
		}
	}
	();
	return {
		registerObject : function (ab, X, aa, Z) {
			if (M.w3 && ab && X) {
				var Y = {};
				Y.id = ab;
				Y.swfVersion = X;
				Y.expressInstall = aa;
				Y.callbackFn = Z;
				o[o.length] = Y;
				w(ab, false)
			} else {
				if (Z) {
					Z({
						success : false,
						id : ab
					})
				}
			}
		},
		getObjectById : function (X) {
			if (M.w3) {
				return z(X)
			}
		},
		embedSWF : function (ab, ah, ae, ag, Y, aa, Z, ad, af, ac) {
			var X = {
				success : false,
				id : ah
			};
			if (M.w3 && !(M.wk && M.wk < 312) && ab && ah && ae && ag && Y) {
				w(ah, false);
				K(function () {
					ae += "";
					ag += "";
					var aj = {};
					if (af && typeof af === r) {
						for (var al in af) {
							aj[al] = af[al]
						}
					}
					aj.data = ab;
					aj.width = ae;
					aj.height = ag;
					var am = {};
					if (ad && typeof ad === r) {
						for (var ak in ad) {
							am[ak] = ad[ak]
						}
					}
					if (Z && typeof Z === r) {
						for (var ai in Z) {
							if (typeof am.flashvars != D) {
								am.flashvars += g_strAmp + ai + "=" + Z[ai]
							} else {
								am.flashvars = ai + "=" + Z[ai]
							}
						}
					}
					if (F(Y)) {
						var an = u(aj, am, ah);
						if (aj.id == ah) {
							w(ah, true)
						}
						X.success = true;
						X.ref = an
					} else {
						if (aa && A()) {
							aj.data = aa;
							P(aj, am, ah, ac);
							return
						} else {
							w(ah, true)
						}
					}
					if (ac) {
						ac(X)
					}
				})
			} else {
				if (ac) {
					ac(X)
				}
			}
		},
		switchOffAutoHideShow : function () {
			m = false
		},
		ua : M,
		getFlashPlayerVersion : function () {
			return {
				major : M.pv[0],
				minor : M.pv[1],
				release : M.pv[2]
			}
		},
		hasFlashPlayerVersion : F,
		createSWF : function (Z, Y, X) {
			if (M.w3) {
				return u(Z, Y, X)
			} else {
				return undefined
			}
		},
		showExpressInstall : function (Z, aa, X, Y) {
			if (M.w3 && A()) {
				P(Z, aa, X, Y)
			}
		},
		removeSWF : function (X) {
			if (M.w3) {
				y(X)
			}
		},
		createCSS : function (aa, Z, Y, X) {
			if (M.w3) {
				v(aa, Z, Y, X)
			}
		},
		addDomLoadEvent : K,
		addLoadEvent : s,
		getQueryParamValue : function (aa) {
			var Z = j.location.search || j.location.hash;
			if (Z) {
				if (/\?/.test(Z)) {
					Z = Z.split("?")[1]
				}
				if (aa == null) {
					return L(Z)
				}
				var Y = Z.split("&");
				for (var X = 0; X < Y.length; X++) {
					if (Y[X].substring(0, Y[X].indexOf("=")) == aa) {
						return L(Y[X].substring((Y[X].indexOf("=") + 1)))
					}
				}
			}
			return ""
		},
		expressInstallCallback : function () {
			if (a) {
				var X = c(R);
				if (X && l) {
					X.parentNode.replaceChild(l, X);
					if (Q) {
						w(Q, true);
						if (M.ie && M.win) {
							l.style.display = "block"
						}
					}
					if (E) {
						E(B)
					}
				}
				a = false
			}
		}
	}
}
();

if (typeof(SpeechStream) == "undefined") {
	var SpeechStream = new function () {};
}
var baa = "r\x77DontA\x6cter";
var caa = "r\x77T\x48c\x6fmp";
var daa = "r\x77THgen";
var eaa = "r\x77THpgen";
var faa = ["ScanSoft Emily_Full_22kHz", "ScanSoft Samantha_Full_22kHz", "ScanSoft Paulina_Full_22kHz", "ScanSoft Isabel_Full_22kHz", "ScanSoft Virginie_Full_22kHz", "ScanSoft Julie_Full_22kHz", "ScanSoft Steffi_Full_22kHz", "ScanSoft Silvia_Full_22kHz", "ScanSoft Claire_Full_22kHz", "ScanSoft Ingrid_Full_22kHz", "ScanSoft Karen_Full_22kHz", "ScanSoft Raquel_Full_22kHz", "ScanSoft Joana_Full_22kHz", "Vocalizer Expressive Milena Premium High 22kHz", "Vocalizer Expressive Ewa Premium High 22kHz", "Vocalizer Expressive Tarik Premium High 22kHz", "Vocalizer Expressive Tian-tian Premium High 22kHz", "Vocalizer Expressive Sora Premium High 22kHz", "Vocalizer Expressive Sin-ji Premium High 22kHz"];
var gaa = [["\x43lick\x20\x54\x6f\x20Spe\x61k\x20M\x6fde", "\x53elect\x20this\x20then\x20cli\x63k\x20anywher\x65\x20in\x20the\x20bo\x6fk\x20t\x6f\x20st\x61rt\x20rea\x64ing\x20text", "\x48az\x20cli\x63\x20para\x20el\x20mo\x64\x6f\x20\x68abla\x64o"], ["\x53peak\x20The\x20Current\x20Select\x69on", "\x53p\x65ak\x20t\x68e\x20c\x75rrent\x20s\x65lection", "\x4ceer\x20en\x20voz\x20alta\x20e\x6c\x20texto\x20s\x65\x6c\x65\x63cionad\x6f"], ["\x50aus\x65\x20Sp\x65ech", "\x50ause\x20\x53peech", "\x44isc\x75rso\x20\x64e\x20paus\x61"], ["\x53top\x20S\x70ee\x63h", "\x53tops\x20speec\x68\x20playba\x63k", "\x50\x61rar\x20v\x6fz"], ["\x54ranslat\x65\x20W\x6frd", "\x44oub\x6ce-c\x6ci\x63k\x20a\x20word\x20in\x20t\x68e\x20book\x20and\x20\x63lick\x20this\x20\x69con\n" + "t\x6f\x20translat\x65\x20t\x68e\x20\x77ord\x20into\x20\x53panish", "\x54raducir\x20pa\x6cabr\x61"], ["\x46act\x20Fin\x64er", "\x53elect\x20som\x65\x20text\x20\x69n\x20the\x20book\x20and\x20click\x20t\x68\x69s\x20i\x63on\x20to\n" + "\x70\x65rform\x20a\x20Goo\x67le\x20search", "\x42usc\x61dor\x20d\x65\x20datos"], ["\x44iction\x61ry", "\x44ou\x62\x6ce-cl\x69ck\x20a\x20\x77\x6fr\x64\x20in\x20t\x68e\x20book\x20and\x20click\x20th\x69s\x20icon\x20to\n" + "s\x65e\x20d\x69ctionar\x79\x20\x64efinit\x69ons", "\x44i\x63c\x69onario"], ["\x48ig\x68\x6cig\x68t\x20C\x79an", "\x4da\x6be\x20a\x20sele\x63tion\x20in\x20the\x20book\x20and\x20click\x20this\x20ic\x6fn\x20to\n" + "\x63reate\x20a\x20b\x6cue\x20\x68ig\x68l\x69ght", "\x52ealc\x65\x20azul\x20v\x65r\x64\x6fso"], ["\x48i\x67hli\x67ht\x20\x4dagent\x61", "\x4da\x6be\x20a\x20sel\x65ction\x20in\x20the\x20\x62oo\x6b\x20and\x20click\x20this\x20icon\x20to\n" + "\x63reate\x20\x61\x20pink\x20\x68\x69gh\x6cight", "\x52e\x61l\x63e\x20morado"], ["\x48ig\x68light\x20Yello\x77", "\x4dak\x65\x20a\x20se\x6cection\x20in\x20th\x65\x20boo\x6b\x20and\x20clic\x6b\x20this\x20ic\x6fn\x20t\x6f\n" + "\x63reat\x65\x20a\x20yel\x6co\x77\x20highli\x67\x68t", "\x52ealce\x20\x61m\x61rillo"], ["\x48ighlig\x68t\x20Gr\x65en", "\x4dake\x20a\x20select\x69on\x20in\x20th\x65\x20book\x20\x61n\x64\x20click\x20this\x20\x69con\x20to\n" + "\x63reate\x20\x61\x20gr\x65en\x20\x68ighlig\x68t", "\x52\x65alce\x20v\x65rde"], ["\x52\x65move\x20H\x69g\x68lights", "\x52\x65move\x20\x61ll\x20your\x20\x68\x69\x67\x68l\x69g\x68ts\x20\x66ro\x6d\x20th\x69s\x20page", "\x42orrar\x20re\x61lc\x65"], ["\x43o\x6cle\x63t\x20High\x6cights", "\x43ollect\x20\x61\x6cl\x20\x79our\x20hig\x68l\x69ghts\x20\x61nd\x20d\x69splay\x20them\n" + "\x69n\x20a\x20\x77indow,\x20\x67roup\x65d\x20\x62y\x20\x63\x6flor", "\x52ecopil\x61r\x20re\x61l\x63es"], ["\x43lick\x20\x68ere\x20to\x20s\x65l\x65ct\x20th\x65\x20text", "\x43\x6c\x69ck\x20here\x20to\x20select\x20th\x65\x20t\x65xt", "clic aqu" + String.fromCharCode(237) + "\x20para\x20destac\x61r"], ["\x4dP3\x20Maker", "\x4dP3\x20Mak\x65r", "\x4d\x503\x20Maker"], ["\x43\x61lculator", "\x43\x61lculat\x6fr", "\x43alcu\x6cator"], ["\x47en\x65rate\x20Ca\x63h\x65", "\x47\x65nerat\x65\x20Cac\x68e", "\x47ener\x61te\x20Cach\x65"], ["\x43\x68\x65\x63k\x20C\x61che", "\x43hec\x6b\x20Cach\x65", "\x43h\x65ck\x20Cache"], ["\x50i\x63ture\x20Dicti\x6fn\x61ry", "\x50\x69cture\x20Dict\x69onary", "\x44iccion\x61rio\x20visual"], ["\x53\x70el\x6c\x20Check\x65r", "\x53p\x65l\x6c\x20Chec\x6ber", "\x53pell\x20\x43\x68\x65cker"], ["\x48\x6fmoph\x6fne\x20Checker", "\x48omop\x68\x6fne\x20Checker", "\x48omo\x70hone\x20Che\x63\x6ber"], ["\x50re\x64iction\x20C\x68eck\x65r", "\x50redict\x69on\x20C\x68ec\x6b\x65r", "\x50redi\x63tion\x20Checker"], ["\x53ubmit", "\x53ub\x6dit", "\x53ub\x6dit"], ["\x53tic\x6by\x20note", "\x53tic\x6b\x79\x20note", "\x53tick\x79\x20not\x65"], ["\x43reate\x20pr\x6fnunciat\x69\x6fn", "\x43r\x65\x61te\x20pr\x6fnunciat\x69on", "\x43re\x61t\x65\x20\x70ron\x75n\x63iation"], ["\x45\x64it\x20pronun\x63iat\x69on", "\x45dit\x20pron\x75nci\x61ti\x6fn", "\x45d\x69t\x20pronunc\x69ation"], ["\x56oc\x61bulary\x20lookup", "\x56\x6fcab\x75l\x61r\x79\x20lookup", "\x56oc\x61bulary\x20\x6coo\x6bup"], ["\x53trike\x20t\x68rough", "\x4dake\x20a\x20s\x65lect\x69on\x20in\x20the\x20book\x20and\x20click\x20t\x68is\x20\x69con\x20to\n" + "s\x65t\x20strike-t\x68rough\x20styl\x65", "\x53tri\x6b\x65\x20throug\x68"], ["\x53peec\x68\x20Recor\x64er", "\x53p\x65ech\x20Record\x65r", "\x53p\x65ec\x68\x20Re\x63\x6frder"], ["\x53cr\x65en\x20Mask", "\x53creen\x20Mask", "\x53\x63reen\x20M\x61sk"]];
var haa = 0;
var iaa = haa++;
var jaa = haa++;
var kaa = haa++;
var laa = haa++;
var maa = haa++;
var naa = haa++;
var oaa = haa++;
var paa = haa++;
var qaa = haa++;
var raa = haa++;
var saa = haa++;
var taa = haa++;
var uaa = haa++;
var vaa = haa++;
var waa = haa++;
var xaa = haa++;
var yaa = haa++;
var zaa = haa++;
var Aba = haa++;
var Bba = haa++;
var Cba = haa++;
var Dba = haa++;
var Eba = haa++;
var Fba = haa++;
var Gba = haa++;
var Hba = haa++;
var Iba = haa++;
var Jba = haa++;
var Kba = haa++;
var Lba = haa++;
var Mba = haa++;
var Nba = haa++;
var Oba = 0;
var Pba = 1;
var Qba = 2;
var Rba = 3;
var Sba = 4;
var Tba = 5;
var Uba = 6;
var Vba = 7;
var Wba = 8;
var Xba = 9;
var Yba = 10;
var Zba = 11;
var aba = 12;
var bba = 13;
var cba = 14;
var dba = 15;
var eba = 16;
var fba = [0, 33, 66, 99, 132, 165, 198, 264, 297, 330, 363, 396, 429, 0, 528, 462, 561, 594, 231, 0, 0, 0, 0, 495, 627, 660, 759, 792, 825, 858, 693, 726];
var gba = "t\x65xth\x65l\x70StopC\x6fntinu\x6fus";
var hba = "t\x65xth\x65lpSkip";
var FAST_SPEED = 55;
var MEDIUM_SPEED = 40;
var DEFAULT_SPEED = MEDIUM_SPEED;
var SLOW_SPEED = 25;
var VERY_SLOW_SPEED = 15;
var READING_AGE_4 = 25;
var READING_AGE_5 = 25;
var READING_AGE_6 = 26;
var READING_AGE_7 = 27;
var READING_AGE_8 = 28;
var nba = 29;
var READING_AGE_10 = 30;
var READING_AGE_11 = 35;
var READING_AGE_12 = 40;
var READING_AGE_13 = 44;
var READING_AGE_14 = 46;
var READING_AGE_15 = 48;
var READING_AGE_16 = 50;
var oba = "eba_language ENG_UK  ENGLISH UK ENG_US ENGLISH_US SPANISH SPANISH_US ESPANOL SPANISH_ES " + "FRENCH FRENCH_CA FRENCH_CN GERMAN ITALIAN DUTCH SWEDISH AUSTRALIAN PORTUGUESE PORTUGUESE_BR " + "PORTUGUES PORTUGUES_PT";
var ENG_UK = 0;
var UK = 0;
var ENGLISH = 0;
var ENGLISH_UK = 0;
var ENG_US = 1;
var ENGLISH_US = 1;
var SPANISH = 2;
var SPANISH_US = 2;
var ESPANOL = 3;
var SPANISH_ES = 3;
var FRENCH = 4;
var FRENCH_CA = 5;
var FRENCH_CN = 5;
var GERMAN = 6;
var ITALIAN = 7;
var DUTCH = 8;
var SWEDISH = 9;
var AUSTRALIAN = 10;
var PORTUGUESE = 11;
var PORTUGUESE_BR = 11;
var PORTUGUES = 12;
var Jca = 12;
var Kca = 13;
var Lca = 14;
var Mca = 15;
var CHINESE = 16;
var Oca = 17;
var CANTONESE = 18;
var Qca = "eba_locale LOCALE_UK LOCALE_US ";
var LOCALE_UK = "UK";
var LOCALE_US = "US";
var clicktospeak_icon = 1;
var play_icon = 2;
var search_icons = 28;
var translation_icon = 4;
var translate_icon = 4;
var translator_icon = 4;
var factfinder_icon = 8;
var dictionary_icon = 16;
var language_icons = 224;
var spelling_icon = 32;
var homophone_icon = 64;
var prediction_icon = 128;
var highlight_icons = 3840;
var highlightcyan_icon = 256;
var highlightmagenta_icon = 512;
var highlightyellow_icon = 1024;
var highlightgreen_icon = 2048;
var collect_icon = 4096;
var sticky_icon = 16384;
var funplay_icon = 32768;
var proncreate_icon = 65536;
var createpron_icon = 65536;
var pronCreate_icon = 65536;
var pronedit_icon = 131072;
var pronEdit_icon = 131072;
var editpron_icon = 131072;
var selectspeed_icon = 262144;
var selectSpeed_icon = 262144;
var pause_icon = 524288;
var mp3_icon = 1048576;
var calculator_icon = 2097152;
var generatecache_icon = 4194304;
var checkcache_icon = 8388608;
var picturedictionary_icon = 16777216;
var imagedictionary_icon = 16777216;
var vocabulary_icon = 33554432;
var strike_icon = 67108864;
var record_icon = 134217728;
var screenmask_icon = 268435456;
var stop_icon = 536870912;
var fullbrowsealoud_icons = 7967;
var standardbrowsealoud_icons = 31;
var minbrowsealoud_icons = 1;
var submit_icon = 8192;
var no_bar = 0;
var main_icons = 7967;
var standard_icons = 31;
var min_icons = 1;
var title_rw = 0;
var title_ba = 1;
var title_ebooks = 2;
var title_th = 3;
var title_portal = 4;
SpeechStream.EnumPlacement = {
	LEFT : 1,
	RIGHT : 2
};
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (elt) {
		var fwa = this.length;
		var from = Number(arguments[1]) || 0;
		from = (from < 0) ? Math.ceil(from) : Math.floor(from);
		if (from < 0) {
			from += fwa;
		}
		for (; from < fwa; from++) {
			if (from in this && this[from] === elt) {
				return from;
			}
		}
		return -1;
	};
}
String.prototype.trimTH = function () {
	return this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, "");
};
String.prototype.trimStartTH = function () {
	return this.replace(/^[\s\xA0]+/, "");
};
String.prototype.trimEndTH = function () {
	return this.replace(/[\s\xA0]+$/, "");
};
String.prototype.equalsTH = function (s) {
	if (this.length != s.length) {
		return false;
	}
	for (var i = 0; i < this.length; i += 1) {
		if (this.charAt(i) != s.charAt(i)) {
			return false;
		}
	}
	return true;
};
String.prototype.trimSpaceTH = function () {
	return this.replace(/^[\t\r\n ]+/, "").replace(/[\t\r\n ]+$/, "");
};
String.prototype.trimSpaceStartTH = function () {
	return this.replace(/^[\t\r\n ]+/, "");
};
String.prototype.trimSpaceEndTH = function () {
	return this.replace(/[\t\r\n ]+$/, "");
};
/* Copyright 2005-2008 Texthelp Systems Ltd
 */
var Vca = true;
var Wca = true;
var Xca = "dtdType ls_teacherFlag FAST_SPEED DEFAULT_SPEED MEDIUM_SPEED SLOW_SPEED VERY_SLOW_SPEED pause_icon mp3_icon calculator_icon generatecache_icon checkcache_icon picturedictionary_icon test sentence";
var ega;
var Zca = false;
var aca = null;
var bca = false;
var cca = false;
var dca = false;
var eba_actual_version = "195";
var eca = "5";
var fca = "195";
var gca = "167";
var hca = -1;
var ica = "speechus.texthelp.com";
var jca = fullbrowsealoud_icons;
var kca = -1;
var lca = 0;
var mca = ica;
var nca = ica;
var oca = null;
var pca = null;
var qca = null;
var rca = null;
var sca = null;
var tca = null;
var uca = null;
var vca = "SpeechStream";
var wca = "/SpeechStream/";
var xca = "ScanSoft Samantha_Full_22kHz";
var yca = null;
var zca = null;
var Ada = null;
var Bda = -1;
var Cda = "rwonline";
var Dda = "rwonline";
var Eda = 0;
var Fda = false;
var Gda = "US";
var Hda = 40;
var Ida = -1;
var Jda = false;
var Kda = false;
var Lda = false;
var Mda = false;
var Nda = false;
var Oda = false;
var Pda = false;
var Qda = false;
var Rda = null;
var Sda = 1;
var Tda = false;
var Uda = false;
var Vda = 3;
var Wda = 10;
var Xda = false;
var Yda = false;
var Zda = false;
var ada = "*";
var bda = "*";
var cda = "*";
var dda = "*";
var eda = false;
var fda = false;
var gda = true;
var hda = "portal.texthelp.com";
var ida = null;
var jda = null;
var kda = "";
var lda = true;
var mda = null;
var nda = null;
var oda = false;
var pda = false;
var qda = null;
var rda = 10 * 1024;
var sda = false;
var tda = false;
var uda = false;
var vda = -1;
var wda = -1;
var xda = -1;
var yda = -1;
var zda = -1;
var Aea = false;
var Bea = false;
var Cea = false;
var Dea = false;
var Eea = true;
var Fea = null;
var Gea = false;
var Hea = false;
var Iea = false;
var Jea = false;
var Kea = false;
var Lea = false;
var Mea = true;
var Nea = true;
var Oea = true;
var Pea = null;
var Qea = null;
var Rea = false;
var Sea = null;
var Tea = null;
var Uea = 0;
var Vea = 0;
var Wea = 0;
var Xea = new Array();
var Yea = -1;
var Zea = 0;
var aea = 0;
var bea = false;
var cea = false;
var dea = false;
var eea = null;
var fea = false;
var gea = 0;
var hea = "";
function $rw_getAutoCacheMissingCount() {
	return gea;
};
function $rw_getAutoCacheError() {
	return hea || "";
};
var iea = false;
var dtdType;
var kea = false;
var lea = false;
var g_icons = new Array();
var mea = new Array();
var nea = 0;
var oea = 0;
var pea = 300;
var qea = {
	x : 0,
	y : 0
};
var rea = {
	x : 0,
	y : 0
};
var sea = null;
var tea = false;
var uea = 5;
var vea = false;
var wea = 0;
var xea = "";
var yea = 1.0;
var zea = 0.01;
var Afa = true;
var Bfa = 8;
var Cfa = 60;
var Dfa = [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 1.00, 1.00, 1.00];
var Efa = [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.20, 0.20, 0.20, 0.20];
var Ffa = [300, 300, 300, 300, 600, 250, 220, 660, 240, 300, 300, 100, 380, 380, 300, 300, 300];
var Gfa = [40, 40, 40, 40, 40, 250, 40, 60, 256, 30, 30, 312, 227, 420, 338, 420];
var Hfa = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
var $g_bMouseSpeech = false;
var Ifa = "";
var Jfa = false;
var Kfa = false;
var Lfa = navigator.appName == "\x4d\x69cros\x6fft\x20\x49nternet\x20Exp\x6corer";
var Mfa = navigator.appVersion.indexOf("MSIE 6.") > -1;
var Nfa = navigator.appVersion.indexOf("MSIE 7.") > -1;
var Ofa = navigator.appVersion.indexOf("MSIE 8.") > -1;
var Pfa = navigator.appVersion.indexOf("MSIE 9.") > -1;
var Qfa = navigator.appVersion.indexOf("MSIE 10.") > -1;
var Rfa = false;
var Sfa = false;
var Tfa = -1;
var Ufa;
var Vfa;
var Yga = navigator.userAgent.toLowerCase();
var Xfa = Yga.indexOf("\x63hro\x6de") > -1;
var Yfa = Yga.indexOf("\x61pp\x6ceweb\x6b\x69t") > -1;
var Zfa = (Yfa && !Xfa) ? (Yga.indexOf("s\x61f\x61ri") == -1) : false;
var afa = Yga.indexOf("\x66irefox") > -1;
if (!Lfa && !Xfa && !Yfa && !afa) {
	Rfa = Yga.indexOf("trident") > -1;
	if (Rfa) {
		Lfa = true;
	} else {
		afa = true;
	}
}
if (Lfa) {
	if (Mfa) {
		if (document.compatMode) {
			if (document.compatMode != "CSS1Compat") {
				Sfa = true;
			}
		}
	} else if (Nfa) {
		Ofa = true;
		if (document.compatMode) {
			if (document.compatMode != "CSS1Compat") {
				Sfa = true;
			} else {
				Jfa = true;
			}
		}
		if (document.documentMode) {
			Tfa = document.documentMode;
		}
	} else {
		Jfa = true;
		Tfa = document.documentMode;
	}
	if (Tfa >= 9) {
		Vfa = true;
		Ufa = false;
	} else {
		Vfa = false;
		Ufa = true;
	}
}
var bfa = (Yga.indexOf("ipad") > -1 || Yga.indexOf("ipod") > -1 || Yga.indexOf("iphone") > -1 || Yga.indexOf("android") > -1);
var cfa = Yga.indexOf("ipad") > -1 || Yga.indexOf("ipod") > -1 || Yga.indexOf("iphone") > -1;
var hfa = 0;
if (cfa) {
	hfa = parseInt(Yga.substring(Yga.indexOf("cpu os ") + 7, Yga.indexOf("_")));
	if (isNaN(hfa)) {
		hfa = 6;
	}
}
var efa = !(top === self) && cfa;
if (efa) {
	yea = 2000;
}
var ffa = Yga.indexOf("android") > -1;
var gfa;
var hfa = 0;
if (cfa) {
	hfa = parseInt(Yga.substring(Yga.indexOf("cpu os ") + 7, Yga.indexOf("_")));
	if (isNaN(hfa)) {
		hfa = 6;
	}
}
function iOS6Workaround() {
	if ($rw_getTouchSelection) {
		$rw_getTouchSelection();
	}
}
if (hfa >= 6 && efa) {
	setInterval("iOS6Workaround();", 100);
}
var jfa = "\x6c\x6fca\x6chost";
var g_speakableTextAreaTarget = null;
var g_nSpeakableTextAreaTimerId = 0;
var mfa = 1;
var nfa = false;
var ofa = false;
var pfa = false;
var qfa = false;
var rfa = new Array();
var sfa = new Object();
var tfa = "-:minus;+:plus;/:divided by;*:multiplied by;<:less than;>:greater than;=:equals;^:raised to the power of;<=:less than or equal to;>=:greater than or equal to;";
var ufa = false;
var vfa = false;
var wfa = false;
var xfa = false;
var yfa = "mp3";
var zfa = false;
var Aga = null;
var Bga = 20;
var Cga = true;
var Dga = false;
var Ega = "texthelpswa";
var Fga = null;
var Gga = null;
var Hga = null;
var Iga = null;
var Jga = false;
var Kga = false;
var Lga = false;
var Mga = null;
var Nga = null;
var Oga = null;
var Pga = null;
var Qga = false;
var Rga = null;
var Sga = true;
var Tga = null;
var eba_server_version;
var eba_serverVersion;
var eba_client_version;
var eba_clientVersion;
var eba_login_name;
var eba_login_password;
var eba_loginName;
var eba_loginPassword;
var eba_server;
var eba_speech_server;
var eba_speechServer;
var eba_speech_server_backup;
var eba_speechServerBackup;
var eba_folder;
var eba_client_folder;
var eba_clientFolder;
var eba_speech_stream_server_version = -1;
var eba_voice;
var eba_hover_flag;
var eba_bubble_mode;
var eba_bubble_freeze_on_shift_flag;
var eba_voice_from_lang_flag;
var eba_voice_language_map;
var eba_speed_value;
var eba_speedValue;
var eba_speed_offset;
var eba_volume_value;
var eba_initial_speech_on;
var eba_continuous_reading;
var eba_play_start_point;
var eba_speech_redirect_callback;
var eba_speech_started_callback;
var eba_rendering_speech_callback;
var eba_speech_complete_callback;
var eba_page_complete_callback;
var eba_speech_word_highlight_callback;
var eba_speak_selection_by_sentence;
var eba_page_complete_after_selection;
var eba_dont_extend_selection;
var eba_translate_complete_callback;
var eba_use_container;
var eba_override_x;
var eba_override_y;
var eba_icons;
var eba_no_display_icons;
var eba_no_title;
var eba_noTitleFlag;
var eba_hidden_bar;
var eba_speech_range_style;
var eba_speech_word_style;
var eba_allow_alerts_flag;
var eba_alerts;
var eba_language;
var eba_locale;
var eba_ignore_buttons;
var eba_max_word_count;
var eba_logo_url = null;
var eba_inline_img;
var eba_cache_buster;
var eba_dictionary_server;
var eba_custom_dictionary_url;
var eba_alt_dictionary_url;
var eba_picturedictionary_server;
var eba_imagedictionary_server;
var eba_translate_server;
var eba_translation_server;
var eba_translate_source;
var eba_translate_target;
var eba_translate_type;
var eba_language_server;
var eba_spelling_server;
var eba_homophone_server;
var eba_prediction_server;
var eba_mp3_id;
var eba_mp3_limit;
var eba_mp3_callback;
var eba_cust_id;
var eba_custId;
var eba_book_id;
var eba_bookId;
var eba_page_id;
var eba_pageId;
var eba_use_annotations;
var eba_annotate_confirm_delete_note;
var eba_annotate_persist_notes;
var eba_annotate_persist_highlights;
var eba_annotate_note_editor_id;
var eba_annotate_highlight_editor_id;
var eba_annotate_storage_url;
var eba_annotate_note_storage_url;
var eba_annotate_highlight_storage_url;
var eba_annotate_note_reader_id;
var eba_annotate_highlight_reader_id;
var eba_annotate_highlight_store_text;
var eba_speechCacheGenerateFlag;
var eba_cache_building_mode;
var eba_speechCacheFlag;
var eba_speech_cache_flag;
var eba_cache_mode;
var eba_cache_live_generation;
var eba_autoCachePage;
var eba_cacheResult = "";
var eba_cache_retry;
var eba_cache_retry_timeout;
var eba_cache_selection;
var eba_cache_user_text;
var eba_split_cache_path;
var eba_autocache_generate;
var eba_autocache_check;
var eba_autocache_allspeeds;
var eba_autocache_callback;
var eba_autocache_no_alert;
var eba_clientside_pronunciation;
var eba_check_pronunciation_before_cache;
var eba_skip_on_error;
var eba_alter_browser_for_consistency;
var eba_ssl_flag;
var eba_ssl_speech;
var eba_ssl_toolbar;
var eba_search_speech_server;
var eba_replace_speech_server;
var eba_no_flash;
var eba_handle_radio_checkbox_click;
var eba_bypass_dom_check = false;
var eba_limit_cookies;
var eba_ignore_frames;
var eba_math;
var eba_maths;
var eba_symbol_text;
var eba_abbr_array = null;
var eba_date_filter_mode;
var eba_build_cache_for_external_use;
var eba_use_html5;
var eba_speech_file_type;
var eba_use_vocab;
var eba_use_vocabulary;
var eba_vocabulary_server;
var eba_vocabulary_limit;
var eba_break_list;
var eba_use_commands;
var eba_tinymce;
var eba_tinymce_id;
var eba_swa;
var eba_prediction_placement;
var eba_prediction_placement_id;
var eba_homophone_placement;
var eba_homophone_placement_id;
var eba_spelling_placement;
var eba_spelling_placement_id;
var eba_grammar_placement;
var eba_grammar_placement_id;
var eba_disable_speech;
var eba_no_scroll;
var eba_ignore_hidden;
var eba_screen_mask_letterbox_depth = 40;
var eba_screen_mask_opacity = 50;
var $jq;
var eba_apip;
var eba_apip_data;
var eba_apip_order;
var eba_chrome_extension;
var eba_local_file_path;
var ls_teacherFlag = false;
var eba_reading_age;
if (typeof(SpeechStream) == "undefined") {
	SpeechStream = {};
}
SpeechStream.DataTypes = {};
SpeechStream.DataTypes.PageData = function () {
	var self = this;
	var Vga = "~p~br~head~body~hr~div~h1~h2~h3~h4~h5~h6~blockquote~table~tbody~tr~td~th~";
	var Wga = "~em~strong~b~i~u~tt~font~kbd~dfn~cite~sup~sub~a~embed~span~small~nobr~wbr~acronym~" + "abbr~code~s~chunk~th:pron~img~/th:pron~w~/w~lic/lic~break~silence~";
	this.gja = Vga;
	this.hja = Wga;
	this.strHighlightTag = "span";
	this.placeholder = document.body;
	this.isInBreakList = function (kxb) {
		return self.gja.indexOf("~" + kxb + "~") > -1;
	};
	this.isInStyleList = function (kxb) {
		return self.hja.indexOf("~" + kxb + "~") > -1;
	};
};
SpeechStream.DataTypes.ControlData = function () {
	this.bIgnoreSkipSection = false;
	this.bOnloadFinished = false;
};
SpeechStream.DataTypes.HighlightData = function () {
	this.strSpeechRangeColours = "color:#000000; background:#FFFF00";
	if (Qga) {
		this.strSpeechWordColours = "color:#FFFFFF; background:#0000FF; padding: 2px; margin: -2px; border-radius: 4px; text-shadow: 0 3px 8px #2A2A2A";
	} else {
		this.strSpeechWordColours = "color:#FFFFFF; background:#0000FF; border-radius: 1px;";
	}
};
SpeechStream.DataTypes.BrowserData = function () {
	var mBc = navigator.appVersion;
	this.bIE = navigator.appName == "\x4dicroso\x66t\x20Int\x65rnet\x20Explorer";
	this.bIE6 = mBc.indexOf("MSIE 6.") > -1;
	this.bIE7 = mBc.indexOf("MSIE 7.") > -1;
	this.bIE8 = mBc.indexOf("MSIE 8.") > -1;
	this.bIE9 = mBc.indexOf("MSIE 9.") > -1;
	this.bIE10 = mBc.indexOf("MSIE 10.") > -1;
	this.bIE11 = false;
	this.bIEBackCompat = false;
	this.nIEDocumentMode = -1;
	this.bIEOld = false;
	this.bIENew = false;
	this.dtdType = "";
	this.bXDTDType = false;
	this.bLooseType = false;
	if (this.bIE) {
		if (this.bIE6) {
			if (document.compatMode) {
				if (document.compatMode != "CSS1Compat") {
					this.bIEBackCompat = true;
				}
			}
		} else if (this.bIE7) {
			this.bIE8 = true;
			if (document.compatMode) {
				if (document.compatMode != "CSS1Compat") {
					this.bIEBackCompat = true;
				} else {
					this.bXDTDType = true;
				}
			}
			if (document.documentMode) {
				this.nIEDocumentMode = document.documentMode;
			}
		} else {
			this.bXDTDType = true;
			this.nIEDocumentMode = document.documentMode;
		}
	}
	if (this.bIE) {
		if (this.nIEDocumentMode >= 9) {
			this.bIENew = true;
			this.bIEOld = false;
		} else {
			this.bIENew = false;
			this.bIEOld = true;
		}
	}
	var Yga = navigator.userAgent.toLowerCase();
	this.bChrome = Yga.indexOf("\x63hrome") > -1;
	this.bSafari = Yga.indexOf("\x61pplewe\x62kit") > -1;
	this.bWebkit = (this.bSafari && !this.bChrome) ? (Yga.indexOf("s\x61fari") == -1) : false;
	this.bFireFox = Yga.indexOf("\x66\x69ref\x6fx") > -1;
	if (!this.bIE && !this.bChrome && !this.bSafari && !this.bFireFox) {
		this.bIE11 = Yga.indexOf("trident" > -1);
		if (this.bIE11) {
			this.bIE = true;
		} else {
			this.bFireFox = true;
		}
	}
};
SpeechStream.DataTypes.BubbleData = function () {
	this.bBubbleMode = false;
	this.bBubbleFreezeOnShiftFlag = true;
	this.bBubbleModeStartDisabled = false;
};
SpeechStream.DataTypes.Paths = function () {
	var Zga = "strFileLoc strSwfLoc";
	this.strFileLoc = "";
	this.strSwfLoc = "";
};
SpeechStream.Data = function () {
	this.browser = new SpeechStream.DataTypes.BrowserData();
	this.paths = new SpeechStream.DataTypes.Paths();
	this.pageData = new SpeechStream.DataTypes.PageData();
	this.controlData = new SpeechStream.DataTypes.ControlData();
	this.highlightData = new SpeechStream.DataTypes.HighlightData();
	this.bubbleData = new SpeechStream.DataTypes.BubbleData();
};
SpeechStream.data = new SpeechStream.Data();
var aga = SpeechStream.data;
function $rw_getVersion() {
	return eba_actual_version;
}
function $rw_getRevision() {
	return eca;
}
function $rw_setIconsToLoad(p_nIcons) {
	var bga = false;
	if ((p_nIcons & clicktospeak_icon) == clicktospeak_icon) {
		if (!aga.bubbleData.bBubbleMode) {
			vda = Hza('hover', gaa[iaa][Eda], fba[iaa], true);
		}
		bga = true;
	}
	if (Qga) {
		if ((p_nIcons & dictionary_icon) == dictionary_icon) {
			Hza('dictionary', gaa[oaa][Eda], fba[oaa], false);
		}
		if ((p_nIcons & picturedictionary_icon) == picturedictionary_icon) {
			Hza('picturedictionary', gaa[Aba][Eda], fba[Aba], false);
		}
	}
	if ((p_nIcons & play_icon) == play_icon) {
		if (!(Nda && !Mda && !Oda)) {
			if (!aga.bubbleData.bBubbleMode) {
				Hza('play', gaa[jaa][Eda], fba[jaa], false);
			}
			bga = true;
		}
	}
	if ((p_nIcons & pause_icon) == pause_icon) {
		Hza('pause', gaa[kaa][Eda], fba[kaa], false);
	}
	var cga = false;
	if ((p_nIcons & funplay_icon) == funplay_icon) {
		Hza('funplay', gaa[jaa][Eda], fba[Mba], false);
		cga = true;
		bga = false;
	}
	if ((p_nIcons & stop_icon) == stop_icon) {
		bga = true;
	}
	if (bga) {
		Hza('stop', gaa[laa][Eda], fba[laa], false);
	}
	if (cga) {
		Hza('funstop', gaa[laa][Eda], fba[Nba], false);
	}
	if ((p_nIcons & translation_icon) == translation_icon) {
		Hza('trans', gaa[maa][Eda], fba[maa], false);
	}
	if ((p_nIcons & factfinder_icon) == factfinder_icon) {
		Hza('ffinder', gaa[naa][Eda], fba[naa], false);
	}
	if (!Qga) {
		if ((p_nIcons & dictionary_icon) == dictionary_icon) {
			Hza('dictionary', gaa[oaa][Eda], fba[oaa], false);
		}
		if ((p_nIcons & picturedictionary_icon) == picturedictionary_icon) {
			Hza('picturedictionary', gaa[Aba][Eda], fba[Aba], false);
		}
	}
	if ((p_nIcons & spelling_icon) == spelling_icon) {
		xda = Hza('spell', gaa[Bba][Eda], fba[Bba], true);
	}
	if ((p_nIcons & homophone_icon) == homophone_icon) {
		yda = Hza('homophone', gaa[Cba][Eda], fba[Cba], true);
	}
	if ((p_nIcons & prediction_icon) == prediction_icon) {
		zda = Hza('pred', gaa[Dba][Eda], fba[Dba], true);
	}
	var dga = false;
	if ((p_nIcons & highlightcyan_icon) == highlightcyan_icon) {
		Hza('cyan', gaa[paa][Eda], fba[paa], false);
		dga = true;
	}
	if ((p_nIcons & highlightmagenta_icon) == highlightmagenta_icon) {
		Hza('magenta', gaa[qaa][Eda], fba[qaa], false);
		dga = true;
	}
	if ((p_nIcons & highlightyellow_icon) == highlightyellow_icon) {
		Hza('yellow', gaa[raa][Eda], fba[raa], false);
		dga = true;
	}
	if ((p_nIcons & highlightgreen_icon) == highlightgreen_icon) {
		Hza('green', gaa[saa][Eda], fba[saa], false);
		dga = true;
	}
	if ((p_nIcons & strike_icon) == strike_icon) {
		Hza('strike', gaa[Jba][Eda], fba[Jba], false);
		dga = true;
	}
	if (dga) {
		Hza('clear', gaa[taa][Eda], fba[taa], false);
	}
	if ((p_nIcons & collect_icon) == collect_icon) {
		Hza('collect', gaa[uaa][Eda], fba[uaa], false);
	}
	if ((p_nIcons & vocabulary_icon) == vocabulary_icon) {
		Hza('vocabulary', gaa[Iba][Eda], fba[Iba], false);
	}
	if ((p_nIcons & mp3_icon) == mp3_icon) {
		Hza('mp3', gaa[waa][Eda], fba[waa], false);
	}
	if ((p_nIcons & calculator_icon) == calculator_icon) {
		Hza('calculator', gaa[xaa][Eda], fba[xaa], false);
	}
	if ((p_nIcons & record_icon) == record_icon) {
		Hza('recorder', gaa[Kba][Eda], fba[Kba], false);
	}
	if ((p_nIcons & generatecache_icon) == generatecache_icon) {
		Hza('generate_cache', gaa[yaa][Eda], fba[yaa], false);
	}
	if ((p_nIcons & checkcache_icon) == checkcache_icon) {
		Hza('check_cache', gaa[zaa][Eda], fba[zaa], false);
	}
	if ((p_nIcons & submit_icon) == submit_icon) {
		Hza('submit', gaa[Eba][Eda], fba[Eba], false);
	}
	if ((p_nIcons & sticky_icon) == sticky_icon) {
		wda = Hza('sticky', gaa[Fba][Eda], fba[Fba], true);
	}
	if (yca != null && zca != null && Ada != null) {
		if ((p_nIcons & pronCreate_icon) == pronCreate_icon) {
			Hza('pronCreate', gaa[Gba][Eda], fba[Gba], false);
		}
		if ((p_nIcons & pronEdit_icon) == pronEdit_icon) {
			Hza('pronEdit', gaa[Hba][Eda], fba[Hba], false);
		}
	}
	if ((p_nIcons & screenmask_icon) == screenmask_icon) {
		Hza('screenmask', gaa[Lba][Eda], fba[Lba], false);
	}
}
var ega = 0;
function $rw_setVoice(kxb) {
	if (typeof(kxb) == "string") {
		if (kxb != null && kxb.length > 0 && kxb != xca) {
			eba_voice = kxb;
			xca = kxb;
			try {
				var tga = xbb.getConnector();
				if (tga != null) {
					tga.setVoiceName(xca);
				}
			} catch (err) {
				thLogE(err);
			}
		}
	}
}
function $rw_setVoiceForLanguage(kxb, p_languageCode) {
	var AKb;
	if (typeof(p_languageCode) == "string") {
		AKb = iga(p_languageCode);
		if (AKb == -1) {
			try {
				AKb = parseInt(p_languageCode, 10);
			} catch (err) {
				thLogE(err);
				return;
			}
		}
	} else if (typeof(p_languageCode) == "number") {
		AKb = p_languageCode;
	} else {
		return;
	}
	if (typeof(kxb) == "string" && kxb != null && kxb.length > 0 && AKb >= 0 && AKb < faa.length) {
		faa[AKb] = kxb;
	}
}
function iga(jga) {
	var lib = jga.toLowerCase();
	if (lib == "english" || lib == "english_uk") {
		return ENG_UK;
	} else if (lib == "english_us") {
		return ENG_US;
	} else if (lib == "spanish") {
		return SPANISH;
	} else if (lib == "espanol") {
		return ESPANOL;
	} else if (lib.substr(0, 4) == "espa" && lib == ("espa" + String.fromCharCode(241) + "ol")) {
		return ESPANOL;
	} else if (lib == "french") {
		return FRENCH;
	} else if (lib.substr(0, 4) == "fran" && lib == ("fran" + String.fromCharCode(231) + "ais")) {
		return FRENCH;
	} else if (lib == "french_ca") {
		return FRENCH_CA;
	} else if (lib == "german") {
		return GERMAN;
	} else if (lib == "italian") {
		return ITALIAN;
	} else if (lib == "dutch") {
		return DUTCH;
	} else if (lib == "swedish") {
		return SWEDISH;
	} else if (lib == "australian") {
		return AUSTRALIAN;
	} else if (lib == "portuguese") {
		return PORTUGUESE;
	} else if (lib == "portugues") {
		return PORTUGUES;
	} else {
		return -1;
	}
}
function lga(Xhb) {
	try {
		var ija = Xhb.split("~");
		var UBc = ija.length;
		var i;
		var lib;
		var DOb;
		for (i = 0; i < UBc; i += 2) {
			DOb = ija[i];
			lib = ija[i + 1];
			$rw_setVoiceForLanguage(DOb, lib);
		}
	} catch (e) {}

}
function $rw_setSpeedValue(p_nSpeedValue) {
	if (typeof(p_nSpeedValue) == "number") {
		if (p_nSpeedValue > -4 && p_nSpeedValue < 101 && p_nSpeedValue != Hda) {
			eba_speedValue = p_nSpeedValue;
			eba_speed_value = p_nSpeedValue;
			eba_reading_age = p_nSpeedValue;
			Hda = p_nSpeedValue;
			try {
				if (kea) {
					var tga = xbb.getConnector();
					if (tga != null) {
						tga.setSpeedValue("" + Hda);
					}
				}
			} catch (err) {
				thLogE(err);
			}
		}
	} else if (typeof(p_nSpeedValue) == "string") {
		var Wnb = p_nSpeedValue.toUpperCase();
		if (Wnb == "VERY_SLOW_SPEED") {
			$rw_setSpeedValue(VERY_SLOW_SPEED);
		} else if (Wnb == "SLOW_SPEED") {
			$rw_setSpeedValue(SLOW_SPEED);
		} else if (Wnb == "MEDIUM_SPEED") {
			$rw_setSpeedValue(MEDIUM_SPEED);
		} else if (Wnb == "FAST_SPEED") {
			$rw_setSpeedValue(FAST_SPEED);
		} else {
			var uvb = parseInt(p_nSpeedValue, 10);
			$rw_setSpeedValue(uvb);
		}
	}
}
function $rw_setVolumeValue(p_nVolumeValue) {
	if (typeof(p_nVolumeValue) == "number") {
		if (p_nVolumeValue >= -1 && p_nVolumeValue <= 100 && p_nVolumeValue != Ida) {
			eba_volume_value = p_nVolumeValue;
			Ida = p_nVolumeValue;
			try {
				if (kea) {
					var tga = xbb.getConnector();
					if (tga != null) {
						tga.setVolumeValue("" + Ida);
					}
				}
			} catch (err) {
				thLogE(err);
			}
		}
	}
}
function $rw_setBarVisibility(ILb) {
	if (typeof(ILb) == "\x62\x6folean") {
		var uga = document.getElementById("r\x77Drag");
		if (ILb) {
			uga.style.visibility = "\x76is\x69ble";
			uga.style.display = "\x69nline";
		} else {
			uga.style.visibility = "\x68idden";
			uga.style.display = "n\x6fne";
		}
		Kda = !ILb;
		jKb();
	}
}
function $rw_enableClickToSpeak(p_bEnable) {
	if (p_bEnable && !$g_bMouseSpeech) {
		$rw_event_hover(null, vda);
	} else if (!p_bEnable && $g_bMouseSpeech) {
		$rw_event_hover(null, vda);
		if (vda > -1) {
			var hta = wea;
			wea = 0;
			hIb("hover" + "", vda, true);
			wea = hta;
		}
	}
}
function $rw_enableSpeachByBubbleMode(uEb) {
	var wga = false;
	if (typeof(SpeechStream.bubbleSpeech) == "undefinded" || typeof(SpeechStream.bubbleSpeech.initSpeechBubble) == "undefinded") {
		aga.bubbleData.bBubbleModeStartDisabled = !uEb;
		return;
	}
	if (aga.controlData.bOnloadFinished) {
		aga.bubbleData.bBubbleMode = uEb;
		if (!aga.bubbleData.bBubbleMode) {
			SpeechStream.bubbleSpeech.hideStartBubble();
			SpeechStream.bubbleSpeech.hideStopBubble();
			$rw_stopSpeech();
		}
	} else {
		aga.bubbleData.bBubbleModeStartDisabled = !uEb;
	}
}
function $rw_enableContinuousReading(uEb) {
	Eea = uEb;
	eba_continuous_reading = uEb;
	if (!Eea) {
		Fea = null;
		Mea = false;
	} else {
		if (typeof(eba_speak_selection_by_sentence) == "boolean") {
			Mea = eba_speak_selection_by_sentence;
		} else {
			Mea = true;
		}
		if ($rw_isSpeaking() && rLb != null && Fea == null) {
			EJb();
			JJb(rLb);
		}
	}
}
function $rw_getUserTarget() {
	return Tga;
}
var xga = "The SpeechStream object will contain parameter objects in the future. It holds actionOnError cacheMode and pronunciation";
SpeechStream.EnumActionOnError = {
	STOP : 0,
	SKIP : 1
};
SpeechStream.ActionOnError = function () {
	this.action = SpeechStream.EnumActionOnError.STOP;
};
var yga = function () {
	this.NONE = 0;
	this.CACHE_WITH_LIVE_SERVER = 1;
	this.CACHE_ONLY = 2;
	this.CACHE_BUILDING_MODE = 3;
	this.mode = this.NONE;
	this.getLiveServer = function () {
		if (this.mode == this.NONE) {
			return nca;
		} else if (this.mode == this.CACHE_ONLY) {
			return null;
		} else if (this.mode == this.CACHE_WITH_LIVE_SERVER) {
			return oca;
		} else if (this.mode == this.CACHE_BUILDING_MODE) {
			if (oca != null) {
				return oca;
			} else {
				return nca;
			}
		}
	};
	this.useBackupForLiveRequests = function () {
		if (this.mode == this.NONE) {
			return false;
		} else if (this.mode == this.CACHE_ONLY) {
			return false;
		} else if (this.mode == this.CACHE_WITH_LIVE_SERVER) {
			return oca != null;
		} else if (this.mode == this.CACHE_BUILDING_MODE) {
			return oca != null;
		}
	};
	this.setCacheMode = function (p_nMode) {
		try {
			var flash = xbb.getConnector();
			if (flash != null) {
				switch (p_nMode) {
				case this.NONE:
					this.mode = this.NONE;
					eba_cache_mode = false;
					eba_cache_building_mode = false;
					eba_cache_live_generation = false;
					Nda = false;
					Mda = false;
					Oda = false;
					flash.setCacheMode(false, false);
					break;
				case this.CACHE_WITH_LIVE_SERVER:
					this.mode = this.CACHE_WITH_LIVE_SERVER;
					eba_cache_mode = true;
					eba_cache_building_mode = false;
					eba_cache_live_generation = true;
					Nda = true;
					Mda = false;
					Oda = true;
					flash.setCacheMode(true, true);
					break;
				case this.CACHE_ONLY:
					this.mode = this.CACHE_ONLY;
					eba_cache_mode = true;
					eba_cache_building_mode = false;
					eba_cache_live_generation = false;
					Nda = true;
					Mda = false;
					Oda = false;
					flash.setCacheMode(true, false);
					break;
				case this.CACHE_BUILDING_MODE:
					this.mode = this.CACHE_BUILDING_MODE;
					eba_cache_mode = false;
					eba_cache_building_mode = true;
					eba_cache_live_generation = false;
					Nda = false;
					Mda = true;
					Oda = false;
					flash.setCacheMode(true, true);
					break;
				default:
					eya("Tried to set to an invalid mode, " + p_nMode + " is not recognised.");
				}
			}
		} catch (err) {}

	};
};
SpeechStream.actionOnError = new SpeechStream.ActionOnError();
SpeechStream.cacheMode = new yga();
function $rw_stopSpeech() {
	$rw_event_stop();
}
var Aha = 200;
function $rw_speakFromId(id) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	if (Oga != null) {
		var gCc = document.getElementById(id);
		SSAPIP.apipHandler.checkElement(gCc, SSAPIP.apipHandler.Type.DEFAULT);
		return;
	}
	var target = Nha(id);
	if (target != null) {
		$rw_stopSpeech();
		if (Mea) {
			var ota;
			ota = zta(target, false);
			Pea = null;
			if (ota != null) {
				target = ota;
			}
		}
		Tga = target;
		rw_speakHoverTarget(target);
	} else {
		var gCc = document.getElementById(id);
		if (gCc != null) {
			gCc = SSDOM.getNextTextNodeNoBlank(gCc, false, null);
			ega = 0;
			$rw_speakCurrentSentence(gCc, 0);
		}
	}
}
function $rw_speakById(id) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	if (Oga != null) {
		var gCc = document.getElementById(id);
		SSAPIP.apipHandler.checkElement(gCc, SSAPIP.apipHandler.Type.SINGLE_ITEM);
		return;
	}
	var target = Nha(id);
	if (target != null) {
		$rw_stopSpeech();
		var ota;
		if (Mea) {
			ota = zta(target, false);
			if (ota == null) {
				Pea = target;
			}
		} else {
			ota = target;
			ota.allowContinuous = false;
		}
		if (ota != null) {
			target = ota;
			rw_speakHoverTarget(target);
		} else {
			target.blockCache = !Tda;
			rw_speakHoverTarget(target);
		}
		Tga = target;
	}
}
function $rw_speakByIdFromFile(id, XNb) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	var target = Nha(id);
	if (target != null) {
		$rw_stopSpeech();
		Tga = target;
		TNb(target, XNb);
	}
}
function $rw_speakByIdHighlightOnly(id) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	var target = Nha(id);
	if (target != null) {
		$rw_stopSpeech();
		Tga = target;
		rw_speechHighlightOnly(target);
	}
}
function $rw_speakByIdWithSpeaker(id) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	var target = Nha(id);
	if (target != null) {
		$rw_stopSpeech();
		Sea = id;
		Tga = target;
		rw_speakHoverTarget(target);
	} else {
		var gCc = document.getElementById(id);
		if (gCc != null && gCc.nodeType == 1 && gCc.firstChild == null) {
			Sea = id;
			ega = 0;
			$rw_speakFromId(id);
		}
	}
}
function Nha(id) {
	var qhb = document.getElementById(id);
	if (qhb != null) {
		if (qhb.nodeType == 1 && qhb.tagName == "img" && qhb.getAttribute("msg") == null) {
			return new THHoverTarget(qhb.ownerDocument.body, SSDOM.getPositionInDom(qhb), null);
		}
		var Oha = qhb.innerHTML;
		if (Oha.length > 0) {
			var rOb = SSDOM.getFirstChildTextNode(qhb, true);
			if (rOb == null || rOb.nodeType != 3) {
				if (rOb != null && rOb.nodeType == 1 && rOb.tagName == "img") {
					var attr = rOb.getAttribute("msg");
					if (attr != null && attr.length > 0) {}
					else {
						return null;
					}
				} else {
					return null;
				}
			}
			var stb = SSDOM.getLastChildTextNode(qhb, true);
			if (stb == null || stb.nodeType != 3) {
				if (stb != null && stb.nodeType == 1 && stb.tagName == "img") {
					var attr = stb.getAttribute("msg");
					if (attr != null && attr.length > 0) {}
					else {
						return null;
					}
				} else {
					return null;
				}
			}
			var Upb = ZUb(rOb, 0);
			var Vpb = (stb.nodeType == 3) ? ZUb(stb, stb.nodeValue.length) : ZUb(stb, 0);
			var tcb = new THRange(qhb.ownerDocument.body, Upb, Vpb);
			var target = new THHoverTarget(null, null, tcb);
			return target;
		}
	}
	return null;
}
function $rw_setSentenceFromSelection() {
	try {
		var Rpb = $rw_getTHCaretRangeFromSelection();
		if (Rpb == null) {
			return;
		}
		var ygb = SSDOM.getSentenceBreakToLeft(Rpb.ygb);
		var zgb = SSDOM.getSentenceBreakToRight(Rpb.zgb);
		if (ygb != null && zgb != null) {
			var Xha = new qqa(ygb, zgb);
			var tcb = TQb(Xha);
			if (tcb != null) {
				Tga = new THHoverTarget(null, null, tcb);
			}
		}
	} catch (err) {
		thLogE(err);
	}
}
function $rw_speakSentenceAtNode(Bub, Thb) {
	if (!Bub && (Bub.nodeType != 1 || Bub.nodeType != 3)) {
		return;
	}
	$rw_speakCurrentSentence(Bub, Thb);
}
function $rw_speakCurrentSentence(Bub, Thb) {
	var EKb = (new Date).getTime();
	var aha = (EKb - ega);
	if (aha < Aha) {
		return;
	}
	ega = EKb;
	var Yia = mha(Bub, Thb);
	if (Yia != null) {
		if (Yia.equals(Tga)) {
			if (aha < Aha * 5) {
				return;
			}
		}
		$rw_stopSpeech();
		Tga = Yia;
		rw_speakHoverTarget(Yia);
	}
}
function $rw_speakCurrentSentenceHighlightOnly(Bub, Thb) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	var Yia = mha(Bub, Thb);
	if (Yia != null) {
		$rw_stopSpeech();
		Tga = Yia;
		rw_speechHighlightOnly(Yia);
	}
}
function fha(Bub, Thb) {
	var sBc;
	if (typeof(Thb) == "undefined") {
		sBc = new kqa(Bub, 0, true);
	} else {
		sBc = new kqa(Bub, Thb, true);
	}
	var ygb = SSDOM.getSentenceBreakToLeft(sBc);
	var zgb = SSDOM.getSentenceBreakToRight(sBc);
	if (ygb == null || zgb == null) {
		return null;
	}
	return new qqa(ygb, zgb);
}
function mha(Bub, Thb) {
	var tcb;
	var Yia;
	if (typeof(Bub) == "undefined" || Bub == null) {
		if (Tga == null) {
			var Rpb = SSDOM.getFirstSentence(document.body);
			if (Rpb == null) {
				return null;
			}
			tcb = TQb(Rpb);
			Yia = new THHoverTarget(null, null, tcb);
		} else {
			Yia = Tga;
		}
	} else {
		if (Bub instanceof qqa) {
			tcb = TQb(Bub);
		} else {
			var Rpb = fha(Bub, Thb);
			if (Rpb == null) {
				return null;
			}
			tcb = TQb(Rpb);
		}
		Yia = new THHoverTarget(null, null, tcb);
	}
	return Yia;
}
function $rw_getCurrentTarget() {
	return Tga;
}
function $rw_setCurrentTarget(ZNb) {
	Tga = ZNb;
	if (typeof(ZNb) == "undefined") {
		Tga = null;
	} else {
		Tga = ZNb;
	}
}
function $rw_speakFirstSentence(fNb) {
	var cub = null;
	if (fNb && fNb.nodeType) {
		cub = fNb;
	} else {
		if (aca != null && aca != "") {
			try {
				cub = vha(window, aca);
			} catch (e) {}

		}
		if (cub == null) {
			cub = document.body;
		}
	}
	if (!(cub.nodeType == 3 && mya(cub.nodeValue))) {
		cub = SSDOM.getNextTextNodeNoBlank(cub, false, null);
	}
	$rw_speakCurrentSentence(cub, 0);
}
function vha(wha, kxb) {
	var gCc = wha.document.getElementById(kxb);
	if (gCc != null) {
		return gCc;
	}
	var UBc = wha.length;
	for (var i = 0; i < UBc; i++) {
		var jeb = wha[i];
		gCc = vha(jeb, kxb);
		if (gCc != null) {
			return gCc;
		}
	}
	return null;
}
function $rw_speakNextSentence() {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	if (Tga == null) {
		ega = 0;
		$rw_speakCurrentSentence();
		return;
	}
	var Via = Tga.getCaretRange();
	var Ira = zqa(Via.zgb.node);
	if (Ira.length > 0) {
		var Pia = null;
		for (var i = 0; i < Ira.length; i++) {
			var cmd = Ira[i];
			if (cmd.command == cmd.CMD_JUMP) {
				if (document.getElementById(cmd.nEb) != null) {
					Pia = cmd.nEb;
				}
			}
		}
		if (Pia != null) {
			$rw_stopSpeech();
			ega = 0;
			$rw_speakFromId(Pia);
			return;
		}
	}
	var PJb = SSDOM.getNextSentence(Via);
	if (PJb == null) {
		return;
	}
	if (eNb(Via.ygb.node, PJb.ygb.node)) {
		return;
	}
	var tcb = TQb(PJb);
	var Yia = new THHoverTarget(null, null, tcb);
	$rw_stopSpeech();
	Tga = Yia;
	rw_speakHoverTarget(Yia);
}
function $rw_speakNextSentenceHighlightOnly() {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	if (Tga == null) {
		ega = 0;
		$rw_speakCurrentSentenceHighlightOnly();
		return;
	}
	var Via = Tga.getCaretRange();
	var PJb = SSDOM.getNextSentence(Via);
	if (PJb == null) {
		return;
	}
	var tcb = TQb(PJb);
	var Yia = new THHoverTarget(null, null, tcb);
	$rw_stopSpeech();
	Tga = Yia;
	rw_speechHighlightOnly(Yia);
}
function $rw_speakPreviousSentence() {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	if (Tga == null) {
		ega = 0;
		$rw_speakCurrentSentence();
		return;
	}
	var Via = Tga.getCaretRange();
	var Ira = zqa(Via.zgb.node);
	if (Ira.length > 0) {
		var Pia = null;
		for (var i = 0; i < Ira.length; i++) {
			var cmd = Ira[i];
			if (cmd.command == cmd.CMD_JUMPBACK) {
				if (document.getElementById(cmd.nEb) != null) {
					Pia = cmd.nEb;
				}
			}
		}
		if (Pia != null) {
			$rw_stopSpeech();
			ega = 0;
			$rw_speakFromId(Pia);
			return;
		}
	}
	var Wia = SSDOM.getPreviousSentence(Via);
	if (Wia == null) {
		return;
	}
	if (eNb(Wia.zgb.node, Via.zgb.node)) {
		return;
	}
	var tcb = TQb(Wia);
	var Yia = new THHoverTarget(null, null, tcb);
	$rw_stopSpeech();
	Tga = Yia;
	rw_speakHoverTarget(Yia);
}
function $rw_speakPreviousSentenceHighlightOnly() {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	if (Tga == null) {
		ega = 0;
		$rw_speakCurrentSentenceHighlightOnly();
		return;
	}
	var Via = Tga.getCaretRange();
	var Wia = SSDOM.getPreviousSentence(Via);
	if (Wia == null) {
		return;
	}
	var tcb = TQb(Wia);
	var Yia = new THHoverTarget(null, null, tcb);
	$rw_stopSpeech();
	Tga = Yia;
	rw_speechHighlightOnly(Yia);
}
function $rw_getTHCaretRangeFromSelection() {
	var wSb = uSb();
	if (wSb != null && wSb.range instanceof THRange) {
		return rQb(wSb.range);
	}
	return null;
}
function $rw_isTextSelectedForPlay() {
	if (kea) {
		try {
			if (g_speakableTextAreaTarget != null) {
				if ($rw_isPaused()) {
					return true;
				}
				if (g_nSpeakableTextAreaTimerId != 0) {
					return false;
				}
				return true;
			} else {
				if ($rw_isPaused()) {
					return true;
				}
				var jta = uSb();
				if (jta != null && jta.range != null) {
					var tcb = jta.range;
					if (tcb instanceof String) {
						return true;
					} else {
						var target = new THHoverTarget(null, null, tcb);
						var bub = target.getTextPreparedForSpeech();
						if (bub != null && bub.length > 0) {
							return true;
						}
					}
				}
			}
		} catch (err) {
			thLogE(err);
		}
	}
	return false;
}
function $rw_getNumberOfHighlights() {
	if (typeof(ocb) != "undefined") {
		return ocb.length;
	} else {
		return 0;
	}
}
function $rw_getHighlightText(index) {
	if (typeof(ocb) != "undefined" && index > -1 && index < ocb.length) {
		if (Ufa) {
			return ocb[index].text;
		} else {
			return ocb[index].toString();
		}
	}
	return "";
}
function $rw_getHighlightColor(index) {
	if (typeof(pcb) != "undefined" && index > -1 && index < pcb.length) {
		return pcb[index];
	}
	return "";
}
function $rw_getHighlightColour(index) {
	return $rw_getHighlightColor(index);
}
function $rw_isPageLoaded() {
	return (aga.controlData.bOnloadFinished && kea);
}
function $rw_highlightOnlyWTSFailed() {}

function $rw_log(LPb) {
	eya(LPb);
}
function $rw_setReadingAge(p_nAge) {
	if (typeof(p_nAge) == "string") {
		try {
			p_nAge = parseInt(p_nAge, 10);
		} catch (e) {
			thLogE(e);
			return;
		}
	}
	if (typeof(p_nAge) == "number") {
		switch (p_nAge) {
		case 1:
		case 2:
		case 3:
		case 4:
			$rw_setSpeedValue(READING_AGE_4);
			break;
		case 5:
			$rw_setSpeedValue(READING_AGE_5);
			break;
		case 6:
			$rw_setSpeedValue(READING_AGE_6);
			break;
		case 7:
			$rw_setSpeedValue(READING_AGE_7);
			break;
		case 8:
			$rw_setSpeedValue(READING_AGE_8);
			break;
		case 9:
			$rw_setSpeedValue(nba);
			break;
		case 10:
			$rw_setSpeedValue(READING_AGE_10);
			break;
		case 11:
			$rw_setSpeedValue(READING_AGE_11);
			break;
		case 12:
			$rw_setSpeedValue(READING_AGE_12);
			break;
		case 13:
			$rw_setSpeedValue(READING_AGE_13);
			break;
		case 14:
			$rw_setSpeedValue(READING_AGE_14);
			break;
		case 15:
			$rw_setSpeedValue(READING_AGE_15);
			break;
		case 16:
			$rw_setSpeedValue(READING_AGE_16);
			break;
		default:
			$rw_setSpeedValue(READING_AGE_10);
		}
	}
}
function $rw_getVoice() {
	return eba_voice;
}
function $rw_getSpeed() {
	return eba_speed_value;
}
function $rw_setCustomerId(p_strVal, p_bRefreshImmediately) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (yca != Mla) {
				yca = Mla;
				eba_cust_id = yca;
				var flash = xbb.getConnector();
				if (flash != null) {
					flash.setCustomerId(yca);
				}
				var gCc = document.getElementById("editPageMsg");
				if (gCc != null) {
					gCc.innerHTML = "";
				}
				if (typeof(p_bRefreshImmediately) != "boolean" || p_bRefreshImmediately) {
					if (pfa) {
						hXb.deleteAll();
						kYb();
					}
					nia();
				}
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setBookId(p_strVal, p_bRefreshImmediately) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (zca != Mla) {
				zca = Mla;
				eba_book_id = zca;
				var flash = xbb.getConnector();
				if (flash != null) {
					flash.setBookId(zca);
				}
				var gCc = document.getElementById("editPageMsg");
				if (gCc != null) {
					gCc.innerHTML = "";
				}
				if (typeof(p_bRefreshImmediately) != "boolean" || p_bRefreshImmediately) {
					if (pfa) {
						hXb.deleteAll();
						kYb();
					}
					nia();
				}
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setPageId(p_strVal, p_bRefreshImmediately) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (Ada != Mla) {
				Ada = Mla;
				eba_page_id = Ada;
				var flash = xbb.getConnector();
				if (flash != null) {
					flash.setPageId(Ada);
				}
				var gCc = document.getElementById("editPageMsg");
				if (gCc != null) {
					gCc.innerHTML = "";
				}
				if (typeof(p_bRefreshImmediately) != "boolean" || p_bRefreshImmediately) {
					if (pfa) {
						hXb.deleteAll();
						kYb();
					}
					nia();
				}
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setTinymceId(p_strVal) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (Ega != Mla) {
				Ega = Mla;
				eba_tinymce_id = Ega;
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setSymbolText(exb) {
	if (exb != null && typeof(exb) == "string" && exb.length > 0) {
		qfa = true;
		rfa = new Array();
		sfa = new Object();
		MVb(tfa);
		MVb(exb);
	}
}
function $rw_refreshPageData() {
	if (pfa) {
		hXb.deleteAll();
		kYb();
	}
	nia();
}
function nia() {
	if (Zda) {
		if (bda != "*" && typeof(Dib) != "undefined") {
			var zfb = Zda;
			Zda = false;
			Fib = null;
			Gib = null;
			eeb(true);
			Zda = zfb;
		}
		if (ada != "*" && typeof(Imb) != "undefined") {
			Vlb();
		}
		if (bda != "*" && typeof(Dib) != "undefined") {
			Dib();
		} else {
			if (ada != "*" && typeof(Imb) != "undefined") {
				Imb();
			}
		}
	}
}
function pia() {
	Xea = via();
	Uea = 0;
	Vea = 1;
	if (bea) {
		Sda = 1;
		$rw_setSpeedValue(SLOW_SPEED);
	}
	Tea = SSDOM.getFirstSentence(document.body);
	Uea = ria(Tea);
	Wea = Xea.length;
	Yea = -1;
	Fja(true);
}
function ria(sia) {
	var QKb = sia;
	var i = 0;
	while (QKb != null) {
		i++;
		QKb = SSDOM.getNextSentence(QKb);
	}
	i += Xea.length;
	if (bea) {
		return i * 3;
	} else {
		return i;
	}
}
function via() {
	var tia = new Array();
	if (Gea) {
		return tia;
	}
	var uia = document.getElementsByTagName("img");
	var UBc = uia.length;
	var i;
	for (i = 0; i < UBc; i++) {
		var xia = uia[i];
		if (xia.style.display == "none" && Sga) {
			continue;
		}
		if (SSDOM.isIgnored(xia)) {
			continue;
		}
		if (xia.getAttribute("msg") != null) {
			continue;
		}
		var VRb = xia.getAttribute("title");
		if (VRb != null && VRb.length > 0) {
			if (VRb.trimTH().length > 0) {
				tia.push(xia);
			}
		} else {
			VRb = xia.getAttribute("alt");
			if (VRb != null && VRb.length > 0) {
				if (VRb.trimTH().length > 0) {
					tia.push(xia);
				}
			}
		}
	}
	return tia;
}
function zia(Bub) {
	if (Bub == null) {
		return "";
	}
	var Jpb = Bub.getAttribute("title");
	if (Jpb != null && Jpb.length > 0) {
		return Jpb;
	} else {
		var Kpb = Bub.getAttribute("alt");
		if (Kpb != null && Kpb.length > 0) {
			return Kpb;
		} else {
			var Lpb = Bub.getAttribute("msg");
			if (Lpb != null && Lpb.length > 0) {
				return Lpb;
			}
		}
	}
	return "";
}
function Fja(Gja) {
	var oCc = "";
	var Hja = "";
	var Ija = null;
	if (Tea == null) {
		var Xxb = false;
		if (Yea < Wea) {
			if (Yea > -1 && Yea < Xea.length) {
				var Kja = zia(Xea[Yea]);
				if (Kja.trimTH().length > 0) {
					if (Fda) {
						Ija = ZJb(Xea[Yea]);
					}
					var MOb = new SpeechStream.SpeechRequest();
					MOb.setString(Kja, SpeechStream.SpeechRequestBookmarks.OUTER);
					Hja = MOb.getText();
					oCc = MOb.getFinalText();
					Xxb = true;
				}
			}
		}
		if (!Xxb) {
			if (Gja) {
				$rw_autogenSpeechFilesCallback("Success");
			} else {
				$rw_checkAutogenCachedFilesCallback("Success");
			}
			return;
		}
	} else {
		var pPb = BOb(Tea, new Array());
		Ija = pPb.voice;
		if (pPb.YCc != null) {
			Tea = pPb.YCc;
			Uea++;
		}
		oCc = pPb.bub;
		Hja = pPb.LNb;
	}
	var flash = xbb.getConnector();
	if (flash != null) {
		var wPb = IQb();
		var mua;
		if (SpeechStream.pronunciation.mode == SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER) {
			mua = IRb(oCc);
		} else {
			mua = IRb(Hja);
		}
		if (Lga) {
			wPb = qua(wPb);
			mua = tua(mua);
		}
		if (Pda) {
			var CQb = JQb(mua);
			wPb = wPb + "/" + CQb;
		}
		if (Gja) {
			var pua = Dza(true) + SpeechStream.cacheMode.getLiveServer() + "/";
			flash.autogenSpeechFiles(oCc, wPb, mua, !ofa, pua);
		} else {
			var hob = wPb + "/" + mua;
			flash.checkAutogenCachedFiles(hob);
		}
	}
}
function $rw_autogenSpeechFilesCallback(Xja) {
	if (Xja == 'Success') {
		var Yja = false;
		if (bea) {
			if (Sda == 1 || Sda == 2) {
				Sda++;
			} else {
				Sda = 1;
				Yja = true;
			}
			switch (Sda) {
			case 1:
				$rw_setSpeedValue(SLOW_SPEED);
				break;
			case 2:
				$rw_setSpeedValue(MEDIUM_SPEED);
				break;
			case 3:
				$rw_setSpeedValue(FAST_SPEED);
				break;
			}
		} else {
			Yja = true;
		}
		if (Yja) {
			Tea = SSDOM.getNextSentence(Tea, null);
			if (Tea != null) {
				Vea++;
				if (Uea >= Vea) {
					$rwj('#pb1').progressBar((Vea / Uea) * 100);
					if (Cga) {
						var target = new THHoverTarget(null, null, TQb(Tea));
						Mra(target);
					}
					Fja(true);
				} else {
					$rwj.unblockUI();
					hea = "Error: More sentences to be cached than counted in initial count at the start of this process!";
					if (!fea) {
						alert(hea);
					}
					if (typeof(eea) == "string") {
						Fxa(eea);
					}
				}
			} else {
				if (Wea > 0 && Yea < (Wea - 1)) {
					++Vea;
					$rwj('#pb1').progressBar((Vea / Uea) * 100);
					++Yea;
					Fja(true);
				} else {
					$rwj.unblockUI();
					var pua = Dza(true) + SpeechStream.cacheMode.getLiveServer() + "/";
					var flash = xbb.getConnector();
					flash.autoGenComplete(pua);
					if (eea == null && !fea) {
						alert("Page Cached Successfully!");
					}
				}
			}
		} else {
			Vea++;
			Fja(true);
		}
	} else {
		$rwj.unblockUI();
		hea = Xja;
		if (!fea) {
			alert(Xja);
		}
		if (typeof(eea) == "string") {
			Fxa(eea);
		}
	}
}
function Wja() {
	Xea = via();
	Uea = 0;
	Vea = 1;
	Zea = 0;
	aea = 0;
	if (bea) {
		Sda = 1;
		$rw_setSpeedValue(SLOW_SPEED);
	}
	Tea = SSDOM.getFirstSentence(document.body);
	Uea = ria(Tea);
	Wea = Xea.length;
	Yea = -1;
	Fja(false);
}
function $rw_checkAutogenCachedFilesCallback(Xja) {
	if (Xja == 'Success') {
		++Zea;
	} else {
		++aea;
	}
	var Yja = false;
	if (bea) {
		if (Sda == 1 || Sda == 2) {
			Sda++;
		} else {
			Sda = 1;
			Yja = true;
		}
		switch (Sda) {
		case 1:
			$rw_setSpeedValue(SLOW_SPEED);
			break;
		case 2:
			$rw_setSpeedValue(MEDIUM_SPEED);
			break;
		case 3:
			$rw_setSpeedValue(FAST_SPEED);
			break;
		}
	} else {
		Yja = true;
	}
	if (Yja) {
		Tea = SSDOM.getNextSentence(Tea, null);
		if (Tea != null) {
			Vea++;
			if (Uea >= Vea) {
				$rwj('#pb1').progressBar((Vea / Uea) * 100);
				Fja(false);
			} else {
				$rwj.unblockUI();
				hea = "Error: More sentences to be cached than counted in initial count at the start of this process!";
				if (!fea) {
					alert(hea);
				}
				if (typeof(eea) == "string") {
					Fxa(eea);
				}
			}
		} else {
			if (Wea > 0 && Yea < (Wea - 1)) {
				++Vea;
				$rwj('#pb1').progressBar((Vea / Uea) * 100);
				++Yea;
				Fja(false);
			} else {
				$rwj.unblockUI();
				if (aea > 0) {
					hea = "Missing files!  Checked page and found that " + aea + " sentences out of " + Vea + " where not cached.";
					gea = aea;
					if (!fea) {
						alert(hea);
					}
				}
				if (typeof(eea) == "string") {
					Fxa(eea);
				} else {
					if (aea == 0 && !fea) {
						alert("Checked page and found that all " + Vea + " sentences were cached.");
					}
				}
			}
		}
	} else {
		Vea++;
		Fja(false);
	}
}
function $rw_autogenCompleteCallback(Xja) {
	if (Xja != "Success") {
		hea = Xja;
		if (!fea) {
			alert("Finished autogeneration process.  " + Xja);
		}
	}
	if (typeof(eea) == "string") {
		Fxa(eea);
	}
}
function $rw_setTranslateSource(Qbb) {
	SpeechStream.translatorData.setSource(Qbb);
}
function $rw_setTranslateTarget(p_strTarget) {
	SpeechStream.translatorData.setTarget(p_strTarget);
}
var Zja = null;
var aja = null;
var bja = 0;
function $rw_getTouchSelection() {
	var ueb;
	var dja;
	if (window.getSelection().rangeCount > 0 && !window.getSelection().isCollapsed) {
		ueb = window.getSelection().getRangeAt(0).cloneRange();
		dja = window;
	} else {
		ueb = null;
		dja = null;
		var jeb = SSDOM.getFrameSelection(window);
		if (jeb) {
			var OCc = jeb.getSelection();
			if (OCc.rangeCount > 0 && !OCc.isCollapsed) {
				ueb = OCc.getRangeAt(0).cloneRange();
				dja = jeb;
			}
		}
	}
	if (cfa && hfa >= 6 && efa && ueb == null || dja == null) {
		++bja;
		if (bja >= 10) {
			Zja = ueb;
			aja = dja;
			bja = 0;
		}
	} else {
		Zja = ueb;
		aja = dja;
		bja = 0;
	}
}
function $rw_setBreakList(JCc) {
	var gja = aga.pageData.gja;
	var hja = aga.pageData.hja;
	var ija = JCc.split("~");
	var i;
	for (i = 0; i < ija.length; i++) {
		var mwb = ija[i];
		if (mwb.length > 0) {
			if (mwb.charAt(0) == "!") {
				mwb = mwb.substring(1);
				if (aga.pageData.isInBreakList(mwb)) {
					var itb = gja.indexOf("~" + mwb + "~");
					var jtb = itb + 1 + mwb.length;
					gja = gja.substring(0, itb) + gja.substring(jtb);
				}
				if (!aga.pageData.isInStyleList(mwb)) {
					hja += mwb + "~";
				}
			} else {
				if (aga.pageData.isInBreakList(mwb)) {
					gja += mwb + "~";
				}
				if (aga.pageData.isInStyleList(mwb)) {
					var itb = hja.indexOf("~" + mwb + "~");
					var jtb = itb + 1 + mwb.length;
					hja = hja.substring(0, itb) + hja.substring(jtb);
				}
			}
		}
	}
	aga.pageData.gja = gja;
	aga.pageData.hja = hja;
}
function $rw_parseNewSection(Bub) {
	try {
		if (Bub != null) {
			$rw_tagSentencesForDynamicSection(Bub);
			if (ada != "*" && typeof(Imb) != "undefined") {
				if (Dkb != -1) {
					wkb(Dkb);
					var nmb = function () {
						$rw_parseNewSection(Bub);
					};
					setTimeout(nmb, 100);
					return;
				}
				Vlb();
			}
			if (typeof($rw_refreshHighlights) == "function" && Zda) {
				if (hgb == 1) {
					hgb = 2;
				}
				$rw_refreshHighlights();
			} else {
				if (typeof(Imb) == "function" && Zda) {
					Imb();
				}
			}
			if (Dga && cfa) {
				var YAb = null;
				if (typeof(eba_tinymce_id) == "string") {
					YAb = document.getElementById(Ega);
				}
				if (YAb) {
					try {
						var ipb = document.body;
						var mlb = ipb.firstChild;
						while (mlb != null) {
							if (lAb(mlb, Ega)) {
								mlb = SSDOM.getNextNodeIgnoreChildren(mlb, false, ipb);
							} else {
								if (mlb.nodeType == 1) {
									YEb(mlb, "touchstart", SpeechStream.tinymceipadfocusbugworkaround);
								}
								mlb = SSDOM.getNextNode(mlb, false, ipb);
							}
						}
					} catch (err) {
						thLogE(err.message);
					}
					var Orb = document.getElementById(SpeechStream.tinymceipadfix);
					if (Orb == null) {
						var ta = document.createElement("input");
						ta.setAttribute("type", "text");
						ta.setAttribute("style", "opacity:0;color:#ffffff;padding:0px;margin:0px;" + "border-top-color:#FFFFFF;border-right-color:#FFFFFF;border-bottom-color:#FFFFFF;border-left-color:#FFFFFF;" + "border-top-style:none;border-right-style:none;border-bottom-style:none;border-left-style:none;" + "height:1px;width:1px");
						ta.setAttribute("id", SpeechStream.tinymceipadfix);
						var Orb = document.getElementById("rwDrag");
						if (Orb != null) {
							Orb.appendChild(ta);
						} else {
							YAb.parentNode.insertBefore(ta, YAb);
						}
						qAb(false);
					}
				}
			}
		}
	} catch (err) {
		eya(err.message);
	}
}
var QDb = "[\\x21\\x2E\\x3F\\x3A]";
var RDb = /[\n\r\t ]{2,}/g;
function $rw_tagSentencesForDynamicSection(Bub) {
	if (Bub == null) {
		return;
	}
	try {
		var SDb = false;
		var jOb = SSDOM.getNextNodeAllowMoveToChild(Bub, false, Bub);
		while (jOb != null) {
			if (jOb.nodeType == 3) {
				var qRb = jOb.parentNode.tagName.toLowerCase();
				if (qRb == "textarea") {
					jOb = SSDOM.getNextNode(jOb, false, Bub);
					continue;
				}
				var bub = jOb.nodeValue;
				var XDb = bub.trimSpaceTH();
				var AEb = XDb.length > 0;
				if (pda && qRb == "a") {
					AEb = false;
				}
				if (!AEb) {
					if (Zda || Nda && (Bea || Yda || Lea)) {
						if (SDb) {
							if (!pda) {
								jOb.nodeValue = " ";
							}
							SDb = false;
							jOb = SSDOM.getNextNode(jOb, false, Bub);
						} else {
							var mBc = jOb;
							jOb = SSDOM.getNextNode(jOb, false, Bub);
							if (!pda) {
								mBc.parentNode.removeChild(mBc);
							}
						}
					} else {
						jOb = SSDOM.getNextNode(jOb, false, Bub);
					}
				} else {
					if (!pda) {
						if (Zda || Nda && (Bea || Yda || Lea)) {
							if (XDb.length < bub.length) {
								var Cdb = false;
								XDb = bub.trimSpaceStartTH();
								if ((bub.length - XDb.length) > 0) {
									if (SDb) {
										bub = " " + XDb;
									} else {
										bub = XDb;
									}
									Cdb = true;
								}
								XDb = bub.trimSpaceEndTH();
								if ((bub.length - XDb.length) > 1) {
									bub = XDb + " ";
									SDb = false;
									Cdb = true;
								}
								if (Cdb) {
									jOb.nodeValue = bub;
								}
							}
							XDb = bub.replace(RDb, " ");
							if (XDb.length < bub.length) {
								bub = XDb;
								Cdb = true;
							}
						}
					}
					var FBc;
					FBc = bub.search(QDb);
					var cDb = (jOb.parentNode.getAttribute("texthelpSkip") != null);
					var dDb = jOb;
					if (FBc > -1 && FBc < (bub.length - 1)) {
						var eDb = true;
						while (true) {
							var ghb = JEb(bub, FBc, jOb);
							if (ghb) {
								break;
							} else {
								var gDb = bub.substring(FBc + 1);
								var fpb = gDb.search(QDb);
								if (fpb > -1) {
									FBc = FBc + 1 + fpb;
								} else {
									eDb = false;
									break;
								}
							}
						}
						if (eDb) {
							var iDb = bub.substring(0, FBc + 1);
							var jDb = bub.substring(FBc + 1);
							var span = document.createElement("span");
							span.setAttribute(eaa, "1");
							var sDb = document.createTextNode(iDb);
							var mDb = document.createTextNode(jDb);
							var tDb = jOb.parentNode;
							tDb.insertBefore(mDb, jOb);
							tDb.insertBefore(span, mDb);
							span.appendChild(sDb);
							tDb.removeChild(jOb);
							jOb = mDb;
							dDb = sDb;
						} else {
							if (jOb.previousSibling != null || jOb.nextSibling != null || cDb) {
								var span = document.createElement("span");
								span.setAttribute(eaa, "1");
								var sDb = document.createTextNode(bub);
								var tDb = jOb.parentNode;
								tDb.insertBefore(span, jOb);
								span.appendChild(sDb);
								tDb.removeChild(jOb);
								jOb = sDb;
							}
							dDb = jOb;
							jOb = SSDOM.getNextNode(jOb, false, null);
						}
					} else {
						if (jOb.previousSibling != null || jOb.nextSibling != null || cDb) {
							var span = document.createElement("span");
							span.setAttribute(eaa, "1");
							var sDb = document.createTextNode(bub);
							var tDb = jOb.parentNode;
							tDb.insertBefore(span, jOb);
							span.appendChild(sDb);
							tDb.removeChild(jOb);
							jOb = sDb;
						}
						dDb = jOb;
						jOb = SSDOM.getNextNode(jOb, false, null);
					}
					if (Zda || Nda && (Bea || Yda || Lea)) {
						var uDb = dDb.nodeValue;
						var vDb = dDb.nodeValue.length;
						if (vDb > 0 && uDb.charCodeAt(vDb - 1) == 32) {
							SDb = false;
						} else {
							SDb = true;
						}
					}
				}
			} else if (jOb.nodeType == 1) {
				if (Zda) {
					if (!SSDOM.isStyleNode(jOb)) {
						if (SSDOM.isLineBreakNode(jOb)) {
							SDb = false;
						}
					} else if (jOb.tagName.toLowerCase() == "img") {
						SDb = true;
					}
				}
				if (Bea) {
					if (jOb.tagName.toLowerCase() == "img") {
						var VRb = jOb.getAttribute("title");
						if (VRb != null && VRb.length > 0) {
							jOb.setAttribute("msg", VRb);
						}
					}
				} else if (Gea) {
					if (jOb.tagName.toLowerCase() == "img") {
						var VRb = jOb.getAttribute("msg");
						if (!(VRb != null && VRb.length > 0)) {
							VRb = jOb.getAttribute("title");
							if (VRb != null && VRb.length > 0) {
								jOb.setAttribute("msg", VRb);
							} else {
								VRb = jOb.getAttribute("alt");
								if (VRb != null && VRb.length > 0) {
									jOb.setAttribute("msg", VRb);
								}
							}
						}
					}
				}
				var EEb = jOb.getAttribute(caa);
				var FEb = jOb.getAttribute(baa);
				if (jOb.tagName.toLowerCase() == "pre" || (EEb != null && EEb.length > 0) || (FEb != null && FEb.length > 0)) {
					jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, Bub);
				} else {
					jOb = SSDOM.getNextNode(jOb, false, Bub);
				}
			} else {
				jOb = SSDOM.getNextNode(jOb, false, Bub);
			}
		}
		if (Zda) {
			jOb = SSDOM.getNextNodeAllowMoveToChild(Bub, false, Bub);
			while (jOb != null) {
				if (jOb.nodeType == 3) {
					var AEb = jOb.nodeValue.trimTH().length > 0;
					if (AEb) {
						var BEb = jOb.parentNode;
						var DEb = BEb.getAttribute("rwthgen");
						while ((DEb != null && DEb.length > 0)) {
							BEb = BEb.parentNode;
							DEb = BEb.getAttribute("rwthgen");
						}
						var CEb = BEb.getAttribute("id");
						if (CEb != null && CEb.substr(0, 14) == "rwTHnoteMarker") {
							var rlb = CEb;
							BEb.id = "";
							CEb = null;
							if (BEb.nextSibling == null && BEb.previousSibling == null && (BEb.parentNode.id == null || BEb.parentNode.id == "")) {
								BEb.parentNode.id = rlb;
							} else {
								var vBb = BEb.ownerDocument.createElement("span");
								BEb.id = rlb;
								while (BEb.firstChild != null) {
									vBb.appendChild(BEb.firstChild);
								}
								BEb.appendChild(vBb);
								BEb = vBb;
							}
						}
						if (CEb == null || CEb.length == 0) {
							var MAc = tka(jOb);
							if (MAc != null) {
								BEb.id = MAc;
							}
						}
					}
					jOb = SSDOM.getNextNode(jOb, false, Bub);
				} else if (jOb.nodeType == 1) {
					if (gkb(jOb)) {
						if (jOb.tagName.toLocaleLowerCase() == "img") {
							var DEb = jOb.getAttribute("id");
							if (DEb != null && DEb.substr(0, 14) == "rwTHnoteMarker") {
								var rlb = DEb;
								jOb.id = "";
								DEb = null;
								if (jOb.nextSibling == null && jOb.previousSibling == null && (jOb.parentNode.id == null || jOb.parentNode.id == "")) {
									jOb.parentNode.id = rlb;
								} else {
									var vBb = jOb.ownerDocument.createElement("span");
									vBb.id = rlb;
									jOb.parentNode.insertBefore(vBb, jOb);
									vBb.appendChild(jOb);
								}
							}
							if (DEb == null || DEb.length == 0) {
								var MAc = xka(jOb);
								if (MAc != null) {
									jOb.id = MAc;
								}
							}
						}
					}
					var EEb = jOb.getAttribute(caa);
					var FEb = jOb.getAttribute(baa);
					if (jOb.tagName.toLowerCase() == "pre" || (EEb != null && EEb.length > 0) || (FEb != null && FEb.length > 0)) {
						jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, Bub);
					} else {
						jOb = SSDOM.getNextNode(jOb, false, Bub);
					}
				} else {
					jOb = SSDOM.getNextNode(jOb, false, Bub);
				}
			}
		}
	} catch (exception) {
		eya("Error in $rw_tagSentences: " + exception);
	}
	Aea = true;
}
function tka(Bub) {
	if (Bub != null && Bub.nodeType == 3) {
		var Jwb = Bub.ownerDocument;
		var ipb = Jwb.body;
		var bub = Bub.nodeValue;
		if (bub != null) {
			bub = bub.replace(/\s+/g, "");
			var hHb = Bub.parentNode;
			while (hHb != null && hHb.parentNode != null) {
				hHb = hHb.parentNode;
				if (hHb.id != null && hHb.id.length > 0) {
					if (hHb.id.substr(0, 4) != "rwTH") {
						bub = hHb.id + bub;
						break;
					}
				}
				if (hHb == ipb) {
					break;
				}
			}
			var MAc = "rwTH" + PPb(bub);
			if (Jwb.getElementById(MAc) != null) {
				var n = 1;
				while (Jwb.getElementById(MAc + n) != null) {
					++n;
				}
				MAc = MAc + n;
			}
			return MAc;
		}
	}
	return null;
}
function xka(Bub) {
	if (Bub != null && Bub.nodeType == 1 && Bub.tagName.toLocaleLowerCase() == "img") {
		var Jwb = Bub.ownerDocument;
		var ipb = Jwb.body;
		var bub = Bub.src;
		if (bub != null) {
			bub = bub.replace(/\s+/g, "");
			var hHb = Bub;
			while (hHb != null && hHb.parentNode != null) {
				hHb = hHb.parentNode;
				if (hHb.id != null && hHb.id.length > 0) {
					if (hHb.id.substr(0, 4) != "rwTH") {
						bub = hHb.id + bub;
						break;
					}
				}
				if (hHb == ipb) {
					break;
				}
			}
			var MAc = "rwTH" + PPb(bub);
			if (Jwb.getElementById(MAc) != null) {
				var n = 1;
				while (Jwb.getElementById(MAc + n) != null) {
					++n;
				}
				MAc = MAc + n;
			}
			return MAc;
		}
	}
	return null;
}
if (typeof(SpeechStream) == "undefined") {
	SpeechStream = {};
}
SpeechStream.annotateBlock = {};
SpeechStream.annotateBlock.m_bUseAnnotations = true;
SpeechStream.annotateBlock.setUseAnnotations = function (p_bUse) {
	SpeechStream.annotateBlock.m_bUseAnnotations = p_bUse;
};
SpeechStream.annotateBlock.getUseAnnotations = function () {
	return SpeechStream.annotateBlock.m_bUseAnnotations;
};
function $rw_isCustId(FWb) {
	if (FWb == 1600) {
		return Yda;
	} else {
		return false;
	}
}
function $rw_disableSpeech() {
	if (!Kga) {
		try {
			var nEb = SpeechStream.EnumIconParameter;
			for (var i = 0; i < nea; i++) {
				var Hpb = g_icons[i][nEb.ICON_NAME];
				if (lEb.indexOf(Hpb) > -1) {
					zza(g_icons[i][nEb.ICON_NAME], "mask", g_icons[i][nEb.ICON_OFFSET], false);
				}
			}
		} catch (err) {
			thLogE(err);
		}
	}
	Kga = true;
	$rw_divPress(Rba);
	$rw_divPress(Pba);
	$rw_divPress(Zba);
}
function $rw_enableSpeech() {
	if (Kga) {
		try {
			var nEb = SpeechStream.EnumIconParameter;
			for (var i = 0; i < nea; i++) {
				var Hpb = g_icons[i][nEb.ICON_NAME];
				if (lEb.indexOf(Hpb) > -1) {
					zza(g_icons[i][nEb.ICON_NAME], "flat", g_icons[i][nEb.ICON_OFFSET], false);
				}
			}
		} catch (err) {
			thLogE(err);
		}
	}
	Kga = false;
	$rw_divPress(Rba);
	$rw_divPress(Pba);
	$rw_divPress(Zba);
}
function $rw_setApipFolder(rua) {
	Mga = rua;
}
function $rw_setApipFile(uua) {
	Nga = uua;
}
function $rw_setPageId(p_strVal, p_bRefreshImmediately) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (Ada != Mla) {
				Ada = Mla;
				eba_page_id = Ada;
				var flash = xbb.getConnector();
				if (flash != null) {
					flash.setPageId(Ada);
				}
				var gCc = document.getElementById("editPageMsg");
				if (gCc != null) {
					gCc.innerHTML = "";
				}
				if (typeof(p_bRefreshImmediately) != "boolean" || p_bRefreshImmediately) {
					if (pfa) {
						hXb.deleteAll();
						kYb();
					}
					nia();
				}
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setStudentId(p_strVal, p_bRefreshImmediately) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (bda != Mla) {
				bda = Mla;
				cda = Mla;
				if (typeof(p_bRefreshImmediately) != "boolean" || p_bRefreshImmediately) {
					nia();
				}
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setTeacherId(p_strVal, p_bRefreshImmediately) {
	try {
		if (typeof(p_strVal) == "string") {
			var Mla = xVb(p_strVal.trimTH());
			if (ada != Mla) {
				ada = Mla;
				dda = Mla;
				if (typeof(p_bRefreshImmediately) != "boolean" || p_bRefreshImmediately) {
					nia();
				}
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
}
function $rw_setNoteEditorId(Brb) {
	ada = xVb(Brb);
}
function $rw_setHighlightEditorId(Brb) {
	bda = xVb(Brb);
}
function $rw_setNoteReaderId(Brb) {
	cda = xVb(Brb);
}
function $rw_setHighlightReaderId(Brb) {
	dda = xVb(Brb);
}
function $rw_setPersistHighlights(p_bVal) {
	fda = p_bVal;
}
function $rw_setPersistNotes(p_bVal) {
	eda = p_bVal;
}
if (typeof(SpeechStream) == "undefined") {
	SpeechStream = {};
}
SpeechStream.Dom = function (p_dat) {
	var self = this;
	this.bIE = false;
	this.bIEOld = false;
	if (typeof(p_dat) != "undefined") {
		self.bIE = p_dat.browser.bIE;
		self.bIEOld = p_dat.browser.bIEOld;
	} else {
		try {
			window.console.log("Data not available when setting up the SpeechStream.Dom");
		} catch (err) {}

	}
};
SpeechStream.dom = new SpeechStream.Dom(aga);
var SSDOM = SpeechStream.dom;
SSDOM.toString = function () {
	return "Singleton instance to control DOM requests.";
};
SSDOM.alert = function () {
	alert("testing SSDOM " + SSDOM.toString());
};
SSDOM.getPreviousNode = function (Bub, p_bGoByStyle, mTb) {
	if (Bub == null || Bub == mTb) {
		return null;
	}
	var hUb = Bub;
	if (hUb.previousSibling != null) {
		hUb = hUb.previousSibling;
		if (p_bGoByStyle) {
			if (!SSDOM.isStyleNode(hUb)) {
				return null;
			}
		}
		if (hUb != null && SSDOM.isInvalidNode(hUb)) {
			if (mTb == hUb) {
				return null;
			}
			hUb = SSDOM.getPreviousNode(hUb, p_bGoByStyle, mTb);
		} else {
			while (hUb != null && hUb.lastChild != null) {
				if (hUb.nodeType == 1 && hUb.tagName.toLowerCase() == "math") {
					break;
				}
				if (hUb.nodeType == 1 && hUb.isMathJax) {
					break;
				}
				hUb = hUb.lastChild;
				if (p_bGoByStyle) {
					if (!SSDOM.isStyleNode(hUb)) {
						hUb = null;
					}
				}
				if (hUb != null && SSDOM.isInvalidNode(hUb)) {
					if (mTb == hUb) {
						return null;
					}
					hUb = SSDOM.getPreviousNode(hUb, p_bGoByStyle, mTb);
					break;
				}
			}
		}
	} else {
		hUb = hUb.parentNode;
		if (p_bGoByStyle && hUb != null) {
			if (!SSDOM.isStyleNode(hUb)) {
				hUb = null;
			}
		}
	}
	return hUb;
};
SSDOM.getNextNodeImpl = function (Bub, p_bGoByStyle, mTb, p_bDeferEndNodeCheck, p_bAlwaysIgnoreChild) {
	if (Bub == null || (Bub == mTb && !p_bDeferEndNodeCheck)) {
		return null;
	}
	var Vla = SSDOM.isInvalidNode(Bub);
	if (Bub.nodeType == 1 && Bub.tagName.toLowerCase() == "math") {
		Vla = true;
	}
	if (Bub.nodeType == 1 && Bub.isMathJax) {
		Vla = true;
	}
	var aOb = null;
	if (Bub == mTb && p_bDeferEndNodeCheck) {
		if (!Vla && Bub.firstChild != null) {
			aOb = Bub.firstChild;
		} else {
			return null;
		}
	} else {
		aOb = Bub;
		if (aOb.firstChild != null && !Vla && !p_bAlwaysIgnoreChild) {
			aOb = aOb.firstChild;
		} else if (aOb.nextSibling != null) {
			aOb = aOb.nextSibling;
		} else {
			while (aOb != null && aOb.nextSibling == null) {
				aOb = aOb.parentNode;
				if (p_bGoByStyle) {
					if (!SSDOM.isStyleNode(aOb)) {
						aOb = null;
					}
				}
				if (mTb == aOb) {
					break;
				}
			}
			if (aOb != null && mTb != aOb) {
				aOb = aOb.nextSibling;
			}
		}
	}
	if (aOb != null) {
		if (p_bGoByStyle) {
			if (!SSDOM.isStyleNode(aOb)) {
				aOb = null;
			}
		}
	}
	if (aOb != null && SSDOM.isInvalidNode(aOb)) {
		aOb = SSDOM.getNextNodeImpl(aOb, p_bGoByStyle, mTb, false, p_bAlwaysIgnoreChild);
		if (p_bDeferEndNodeCheck && aOb == mTb && Bub == mTb) {
			aOb = null;
		}
	}
	return aOb;
};
SSDOM.getNextNode = function (Bub, p_bGoByStyle, mTb) {
	return SSDOM.getNextNodeImpl(Bub, p_bGoByStyle, mTb, false, false);
};
SSDOM.getNextNodeAllowMoveToChild = function (Bub, p_bGoByStyle, mTb) {
	return SSDOM.getNextNodeImpl(Bub, p_bGoByStyle, mTb, true, false);
};
SSDOM.getPreviousNodeIgnoreChildren = function (Bub, p_bGoByStyle, mTb) {
	if (Bub == null || Bub == mTb) {
		return null;
	}
	var hUb = Bub;
	if (hUb.previousSibling != null) {
		hUb = hUb.previousSibling;
		if (p_bGoByStyle) {
			if (!SSDOM.isStyleNode(hUb)) {
				hUb = null;
			}
		}
		if (hUb != null && SSDOM.isInvalidNode(hUb)) {
			if (mTb == hUb) {
				hUb = null;
			} else {
				hUb = SSDOM.getPreviousNodeIgnoreChildren(hUb, p_bGoByStyle, mTb);
			}
		}
	} else {
		hUb = hUb.parentNode;
		if (p_bGoByStyle && hUb != null) {
			if (!SSDOM.isStyleNode(hUb)) {
				hUb = null;
			}
		}
	}
	return hUb;
};
SSDOM.getNextNodeIgnoreChildren = function (Bub, p_bGoByStyle, mTb) {
	return SSDOM.getNextNodeImpl(Bub, p_bGoByStyle, mTb, false, true);
};
SSDOM.getFirstChildTextNode = function (Bub, p_bAllowImg) {
	if (Bub == null) {
		return null;
	}
	if (Bub.firstChild == null || SSDOM.isInvalidNode(Bub)) {
		return Bub;
	}
	if (Bub.nodeType == 1 && Bub.tagName.toLowerCase() == "textarea") {
		return Bub;
	}
	if (Bub.nodeType == 1 && Bub.tagName.toLowerCase() == "math") {
		return Bub;
	}
	if (Bub.nodeType == 1 && Bub.isMathJax) {
		return Bub;
	}
	var wvb = Bub.firstChild;
	if (wvb.nodeType == 3) {
		return wvb;
	} else if (wvb.nodeType == 1 && p_bAllowImg && wvb.tagName.toLowerCase() == "img" && wvb.getAttribute("msg") != null && wvb.getAttribute("msg").length > 0) {
		return wvb;
	} else {
		if (p_bAllowImg) {
			return SSDOM.getNextTextNode(wvb, false, Bub);
		} else {
			return SSDOM.getNextTextNodeNoImg(wvb, false, Bub, true);
		}
	}
};
SSDOM.getLastChildTextNode = function (Bub, p_bAllowImg) {
	if (Bub == null) {
		return null;
	}
	if (Bub.lastChild == null || SSDOM.isInvalidNode(Bub)) {
		return Bub;
	}
	if (Bub.nodeType == 1 && Bub.tagName.toLowerCase() == "textarea") {
		return Bub;
	}
	if (Bub.nodeType == 1 && Bub.tagName.toLowerCase() == "math") {
		return Bub;
	}
	if (Bub.nodeType == 1 && Bub.isMathJax) {
		return Bub;
	}
	var wvb = Bub.lastChild;
	while (wvb != null) {
		if (wvb.nodeType == 3) {
			return wvb;
		} else if (wvb.nodeType == 1 && p_bAllowImg && wvb.tagName.toLowerCase() == "img" && wvb.getAttribute("msg") != null && wvb.getAttribute("msg").length > 0) {
			return wvb;
		} else if (SSDOM.isInvalidNode(wvb) || wvb.lastChild == null) {
			var fkb;
			if (p_bAllowImg) {
				fkb = SSDOM.getPreviousTextNode(wvb, false, Bub);
			} else {
				fkb = SSDOM.getPreviousTextNodeNoImg(wvb, false, Bub, true);
			}
			return fkb;
		} else {
			wvb = wvb.lastChild;
		}
	}
	return Bub;
};
SSDOM.getNextNodeIEBugFix = function (ZWb) {
	var Vla = SSDOM.isInvalidNode(ZWb);
	var aOb = ZWb;
	if (aOb.firstChild != null && !Vla) {
		aOb = aOb.firstChild;
	} else if (aOb.nextSibling != null) {
		var jOb = aOb;
		aOb = aOb.nextSibling;
		var wvb = aOb;
		var ipb = wvb.ownerDocument.body;
		while (wvb != null && wvb != ipb) {
			if (wvb == jOb) {
				throw "DOM Error";
			}
			wvb = wvb.parentNode;
		}
	} else {
		while (aOb != null && aOb.nextSibling == null) {
			aOb = aOb.parentNode;
		}
		if (aOb != null) {
			var jOb = aOb;
			aOb = aOb.nextSibling;
			var wvb = aOb;
			var ipb = wvb.ownerDocument.body;
			while (wvb != null && wvb != ipb) {
				if (wvb == jOb) {
					throw "DOM Error";
				}
				wvb = wvb.parentNode;
			}
		}
	}
	if (aOb != null && SSDOM.isInvalidNode(aOb)) {
		aOb = SSDOM.getNextNodeIEBugFix(aOb);
	}
	return aOb;
};
SSDOM.getSentenceBreakToLeft = function (Jkb, mTb) {
	if (typeof(mTb) == "undefined") {
		mTb = null;
	}
	if (Jkb == null || Jkb.node == null) {
		return null;
	}
	var node = Jkb.node;
	var rzb = Jkb.offset;
	if (Jkb.node.nodeType == 1 && Jkb.node.tagName.toLowerCase() == "math") {
		return Jkb;
	}
	if (Jkb.node.nodeType == 1 && Jkb.node.isMathJax) {
		return Jkb;
	}
	if (Jkb.forwardBias) {
		if (node.nodeType == 3 && rzb == node.nodeValue.length) {
			node = SSDOM.getNextTextNode(node, true, mTb);
			rzb = 0;
			if (node == null) {
				node = Jkb.node;
				rzb = Jkb.offset;
			}
		}
	} else {
		if (rzb > 0) {
			--rzb;
		} else {
			node = SSDOM.getPreviousTextNode(node, true, mTb);
			if (node == null) {
				return Jkb;
			}
			if (node.nodeType == 3) {
				rzb = node.nodeValue.length - 1;
			} else {
				rzb = 0;
				if (node.tagName.toLowerCase() == "math") {
					return Jkb;
				}
				if (node.isMathJax) {
					return Jkb;
				}
			}
		}
	}
	if (node.nodeType == 3) {
		var Vkb = node.nodeValue.charAt(rzb);
		if (Vkb == '.' || Vkb == '!' || Vkb == '?' || Vkb == ':') {
			if (rzb > 0) {
				--rzb;
			} else {
				node = SSDOM.getPreviousTextNode(node, true, mTb);
				if (node == null) {
					return Jkb;
				}
				if (node.nodeType == 3) {
					rzb = node.nodeValue.length - 1;
				} else {
					rzb = 0;
					if (node.tagName.toLowerCase() == "math") {
						return Jkb;
					}
					if (node.isMathJax) {
						return Jkb;
					}
				}
			}
		}
	}
	var qla = node;
	var rla = rzb;
	var hUb = node;
	var cOb = rzb;
	var OWb = false;
	var tla = ' ';
	while (!OWb) {
		if (hUb.nodeType == 3) {
			var bub = hUb.nodeValue;
			if (bub.length > 0) {
				if (cOb == -1) {
					cOb = bub.length;
				}
				bub = bub.replace(/[\x21\x3f\x3a]/g, ".");
				var FBc = bub.lastIndexOf(".", cOb);
				while (FBc > -1) {
					if (JEb(bub, FBc, hUb)) {
						if (FBc < bub.length - 1) {
							qla = hUb;
							rla = FBc + 1;
							OWb = true;
							break;
						} else {
							if (!Bya(tla)) {
								OWb = true;
								break;
							}
						}
					}
					if (FBc == 0) {
						FBc = -1;
					} else {
						FBc = bub.lastIndexOf(".", FBc - 1);
					}
				}
				if (OWb) {
					break;
				}
				if (bub.trimTH().length > 0) {
					qla = hUb;
					rla = 0;
				}
				tla = bub.charAt(0);
			}
		} else {
			if (SSDOM.isSpecialCase(hUb) && hUb.getAttribute("ignore") == null) {
				if (hUb.tagName.toLowerCase() == "math") {
					OWb = true;
					break;
				}
				if (hUb.isMathJax) {
					OWb = true;
					break;
				}
				qla = hUb;
				rla = 0;
			}
		}
		hUb = SSDOM.getPreviousNode(hUb, true, mTb);
		cOb = -1;
		if (hUb == null) {
			OWb = true;
			break;
		}
		if (hUb.nodeType == 3 && SSDOM.checkForSpecialParent(hUb) != null) {
			hUb = SSDOM.checkForSpecialParent(hUb);
			if (hUb == null) {
				OWb = true;
				break;
			}
		}
	}
	if (qla.nodeType == 3) {
		var bub = qla.nodeValue;
		if (rla < bub.length) {
			while (rla < bub.length) {
				if (Zya(bub.charAt(rla))) {
					++rla;
				} else {
					break;
				}
			}
		}
	}
	return new kqa(qla, rla, true);
};
SSDOM.getSentenceBreakToRight = function (Jkb, mTb) {
	if (typeof(mTb) == "undefined") {
		mTb = null;
	}
	if (Jkb == null || Jkb.node == null) {
		return null;
	}
	var aOb = Jkb.node;
	var dOb = Jkb.offset;
	var qla = aOb;
	var rla = dOb;
	var OWb = false;
	var tla = ' ';
	while (!OWb) {
		if (aOb.nodeType == 3) {
			var bub = aOb.nodeValue;
			if (bub.length > 0) {
				if (tla == '.') {
					var vla = bub.charAt(dOb);
					if (!Bya(vla)) {
						OWb = true;
						break;
					}
				}
				bub = bub.replace(/[\x21\x3f\x3a]/g, ".");
				var FBc = bub.indexOf(".", dOb);
				while (FBc > -1) {
					if (JEb(bub, FBc, aOb)) {
						if (FBc < bub.length - 1) {
							qla = aOb;
							rla = FBc + 1;
							OWb = true;
						}
						break;
					}
					dOb = FBc + 1;
					FBc = bub.indexOf(".", dOb);
				}
				if (OWb) {
					break;
				}
				if (bub.trimTH().length > 0) {
					qla = aOb;
					rla = bub.length;
				}
				tla = bub.charAt(bub.length - 1);
				if (tla == '.') {
					if (!JEb(bub, bub.length - 1, aOb)) {
						tla = ' ';
					}
				}
			}
			aOb = SSDOM.getNextNode(aOb, true, mTb);
		} else {
			if (SSDOM.isSpecialCase(aOb) && aOb.getAttribute("ignore") == null) {
				if (aOb.tagName.toLowerCase() == "math") {
					OWb = true;
					break;
				}
				if (aOb.isMathJax) {
					OWb = true;
					break;
				}
				qla = aOb;
				rla = 0;
				aOb = SSDOM.getNextNodeIgnoreChildren(aOb, true, mTb);
			} else {
				aOb = SSDOM.getNextNode(aOb, true, mTb);
			}
		}
		dOb = 0;
		if (aOb == null) {
			OWb = true;
			break;
		}
	}
	if (qla.nodeType == 3) {
		var bub = qla.nodeValue;
		if (rla > 0 && rla <= bub.length) {
			while (rla > 0) {
				if (Zya(bub.charAt(rla - 1))) {
					--rla;
				} else {
					break;
				}
			}
		}
	}
	return new kqa(qla, rla, false);
};
SSDOM.getPreviousTextNode = function (Bub, p_bGoByStyle, mTb) {
	var hUb = Bub;
	var Xxb = false;
	while (hUb != null && hUb != mTb) {
		hUb = SSDOM.getPreviousNode(hUb, p_bGoByStyle, mTb);
		if (hUb != null) {
			if (hUb.nodeType == 3 && hUb.parentNode.tagName.toLowerCase() != "textarea") {
				if (hUb.nodeValue.length > 0) {
					Xxb = true;
				}
			}
			if (hUb.nodeType == 1 && hUb.tagName.toLowerCase() == "math") {
				Xxb = true;
			} else if (hUb.nodeType == 1 && hUb.isMathJax) {
				Xxb = true;
			} else if (hUb.nodeType == 1 && hUb.tagName.toLowerCase() == "img") {
				var VRb = hUb.getAttribute("msg");
				if (VRb != null && VRb.length > 0) {
					Xxb = true;
				}
			}
			if (Xxb) {
				return hUb;
			}
		}
	}
	return null;
};
SSDOM.getPreviousTextNodeNoBlank = function (Bub, p_bGoByStyle, mTb) {
	var hUb = Bub;
	while (hUb != null && hUb != mTb) {
		hUb = SSDOM.getPreviousTextNode(hUb, p_bGoByStyle, mTb);
		if (hUb != null) {
			var Ima = (hUb.nodeType == 3) ? hUb.nodeValue.trimTH() : hUb.getAttribute("msg").trimTH();
			if (mya(Ima)) {
				return hUb;
			}
		}
	}
	return null;
};
SSDOM.getPreviousTextNodeNoImg = function (Bub, p_bGoByStyle, mTb, p_bIncludeBlanks) {
	var hUb = (p_bIncludeBlanks) ? SSDOM.getPreviousTextNode(Bub, p_bGoByStyle, mTb) : SSDOM.getPreviousTextNodeNoBlank(Bub, p_bGoByStyle, mTb);
	while (hUb != null && hUb.nodeType != 3 && hUb != mTb) {
		if (hUb.tagName.toLowerCase() == "math") {
			break;
		}
		if (hUb.isMathJax) {
			break;
		}
		hUb = (p_bIncludeBlanks) ? SSDOM.getPreviousTextNode(hUb, p_bGoByStyle, mTb) : SSDOM.getPreviousTextNodeNoBlank(hUb, p_bGoByStyle, mTb);
	}
	return hUb;
};
SSDOM.getNextTextNode = function (Bub, p_bGoByStyle, mTb) {
	var aOb = Bub;
	var Xxb = false;
	while (aOb != null && aOb != mTb) {
		aOb = SSDOM.getNextNode(aOb, p_bGoByStyle, mTb);
		if (aOb != null) {
			if (aOb.nodeType == 3 && aOb.parentNode.tagName.toLowerCase() != "textarea") {
				if (aOb.nodeValue.length > 0) {
					Xxb = true;
				}
			}
			if (aOb.nodeType == 1 && aOb.tagName.toLowerCase() == "math") {
				Xxb = true;
			} else if (aOb.nodeType == 1 && aOb.isMathJax) {
				Xxb = true;
			} else if (aOb.nodeType == 1 && aOb.tagName.toLowerCase() == "img") {
				var VRb = aOb.getAttribute("msg");
				if (VRb != null && VRb.length > 0) {
					Xxb = true;
				}
			}
			if (Xxb) {
				return aOb;
			}
		}
	}
	return null;
};
SSDOM.getNextTextNodeNoBlank = function (Bub, p_bGoByStyle, mTb) {
	var aOb = Bub;
	while (aOb != null && aOb != mTb) {
		aOb = SSDOM.getNextTextNode(aOb, p_bGoByStyle, mTb);
		if (aOb != null) {
			var Ima;
			if (aOb.nodeType == 3) {
				Ima = aOb.nodeValue.trimTH();
			} else {
				if (aOb.tagName.toLowerCase() == "img") {
					Ima = aOb.getAttribute("msg").trimTH();
				} else if (aOb.tagName.toLowerCase() == "math") {
					Ima = SSDOM.getTextFromMathMl(aOb);
				} else if (aOb.isMathJax) {
					Ima = SSDOM.getTextFromMathJax(aOb);
				}
			}
			if (mya(Ima)) {
				return aOb;
			}
		}
	}
	return null;
};
SSDOM.getNextTextNodeNoImg = function (Bub, p_bGoByStyle, mTb, p_bIncludeBlanks) {
	var aOb = (p_bIncludeBlanks) ? SSDOM.getNextTextNode(Bub, p_bGoByStyle, mTb) : SSDOM.getNextTextNodeNoBlank(Bub, p_bGoByStyle, mTb);
	while (aOb != null && aOb.nodeType != 3 && aOb != mTb) {
		if (aOb.tagName.toLowerCase() == "math") {
			break;
		}
		if (aOb.isMathJax) {
			break;
		}
		aOb = (p_bIncludeBlanks) ? SSDOM.getNextTextNode(aOb, p_bGoByStyle, mTb) : SSDOM.getNextTextNodeNoBlank(aOb, p_bGoByStyle, mTb);
	}
	return aOb;
};
SSDOM.getFirstSentence = function (OXb) {
	var kma = SSDOM.getFirstChildTextNode(OXb, true);
	var ygb = new kqa(kma, 0, true);
	var zgb = SSDOM.getSentenceBreakToRight(ygb, OXb);
	ygb = SSDOM.getSentenceBreakToLeft(zgb, OXb);
	if (ygb == null || zgb == null) {
		return null;
	}
	var Rpb = new qqa(ygb, zgb);
	var Uma = false;
	while (!Uma) {
		Uma = wya(Rpb) && SSDOM.checkSentence(Rpb);
		if (!Uma) {
			var Vma = SSDOM.getNextSentence(Rpb, OXb);
			if (Vma == null || (zgb.node == Vma.zgb.node && zgb.offset == Vma.zgb.offset) || (ygb.node == Vma.ygb.node && ygb.offset == Vma.ygb.offset)) {
				break;
			} else {
				Rpb = Vma;
			}
		}
	}
	return Rpb;
};
SSDOM.getLastSentence = function (OXb) {
	var kma = SSDOM.getLastChildTextNode(OXb, true);
	var zgb;
	if (kma.nodeType == 3) {
		zgb = new kqa(kma, kma.nodeValue.length, false);
	} else {
		zgb = new kqa(kma, -1, false);
	}
	var ygb = SSDOM.getSentenceBreakToLeft(zgb, OXb);
	zgb = SSDOM.getSentenceBreakToRight(ygb, OXb);
	if (ygb == null || zgb == null) {
		return null;
	}
	var Rpb = new qqa(ygb, zgb);
	var Uma = false;
	while (!Uma) {
		Uma = wya(Rpb) && SSDOM.checkSentence(Rpb);
		if (!Uma) {
			var Vma = SSDOM.getPreviousSentence(Rpb, OXb);
			if (Vma == null || (zgb.node == Vma.zgb.node && zgb.offset == Vma.zgb.offset) || (ygb.node == Vma.ygb.node && ygb.offset == Vma.ygb.offset)) {
				break;
			} else {
				Rpb = Vma;
			}
		}
	}
	return Rpb;
};
SSDOM.getSentenceFromPoint = function (Jkb) {
	var zgb = SSDOM.getSentenceBreakToRight(Jkb);
	var ygb = SSDOM.getSentenceBreakToLeft(zgb);
	if (ygb == null || zgb == null) {
		return null;
	}
	return new qqa(ygb, zgb);
};
SSDOM.getSentenceFromPointByLang = function (Jkb) {
	var zgb = SSDOM.getSentenceBreakToRight(Jkb);
	var DOb = ZJb(Jkb.node);
	var EOb = mJb(Jkb.node, zgb.node, DOb);
	if (EOb != null) {
		zgb = EOb;
	}
	var ygb = SSDOM.getSentenceBreakToLeft(zgb);
	DOb = ZJb(ygb.node);
	var cma = sJb(ygb.node, zgb.node, DOb);
	if (cma != null) {
		ygb = cma;
	}
	if (ygb == null || zgb == null) {
		return null;
	}
	return new qqa(ygb, zgb);
};
SSDOM.getSentenceFromPoints = function (gOb, hOb) {
	var ygb = SSDOM.getSentenceBreakToLeft(gOb, null);
	var zgb = SSDOM.getSentenceBreakToRight(hOb, null);
	if (ygb == null || zgb == null) {
		return null;
	}
	return new qqa(ygb, zgb);
};
SSDOM.getNextSentence = function (WQb, mTb) {
	if (WQb == null) {
		return null;
	}
	if (typeof(mTb) == "undefined") {
		mTb = null;
	}
	var jOb = WQb.zgb.node;
	var pma = WQb.zgb.offset;
	if (WQb.zgb.isSpecialCase()) {
		jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, mTb);
		pma = 0;
	}
	var zgb;
	var ygb;
	while (jOb != null) {
		if (jOb.nodeType == 3 && pma < jOb.nodeValue.length) {
			zgb = SSDOM.getSentenceBreakToRight(new kqa(jOb, pma, false), mTb);
			if (zgb == null) {
				return null;
			}
			if (zgb.node == jOb && zgb.offset == pma) {
				var kma = SSDOM.getNextTextNodeNoBlank(jOb, false, mTb);
				if (kma == null) {
					return null;
				}
				zgb = SSDOM.getSentenceBreakToRight(new kqa(kma, 0, false), mTb);
			}
		} else {
			var kma = SSDOM.getNextTextNodeNoBlank(jOb, false, mTb);
			if (kma == null) {
				return null;
			}
			zgb = SSDOM.getSentenceBreakToRight(new kqa(kma, 0, false), mTb);
		}
		ygb = SSDOM.getSentenceBreakToLeft(zgb, null);
		if (ygb == null) {
			return null;
		}
		if (WQb.ygb.node != ygb.node || WQb.ygb.offset != ygb.offset) {
			var Rpb = new qqa(ygb, zgb);
			if (SSDOM.checkSentence(Rpb) && wya(Rpb)) {
				return Rpb;
			}
		}
		jOb = zgb.node;
		if (jOb.nodeType == 3) {
			var RAc = jOb.nodeValue.replace(/[\x21\x3f\x3a]/g, ".");
			var GBc = RAc.indexOf(".", zgb.offset + 1);
			if (GBc == -1) {
				pma = RAc.length;
			} else {
				pma = GBc;
			}
		}
	}
	return null;
};
SSDOM.getPreviousSentence = function (WQb, mTb) {
	if (typeof(mTb) == "undefined") {
		mTb = null;
	}
	var jOb = WQb.ygb.node;
	var pma = WQb.ygb.offset;
	var ygb;
	var zgb;
	while (jOb != null) {
		if (jOb.nodeType == 3) {
			var RAc = jOb.nodeValue.replace(/[\x21\x3f\x3a]/g, ".");
			var GBc;
			if (pma > 0) {
				GBc = RAc.lastIndexOf(".", pma);
			} else if (pma == 0) {
				GBc = -1;
			} else {
				GBc = RAc.lastIndexOf(".");
			}
			while (GBc > -1) {
				pma = GBc;
				zgb = SSDOM.getSentenceBreakToRight(new kqa(jOb, pma, true), mTb);
				if (zgb == null) {
					return null;
				}
				if (zgb.node != WQb.zgb.node || zgb.offset != WQb.zgb.offset) {
					ygb = SSDOM.getSentenceBreakToLeft(zgb, mTb);
					if (ygb == null) {
						return null;
					}
					var Rpb = new qqa(ygb, zgb);
					if (SSDOM.checkSentence(Rpb) && wya(Rpb)) {
						return Rpb;
					}
				}
				if (GBc == 0) {
					GBc = -1;
				} else {
					GBc = RAc.lastIndexOf(".", GBc - 1);
				}
			}
		}
		pma = -1;
		wvb = SSDOM.getPreviousTextNodeNoImg(jOb, true, mTb, false);
		if (wvb != null) {
			jOb = wvb;
		} else {
			jOb = SSDOM.getPreviousTextNodeNoBlank(jOb, false, mTb);
			if (jOb != null) {
				if (jOb.nodeType == 3) {
					zgb = SSDOM.getSentenceBreakToRight(new kqa(jOb, jOb.nodeValue.length, false), mTb);
				} else {
					zgb = SSDOM.getSentenceBreakToRight(new kqa(jOb, 0, false), mTb);
				}
				if (zgb == null) {
					return null;
				}
				if (zgb.node != WQb.zgb.node || zgb.offset != WQb.zgb.offset) {
					ygb = SSDOM.getSentenceBreakToLeft(zgb, mTb);
					if (ygb == null) {
						return null;
					}
					return new qqa(ygb, zgb);
				}
			}
		}
	}
	return null;
};
SSDOM.checkSentence = function (WQb) {
	var cub = WQb.ygb.node;
	var ipb = cub.ownerDocument.body;
	while (cub != null && cub != ipb) {
		if (cub.nodeType == 1) {
			if (cub.className == "rwDictDefin" || cub.className == "rwPopupContent" || cub.className == "rwToolbarBarCollect") {
				break;
			}
			if (cub.getAttribute(caa) != null) {
				return false;
			}
		}
		cub = cub.parentNode;
	}
	var ugb = WQb.zgb.node;
	if (ugb != cub) {
		while (ugb != null && ugb != ipb) {
			if (ugb.className == "rwDictDefin" || ugb.className == "rwPopupContent" || ugb.className == "rwToolbarBarCollect") {
				break;
			}
			if (ugb.nodeType == 1 && ugb.getAttribute(caa) != null) {
				return false;
			}
			ugb = ugb.parentNode;
		}
	}
	return true;
};
SSDOM.getStartOfParagraph = function (Bub) {
	var kAb;
	var match = Bub;
	var fkb = SSDOM.getPreviousNode(Bub, true, null);
	while (fkb != null) {
		kAb = false;
		if (fkb.nodeType == 1) {
			if (SSDOM.isSpecialCase(fkb)) {
				if (fkb.getAttribute("ignore") != null) {
					kAb = true;
				}
			} else {
				kAb = true;
			}
		} else if (fkb.nodeType == 3) {
			if (fkb.nodeValue.trimTH().length == 0) {
				kAb = true;
			}
		}
		if (!kAb) {
			match = fkb;
		}
		fkb = SSDOM.getPreviousNode(fkb, true, null);
	}
	return match;
};
SSDOM.getEndOfParagraph = function (Bub) {
	var kAb;
	var match = Bub;
	var nextNode = SSDOM.getNextNode(Bub, true, null);
	while (nextNode != null) {
		kAb = false;
		if (nextNode.nodeType == 1) {
			if (SSDOM.isSpecialCase(nextNode)) {
				if (nextNode.getAttribute("ignore") != null) {
					kAb = true;
				}
			} else {
				kAb = true;
			}
		} else if (nextNode.nodeType == 3) {
			if (nextNode.nodeValue.trimTH().length == 0) {
				kAb = true;
			}
		}
		if (!kAb) {
			match = nextNode;
		}
		nextNode = SSDOM.getNextNode(nextNode, true, null);
	}
	return match;
};
SSDOM.getEndOfWordAsCaret = function (Ghb) {
	var tBc = null;
	var node = Ghb.node;
	if (node.nodeType != 3) {
		return null;
	}
	var bub = node.nodeValue;
	var rzb = Ghb.offset;
	var Xxb = false;
	var i;
	var c;
	var Gna = false;
	while (!Xxb) {
		for (i = rzb; i < bub.length; i++) {
			c = bub.charCodeAt(i);
			if (!jya(c)) {
				if (Gna) {
					return new kqa(node, i, false);
				} else {
					return tBc;
				}
			} else {
				Gna = true;
			}
		}
		if (!Xxb) {
			if (Gna) {
				tBc = new kqa(node, bub.length, false);
				Gna = false;
			}
			node = SSDOM.getNextTextNode(node, true, null);
			if (node == null) {
				break;
			}
			if (node.nodeType != 3) {
				break;
			}
			bub = node.nodeValue;
			rzb = 0;
		}
	}
	return tBc;
};
SSDOM.eob = function (o, p, q) {
	function r(a) {
		var b;
		if (typeof DOMParser != "undefined") {
			b = (new DOMParser()).parseFromString(a, "application/xml");
		} else {
			var c = ["MSXML2.DOMDocument", "MSXML.DOMDocument", "Microsoft.XMLDOM"];
			for (var i = 0; i < c.length && !b; i++) {
				try {
					b = new ActiveXObject(c[i]);
					b.loadXML(a);
				} catch (e) {}

			}
		}
		return b;
	}
	function s(a, b, c) {
		a[b] = function () {
			return eval(c);
		};
	}
	function t(b, c, d) {
		if (typeof d == "undefined") {
			d = 1;
		}
		if (d > 1) {
			if (c.nodeType == 1) {
				var e = document.createElement(c.nodeName);
				var f = {};
				for (var a = 0, g = c.attributes.length; a < g; a++) {
					var h = c.attributes[a].name,
					k = c.attributes[a].value,
					l = (h.substr(0, 2) == "on");
					if (l) {
						f[h] = k;
					} else {
						switch (h) {
						case "class":
							e.className = k;
							break;
						case "for":
							e.htmlFor = k;
							break;
						default:
							e.setAttribute(h, k);
						}
					}
				}
				b = b.appendChild(e);
				for (l in f) {
					s(b, l, f[l]);
				}
			} else if (c.nodeType == 3) {
				var m = (c.nodeValue ? c.nodeValue : "");
				var n = m.replace(/^\s*|\s*$/g, "");
				if (n.length < 7 || (n.indexOf("<!--") != 0 && n.indexOf("-->") != (n.length - 3))) {
					b.appendChild(document.createTextNode(m));
				}
			}
		}
		for (var i = 0, j = c.childNodes.length; i < j; i++) {
			t(b, c.childNodes[i], d + 1);
		}
	}
	p = "<root>" + p + "</root>";
	var u = r(p);
	if (o && u) {
		if (q != false) {
			while (o.lastChild) {
				o.removeChild(o.lastChild);
			}
		}
		t(o, u.documentElement);
	}
};
SSDOM.isStyleNode = function (Bub) {
	if (Bub == null) {
		return false;
	}
	if (Bub.nodeType != 1) {
		return Bub.nodeType == 3;
	}
	var Hpb = Bub.tagName.toLowerCase().trimTH();
	if (Hpb == aga.pageData.strHighlightTag) {
		var alb = Bub.getAttribute("started");
		if (alb != null && alb == "1") {
			return false;
		}
	}
	if (Hpb == "span" && Bub.getAttribute(hba) != null) {
		return false;
	}
	return aga.pageData.isInStyleList(Hpb);
};
SSDOM.isInvalidNode = function (Bub) {
	if (Bub == null) {
		return true;
	}
	if (Bub.nodeType != 1) {
		return Bub.nodeType != 3;
	}
	var attr;
	attr = Bub.getAttribute("ignore");
	if (attr != null) {
		return true;
	}
	attr = Bub.getAttribute(hba);
	if (attr != null && aga.controlData.bIgnoreSkipSection) {
		return true;
	}
	if (Sga) {
		var Jna = SSDOM.getComputedStyle(Bub);
		if (Jna != null) {
			if (Jna.visibility == "hidden" || Jna.display == "none") {
				return true;
			}
		}
	}
	var Hpb = Bub.tagName.toLowerCase();
	return Hpb == "link" || Hpb == "area" || Hpb == "script" || Hpb == "noscript" || Hpb == "annotation" || Hpb == "style" || Hpb == "!--" || Hpb == "title" || Hpb == "html:script";
};
SSDOM.isInvalidNodeNested = function (Bub) {
	if (Bub == null) {
		return true;
	}
	var ipb = Bub.ownerDocument.body;
	var wvb = Bub;
	while (wvb != null) {
		if (SSDOM.isInvalidNode(wvb)) {
			return true;
		}
		if (wvb == ipb) {
			break;
		}
		wvb = wvb.parentNode;
	}
	return false;
};
SSDOM.isIgnored = function (Bub) {
	if (Bub != null && Bub.nodeType == 3) {
		Bub = Bub.parentNode;
	}
	if (Bub == null) {
		return true;
	}
	var ipb = Bub.ownerDocument.body;
	var wvb = Bub;
	while (wvb != null && wvb.nodeType == 1) {
		if (wvb.getAttribute("ignore") != null) {
			return true;
		}
		if (wvb == ipb) {
			break;
		}
		wvb = wvb.parentNode;
	}
	return false;
};
SSDOM.isLineBreakNode = function (ZWb) {
	if (ZWb.nodeType != 1) {
		return false;
	}
	var Hpb = ZWb.tagName.toLowerCase().trimTH();
	return aga.pageData.isInBreakList(Hpb);
};
SSDOM.isFollowedBySpace = function (Ghb) {
	var node = Ghb.node;
	var rzb = Ghb.offset;
	if (node.nodeType != 3) {
		return false;
	}
	if (rzb == node.nodeValue.length) {
		rzb = 0;
		node = SSDOM.getNextTextNodeNoImg(node, true, null, true);
		if (node == null) {
			return false;
		}
	}
	var c = node.nodeValue.charCodeAt(rzb);
	return c == 32 || c == 160;
};
SSDOM.isFollowedByWordBreak = function (Ghb) {
	if (!Ghb.forwardBias) {
		Ghb = Ghb.clone();
		Ghb.forwardBias = true;
	}
	if (Ghb.offset == Ghb.node.nodeValue.length) {
		Ghb = SSDOM.moveCaret(Ghb, 0, true);
		if (Ghb == null) {
			return true;
		}
	}
	var c = Ghb.node.nodeValue.charCodeAt(Ghb.offset);
	if (c == 39) {
		if (arguments.length > 1 && arguments[1] === true) {
			return true;
		} else {
			var Rna = SSDOM.moveCaret(Ghb, 1, true);
			if (Rna == null) {
				return true;
			}
			return SSDOM.isFollowedByWordBreak(Rna, true);
		}
	} else {
		return !vxa(c);
	}
};
SSDOM.isFollowingWordBreak = function (Ghb) {
	if (Ghb.forwardBias) {
		Ghb = Ghb.clone();
		Ghb.forwardBias = true;
	}
	if (Ghb.offset == 0) {
		Ghb = SSDOM.moveCaret(Ghb, 0, true);
		if (Ghb == null) {
			return true;
		}
	}
	var c = Ghb.node.nodeValue.charCodeAt(Ghb.offset - 1);
	if (c == 39) {
		if (arguments.length > 1 && arguments[1] === true) {
			return true;
		} else {
			var Sna = SSDOM.moveCaret(Ghb, -1, true);
			if (Sna == null) {
				return true;
			}
			return SSDOM.isFollowingWordBreak(Sna, true);
		}
	} else {
		return !vxa(c);
	}
};
SSDOM.isSpecialCase = function (Bub) {
	if (Bub == null) {
		return false;
	}
	if (Bub.nodeType == 1) {
		var tagName = Bub.tagName.toLowerCase();
		if (tagName == "span") {
			var attr = Bub.getAttribute("pron");
			if (attr != null) {
				return true;
			}
			attr = Bub.getAttribute("chunk");
			if (attr != null) {
				return true;
			}
			attr = Bub.isMathJax;
			if (attr != null && attr) {
				var DVb = SSDOM.getTextFromMathJax(Bub);
				if (DVb.length > 0) {
					return true;
				}
			}
		} else if (tagName == "acronym" || tagName == "abbr") {
			var attr = Bub.getAttribute("title");
			if (attr != null) {
				return true;
			}
		} else if (tagName == "chunk") {
			return true;
		} else if (tagName == "img") {
			var attr = Bub.getAttribute("msg");
			if (attr != null) {
				return true;
			}
		} else if (tagName == "math") {
			var DVb = SSDOM.getTextFromMathMl(Bub);
			if (DVb.length > 0) {
				return true;
			}
		}
		if (Bub.getAttribute("ignore") != null) {
			return true;
		}
	}
	return false;
};
SSDOM.isSpecialCaseHighlightable = function (Bub) {
	if (Bub.nodeType == 1) {
		var tagName = Bub.tagName.toLowerCase();
		if (tagName == "span") {
			var attr = Bub.getAttribute("pron");
			if (attr != null) {
				return true;
			}
			attr = Bub.getAttribute("chunk");
			if (attr != null && attr == "1") {
				return true;
			}
			attr = Bub.isMathJax;
			if (attr != null && attr) {
				var DVb = SSDOM.getTextFromMathJax(Bub);
				if (DVb.length > 0) {
					return true;
				}
			}
		} else if (tagName == "acronym" || tagName == "abbr") {
			var attr = Bub.getAttribute("title");
			if (attr != null) {
				return true;
			}
		} else if (tagName == "math") {
			var DVb = SSDOM.getTextFromMathMl(Bub);
			if (DVb.length > 0) {
				return true;
			}
		}
	}
	return false;
};
SSDOM.checkForSpecialParent = function (Bub) {
	if (Bub != null) {
		var ipb = SSDOM.getBody(Bub);
		var wvb = Bub;
		while (wvb != null && wvb != ipb) {
			if (SSDOM.isSpecialCase(wvb)) {
				return wvb;
			}
			wvb = wvb.parentNode;
		}
		if (wvb == ipb) {
			if (wvb.getAttribute("ignore") != null) {
				return wvb;
			}
		}
	}
	return null;
};
SSDOM.getContainingNode = function (fNb, p_attr, p_attrVal) {
	var ipb = SSDOM.getBody(fNb);
	var wvb = fNb;
	if (wvb.nodeType == 3) {
		wvb = wvb.parentNode;
	}
	while (wvb != null && wvb != ipb) {
		if (p_attr == "class" && Lfa) {
			if (wvb.className == p_attrVal) {
				return wvb;
			}
			if (wvb.getAttribute("className") == p_attrVal) {
				return wvb;
			}
		}
		if (wvb.getAttribute(p_attr) != null) {
			if (p_attrVal == null || wvb.getAttribute(p_attr) == p_attrVal) {
				return wvb;
			}
		}
		wvb = wvb.parentNode;
	}
	return null;
};
SSDOM.getAllTextFromNode = function (Bub) {
	var bub = "";
	if (Bub.nodeType == 3) {
		if (!SSDOM.isInvalidNode(Bub.parentNode) && Bub.parentNode.tagName.toLowerCase() != "textarea") {
			bub = Bub.nodeValue;
		}
	} else if (Bub.nodeType == 1) {
		if (Bub.getAttribute("ignore") != null) {
			bub = "";
			OWb = true;
		} else {
			var qRb = Bub.tagName.toLowerCase();
			var OWb = false;
			if (qRb == "img") {
				var VRb = Bub.getAttribute("msg");
				if (VRb != null && VRb.trimTH().length > 0) {
					bub = " " + VRb.trimTH() + " ";
				}
				OWb = true;
			} else if (qRb == "span") {
				var VRb = Bub.getAttribute("pron");
				if (VRb != null && VRb.trimTH().length > 0) {
					bub = VRb.trimTH();
					OWb = true;
				}
				VRb = Bub.isMathJax;
				if (VRb) {
					bub = SSDOM.getTextFromMathJax(Bub);
				}
			} else if (qRb == "acronym" || qRb == "abbr") {
				var VRb = Bub.getAttribute("pron");
				if (VRb != null && VRb.trimTH().length > 0) {
					bub = VRb.trimTH();
				} else {
					VRb = Bub.getAttribute("title");
					if (VRb != null && VRb.trimTH().length > 0) {
						bub = VRb.trimTH();
						OWb = true;
					}
				}
			} else if (qRb == "math") {
				bub = SSDOM.getTextFromMathMl(Bub);
			}
		}
		if (!OWb) {
			var Vpa = Bub.firstChild;
			while (Vpa != null) {
				bub += SSDOM.getAllTextFromNode(Vpa);
				Vpa = Vpa.nextSibling;
			}
		}
	}
	return bub;
};
SSDOM.getTextFromNode = function (Bub) {
	var bub = "";
	if (Bub.nodeType == 3) {
		if (!SSDOM.isInvalidNode(Bub.parentNode) && Bub.parentNode.tagName.toLowerCase() != "textarea") {
			bub = Bub.nodeValue;
		}
	} else if (Bub.nodeType == 1) {
		if (Bub.getAttribute("ignore") != null) {
			bub = "";
		} else {
			var qRb = Bub.tagName.toLowerCase();
			if (qRb == "img") {
				var VRb = Bub.getAttribute("msg");
				if (VRb != null && VRb.trimTH().length > 0) {
					bub = " " + VRb.trimTH() + " ";
				}
			} else if (qRb == "span") {
				var VRb = Bub.getAttribute("pron");
				if (VRb != null && VRb.trimTH().length > 0) {
					bub = VRb.trimTH();
				}
				VRb = Bub.getAttribute("chunk");
				if (VRb != null && VRb == "1") {
					bub = Bub.innerHTML;
				}
				VRb = Bub.isMathJax;
				if (VRb) {
					bub = SSDOM.getTextFromMathJax(Bub);
				}
			} else if (qRb == "acronym" || qRb == "abbr") {
				var VRb = Bub.getAttribute("pron");
				if (VRb != null && VRb.trimTH().length > 0) {
					bub = VRb.trimTH();
				} else {
					VRb = Bub.getAttribute("title");
					if (VRb != null && VRb.trimTH().length > 0) {
						bub = VRb.trimTH();
					}
				}
			} else if (qRb == "math") {
				bub = SSDOM.getTextFromMathMl(Bub);
			}
		}
	}
	return bub;
};
SSDOM.getTextOverRange = function (OXb, qNb, p_domRefRight) {
	try {
		if (qNb == null || p_domRefRight == null) {
			return "";
		}
		var YCc = SSDOM.getCaretPairFromDomPosition(OXb, qNb.path, qNb.offset, p_domRefRight.path, p_domRefRight.offset);
		return SSDOM.getTextOverCaretRange(YCc);
	} catch (err) {
		eya("Error getTextOverRange: " + err.message);
		return "";
	}
};
SSDOM.getTextOverCaretRange = function (COb) {
	try {
		if (COb == null || COb.ygb == null || COb.zgb == null) {
			return "";
		}
		var ygb = COb.ygb;
		var zgb = COb.zgb;
		var hUb = ygb.node;
		var aOb = zgb.node;
		var mOb = true;
		var jOb = hUb;
		var bub = "";
		while (jOb != null) {
			var mBc = "";
			if (jOb.nodeType == 3) {
				if (!SSDOM.isInvalidNode(jOb.parentNode) && jOb.parentNode.tagName.toLowerCase() != "textarea") {
					mBc = jOb.nodeValue;
				}
			}
			if (mBc != null && mBc != "") {
				if (jOb == aOb && zgb.offset > -1) {
					mBc = mBc.substring(0, zgb.offset);
				}
				if (jOb == hUb && ygb.offset > -1) {
					mBc = mBc.substring(ygb.offset);
				}
				bub += mBc;
			}
			if (mOb) {
				jOb = SSDOM.getNextNodeAllowMoveToChild(jOb, false, aOb);
			} else {
				jOb = SSDOM.getNextTextNode(jOb, false, aOb);
			}
			mOb = false;
		}
		return bub.trimTH();
	} catch (err) {
		eya("Error getTextOverCaretRange: " + err.message);
		return "";
	}
};
SSDOM.getTextSpokenOverCaretRange = function (COb) {
	try {
		if (COb == null || COb.ygb == null || COb.zgb == null) {
			return "";
		}
		var ygb = COb.ygb;
		var zgb = COb.zgb;
		var hUb = ygb.node;
		var aOb = zgb.node;
		var tya = false;
		var jOb = hUb;
		var bub = "";
		while (jOb != null) {
			tya = SSDOM.isSpecialCase(jOb);
			if (tya || jOb.nodeType == 3) {
				var mBc = SSDOM.getTextFromNode(jOb);
				if (mBc != null && mBc != "") {
					if (!tya) {
						if (jOb == aOb && zgb.offset > -1) {
							mBc = mBc.substring(0, zgb.offset);
						}
						if (jOb == hUb && ygb.offset > -1) {
							mBc = mBc.substring(ygb.offset);
						}
					}
					bub += mBc;
				}
			}
			if (tya) {
				jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, aOb);
			} else {
				jOb = SSDOM.getNextNode(jOb, false, aOb);
			}
		}
		return bub.trimTH();
	} catch (err) {
		eya("Error getTextOverCaretRange: " + err.message);
		return "";
	}
};
SSDOM.textFromFormTH = function (ZWb) {
	var Pib = null;
	var qRb = ZWb.tagName.toLowerCase();
	var klb = SSDOM.getPositionInDom(ZWb);
	if (qRb == "input") {
		var Fcb = ZWb.getAttribute("type");
		if (Fcb != null) {
			Fcb = Fcb.toLowerCase();
		}
		var FZb = "";
		if (Fcb == null || Fcb.equalsTH("") || Fcb.equalsTH("text")) {
			FZb = ZWb.value;
		} else if (Fcb.equalsTH("password")) {
			FZb = "Masked password field";
		} else if (Fcb.equalsTH("image")) {
			FZb = "";
		} else if (Fcb.equalsTH("button") || Fcb.equalsTH("submit") || Fcb.equalsTH("reset")) {
			FZb = ZWb.getAttribute("value");
		}
		if (!FZb.equalsTH("")) {
			Pib = "form:" + klb + ";" + FZb;
		}
	} else if (qRb == "select") {
		var FZb = "";
		var wUb = ZWb.selectedIndex;
		var htb = "";
		for (var bxb = 0; bxb < ZWb.options.length; bxb++) {
			htb += ZWb.options[bxb].text + " ";
		}
		if (!htb.equalsTH("")) {
			if (wUb > -1) {
				FZb = ZWb.options[wUb].text;
				FZb += " selected from the list " + htb;
			} else {
				FZb = "No selection from the list " + htb;
			}
			Pib = "form" + klb + ";" + FZb;
		}
	} else if (qRb == "textarea") {
		var FZb = ZWb.value;
		Pib = "form" + klb + ";" + FZb;
	} else if (qRb == "option") {
		var FZb = ZWb.value;
		Pib = "form" + klb + ";" + FZb;
	}
	return Pib;
};
SSDOM.getTargetElement = function (evt) {
	var VMb;
	if (Lfa) {
		VMb = evt.srcElement;
	} else if (Yfa) {
		VMb = evt.target;
	} else {
		VMb = evt.target;
	}
	return VMb;
};
SSDOM.getClickPoint = function (evt) {
	var Zub = null;
	var VMb;
	if (Lfa) {
		VMb = evt.srcElement;
		if (VMb.nodeType == 1 && VMb.tagName.toLowerCase() == "textarea") {}
		else {
			Zub = rw_getTargetNodeAsCaretIE(evt, true);
			if (Zub != null) {
				if (Zub.node == null || Zub.node.parentNode == null || Zub.node.parentNode != VMb) {
					Zub = null;
					return null;
				} else {
					if (Zub.node.nodeType == 3) {
						Zub.node = SSDOM.mergeTextNodes(Zub.node);
					}
				}
			}
		}
	} else if (Yfa) {
		VMb = evt.target;
		if (VMb != null) {
			if (wLb) {
				if (VMb.firstChild != null && VMb.firstChild.nodeType == 3 && VMb.tagName.toLowerCase() != "textarea") {
					var UWb = VMb.firstChild.nodeValue;
					if (UWb.trimTH().length > 0) {
						VMb = VMb.firstChild;
					}
				}
			} else if (vLb) {
				if (evt.fromElement != null && VMb.nodeType == 1 && VMb.tagName.toLowerCase() != "textarea") {
					if (evt.fromElement.nodeType == 3) {
						VMb = evt.fromElement;
					}
				} else {
					if (VMb.nodeType == 1 && VMb.firstChild != null && VMb.firstChild.nodeType == 3 && VMb.tagName.toLowerCase() != "textarea") {
						var UWb = VMb.firstChild.nodeValue;
						if (UWb.trimTH().length > 0) {
							VMb = VMb.firstChild;
						}
					}
				}
			}
		}
	} else {
		if (evt.explicitOriginalTarget.nodeValue != null) {
			if (evt.target.tagName.toLowerCase() == "textarea") {
				VMb = evt.target;
			} else {
				VMb = evt.explicitOriginalTarget;
				var iSb = window.getSelection();
				if (iSb.anchorNode == null || iSb.anchorNode != VMb) {
					return null;
				} else {
					Zub = new kqa(iSb.anchorNode, iSb.anchorOffset, true);
				}
			}
		} else {
			VMb = evt.target;
		}
	}
	if (Zub == null && VMb != null) {
		Zub = new kqa(VMb, 0, true);
	}
	return Zub;
};
SSDOM.getPositionInDom = function (p_theNode) {
	var klb = "";
	var Woa = 0;
	var Xoa = "";
	if (p_theNode != null && p_theNode.ownerDocument != null) {
		var koa = false;
		var joa = false;
		var ipb = p_theNode.ownerDocument.body;
		while (p_theNode != null && p_theNode != ipb) {
			if (SSDOM.isSpecialCase(p_theNode)) {
				klb = "";
			}
			koa = (p_theNode.nodeType == 3) || (p_theNode.nodeType == 1 && p_theNode.tagName.toLowerCase() == aga.pageData.strHighlightTag && p_theNode.getAttribute("rwstate") != null);
			var ZWb = p_theNode.previousSibling;
			while (ZWb != null) {
				joa = (ZWb.nodeType == 3) || (ZWb.nodeType == 1 && ZWb.tagName.toLowerCase() == aga.pageData.strHighlightTag && ZWb.getAttribute("rwstate") != null);
				if (koa && joa) {}
				else {
					++Woa;
				}
				ZWb = ZWb.previousSibling;
				koa = joa;
			}
			klb = klb + Woa + "~";
			Woa = 0;
			p_theNode = p_theNode.parentNode;
			if (p_theNode != null && p_theNode.getAttribute != null && p_theNode.tagName != null) {
				var coa = p_theNode.getAttribute("chunk");
				if (p_theNode.tagName.toLowerCase() == "span" && coa == "1") {
					var doa = SSDOM.getPositionInDom(p_theNode);
					Xoa = "#^th*" + doa + "#^th*";
				}
			}
		}
	}
	return Xoa + klb;
};
SSDOM.getNodeFromPosition = function (p_theBody, Vsa) {
	var aub = p_theBody;
	if (Vsa.lastIndexOf("*") > -1) {
		var FBc = Vsa.lastIndexOf("*");
		Vsa = Vsa.substring(FBc + 1);
	}
	var goa = Vsa.split("~");
	var UBc = goa.length;
	var i;
	for (i = UBc - 2; i > -1; i--) {
		aub = aub.firstChild;
		if (aub == null) {
			return null;
		}
		var uvb;
		if (goa[i].length == 0) {
			uvb = 0;
		} else {
			uvb = parseInt(goa[i], 10);
		}
		var joa = false;
		var koa = (aub.nodeType == 3) || (aub.nodeType == 1 && aub.tagName.toLowerCase() == aga.pageData.strHighlightTag && aub.getAttribute("rwstate") != null);
		while (uvb > 0) {
			aub = aub.nextSibling;
			if (aub == null) {
				return null;
			}
			joa = (aub.nodeType == 3) || (aub.nodeType == 1 && aub.tagName.toLowerCase() == aga.pageData.strHighlightTag && aub.getAttribute("rwstate") != null);
			if (joa && koa) {}
			else {
				--uvb;
				koa = joa;
			}
		}
	}
	return aub;
};
SSDOM.getCaretFromDomPosition = function (p_theBody, Vsa, Ueb, Cub, p_bForceSpecial) {
	if (typeof(p_bForceSpecial) == "undefined") {
		p_bForceSpecial = false;
	}
	try {
		if (p_theBody == null) {
			return null;
		}
		var aub = SSDOM.getNodeFromPosition(p_theBody, Vsa);
		if (p_bForceSpecial) {
			var uzb = new kqa(aub, 0, Cub);
			uzb.setSpecialCase(true);
			return uzb;
		}
		if (SSDOM.isSpecialCase(aub)) {
			if (SSDOM.isSpecialCaseHighlightable(aub)) {
				if (Cub) {
					var cub = SSDOM.getFirstChildTextNode(aub, false);
					if (cub != null) {
						return new kqa(cub, 0, Cub);
					} else {
						return new kqa(aub, 0, Cub);
					}
				} else {
					var ugb = SSDOM.getLastChildTextNode(aub, false);
					if (ugb != null) {
						if (ugb.nodeType == 3) {
							return new kqa(ugb, ugb.length, Cub);
						} else {
							return new kqa(ugb, 0, Cub);
						}
					} else {
						return new kqa(aub, 0, Cub);
					}
				}
			} else {
				return new kqa(aub, 0, Cub);
			}
		}
		var rzb = 0;
		if (!Cub) {
			++rzb;
		}
		if (Ueb > -1) {
			if (aub == null) {
				return null;
			}
			var Xxb = false;
			var ugb = aub.parentNode;
			var soa = aub;
			var RAc;
			while (!Xxb) {
				if (aub.nodeType == 3) {
					RAc = aub.nodeValue;
					if (Ueb < (rzb + RAc.length)) {
						Xxb = true;
						break;
					}
					soa = aub;
					rzb += aub.nodeValue.length;
					aub = SSDOM.getNextNode(aub, false, ugb);
				} else if (aub.nodeType == 1) {
					if (SSDOM.isSpecialCase(aub)) {
						var uoa = Ueb - rzb;
						if (uoa > 0) {
							rzb += 1;
						} else {
							Xxb = true;
							break;
						}
						aub = SSDOM.getNextNodeIgnoreChildren(aub, false, ugb);
					} else {
						aub = SSDOM.getNextNode(aub, false, ugb);
					}
				}
				if (aub == null || aub == ugb) {
					if (soa != null) {
						aub = soa;
						if (aub.nodeType == 3) {
							rzb = Ueb - aub.nodeValue.length;
						} else {
							rzb = 0;
						}
						if (!Cub) {
							++rzb;
						}
						break;
					} else {
						return null;
					}
				}
			}
			if (Cub) {
				return new kqa(aub, Ueb - rzb, Cub);
			} else {
				return new kqa(aub, Ueb - (rzb - 1), Cub);
			}
		} else {
			return new kqa(aub, Ueb, Cub);
		}
	} catch (err) {
		eya("getCaretFromDomPosition error: " + err);
		return null;
	}
};
SSDOM.getCaretPairFromDomPosition = function (p_theBody, p_strPathLeft, p_nPosLeft, p_strPathRight, p_nPosRight) {
	var ygb = SSDOM.getCaretFromDomPosition(p_theBody, p_strPathLeft, p_nPosLeft, true);
	var zgb;
	if (p_strPathLeft == p_strPathRight && p_nPosLeft >= p_nPosRight) {
		zgb = ygb;
	} else {
		zgb = SSDOM.getCaretFromDomPosition(p_theBody, p_strPathRight, p_nPosRight, false);
	}
	return new qqa(ygb, zgb);
};
SSDOM.getClass = function (Bub) {
	if (Bub == null) {
		return "";
	}
	if (Bub.className) {
		return Bub.className;
	} else {
		return Bub.getAttribute("class");
	}
};
SSDOM.getComputedStyle = function (ypb) {
	if (ypb == null) {
		return null;
	}
	if (ypb.nodeType == 3) {
		ypb = ypb.parentNode;
		if (ypb == null) {
			return null;
		}
	}
	if (SSDOM.bIEOld) {
		return ypb.currentStyle;
	} else {
		return window.getComputedStyle(ypb, null);
	}
};
SSDOM.getTextFromMathMl = function (Bub) {
	if (Bub.previousSibling != null || Bub.nextSibling != null) {
		var xoa = document.createElement("span");
		Bub.parentNode.replaceChild(xoa, Bub);
		xoa.appendChild(Bub);
	}
	if (Lfa) {
		var DVb = Bub.outerHTML;
		if (DVb == null) {
			return "";
		} else {
			if (DVb.indexOf("<?import namespace") > -1) {
				var n = DVb.indexOf("/>");
				if (n > -1) {
					DVb = DVb.substring(n + 2);
					DVb = DVb.replace(/m:/gi, "");
				}
			}
			return DVb;
		}
	} else {
		var DVb = Bub.parentNode.innerHTML;
		if (DVb != null && DVb.length > 0) {
			return DVb;
		} else {
			return "";
		}
	}
};
SSDOM.getTextFromMathJax = function (Bub) {
	if (Bub.isMathJax) {
		var wvb = Bub;
		while (wvb != null && wvb.isMathJax && !wvb.id) {
			wvb = wvb.parentNode;
		}
		var Bpa;
		if (wvb && wvb.id) {
			Bpa = "math:" + wvb.id + ";";
		} else {
			Bpa = "math:none;";
		}
	}
	var lLb = SpeechStream.mathJaxHighlighter.getJaxFor(Bub);
	if (lLb != null) {
		var Dpa = null;
		try {
			Dpa = lLb.root.toMathML("");
			if (Dpa != null || Dpa.length > 0) {
				return Bpa + Dpa;
			}
		} catch (err) {
			thLogE(err);
		}
		return "";
	} else {
		return "";
	}
};
SSDOM.getFrameSelection = function (wha) {
	if (!uda && wha.frames && wha.length > 0) {
		var i;
		var UBc = wha.length;
		for (i = 0; i < UBc; i++) {
			try {
				var OCc = wha[i].getSelection();
				if (OCc != null && !OCc.isCollapsed) {
					return wha[i];
				} else {
					if (wha[i].length > 0) {
						var Mpa = SSDOM.getFrameSelection(wha[i]);
						if (Mpa) {
							return Mpa;
						}
					}
				}
			} catch (e) {}

		}
	}
	return null;
};
SSDOM.getFrameSelectionSFF = function (wha) {
	var kSb = {};
	var mBc = SSDOM.getFrameSelection(wha);
	if (mBc != null) {
		kSb.MWb = mBc;
		kSb.jSb = mBc.getSelection();
	}
	return kSb;
};
SSDOM.getFrameSelectionOldIE = function (wha) {
	var kSb = {};
	var range;
	if (!uda && wha.frames && wha.length > 0) {
		var i;
		var UBc = wha.length;
		for (i = 0; i < UBc; i++) {
			try {
				var jeb = wha[i];
				range = jeb.document.selection.createRange();
				if (range != null && range.text != null && range.text.length > 0) {
					kSb.MWb = jeb;
					kSb.dSb = range;
					break;
				} else {
					if (jeb.length > 0) {
						var Mpa = SSDOM.getFrameSelectionOldIE(jeb);
						if (Mpa.dSb) {
							kSb = Mpa;
							break;
						}
					}
				}
			} catch (e) {}

		}
	}
	return kSb;
};
SSDOM.getByClassName = function (p_strClass, OXb, p_strTagName) {
	var jBc;
	if (!Lfa) {
		jBc = [];
		var Qpa = OXb.getElementsByClassName(p_strClass);
		var UBc = Qpa.length;
		var i;
		for (i = 0; i < UBc; i++) {
			jBc.push(Qpa[i]);
		}
	} else {
		jBc = SSDOM.getElementsByClass(p_strClass, OXb, p_strTagName);
	}
	return jBc;
};
SSDOM.getElementsByClass = function (p_strClassName, Bub, p_strTagName) {
	var Qpa = [];
	if (p_strTagName == null || p_strTagName == "*") {
		p_strTagName = aga.pageData.strHighlightTag;
	}
	var jBc = Bub.getElementsByTagName(p_strTagName);
	var UBc = jBc.length;
	var Tpa = new RegExp("(^|\\s)" + p_strClassName + "(\\s|$)");
	var i;
	for (i = 0; i < UBc; i++) {
		if (Tpa.test(jBc[i].getAttribute("className"))) {
			Qpa.push(jBc[i]);
		} else if (Tpa.test(jBc[i].getAttribute("class"))) {
			Qpa.push(jBc[i]);
		}
	}
	return Qpa;
};
SSDOM.getContentDocument = function (ypb) {
	return ypb.contentDocument ? ypb.contentDocument : ypb.contentWindow.document;
};
SSDOM.allTextFromNodeTH = function (ZWb) {
	var bub = "";
	if (ZWb.nodeType == 3) {
		bub = ZWb.nodeValue;
	} else if (ZWb.nodeType == 1) {
		var Vpa = ZWb.firstChild;
		while (Vpa != null) {
			if (Vpa.nodeType == 3) {
				bub += Vpa.nodeValue;
			} else if (Vpa.nodeType == 1) {
				bub += SSDOM.allTextFromNodeTH(Vpa);
			}
			Vpa = Vpa.nextSibling;
		}
	}
	return bub;
};
SSDOM.getListOfHighlightableNodes = function (p_startCaret, p_endCaret) {
	var Wpa = Sga;
	Sga = false;
	var ZBc = new Array();
	try {
		var cub = p_startCaret.node;
		var ugb = p_endCaret.node;
		if (cub.nodeType != 3) {
			if (cub.nodeType == 1 && cub.tagName.toLowerCase() == "math") {
				ZBc.push(cub);
				if (cub == ugb) {
					return ZBc;
				} else {
					cub = SSDOM.getNextTextNode(cub, false, ugb);
				}
			} else if (Lfa && cub.nodeType == 1 && cub.firstChild != null && cub.firstChild.nodeType == 1 && cub.firstChild.tagName.toLowerCase() == "math") {
				ZBc.push(cub.firstChild);
				if (cub == ugb) {
					return ZBc;
				} else {
					cub = cub.firstChild;
					cub = SSDOM.getNextTextNode(cub, false, ugb);
				}
			} else if (cub.nodeType == 1 && cub.isMathJax) {
				ZBc.push(cub);
				if (cub == ugb) {
					return ZBc;
				} else {
					cub = SSDOM.getNextTextNode(cub, false, ugb);
				}
			} else if (Lfa && cub.nodeType == 1 && cub.firstChild != null && cub.firstChild.isMathJax) {
				ZBc.push(cub.firstChild);
				if (cub == ugb) {
					return ZBc;
				} else {
					cub = cub.firstChild;
					cub = SSDOM.getNextTextNode(cub, false, ugb);
				}
			} else {
				cub = SSDOM.getFirstChildTextNode(cub, false);
				if (cub == null) {
					return ZBc;
				}
			}
		}
		if (cub == ugb) {
			if (cub.nodeType == 3) {
				var bub = cub.nodeValue;
				if (bub.length > 0 && p_startCaret.offset < bub.length && p_endCaret.offset > 0 && p_endCaret.offset > p_startCaret.offset) {
					ZBc.push(cub);
				}
			}
		} else {
			if (cub.nodeType == 3) {
				var bub = cub.nodeValue;
				if (bub.length > 0 && p_startCaret.offset < bub.length) {
					ZBc.push(cub);
				}
			} else {
				if (cub.nodeType == 1 && cub.tagName.toLowerCase() == "math") {
					ZBc.push(cub);
				} else if (Lfa && cub.nodeType == 1 && cub.firstChild != null && cub.firstChild.nodeType == 1 && cub.firstChild.tagName.toLowerCase() == "math") {
					cub = cub.firstChild;
					ZBc.push(cub);
				} else if (cub.nodeType == 1 && cub.isMathJax) {
					ZBc.push(cub);
				} else if (Lfa && cub.nodeType == 1 && cub.firstChild != null && cub.firstChild.isMathJax) {
					cub = cub.firstChild;
					ZBc.push(cub);
				}
			}
			var wvb = SSDOM.getNextTextNodeNoImg(cub, false, ugb, true);
			while (wvb != null) {
				if (wvb == ugb) {
					if (ugb.nodeType == 3) {
						var bub = ugb.nodeValue;
						if (bub.length > 0 && p_endCaret.offset > 0) {
							ZBc.push(ugb);
						}
					} else {
						if (wvb.nodeType == 1 && wvb.tagName.toLowerCase() == "math") {
							ZBc.push(wvb);
						} else if (Lfa && wvb.nodeType == 1 && wvb.firstChild != null && wvb.firstChild.nodeType == 1 && wvb.firstChild.tagName.toLowerCase() == "math") {
							ZBc.push(wvb.firstChild);
						} else if (wvb.nodeType == 1 && wvb.isMathJax) {
							ZBc.push(wvb);
						} else if (Lfa && wvb.nodeType == 1 && wvb.firstChild != null && wvb.firstChild.isMathJax) {
							ZBc.push(wvb.firstChild);
						}
					}
					break;
				} else {
					ZBc.push(wvb);
				}
				wvb = SSDOM.getNextTextNodeNoImg(wvb, false, ugb, true);
			}
		}
	} catch (err) {
		eya("getListOfHighlightableNodes error:" + err.message);
	}
	Sga = Wpa;
	return ZBc;
};
SSDOM.getListOfNodes = function (p_startCaret, p_endCaret) {
	var ZBc = new Array();
	try {
		var cub = p_startCaret.node;
		var ugb = p_endCaret.node;
		if (cub == ugb) {
			ZBc.push(cub);
		} else {
			ZBc.push(cub);
			var wvb = SSDOM.getNextTextNode(cub, false, ugb, true);
			while (wvb != null) {
				ZBc.push(wvb);
				wvb = SSDOM.getNextTextNodeNoImg(wvb, false, ugb, true);
			}
		}
	} catch (err) {
		eya("getListOfNodes error:" + err.message);
	}
	return ZBc;
};
SSDOM.getRangeFromSelectionPoint = function (p_sel) {
	try {
		var bub = p_sel + "";
		bub = bub.trimTH();
		p_sel.collapseToStart();
		var cub = p_sel.anchorNode;
		var kpa = p_sel.anchorOffset;
		if (cub.nodeType != 3) {
			cub = SSDOM.getNextTextNodeNoBlank(cub, false, null);
			kpa = 0;
		} else if (kpa == cub.nodeValue.length) {
			cub = SSDOM.getNextTextNodeNoBlank(cub, false, null);
			kpa = 0;
		}
		if (cub != null && cub.nodeType == 3) {
			var RAc = cub.nodeValue.substring(kpa);
			var mpa = RAc.trimStartTH();
			while (RAc.length > mpa.length) {
				if (mpa.length == 0) {
					cub = SSDOM.getNextTextNodeNoBlank(cub, false, null);
					kpa = 0;
					if (cub == null || cub.nodeType != 3) {
						break;
					}
				} else {
					kpa += RAc.length - mpa.length;
				}
				RAc = cub.nodeValue.substring(kpa);
				mpa = RAc.trimStartTH();
			}
		}
		var range = SSDOM.getRangeObject();
		if (cub == null) {
			range.setStart(p_sel.anchorNode, p_sel.anchorOffset);
			range.setEnd(p_sel.anchorNode, p_sel.anchorOffset);
		} else {
			if (cub.nodeType != 3) {
				range = SSDOM.getNodesForText(cub, kpa, bub);
			} else {
				if ((kpa + bub.length) < cub.nodeValue.length) {
					range.setStart(cub, kpa);
					range.setEnd(cub, kpa + bub.length);
				} else {
					range = SSDOM.getNodesForText(cub, kpa, bub);
				}
			}
		}
		return range;
	} catch (ignore) {
		var range = SSDOM.getRangeObject();
		range.setStart(p_sel.anchorNode, p_sel.anchorOffset);
		range.setEnd(p_sel.anchorNode, p_sel.anchorOffset);
		return range;
	}
};
SSDOM.getNodesForText = function (fNb, Thb, LPb) {
	var range = SSDOM.getRangeObject(fNb.ownerDocument.body);
	range.setStart(fNb, Thb);
	range.setEnd(fNb, Thb);
	var UBc = 0;
	var opa = LPb.length + Thb;
	var wvb = fNb;
	while (wvb != null && UBc < opa) {
		if (wvb.nodeType == 3) {
			var bub = wvb.nodeValue;
			UBc += bub.length;
		}
		if (UBc < opa) {
			wvb = SSDOM.getNextTextNodeNoBlank(wvb, false, null);
		} else {
			var rpa = UBc - opa;
			range.setEnd(wvb, wvb.nodeValue.length - rpa);
		}
	}
	return range;
};
SSDOM.getSelectionObject = function () {
	var dSb = null;
	if (window.getSelection) {
		if (Zja != null) {
			return null;
		}
		var iSb = window.getSelection();
		var jSb = null;
		if (!iSb.isCollapsed) {
			jSb = iSb;
		} else {
			iSb = SSDOM.getFrameSelectionSFF(window);
			if (iSb.jSb) {
				jSb = iSb.jSb;
			}
		}
		if (jSb == null) {
			return null;
		}
		dSb = jSb;
	} else if (document.selection) {
		var range = document.selection.createRange();
		if (range.text.length > 0) {
			MWb = window;
			dSb = range;
		} else {
			var mBc = SSDOM.getFrameSelectionOldIE(window);
			if (mBc.dSb) {
				dSb = mBc.dSb;
			}
		}
	}
	return dSb;
};
SSDOM.getPageText = function () {
	var bub = "";
	if (SSDOM.bIEOld) {
		var range = document.body.createTextRange();
		range.expand("textedit");
		bub = range.text;
	} else {
		var range = document.createRange();
		range.setStartBefore(document.body);
		range.setEndAfter(document.body);
		bub = range.toString();
	}
	return bub;
};
SSDOM.getTextFromElement = function (Bub) {
	if (Bub.textContent) {
		return Bub.textContent;
	} else if (Bub.innerText) {
		return Bub.innerText;
	} else {
		return "";
	}
};
SSDOM.getRangeObject = function (OXb) {
	if (typeof(OXb) == 'undefined' || OXb == null) {
		OXb = document.body;
	}
	if (SSDOM.bIEOld) {
		return OXb.createTextRange();
	} else {
		var Jwb = OXb.ownerDocument;
		return Jwb.createRange();
	}
};
SSDOM.getBody = function (Bub) {
	if (Bub.document) {
		return Bub.document.body;
	} else {
		return Bub.ownerDocument.body;
	}
};
SSDOM.getWindow = function (Bub) {
	try {
		if (Bub == null) {
			return window;
		}
		if (top.frames.length === 0) {
			return window;
		} else {
			var ypa = Bub.ownerDocument.body;
			var zpa = window.document.body;
			if (ypa === zpa) {
				return window;
			}
			if (!uda) {
				var i;
				var UBc = top.frames.length;
				for (i = 0; i < UBc; i++) {
					try {
						var nhb = top.frames[i].document.body;
						if (nhb === ypa) {
							return top.frames[i];
						}
					} catch (e) {}

				}
			}
		}
	} catch (err) {
		eya("Error getWindow: " + err);
	}
	return window;
};
SSDOM.splitText = function (Bub, Ueb) {
	if (Bub == null || Bub.nodeType != 3 || Bub.parentNode == null) {
		return Bub;
	}
	var Cqa = 0;
	var Ulb = Bub.parentNode;
	var Eqa = Ulb.parentNode;
	if (Eqa != null && Ulb.tagName.toLowerCase() == "span" && Eqa.tagName.toLowerCase() == "span" && Ulb.getAttribute(daa) != null && (Eqa.getAttribute(daa) != null || Eqa.getAttribute(eaa) != null)) {
		Cqa = 2;
		if (Bub.nextSibling != null || Bub.previousSibling != null) {
			Cqa = 1;
		}
	} else if (Ulb.tagName.toLowerCase() == "span" && (Ulb.getAttribute(daa) != null || Ulb.getAttribute(eaa) != null)) {
		Cqa = 1;
	}
	if (Bub.nodeValue.length == 0 || Ueb <= 0 || Ueb >= Bub.nodeValue.length) {
		if (Cqa == 0) {
			var span = document.createElement("span");
			span.setAttribute(daa, "1");
			var Lqa = document.createElement("span");
			Lqa.setAttribute(daa, "1");
			Ulb.insertBefore(span, Bub);
			span.appendChild(Lqa);
			Lqa.appendChild(Bub);
		} else if (Cqa == 1) {
			var span = document.createElement("span");
			span.setAttribute(daa, "1");
			Ulb.insertBefore(span, Bub);
			span.appendChild(Bub);
		}
		return Bub;
	}
	var bub = Bub.nodeValue;
	var iDb = bub.substring(0, Ueb);
	var jDb = bub.substring(Ueb);
	var Lqa = document.createElement("span");
	var Mqa = document.createElement("span");
	var sDb = document.createTextNode(iDb);
	var mDb = document.createTextNode(jDb);
	Lqa.appendChild(sDb);
	Mqa.appendChild(mDb);
	Lqa.setAttribute(daa, "1");
	Mqa.setAttribute(daa, "1");
	if (Cqa == 2) {
		Eqa.insertBefore(Mqa, Ulb);
		Eqa.insertBefore(Lqa, Mqa);
		Eqa.removeChild(Ulb);
	} else if (Cqa == 1) {
		Ulb.insertBefore(Lqa, Bub);
		Ulb.insertBefore(Mqa, Bub);
		Ulb.removeChild(Bub);
	} else {
		var span = document.createElement("span");
		span.setAttribute(daa, "1");
		span.appendChild(Lqa);
		span.appendChild(Mqa);
		Ulb.insertBefore(span, Bub);
		Ulb.removeChild(Bub);
	}
	return mDb;
};
SSDOM.createObjectWithNameAttr = function (kxb, p_attrList, Brb, p_strClass, p_strNameAttr) {
	if (SSDOM.bIEOld) {
		return SSDOM.createObject("<" + kxb + " name='" + p_strNameAttr + "'>", p_attrList, Brb, p_strClass);
	} else {
		var rBc = SSDOM.createObject(kxb, p_attrList, Brb, p_strClass);
		rBc.setAttribute("name", p_strNameAttr);
		rBc.name = p_strNameAttr;
		return rBc;
	}
};
SSDOM.createObject = function (kxb, p_attrList, Brb, p_strClass) {
	var kza = document.createElement(kxb);
	if (Brb != null) {
		kza.id = Brb;
	}
	if (p_strClass != null) {
		kza.className = p_strClass;
	}
	if (p_attrList != null) {
		var UBc = p_attrList.length;
		if (SSDOM.bIEOld) {
			for (var i = 0; i < UBc; i += 2) {
				if (p_attrList[i] == "style") {
					SSDOM.setStyle(kza, p_attrList[i + 1]);
				} else {
					kza.setAttribute(p_attrList[i], p_attrList[i + 1]);
				}
			}
		} else {
			for (var i = 0; i < UBc; i += 2) {
				kza.setAttribute(p_attrList[i], p_attrList[i + 1]);
			}
		}
	}
	return kza;
};
SSDOM.createObjectFromMap = function (kxb, p_attrMap, Brb, p_strClass) {
	var kza = document.createElement(kxb);
	if (Brb != null) {
		kza.id = Brb;
	}
	if (p_strClass != null) {
		kza.className = p_strClass;
	}
	if (p_attrMap != null) {
		if (SSDOM.bIEOld) {
			for (var i in p_attrMap) {
				if (i == "style") {
					SSDOM.setStyle(kza, p_attrMap[i]);
				} else {
					kza.setAttribute(i, p_attrMap[i]);
				}
			}
		} else {
			for (var i in p_attrMap) {
				kza.setAttribute(i, p_attrMap[i]);
			}
		}
	}
	return kza;
};
SSDOM.setStyle = function (p_theObj, p_strStyles) {
	var Uqa = p_strStyles.indexOf(":");
	var Vqa = p_strStyles.indexOf(";", Uqa);
	var mrb;
	var FZb;
	var Pib = p_strStyles;
	while (Uqa > -1) {
		mrb = Pib.substring(0, Uqa);
		if (Vqa > -1) {
			FZb = Pib.substring(Uqa + 1, Vqa);
			Pib = Pib.substr(Vqa + 1);
			Uqa = Pib.indexOf(":");
			Vqa = Pib.indexOf(";", Uqa);
		} else {
			FZb = Pib.substr(Uqa + 1);
			Uqa = -1;
		}
		if (SSDOM.bIE) {
			if (mrb == " background-position") {
				mrb = " backgroundPosition";
			}
		}
		eval("p_theObj.style." + mrb + "=\"" + FZb + "\";");
	}
};
SSDOM.addContentToPage = function (p_strHTML, p_strTag) {
	var element = document.createElement(p_strTag);
	element.innerHTML = p_strHTML;
	document.body.appendChild(element);
};
SSDOM.addContentToPageWithAttr = function (p_strHTML, p_strTag, p_astrAttr) {
	var element = document.createElement(p_strTag);
	var UBc = p_astrAttr.length;
	for (i = 0; i < UBc; i += 2) {
		element.setAttribute(p_astrAttr[i], p_astrAttr[i + 1]);
	}
	element.innerHTML = p_strHTML;
	document.body.appendChild(element);
};
SSDOM.moveCaret = function (Ghb, p_nCount, p_bGoByStyle) {
	var wvb;
	var node = Ghb.node;
	var rzb = Ghb.offset;
	var cqa = Ghb.forwardBias;
	if (node.nodeType != 3) {
		return null;
	}
	while (node) {
		var UBc = node.nodeValue.length;
		if (UBc == 0) {
			if (p_nCount > 0 || (p_nCount == 0 && cqa)) {
				wvb = SSDOM.getNextTextNodeNoImg(node, p_bGoByStyle, null, true);
				if (wvb == null) {
					if (p_nCount == 0) {
						return new kqa(node, node.nodeValue.length, cqa);
					} else {
						return null;
					}
				}
				node = wvb;
				rzb = 0;
			} else {
				wvb = SSDOM.getPreviousTextNodeNoImg(node, p_bGoByStyle, null, true);
				if (wvb == null) {
					if (p_nCount == 0) {
						return new kqa(node, 0, cqa);
					} else {
						return null;
					}
				}
				node = wvb;
				rzb = node.nodeValue.length;
			}
		} else {
			rzb += p_nCount;
			if (rzb < 0 || (rzb == 0 && !cqa)) {
				p_nCount = rzb;
				wvb = SSDOM.getPreviousTextNodeNoImg(node, p_bGoByStyle, null, true);
				if (wvb == null) {
					if (p_nCount == 0) {
						return new kqa(node, 0, cqa);
					} else {
						return null;
					}
				}
				node = wvb;
				rzb = node.nodeValue.length;
			} else if (rzb > UBc || (rzb == UBc && cqa)) {
				p_nCount = rzb - UBc;
				wvb = SSDOM.getNextTextNodeNoImg(node, p_bGoByStyle, null, true);
				if (wvb == null) {
					if (p_nCount == 0) {
						return new kqa(node, node.nodeValue.length, cqa);
					} else {
						return null;
					}
				}
				node = wvb;
				rzb = 0;
			} else {
				return new kqa(node, rzb, cqa);
			}
		}
	}
	return null;
};
SSDOM.insertAfter = function (Bub, p_newNode) {
	var parent = Bub.parentNode;
	if (Bub.nextSibling != null) {
		parent.insertBefore(p_newNode, Bub.nextSibling);
	} else {
		parent.appendChild(p_newNode);
	}
};
SSDOM.mergeTextNodes = function (Bub) {
	if (Bub == null) {
		return Bub;
	}
	var hHb = Bub.parentNode;
	if (hHb != null && Bub.nodeType == 3) {
		var Jwb = Bub.ownerDocument;
		while (Bub.previousSibling != null && Bub.previousSibling.nodeType == 3) {
			var bub = Bub.previousSibling.nodeValue + Bub.nodeValue;
			var lmb = Jwb.createTextNode(bub);
			hHb.removeChild(Bub.previousSibling);
			hHb.replaceChild(lmb, Bub);
			Bub = lmb;
		}
		while (Bub.nextSibling != null && Bub.nextSibling.nodeType == 3) {
			var bub = Bub.nodeValue + Bub.nextSibling.nodeValue;
			var lmb = Jwb.createTextNode(bub);
			hHb.removeChild(Bub.nextSibling);
			hHb.replaceChild(lmb, Bub);
			Bub = lmb;
		}
	}
	return Bub;
};
function kqa(Bub, Thb, Cub) {
	this.node = Bub;
	this.offset = Thb;
	this.forwardBias = Cub;
	this.specialCase = false;
	if (SSDOM.isSpecialCase(this.node)) {
		this.specialCase = true;
		this.offset = 0;
	}
}
kqa.prototype.setSpecialCase = function (p_bSpecialCase) {
	this.specialCase = p_bSpecialCase;
};
kqa.prototype.isSpecialCase = function () {
	return this.specialCase;
};
kqa.prototype.check = function () {
	var ghb = true;
	if (this.node == null || this.node.parentNode == null) {
		ghb = false;
	} else {
		if (this.node.nodeType != 3) {
			if (this.node.nodeType == 1 && this.specialCase) {}
			else {
				ghb = false;
			}
		} else if (this.offset < 0 || this.offset > this.node.nodeValue.length) {
			ghb = false;
		}
	}
	return ghb;
};
kqa.prototype.toString = function () {
	var bub = "TH" + "Caret ";
	if (this.node != null) {
		if (this.node.nodeType == 3) {
			bub += this.node.nodeValue + " " + this.node.parentNode.tagName + " ";
		} else if (this.node.nodeType == 1) {
			bub += this.node.tagName + " ";
		}
	}
	bub += this.offset;
	return bub;
};
kqa.prototype.equals = function (Ghb) {
	if (Ghb == null) {
		return false;
	}
	return this.node == Ghb.node && this.offset == Ghb.offset && this.forwardBias == Ghb.forwardBias;
};
kqa.prototype.clone = function () {
	return new kqa(this.node, this.offset, this.forwardBias);
};
function qqa(gOb, hOb) {
	this.ygb = gOb;
	this.zgb = hOb;
}
qqa.prototype.equals = function (WQb) {
	if (WQb == null) {
		return false;
	}
	return this.ygb.equals(WQb.ygb) && this.zgb.equals(WQb.zgb);
};
qqa.prototype.toString = function () {
	return SSDOM.getTextOverCaretRange(this);
};
function uqa(vqa) {
	this.command = null;
	this.nEb = null;
	if (vqa != null) {
		var FBc = vqa.indexOf(":");
		if (FBc > -1) {
			this.command = vqa.substr(0, FBc).toLowerCase();
			this.nEb = vqa.substr(FBc + 1);
		} else {
			this.command = vqa.toLowerCase();
		}
	}
}
uqa.prototype.CMD_LIST = "list";
uqa.prototype.CMD_STOP = "stop";
uqa.prototype.CMD_STOPAFTER = "stopafter";
uqa.prototype.CMD_JUMP = "jump";
uqa.prototype.CMD_JUMPBACK = "jumpback";
uqa.prototype.CMD_VOICE = "voice";
uqa.prototype.CMD_PAGEID = "pageid";
uqa.prototype.CMD_BOOKID = "bookid";
uqa.prototype.CMD_EXEC = "exec";
uqa.prototype.CMD_EXECAFTER = "execafter";
function zqa(Bub) {
	var wqa = [];
	if (Bub != null) {
		var ipb = Bub.ownerDocument.body;
		var mBc = Bub;
		if (mBc.nodeType == 3) {
			mBc = mBc.parentNode;
		}
		if (mBc.nodeType != 1) {
			return wqa;
		}
		while (mBc != null && mBc != ipb) {
			if (mBc.getAttribute("texthelpCmd") != null) {
				var bgb = mBc.getAttribute("texthelpCmd").trimTH();
				if (bgb == uqa.prototype.CMD_LIST || bgb == (uqa.prototype.CMD_LIST + ":")) {
					var bxb = 1;
					bgb = mBc.getAttribute(("texthelpCmd" + bxb));
					while (bgb != null) {
						wqa.push(new uqa(bgb));
						++bxb;
						bgb = mBc.getAttribute(("texthelpCmd" + bxb));
					}
				} else {
					wqa.push(new uqa(bgb));
				}
				break;
			}
			mBc = mBc.parentNode;
		}
	}
	return wqa;
}
function Fra(Bub) {
	if (Bub != null) {
		var ipb = Bub.ownerDocument.body;
		var mBc = Bub;
		if (mBc.nodeType == 3) {
			mBc = mBc.parentNode;
		}
		if (mBc.nodeType != 1) {
			return null;
		}
		while (mBc != null && mBc != ipb) {
			if (mBc.getAttribute("texthelpCmd") != null) {
				return mBc;
			}
			mBc = mBc.parentNode;
		}
	}
	return null;
}
function Mra(ZNb) {
	if (Cga) {
		var uzb = ZNb.getCaretRange();
		if (uzb != null) {
			var Ira = zqa(uzb.zgb.node);
			if (Ira.length > 0) {
				var FUb = false;
				for (var i = 0; i < Ira.length; i++) {
					var cmd = Ira[i];
					if (cmd.command == cmd.CMD_STOP) {
						FUb = true;
					} else if (cmd.command == cmd.CMD_STOPAFTER) {
						ZNb.allowContinuous = false;
					} else if (cmd.command == cmd.CMD_JUMP) {
						ZNb.jumpId = cmd.nEb;
					} else if (cmd.command == cmd.CMD_VOICE) {
						ZNb.voice = cmd.nEb;
					} else if (cmd.command == cmd.CMD_PAGEID) {
						ZNb.pageId = cmd.nEb;
					} else if (cmd.command == cmd.CMD_BOOKID) {
						ZNb.bookId = cmd.nEb;
					} else if (cmd.command == cmd.CMD_EXEC) {
						try {
							eval(cmd.nEb);
						} catch (err) {
							thLogE(err.message);
						}
					} else if (cmd.command == cmd.CMD_EXECAFTER) {
						var Lra = "try{eval(\"" + cmd.nEb + "\");}catch(err){thLogE(err.message);}";
						CJb.push(Lra);
					}
				}
				if (FUb) {
					ZNb.valid = false;
				}
			}
		}
	}
}
SpeechStream.DateFilterModes = {
	DATE : 2,
	NUMBER : 1,
	NONE : 0
};
SpeechStream.dateFilter = new function () {
	var Ora = 2;
	var Pra = SpeechStream.DateFilterModes;
	var Qra = /^[ ,.?!;:\x27\x22\x28\x29\x5b\x5d\x7b\x7d\x82\x91\x92\x93\x94]+|[ ,.?!;:\x27\x22\x28\x29\x5b\x5d\x7b\x7d\x82\x91\x92\x93\x94]+$/g;
	this.setMode = function (p_nMode) {
		var Rra = parseInt(p_nMode, 10);
		if (Rra == Pra.NONE || Rra == Pra.NUMBER || Rra == Pra.DATE) {
			Ora = Rra;
		} else if (typeof(p_nMode) == "string") {
			var Sra = p_nMode.toUpperCase();
			if (Sra == "NONE") {
				Ora = Pra.NONE;
			} else if (Sra == "NUMBER") {
				Ora = Pra.NUMBER;
			} else if (Sra == "DATE") {
				Ora = Pra.DATE;
			}
		}
	};
	this.getMode = function () {
		return Ora;
	};
	this.checkDatesFromString = function (exb) {
		var Cdb = false;
		var wordList = exb.split(" ");
		Cdb = this.checkDatesFromList(wordList);
		if (Cdb) {
			var UBc = wordList.length;
			var hVb = "";
			for (i = 0; i < UBc - 1; i++) {
				hVb += wordList[i];
				hVb += " ";
			}
			hVb += wordList[UBc - 1];
			return hVb;
		} else {
			return exb;
		}
	};
	this.checkDatesFromList = function (Pyb) {
		var mwb;
		var Cdb = false;
		var UBc = Pyb.length;
		var i;
		for (i = 0; i < UBc; i++) {
			mwb = ira(Pyb[i]);
			if (mwb != Pyb[i]) {
				Pyb[i] = mwb;
				Cdb = true;
			}
		}
		return Cdb;
	};
	function bra(Usb) {
		var cjb;
		switch (Ora) {
		case 0:
			cjb = Usb;
			break;
		case 1:
			if (fra(Usb)) {
				cjb = " " + Usb.substring(0, 1) + "," + Usb.substring(1, 4) + " ";
			} else {
				cjb = Usb;
			}
			break;
		case 2:
			if (fra(Usb)) {
				var Tib = parseInt(Usb, 10);
				var era = parseInt(Usb.substring(2, 4), 10);
				if (Tib < 1000) {
					cjb = Usb;
				} else if (((Tib >= 1000 && Tib < 2000) || Tib >= 2100) && era == 0) {
					cjb = " " + Usb.substring(0, 2) + " hundred ";
				} else if (((Tib >= 1000 && Tib < 2000) || Tib >= 2100) && (era > 0 && era < 10)) {
					cjb = " " + Usb.substring(0, 2) + " oh " + Usb.substring(3, 4) + " ";
				} else if (((Tib >= 1000 && Tib < 2000) || Tib >= 2010) && era >= 10) {
					cjb = " " + Usb.substring(0, 2) + " " + Usb.substring(2, 4) + " ";
				} else if (Tib == 2000) {
					cjb = " two thousand ";
				} else if (Tib > 2000 && Tib < 2010) {
					cjb = " two thousand and " + Usb.substring(3, 4) + " ";
				} else {
					cjb = Usb;
				}
			} else {
				cjb = Usb;
			}
			break;
		default:
			cjb = Usb;
			break;
		}
		return cjb;
	};
	function fra(Usb) {
		if (Usb.length == 4) {
			if (!isNaN(Usb)) {
				if (parseInt(Usb, 10) >= 1000) {
					return true;
				}
			}
		}
		return false;
	};
	function ira(exb) {
		if (exb == null) {
			return exb;
		}
		var oCc = exb.replace(Qra, '');
		if (oCc.length == 4) {
			var hVb = bra(oCc);
			if (hVb != oCc) {
				var n = exb.indexOf(oCc);
				return exb.substring(0, n) + hVb + exb.substring(n + 4);
			}
		}
		return exb;
	};
};
var lra = -1;
var mra = 0;
var nra = 1;
var ora = 2;
var pra = 3;
var qra = 4;
var rra = 5;
var sra = 6;
var tra = 7;
var ura = 8;
function vra(fNb, Csa, mTb, Dsa) {
	this.body = fNb.ownerDocument.body;
	this.sBc = new kqa(fNb, Csa, true);
	this.tBc = new kqa(mTb, Dsa, false);
	this.Upb = ZUb(fNb, Csa);
	this.Vpb = ZUb(mTb, Dsa);
}
vra.prototype.refresh = function () {
	with (this) {
		if (sBc.check() == false || tBc.check() == false) {
			var YCc = SSDOM.getCaretPairFromDomPosition(this.body, this.Upb.path, this.Upb.offset, this.Vpb.path, this.Vpb.offset);
			var sBc = YCc.ygb;
			var tBc = YCc.zgb;
			if (sBc == null && tBc == null) {
				sBc = new kqa(document.body, 0, true);
				tBc = new kqa(document.body, 0, false);
			} else if (sBc == null || tBc == null) {
				if (sBc == null) {
					sBc = new kqa(tBc.node, tBc.offset, true);
				} else {
					tBc = new kqa(sBc.node, sBc.offset, false);
				}
			}
		}
	}
};
vra.prototype.toString = function () {
	this.refresh();
	var range = SSDOM.getRangeObject(this.body);
	range.setStart(this.sBc.node, this.sBc.offset);
	range.setEnd(this.tBc.node, this.tBc.offset);
	return range.toString();
};
vra.prototype.getStartAsRange = function () {
	var range = SSDOM.getRangeObject(this.body);
	range.setStart(this.sBc.node, this.sBc.offset);
	range.setEnd(this.sBc.node, this.sBc.offset);
	return range;
};
vra.prototype.getEndAsRange = function () {
	var range = SSDOM.getRangeObject(this.body);
	range.setStart(this.tBc.node, this.tBc.offset);
	range.setEnd(this.tBc.node, this.tBc.offset);
	return range;
};
vra.prototype.equals = function (ZNb) {
	return (this.Upb.path == ZNb.Upb.path && this.Upb.offset == ZNb.Upb.offset && this.Vpb.path == ZNb.Vpb.path && this.Vpb.offset == ZNb.Vpb.offset);
};
vra.prototype.compareRange = function (ZNb) {
	if (this.equals(ZNb)) {
		return mra;
	}
	this.refresh();
	ZNb.refresh();
	var Hsa = this.getStartAsRange();
	var Isa = this.getEndAsRange();
	var Jsa = ZNb.getStartAsRange();
	var Ksa = ZNb.getEndAsRange();
	var Lsa = Hsa.compareBoundaryPoints("START_TO_START", Jsa);
	var Msa = Hsa.compareBoundaryPoints("START_TO_START", Ksa);
	var Nsa = Isa.compareBoundaryPoints("START_TO_START", Jsa);
	var Osa = Isa.compareBoundaryPoints("START_TO_START", Ksa);
	var xeb = lra;
	if (Msa > -1) {
		xeb = nra;
	} else if (Nsa < 1) {
		xeb = ora;
	} else if (Lsa == -1) {
		if (Osa == -1) {
			xeb = sra;
		} else {
			xeb = pra;
		}
	} else if (Lsa == 0) {
		if (Osa == -1) {
			xeb = tra;
		} else if (Osa == 0) {
			xeb = mra;
		} else {
			xeb = pra;
		}
	} else {
		if (Osa == -1) {
			xeb = qra;
		} else if (Osa == 0) {
			xeb = ura;
		} else {
			xeb = rra;
		}
	}
	return xeb;
};
function THDomRefPt(Vsa, Thb) {
	this.path = Vsa;
	this.offset = Thb;
}
THDomRefPt.prototype.toString = function () {
	return "THDomRefPt " + this.path + " " + this.offset;
};
function THHoverTarget(OXb, Vsa, Wsa) {
	this.body = OXb;
	this.path = Vsa;
	this.range = Wsa;
	this.dVb = null;
	this.blockCache = false;
	this.textToSpeak = null;
	this.textToSpeakNoChanges = null;
	this.allowContinuous = true;
	this.useHighlighting = true;
	this.prepared = false;
	this.valid = true;
	this.jumpId = null;
	this.voice = null;
	this.pageId = null;
	this.bookId = null;
}
THHoverTarget.prototype.isRange = function () {
	return this.range != null;
};
THHoverTarget.prototype.isValid = function () {
	return this.valid && !Kga;
};
THHoverTarget.prototype.isOverridingGlobal = function () {
	return this.voice != null || this.pageId != null || this.bookId != null;
};
THHoverTarget.prototype.getCaretRange = function () {
	var YCc;
	if (this.isRange()) {
		YCc = SSDOM.getCaretPairFromDomPosition(this.range.body, this.range.Upb.path, this.range.Upb.offset, this.range.Vpb.path, this.range.Vpb.offset);
	} else {
		var caret = SSDOM.getCaretFromDomPosition(this.body, this.path, -1, true);
		YCc = new qqa(caret, caret);
	}
	return YCc;
};
THHoverTarget.prototype.getTextPreparedForSpeech = function () {
	this.prepareTextForSpeech();
	return this.textToSpeak;
};
THHoverTarget.prototype.prepareTextForSpeech = function () {
	if (this.prepared) {
		return;
	}
	var Zsa;
	var asa;
	if (this.isRange()) {
		this.dVb = new Array();
		var bsa = oNb(this.range.body, this.range.Upb, this.range.Vpb, this.dVb);
		this.voice = bsa.voice;
		if (bsa.YCc != null) {
			var HOb = bsa.YCc;
			this.range = TQb(HOb);
		}
		Zsa = bsa.bub;
		asa = bsa.LNb;
	} else {
		var caret = SSDOM.getCaretFromDomPosition(this.body, this.path, -1, true);
		if (caret != null && caret.node != null) {
			var esa = sUb(caret.node);
			if (esa.trimTH().length == 0) {
				Zsa = "";
				asa = "";
			} else {
				if (Fda) {
					this.voice = ZJb(caret.node);
				} else {
					this.voice = null;
				}
				var MOb = new SpeechStream.SpeechRequest();
				if (caret.node.isMathJax) {
					Zsa = esa;
					asa = esa;
				} else {
					MOb.setString(esa, SpeechStream.SpeechRequestBookmarks.OUTER);
					Zsa = MOb.getFinalText();
					asa = MOb.getText();
				}
			}
		} else {
			Zsa = "";
			asa = "";
		}
	}
	this.textToSpeak = Zsa;
	this.textToSpeakNoChanges = asa;
	this.prepared = true;
};
THHoverTarget.prototype.highlightRange = function () {
	try {
		if (this.range != null) {
			var YCc = SSDOM.getCaretPairFromDomPosition(this.range.body, this.range.Upb.path, this.range.Upb.offset, this.range.Vpb.path, this.range.Vpb.offset);
			var ygb = YCc.ygb;
			var zgb = YCc.zgb;
			if (ygb != null && zgb != null) {
				rw_setSpeechRangeImpl(ygb.node, ygb.offset, zgb.node, zgb.offset, "sp");
			} else {}

		}
	} catch (err) {
		eya("Error in THHoverTargetClass:highlightRange: " + err.message);
	}
};
THHoverTarget.prototype.unhighlightRange = function () {
	try {
		if (this.range != null) {
			var YCc = SSDOM.getCaretPairFromDomPosition(this.range.body, this.range.Upb.path, this.range.Upb.offset, this.range.Vpb.path, this.range.Vpb.offset);
			var ygb = YCc.ygb;
			var zgb = YCc.zgb;
			if (ygb != null && zgb != null) {
				rw_removeSpeechHighlight(SSDOM.getListOfHighlightableNodes(ygb, zgb), false);
			} else {}

		}
	} catch (err) {
		eya("Error in THHoverTarget:unhighlightRange: " + err.message);
	}
};
THHoverTarget.prototype.equals = function (Ypb) {
	if (Ypb == null) {
		return false;
	}
	if (this.isRange() != Ypb.isRange()) {
		return false;
	}
	if (this.isRange()) {
		return this.range.equals(Ypb.range);
	} else {
		return this.path.equalsTH(Ypb.path);
	}
};
THHoverTarget.prototype.equalsAprox = function (Ypb) {
	if (Ypb == null) {
		return false;
	}
	if (this.isRange() != Ypb.isRange()) {
		return false;
	}
	if (this.isRange()) {
		if (this.range.equals(Ypb.range)) {
			return true;
		}
		var r1 = this.getCaretRange();
		var r2 = Ypb.getCaretRange();
		r1 = Mua(r1);
		r2 = Mua(r2);
		return r1.equals(r2);
	} else {
		return this.path.equalsTH(Ypb.path);
	}
};
THHoverTarget.prototype.toString = function () {
	var bub = "THHoverTarget ";
	if (this.path != null) {
		bub += "path=" + this.path;
	} else if (this.range != null) {
		bub += this.range.toString();
	}
	return bub;
};
SpeechStream.pronunciation = new function () {
	this.NONE = 0;
	this.SERVER_PRONUNCIATION = 1;
	this.CLIENT_PRONUNCIATION_FOR_OFFLINE_CACHE = 2;
	this.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER = 3;
	this.mode = this.SERVER_PRONUNCIATION;
	this.checkPronunciation = function () {
		return (this.mode != this.NONE && SpeechStream.cacheMode.mode != SpeechStream.cacheMode.CACHE_ONLY && this.mode != this.SERVER_PRONUNCIATION);
	};
	this.fetchData = function () {
		if (this.mode == this.NONE || SpeechStream.cacheMode.mode == SpeechStream.cacheMode.CACHE_ONLY) {
			return false;
		}
		if (this.mode != this.SERVER_PRONUNCIATION) {
			return true;
		} else {
			if ((jca & pronCreate_icon) == pronCreate_icon || (jca & pronEdit_icon) == pronEdit_icon) {
				return true;
			}
		}
		return false;
	};
	this.encodeData = function () {
		return (this.mode != this.NONE && this.mode != this.SERVER_PRONUNCIATION);
	};
	this.setPronunciation = function (p_nMode) {
		if (p_nMode == this.NONE || p_nMode == this.SERVER_PRONUNCIATION || p_nMode == this.CLIENT_PRONUNCIATION_FOR_OFFLINE_CACHE || p_nMode == this.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER) {
			this.mode = p_nMode;
		}
	};
};
SpeechStream.Dictionary = function () {
	function nsa(Eta) {
		return (this["+" + Eta]);
	}
	function qsa(Eta) {
		var vsa = "+" + Eta;
		this[vsa] = null;
		delete this[vsa];
		var FBc = this.Keys$__.indexOf(Eta);
		if (FBc > -1) {
			this.Keys$__.splice(FBc, 1);
		}
		FBc = this.AllPageKeys$__.indexOf(Eta);
		if (FBc > -1) {
			this.AllPageKeys$__.splice(FBc, 1);
		}
		if (Eta.indexOf(" ") > -1) {
			var FBc = Eta.indexOf(" ");
			var AQb = Eta.substring(0, FBc);
			FBc = this.MultiwordStart$__.indexOf(AQb);
			if (FBc > -1) {
				this.MultiwordStart$__.splice(FBc, 1);
			}
		}
	}
	function wsa(Eta, lwa, zsa) {
		var vsa = "+" + Eta;
		if (Eta.substr(Eta.length - 3) == "$__") {
			Eta = Eta.substr(0, Eta.length - 1);
		}
		if (Eta != null && Eta.length > 0 && lwa != null && lwa.length > 0) {
			var Ata = false;
			if (this[vsa] != null) {
				Ata = true;
				if (zsa && !this.isAllPage$__(Eta)) {
					return;
				}
			}
			this[vsa] = lwa;
			if (this.Keys$__.indexOf(Eta) == -1) {
				this.Keys$__[this.Keys$__.length] = Eta;
			}
			if (zsa) {
				if (this.AllPageKeys$__.indexOf(Eta) == -1) {
					this.AllPageKeys$__[this.AllPageKeys$__.length] = Eta;
				}
			} else {
				if (this.AllPageKeys$__.indexOf(Eta) > -1) {
					this.AllPageKeys$__.splice(this.AllPageKeys$__.indexOf(Eta), 1);
				}
			}
			if (!Ata) {
				if (Eta.indexOf(" ") > -1) {
					var FBc = Eta.indexOf(" ");
					var AQb = Eta.substring(0, FBc);
					this.MultiwordStart$__.push(AQb);
				}
			}
		}
	}
	function Dta(Eta) {
		return this.AllPageKeys$__.indexOf(Eta) > -1;
	}
	this.add$__ = wsa;
	this.get$__ = nsa;
	this.remove$__ = qsa;
	this.isAllPage$__ = Dta;
	this.Keys$__ = new Array();
	this.AllPageKeys$__ = new Array();
	this.MultiwordStart$__ = new Array();
};
SpeechStream.Dictionary.prototype.deleteAll = function () {
	if (typeof(this.Keys$__) != "undefined") {
		var mwb;
		var i;
		for (i = 0; i < this.Keys$__.length; i++) {
			mwb = this.Keys$__[i];
			this["+" + mwb] = null;
			delete this["+" + mwb];
		}
		this.Keys$__ = new Array();
		this.AllPageKeys$__ = new Array();
		this.MultiwordStart$__ = new Array();
	}
};
function THRange(OXb, Ita, Jta) {
	this.body = OXb;
	this.Upb = Ita;
	this.Vpb = Jta;
}
THRange.prototype.equals = function (ZNb) {
	return (this.body == ZNb.body && this.Upb.path == ZNb.Upb.path && this.Upb.offset == ZNb.Upb.offset && this.Vpb.path == ZNb.Vpb.path && this.Vpb.offset == ZNb.Vpb.offset);
};
THRange.prototype.toString = function () {
	var range = this.getAsRange();
	if (range != null) {
		if (Ufa) {
			return this.getAsRange().text;
		} else {
			return this.getAsRange().toString();
		}
	} else {
		return "";
	}
};
THRange.prototype.getAsRange = function () {
	var range = null;
	if (Ufa) {
		range = rw_getAsTextRange(this.body, this.Upb.path, this.Upb.offset, this.Vpb.path, this.Vpb.offset);
	} else {
		range = SSDOM.getRangeObject(this.body);
		var YCc = SSDOM.getCaretPairFromDomPosition(this.body, this.Upb.path, this.Upb.offset, this.Vpb.path, this.Vpb.offset);
		var sBc = YCc.ygb;
		var tBc = YCc.zgb;
		if (sBc != null && tBc != null) {
			range.setStart(sBc.node, sBc.offset);
			range.setEnd(tBc.node, tBc.offset);
		} else {
			range = null;
			eya("Error in THRange:getAsRange: Failed to get the start or end caret.");
		}
	}
	return range;
};
THRange.prototype.clone = function () {
	return new THRange(this.body, this.Upb, this.Vpb);
};
SpeechStream.SpeechRequestBookmarks = {
	NONE : 0,
	OUTER : 1,
	ALL : 2
};
SpeechStream.SpeechRequest = function () {
	this.m_strText = null;
	this.m_strFinalText = null;
	this.m_bChanged = false;
	this.m_wordList = null;
};
SpeechStream.SpeechRequest.prototype.setString = function (exb, p_nBookmarks) {
	var wordList = exb.split(" ");
	var i;
	var UBc = wordList.length - 1;
	for (i = 0; i < UBc; i++) {
		wordList[i] = wordList[i] + " ";
	}
	this.setWordList(wordList, p_nBookmarks);
};
SpeechStream.SpeechRequest.prototype.setWordList = function (Pyb, p_nBookmarks) {
	if (Pyb.length == 1 && Pyb[0].indexOf("math:") == 0) {
		var FBc = Pyb[0].indexOf(";");
		var Qta = Pyb[0].substring(0, FBc + 1) + oLb + "0" + pLb + Pyb[0].substr(FBc + 1) + oLb + "1" + pLb;
		this.m_strText = Qta;
		this.m_strFinalText = Qta;
		return;
	}
	var Rta = null;
	if (typeof(eba_build_cache_for_external_use) == "boolean" && eba_build_cache_for_external_use) {
		var mBc = "";
		var UBc = Pyb.length;
		for (i = 0; i < UBc; i++) {
			mBc += Pyb[i];
		}
		Rta = mBc;
	}
	var Cdb = false;
	if (qfa) {
		if (lVb(Pyb)) {
			Cdb = true;
		}
	}
	if (SpeechStream.dateFilter.getMode() != SpeechStream.DateFilterModes.NONE) {
		if (SpeechStream.dateFilter.checkDatesFromList(Pyb)) {
			Cdb = true;
		}
	}
	this.m_strText = this.buildString(Pyb, p_nBookmarks);
	var inb;
	var i,
	FBc,
	vBc,
	wBc,
	bSub;
	var UBc = Pyb.length;
	for (i = 0; i < UBc; i++) {
		var mBc = Pyb[i];
		FBc = mBc.indexOf("-");
		if (FBc > 0) {
			vBc = mBc.substr(0, FBc);
			wBc = mBc.substr(FBc + 1);
			if (wBc.length == 0 || wBc.indexOf("-") > -1) {
				break;
			}
			if (!isNaN(vBc) && !isNaN(wBc)) {
				inb = [vBc, "to", wBc];
				SpeechStream.dateFilter.checkDatesFromList(inb);
				Pyb[i] = inb.join(" ");
				Cdb = true;
				bSub = true;
			}
		}
	}
	if (bSub) {
		this.m_strFinalText = this.buildString(Pyb, p_nBookmarks);
	}
	if (SpeechStream.pronunciation.checkPronunciation()) {
		if (KYb(Pyb)) {
			Cdb = true;
			this.m_strFinalText = this.buildString(Pyb, p_nBookmarks);
		}
	}
	this.m_bChanged = Cdb;
	if (this.m_strFinalText == null) {
		this.m_strFinalText = this.m_strText;
	}
	if (Rta != null) {
		this.m_strText = Rta;
	}
};
SpeechStream.SpeechRequest.prototype.buildString = function (Pyb, p_nBookmarks) {
	if (Pyb.length == 0) {
		return "";
	}
	var Znb = (p_nBookmarks == SpeechStream.SpeechRequestBookmarks.ALL);
	var Zta = (p_nBookmarks == SpeechStream.SpeechRequestBookmarks.OUTER);
	var mBc = "";
	var UBc = Pyb.length;
	if (Zta) {
		mBc += oLb + "0" + pLb;
	}
	for (i = 0; i < UBc; i++) {
		if (Znb) {
			mBc += oLb + i + pLb;
		}
		if (SpeechStream.pauseHandler.hasPauseKey(Pyb[i])) {
			var mLb = TOb(Pyb[i]);
			var dta = SpeechStream.pauseHandler.decodedPause(mLb);
			mBc += dta.join("");
		} else {
			mBc += TOb(Pyb[i]);
		}
	}
	if (Zta || Znb) {
		mBc += oLb + UBc + pLb;
	}
	return mBc;
};
SpeechStream.SpeechRequest.prototype.isChanged = function () {
	return this.m_bChanged;
};
SpeechStream.SpeechRequest.prototype.getFinalText = function () {
	return this.m_strFinalText;
};
SpeechStream.SpeechRequest.prototype.getText = function () {
	return this.m_strText;
};
var eta = null;
var fta = null;
function $rw_event_click(event, i) {
	return $rw_event_hover(event, i);
}
function $rw_isTouchDevice() {
	return bfa;
}
function $rw_event_hover(event, i) {
	if (sda && Yfa) {
		$rw_tagSentences();
	}
	if (!kea) {
		return;
	}
	if (!Aea) {
		if (Mda) {
			throw "The page has not fully loaded, click and speak is not available yet.";
		} else {
			Bza("The page has not fully loaded, click and speak is not available yet.");
		}
		return;
	}
	$g_bMouseSpeech = !$g_bMouseSpeech;
	var rmb = SpeechStream.EnumIconParameter;
	if (i > -1) {
		g_icons[i][rmb.ICON_TOGGLE_STATE] = $g_bMouseSpeech;
		if (bfa && !$g_bMouseSpeech) {
			zza(g_icons[i][rmb.ICON_NAME], "flat", g_icons[i][rmb.ICON_OFFSET], true);
		} else {
			zza(g_icons[i][rmb.ICON_NAME], "toggleOn", g_icons[i][rmb.ICON_OFFSET], true);
		}
	}
	if (nfa && $g_bMouseSpeech) {
		if (wda > -1 && typeof($rw_event_sticky) != "undefined") {
			$rw_event_sticky(event, wda);
			var hta = wea;
			wea = 0;
			hIb("sticky" + "", wda, true);
			wea = hta;
		}
	}
	if (!$g_bMouseSpeech) {
		if (Qga) {
			window.postMessage({
				type : "1757FROM_PAGERW4G",
				command : "saveOnClickHover",
				settings : {
					'clickHoverOn' : false
				}
			}, '*');
		}
		xbb.enableTouchEvents(false);
		$rw_event_stop();
		pEb(false);
		rEb(false);
		xbb.enableTouchEvents(false);
	} else {
		if (Qga) {
			window.postMessage({
				type : "1757FROM_PAGERW4G",
				command : "saveOnClickHover",
				settings : {
					'clickHoverOn' : true
				}
			}, '*');
		}
		pEb(true);
		xbb.enableTouchEvents(true);
	}
}
function $rw_isPaused() {
	return (kea && xbb.getConnector && $rw_isSpeaking() && xbb.getConnector() != null && xbb.getConnector().isPaused());
}
function $rw_event_play() {
	if (kea) {
		try {
			if (g_speakableTextAreaTarget != null) {
				if ($rw_isPaused()) {
					$rw_event_pause();
					return;
				}
				if (g_nSpeakableTextAreaTimerId != 0) {
					clearTimeout(g_nSpeakableTextAreaTimerId);
					g_nSpeakableTextAreaTimerId = 0;
					if (g_speakableTextAreaTarget == null) {
						$rw_event_play();
						return;
					}
				}
				var ita = g_speakableTextAreaTarget;
				ita.focus();
				g_speakableTextAreaTarget = null;
				$rw_event_stop();
				rw_speakHoverTarget(new THHoverTarget(ita.ownerDocument.body, SSDOM.getPositionInDom(ita), null));
			} else {
				if ($rw_isPaused()) {
					$rw_event_pause();
					return;
				}
				var jta = uSb();
				if (jta != null && jta.range != null) {
					var tcb = jta.range;
					if (tcb instanceof String) {
						$rw_event_stop();
						rw_speakHoverTarget(tcb);
					} else {
						if (Ufa) {
							var range = SSDOM.getSelectionObject();
							if (range != null) {
								range.collapse();
								range.select();
							}
						} else {
							var range = SSDOM.getSelectionObject();
							if (range != null) {
								range.collapseToStart();
							}
						}
						var target = new THHoverTarget(null, null, tcb);
						var ueb = tcb.clone();
						var bub = target.getTextPreparedForSpeech();
						var nta = false;
						if (!ueb.equals(target.range)) {
							nta = true;
						}
						if (bub == null || bub.length == 0) {
							return;
						}
						$rw_event_stop();
						var ota;
						if (Mea) {
							if (nta) {
								ota = zta(new THHoverTarget(null, null, ueb), true);
							} else {
								ota = zta(target, true);
							}
							if (ota == null) {
								Pea = target;
							}
						} else {
							ota = target;
						}
						if (ota != null) {
							target = ota;
							rw_speakHoverTarget(target);
						} else {
							target.blockCache = !Tda;
							rw_speakHoverTarget(target);
						}
					}
				} else {
					if (aca != null || arguments[0]) {
						$rw_event_stop();
						if (Tga == null || bca) {
							bca = false;
							$rw_speakFirstSentence(arguments[0]);
						} else {
							$rw_speakCurrentSentence();
						}
					}
				}
			}
		} catch (err) {
			thLogE(err);
		}
	}
}
function zta(ZNb, Bua) {
	if (ZNb == null) {
		return null;
	}
	Qea = null;
	var YCc = ZNb.getCaretRange();
	YCc = Mua(YCc);
	var lPb = Bua ? SSDOM.getSentenceFromPointByLang(YCc.ygb) : SSDOM.getSentenceFromPoint(YCc.ygb);
	if (lPb == null) {
		return null;
	}
	if (lPb.zgb.node == YCc.zgb.node) {
		if (lPb.zgb.offset == YCc.zgb.offset) {
			return null;
		}
		var wvb = lPb.zgb.node;
		if (wvb.nodeType == 3) {
			var sta = wvb.nodeValue.substring(lPb.zgb.offset, YCc.zgb.offset).trimTH();
			if (sta.length == 0) {
				return null;
			}
		}
	}
	var tta = Bua ? SSDOM.getSentenceFromPointByLang(YCc.zgb) : SSDOM.getSentenceFromPoint(YCc.zgb);
	if (tta == null) {
		return null;
	}
	if (lPb.equals(tta)) {
		return null;
	} else {
		var uta;
		if (!Nea) {
			uta = lPb;
			lPb = new qqa(YCc.ygb, lPb.zgb);
		}
		if (!wya(lPb)) {
			lPb = SSDOM.getNextSentence(lPb, tta.zgb.node);
			if (lPb == null) {
				return null;
			}
			if (lPb.equals(tta)) {
				return null;
			}
		}
		if (!wya(tta)) {
			tta = SSDOM.getPreviousSentence(tta, null);
			if (tta == null) {
				return null;
			}
			if (lPb.equals(tta)) {
				return null;
			}
			if (uta) {
				if (uta.equals(tta)) {
					return null;
				}
			}
		}
		if (!Nea) {
			var vta = new qqa(tta.ygb, YCc.zgb);
			Qea = new THHoverTarget(null, null, TQb(vta));
		}
		var wta;
		var xta;
		var tcb = TQb(lPb);
		wta = new THHoverTarget(null, null, tcb);
		tta = new qqa(Mua(tta).ygb, tta.zgb);
		tcb = TQb(tta);
		xta = new THHoverTarget(null, null, tcb);
		Pea = xta;
		return wta;
	}
}
function Mua(WQb) {
	var Cdb = false;
	var aOb;
	var fhb;
	var hUb;
	var chb;
	try {
		var wvb;
		var Iua;
		if (!WQb.zgb.isSpecialCase()) {
			aOb = WQb.zgb.node;
			fhb = WQb.zgb.offset;
			var Jua = aOb.nodeValue;
			while (true) {
				if (fhb == 0) {
					wvb = SSDOM.getPreviousTextNode(aOb, false, hUb);
					if (wvb == null) {
						break;
					}
					if (wvb.nodeType == 1) {
						aOb = wvb;
						fhb = 0;
						Cdb = true;
						break;
					}
					aOb = wvb;
					Jua = aOb.nodeValue;
					fhb = Jua.length;
					Cdb = true;
				}
				if (fhb > 0) {
					Iua = Jua.charAt(fhb - 1);
					if (Zya(Iua)) {
						--fhb;
						Cdb = true;
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		if (!WQb.ygb.isSpecialCase()) {
			hUb = WQb.ygb.node;
			chb = WQb.ygb.offset;
			var Kua = hUb.nodeValue;
			var UBc = Kua.length;
			while (true) {
				if (chb == UBc) {
					wvb = SSDOM.getNextTextNode(hUb, false, aOb);
					if (wvb == null) {
						break;
					}
					if (wvb.nodeType == 1) {
						hUb = wvb;
						chb = 0;
						UBc = 0;
						Cdb = true;
						break;
					}
					hUb = wvb;
					Kua = hUb.nodeValue;
					chb = 0;
					UBc = Kua.length;
					Cdb = true;
				}
				if (chb < UBc) {
					Iua = Kua.charAt(chb);
					if (Zya(Iua)) {
						++chb;
						Cdb = true;
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
	} catch (err) {
		thLogE(err);
	}
	if (!Cdb) {
		return WQb;
	} else {
		return (new qqa(new kqa(hUb, chb, true), new kqa(aOb, fhb, false)));
	}
}
function $rw_event_funplay() {
	$rw_event_play();
}
var Oua = null;
function $rw_speakText(exb) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	$rw_event_stop();
	exb = SpeechStream.pauseHandler.encodePauseFromString(exb);
	var MOb = new SpeechStream.SpeechRequest();
	MOb.setString(exb, SpeechStream.SpeechRequestBookmarks.NONE);
	var oCc = MOb.getText();
	var Wua = MOb.getFinalText();
	Xua(Wua, !Uda, oCc);
}
function $rw_speak(exb, p_bNoCache, p_bFilter) {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	$rw_event_stop();
	if (p_bFilter) {
		var MOb = new SpeechStream.SpeechRequest();
		MOb.setString(exb, SpeechStream.SpeechRequestBookmarks.NONE);
		var oCc = MOb.getText();
		var Wua = MOb.getFinalText();
		Xua(Wua, p_bNoCache, oCc);
	} else {
		Xua(exb, p_bNoCache, exb);
	}
}
function Xua(exb, Zua, aua) {
	if (!Hxa(exb)) {
		return;
	}
	if (typeof(eba_no_flash) == "boolean" && eba_no_flash) {
		rw_sendSocketMessage("THStart" + exb + "THEnd");
		return;
	}
	if (exb == null && exb.length == 0) {
		$rw_doSelection(-1);
		return;
	}
	if (!kea) {
		$rw_doSelection(-1);
		return;
	}
	try {
		exb = SpeechStream.stringAssist.filterNbsp(exb);
		if (typeof(aua) == "undefinded" || aua == null) {
			aua = exb;
		} else {
			aua = SpeechStream.stringAssist.filterNbsp(aua);
		}
		var flash = xbb.getConnector();
		if (flash != null) {
			if (rLb && rLb.isRange()) {
				pEb(true);
				rEb(true);
				var ibb = exb.indexOf("math:") == 0;
				if (!ibb) {
					$rw_doSelection(0);
				}
			}
			if (Zua && Nda && Oda) {
				flash.startSpeechFromBackup(exb, !ofa);
				Jxa();
			} else {
				if (SpeechStream.cacheMode.mode == SpeechStream.cacheMode.CACHE_ONLY) {
					var wPb = IQb();
					var mua = IRb(aua);
					if (Lga) {
						wPb = qua(wPb);
						mua = tua(mua);
					}
					var hob;
					if (Pda) {
						var CQb = JQb(mua);
						wPb = wPb + "/" + CQb;
						hob = wPb + "/" + mua;
					} else {
						hob = wPb + "/" + mua;
					}
					flash.startSpeechFromCache(hob, exb, false);
					Jxa();
				} else if (SpeechStream.cacheMode.mode == SpeechStream.cacheMode.CACHE_WITH_LIVE_SERVER) {
					var wPb = IQb();
					var mua;
					if (SpeechStream.pronunciation.mode == SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER) {
						mua = IRb(exb);
					} else {
						mua = IRb(aua);
					}
					if (Lga) {
						wPb = qua(wPb);
						mua = tua(mua);
					}
					var hob;
					if (Pda) {
						var CQb = JQb(mua);
						wPb = wPb + "/" + CQb;
						hob = wPb + "/" + mua;
					} else {
						hob = wPb + "/" + mua;
					}
					flash.startSpeechFromCacheWithGen(hob, exb, wPb, mua, !ofa);
					Jxa();
				} else if (SpeechStream.cacheMode.mode == SpeechStream.cacheMode.CACHE_BUILDING_MODE) {
					var wPb = IQb();
					var mua;
					if (SpeechStream.pronunciation.mode == SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER) {
						mua = IRb(exb);
					} else {
						mua = IRb(aua);
					}
					if (Lga) {
						wPb = qua(wPb);
						mua = tua(mua);
					}
					var hob;
					if (Pda) {
						var CQb = JQb(mua);
						wPb = wPb + "/" + CQb;
						hob = wPb + "/" + mua;
					} else {
						hob = wPb + "/" + mua;
					}
					var pua = Dza(true) + SpeechStream.cacheMode.getLiveServer() + "/";
					flash.startSpeechGenerateCache(hob, exb, wPb, mua, !ofa, pua);
					Jxa();
				} else {
					flash.startSpeech(exb, !ofa);
					Jxa();
				}
			}
		}
	} catch (err) {
		thLogE(err);
	}
}
function qua(rua) {
	if (Mga != null) {
		return Mga;
	} else {
		return rua;
	}
}
function tua(uua) {
	if (Nga != null) {
		return Nga;
	} else {
		return uua;
	}
}
function wua(swa, exb) {
	if (exb.indexOf("\n") > -1) {
		var Nbb = Dza(true) + nca + "/SpeechCache/" + swa + ".xml";
		var Ava = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
		Ava.open("HEAD", Nbb, false);
		Ava.send();
		return (Ava.status == 404);
	}
	return false;
}
function Cva(exb, XNb) {
	if (exb == null && exb.length == 0) {
		$rw_doSelection(-1);
		return;
	}
	if (!kea) {
		$rw_doSelection(-1);
		return;
	}
	try {
		var flash = xbb.getConnector();
		if (flash != null) {
			pEb(true);
			$rw_doSelection(0);
			flash.startSpeechFromFile(exb, XNb, !ofa);
			Jxa();
		}
	} catch (err) {
		thLogE(err);
	}
}
function Iva(exb) {
	if (exb == null && exb.length == 0) {
		$rw_doSelection(-1);
		return;
	}
	if (!kea) {
		$rw_doSelection(-1);
		return;
	}
	try {
		var flash = xbb.getConnector();
		if (flash != null) {
			pEb(true);
			$rw_doSelection(0);
			flash.startHighlighting(exb);
		}
	} catch (err) {
		thLogE(err);
	}
}
var g_nPauseEventTimeout = 0;
function $rw_event_pause() {
	if (g_nPauseEventTimeout > 0) {
		clearTimeout(g_nPauseEventTimeout);
		g_nPauseEventTimeout = 0;
	}
	if ($rw_isSpeaking()) {
		var EKb = (new Date).getTime();
		if ((EKb - ega) < Aha) {
			return;
		}
		ega = EKb;
		var flash = xbb.getConnector();
		if (flash != null) {
			if (!flash.canPause()) {
				g_nPauseEventTimeout = setTimeout("g_nPauseEventTimeout=0;$rw_event_pause();", 100);
				return;
			}
			if (flash.isPaused()) {
				if (!flash.resume()) {
					g_nPauseEventTimeout = setTimeout("g_nPauseEventTimeout=0;$rw_event_pause();", 100);
				}
			} else {
				if (!flash.pause()) {
					g_nPauseEventTimeout = setTimeout("g_nPauseEventTimeout=0;$rw_event_pause();", 100);
				}
			}
		}
	} else {
		if (hEb) {
			var EKb = (new Date).getTime();
			if ((EKb - ega) < Aha) {
				return;
			}
			ega = EKb;
			g_nPauseEventTimeout = setTimeout("g_nPauseEventTimeout=0;$rw_event_pause();", 100);
		}
	}
}
function $rw_event_funstop() {
	$rw_event_stop();
}
function $rw_event_stop() {
	if (!kea) {
		return;
	}
	try {
		if (aga.bubbleData.bBubbleMode) {
			SpeechStream.bubbleSpeech.hideStopBubble();
		}
		EJb();
		Fea = null;
		Pea = null;
		if (vMb > 0) {
			clearTimeout(vMb);
			vMb = 0;
		}
		tMb.length = 0;
		if (nMb) {
			setTimeout($rw_event_stop, 100);
			return;
		}
		if (sLb > 0) {
			clearTimeout(sLb);
			sLb = 0;
		}
		if (tLb > 0) {
			clearTimeout(tLb);
			tLb = 0;
		}
		pEb(false);
		rEb(false);
		var flash = xbb.getConnector();
		if (flash != null) {
			flash.stopSpeechAlt();
		}
		qLb = null;
	} catch (err) {
		thLogE(err);
	}
}
function $rw_event_stop_limited() {
	if (!kea) {
		return;
	}
	try {
		EJb();
		Fea = null;
		pEb(false);
		rEb(false);
		var flash = xbb.getConnector();
		if (flash != null) {
			flash.stopSpeechAlt();
		}
	} catch (err) {
		thLogE(err);
	}
}
function $rw_event_mp3() {
	try {
		var Pva = null;
		if (qda != null) {
			var target = document.getElementById(qda);
			if (target != null) {
				Pva = SSDOM.getAllTextFromNode(target);
			}
		} else {
			Pva = gva();
		}
		if (Pva != null) {
			Pva = Pva.trimTH();
			if (Pva.length > 0) {
				if (Pva.length > rda) {
					alert("Too much text was selected for making an MP3 file, there is a " + (rda / 1024) + "k limit.");
					return;
				}
				var flash = xbb.getConnector();
				if (flash != null) {
					var bub = "<br>The MP3 file for the text is being generated, <br> " + "this could take some time depending on the amount of text selected.<br><p align=\"center\">" + "<img alt=\"loading\" src=\"" + aga.paths.strFileLoc + "rwimgs/request-processing.gif\"> </p>";
					CLb(Oba, bub);
					HLb(true, Oba);
					flash.getMP3File(Pva, !ofa);
				}
			}
		}
	} catch (err) {
		thLogE(err);
	}
}
function $rw_mp3reply(bub) {
	try {
		if (typeof(eba_mp3_callback) != "undefined") {
			HLb(false, Oba);
			eval("" + eba_mp3_callback + "('" + bub + "')");
		} else {
			if (bub.length > 0) {
				var qAc = "Save Target As...";
				if (!Lfa) {
					if (Xfa) {
						qAc = "Save Link As...";
					} else if (Yfa) {
						qAc = "Download Linked File";
					} else {
						qAc = "Save Link As...";
					}
				}
				bub = "<br>Right click on the link below and select " + "'" + qAc + "' to save the mp3 file to your hard drive.<p></p>" + "<a type='application/octet-stream' href=\"" + bub + "\">Download speech mp3 file.</a><p></p>";
				CLb(Oba, bub);
				HLb(true, Oba);
			}
		}
	} catch (err) {
		thLogE(err);
	}
}
var Tva = "setWarning";
function setWarning() {
	$rw_lexiSubmitEvent();
}
function $rw_lexiSubmitEvent() {
	vea = true;
}
function Vva() {
	if (Ufa) {
		var range = document.selection.createRange();
		if (range.text.length > 0) {
			return true;
		}
		if (!uda && top.frames.length > 0) {
			var i = 0;
			var UBc = top.frames.length;
			for (i = 0; i < UBc; i++) {
				try {
					var jeb = top.frames[i];
					range = jeb.document.selection.createRange();
					if (range.text != null && range.text.length > 0) {
						return true;
					}
				} catch (e) {}

			}
		}
	} else {
		if (Zja != null) {
			return true;
		}
		if (window.getSelection() != null && !window.getSelection().isCollapsed) {
			return true;
		}
		if (!uda && top.frames.length > 0) {
			var i = 0;
			var UBc = top.frames.length;
			for (i = 0; i < UBc; i++) {
				try {
					if (top.frames[i].getSelection() != null && !top.frames[i].getSelection().isCollapsed) {
						return true;
					}
				} catch (e) {}

			}
		}
	}
	return false;
}
function Zva() {
	if (Ufa) {
		var range = document.selection.createRange();
		if (range.text == null || range.text.length == 0) {
			if (!uda && top.frames.length > 0) {
				var i = 0;
				var UBc = top.frames.length;
				for (i = 0; i < UBc; i++) {
					try {
						var jeb = top.frames[i];
						range = jeb.document.selection.createRange();
						if (range.text != null && range.text.length > 0) {
							return range.parentElement();
						}
					} catch (e) {}

				}
			}
		} else {
			return range.parentElement();
		}
	} else {
		if (Zja != null) {
			return Zja.startContainer;
		}
		var iSb = window.getSelection();
		if (iSb.isCollapsed || iSb.toString().trimTH().length == 0) {
			if (WEb && WEb.selectionStart != WEb.selectionEnd) {
				return WEb;
			}
			if (!uda && top.frames.length > 0) {
				var i = 0;
				var UBc = top.frames.length;
				for (i = 0; i < UBc; i++) {
					try {
						var OCc = top.frames[i].getSelection();
						if (OCc != null && !OCc.isCollapsed) {
							return OCc;
						}
					} catch (e) {}

				}
			}
		} else {
			return iSb.focusNode;
		}
	}
	return null;
}
function gva() {
	var bub = '';
	if (Ufa) {
		var range = document.selection.createRange();
		if (range.text == null || range.text.length == 0) {
			if (!uda && top.frames.length > 0) {
				var i = 0;
				var UBc = top.frames.length;
				for (i = 0; i < UBc; i++) {
					try {
						var jeb = top.frames[i];
						range = jeb.document.selection.createRange();
						if (range.text != null && range.text.length > 0) {
							break;
						}
					} catch (e) {}

				}
			}
		}
		bub = range.text;
		var ueb = range.duplicate();
		ueb.collapse();
		var qlb = ueb.parentElement();
		if (SSDOM.isIgnored(qlb)) {
			bub = "";
		}
	} else {
		if (Zja != null) {
			return Zja.toString();
		}
		var iSb = window.getSelection();
		if (iSb.isCollapsed || iSb.toString().trimTH().length == 0) {
			if (WEb && WEb.selectionStart != WEb.selectionEnd) {
				return WEb.value.substring(WEb.selectionStart, WEb.selectionEnd);
			}
			if (!uda && top.frames.length > 0) {
				var i = 0;
				var UBc = top.frames.length;
				for (i = 0; i < UBc; i++) {
					try {
						var OCc = top.frames[i].getSelection();
						if (OCc != null && !OCc.isCollapsed) {
							iSb = OCc;
							break;
						}
					} catch (e) {}

				}
			}
		}
		if (iSb.anchorNode) {
			if (SSDOM.isIgnored(iSb.anchorNode)) {
				return "";
			}
		}
		if (iSb.focusNode && iSb.focusNode.id) {
			if (iSb.focusNode.id == "flashcontent") {
				return "";
			}
		}
		if (!iSb.isCollapsed) {
			bub = iSb.toString();
		} else {
			bub = "";
		}
	}
	return bub;
}
function ova() {
	pEb(false);
	rEb(false);
}
var pva = -1;
var qva = null;
function $rw_doSelection(nIb) {
	if (qva) {
		SpeechStream.mathJaxHighlighter.clearHighlights(qva);
		qva = null;
	}
	if (nIb < 0) {
		if (nIb == -2 && $rw_isSpeaking()) {
			$rw_speechCompleteCallback("Stopped");
		} else if (nIb == -3) {
			$rw_speechErrorCallback();
		}
		mEb = setTimeout(ova, 200);
		tEb(false);
		if (aga.bubbleData.bBubbleMode) {
			SpeechStream.bubbleSpeech.hideStopBubble();
		}
	} else {
		tEb(true);
	}
	if (rLb && rLb.isRange()) {
		if (nIb != pva) {
			if (nIb == -1 || nIb == -2 || nIb == -3) {
				if (pva > -1 && rLb.dVb != null) {
					try {
						var dVb = rLb.dVb;
						var UBc = dVb.length;
						if (pva < UBc) {
							var tcb = dVb[pva].range;
							var YCc = SSDOM.getCaretPairFromDomPosition(tcb.body, tcb.Upb.path, tcb.Upb.offset, tcb.Vpb.path, tcb.Vpb.offset);
							var ygb = YCc.ygb;
							var zgb = YCc.zgb;
							if (ygb != null && zgb != null) {
								rw_removeSpeechHighlight(SSDOM.getListOfHighlightableNodes(ygb, zgb), true);
							} else {
								eya("Cannot determine valid range to remove speech highlight from. " + ygb + " " + zgb);
							}
						}
					} catch (err) {
						eya("$rw_doSelection:clear last speech:" + err.toString());
					}
				}
				pva = -1;
				try {
					rLb.unhighlightRange();
				} catch (err) {
					eya("$rw_doSelection:unhighlightRange:" + err.message);
				}
			} else if (rLb.dVb != null) {
				if (pva == nIb) {
					return;
				}
				var dVb = rLb.dVb;
				var UBc = dVb.length;
				try {
					if (pva > -1 && pva < UBc) {
						var tcb = dVb[pva].range;
						var YCc = SSDOM.getCaretPairFromDomPosition(tcb.body, tcb.Upb.path, tcb.Upb.offset, tcb.Vpb.path, tcb.Vpb.offset);
						var ygb = YCc.ygb;
						var zgb = YCc.zgb;
						if (ygb != null && zgb != null) {
							rw_removeSpeechHighlight(SSDOM.getListOfHighlightableNodes(ygb, zgb), true);
						} else {
							eya("Cannot determine valid range to remove speech highlight from. " + ygb + " " + zgb);
						}
					}
				} catch (err) {
					thLogE(err);
				}
				if (nIb < 0 || nIb >= UBc) {
					return;
				}
				pva = nIb;
				var tcb = dVb[nIb].range;
				try {
					var YCc = SSDOM.getCaretPairFromDomPosition(tcb.body, tcb.Upb.path, tcb.Upb.offset, tcb.Vpb.path, tcb.Vpb.offset);
					var ygb = YCc.ygb;
					var zgb = YCc.zgb;
					if (ygb != null && zgb != null) {
						try {
							var Xxb = false;
							var Iwa = SSDOM.getComputedStyle(ygb.node);
							if (Iwa != null && (Iwa.display == "none" || Iwa.visibility == "hidden")) {
								Xxb = true;
							} else {
								var wvb = ygb.node;
								var ipb = wvb.ownerDocument.body;
								while (wvb != ipb) {
									wvb = wvb.parentNode;
									if (SSDOM.getComputedStyle(wvb).display == "none") {
										Xxb = true;
										break;
									}
								}
							}
							if (Xxb) {
								var $tmpFunc = function () {
									$rw_stopSpeech();
									$rw_setCurrentTarget(null);
								};
								setTimeout($tmpFunc, 1);
								return;
							}
						} catch (e) {}

						var result = rw_setSpeechRangeImpl(ygb.node, ygb.offset, zgb.node, zgb.offset, "csp");
						if (result != null && result.node != null) {
							ySb(result.node);
						} else {
							ySb(ygb.node);
						}
						if (typeof(eba_speech_word_highlight_callback) != "undefined") {
							var Mwa = dVb[nIb].word;
							if (typeof(Mwa) == "string" && Mwa.length > 0) {
								if (typeof(eba_speech_word_highlight_callback) == "string") {
									Cxa(eba_speech_word_highlight_callback, "'" + Mwa + "'");
								} else if (typeof(eba_speech_word_highlight_callback) == "function") {
									eba_speech_word_highlight_callback(Mwa);
								}
							}
						}
					} else {
						eya("Cannot determine valid range to add speech highlight from. " + ygb + " " + zgb);
					}
				} catch (err) {
					eya("error with highlight speech range in rw_doSelection:" + err.message);
				}
			}
		}
	}
	if (nIb == -1 || nIb == -2 || nIb == -3) {
		pva = -1;
		rLb = null;
		var Nwa = null;
		if (CJb.length > 0) {
			Nwa = new Array();
		}
		while (CJb.length > 0) {
			if (typeof(CJb[0]) == "string") {
				if (CJb[0].indexOf("$rw_readNextTarget") > -1) {
					break;
				}
			}
			Nwa.push(CJb.shift());
		}
		if (nIb == -1) {
			if (CJb.length > 0) {
				Nwa.push(CJb.shift());
			}
		} else if (nIb == -3) {
			var flash = xbb.getConnector();
			var Pwa = flash.getLastError();
			var LCc = "An error occurred with speech.  " + Pwa;
			if (SpeechStream.actionOnError.action == SpeechStream.EnumActionOnError.SKIP) {
				eya(LCc);
				if (CJb.length > 0) {
					Nwa.push(CJb.shift());
				}
			} else {
				Bza(LCc);
			}
		}
		if (Nwa != null && Nwa.length > 0) {
			WJb(Nwa);
		}
	}
}
function $rw_doMathJaxSelection(p_strMathId, Sab) {
	var itb = 0;
	var jtb = 0;
	if (Sab > 65535) {
		itb = Sab & 65535;
		jtb = ((Sab | 65535)^65535) / 65536;
	}
	SpeechStream.mathJaxHighlighter.highlightWord(p_strMathId, itb, jtb);
}
function $displayMe(bub) {
	Bza(bub);
}
function Wwa(Xwa, Ywa, Zwa, awa, a_bool_scrollbars, a_bool_resizable, a_bool_menubar, a_bool_toolbar, a_bool_addressbar, a_bool_statusbar, a_bool_fullscreen) {
	var Twa = (screen.width - Zwa) / 2;
	var Uwa = (screen.height - awa) / 2;
	var Vwa = 'height=' + awa + ',width=' + Zwa + ',top=' + Uwa + ',left=' + Twa + ',scrollbars=' + a_bool_scrollbars + ',resizable=' + a_bool_resizable + ',menubar=' + a_bool_menubar + ',toolbar=' + a_bool_toolbar + ',location=' + a_bool_addressbar + ',statusbar=' + a_bool_statusbar + ',fullscreen=' + a_bool_fullscreen + '';
	var bwa = window.open(Xwa, Ywa, Vwa);
	if (parseInt(navigator.appVersion, 10) >= 4) {
		bwa.window.focus();
	}
}
function dwa(rwa) {
	var start = document.cookie.indexOf(rwa + "=");
	var fwa = start + rwa.length + 1;
	if ((!start) && (rwa != document.cookie.substring(0, rwa.length))) {
		return null;
	}
	if (start == -1) {
		return null;
	}
	var end = document.cookie.indexOf(";", fwa);
	if (end == -1) {
		end = document.cookie.length;
	}
	return unescape(document.cookie.substring(fwa, end));
}
function jwa(rwa, lwa, mwa, swa, twa, pwa) {
	var hwa = new Date();
	hwa.setTime(hwa.getTime());
	if (mwa) {
		mwa = mwa * 1000 * 60 * 60 * 24;
	}
	var iwa = new Date(hwa.getTime() + (mwa));
	document.cookie = rwa + "=" + escape(lwa) + ((mwa) ? ";expires=" + iwa.toGMTString() : "") + ((swa) ? ";path=" + swa : "") + ((twa) ? ";domain=" + twa : "") + ((pwa) ? ";secure" : "");
}
function qwa(rwa, swa, twa) {
	if (dwa(rwa)) {
		document.cookie = rwa + "=" + ((swa) ? ";path=" + swa : "") + ((twa) ? ";domain=" + twa : "") + ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	}
}
function wwa() {
	if (yfb()) {
		var oCc = Ffb();
		CLb(Sba, oCc);
		HLb(true, Sba);
		var NLb = AFb("rwcollatewrapper");
		var xwa = document.getElementById("rwCollect");
		if (xwa != null) {
			if (parseInt(NLb.scrollHeight, 10) >= 380) {
				var rwTextCollect = AFb("rwTextCollect");
				rwTextCollect.style.height = "360px";
			} else {
				var rwTextCollect = AFb("rwTextCollect");
				rwTextCollect.style.height = (NLb.scrollHeight + 24);
			}
		}
	}
}
function $rw_event_axendolink() {
	document.location = "http://www.browsealoud.info";
}
function dis(node) {
	Bza(node.tagName + "|" + node.nodeValue + "|" + SSDOM.getPositionInDom(node));
}
function Cxa(kxb, Exa) {
	if (kxb == null) {
		return;
	}
	try {
		var Xxb = false;
		var UBc = kxb.length;
		if (UBc > 0) {
			if (kxb.charAt(UBc - 1) == ")" || kxb.substr(UBc - 1) == ";") {
				Xxb = true;
			}
		} else {
			return;
		}
		if (Xxb) {
			eval(kxb);
		} else {
			try {
				if (typeof(Exa) != "undefined" && Exa != null) {
					eval(kxb + "(" + Exa + ");");
				} else {
					eval(kxb + "();");
				}
			} catch (innerErr) {
				eval(kxb);
			}
		}
	} catch (err) {
		thLogE(err);
	}
}
function Fxa(fVb) {
	try {
		(new Function(fVb))();
	} catch (err) {
		thLogE(err.message);
	}
}
function Hxa(exb) {
	if (eba_speech_redirect_callback && typeof(eba_speech_redirect_callback) == "function") {
		return eba_speech_redirect_callback(exb);
	}
	return true;
}
function Jxa() {
	if (typeof(eba_speech_started_callback) == "string") {
		Cxa(eba_speech_started_callback.trimTH());
	}
}
function $rw_renderingSpeechCallback() {
	if (typeof(eba_rendering_speech_callback) == "string") {
		Cxa(eba_rendering_speech_callback.trimTH());
	}
}
function $rw_speechCompleteCallback(p_strParam) {
	try {
		if (typeof(p_strParam) == "undefined" || p_strParam == null) {
			p_strParam = "Complete";
		}
		if (typeof(eba_speech_complete_callback) == "string") {
			Cxa(eba_speech_complete_callback.trimTH(), "'" + p_strParam + "'");
		}
		if (Lga && p_strParam == "Complete") {
			SSAPIP.apipHandler.handleSpeechComplete();
		}
	} catch (e) {}

}
function $rw_speechErrorCallback() {
	try {
		if (typeof(eba_speech_complete_callback) == "string") {
			Cxa(eba_speech_complete_callback.trimTH(), "'Error'");
		}
	} catch (e) {}

}
function rw_pageCompleteCallBack() {
	try {
		if (typeof(eba_page_complete_callback) == "string") {
			Cxa(eba_page_complete_callback.trimTH());
		}
	} catch (e) {}

}
function $rw_event_calculator() {
	try {
		CLb(Wba, "");
		HLb(true, Wba);
		rw_calClearMem();
		rw_calAddDigit('0');
	} catch (err) {
		thLogE(err);
	}
}
function $rw_event_recorder() {
	try {
		CLb(aba, "");
		HLb(true, aba);
		var Lxa = document.getElementById("rw_micDisplay");
		Lxa.style.visibility = "visible";
		var Mxa = document.getElementById("rw_micRecordingOn");
		Mxa.style.visibility = "visible";
	} catch (err) {
		thLogE(err);
	}
}
function $rw_event_generate_cache() {
	try {
		if (typeof($rwj) == "undefined") {
			setTimeout($rw_event_generate_cache, 1000);
			return;
		}
		$rwj.blockUI({
			message : '<div id="rwDragMeGenerateCache" class="rwToolbarCaptionGenerateCache" ignore> Caching page please wait...</div><br><span id="pb1" ignore>0%</span>'
		});
		$rwj('#pb1').progressBar(0);
		pia();
	} catch (err) {
		thLogE(err);
	}
}
function $rw_event_check_cache() {
	try {
		if (typeof($rwj) == "undefined") {
			setTimeout($rw_event_check_cache, 1000);
			return;
		}
		$rwj.blockUI({
			message : '<div id="rwDragMeGenerateCache" class="rwToolbarCaptionGenerateCache" ignore> Reading Cache please wait...</div><br><span id="pb1" ignore>0%</span>'
		});
		$rwj('#pb1').progressBar(0);
		Wja();
	} catch (err) {
		$rwj.unblockUI();
		thLogE(err);
	}
}
function $rw_event_screenmask(event, i) {
	var rmb = SpeechStream.EnumIconParameter,
	isToggled = g_icons[i][rmb.ICON_TOGGLE_STATE] == true;
	g_icons[i][rmb.ICON_TOGGLE_STATE] = !isToggled;
	Rxa(event ? event : window.event);
}
var Oxa = 60;
var Pxa;
function Rxa(e) {
	var Qxa = eba_screen_mask_opacity,
	axa = eba_screen_mask_letterbox_depth,
	viewportWidth = window.innerWidth ? window.innerWidth : $jq(window).width(),
	viewportHeight = window.innerHeight ? window.innerHeight : $jq(window).height(),
	posX = e && e.pageX ? e.pageX : 0,
	posY = e && e.pageY ? e.pageY - $jq(window).scrollTop() : 0,
	maskTopHeight = posY - axa,
	maskBottomHeight = viewportHeight - posY - axa,
	maskVisible = false,
	firstTime = false,
	Yxa = null,
	bottomMask = null;
	if ($jq("#rwMaskTop").length === 0) {
		Yxa = $jq("<div/>").attr("id", "rwMaskTop").addClass("rwScreenMaskOverlay").css({
				"opacity" : Qxa / 100,
				"top" : 0,
				"height" : maskTopHeight + "px"
			}).appendTo("body");
		if ($rw_isTouchDevice()) {
			upArrow = $jq("<a/>").attr({
					"id" : "rwMaskUpArrow",
					"class" : "rwScreenMaskArrow",
					"ignore" : "1",
					"href" : "#"
				}).css({
					"top" : "0"
				}).html("<span><img src=\"" + aga.paths.strFileLoc + "/rwimgs/arrow_up.png\" alt=\"\"/></span>").appendTo("body");
			downArrow = $jq("<a/>").attr({
					"id" : "rwMaskDownArrow",
					"class" : "rwScreenMaskArrow",
					"ignore" : "1",
					"href" : "#"
				}).css({
					"top" : "50%"
				}).html("<span><img src=\"" + aga.paths.strFileLoc + "/rwimgs/arrow_down.png\" alt=\"\"/></span>").appendTo("body");
			$jq(".rwScreenMaskArrow").on("touchstart", function (e) {
				var Sxa = $jq(this).attr("id") === "rwMaskUpArrow";
				Wxa(e, Sxa);
				Pxa = setInterval(function () {
						Wxa(e, Sxa);
					}, 250);
			}).on("touchend", function () {
				clearInterval(Pxa);
				g_lastTapTarget = null;
			});
			Uxa = setInterval(Xxa, 100);
			jKb();
			document.getElementById('rwDrag').style.zIndex = 99993;
		}
		maskVisible = true;
		firstTime = true;
	} else {
		Yxa = $jq("#rwMaskTop");
	}
	if ($jq("#rwMaskBottom").length === 0) {
		bottomMask = $jq("<div/>").attr("id", "rwMaskBottom").addClass("rwScreenMaskOverlay").css({
				"opacity" : Qxa / 100,
				"bottom" : 0,
				"height" : maskBottomHeight + "px"
			}).appendTo("body");
		maskVisible = true;
		firstTime = true;
	} else {
		bottomMask = $jq("#rwMaskBottom");
	}
	if (maskVisible && firstTime) {
		if (!$rw_isTouchDevice()) {
			$jq(document).on("mousemove", bxa);
		}
	} else {
		maskVisible = Yxa.is(":visible");
		Uxa = setInterval(Xxa, 100);
		if (maskVisible) {
			$jq("#rwMaskTop,#rwMaskBottom,#rwMaskUpArrow,#rwMaskDownArrow").hide();
			if (!$rw_isTouchDevice()) {
				$jq(document).off("mousemove");
			} else {
				jKb();
				clearInterval(Uxa);
			}
		} else {
			$jq("#rwMaskTop,#rwMaskBottom,#rwMaskUpArrow,#rwMaskDownArrow").show();
			if (!$rw_isTouchDevice()) {
				$jq(document).on("mousemove", bxa);
			} else {
				jKb();
			}
		}
	}
}
var Txa = null, g_lastTapTarget = null;
function Wxa(e, Sxa) {
	if (event.touches.length === 1) {
		if ((g_lastTapTarget === null || e.target === g_lastTapTarget) && (Txa === null || new Date() - Txa > 500)) {
			g_lastTapTarget = e.target;
			Txa = new Date();
			Zxa(Sxa ? (Oxa * -1) : Oxa);
		}
		e.preventDefault();
	}
	return false;
}
var Uxa, g_currentHeight = 0, g_currentWidth = 0;
function Xxa() {
	var Yxa = $jq("#rwMaskTop"),
	bottomMask = $jq("#rwMaskBottom"),
	tmHeight = Yxa.height(),
	bmHeight = bottomMask.height(),
	viewportWidth = window.innerWidth ? window.innerWidth : $jq(window).width(),
	viewportHeight = window.innerHeight ? window.innerHeight : $jq(window).height();
	if (viewportWidth !== g_currentWidth || viewportHeight !== g_currentHeight) {
		g_currentWidth = viewportWidth;
		g_currentHeight = viewportHeight;
		bottomMask.css({
			"height" : (viewportHeight - tmHeight - (eba_screen_mask_letterbox_depth * 2)) + "px"
		});
	}
}
function Zxa(pixelChange) {
	var Yxa = $jq("#rwMaskTop"),
	bottomMask = $jq("#rwMaskBottom"),
	tmHeight = Yxa.height(),
	bmHeight = bottomMask.height(),
	viewportWidth = window.innerWidth ? window.innerWidth : $jq(window).width(),
	viewportHeight = window.innerHeight ? window.innerHeight : $jq(window).height();
	if (Yxa.height() < Oxa && pixelChange < 0) {
		pixelChange = Yxa.height() * -1;
	}
	if (bottomMask.height() < Oxa && pixelChange > 0) {
		pixelChange = bottomMask.height();
	}
	Yxa.animate({
		"height" : (tmHeight + pixelChange) + "px"
	}, 250);
	bottomMask.animate({
		"height" : (bmHeight - pixelChange) + "px"
	}, 250);
}
function bxa(e) {
	var axa = eba_screen_mask_letterbox_depth,
	Yxa = $jq("#rwMaskTop"),
	bottomMask = $jq("#rwMaskBottom"),
	viewportWidth = window.innerWidth ? window.innerWidth : $jq(window).width(),
	viewportHeight = window.innerHeight ? window.innerHeight : $jq(window).height(),
	posX = e.pageX,
	posY = e.pageY - $jq(window).scrollTop(),
	maskTopHeight = posY - axa,
	maskBottomHeight = viewportHeight - posY - axa;
	Yxa.css({
		"height" : maskTopHeight + "px"
	});
	bottomMask.css({
		"height" : maskBottomHeight + "px"
	});
}
function cxa(bub) {
	if (bub == null) {
		return "";
	}
	bub = bub.trimTH();
	var Nub = "";
	var i = 0;
	var n = bub.length;
	var itb = -1;
	var jtb = -1;
	for (i = 0; i < n; i++) {
		var uvb = bub.charCodeAt(i);
		if ((uvb > 64 && uvb < 91) || (uvb > 96 && uvb < 123)) {
			if (itb == -1) {
				itb = i;
			}
		} else {
			if (itb > -1) {
				if (uvb == 39) {
					if (i < (n - 1)) {
						nValNext = bub.charCodeAt(i + 1);
						if ((nValNext > 64 && nValNext < 91) || (nValNext > 96 && nValNext < 123)) {
							++i;
						} else {
							jtb = i;
							break;
						}
					} else {
						jtb = i;
						break;
					}
				} else {
					jtb = i;
					break;
				}
			}
		}
	}
	if (itb > -1) {
		if (jtb > -1) {
			Nub = bub.substring(itb, jtb);
		} else {
			Nub = bub.substring(itb);
		}
	}
	return Nub;
}
function hxa(bub) {
	if (bub == null) {
		return "";
	}
	bub = bub.trimTH();
	bub = bub.replace(/[\s\xA0]+/g, " ");
	var Nub = "";
	var i = 0;
	var n = bub.length;
	var itb = -1;
	var jtb = -1;
	for (i = 0; i < n; i++) {
		var uvb = bub.charCodeAt(i);
		if ((uvb > 64 && uvb < 91) || (uvb > 96 && uvb < 123) || uvb > 127) {
			if (itb == -1) {
				itb = i;
			}
		} else {
			if (itb > -1) {
				if (uvb == 39) {
					if (i < (n - 1)) {
						nValNext = bub.charCodeAt(i + 1);
						if ((nValNext > 64 && nValNext < 91) || (nValNext > 96 && nValNext < 123) || uvb > 127) {
							++i;
						} else {
							jtb = i;
							break;
						}
					} else {
						jtb = i;
						break;
					}
				} else {
					jtb = i;
					break;
				}
			}
		}
	}
	if (itb > -1) {
		if (jtb > -1) {
			Nub = bub.substring(itb, jtb);
		} else {
			Nub = bub.substring(itb);
		}
	}
	return Nub;
}
function mxa(LPb) {
	if (LPb == null) {
		return "";
	}
	var Nub = LPb.trimTH();
	if (Nub.length == 0) {
		return "";
	}
	Nub = Nub.replace(/[\s\xA0]+/g, " ");
	var AUb = 0;
	var FBc = Nub.indexOf(' ');
	while (FBc > -1) {
		++AUb;
		if (AUb == 3) {
			Nub = Nub.substr(0, FBc);
			break;
		}
		FBc = Nub.indexOf(' ', FBc + 1);
	}
	return Nub;
}
function rxa(ZAc) {
	if (ZAc && ZAc.length > 0) {
		var n = -1;
		var i;
		for (i = ZAc.length - 1; i >= 0; i--) {
			if (vxa(ZAc.charCodeAt(i))) {
				break;
			}
			n = i;
		}
		if (n > -1) {
			ZAc = ZAc.substr(0, n);
		}
	}
	return ZAc;
}
function txa(ZAc) {
	if (ZAc && ZAc.length > 0) {
		var n = -1;
		var i;
		for (i = 0; i < ZAc.length; i++) {
			if (vxa(ZAc.charCodeAt(i))) {
				break;
			}
			n = i;
		}
		if (n > -1) {
			ZAc = ZAc.substr(n);
		}
	}
	return ZAc;
}
function vxa(Sab) {
	return (Sab > 64 && Sab < 91) || (Sab > 96 && Sab < 123) || (Sab > 127 && Sab != 160);
}
function xxa(Sab) {
	return (Sab > 64 && Sab < 91) || (Sab > 96 && Sab < 123);
}
function zxa(Sab) {
	return (Sab > 47 && Sab < 58);
}
function Bya(Sab) {
	return (Sab > 47 && Sab < 58) || (Sab > 63 && Sab < 91) || (Sab > 94 && Sab < 123);
}
function Dya(Sab) {
	return ((Sab > 127 && Sab != 160) || Bya(Sab));
}
function Gya(bub) {
	if (bub == null) {
		return false;
	}
	for (var i = 0; i < bub.length; i++) {
		var uvb = bub.charCodeAt(i);
		if (uvb == 39 || (uvb > 47 && uvb < 58) || (uvb > 64 && uvb < 91) || uvb == 96 || (uvb > 96 && uvb < 123)) {
			return true;
		}
	}
	return false;
}
function Jya(Kya) {
	if (Kya == null || Kya.length == 0) {
		return Kya;
	}
	var UBc = Kya.length;
	for (var i = 0; i < UBc; i++) {
		var uvb = Kya.charCodeAt(i);
		if (!(uvb == 39 || uvb == 44 || uvb == 46 || (uvb > 47 && uvb < 58) || (uvb > 63 && uvb < 91) || (uvb > 94 && uvb < 123))) {
			Kya = Kya.replace(Kya.charAt(i), ' ');
		}
	}
	return Kya.trimTH();
}
function Mya(range) {
	var bub = "";
	if (range.text) {
		bub = range.text;
	} else {
		bub = range.toString();
	}
	return bub;
}
function Oya() {
	var wSb = SSDOM.getSelectionObject();
	if (wSb == null) {
		return;
	}
	if (wSb.collapseToStart) {
		wSb.collapseToStart();
	} else if (wSb.execCommand) {
		wSb.execCommand("UnSelect", false, null);
	}
}
function Vya(Bub) {
	if (Bub != null) {
		if (Ufa) {
			var range = SSDOM.getRangeObject(Bub.ownerDocument.body);
			range.moveToElementText(Bub);
			range.select();
		} else {
			var wvb = SSDOM.getNextNodeIgnoreChildren(Bub, false, null);
			if (wvb != null) {
				var range = new vra(Bub, 0, wvb, 0);
				Xya(range);
			}
		}
	}
}
function Xya(zeb) {
	if (Ufa) {
		zeb.select();
	} else {
		var start = zeb.sBc;
		var end = zeb.tBc;
		var MWb = SSDOM.getWindow(start.node);
		if (!Yfa) {
			var ogb = MWb.getSelection();
			ogb.collapse(start.node, start.offset);
			if (ogb.extend) {
				ogb.extend(end.node, end.offset);
			} else {
				var range = MWb.document.createRange();
				range.setStart(start.node, start.offset);
				range.setEnd(end.node, end.offset);
				ogb.addRange(range);
			}
		} else {
			MWb.getSelection().setBaseAndExtent(start.node, start.offset, end.node, end.offset);
		}
	}
}
function Zya(aya) {
	return (aya.search(/[\s\xa0]/) > -1);
}
function bya(rBc) {
	var cya = rBc + "  ";
	if (rBc.tagName != null) {
		cya += rBc.tagName + " ";
	}
	if (rBc.className != null) {
		cya += rBc.className + " ";
	}
	for (prop in rBc) {
		cya += prop + "  " + " | ";
	}
	alert(cya);
}
function dya(rBc) {
	var cya = rBc + "  ";
	for (prop in rBc) {
		cya += prop + "  " + " | ";
	}
	return cya;
}
function eya(Lpb) {
	if (typeof(window.console) !== "undefined" && typeof(window.console.log) !== "undefinded") {
		try {
			window.console.log(Lpb);
		} catch (err) {}

	}
	if (typeof($rw_dump) == 'function') {
		try {
			$rw_dump(Lpb + "\n");
		} catch (err) {}

	}
}
function thLogE(error) {
	if (error != null) {
		if (error.name && error.message) {
			eya("Error: " + "" + error.name + ". : " + error.message);
		} else if (error.message) {
			eya("Error: " + "" + error.message);
		}
	}
}
function $rw_logMe(ZAc) {
	eya(ZAc);
}
function $rw_alertMe(ZAc) {
	alert(ZAc);
}
function $rw_inputFieldFilter(ZAc) {
	if (ZAc == null || ZAc.length == 0) {
		return ZAc;
	}
	var i;
	var UBc = ZAc.length;
	for (i = UBc - 1; i >= 0; i--) {
		var c = ZAc.charCodeAt(i);
		if ((c < 44 && c != 39) || c == 47 || (c > 57 && c < 65) || (c > 90 && c < 97 && c != 95) || (c > 122 && c < 128)) {
			ZAc = ZAc.substring(0, i) + ZAc.substr(i + 1);
		}
	}
	return ZAc;
}
function $rw_handleFieldInput(DBc) {
	var pre = DBc.value;
	var post = $rw_inputFieldFilter(pre);
	if (pre != post) {
		DBc.value = post;
	}
}
function $rw_handleFieldKeyDownInput(evt) {
	if (!evt.ctrlKey) {
		var c = evt.keyCode;
		if ((c > 32 && c < 44 && c != 39) || c == 47 || (c > 57 && c < 65) || (c > 90 && c < 97 && c != 95) || (c > 122 && c < 128)) {
			return false;
		}
	}
	return true;
}
function jya(c) {
	return (c > 64 && c < 91) || (c > 96 && c < 123) || c == 39 || (c > 128 && c != 160);
}
function mya(Usb) {
	var UBc = Usb.length;
	var i = 0;
	var uvb;
	for (i = 0; i < UBc; i++) {
		uvb = Usb.charCodeAt(i);
		if ((uvb > 63 && uvb < 91) || (uvb > 96 && uvb < 123) || (uvb > 127 && uvb != 160)) {
			return true;
		} else if (uvb > 46 && uvb < 58) {
			return true;
		} else if ((uvb > 35 && uvb < 39) || uvb == 43 || uvb == 61) {
			return true;
		} else if (uvb == 42 || uvb == 45 || uvb == 92 || (uvb > 93 && uvb < 97)) {
			if (xca != "VW Kate") {
				return true;
			}
		}
	}
	if (qfa) {
		if (ZVb(Usb)) {
			return true;
		}
	}
	return false;
}
function wya(COb) {
	try {
		if (COb == null || COb.ygb == null || COb.zgb == null) {
			return false;
		}
		var ygb = COb.ygb;
		var zgb = COb.zgb;
		var hUb = ygb.node;
		var aOb = zgb.node;
		var mOb = true;
		var tya = false;
		var jOb = hUb;
		var bub = "";
		while (jOb != null) {
			tya = SSDOM.isSpecialCase(jOb);
			if (SpeechStream.pauseHandler.isPauseElement(jOb)) {
				tya = true;
			}
			if (tya || jOb.nodeType == 3) {
				var mBc = SSDOM.getTextFromNode(jOb);
				if (mBc != null && mBc != "") {
					if (!tya) {
						if (jOb == aOb && zgb.offset > -1) {
							mBc = mBc.substring(0, zgb.offset);
						}
						if (jOb == hUb && ygb.offset > 0) {
							mBc = mBc.substring(ygb.offset);
						}
					}
					if (mya(mBc)) {
						return true;
					}
				}
			}
			if (tya) {
				jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, aOb);
			} else {
				if (mOb) {
					jOb = SSDOM.getNextNodeAllowMoveToChild(jOb, true, aOb);
				} else {
					jOb = SSDOM.getNextNode(jOb, false, aOb);
				}
			}
			mOb = false;
		}
	} catch (err) {
		thLogE(err);
	}
	return false;
}
function zya(d) {
	return d.toString(16);
}
function Aza(h) {
	return parseInt(h, 16);
}
function Bza(ZAc) {
	if (dca) {
		alert(ZAc);
	} else {
		eya(ZAc);
	}
}
function $rw_enable_alerts(p_bEnable) {
	if (typeof(p_bEnable) == "boolean") {
		dca = p_bEnable;
	}
}
function $rw_uriEncode(ZAc) {
	return encodeURIComponent(ZAc);
}
function Dza(nCb) {
	if (nCb) {
		return ((Hea || Iea) ? "https://" : "http://");
	} else {
		return ((Hea || Jea) ? "https://" : "http://");
	}
}
/*Code designed and developed by Stuart McWilliams.*/
SpeechStream.EnumIconParameter = {
	ICON_NAME : 0,
	ICON_ALT_TEXT : 1,
	ICON_OFFSET : 2,
	ICON_IS_TOGGLE : 3,
	ICON_TOGGLE_STATE : 4
};
function Hza(kxb, eza, VXb, oIb) {
	var Fza = SpeechStream.EnumIconParameter;
	var FBc = nea;
	g_icons[nea] = new Array(5);
	g_icons[nea][Fza.ICON_NAME] = kxb;
	g_icons[nea][Fza.ICON_ALT_TEXT] = eza;
	g_icons[nea][Fza.ICON_OFFSET] = VXb;
	g_icons[nea][Fza.ICON_IS_TOGGLE] = oIb;
	g_icons[nea][Fza.ICON_TOGGLE_STATE] = false;
	nea++;
	return FBc;
}
function Pza(kxb) {
	var i;
	for (i = 0; i < g_icons.length; i++) {
		if (g_icons[i][SpeechStream.EnumIconParameter.ICON_NAME] == kxb) {
			return i;
		}
	}
	return -1;
}
function Rza(i) {
	var Mza = SpeechStream.EnumIconParameter;
	var Hpb = g_icons[i][Mza.ICON_NAME];
	var Kpb = g_icons[i][Mza.ICON_ALT_TEXT];
	if (Qga && Kpb == "Click To Speak Mode" && !Vca) {
		Kpb = "Hover Speech";
	}
	var chb = g_icons[i][Mza.ICON_OFFSET];
	var Tza = g_icons[i][Mza.ICON_IS_TOGGLE];
	var Uza = false;
	if (Tza) {
		if (Hpb == "hover" + "" && typeof(eba_initial_speech_on) == "boolean" && eba_initial_speech_on) {
			g_icons[i][Mza.ICON_TOGGLE_STATE] = true;
			Uza = true;
		}
	}
	var tKb = 33;
	if (Hpb.equalsTH("submit" + "")) {
		tKb = 53;
	}
	Bfa += tKb;
	var Wza = bza(Hpb, "flat" + "", Kpb, tKb, chb, true, Uza);
	if (cfa && (hfa < 6 || !efa)) {
		YEb(Wza, "touchstart", function () {
			$rw_getTouchSelection();
		});
	}
	YEb(Wza, "mouseover", function () {
		if (!bfa) {
			if (!g_icons[i][Mza.ICON_TOGGLE_STATE]) {
				dIb(Hpb, i, Tza);
			}
		}
	});
	YEb(Wza, "mouseout", function () {
		if (!g_icons[i][Mza.ICON_TOGGLE_STATE]) {
			hIb(Hpb, i, Tza);
		}
	});
	YEb(Wza, "mousedown", function () {
		if (!bfa) {
			if (!g_icons[i][Mza.ICON_TOGGLE_STATE]) {
				lIb(Hpb, i, Tza);
			}
		}
		xea = Hpb;
	});
	YEb(Wza, "mouseup", function (e) {
		if (xea.equalsTH(Hpb)) {
			if ($rw_blockClick(Hpb)) {
				return true;
			} else {
				SpeechStream.ipadUtils.tinyMceBlurFix();
				var LCc;
				LCc = '$rw_event_' + Hpb + '(e, ' + i + ');';
				eval(LCc);
			}
		}
		if (!bfa) {
			if (!g_icons[i][Mza.ICON_TOGGLE_STATE]) {
				dIb(Hpb, i, Tza);
			}
		}
	});
	return Wza;
}
function bza(kxb, jBb, eza, fza, nIb, ILb, iza) {
	var Yza = fza * Pza(kxb);
	var Zza = 0;
	if (iza) {
		Zza = 66;
	}
	var ECb = {};
	ECb["ignore"] = "1";
	ECb["name"] = kxb + jBb;
	ECb["width"] = "" + fza;
	ECb["height"] = "32";
	ECb["title"] = eza;
	ECb["unselectable"] = "on";
	if (ILb) {
		ECb["style"] = "left:" + Yza + "px; background-position: -" + nIb + "px -" + Zza + "px;";
	} else {
		ECb["style"] = "left:" + Yza + "px; background-position: -" + nIb + "px -" + Zza + "px;";
	}
	if (Qga) {
		var kza = SSDOM.createObjectFromMap("thrwgdwrns:thtoolbarIcon", ECb, kxb, null, false);
	} else {
		var kza = SSDOM.createObjectFromMap("span", ECb, kxb, null, false);
	}
	if (Lfa) {
		kza.onselectstart = function () {
			return false;
		};
	} else {
		kza.onmousedown = function () {
			return false;
		};
	}
	return kza;
}
function mza() {
	Bfa += 100;
	nLeftPosition = 33 * nea;
	var ECb = {};
	ECb["ignore"] = "1";
	if (Lfa) {
		ECb["style"] = "width:95px;position:relative;left:" + nLeftPosition + "px;top:6px;border: 1px solid;color:#000000;backgroundColor:#f1efe5";
	} else {
		ECb["style"] = "width:95px;position:relative;left:" + nLeftPosition + "px;top:6px;border: 1px solid;color:#000000;background-color:#f1efe5";
	}
	var nza = SSDOM.createObjectFromMap("select", ECb, null, null, true);
	ZEb(nza, "change", function () {
		$rw_setSpeedValue(parseInt(nza.value, 10));
	});
	var oza;
	ECb = {};
	ECb["ignore"] = "1";
	ECb["value"] = "" + SLOW_SPEED;
	if (Hda == -3 || Hda == SLOW_SPEED) {
		ECb["selected"] = "1";
	}
	oza = SSDOM.createObjectFromMap("option", ECb, null, null, true);
	oza.innerHTML = "Slow";
	var pza;
	ECb = {};
	ECb["ignore"] = "1";
	ECb["value"] = "" + MEDIUM_SPEED;
	if (Hda == -2 || Hda == MEDIUM_SPEED) {
		ECb["selected"] = "1";
	}
	pza = SSDOM.createObjectFromMap("option", ECb, null, null, true);
	pza.innerHTML = "Medium";
	var qza;
	ECb = {};
	ECb["ignore"] = "1";
	ECb["value"] = "" + FAST_SPEED;
	if (Hda == -1 || Hda == FAST_SPEED) {
		ECb["selected"] = "1";
	}
	qza = SSDOM.createObjectFromMap("option", ECb, null, null, true);
	qza.innerHTML = "Fast";
	nza.appendChild(oza);
	nza.appendChild(pza);
	nza.appendChild(qza);
	return nza;
}
function tza(kxb, jBb) {
	var rza = null;
	if (jBb != null) {
		rza = document.images[kxb + jBb];
		if (rza != null) {
			var yza = rza.style;
			yza.visibility = "\x76isible";
			yza.display = "\x69n\x6cine";
			yza.width = "\x326\x70x";
		}
		if (jBb != "\x6fff") {
			rza = document.images[kxb + "off"];
			if (rza != null) {
				var yza = rza.style;
				yza.visibility = "\x68idden";
				yza.display = "n\x6fn\x65";
				yza.width = "\x30px";
			}
		} else if (jBb != "\x6fn") {
			rza = document.images[kxb + "on"];
			if (rza != null) {
				var yza = rza.style;
				yza.visibility = "\x68idden";
				yza.display = "n\x6fne";
				yza.width = "\x30px";
			}
		}
	}
}
function zza(kxb, jBb, Thb, DAb) {
	if (jBb != null) {
		var yza = document.getElementById(kxb);
		if (kxb == "s\x75bmit") {
			yza.width = "\x353px";
		} else {
			yza.width = "\x333px";
		}
	}
	if (jBb == "\x66\x6c\x61t") {
		SSDOM.setStyle(yza, "backgroundPosition: -" + Thb + "px 0px;");
	}
	if (jBb == "hover") {
		SSDOM.setStyle(yza, "backgroundPosition: -" + Thb + "px -33px;");
	}
	if (jBb == "toggle") {
		SSDOM.setStyle(yza, "backgroundPosition: -" + Thb + "px -66px;");
	}
	if (jBb == "mask") {
		SSDOM.setStyle(yza, "backgroundPosition: -" + Thb + "px -99px;");
	}
	if (DAb) {
		if (jBb == "toggleOn") {
			SSDOM.setStyle(yza, "backgroundPosition: -" + Thb + "px -66px;");
		}
		if (jBb == "mask") {
			SSDOM.setStyle(yza, "backgroundPosition: -" + Thb + "px -99px;");
		}
	}
}
SpeechStream.tinymceipadfix = "thtinymceipadbugworkaround";
SpeechStream.tinymceipadfocusbugworkaround = function () {
	SpeechStream.ipadUtils.tinyMceBlurFix();
};
function FAb(kxb) {
	if (document.getElementById(kxb)) {
		var icon = g_icons[Pza(kxb)];
		icon[SpeechStream.EnumIconParameter.ICON_TOGGLE_STATE] = false;
		zza(kxb, "flat", icon[SpeechStream.EnumIconParameter.ICON_OFFSET], false);
	}
}
function $rw_barInit() {
	HAb();
}
function HAb() {
	if (!sAb()) {
		return;
	}
	if ((typeof(eba_custId) == "string" && eba_custId == "200") || (typeof(eba_cust_id) == "string" && eba_cust_id == "200")) {
		QBb();
	}
	tAb();
	gfa = bfa && !(Yga.indexOf("android") > -1) && (jca & clicktospeak_icon) != clicktospeak_icon && (jca & sticky_icon) != sticky_icon && (lca & clicktospeak_icon) != clicktospeak_icon && (lca & sticky_icon) != sticky_icon;
	aga.pageData.placeholder = sBb();
	Bda = parseInt(yca, 10);
	if (Bda == 300) {
		VBb();
	}
	if (Bda >= 500 && Bda < 600) {
		Cea = true;
	}
	if (Bda >= 810 && Bda < 820) {
		pda = true;
	}
	if (Bda >= 1220 && Bda < 1229) {
		sda = true;
	}
	if (Lfa && !oda) {
		var OMb = yCb();
		if (!OMb) {
			Dea = true;
			if (Mda) {}
			else {
				Bza("The embedded speech toolbar cannot be added due to invalid html tag markup in this page .\n" + "Try using FireFox or Safari to view this page or contact the page author to notify them of this error.");
				return;
			}
		}
	}
	SpeechStream.calculatePaths.initPaths();
	KAb();
}
function KAb() {
	var strFileLoc = aga.paths.strFileLoc;
	if (Jga) {
		SSDOM.eob(aga.pageData.placeholder, "<link " + "href=\"" + strFileLoc + "rwLanguageSWA.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
		var LAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "SSLS.js"]);
		aga.pageData.placeholder.appendChild(LAb);
		var MAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpSwa.js"]);
		aga.pageData.placeholder.appendChild(MAb);
		try {
			if (typeof(SpeechStream.swaConfig) != "undefined" && SpeechStream.swaConfig != null && typeof(SpeechStream.swaConfig.getBlockedToolbar) == "function") {
				var uvb = SpeechStream.swaConfig.getBlockedToolbar();
				var Txb = jca & uvb;
				jca -= Txb;
			} else {
				setTimeout(KAb, 50);
				return;
			}
		} catch (err) {}

	}
	if (jca == 0) {
		Afa = false;
	}
	if (Afa) {
		xBb();
	}
	mBb();
	if (Zda && Lfa) {
		lea = true;
	}
	qBb();
	JDb();
	if ((jca & calculator_icon) == calculator_icon) {
		var PAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpCalc.js"]);
		aga.pageData.placeholder.appendChild(PAb);
	}
	if ((jca & record_icon) == record_icon) {
		$rw_barRecInit();
	}
	if ((jca & generatecache_icon) == generatecache_icon) {
		$rw_barCacheInit();
	}
	if ((jca & dictionary_icon) == dictionary_icon || (jca & factfinder_icon) == factfinder_icon || (jca & translation_icon) == translation_icon || (jca & mp3_icon) == mp3_icon) {
		var QAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpSearch.js"]);
		aga.pageData.placeholder.appendChild(QAb);
	}
	if ((jca & highlightcyan_icon) == highlightcyan_icon || (jca & highlightgreen_icon) == highlightgreen_icon || (jca & highlightmagenta_icon) == highlightmagenta_icon || (jca & highlightyellow_icon) == highlightyellow_icon || (jca & collect_icon) == collect_icon || Zda || (jca & vocabulary_icon) == vocabulary_icon || (jca & strike_icon) == strike_icon) {
		var RAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpSS.js"]);
		aga.pageData.placeholder.appendChild(RAb);
	}
	if ((jca & sticky_icon) == sticky_icon || Zda) {
		var SAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpSticky.js"]);
		aga.pageData.placeholder.appendChild(SAb);
	}
	if ((jca & pronCreate_icon) == pronCreate_icon || (jca & pronEdit_icon) == pronEdit_icon) {
		var TAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpPron.js"]);
		aga.pageData.placeholder.appendChild(TAb);
	}
	if ((jca & generatecache_icon) == generatecache_icon || (jca & checkcache_icon) == checkcache_icon) {
		var VAb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "jquerycombined.js"]);
		aga.pageData.placeholder.appendChild(VAb);
	}
	if ((jca & screenmask_icon) == screenmask_icon) {
		if (!DFb()) {
			var VAb = SSDOM.createObject("script", ["type", "text/javascript", "src", "//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"]);
			aga.pageData.placeholder.appendChild(VAb);
			CFb();
		} else {
			$jq = jQuery;
		}
	}
	if (!Lfa) {
		var WAb = document.getElementsByTagName('input');
		for (var i = 0; i < WAb.length; i++) {
			var XAb = WAb.item(i);
			var attr = XAb.getAttribute("t\x79pe");
			if (attr != null && attr == "t\x65xt") {
				YEb(XAb, "\x6do\x75seu\x70", XEb);
			}
		}
	}
	if (Dga && cfa) {
		var YAb = null;
		if (typeof(eba_tinymce_id) == "string") {
			YAb = document.getElementById(eba_tinymce_id);
		}
		if (YAb) {
			try {
				var ipb = document.body;
				var mlb = ipb.firstChild;
				while (mlb != null) {
					if (lAb(mlb, Ega)) {
						mlb = SSDOM.getNextNodeIgnoreChildren(mlb, false, ipb);
					} else {
						if (mlb.nodeType == 1) {
							YEb(mlb, "touchstart", SpeechStream.tinymceipadfocusbugworkaround);
						}
						mlb = SSDOM.getNextNode(mlb, false, ipb);
					}
				}
			} catch (err) {
				thLogE(err.message);
			}
			var ta = document.createElement("input");
			ta.setAttribute("type", "text");
			ta.setAttribute("style", "opacity:0;color:#ffffff;padding:0px;margin:0px;" + "border-top-color:#FFFFFF;border-right-color:#FFFFFF;border-bottom-color:#FFFFFF;border-left-color:#FFFFFF;" + "border-top-style:none;border-right-style:none;border-bottom-style:none;border-left-style:none;" + "height:1px;width:1px");
			ta.setAttribute("id", SpeechStream.tinymceipadfix);
			var Orb = document.getElementById("rwDrag");
			if (Orb != null) {
				Orb.appendChild(ta);
			} else {
				YAb.parentNode.insertBefore(ta, YAb);
			}
			qAb(false);
		}
	}
	if (cfa) {
		var Orb = document.getElementById("rwDrag");
		if (Orb != null) {
			YEb(Orb, 'dragstart', AIb);
			YEb(Orb, 'touchend', ZIb);
			YEb(Orb, 'touchmove', TIb);
			YEb(Orb, 'touchstart', xHb);
		}
	}
	if (aga.bubbleData.bBubbleMode) {
		if (!oda) {
			var yIb = SSDOM.createObject("script", ["type", "text/javascript", "src", strFileLoc + "texthelpBubble.js"]);
			aga.pageData.placeholder.appendChild(yIb);
		}
	}
	if (typeof(eba_initial_speech_on) == "boolean") {
		if (eba_initial_speech_on) {
			$g_bMouseSpeech = true;
			pEb(true);
			xbb.enableTouchEvents(eba_initial_speech_on);
		}
	}
	var gCc = document.getElementById(eba_tinymce_id);
	if (gCc) {
		var fAb = gCc.getAttribute("ongrammarclick");
		if (fAb && fAb.length > 0) {
			Fga = fAb;
		}
		var gAb = gCc.getAttribute("onspellingclick");
		if (gAb && gAb.length > 0) {
			Gga = gAb;
		}
		var hAb = gCc.getAttribute("onhomophoneclick");
		if (hAb && hAb.length > 0) {
			Hga = hAb;
		}
		var iAb = gCc.getAttribute("onpredictionclick");
		if (iAb && iAb.length > 0) {
			Iga = iAb;
		}
	}
	if (iea) {
		rIb();
	}
}
function lAb(oAb, Brb) {
	if (oAb.id != null && oAb.id.indexOf(Brb) > -1) {
		return true;
	}
	var mlb = oAb.firstChild;
	var kAb;
	while (mlb != null) {
		kAb = false;
		if (mlb.id != null && mlb.id.indexOf(Brb) > -1) {
			return true;
		}
		mlb = SSDOM.getNextNode(mlb, false, oAb);
	}
	return false;
}
function qAb(rAb) {
	try {
		var nhb = document.getElementById(eba_tinymce_id + "_ifr").contentDocument.body;
		if (nhb && typeof(nhb.ipadTinyMceFocus) == "undefined") {
			nhb.ipadTinyMceFocus = rAb;
			ZEb(nhb, "click", function () {
				nhb.ipadTinyMceFocus = true;
			});
			ZEb(document.getElementById(eba_tinymce_id + "_ifr").contentWindow, "focus", function () {
				nhb.ipadTinyMceFocus = true;
			});
		}
		return nhb;
	} catch (e) {}

	return null;
}
function sAb() {
	if (typeof(eba_bypass_dom_check) == "boolean" && eba_bypass_dom_check) {
		oda = true;
	}
	return true;
}
function tAb() {
	iCb();
	if (typeof(pktTitleId) != "\x75n\x64\x65f\x69ne\x64" || yca == "1600") {
		pCb();
	}
	if (yca == null || yca.length == 0 || zca == null || zca.length == 0 || Ada == null || Ada.length == 0) {
		SpeechStream.pronunciation.mode = SpeechStream.pronunciation.NONE;
		SpeechStream.cacheMode.mode = SpeechStream.cacheMode.NONE;
	}
	if (pca == null || qca == null || rca == null) {
		var RBc = ".speechstream.net";
		var vAb = RBc.length;
		var wAb = false;
		var xAb = SpeechStream.cacheMode.getLiveServer();
		if (xAb != null && (xAb == "speechus.texthelp.com" || (xAb.length > vAb && xAb.substring(xAb.length - vAb) == RBc))) {
			wAb = true;
		}
		if (wAb) {
			xAb = lCb(xAb, false);
			if (pca == null) {
				pca = xAb;
			}
			if (qca == null) {
				qca = xAb;
			}
			if (rca == null) {
				rca = xAb;
			}
		}
	}
	if (Aga == null) {
		Aga = qca;
	}
	if (typeof(eba_annotate_storage_url) == "string" || typeof(eba_annotate_highlight_editor_id) == "string" || typeof(eba_annotate_note_editor_id) == "string" || typeof(eba_annotate_note_storage_url) == "string" || typeof(eba_annotate_highlight_storage_url) == "string") {
		oCb();
	}
	if (Eda == SPANISH && typeof(eba_voice) == "undefined" && typeof(pktVoice) == "undefined" && !Fda) {
		xca = "\x53c\x61nSoft\x20Paulina_\x46ull_22\x6bHz";
	}
	if (typeof(dtdType) != "undefined" && afa) {
		Ifa = dtdType;
		if (dtdType == "xtran") {
			Jfa = true;
		} else if (dtdType == "loose") {
			Kfa = true;
		}
	}
	zAb();
	if (typeof(eba_use_html5) == "boolean") {
		if (!eba_use_html5) {
			lea = true;
		}
	} else {
		lea = !((typeof(nca) == "string" && nca == "speechus.texthelp.com") || (typeof(oca) == "string" && oca == "speechus.texthelp.com"));
	}
	if (Oga != null) {
		SSAPIP.apipHandler.init(Oga, Pga);
	}
}
function zAb() {
	var UBb;
	try {
		UBb = window.location.search;
	} catch (err) {
		UBb = "";
	}
	if (typeof(eba_autocache_generate) == "boolean" && eba_autocache_generate) {
		cea = true;
	} else {
		var FZb = GBb(UBb, "speechstreamautocache");
		cea = (FZb != null) && (FZb.toLowerCase() == "true");
	}
	if (!cea) {
		if (typeof(eba_autocache_check) == "boolean" && eba_autocache_check) {
			dea = true;
		} else {
			var FZb = GBb(UBb, "speechstreamautocachecheck");
			dea = (FZb != null) && (FZb.toLowerCase() == "true");
		}
	}
	if (typeof(eba_autocache_no_alert) == "boolean" && eba_autocache_no_alert) {
		fea = true;
	} else {
		var FZb = GBb(UBb, "speechstreamautocachenoalert");
		fea = (FZb != null) && (FZb.toLowerCase() == "true");
	}
	if (cea || dea) {
		lca = lca | generatecache_icon;
		lca = lca | checkcache_icon;
		lea = true;
	} else {
		if (typeof(eba_icons) == "number" && eba_icons > 0) {
			if ((eba_icons & generatecache_icon) == generatecache_icon || (eba_icons & checkcache_icon) == checkcache_icon) {
				lea = true;
			}
		}
		if (typeof(eba_no_display_icons) == "number" && eba_no_display_icons > 0) {
			if ((eba_no_display_icons & generatecache_icon) == generatecache_icon || (eba_no_display_icons & checkcache_icon) == checkcache_icon) {
				lea = true;
			}
		}
	}
	if (typeof(eba_autocache_callback) == "string") {
		eea = eba_autocache_callback;
	} else {
		var FZb = GBb(UBb, "speechstreamautocachecallback");
		if (FZb != null && FZb.length > 0) {
			FZb = decodeURIComponent(FZb);
			eea = FZb;
		}
	}
	if (eea && eea.length > 0) {
		eea = pVb(eea, "\\amp;", "&");
	}
	if (typeof(eba_autocache_allspeeds) == "boolean") {
		bea = eba_autocache_allspeeds;
	}
}
function GBb(HBb, IBb) {
	var FZb = null;
	var FBb = IBb + "=";
	FBb = FBb.toLowerCase();
	var UBc = FBb.length;
	var itb;
	var jtb;
	var MBb;
	var NBb = HBb.toLowerCase();
	itb = NBb.indexOf(FBb);
	while (itb > 0) {
		MBb = HBb.charAt(itb - 1);
		if (MBb == "?" || MBb == "&") {
			jtb = HBb.indexOf("&", itb + UBc);
			if (jtb == -1) {
				FZb = HBb.substr(itb + UBc);
			} else {
				FZb = HBb.substring(itb + UBc, jtb);
			}
			break;
		} else {
			itb = NBb.indexOf(FBb, itb + 1);
		}
	}
	return FZb;
}
function QBb() {
	var OBb = false;
	var PBb = false;
	Bea = true;
	if (Cda == null) {
		Cda = "s\x63holast\x69c";
		Dda = "s\x63holast\x69\x63";
	}
	var RBb = document.getElementsByTagName("meta");
	var UBc = RBb.length;
	var i;
	for (i = 0; i < UBc; i++) {
		var TBb = RBb[i];
		if (TBb.name != null) {
			if (TBb.name.toLowerCase() == "assetid" && TBb.content != null && TBb.content.length > 0) {
				Ada = TBb.content;
				PBb = true;
			} else if (TBb.name.toLowerCase() == "pcode" && TBb.content != null && TBb.content.length > 0) {
				zca = TBb.content;
				OBb = true;
			}
		}
	}
	var UBb = window.location.search;
	itb = UBb.indexOf("id=");
	while (itb > 0) {
		MBb = UBb.charAt(itb - 1);
		if (MBb == "?" || MBb == "&") {
			jtb = UBb.indexOf("&", itb + 3);
			if (jtb == -1) {
				Ada = UBb.substr(itb + 3);
			} else {
				Ada = UBb.substring(itb + 3, jtb);
			}
			itb = -1;
		} else {
			itb = UBb.indexOf("id=", itb + 1);
		}
	}
	itb = UBb.indexOf("product_id=");
	while (itb > 0) {
		MBb = UBb.charAt(itb - 1);
		if (MBb == "?" || MBb == "&") {
			jtb = UBb.indexOf("&", itb + 11);
			if (jtb == -1) {
				zca = UBb.substr(itb + 11);
				OBb = true;
			} else {
				zca = UBb.substring(itb + 11, jtb);
				PBb = true;
			}
			itb = -1;
		} else {
			itb = UBb.indexOf("product_id=", itb + 1);
		}
	}
	if (!OBb && (zca == null || zca == "")) {
		zca = "none";
	}
	if (!PBb && (Ada == null || Ada == "")) {
		Ada = "none";
	}
}
function VBb() {
	zca = "index";
	Ada = "1";
	var WBb = document.location;
	if (WBb != null) {
		var mwb = WBb.pathname;
		if (mwb.length > 0) {
			var YBb = mwb.lastIndexOf("/");
			if (YBb > -1) {
				mwb = mwb.substr(YBb + 1);
				var ZBb = mwb.indexOf(".html");
				if (ZBb > -1) {
					mwb = mwb.substring(0, ZBb);
					zca = mwb;
				}
			}
		}
	}
}
function dBb() {
	var aBb;
	var bBb;
	var cBb;
	this.initPaths = function () {
		aBb = Dza(false) + mca + "/";
		bBb = Dza(true) + nca + "/";
		if (oca != null) {
			cBb = Dza(true) + oca + "/";
		} else {
			cBb = null;
		}
		fBb();
		var gBb = hBb();
		iBb(gBb);
	};
	this.getServerUrl = function () {
		return aBb;
	};
	this.getSpeechServerUrl = function () {
		return bBb;
	};
	this.getSpeechServerUrlBackup = function () {
		return cBb;
	};
	function fBb() {
		if (vca == null) {
			vca = "";
		}
		while (vca.length > 0 && vca.charAt(0) == '/') {
			vca = vca.substr(1);
		}
		while (vca.length > 0 && vca.charAt(vca.length - 1) == '/') {
			vca = vca.substr(0, vca.length - 1);
		}
		if (vca == "") {
			vca = "SpeechStream";
		}
	}
	function hBb() {
		var gBb;
		if (fca.length > 0) {
			if (fca == "latest") {
				gBb = vca + "/" + fca;
			} else {
				gBb = vca + "/v" + fca;
			}
		} else {
			gBb = vca;
		}
		return gBb;
	}
	function iBb(jBb) {
		if (Rga != null) {
			if (fca == "latest") {
				aga.paths.strFileLoc = Rga + fca + "/";
			} else {
				aga.paths.strFileLoc = Rga + "v" + fca + "/";
			}
		} else {
			if (jBb.length > 0) {
				aga.paths.strFileLoc = aBb + jBb + "/";
			} else {
				aga.paths.strFileLoc = aBb;
			}
			aga.paths.strSwfLoc = wca;
		}
	}
}
SpeechStream.calculatePaths = new dBb();
function mBb() {
	var kBb;
	var OCb = PCb();
	if (Bfa > 0) {
		if (cca) {
			if (afa) {
				ufa = true;
				kBb = SSDOM.createObject("div", ["rwTHComp", "1", "style", "visibility:hidden;display:none"], "rwDrag");
			} else {
				kBb = SSDOM.createObject("div", ["rwTHComp", "1", "style", "position:relative;visibility:hidden;display:none"], "rwDrag");
			}
		} else {
			if (Qga) {
				window.document.documentElement.setAttribute("xmlns:thRWGDWRns", "");
				kBb = SSDOM.createObject("thrwgdwrns:thdragtoolbar", ["rwTHComp", "1", "style", "visibility:hidden;display:none"], "rwDrag");
			} else {
				kBb = SSDOM.createObject("div", ["rwTHComp", "1", "style", "visibility:hidden;display:none"], "rwDrag");
			}
		}
		var BCb = DCb();
		if (!Zca) {
			BCb.appendChild(JCb());
		}
		BCb.appendChild(OCb);
		kBb.appendChild(BCb);
	} else {
		kBb = SSDOM.createObject("div", ["rwTHComp", "1", "visibility", "hidden"], "rwDrag");
		var BCb = SSDOM.createObject("div", ["rwTHComp", "1", "visibility", "hidden"], "rwDrag");
		kBb.appendChild(BCb);
	}
	aga.pageData.placeholder.appendChild(kBb);
}
function qBb() {
	var Orb = document.getElementById("WebToSpeech");
	if (Orb == null) {
		Orb = document.createElement("span");
		Orb.id = "WebToSpeech";
		aga.pageData.placeholder.appendChild(Orb);
	}
	cCb(SpeechStream.calculatePaths.getServerUrl(), SpeechStream.calculatePaths.getSpeechServerUrl(), SpeechStream.calculatePaths.getSpeechServerUrlBackup());
}
function $rw_barDynamicStart() {
	iea = true;
}
function sBb() {
	var rBb;
	if (Qga) {
		rBb = document.getElementById("thSpeechStreamTBPlaceHolder");
	} else {
		rBb = document.getElementById("speechStreamPlaceholder");
	}
	if (rBb == null) {
		if (iea) {
			rBb = document.body;
		} else {
			if (cca || (Lfa && Ufa)) {
				var mBc = document.body;
				var match;
				while (mBc != null) {
					if (mBc != null && mBc.nodeType == 1) {
						match = mBc;
					}
					mBc = mBc.lastChild;
				}
				if (match == document.body) {
					rBb = match;
				} else {
					var uBb = match.parentNode;
					var vBb = document.createElement("span");
					if (Qga) {
						vBb.id = "thSpeechStreamTBPlaceHolder";
					} else {
						vBb.id = "speechStreamPlaceholder";
					}
					uBb.insertBefore(vBb, match);
					rBb = vBb;
				}
			} else {
				rBb = document.body;
			}
		}
	}
	return rBb;
}
function xBb() {
	var strFileLoc = aga.paths.strFileLoc;
	if (Cea) {
		SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + strFileLoc + "rwMain500Bar.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
	} else {
		var yBb = jca;
		if (kca != -1) {
			yBb = kca;
		}
		var zBb = clicktospeak_icon + play_icon + pause_icon + stop_icon;
		if ((yBb | zBb) == zBb) {
			SSDOM.eob(aga.pageData.placeholder, "<link " + "href=\"" + strFileLoc + "rwMainTHSpeechBar.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
		} else {
			var ACb = zBb + search_icons + picturedictionary_icon;
			if ((yBb | ACb) == ACb) {
				if (Qga) {
					SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + strFileLoc + "rwMainExtSearchBar.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
				} else {
					SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + strFileLoc + "rwMainTHSearchBar.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
				}
			} else {
				if (Qga) {
					SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + strFileLoc + "rwMainExtFullBar.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
				} else {
					SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + strFileLoc + "rwMainTHFullBar.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
				}
			}
		}
	}
}
function DCb() {
	var BCb;
	if (Qga) {
		Bfa = Bfa + 280;
	}
	var mrb = "width:" + Bfa + "px;" + "visibility:hidden;";
	if (Kda || Zca) {
		mrb += "display:none;";
	}
	var ECb = {};
	ECb["style"] = mrb;
	if (!Zca) {
		if (Qga) {
			BCb = SSDOM.createObjectFromMap("thrwgdwrns:thmainoutlinetoolbar", ECb, "rwMainOutline", "rwToolbarOutline");
		} else {
			BCb = SSDOM.createObjectFromMap("div", ECb, "rwMainOutline", "rwToolbarOutline");
		}
	} else {
		BCb = SSDOM.createObjectFromMap("div", ECb, "rwMainOutline", null);
	}
	return BCb;
}
function JCb() {
	var strFileLoc = aga.paths.strFileLoc;
	var GCb;
	var Jpb;
	var qAc;
	if (eba_logo_url == null) {
		if (Cea || Yda) {
			Jpb = "";
			qAc = null;
		} else {
			Jpb = "Click here to go to www.texthelp.com";
			qAc = "www.texthelp.com";
		}
	} else if (eba_logo_url == "none") {
		Jpb = "";
		qAc = null;
	} else {
		Jpb = "Click here to go to " + eba_logo_url;
		qAc = eba_logo_url;
		if (qAc.substr(0, 7) == "http://") {
			qAc = qAc.substr(7);
		} else if (qAc.substr(0, 8) == "https://") {
			qAc = qAc.substr(8);
		}
	}
	if (Qga) {
		GCb = SSDOM.createObject("thrwgdwrns:thdragmetoolbar", ["style", "display:block"], "rwDragMe", "rwToolbarCaption");
	} else {
		GCb = SSDOM.createObject("div", null, "rwDragMe", "rwToolbarCaption");
	}
	if (Qga) {
		var KCb = SSDOM.createObject("thrwgdwrns:thtoolbartitileRW", null, "rwToolbarTitleRW", null);
		KCb.setAttribute('ignore', '');
		var content = document.createTextNode("Read&Write for Google");
		KCb.appendChild(content);
		GCb.appendChild(KCb);
	}
	var MCb;
	if (qAc == null) {
		ECb = {};
		ECb["border"] = "0";
		ECb["ignore"] = "1";
		ECb["align"] = "right";
		if (Cea) {
			ECb["src"] = strFileLoc + "rwimgs500/logo500.gif";
			ECb["style"] = "margin: 5px; cursor:default;";
		} else if (Qga) {
			ECb["style"] = "cursor:default";
		} else {
			ECb["src"] = strFileLoc + "rwimgs/logo.gif";
			ECb["style"] = "cursor:default";
		}
		ECb["title"] = "";
		ECb["alt"] = "";
		if (Qga) {
			MCb = SSDOM.createObjectFromMap("thrwgdwrns:thtoolbarImage", ECb, null, "rwCaptionIcon");
		} else {
			MCb = SSDOM.createObjectFromMap("img", ECb, null, null);
		}
		GCb.appendChild(MCb);
	} else {
		ECb = {};
		ECb["border"] = "0";
		ECb["ignore"] = "1";
		ECb["align"] = "right";
		if (Cea) {
			ECb["src"] = strFileLoc + "rwimgs500/logo500.gif";
			ECb["style"] = "margin: 5px;";
		} else if (Qga) {}
		else {
			ECb["src"] = strFileLoc + "rwimgs/logo.gif";
		}
		ECb["title"] = Jpb;
		ECb["alt"] = Jpb;
		if (Qga) {
			MCb = SSDOM.createObjectFromMap("thrwgdwrns:thtoolbarImage", ECb, null, "rwCaptionIcon");
		} else {
			MCb = SSDOM.createObjectFromMap("img", ECb, null, null);
		}
		var NCb;
		ECb = {};
		ECb["href"] = Dza(false) + qAc;
		ECb["target"] = "_blank";
		if (!Yfa) {
			ECb["style"] = "cursor:hand";
		}
		NCb = SSDOM.createObjectFromMap("a", ECb, null, null);
		NCb.appendChild(MCb);
		GCb.appendChild(NCb);
	}
	return GCb;
}
function PCb() {
	var OCb;
	if (Zca) {
		OCb = SSDOM.createObject("div", null, null, "rwToolbarBarNoBorder");
	} else {
		if (Qga) {
			OCb = SSDOM.createObject("thrwgdwrns:thtoolbar", ["style", "display:block"], null, "rwToolbarBar");
		} else {
			OCb = SSDOM.createObject("div", null, null, "rwToolbarBar");
		}
	}
	if (Qga) {
		var RCb = SSDOM.createObject("thrwgdwrns:thtoolbarlist", ["style", "display:block"], "rwToolbarList", null);
	} else {
		var RCb = SSDOM.createObject("div", null, "rwToolbarList", null);
	}
	$rw_setIconsToLoad(jca);
	var SCb = false;
	for (var i = 0; i < nea; i++) {
		SCb = true;
		RCb.appendChild(Rza(i));
	}
	if ((jca & selectSpeed_icon) == selectSpeed_icon) {
		SCb = true;
		RCb.appendChild(mza());
	}
	if (SCb && Bfa < 110) {
		Bfa = 110;
	} else if (!SCb) {
		Bfa = 0;
	}
	if (Qga) {
		var TCb;
		ECb = {};
		ECb["ignore"] = "1";
		ECb["title"] = "Click here to go to www.texthelp.com";
		ECb["alt"] = "Click here to go to www.texthelp.com";
		TCb = SSDOM.createObjectFromMap("thrwgdwrns:thtoolbarImage", ECb, null, "rwMainLogoIcon");
		var UCb;
		ECb = {};
		ECb["href"] = Dza(false) + "www.texthelp.com";
		ECb["target"] = "_blank";
		ECb["style"] = "cursor:hand";
		UCb = SSDOM.createObjectFromMap("a", ECb, null, null);
		var VCb;
		var WCb = Bfa - 8;
		ECb = {};
		ECb["id"] = "texthelpLogo";
		ECb["ignore"] = "1";
		ECb["width"] = "82";
		ECb["height"] = "24";
		ECb["unselectable"] = "on";
		ECb["style"] = "left:" + WCb + "px;cursor:hand";
		VCb = SSDOM.createObjectFromMap("thrwgdwrns:thtoolbarfeaturelogo", ECb, null, null);
		UCb.appendChild(TCb);
		var XCb;
		ECb = {};
		ECb["href"] = Dza(false) + "rw.texthelp.com/drive/Support/featurepreview";
		ECb["title"] = "Click here to learn more";
		ECb["target"] = "_blank";
		ECb["style"] = "cursor:hand; padding-top: 4px;";
		ECb["onClick"] = "window.postMessage({type: \"1757FROM_PAGERW4G\",command:\"trackEvent\",settings:{\'category\': \"FromWebReader\",\'action\': \"WebReader Feature Link\",\'label\':\"" + window.location.host + "\"}},\'*\');";
		XCb = SSDOM.createObjectFromMap("a", ECb, null, null);
		var content = document.createTextNode("Feature Preview. Learn more...");
		var ZCb;
		ECb = {};
		ECb["id"] = "texthelpPreviewText";
		ZCb = SSDOM.createObjectFromMap("thrwgdwrns:thtoolbarfeature", ECb, null, null);
		XCb.appendChild(content);
		ZCb.appendChild(XCb);
		VCb.appendChild(ZCb);
		VCb.appendChild(UCb);
		RCb.appendChild(VCb);
	}
	OCb.appendChild(RCb);
	return OCb;
}
function cCb(dCb, eCb, fCb) {
	var aCb = {};
	var strSwfLoc = aga.paths.strSwfLoc;
	if (Cda != null && Dda != null) {
		aCb.userName = Cda;
		aCb.userPassword = Dda;
	} else {}

	aCb.lessonServerLoc = dCb;
	aCb.speechServerLoc = eCb;
	aCb.speedValue = Hda;
	if (hca >= 3) {
		aCb.useServices = "true";
	} else if (hca == -1) {
		if ((nca != null && nca.indexOf(".speechstream.net") > -1) || (oca != null && oca.indexOf(".speechstream.net") > -1)) {
			aCb.useServices = "true";
		} else {
			aCb.useServices = "false";
		}
	} else {
		aCb.useServices = "false";
		lea = true;
	}
	if (pca != null) {
		aCb.translateServerLoc = pca;
	}
	if (qca != null) {
		aCb.dictionaryServerLoc = qca;
	}
	if (rca != null) {
		aCb.imagedictionaryServerLoc = rca;
	}
	if (sca != null) {
		aCb.predictionServerLoc = sca;
	}
	if (tca != null) {
		aCb.spellingServerLoc = tca;
	}
	if (uca != null) {
		aCb.homophoneServerLoc = uca;
	}
	if (Mda || Nda) {
		aCb.cacheMode = "true";
		if (fCb != null) {
			aCb.cacheLiveFallover = "true";
		}
	}
	if (fCb != null) {
		aCb.speechServerBackupLoc = fCb;
	}
	if (yca != null && zca != null && Ada != null) {
		aCb.custID = yca;
		aCb.bookID = zca;
		aCb.pageID = Ada;
	}
	if (Vda > -1) {
		aCb.cacheCount = Vda;
	}
	if (Wda > -1) {
		aCb.cacheTimeDelay = Wda;
	}
	aCb.locale = Gda;
	aCb.speechName = xca;
	if (mda != null && nda != null) {
		aCb.searchString = mda;
		aCb.replaceString = nda;
	}
	if (strSwfLoc == null || strSwfLoc == "") {
		strSwfLoc = Dza(false) + mca + "/";
	} else if (strSwfLoc.charAt(0) == '.') {
		if (strSwfLoc.charAt(strSwfLoc.length - 1) != '/') {
			strSwfLoc = mca + "/";
		}
	} else {
		if (strSwfLoc.charAt(0) != '/') {
			strSwfLoc = "/" + strSwfLoc;
		}
		if (strSwfLoc.charAt(strSwfLoc.length - 1) != '/') {
			strSwfLoc = strSwfLoc + "/";
		}
		strSwfLoc = Dza(false) + mca + strSwfLoc;
	}
	aga.paths.strSwfLoc = strSwfLoc;
	if (Hea) {
		aCb.SSLSpeech = "true";
		aCb.SSLToolbar = "true";
	} else {
		if (Iea) {
			aCb.SSLSpeech = "true";
		}
		if (Jea) {
			aCb.SSLToolbar = "true";
		}
	}
	if (Lfa && (Hea || Iea)) {
		aCb.IESSL = "true";
	}
	kca = jca;
	jca = jca | lca;
	if ((jca & pronCreate_icon) == pronCreate_icon || (jca & pronEdit_icon) == pronEdit_icon || Xda) {
		aCb.cacheBuster = "true";
	}
	if (Ida > -1) {
		aCb.volumeValue = "" + Ida;
	}
	var Jgb = {};
	Jgb.allowScriptAccess = "always";
	Jgb.movie = strSwfLoc + 'WebToSpeech' + gca + '.swf';
	Jgb.quality = "high";
	Jgb.bgcolor = "#ffffff";
	SpeechStream.m_flashVars = aCb;
	SpeechStream.setUpControllerFactory();
	if (xbb.doesSupportHtml5()) {
		xbb.getConnector().initialise(aCb, dCb, eCb, fCb);
		xbb.setSwaConntector(xbb.getConnector());
	} else {
		try {
			if (Vfa && !Rfa) {
				g_strAmp = "&amp;";
			}
			swfobject.embedSWF(strSwfLoc + 'WebToSpeech' + gca + '.swf', "WebToSpeech", "1", "1", "9.0.0", false, aCb, Jgb);
		} catch (err) {
			g_strAmp = "&amp;";
			swfobject.embedSWF(strSwfLoc + 'WebToSpeech' + gca + '.swf', "WebToSpeech", "1", "1", "9.0.0", false, aCb, Jgb);
		}
		if (afa) {
			var hCb = new SpeechStream.HTML5Controller();
			hCb.initialise(aCb, dCb, eCb, fCb);
			xbb.setSwaConntector(hCb);
		}
	}
}
function iCb() {
	if (typeof(eba_use_container) == "boolean") {
		cca = eba_use_container;
	}
	if (typeof(eba_allow_alerts_flag) == "boolean") {
		dca = eba_allow_alerts_flag;
	}
	if (typeof(eba_alerts) == "boolean") {
		dca = eba_alerts;
	}
	if (typeof(eba_no_title) == "boolean") {
		Zca = eba_no_title;
	}
	if (typeof(eba_noTitleFlag) == "boolean") {
		Zca = eba_noTitleFlag;
	}
	if (typeof(eba_hidden_bar) == "boolean") {
		Kda = eba_hidden_bar;
	}
	if (typeof(eba_continuous_reading) == "boolean") {
		Eea = eba_continuous_reading;
	}
	if (typeof(eba_speak_selection_by_sentence) == "boolean") {
		if (Eea) {
			Mea = eba_speak_selection_by_sentence;
		} else {
			Mea = false;
		}
	} else {
		if (!Eea) {
			Mea = false;
		}
	}
	if (typeof(eba_dont_extend_selection) == "boolean") {
		Nea = !eba_dont_extend_selection;
	}
	if (typeof(eba_ignore_buttons) == "boolean") {
		Lda = eba_ignore_buttons;
	}
	if (typeof(eba_page_complete_after_selection) == "boolean") {
		Oea = eba_page_complete_after_selection;
	}
	var jCb = false;
	if (typeof(eba_speechCacheFlag) == "boolean") {
		Nda = eba_speechCacheFlag;
		jCb = true;
	}
	if (typeof(eba_speech_cache_flag) == "boolean") {
		Nda = eba_speech_cache_flag;
		jCb = true;
	}
	if (typeof(eba_cache_mode) == "boolean") {
		Nda = eba_cache_mode;
		jCb = true;
	}
	if (jCb && Nda) {
		SpeechStream.cacheMode.mode = SpeechStream.cacheMode.CACHE_ONLY;
	}
	if (typeof(eba_cache_live_generation) == "boolean") {
		Oda = eba_cache_live_generation;
		if (Oda) {
			SpeechStream.cacheMode.mode = SpeechStream.cacheMode.CACHE_WITH_LIVE_SERVER;
		}
	}
	if (typeof(eba_speechCacheGenerateFlag) == "boolean") {
		Mda = eba_speechCacheGenerateFlag;
		if (Mda) {
			SpeechStream.cacheMode.mode = SpeechStream.cacheMode.CACHE_BUILDING_MODE;
		}
	}
	if (typeof(eba_cache_building_mode) == "boolean") {
		Mda = eba_cache_building_mode;
		if (Mda) {
			SpeechStream.cacheMode.mode = SpeechStream.cacheMode.CACHE_BUILDING_MODE;
		}
	}
	if (typeof(eba_split_cache_path) == "boolean") {
		Pda = eba_split_cache_path;
	}
	if (typeof(eba_autoCachePage) == "boolean") {
		Qda = eba_autoCachePage;
	}
	if (typeof(eba_voice_from_lang_flag) == "boolean") {
		Fda = eba_voice_from_lang_flag;
	}
	if (typeof(eba_bubble_mode) == "boolean") {
		aga.bubbleData.bBubbleMode = eba_bubble_mode;
	}
	if (typeof(eba_bubble_freeze_on_shift_flag) == "boolean") {
		aga.bubbleData.bBubbleFreezeOnShiftFlag = eba_bubble_freeze_on_shift_flag;
	}
	if (typeof(eba_hover_flag) == "boolean") {
		Vca = !eba_hover_flag;
	}
	if (typeof(eba_ssl_flag) == "boolean") {
		Hea = eba_ssl_flag;
		Iea = Hea;
		Jea = Hea;
	}
	if (typeof(eba_ssl_speech) == "boolean") {
		Iea = eba_ssl_speech;
	}
	if (typeof(eba_ssl_toolbar) == "boolean") {
		Jea = eba_ssl_toolbar;
	}
	if (typeof(eba_clientside_pronunciation) == "boolean") {
		if (eba_clientside_pronunciation) {
			if (typeof(eba_check_pronunciation_before_cache) == "boolean") {
				if (eba_check_pronunciation_before_cache) {
					SpeechStream.pronunciation.mode = SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER;
				} else {
					SpeechStream.pronunciation.mode = SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_OFFLINE_CACHE;
				}
			} else {
				SpeechStream.pronunciation.mode = SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_OFFLINE_CACHE;
			}
		} else {
			SpeechStream.pronunciation.mode = SpeechStream.pronunciation.SERVER_PRONUNCIATION;
		}
	} else {
		if (typeof(eba_check_pronunciation_before_cache) == "boolean") {
			if (eba_check_pronunciation_before_cache) {
				SpeechStream.pronunciation.mode = SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER;
			} else {
				SpeechStream.pronunciation.mode = SpeechStream.pronunciation.SERVER_PRONUNCIATION;
			}
		}
	}
	if (typeof(eba_alter_browser_for_consistency) == "boolean") {
		Lea = eba_alter_browser_for_consistency;
	}
	if (typeof(eba_cache_selection) == "boolean") {
		Tda = eba_cache_selection;
	}
	if (typeof(eba_cache_user_text) == "boolean") {
		Uda = eba_cache_user_text;
	}
	if (typeof(eba_skip_on_error) == "boolean") {
		if (eba_skip_on_error) {
			SpeechStream.actionOnError.action = SpeechStream.EnumActionOnError.SKIP;
		}
	}
	if (typeof(eba_cache_buster) == "boolean") {
		if (eba_cache_buster) {
			Xda = true;
		}
	}
	if (typeof(eba_handle_radio_checkbox_click) == "boolean") {
		if (eba_handle_radio_checkbox_click) {
			Rea = true;
		}
	}
	if (typeof(eba_inline_img) == "boolean") {
		Gea = eba_inline_img;
	}
	if (typeof(eba_ignore_hidden) == "boolean") {
		Sga = eba_ignore_hidden;
	}
	if (typeof(eba_limit_cookies) == "boolean") {
		tda = eba_limit_cookies;
	}
	if (typeof(eba_ignore_frames) == "boolean") {
		uda = eba_ignore_frames;
	}
	if (typeof(eba_math) == "boolean") {
		qfa = eba_math;
	}
	if (typeof(eba_maths) == "boolean") {
		qfa = eba_maths;
	}
	if (typeof(eba_use_vocabulary) == "boolean") {
		zfa = eba_use_vocabulary;
	}
	if (typeof(eba_use_vocab) == "boolean") {
		zfa = eba_use_vocab;
	}
	if (typeof(eba_use_commands) == "boolean") {
		Cga = eba_use_commands;
	}
	if (typeof(eba_tinymce) == "boolean") {
		Dga = eba_tinymce;
	}
	if (typeof(eba_swa) == "boolean") {
		Jga = eba_swa;
	}
	if (typeof(eba_disable_speech) == "boolean") {
		Kga = eba_disable_speech;
	}
	if (typeof(eba_apip) == "boolean") {
		Lga = eba_apip;
	}
	if (typeof(eba_chrome_extension) == "boolean") {
		Qga = eba_chrome_extension;
	}
	if (typeof(eba_icons) == "number") {
		jca = eba_icons;
	}
	if (typeof(eba_no_display_icons) == "number") {
		lca = eba_no_display_icons;
	}
	if (typeof(eba_language) == "number") {
		Eda = eba_language;
		if (Eda == 3) {
			Eda = 2;
		} else if (Eda > 3 || Eda < 0) {
			Eda = 0;
		}
		if (Fda) {
			if (eba_language >= 0 && eba_language < faa.length) {
				xca = faa[eba_language];
			}
		}
	}
	if (typeof(eba_speedValue) == "number") {
		Hda = eba_speedValue;
	}
	if (typeof(eba_speed_value) == "number") {
		Hda = eba_speed_value;
	}
	if (typeof(eba_reading_age) == "number") {
		$rw_setReadingAge(eba_reading_age);
	}
	if (typeof(eba_speed_offset) == "number") {
		Hda += eba_speed_offset;
	}
	if (typeof(eba_volume_value) == "number") {
		Ida = eba_volume_value;
	}
	if (typeof(eba_cache_retry) == "number") {
		Vda = eba_cache_retry;
	}
	if (typeof(eba_cache_retry_timeout) == "number") {
		Wda = eba_cache_retry_timeout;
	}
	if (typeof(eba_mp3_limit) == "number") {
		if (eba_mp3_limit < 1000) {
			rda = rda * 1024;
		} else {
			rda = eba_mp3_limit;
		}
	}
	if (typeof(eba_max_word_count == "number")) {
		NOb = eba_max_word_count;
	}
	if (typeof(eba_date_filter_mode == "number") || typeof(eba_date_filter_mode) == "string") {
		SpeechStream.dateFilter.setMode(eba_date_filter_mode);
	}
	if (typeof(eba_vocabulary_limit) == "number") {
		Bga = eba_vocabulary_limit;
	}
	if (typeof(eba_server_version) == "string") {
		fca = eba_server_version;
	}
	if (typeof(eba_serverVersion) == "string") {
		fca = eba_serverVersion;
	}
	if (typeof(eba_client_version) == "string") {
		if (eba_client_version != "latest") {
			gca = eba_client_version;
		}
	}
	if (typeof(eba_clientVersion) == "string") {
		if (eba_clientVersion != "latest") {
			gca = eba_clientVersion;
		}
	}
	if (typeof(eba_server) == "string") {
		mca = eba_server;
		if (mca.length > 6 && mca.substring(0, 7) == "http://") {
			mca = mca.substring(7);
		} else if (mca.length > 7 && mca.substring(0, 8) == "https://") {
			mca = mca.substring(8);
		}
	}
	if (typeof(eba_speech_server) == "string") {
		nca = eba_speech_server;
	}
	if (typeof(eba_speechServer) == "string") {
		nca = eba_speechServer;
	}
	if (typeof(eba_speech_server_backup) == "string") {
		oca = eba_speech_server_backup;
	}
	if (typeof(eba_speechServerBackup) == "string") {
		oca = eba_speechServerBackup;
	}
	if (typeof(eba_translation_server) == "string") {
		pca = lCb(eba_translation_server, false);
	}
	if (typeof(eba_translate_server) == "string") {
		pca = lCb(eba_translate_server, false);
	}
	if (typeof(eba_dictionary_server) == "string") {
		qca = lCb(eba_dictionary_server, false);
	}
	if (typeof(eba_imagedictionary_server) == "string") {
		rca = lCb(eba_imagedictionary_server, false);
	}
	if (typeof(eba_picturedictionary_server) == "string") {
		rca = lCb(eba_picturedictionary_server, false);
	}
	if (typeof(eba_language_server) == "string") {
		sca = lCb(eba_language_server, false);
		tca = lCb(eba_language_server, false);
		uca = lCb(eba_language_server, false);
	}
	if (typeof(eba_spelling_server) == "string") {
		tca = lCb(eba_spelling_server, false);
	}
	if (typeof(eba_homophone_server) == "string") {
		uca = lCb(eba_homophone_server, false);
	}
	if (typeof(eba_prediction_server) == "string") {
		sca = lCb(eba_prediction_server, false);
	}
	if (typeof(eba_vocabulary_server) == "string") {
		Aga = lCb(eba_vocabulary_server, false);
	}
	if (typeof(eba_folder) == "string") {
		vca = eba_folder;
	}
	if (typeof(eba_client_folder) == "string") {
		wca = eba_client_folder;
	}
	if (typeof(eba_clientFolder) == "string") {
		wca = eba_clientFolder;
	}
	if (typeof(eba_voice) == "string") {
		xca = eba_voice;
		if (xca == "ScanSoft Daniel_Full_22kHz") {
			faa[ENG_UK] = xca;
		} else if (xca == "ScanSoft Tom_Full_22kHz") {
			faa[ENG_US] = xca;
		} else if (xca == "ScanSoft Jill_Full_22kHz") {
			faa[ENG_US] = xca;
		}
	}
	if (typeof(eba_custId) == "string") {
		eba_cust_id = eba_custId;
	}
	if (typeof(eba_cust_id) == "string") {
		yca = xVb(eba_cust_id.trimTH());
	}
	if (typeof(eba_bookId) == "string") {
		eba_book_id = eba_bookId;
	}
	if (typeof(eba_book_id) == "string") {
		zca = xVb(eba_book_id.trimTH());
	}
	if (typeof(eba_pageId) == "string") {
		eba_page_id = eba_pageId;
	}
	if (typeof(eba_page_id) == "string") {
		Ada = xVb(eba_page_id.trimTH());
	}
	if (typeof(eba_loginName) == "string") {
		eba_login_name = eba_loginName;
	}
	if (typeof(eba_login_name) == "string") {
		Cda = eba_login_name;
	}
	if (typeof(eba_loginPassword) == "string") {
		eba_login_password = eba_loginPassword;
	}
	if (typeof(eba_login_password) == "string") {
		Dda = eba_login_password;
	} else {
		Dda = Cda;
	}
	if (typeof(eba_locale) == "string") {
		Gda = eba_locale;
	}
	if (typeof(eba_speech_range_colors) == "string") {
		aga.highlightData.strSpeechRangeColours = eba_speech_range_colors;
	}
	if (typeof(eba_speech_range_colours) == "string") {
		aga.highlightData.strSpeechRangeColours = eba_speech_range_colours;
	}
	if (typeof(eba_speech_range_style) == "string") {
		aga.highlightData.strSpeechRangeColours = eba_speech_range_style;
	}
	if (typeof(eba_speech_word_colors) == "string") {
		aga.highlightData.strSpeechWordColours = eba_speech_word_colors;
	}
	if (typeof(eba_speech_word_colours) == "string") {
		aga.highlightData.strSpeechWordColours = eba_speech_word_colours;
	}
	if (typeof(eba_speech_word_style) == "string") {
		aga.highlightData.strSpeechWordColours = eba_speech_word_style;
	}
	if (typeof(eba_mp3_id) == "string") {
		qda = eba_mp3_id;
	}
	if (typeof(eba_search_speech_server) == "string") {
		mda = eba_search_speech_server;
	}
	if (typeof(eba_replace_speech_server) == "string") {
		nda = eba_replace_speech_server;
	}
	if (typeof(eba_amazon_mode) == "string") {
		var mLb = eba_amazon_mode.toLowerCase();
		if (mLb.indexOf("firewall") > -1) {
			mda = "http";
			nda = "http://control01.speechstream.net/SpeechProxy?url=http";
		}
	}
	if (typeof(eba_play_start_point) == "string") {
		aca = eba_play_start_point;
	}
	if (typeof(eba_speech_stream_server_version) == "string" || typeof(eba_speech_stream_server_version) == "number") {
		if (typeof(eba_speech_stream_server_version) == "number") {
			hca = eba_speech_stream_server_version;
		} else {
			try {
				hca = parseInt(eba_speech_stream_server_version, 10);
			} catch (e) {}

		}
	} else {
		if (eba_cust_id == "810") {
			hca = 3;
		}
	}
	if (typeof(eba_symbol_text) == "string") {
		MVb(eba_symbol_text);
	}
	if (typeof(eba_speech_file_type) == "string") {
		eba_speech_file_type = eba_speech_file_type.toLowerCase();
		if (eba_speech_file_type == "mp3" || eba_speech_file_type == "ogg" || eba_speech_file_type == "wav") {
			yfa = eba_speech_file_type;
		}
	}
	if (typeof(eba_voice_language_map) == "string") {
		lga(eba_voice_language_map);
	}
	if (typeof(eba_break_list) == "string") {
		$rw_setBreakList(eba_break_list);
	}
	if (typeof(eba_tinymce_id) == "string") {
		Ega = eba_tinymce_id;
	}
	if (typeof(eba_local_file_path) == "string") {
		Rga = eba_local_file_path;
	}
	if (typeof(eba_apip_data) == "object") {
		Oga = eba_apip_data;
	}
	if (typeof(eba_apip_order == "object")) {
		Pga = eba_apip_order;
	}
}
function lCb(mCb, nCb) {
	if (mCb.length < 4 || mCb.substring(0, 4) != "http") {
		mCb = Dza(nCb) + mCb;
	}
	if (mCb.substr(mCb.length - 1, 1) != "/") {
		mCb += "/";
	}
	return mCb;
}
function oCb() {
	if (zca == null) {
		Bza("Persistent annotations is enabled but no book id was provided, " + "this feature will not work in this page.");
		return;
	}
	if (Ada == null) {
		Bza("Persistent annotations is enabled but no page id was provided, " + "this feature will not work in this page.");
		return;
	}
	if (typeof(eba_use_annotations) == "boolean") {
		Zda = eba_use_annotations;
	} else {
		Zda = true;
	}
	if (typeof(eba_annotate_note_editor_id) == "string") {
		ada = xVb(eba_annotate_note_editor_id);
	}
	if (typeof(eba_annotate_highlight_editor_id) == "string") {
		bda = xVb(eba_annotate_highlight_editor_id);
	}
	if (typeof(eba_annotate_note_reader_id) == "string") {
		cda = xVb(eba_annotate_note_reader_id);
	}
	if (typeof(eba_annotate_highlight_reader_id) == "string") {
		dda = xVb(eba_annotate_highlight_reader_id);
	}
	if (typeof(eba_annotate_persist_notes) == "boolean" && ada != "*") {
		eda = eba_annotate_persist_notes;
	}
	if (typeof(eba_annotate_persist_highlights) == "boolean" && bda != "*") {
		fda = eba_annotate_persist_highlights;
	}
	if (typeof(eba_annotate_highlight_store_text) == "boolean") {
		gda = eba_annotate_highlight_store_text;
	}
	if (typeof(eba_annotate_storage_url) == "string") {
		hda = eba_annotate_storage_url;
		if (typeof(eba_server) == "undefined") {
			mca = hda;
		}
	} else {
		if (Nda) {
			if (oca != null) {
				hda = oca;
			} else {
				hda = mca;
			}
		} else {
			hda = nca;
		}
	}
	if (typeof(eba_annotate_note_storage_url) == "string") {
		ida = eba_annotate_note_storage_url;
	}
	if (typeof(eba_annotate_highlight_storage_url) == "string") {
		jda = eba_annotate_highlight_storage_url;
	}
	if (typeof(eba_annotate_confirm_delete_note) == 'boolean') {
		lda = eba_annotate_confirm_delete_note;
	}
	if (eda) {
		if ((jca & sticky_icon) != sticky_icon) {
			if (jca == no_bar) {
				lca += sticky_icon;
			} else {
				jca += sticky_icon;
			}
		}
	}
}
function pCb() {
	Yda = true;
	if (typeof(eba_use_annotations) == "boolean") {
		Zda = eba_use_annotations;
	} else {
		Zda = true;
	}
	if (typeof(pktIsTeacher) == "boolean") {
		eda = Zda && pktIsTeacher;
	}
	if (typeof(pktTitleId) == "string") {
		zca = pktTitleId;
	}
	if (typeof(pktPageId) == "string") {
		Ada = pktPageId;
	}
	if (typeof(pktStudentId) == "string") {
		if (!eda && Zda) {
			fda = true;
		}
		bda = xVb(pktStudentId);
		cda = xVb(pktStudentId);
	}
	if (typeof(pktTeacherId) == "string") {
		ada = xVb(pktTeacherId);
		dda = xVb(pktTeacherId);
	}
	if (typeof(pktStorageUrl) == "string") {
		hda = pktStorageUrl;
		if (typeof(eba_server) == "undefined") {
			mca = hda;
		}
	} else {
		hda = mca;
	}
	if (typeof(pktSpeechServerUrl) == "string") {
		nca = pktSpeechServerUrl;
	}
	if (typeof(pktVoice) == "string") {
		xca = pktVoice;
	}
	if (typeof(pktCustCode) == 'string') {
		kda = pktCustCode;
	}
	if (typeof(pktConfirmOnDelete) == 'boolean') {
		lda = pktConfirmOnDelete;
	}
	if (eda) {
		if ((jca & sticky_icon) != sticky_icon) {
			if (jca == no_bar) {
				lca += sticky_icon;
			} else {
				jca += sticky_icon;
			}
		}
	}
}
function yCb() {
	if (!uda && top.frames.length > 0) {
		var i;
		var UBc = top.frames.length;
		for (i = 0; i < UBc; i++) {
			var PDb = top.frames[i];
			try {
				var KDb = PDb.document;
				var b = ADb(KDb.body);
				if (!b) {
					return false;
				}
			} catch (e) {}

		}
	}
	if (document.body != null) {
		return ADb(document.body);
	} else {
		return true;
	}
}
function ADb(OXb) {
	if (OXb.firstChild != null) {
		var ZWb = OXb.firstChild;
		var ipb = ZWb.ownerDocument.body;
		try {
			while (ZWb != null && ZWb != ipb) {
				ZWb = SSDOM.getNextNodeIEBugFix(ZWb);
			}
		} catch (er) {
			return false;
		}
	}
	return true;
}
var vCb = 0;
var wCb = 500;
var xCb = false;
function $rw_versionCheck() {
	try {
		kea = xbb.doesSupportSpeech();
		$rw_setSpeedValue(Hda);
	} catch (err) {
		kea = false;
	}
	if (!kea) {
		vCb++;
		if (vCb < wCb) {
			setTimeout($rw_versionCheck, 100);
		} else {
			if (!xCb) {
				if (Mda) {}
				else {
					Bza("A necessary flash component failed to load.  This page will not work as intended.\n" + "Could not load file from: " + aga.paths.strSwfLoc + 'WebToSpeech' + gca + '.swf');
				}
				xCb = true;
			}
		}
	}
}
var CDb = -1;
function $rw_getFlashVersion() {
	if (CDb < 0) {
		try {
			var flash = xbb.getConnector();
			var Jcb = flash.getVersion();
			CDb = parseInt(Jcb, 10);
		} catch (err) {
			CDb = parseInt(gca, 10);
			thLogE(err);
		}
	}
	return CDb;
}
function FDb(GDb, IDb) {
	if (GDb == null || IDb == null) {
		return false;
	}
	if (GDb = IDb) {
		return true;
	}
	if (GDb.frames.length > 0) {
		var i;
		for (i = 0; i < GDb.frames.length; i++) {
			if (IDb == GDb.frames[i]) {
				return true;
			} else {
				if (GDb.frames[i].length > 0) {
					if (FDb(GDb.frames[i], IDb)) {
						return true;
					}
				}
			}
		}
		return false;
	} else {
		return false;
	}
}
function JDb() {
	ZEb(window, 'scroll', jKb);
	ZEb(window, 'resize', jKb);
	ZEb(window, 'scroll', lKb);
	ZEb(window, 'resize', lKb);
	if (!iea) {
		ZEb(window, 'load', rIb);
	}
	ZEb(window, 'beforeunload', AJb);
	ZEb(document, 'click', CIb);
	YEb(document, 'mouseout', bIb);
	YEb(document, 'mouseup', YIb);
	YEb(document, 'mousemove', IIb);
	YEb(document, 'mouseover', DIb);
	YEb(document, 'mousedown', wHb);
	if (hfa >= 6 && Dga) {}
	else {
		YEb(document, 'dragstart', yHb);
		YEb(document, 'touchend', YIb);
		YEb(document, 'touchmove', IIb);
		YEb(document, 'touchstart', wHb);
	}
	if (Qga) {
		g_ExtBackgroundScriptCommObj = new SpeechStream.ExtBackgroundScriptComm();
		window.addEventListener("message", g_ExtBackgroundScriptCommObj.onMessageFromBG, false);
		document.addEventListener("webkitvisibilitychange", SpeechStream.handleVisibilityChange, false);
	}
}
function $rw_addEventsToFrame(wha) {
	try {
		var KDb = wha.document;
		YEb(KDb, 'mouseout', bIb);
		YEb(KDb, 'mouseup', YIb);
		YEb(KDb, 'click', CIb);
		YEb(KDb, 'mousemove', IIb);
		YEb(KDb, 'mouseover', DIb);
		YEb(KDb, 'mousedown', wHb);
		if (hfa >= 6 && Dga) {}
		else {
			YEb(KDb, 'dragstart', yHb);
			YEb(KDb, 'touchend', YIb);
			YEb(KDb, 'touchmove', IIb);
			YEb(KDb, 'touchstart', wHb);
		}
	} catch (er) {}

	if (wha.frames.length > 0) {
		if (!uda) {
			var i;
			for (i = 0; i < wha.frames.length; i++) {
				$rw_addEventsToFrame(wha.frames[i]);
			}
		}
	}
}
function $rw_pageSetup() {
	$rw_tagSentencesWithFrames(window);
	if (!uda && window.frames.length > 0) {
		var i = 0;
		try {
			var UBc = window.frames.length;
			for (i = 0; i < UBc; i++) {
				$rw_addEventsToFrame(window.frames[i]);
			}
		} catch (e) {}

	}
	OLb = document.getElementById('rwDrag').style;
	OLb.display = "inline";
	vCb = 0;
	$rw_versionCheck();
	NDb();
	Sga = true;
}
function NDb() {
	if (!kea) {
		if (vCb < wCb) {
			setTimeout(NDb, 109);
		}
		return;
	}
	ofa = false;
	var MDb = false;
	if (kea && typeof(kYb) == "function") {
		if (SpeechStream.pronunciation.fetchData()) {
			MDb = true;
		}
	}
	if (MDb) {
		if (yca != null && yca.length > 0 && zca != null && zca.length > 0 && Ada != null && Ada.length > 0) {
			pfa = true;
			hXb.deleteAll();
			kYb();
		}
	}
}
function $rw_tagSentencesWithFrames(wha) {
	if (typeof(wha) == "undefined") {
		wha = window;
	}
	if (wha.document && wha.document.body) {
		$rw_tagSentences(wha.document.body);
	}
	if (!uda) {
		if (wha.frames.length > 0) {
			var i;
			var UBc = wha.frames.length;
			for (i = 0; i < UBc; i++) {
				try {
					var PDb = wha.frames[i];
					if (PDb.frames.length > 0) {
						$rw_tagSentencesWithFrames(PDb.frames);
					} else {
						if (PDb.document && PDb.document.body) {
							$rw_tagSentences(PDb.document.body);
						}
					}
				} catch (err) {}

			}
		}
	}
}
var QDb = "[\\x21\\x2E\\x3F\\x3A]";
var RDb = /[\n\r\t ]{2,}/g;
function $rw_tagSentences(OXb) {
	if (typeof(OXb) == 'undefinded' || OXb == null) {
		OXb = document.body;
	}
	try {
		var SDb = false;
		var TDb = false;
		if (Bda >= 200 && Bda < 300) {
			TDb = true;
		}
		Bea = TDb;
		var jOb = OXb;
		while (jOb != null) {
			if (jOb.nodeType == 3) {
				var qRb = jOb.parentNode.tagName.toLowerCase();
				if (qRb == "textarea") {
					jOb = SSDOM.getNextNode(jOb, false, null);
					continue;
				}
				var bub = jOb.nodeValue;
				var XDb = bub.trimSpaceTH();
				var AEb = XDb.length > 0;
				if (pda && qRb == "a") {
					AEb = false;
				}
				if (!AEb) {
					if (Zda || Nda && (Bea || Yda || Lea)) {
						if (SDb) {
							if (!pda) {
								jOb.nodeValue = " ";
							}
							SDb = false;
							jOb = SSDOM.getNextNode(jOb, false, null);
						} else {
							var mBc = jOb;
							jOb = SSDOM.getNextNode(jOb, false, null);
							if (!pda) {
								mBc.parentNode.removeChild(mBc);
							}
						}
					} else {
						jOb = SSDOM.getNextNode(jOb, false, null);
					}
				} else {
					if (!pda) {
						if (Zda || Nda && (Bea || Yda || Lea)) {
							if (XDb.length < bub.length) {
								var Cdb = false;
								XDb = bub.trimSpaceStartTH();
								if ((bub.length - XDb.length) > 0) {
									if (SDb) {
										bub = " " + XDb;
									} else {
										bub = XDb;
									}
									Cdb = true;
								}
								XDb = bub.trimSpaceEndTH();
								if ((bub.length - XDb.length) > 1) {
									bub = XDb + " ";
									SDb = false;
									Cdb = true;
								}
								if (Cdb) {
									jOb.nodeValue = bub;
								}
							}
							XDb = bub.replace(RDb, " ");
							if (XDb.length < bub.length) {
								bub = XDb;
								Cdb = true;
							}
						}
					}
					var FBc;
					FBc = bub.search(QDb);
					var cDb = (jOb.parentNode.getAttribute("texthelpSkip") != null);
					var dDb = jOb;
					if (FBc > -1 && FBc < (bub.length - 1)) {
						var eDb = true;
						while (true) {
							var ghb = JEb(bub, FBc, jOb);
							if (ghb) {
								break;
							} else {
								var gDb = bub.substring(FBc + 1);
								var fpb = gDb.search(QDb);
								if (fpb > -1) {
									FBc = FBc + 1 + fpb;
								} else {
									eDb = false;
									break;
								}
							}
						}
						if (eDb) {
							var iDb = bub.substring(0, FBc + 1);
							var jDb = bub.substring(FBc + 1);
							var span = document.createElement("span");
							span.setAttribute(eaa, "1");
							var sDb = document.createTextNode(iDb);
							var mDb = document.createTextNode(jDb);
							var tDb = jOb.parentNode;
							tDb.insertBefore(mDb, jOb);
							tDb.insertBefore(span, mDb);
							span.appendChild(sDb);
							tDb.removeChild(jOb);
							jOb = mDb;
							dDb = sDb;
						} else {
							if (jOb.previousSibling != null || jOb.nextSibling != null || cDb) {
								var span = document.createElement("span");
								span.setAttribute(eaa, "1");
								var sDb = document.createTextNode(bub);
								var tDb = jOb.parentNode;
								tDb.insertBefore(span, jOb);
								span.appendChild(sDb);
								tDb.removeChild(jOb);
								jOb = sDb;
							}
							dDb = jOb;
							jOb = SSDOM.getNextNode(jOb, false, null);
						}
					} else {
						if (jOb.previousSibling != null || jOb.nextSibling != null || cDb) {
							var span = document.createElement("span");
							span.setAttribute(eaa, "1");
							var sDb = document.createTextNode(bub);
							var tDb = jOb.parentNode;
							tDb.insertBefore(span, jOb);
							span.appendChild(sDb);
							tDb.removeChild(jOb);
							jOb = sDb;
						}
						dDb = jOb;
						jOb = SSDOM.getNextNode(jOb, false, null);
					}
					if (Zda || Nda && (Bea || Yda || Lea)) {
						var uDb = dDb.nodeValue;
						var vDb = dDb.nodeValue.length;
						if (vDb > 0 && uDb.charCodeAt(vDb - 1) == 32) {
							SDb = false;
						} else {
							SDb = true;
						}
					}
				}
			} else if (jOb.nodeType == 1) {
				if (Zda) {
					if (!SSDOM.isStyleNode(jOb)) {
						if (SSDOM.isLineBreakNode(jOb)) {
							SDb = false;
						}
					} else if (jOb.tagName.toLowerCase() == "img") {
						SDb = true;
					}
				}
				if (TDb) {
					if (jOb.tagName.toLowerCase() == "img") {
						var VRb = jOb.getAttribute("title");
						if (VRb != null && VRb.length > 0) {
							jOb.setAttribute("msg", VRb);
						}
					}
				} else if (Gea) {
					if (jOb.tagName.toLowerCase() == "img") {
						var VRb = jOb.getAttribute("msg");
						if (!(VRb != null && VRb.length > 0)) {
							VRb = jOb.getAttribute("title");
							if (VRb != null && VRb.length > 0) {
								jOb.setAttribute("msg", VRb);
							} else {
								VRb = jOb.getAttribute("alt");
								if (VRb != null && VRb.length > 0) {
									jOb.setAttribute("msg", VRb);
								}
							}
						}
					}
				}
				var EEb = jOb.getAttribute(caa);
				var FEb = jOb.getAttribute(baa);
				if (jOb.tagName.toLowerCase() == "pre" || (EEb != null && EEb.length > 0) || (FEb != null && FEb.length > 0)) {
					jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, null);
				} else {
					jOb = SSDOM.getNextNode(jOb, false, null);
				}
			} else {
				jOb = SSDOM.getNextNode(jOb, false, null);
			}
		}
		if (Zda) {
			jOb = OXb;
			while (jOb != null) {
				if (jOb.nodeType == 3) {
					var AEb = jOb.nodeValue.trimTH().length > 0;
					if (AEb) {
						var BEb = jOb.parentNode;
						var CEb = BEb.getAttribute("id");
						if (CEb == null || CEb.length == 0) {
							BEb.id = "rwTHnoteMarker" + mfa;
							++mfa;
						}
					}
					jOb = SSDOM.getNextNode(jOb, false, null);
				} else if (jOb.nodeType == 1) {
					if (gkb(jOb)) {
						var DEb = jOb.getAttribute("id");
						if (DEb == null || DEb.length == 0) {
							jOb.id = "rwTHnoteMarker" + mfa;
							++mfa;
						}
					}
					var EEb = jOb.getAttribute(caa);
					var FEb = jOb.getAttribute(baa);
					if (jOb.tagName.toLowerCase() == "pre" || (EEb != null && EEb.length > 0) || (FEb != null && FEb.length > 0)) {
						jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, null);
					} else {
						jOb = SSDOM.getNextNode(jOb, false, null);
					}
				} else {
					jOb = SSDOM.getNextNode(jOb, false, null);
				}
			}
		}
	} catch (exception) {
		eya("Error in $rw_tagSentences: " + exception);
	}
	Aea = true;
}
function JEb(LPb, Ueb, Bub) {
	var GEb = true;
	var UBc = LPb.length;
	if (UBc > Ueb + 1) {
		var uXb = LPb.charCodeAt(Ueb + 1);
		if (Bya(uXb)) {
			GEb = false;
		}
		if (qfa && uXb == 61) {
			if (ZVb("!=")) {
				if (Bub != null && Bub.nodeValue.charAt(Ueb) == '!') {
					GEb = false;
				}
			}
		}
	}
	if (GEb) {
		if (Bub != null && Bub.nodeValue.charAt(Ueb) != '.') {
			return true;
		}
	}
	if (GEb) {
		if (Ueb > 1) {
			var VAc = LPb.substring(Ueb - 2, Ueb);
			if ((VAc.charAt(0) == ' ' || VAc.charAt(0) == '\n' || VAc.charAt(0) == '\r' || VAc.charAt(0) == '\t') && VAc.charCodeAt(1) > 63 && VAc.charCodeAt(1) < 91) {
				GEb = false;
			} else if (VAc.charAt(0) == '.' && Bya(VAc.charCodeAt(1))) {
				GEb = false;
			} else {
				if (VAc == "Dr" || VAc == "Mr" || VAc == "Ms" || VAc == "Av" || VAc == "St" || VAc == "eg") {
					GEb = false;
				} else if (Ueb > 2) {
					var UAc = LPb.substring(Ueb - 3, Ueb);
					if (UAc == "Mrs" || UAc == "etc" || UAc == "i.e" || UAc == "P.O" || UAc == "PhD") {
						GEb = false;
					} else if (Ueb > 3) {
						var PEb = LPb.substring(Ueb - 4, Ueb);
						if (PEb == "Ph.D") {
							GEb = false;
						}
					}
				}
			}
		} else {
			try {
				if (Bub != null && Ueb == 0) {
					if (sda) {
						var EPb = null;
						if (Bub.previousSibling != null && Bub.previousSibling.nodeType == 1) {
							EPb = Bub.previousSibling;
						} else if (Bub.parentNode.previousSibling != null && Bub.parentNode.previousSibling.nodeType == 1) {
							EPb = Bub.parentNode.previousSibling;
						} else {
							var jOb = Bub.parentNode;
							while (jOb != null && jOb.parentNode.tagName.toLowerCase() == "span") {
								if (jOb.parentNode.previousSibling != null && jOb.parentNode.previousSibling.nodeType == 1) {
									EPb = jOb.parentNode.previousSibling;
									break;
								}
								jOb = jOb.parentNode;
							}
						}
						if (EPb != null) {
							if (EPb.nodeType == 1 && EPb.tagName.toLowerCase() == "span") {
								VRb = EPb.getAttribute(("class"));
								APb = EPb.getAttribute(("className"));
								var CPb;
								CPb = (VRb != null && (VRb.toLowerCase() == "x2" || VRb.toLowerCase() == "x3")) || (APb != null && (APb.toLowerCase() == "x2" || APb.toLowerCase() == "x3"));
								if (CPb) {
									if (EPb.lastChild.nodeType == 1) {
										while (EPb != null && EPb.lastChild != null && EPb.lastChild.nodeType != 3) {
											EPb = EPb.lastChild;
										}
									}
									if (EPb != null && EPb.lastChild.nodeType == 3 && EPb != Bub) {
										return JEb(EPb.lastChild.nodeValue + LPb, EPb.lastChild.nodeValue.length, null);
									} else {
										return true;
									}
								}
							}
						}
					} else {
						var wvb = SSDOM.getPreviousTextNode(Bub, true, null);
						if (wvb != null && wvb.nodeType == 3 && wvb != Bub) {
							if (!JEb(wvb.nodeValue + LPb, wvb.nodeValue.length, null)) {
								return false;
							} else {
								return true;
							}
						}
					}
				}
			} catch (err) {}

		}
	}
	if (GEb && eba_abbr_array != null && typeof(eba_abbr_array) == "object" && typeof(eba_abbr_array.length) == "number") {
		var UEb = eba_abbr_array.length;
		var i;
		var VEb;
		for (i = 0; i < UEb; i++) {
			VEb = eba_abbr_array[i];
			if (typeof(VEb) == "string") {
				if (Ueb - VEb.length > -1) {
					if (LPb.substring(Ueb - VEb.length, Ueb) == VEb) {
						GEb = false;
						break;
					}
				}
			}
		}
	}
	return GEb;
}
var WEb = null;
function XEb(event) {
	WEb = event.currentTarget;
}
function YEb(rBc, eventType, Nsb) {
	if (gfa) {
		if (rBc.document && rBc.document.body && rBc.document.body.screen) {
			return true;
		}
		if (rBc.location) {
			return true;
		}
	}
	return ZEb(rBc, eventType, Nsb);
}
function ZEb(rBc, eventType, Nsb) {
	if (rBc.addEventListener) {
		if (Qga) {
			rBc.addEventListener(eventType, Nsb, true);
		} else {
			rBc.addEventListener(eventType, Nsb, false);
		}
		return true;
	} else if (rBc.attachEvent) {
		return rBc.attachEvent("on" + eventType, Nsb);
	} else {
		return false;
	}
}
function aEb(ev) {
	if (ffa) {
		return {
			x : (ev.touches[0].pageX - document.body.scrollLeft),
			y : (ev.touches[0].pageY - document.body.scrollTop)
		};
	}
	if (ev.pageX) {
		if (Jfa) {
			return {
				x : (ev.pageX - document.documentElement.scrollLeft),
				y : (ev.pageY - document.documentElement.scrollTop)
			};
		} else if (Kfa) {
			return {
				x : (ev.pageX - document.body.parentNode.scrollLeft),
				y : (ev.pageY - document.body.parentNode.scrollTop)
			};
		} else {
			return {
				x : (ev.pageX - document.body.scrollLeft),
				y : (ev.pageY - document.body.scrollTop)
			};
		}
	} else {
		return {
			x : ev.clientX,
			y : ev.clientY
		};
	}
}
function cEb(mpb) {
	var left = 0;
	var top = 0;
	if (typeof(jQuery) != "undefined" && typeof(mpb.id) == "string" && mpb.id.indexOf("summary") == 0) {
		var el = $(mpb);
		if (el != null && el.length && el.length > 0) {
			var mqb = el.position();
			left = mqb.left;
			top = mqb.top;
			left -= rw_getScreenOffsetLeft();
			top -= rw_getScreenOffsetTop();
			return {
				x : left,
				y : top
			};
		}
	}
	if (mpb.nodeType == 3) {
		mpb = mpb.parentNode;
	}
	while (mpb.offsetParent) {
		left += mpb.offsetLeft + (mpb.currentStyle ? (parseInt(mpb.currentStyle.borderLeftWidth, 10)).NaN0() : 0);
		top += mpb.offsetTop + (mpb.currentStyle ? (parseInt(mpb.currentStyle.borderTopWidth, 10)).NaN0() : 0);
		mpb = mpb.offsetParent;
	}
	left += mpb.offsetLeft + (mpb.currentStyle ? (parseInt(mpb.currentStyle.borderLeftWidth, 10)).NaN0() : 0);
	top += mpb.offsetTop + (mpb.currentStyle ? (parseInt(mpb.currentStyle.borderTopWidth, 10)).NaN0() : 0);
	left -= rw_getScreenOffsetLeft();
	top -= rw_getScreenOffsetTop();
	return {
		x : left,
		y : top
	};
}
var fEb = false;
var gEb = false;
var hEb = false;
function $rw_isSpeaking() {
	return fEb;
}
var iEb = "funplay play cyan magenta yellow green strike clear collect trans ffinder dictionary picturedictionary vocabulary";
var jEb = "cyan magenta yellow green strike clear collect";
var kEb = "spell homophone pred";
var lEb = "funplay play hover";
var mEb = 0;
function pEb(uEb) {
	if (mEb > 0) {
		clearTimeout(mEb);
		mEb = 0;
	}
	if ($g_bMouseSpeech) {
		uEb = true;
	}
	if (gEb == uEb) {
		return;
	}
	try {
		var nEb = SpeechStream.EnumIconParameter;
		for (var i = 0; i < nea; i++) {
			var Hpb = g_icons[i][nEb.ICON_NAME];
			if (iEb.indexOf(Hpb) > -1) {
				if (uEb) {
					zza(g_icons[i][nEb.ICON_NAME], "mask", g_icons[i][nEb.ICON_OFFSET], false);
				} else {
					zza(g_icons[i][nEb.ICON_NAME], "flat", g_icons[i][nEb.ICON_OFFSET], false);
				}
			}
		}
		gEb = uEb;
	} catch (err) {
		thLogE(err);
	}
}
function rEb(uEb) {
	if (Sea != null) {
		if (hEb == uEb) {
			return;
		}
		try {
			if (uEb) {
				tza("speaker" + Sea, "on");
			} else {
				tza("speaker" + Sea, "off");
				Sea = null;
			}
			hEb = uEb;
		} catch (err) {
			thLogE(err);
		}
	}
}
function tEb(uEb) {
	fEb = uEb;
}
function xEb() {
	try {
		var rmb = SpeechStream.EnumIconParameter;
		for (var i = 0; i < nea; i++) {
			var Hpb = g_icons[i][rmb.ICON_NAME];
			if (jEb.indexOf(Hpb) > -1) {
				zza(g_icons[i][rmb.ICON_NAME], "flat", g_icons[i][rmb.ICON_OFFSET], false);
			}
		}
		for (var i = 0; i < oea; i++) {
			var Hpb = mea[i][rmb.ICON_NAME];
			if (kEb.indexOf(Hpb) > -1) {
				zza(g_icons[i][rmb.ICON_NAME], "mask", g_icons[i][rmb.ICON_OFFSET], true);
			}
		}
	} catch (err) {
		thLogE(err);
	}
}
function AFb(kxb) {
	return document.getElementById(kxb);
}
function $speechFinishedInFlash() {
	pEb(false);
	rEb(false);
	tEb(false);
}
Number.prototype.NaN0 = function () {
	return isNaN(this) ? 0 : this;
};
function CFb() {
	if (!DFb()) {
		setTimeout(CFb, 250);
	} else {
		$jq = jQuery.noConflict(true);
	}
}
function DFb() {
	var zEb = typeof(jQuery) !== "undefined";
	if (zEb) {
		var EFb = jQuery().jquery.split(".");
		return parseInt(EFb[0]) >= 1 && parseInt(EFb[1]) >= 8;
	}
	return false;
}
SpeechStream.Highlighter = function () {
	var self = this;
	this.dom = null;
	if (typeof(SSDOM) != "undefined") {
		self.dom = SSDOM;
	}
	this.setDom = function (p_dom) {
		self.dom = p_dom;
	};
	this.fob = null;
	if (typeof(aga) != "undefined") {
		self.fob = aga;
	}
	this.setData = function (p_dat) {
		self.fob = p_dat;
	};
	var FFb = "span";
	var GFb = "thhom";
	this.getTagToUse = function () {
		return aga.pageData.strHighlightTag;
	};
	this.setTagToUse = function (p_strTagToUse) {
		aga.pageData.strHighlightTag = p_strTagToUse;
		FFb = p_strTagToUse;
	};
	this.getHomClass = function () {
		return GFb;
	};
	this.setHomClass = function (p_strClassToUse) {
		GFb = p_strClassToUse;
	};
};
SpeechStream.highlighter = new SpeechStream.Highlighter();
var HFb = SpeechStream.highlighter;
function $rw_setSpeechRangeStyle(lHb) {
	HFb.fob.highlightData.strSpeechRangeColours = lHb;
}
function $rw_setSpeechWordStyle(lHb) {
	HFb.fob.highlightData.strSpeechWordColours = lHb;
}
function $rw_getSpeechRangeStyle() {
	return HFb.fob.highlightData.strSpeechRangeColours;
}
function $rw_getSpeechWordStyle() {
	return HFb.fob.highlightData.strSpeechWordColours;
}
var $rw_setSpeechRangeColours = $rw_setSpeechRangeStyle;
var $rw_setSpeechWordColours = $rw_setSpeechWordStyle;
var $rw_getSpeechRangeColours = $rw_getSpeechRangeStyle;
var $rw_getSpeechWordColours = $rw_getSpeechWordStyle;
function rw_setHighlight(fNb, nGb, mTb, oGb, rfb) {
	var xFb = fNb;
	var yFb = mTb;
	try {
		var result = null;
		if (mTb == fNb) {
			result = rw_setNodeBackground(fNb, nGb, oGb, "ss", rfb);
			xFb = result.node;
			yFb = result.node;
		} else {
			if (nGb > 0) {
				result = rw_setNodeBackground(fNb, nGb, fNb.nodeValue.length, "ss", rfb);
			} else {
				result = rw_setNodeBackground(fNb, -1, -1, "ss", rfb);
			}
			xFb = result.node;
			var wvb = HFb.dom.getNextTextNodeNoImg(result.node, false, mTb, true);
			while (wvb != null) {
				if (wvb == mTb) {
					result = rw_setNodeBackground(wvb, 0, oGb, "ss", rfb);
					wvb = result.node;
					yFb = wvb;
					break;
				} else {
					result = rw_setNodeBackground(wvb, -1, -1, "ss", rfb);
					wvb = result.node;
				}
				yFb = wvb;
				wvb = HFb.dom.getNextTextNodeNoImg(wvb, false, mTb, true);
			}
		}
	} catch (err) {
		eya("rw_setHighlight error:" + err.message);
	}
	return {
		start : xFb,
		end : yFb
	};
}
var UFb = 1;
var VFb = 1;
var WFb = 1;
function YFb(fNb, nGb, mTb, oGb) {
	var Zhb = "" + UFb++;
	var xFb = fNb;
	var yFb = mTb;
	try {
		var result = null;
		if (mTb == fNb) {
			result = rw_setNodeBackground(fNb, nGb, oGb, "spell", Zhb);
			xFb = result.node;
			yFb = result.node;
		} else {
			if (nGb > 0) {
				result = rw_setNodeBackground(fNb, nGb, fNb.nodeValue.length, "spell", Zhb);
			} else {
				result = rw_setNodeBackground(fNb, -1, -1, "spell", Zhb);
			}
			xFb = result.node;
			var wvb = HFb.dom.getNextTextNodeNoImg(result.node, false, mTb, true);
			while (wvb != null) {
				if (wvb == mTb) {
					result = rw_setNodeBackground(wvb, 0, oGb, "spell", Zhb);
					wvb = result.node;
					yFb = wvb;
					break;
				} else {
					result = rw_setNodeBackground(wvb, -1, -1, "spell", Zhb);
					wvb = result.node;
				}
				yFb = wvb;
				wvb = HFb.dom.getNextTextNodeNoImg(wvb, false, mTb, true);
			}
		}
	} catch (err) {
		eya("rw_setHighlight error:" + err.message);
	}
	return {
		start : xFb,
		end : yFb
	};
}
function iFb(fNb, nGb, mTb, oGb) {
	var Zhb = "" + VFb++;
	var xFb = fNb;
	var yFb = mTb;
	try {
		var result = null;
		if (mTb == fNb) {
			result = rw_setNodeBackground(fNb, nGb, oGb, "hom", Zhb);
			xFb = result.node;
			yFb = result.node;
		} else {
			if (nGb > 0) {
				result = rw_setNodeBackground(fNb, nGb, fNb.nodeValue.length, "hom", Zhb);
			} else {
				result = rw_setNodeBackground(fNb, -1, -1, "hom", Zhb);
			}
			xFb = result.node;
			var wvb = HFb.dom.getNextTextNodeNoImg(result.node, false, mTb, true);
			while (wvb != null) {
				if (wvb == mTb) {
					result = rw_setNodeBackground(wvb, 0, oGb, "hom", Zhb);
					wvb = result.node;
					yFb = wvb;
					break;
				} else {
					result = rw_setNodeBackground(wvb, -1, -1, "hom", Zhb);
					wvb = result.node;
				}
				yFb = wvb;
				wvb = HFb.dom.getNextTextNodeNoImg(wvb, false, mTb, true);
			}
		}
	} catch (err) {
		eya("rw_setHighlight error:" + err.message);
	}
	return {
		start : xFb,
		end : yFb
	};
}
function sFb(fNb, nGb, mTb, oGb) {
	var Zhb = "" + WFb++;
	var xFb = fNb;
	var yFb = mTb;
	try {
		var result = null;
		if (mTb == fNb) {
			result = rw_setNodeBackground(fNb, nGb, oGb, "grammar", Zhb);
			xFb = result.node;
			yFb = result.node;
		} else {
			if (nGb > 0) {
				result = rw_setNodeBackground(fNb, nGb, fNb.nodeValue.length, "grammar", Zhb);
			} else {
				result = rw_setNodeBackground(fNb, -1, -1, "grammar", Zhb);
			}
			xFb = result.node;
			var wvb = HFb.dom.getNextTextNodeNoImg(result.node, false, mTb, true);
			while (wvb != null) {
				if (wvb == mTb) {
					result = rw_setNodeBackground(wvb, 0, oGb, "grammar", Zhb);
					wvb = result.node;
					yFb = wvb;
					break;
				} else {
					result = rw_setNodeBackground(wvb, -1, -1, "grammar", Zhb);
					wvb = result.node;
				}
				yFb = wvb;
				wvb = HFb.dom.getNextTextNodeNoImg(wvb, false, mTb, true);
			}
		}
	} catch (err) {
		eya("rw_setHighlight error:" + err.message);
	}
	return {
		start : xFb,
		end : yFb
	};
}
function HGb(WGb) {
	try {
		if (WGb == null || !(WGb instanceof Array) || WGb.length == 0) {
			return;
		}
		for (var i = 0; i < WGb.length; i++) {
			var mBc = WGb[i];
			if (JGb(mBc)) {
				var hHb = mBc.parentNode;
				if (mBc.nextSibling != null || mBc.previousSibling != null) {
					var bub = SSDOM.allTextFromNodeTH(hHb);
					var Jwb = hHb.ownerDocument;
					mBc = Jwb.createTextNode(bub);
				}
				var cGb = hHb.parentNode;
				cGb.replaceChild(mBc, hHb);
				mBc = HFb.dom.mergeTextNodes(mBc);
				WGb[i] = mBc;
			} else {
				var SGb = MGb(mBc);
				if (SGb != null) {
					SGb.removeAttribute("rwstate");
					SGb.removeAttribute("style");
				}
			}
		}
		if (!aga.controlData.bSpeakingFlag) {
			rw_removeSpeechHighlight(WGb, false);
		}
	} catch (err) {
		eya("Error in rw_setHighlight: " + err.message);
	}
}
function JGb(Bub) {
	if (Bub.parentNode == null || Bub.parentNode.parentNode == null) {
		return false;
	}
	var parent = Bub.parentNode;
	var attr = parent.getAttribute("rwstate");
	if (parent.tagName.toLowerCase() != SpeechStream.highlighter.getTagToUse() || attr == null || attr == "csp" || attr == "sp") {
		return parent.className == "thspell" || parent.className == SpeechStream.highlighter.getHomClass() || parent.className == "thgrammar";
	}
	return true;
}
function MGb(Bub) {
	if (Bub.nodeType != 3 || Bub.parentNode == null || Bub.parentNode.parentNode == null) {
		return null;
	}
	var gHb = SpeechStream.highlighter.getTagToUse();
	var parent = Bub.parentNode;
	var attr = parent.getAttribute("rwstate");
	if (parent.tagName.toLowerCase() != gHb || attr == null || attr != "ss") {
		if (parent.getAttribute("rwthgen") != null) {
			var Orb = parent;
			attr = "1";
			while (attr != null) {
				Orb = Orb.parentNode;
				if (Orb.getAttribute("rwState") == "ss" && Orb.tagName.toLowerCase() == gHb) {
					return Orb;
				}
				attr = Orb.getAttribute("rwthgen");
			}
		}
		return null;
	}
	return parent;
}
function TGb(WGb) {
	try {
		if (WGb == null || !(WGb instanceof Array) || WGb.length == 0) {
			return;
		}
		for (var i = 0; i < WGb.length; i++) {
			var mBc = WGb[i];
			if (mBc == null) {
				continue;
			}
			if (JGb(mBc)) {
				var hHb = mBc.parentNode;
				var cGb = hHb.parentNode;
				if (mBc.nextSibling != null || mBc.previousSibling != null) {
					var node = hHb.firstChild;
					while (node != null) {
						mBc = node;
						node = node.nextSibling;
						cGb.insertBefore(mBc, hHb);
					}
					cGb.removeChild(hHb);
				} else {
					cGb.replaceChild(mBc, hHb);
				}
			} else {
				var SGb = MGb(mBc);
				if (SGb != null) {
					SGb.removeAttribute("rwstate");
					SGb.removeAttribute("style");
				}
			}
		}
	} catch (err) {
		eya("Error in rw_setHighlight: " + err.message);
	}
}
function rw_removeSpeechHighlight(WGb, fGb) {
	try {
		if (typeof(fGb) == "undefined") {
			fGb = false;
		}
		if (WGb == null || !(WGb instanceof Array) || WGb.length == 0) {
			return;
		}
		for (var i = 0; i < WGb.length; i++) {
			var mBc = WGb[i];
			if (dGb(mBc, fGb)) {
				var hHb = mBc.parentNode;
				if (mBc.nextSibling != null || mBc.previousSibling != null) {
					var bub = SSDOM.allTextFromNodeTH(hHb);
					var Jwb = hHb.ownerDocument;
					mBc = Jwb.createTextNode(bub);
				}
				var cGb = hHb.parentNode;
				cGb.replaceChild(mBc, hHb);
				mBc = SSDOM.mergeTextNodes(mBc);
				WGb[i] = mBc;
				if (dGb(mBc, fGb)) {
					--i;
				}
			} else {
				var tagName = "";
				if (mBc.nodeType == 1) {
					tagName = mBc.tagName.toLowerCase();
				}
				if (tagName == "math") {
					iHb(mBc, null, null, false);
				}
				if (mBc.isMathJax) {
					iHb(mBc, null, null, false);
				}
			}
		}
	} catch (err) {
		eya("rw_removeSpeechHighlight failed error:" + err.message);
	}
}
function dGb(Bub, fGb) {
	if (Bub.nodeType != 3 || Bub.parentNode == null || Bub.parentNode.parentNode == null) {
		return false;
	}
	var parent = Bub.parentNode;
	var attr = parent.getAttribute("rwstate");
	if (parent.tagName.toLowerCase() == SpeechStream.highlighter.getTagToUse() && attr != null) {
		if ((!fGb && attr == "sp") || attr == "csp") {
			return true;
		}
	}
	return false;
}
function rw_setSpeechRangeImpl(fNb, nGb, mTb, oGb, pGb) {
	var result = null;
	try {
		if (mTb == fNb) {
			result = rw_setNodeBackground(fNb, nGb, oGb, pGb, "");
			return result;
		}
		if (nGb > 0) {
			result = rw_setNodeBackground(fNb, nGb, fNb.nodeValue.length, pGb, "");
		} else {
			result = rw_setNodeBackground(fNb, -1, -1, pGb, "");
		}
		var wvb = HFb.dom.getNextTextNodeNoImg(result.node, false, mTb, true);
		while (wvb != null) {
			if (wvb == mTb) {
				result = rw_setNodeBackground(wvb, 0, oGb, pGb, "");
				wvb = result.node;
				break;
			} else {
				result = rw_setNodeBackground(wvb, -1, -1, pGb, "");
				wvb = result.node;
			}
			wvb = HFb.dom.getNextTextNodeNoImg(wvb, false, mTb, true);
		}
	} catch (err) {
		eya("rw_setSpeechRangeImpl error:" + err.message);
	}
	return result;
}
function rGb() {
	this.node = null;
	this.offset = 0;
}
function rw_setNodeBackground(LHb, MHb, NHb, OHb, PHb) {
	var kSb = new rGb();
	kSb.node = LHb;
	kSb.offset = MHb;
	if (LHb.nodeType != 3) {
		if (LHb.nodeType == 1 && HFb.dom.isSpecialCaseHighlightable(LHb)) {
			if (LHb.tagName.toLowerCase() == "math") {
				var hHb = LHb.parentNode;
				kSb = JHb(hHb, LHb, MHb, NHb, OHb, "");
			} else if (LHb.isMathJax) {
				var hHb = LHb.parentNode;
				kSb = JHb(hHb, LHb, MHb, NHb, OHb, "");
			} else {
				var BHb = HFb.dom.getFirstChildTextNode(LHb, false);
				var CHb = HFb.dom.getLastChildTextNode(LHb, false);
				if (BHb != null && BHb.nodeType == 3 && CHb != null && CHb.nodeType == 3) {
					rw_setSpeechRangeImpl(BHb, 0, CHb, CHb.nodeValue.length, OHb);
					kSb.node = BHb;
					kSb.offset = 0;
				} else {}

			}
			return kSb;
		} else {
			return kSb;
		}
	}
	if (LHb.nodeType == 3) {
		var bub = LHb.nodeValue;
		bub = bub.trimTH();
		if (bub.length == 0) {
			var qlb = LHb.parentNode;
			if (qlb != null) {
				var RAc = qlb.tagName.trimTH().toLowerCase();
				if (RAc == "tr" || RAc == "table") {
					return kSb;
				}
			}
		}
	}
	var hHb = LHb.parentNode;
	var HHb = null;
	if (hHb.tagName.toLowerCase() == SpeechStream.highlighter.getTagToUse()) {
		HHb = hHb.getAttribute("rwstate");
	}
	if (OHb == "spell") {
		if (HHb == null || HHb == "") {
			kSb = JHb(hHb, LHb, MHb, NHb, OHb, PHb);
		} else {
			return kSb;
		}
	} else if (OHb == "hom") {
		if (HHb == null || HHb == "") {
			kSb = JHb(hHb, LHb, MHb, NHb, OHb, PHb);
		} else {
			return kSb;
		}
	} else if (OHb == "grammar") {
		if (HHb == null || HHb == "") {
			kSb = JHb(hHb, LHb, MHb, NHb, OHb, PHb);
		} else {
			return kSb;
		}
	} else if (OHb == "ss") {
		if (HHb == null || HHb == "") {
			kSb = JHb(hHb, LHb, MHb, NHb, OHb, PHb);
		} else if (HHb == "ss") {
			return kSb;
		} else {
			return kSb;
		}
	} else if (OHb == "sp") {
		if (HHb == "csp") {
			eya("fail in rw_setNodeBackground setting sp to csp");
			return kSb;
		}
		if (HHb == "sp") {
			eya("fail in rw_setNodeBackground setting sp to sp");
			return kSb;
		}
		kSb = JHb(hHb, LHb, MHb, NHb, OHb, "");
	} else if (OHb == "csp") {
		if (HHb == "csp") {
			eya("fail parent is csp for csp");
			return kSb;
		}
		if (HHb == "sp") {
			kSb = JHb(hHb, LHb, MHb, NHb, OHb, "");
		} else {}

	} else {}

	return kSb;
}
function JHb(KHb, LHb, MHb, NHb, OHb, PHb) {
	var IHb = OHb === "sp" || OHb === "csp";
	var tagName = "";
	if (LHb.nodeType == 1) {
		tagName = LHb.tagName.toLowerCase();
	}
	if ((LHb.nodeType == 3 && (NHb == -1 || NHb > MHb)) || tagName == "math" || LHb.isMathJax) {
		var QHb;
		if (OHb == "ss") {
			if (PHb == "strikethrough") {
				QHb = "text-decoration:line-through";
			} else {
				QHb = "background:" + PHb;
			}
		} else if (OHb == "sp") {
			QHb = HFb.fob.highlightData.strSpeechRangeColours;
		} else if (OHb == "csp") {
			QHb = HFb.fob.highlightData.strSpeechWordColours;
		} else if (OHb == "spell") {
			QHb = "";
		} else if (OHb == "hom") {
			QHb = "";
		} else if (OHb == "grammar") {
			QHb = "";
		} else {
			QHb = "color:#ff000; background:#00ff00";
		}
		if (tagName == "math" || LHb.isMathJax) {
			if (IHb) {
				iHb(LHb, OHb, QHb, true);
			}
		} else {
			var UBc = LHb.nodeValue.length;
			if ((UBc == 1 && (LHb.nodeValue == "\n" || LHb.nodeValue == "\r")) || (UBc == 2 && LHb.nodeValue == "\r\n")) {
				var kSb = new rGb();
				kSb.node = LHb;
				if (MHb < 0) {
					kSb.offset = 0;
				} else {
					kSb.offset = MHb;
				}
				return kSb;
			}
			var Jwb = KHb.ownerDocument;
			var Znb = false;
			if (MHb == -1 && NHb == -1) {
				Znb = true;
			} else if (NHb == -1) {
				NHb = UBc;
			}
			if (MHb == 0 && NHb >= UBc) {
				Znb = true;
			}
			var pHb = Jwb.createElement(aga.pageData.strHighlightTag);
			if (OHb == "spell") {
				pHb.setAttribute("spellnum", PHb);
				if (HFb.fob.browser.bIE) {
					pHb.setAttribute("className", "thspell");
					QHb = "background:url(\"" + Dza(false) + eba_language_server + "/SupportedWritingArea/files/tiny_mce/themes/advanced/skins/default/img/wline.gif\")  repeat-x bottom left";
				} else {
					pHb.className = "thspell";
				}
			}
			if (OHb == "hom") {
				var XHb = SpeechStream.highlighter.getHomClass();
				pHb.setAttribute("homnum", PHb);
				if (HFb.fob.browser.bIE) {
					pHb.setAttribute("className", XHb);
					QHb = "background:url(\"" + Dza(false) + eba_language_server + "/SupportedWritingArea/files/tiny_mce/themes/advanced/skins/default/img/wlineblue.gif\")  repeat-x bottom left";
				} else {
					pHb.className = XHb;
				}
			}
			if (OHb == "grammar") {
				var XHb = "thgrammar";
				pHb.setAttribute("grammarnum", PHb);
				if (HFb.fob.browser.bIE) {
					pHb.setAttribute("className", "thgrammar");
					QHb = "background:url(\"" + Dza(false) + eba_language_server + "/SupportedWritingArea/files/tiny_mce/themes/advanced/skins/default/img/wlinegreen.gif\")  repeat-x bottom left";
				} else {
					pHb.className = XHb;
				}
			}
			if (Znb) {
				if (HFb.fob.browser.bIEOld) {
					pHb.style.setAttribute("cssText", QHb, 0);
				} else {
					pHb.setAttribute("style", QHb);
				}
				pHb.setAttribute("rwstate", OHb);
				if (IHb) {
					pHb.setAttribute("started", "1");
				}
				KHb.replaceChild(pHb, LHb);
				pHb.appendChild(LHb);
			} else {
				var bub = LHb.nodeValue;
				var ZHb;
				var aHb;
				var bHb;
				if (KHb.tagName.toLowerCase() == "span" && KHb.getAttribute("pron") != null) {
					ZHb = "";
					aHb = bub;
					bHb = "";
				} else {
					ZHb = bub.substring(0, MHb);
					aHb = bub.substring(MHb, NHb);
					bHb = bub.substring(NHb);
				}
				if (HFb.fob.browser.bIEOld) {
					pHb.style.setAttribute("cssText", QHb, 0);
				} else {
					pHb.setAttribute("style", QHb);
				}
				pHb.setAttribute("rwstate", OHb);
				if (IHb) {
					pHb.setAttribute("started", "1");
				}
				var cHb = null;
				var lmb;
				var eHb = null;
				if (ZHb.length > 0) {
					cHb = Jwb.createTextNode(ZHb);
				}
				lmb = Jwb.createTextNode(aHb);
				if (bHb.length > 0) {
					eHb = Jwb.createTextNode(bHb);
				}
				pHb.appendChild(lmb);
				KHb.replaceChild(pHb, LHb);
				if (cHb != null) {
					KHb.insertBefore(cHb, pHb);
				}
				if (eHb != null) {
					if (pHb.nextSibling == null) {
						KHb.insertBefore(eHb, null);
					} else {
						KHb.insertBefore(eHb, pHb.nextSibling);
					}
				}
				LHb = lmb;
			}
		}
	}
	var kSb = new rGb();
	kSb.node = LHb;
	if (MHb < 0) {
		kSb.offset = 0;
	} else {
		kSb.offset = MHb;
	}
	return kSb;
}
function iHb(jHb, kHb, lHb, mHb) {
	if (jHb == null) {
		return;
	}
	var gHb = aga.pageData.strHighlightTag;
	if (HFb.fob.browser.bIE) {
		var hHb = jHb.parentNode;
		if (hHb == null) {
			return;
		}
		if (hHb.tagName.toLowerCase() == gHb && hHb.getAttribute("started") != null) {
			if (mHb) {
				hHb.style.setAttribute("cssText", lHb, 0);
				hHb.setAttribute("rwstate", kHb);
			} else {
				var nHb = hHb.parentNode;
				if (nHb == null) {
					return;
				}
				nHb.replaceChild(jHb, hHb);
			}
		} else {
			if (mHb) {
				var oHb = document.createElement(gHb);
				oHb.style.setAttribute("cssText", lHb, 0);
				oHb.setAttribute("started", "1");
				oHb.setAttribute("rwstate", kHb);
				hHb.replaceChild(oHb, jHb);
				oHb.appendChild(jHb);
			}
		}
	} else {
		var pHb = jHb.firstChild;
		while (pHb != null) {
			if (pHb.nodeType == 1) {
				if (mHb) {
					if (pHb.getAttribute("started") != null) {
						pHb.setAttribute("style", lHb);
						pHb.setAttribute("rwstate", kHb);
					} else {
						if (pHb.getAttribute("style") == null) {
							pHb.setAttribute("style", lHb);
							pHb.setAttribute("rwstate", kHb);
							pHb.setAttribute("started", "1");
						}
					}
				} else {
					if (pHb.getAttribute("started") != null) {
						pHb.removeAttribute("style");
						pHb.removeAttribute("started");
						pHb.removeAttribute("rwstate");
					}
				}
			}
			pHb = pHb.nextSibling;
		}
	}
}
function sHb(tHb, uHb) {
	if (tHb == uHb) {
		return 0;
	}
	var qHb = SSDOM.getRangeObject(tHb.ownerDocument.body);
	qHb.setStart(tHb, 0);
	qHb.setEnd(tHb, 0);
	var rHb = SSDOM.getRangeObject(tHb.ownerDocument.body);
	rHb.setStart(uHb, 0);
	rHb.setEnd(uHb, 0);
	return (qHb.compareBoundaryPoints("START_TO_START", rHb));
}
function vHb(event) {}

function wHb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	var target = event.target || event.srcElement;
	if (target.id == 'rwDragMe' || target.id == 'rwToolbarTitleRW' || target.id == 'rwDragMeDisplay' || target.id == 'rwDragMeTrans' || target.id == 'rwDragMeFF' || target.id == 'rwDragMeDict' || target.id == 'rwDragMeCollect' || target.id == 'rwDragMeStickyNoteTop' || target.id == 'rwDragMeStickyNoteBot' || target.id == 'rwDragMePronCreate' || target.id == 'rwDragMePronEdit' || target.id == 'rwDragMeCal' || target.id == 'rwDragMePictureDictionary' || target.id == 'rwDragMeRec' || target.id == 'rwDragMePrediction' || target.id == 'rwDragMeSpelling' || target.id == 'rwDragMeHomophone' || target.id == 'rwDragMeGrammar') {
		sea = target;
		tea = true;
		if (Qga) {
			if (target.id == 'rwToolbarTitleRW') {
				sea.parentElement.parentElement.style.opacity = 0.5;
			} else {
				sea.parentElement.style.opacity = 0.5;
			}
		}
		if (sea.setCapture) {
			sea.setCapture(true);
		}
		rea = cEb(sea);
		if (target.id == 'rwDragMeStickyNoteBot') {
			rea.y -= target.offsetTop;
		}
		qea = aEb(event);
		return false;
	}
}
function xHb(event) {
	SpeechStream.ipadUtils.tinyMceBlurFix();
	wHb(event);
}
function yHb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	var target = event.target || event.srcElement;
	if (target.tagName == "IMG" && target.className == "rwIcon") {
		cIb(event);
		return false;
	}
}
function AIb(event) {
	SpeechStream.ipadUtils.tinyMceBlurFix();
	yHb(event);
}
var zHb = 0;
function CIb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	var EKb = (new Date).getTime();
	if ((EKb - zHb) < Aha) {
		return;
	}
	zHb = EKb;
	if (event != null) {
		if (!tea) {
			if ($g_bMouseSpeech && Vca) {
				EMb(event);
			}
			if (nfa) {
				Gkb(event);
			}
		}
	}
}
function DIb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	if (event != null) {
		if (!tea) {
			if (Yfa) {
				if ($g_bMouseSpeech && !Vca) {
					EMb(event);
				} else if (aga.bubbleData.bBubbleMode) {
					SpeechStream.bubbleSpeech.mouseHoverBubblePopup(event);
				} else if (((jca & calculator_icon) == calculator_icon) && typeof(aob) == "function" && aob()) {
					if (WIb(event)) {
						EMb(event, true);
					}
				}
			}
		}
	}
}
var EIb = -1;
var FIb = -1;
function IIb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	if (event == null) {
		return true;
	}
	if (Lfa) {
		var GIb = event.clientX;
		var HIb = event.clientY;
		if (EIb == GIb && FIb == HIb) {
			return;
		}
		EIb = GIb;
		FIb = HIb;
	}
	if (sea == null) {
		if ($g_bMouseSpeech && (afa || Lfa) && !Vca) {
			EMb(event);
		} else if (aga.bubbleData.bBubbleMode) {
			SpeechStream.bubbleSpeech.mouseHoverBubblePopup(event);
		} else if (((jca & calculator_icon) == calculator_icon) && typeof(aob) == "function" && aob() && (afa || Lfa)) {
			if (WIb(event)) {
				EMb(event, true);
			}
		}
		tea = false;
		return true;
	}
	var VWb = aEb(event);
	if (!efa) {
		if (VWb.x < 0 || VWb.y < 0 || VWb.x > rw_getDocumentDisplayWidth() || VWb.y > rw_getDocumentDisplayHeight()) {
			cIb(event);
			return false;
		}
	}
	var KIb;
	var LIb;
	var MIb = false;
	var NIb = 1.0;
	if (Lfa && !Jfa && Sfa) {
		var a1 = document.body.offsetWidth;
		var a2 = document.documentElement.offsetWidth;
		NIb = (a1 / a2);
		if (NIb > 1.05 || NIb < 99.5) {
			MIb = true;
		}
	}
	if (MIb && !efa) {
		var QIb = (NIb * qea.x) - (rea.x);
		var RIb = (NIb * qea.y) - (rea.y);
		KIb = (((NIb * VWb.x) - QIb)) / NIb;
		LIb = (((NIb * VWb.y) - RIb)) / NIb;
	} else {
		var QIb = qea.x - (rea.x);
		var RIb = qea.y - (rea.y);
		KIb = (VWb.x - QIb);
		LIb = (VWb.y - RIb);
	}
	if (sea == null) {
		return;
	}
	if (sea.id == 'rwDragMe' || sea.id == 'rwToolbarTitleRW') {
		vKb(KIb, LIb);
		if (!efa) {
			if ((KIb + Bfa + uea) > rw_getDocumentDisplayWidthAdjusted()) {
				KIb = rw_getDocumentDisplayWidthAdjusted() - Bfa - uea;
				yea = 1.0;
			}
		}
		if (KIb < uea) {
			KIb = uea;
			yea = 0.0;
		}
		if (!efa) {
			if ((LIb + Cfa + uea) > rw_getDocumentDisplayHeightAdjusted()) {
				LIb = rw_getDisplayHeightAdjusted() - Cfa - uea;
				zea = 1.0;
			}
		}
		if (LIb < uea) {
			LIb = uea;
			zea = 0.0;
		}
		jKb();
		cIb(event);
	} else if (sea.id == 'rwDragMeTrans' || sea.id == 'rwDragMeFF' || sea.id == 'rwDragMeDict' || sea.id == 'rwDragMeDisplay' || sea.id == 'rwDragMeCollect' || sea.id == 'rwDragMeStickyNoteTop' || sea.id == 'rwDragMeStickyNoteBot' || sea.id == 'rwDragMePronCreate' || sea.id == 'rwDragMePronEdit' || sea.id == 'rwDragMeCal' || sea.id == 'rwDragMeGencache' || sea.id == 'rwDragMeCheckcache' || sea.id == 'rwDragMePictureDictionary' || sea.id == 'rwDragMeRec' || sea.id == 'rwDragMePrediction' || sea.id == 'rwDragMeSpelling' || sea.id == 'rwDragMeHomophone' || sea.id == 'rwDragMeGrammar') {
		var SIb;
		if (sea.id == 'rwDragMeDisplay') {
			SIb = Oba;
		} else if (sea.id == 'rwDragMeTrans') {
			SIb = Pba;
		} else if (sea.id == 'rwDragMeFF') {
			SIb = Qba;
		} else if (sea.id == 'rwDragMeDict') {
			SIb = Rba;
		} else if (sea.id == 'rwDragMeStickyNoteTop') {
			SIb = Tba;
		} else if (sea.id == 'rwDragMeStickyNoteBot') {
			SIb = Tba;
		} else if (sea.id == 'rwDragMePronCreate') {
			SIb = Uba;
		} else if (sea.id == 'rwDragMePronEdit') {
			SIb = Vba;
		} else if (sea.id == 'rwDragMeCal') {
			SIb = Wba;
		} else if (sea.id == 'rwDragMeGencache') {
			SIb = Xba;
		} else if (sea.id == 'rwDragMeCache') {
			SIb = Yba;
		} else if (sea.id == 'rwDragMePictureDictionary') {
			SIb = Zba;
		} else if (sea.id == 'rwDragMeRec') {
			SIb = aba;
		} else if (sea.id == 'rwDragMePrediction') {
			SIb = bba;
		} else if (sea.id == 'rwDragMeSpelling') {
			SIb = cba;
		} else if (sea.id == 'rwDragMeHomophone') {
			SIb = dba;
		} else if (sea.id == 'rwDragMeGrammar') {
			SIb = eba;
		} else {
			SIb = Sba;
		}
		xKb(SIb, KIb, LIb);
		if ((KIb + Ffa[SIb] + uea) > rw_getDocumentDisplayWidthAdjusted()) {
			KIb = rw_getDocumentDisplayWidthAdjusted() - Ffa[SIb] - uea;
			Dfa[SIb] = 1.0;
		}
		if (KIb < uea) {
			KIb = uea;
			Dfa[SIb] = 0.0;
		}
		if (!efa) {
			if ((LIb + Gfa[SIb] + uea) > rw_getDocumentDisplayHeightAdjusted()) {
				LIb = rw_getDocumentDisplayHeightAdjusted() - Gfa[SIb] - uea;
				Efa[SIb] = 1.0;
			}
		}
		if (LIb < uea) {
			LIb = uea;
			Efa[SIb] = 0.0;
		}
		pKb(SIb);
		cIb(event);
	}
	return false;
}
function TIb(event) {
	SpeechStream.ipadUtils.tinyMceBlurFix();
	IIb(event);
}
function WIb(event) {
	var target;
	if (afa) {
		target = event.explicitOriginalTarget;
	} else if (Lfa) {
		target = event.srcElement;
	} else {
		target = event.target;
	}
	if (target != null && target.nodeType == 1) {
		var UIb = target.ownerDocument.body;
		if (target != UIb) {
			var Ipb = target.parentNode;
			if (typeof(Ipb.tagName) == "string") {
				while (Ipb != null && Ipb != UIb) {
					var qRb = Ipb.tagName.toLowerCase();
					if (qRb == "form") {
						if (Ipb.id == "rw_calForm") {
							return true;
						}
					}
					Ipb = Ipb.parentNode;
				}
			}
		}
	}
	return false;
}
function YIb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	if (!tea) {
		return true;
	}
	if (sea.releaseCapture) {
		sea.releaseCapture();
	}
	if (Qga) {
		if (sea.id == 'rwToolbarTitleRW') {
			sea.parentElement.parentElement.style.opacity = 1.0;
		} else {
			sea.parentElement.style.opacity = 1.0;
		}
	}
	sea = null;
	tea = false;
	if (Qga) {
		window.postMessage({
			type : "1757FROM_PAGERW4G",
			command : "saveCookiePosition",
			cookiex : Dfa,
			cookiey : Efa,
			cookieBarx : yea,
			cookieBary : zea
		}, "*");
	}
	cIb(event);
	return false;
}
function ZIb(event) {
	SpeechStream.ipadUtils.tinyMceBlurFix();
	YIb(event);
}
function bIb(event) {
	if (!aga.controlData.bOnloadFinished) {
		return;
	}
	if (tea) {
		if (afa) {
			var VWb = aEb(event);
			if (VWb.x < 5 || VWb.y < 5 || VWb.x > (rw_getDocumentDisplayWidth() - 5) || VWb.y > (rw_getDocumentDisplayHeight() - 5)) {
				YIb(event);
				cIb(event);
				return;
			}
		}
		IIb(event);
		cIb(event);
	} else {
		if (!Vca) {
			qLb = null;
		}
	}
}
function cIb(event) {
	if (event == null) {
		return;
	}
	if (event.cancelBubble) {
		event.cancelBubble = true;
	} else if (event.stopPropagation) {
		event.stopPropagation();
	}
	if (event.returnValue) {
		event.returnValue = false;
	} else if (event.preventDefault) {
		event.preventDefault(true);
	}
}
function dIb(kxb, nIb, oIb) {
	if (wea > 0) {
		--wea;
		return;
	}
	if (tea) {
		return;
	}
	if ($rw_blockClick(kxb)) {
		return;
	}
	zza(g_icons[nIb][SpeechStream.EnumIconParameter.ICON_NAME], "hover", g_icons[nIb][SpeechStream.EnumIconParameter.ICON_OFFSET], oIb);
}
function hIb(kxb, nIb, oIb) {
	if (wea > 0) {
		--wea;
		return;
	}
	if (tea) {
		return;
	}
	if ($rw_blockClick(kxb)) {
		return;
	}
	zza(g_icons[nIb][SpeechStream.EnumIconParameter.ICON_NAME], "flat", g_icons[nIb][SpeechStream.EnumIconParameter.ICON_OFFSET], oIb);
}
function lIb(kxb, nIb, oIb) {
	if (Qga) {
		window.postMessage({
			type : "1757FROM_PAGERW4G",
			command : "trackEvent",
			settings : {
				'category' : "FromWebReader",
				'action' : "WebReader " + kxb,
				'label' : window.location.host
			}
		}, '*');
	}
	if (wea > 0) {
		--wea;
		return;
	}
	if (tea) {
		return;
	}
	if ($rw_blockClick(kxb)) {
		return;
	}
	zza(g_icons[nIb][SpeechStream.EnumIconParameter.ICON_NAME], "toggle", g_icons[nIb][SpeechStream.EnumIconParameter.ICON_OFFSET], oIb);
}
function $rw_blockClick(kxb) {
	if (gEb && iEb.indexOf(kxb) > -1) {
		return true;
	}
	if (kEb.indexOf(kxb) > -1) {
		return true;
	}
	if (Kga && (kxb == "play" || kxb == "funplay" || kxb == "hover")) {
		return true;
	}
	return false;
}
var pIb = "rw_speechenablingdata";
function rIb() {
	if (Zda) {
		if (!wfa || !vfa) {
			setTimeout(rIb, 50);
			return;
		}
	}
	if (Afa && !tda) {
		var qIb = dwa("rwebooks-x");
		var sIb = dwa("rwebooks-y");
		if ((qIb != null) && (sIb != null) && qIb != "NaN" && sIb != "NaN") {
			yea = parseFloat(qIb);
			zea = parseFloat(sIb);
		}
	}
	var tIb = false;
	var UBc = Dfa.length;
	var vIb;
	var wIb;
	if (Afa && !tda) {
		for (var i = 0; i < UBc; i++) {
			vIb = dwa("rwebooks-div" + i + "x");
			if (vIb != null) {
				Dfa[i] = parseFloat(vIb);
			}
			wIb = dwa("rwebooks-div" + i + "y");
			if (wIb != null) {
				Efa[i] = parseFloat(wIb);
			}
			if (i == Tba) {
				if (vIb == null && wIb == null) {
					tIb = true;
				}
			}
		}
	} else {
		tIb = true;
	}
	if (tIb) {
		Dfa[Tba] = 0.45;
		if (bfa) {
			Efa[Tba] = 0.0;
		} else {
			Efa[Tba] = 0.35;
		}
	}
	jKb();
	lKb();
	$rw_pageSetup();
	if (Zda) {
		if (bda != "*" && typeof(Dib) != "undefined") {
			Dib();
		} else {
			if (ada != "*" && typeof(Imb) != "undefined") {
				Imb();
			}
		}
	}
	if (Lfa) {
		var xIb = document.createTextNode(" ");
		document.body.appendChild(xIb);
	}
	if (Mda && Qda) {
		$rw_cachePage(null, null);
	}
	if (aga.bubbleData.bBubbleMode) {
		if (oda) {
			var yIb = SSDOM.createObject("script", ["type", "text/javascript", "src", aga.paths.strFileLoc + "texthelpBubble.js"]);
			aga.pageData.placeholder.appendChild(yIb);
		}
		if (aga.bubbleData.bBubbleModeStartDisabled) {
			aga.bubbleData.bBubbleMode = false;
		}
	}
	if (cea) {
		setTimeout($rw_event_generate_cache, 1000);
	}
	if (dea) {
		setTimeout($rw_event_check_cache, 1000);
	}
	zIb();
}
function zIb() {
	if (Jga) {
		if (typeof(SpeechStream.swaConfig) == "undefined") {
			setTimeout(zIb, 100);
			return;
		}
	}
	aga.controlData.bOnloadFinished = true;
	if (typeof($rw_toolbarLoadedCallback) == "function") {
		$rw_toolbarLoadedCallback();
	}
}
function AJb() {
	if (typeof(clb) != 'undefined' && Zda && typeof(Dkb) != 'undefined' && Dkb > -1) {
		wkb(Dkb);
	}
	if (Afa && !tda) {
		jwa("rwebooks-x", yea, 20, "/", window.location.host);
		jwa("rwebooks-y", zea, 20, "/", window.location.host);
		var UBc = Dfa.length;
		for (var i = 0; i < UBc; i++) {
			jwa("rwebooks-div" + i + "x", Dfa[i], 20, "/", window.location.host);
			jwa("rwebooks-div" + i + "y", Efa[i], 20, "/", window.location.host);
		}
	}
}
var CJb = new Array();
function DJb() {
	bca = true;
	rw_pageCompleteCallBack();
}
function $rw_clearInstructionQueue() {
	CJb.length = 0;
}
function EJb() {
	if (CJb != null) {
		var i;
		for (i = 0; i < CJb.length; i++) {
			if (typeof(CJb[i]) == "string") {
				if (CJb[i].indexOf("$rw_readNextTarget") > -1) {
					CJb.length = i;
					break;
				}
			} else if (typeof(CJb[i]) == "function" && CJb[i] == DJb) {
				CJb[i] = "";
			}
		}
	}
}
function JJb(ZNb) {
	var tcb = ZNb.range;
	var YCc;
	var ipb;
	if (tcb != null) {
		ipb = tcb.body;
		YCc = SSDOM.getCaretPairFromDomPosition(ipb, tcb.Upb.path, tcb.Upb.offset, tcb.Vpb.path, tcb.Vpb.offset);
	} else if (ZNb.body != null && ZNb.path != null) {
		ipb = ZNb.body;
		var IJb = new kqa(SSDOM.getNodeFromPosition(ipb, ZNb.path), 0, true);
		YCc = new qqa(IJb, IJb);
	} else {
		CJb.push("rw_pageCompleteCallBack()");
		return;
	}
	aga.controlData.bIgnoreSkipSection = true;
	var PJb = null;
	var NJb = false;
	if (ZNb.jumpId != null) {
		var OJb = YCc.zgb.node.ownerDocument.getElementById(ZNb.jumpId);
		if (OJb != null) {
			PJb = SSDOM.getSentenceFromPoint(new kqa(OJb, 0, true));
			NJb = true;
			if (PJb == null) {
				aga.controlData.bIgnoreSkipSection = false;
				return;
			}
		}
	}
	if (PJb == null) {
		var PJb = SSDOM.getNextSentence(YCc);
		if (PJb == null) {
			CJb.push(DJb);
			aga.controlData.bIgnoreSkipSection = false;
			return;
		}
	}
	while (!wya(PJb)) {
		PJb = SSDOM.getNextSentence(PJb);
		if (PJb == null) {
			CJb.push(DJb);
			aga.controlData.bIgnoreSkipSection = false;
			return;
		}
	}
	aga.controlData.bIgnoreSkipSection = false;
	var QJb = new THHoverTarget(null, null, TQb(PJb));
	QJb.prepareTextForSpeech();
	var RJb = QJb.getCaretRange();
	if (!RJb.equals(PJb)) {
		PJb = RJb;
	}
	if (eNb(YCc.ygb.node, PJb.zgb.node) && !NJb) {
		CJb.push(DJb);
		return;
	}
	bca = false;
	if (aga.bubbleData.bBubbleMode) {
		var mBc = SSDOM.getStartOfParagraph(PJb.ygb.node);
		PJb.ygb.node = mBc;
		PJb.zgb.node = SSDOM.getEndOfParagraph(PJb.zgb.node);
		PJb.ygb.offset = 0;
		if (PJb.zgb.node.nodeType == 1) {
			PJb.zgb.offset = 0;
		} else {
			PJb.zgb.offset = PJb.zgb.node.length;
		}
	}
	var TJb = new THRange(ipb, ZUb(PJb.ygb.node, PJb.ygb.offset), ZUb(PJb.zgb.node, PJb.zgb.offset));
	Fea = new THHoverTarget(null, null, TJb);
	var bub = Fea.getTextPreparedForSpeech();
	if (bub == null || bub.length == 0) {
		CJb.push(DJb);
		return;
	}
	CJb.push("setTimeout($rw_readNextTarget, 50);");
}
function WJb(XJb) {
	while (XJb.length > 0) {
		var VJb = XJb.shift();
		try {
			if (typeof(VJb) == "function") {
				VJb();
			} else if (typeof(VJb) == "string") {
				eval(VJb);
			}
		} catch (err) {
			thLogE(err.message);
		}
	}
}
function YJb() {
	this.bub = "";
	this.LNb = "";
	this.voice = null;
	this.YCc = null;
}
function ZJb(Bub) {
	return BKb(gJb(Bub));
}
function cJb(Bub) {
	if (Bub != null && Bub.nodeType == 1) {
		var wvb = Bub.getAttribute("lang");
		if (wvb == null) {
			try {
				wvb = Bub.getAttribute("xml:lang");
				if (wvb != null) {
					return BKb(wvb);
				}
			} catch (e) {}

		} else {
			return BKb(wvb);
		}
	}
	return null;
}
function gJb(Bub) {
	var jOb = Bub;
	while (jOb != null) {
		if (jOb.nodeType == 1) {
			var fJb = jOb.getAttribute("lang");
			if (fJb != null && fJb.length > 0) {
				return fJb;
			}
			try {
				fJb = jOb.getAttribute("xml:lang");
				if (fJb != null && fJb.length > 0) {
					return fJb;
				}
			} catch (e) {}

		}
		jOb = jOb.parentNode;
	}
	return null;
}
function mJb(hUb, aOb, tJb) {
	var jOb = hUb;
	jOb = SSDOM.getNextNodeAllowMoveToChild(jOb, false, aOb);
	while (jOb != null) {
		var pJb = ZJb(jOb);
		if (pJb != tJb) {
			var stb = SSDOM.getPreviousTextNodeNoBlank(jOb, false, hUb);
			if (stb == null) {
				stb = SSDOM.getPreviousTextNode(jOb, false, hUb);
				if (stb == null) {
					return null;
				}
			}
			if (stb.nodeType == 3) {
				return new kqa(stb, stb.nodeValue.length, false);
			} else {
				var uzb = new kqa(stb, 0, true);
				uzb.setSpecialCase(true);
				return uzb;
			}
		}
		jOb = SSDOM.getNextNode(jOb, false, aOb);
	}
	return null;
}
function sJb(hUb, aOb, tJb) {
	var jOb = aOb;
	jOb = SSDOM.getPreviousNode(jOb, false, hUb);
	while (jOb != null) {
		var pJb = ZJb(jOb);
		if (pJb != tJb) {
			var stb = SSDOM.getNextTextNodeNoBlank(jOb, false, aOb);
			if (stb != null) {
				if (stb.nodeType == 3) {
					return new kqa(stb, 0, false);
				} else {
					var uzb = new kqa(stb, 0, false);
					uzb.setSpecialCase(true);
					return uzb;
				}
			} else {
				return null;
			}
		}
		jOb = SSDOM.getPreviousNode(jOb, false, hUb);
	}
	return null;
}
function xJb(hUb, aOb, yJb) {
	var jOb = hUb;
	jOb = SSDOM.getNextNodeAllowMoveToChild(jOb, false, aOb);
	while (jOb != null) {
		var vJb = Fra(jOb);
		if (vJb != yJb) {
			var stb = SSDOM.getPreviousTextNodeNoBlank(jOb, false, hUb);
			if (stb == null) {
				stb = SSDOM.getPreviousTextNode(jOb, false, hUb);
				if (stb == null) {
					return null;
				}
			}
			if (stb.nodeType == 3) {
				return new kqa(stb, stb.nodeValue.length, false);
			} else {
				return new kqa(stb, 0, true);
			}
		}
		jOb = SSDOM.getNextNode(jOb, false, aOb);
	}
	return null;
}
function BKb(fib) {
	if (fib != null) {
		var LCc = fib.toLowerCase();
		var AKb;
		if (LCc == "en" || LCc == "en-gb" || LCc == "en-uk") {
			AKb = ENGLISH;
		} else if (LCc == "en-us") {
			AKb = ENGLISH_US;
		} else if (LCc == "es-us") {
			AKb = SPANISH;
		} else if (LCc == "es" || LCc == "es-es") {
			AKb = ESPANOL;
		} else if (LCc == "fr" || LCc == "fr-fr") {
			AKb = FRENCH;
		} else if (LCc == "fr-ca") {
			AKb = FRENCH_CA;
		} else if (LCc == "de") {
			AKb = GERMAN;
		} else if (LCc == "it") {
			AKb = ITALIAN;
		} else if (LCc == "nl") {
			AKb = DUTCH;
		} else if (LCc == "sv") {
			AKb = SWEDISH;
		} else if (LCc == "en-au") {
			AKb = AUSTRALIAN;
		} else if (LCc == "pt-br") {
			AKb = PORTUGUESE;
		} else if (LCc == "pt" || LCc == "pt-pt") {
			AKb = PORTUGUES;
		} else if (LCc == "ru") {
			AKb = Kca;
		} else if (LCc == "pl") {
			AKb = Lca;
		} else if (LCc == "ar") {
			AKb = Mca;
		} else if (LCc == "zh-cn" || LCc == "zh-tw") {
			AKb = CHINESE;
			if (SpeechStream.translatorData.bCantoneseTarget) {
				AKb = CANTONESE;
			}
		} else if (LCc == "ko") {
			AKb = Oca;
		} else {
			return null;
		}
		return faa[AKb];
	} else {
		return null;
	}
}
var DKb = "ReadHeader1 ReadSection";
function ReadHeader1() {
	var EKb = (new Date).getTime();
	if ((EKb - ega) < Aha) {
		return;
	}
	ega = EKb;
	var gCc = document.getElementsByTagName("H1")[0];
	var lmb = SSDOM.getNextNodeAllowMoveToChild(gCc, true, gCc);
	if (lmb == null) {
		return;
	}
	if (lmb.nodeType != 3) {
		lmb = SSDOM.getNextTextNode(lmb, true, gCc);
	}
	if (lmb == null) {
		return;
	}
	var uzb = new kqa(lmb, 0, true);
	var target = SMb(uzb);
	if (target != null) {
		var Rpb = target.getCaretRange();
		if (wya(Rpb)) {
			var start = Rpb.ygb.node;
			if (target.isRange()) {
				var end = Rpb.zgb.node;
				start = SSDOM.getStartOfParagraph(start);
				end = SSDOM.getEndOfParagraph(end);
				var Upb = ZUb(start, 0);
				var Vpb;
				if (end.nodeType == 1) {
					Vpb = ZUb(end, 0);
				} else {
					Vpb = ZUb(end, end.nodeValue.length);
				}
				target.range = new THRange(document.body, Upb, Vpb);
			}
			var OKb = Eea;
			$rw_stopSpeech();
			Eea = false;
			rw_speakHoverTarget(target);
			Eea = OKb;
		}
	}
}
function $rw_getHashCodes() {
	var PKb = "";
	var QKb = SSDOM.getFirstSentence(document.body);
	while (QKb != null) {
		var pPb = BOb(QKb, new Array());
		PKb = PKb + IQb() + "/";
		var BQb;
		if (SpeechStream.cacheMode.mode == SpeechStream.cacheMode.CACHE_ONLY || SpeechStream.pronunciation.mode == SpeechStream.pronunciation.CLIENT_PRONUNCIATION_FOR_LIVE_SERVER) {
			BQb = IRb(pPb.bub);
		} else {
			BQb = IRb(pPb.LNb);
		}
		PKb = PKb + BQb + "~";
		QKb = SSDOM.getNextSentence(QKb);
	}
	return PKb;
}
function $rw_getSoundFileLength(Vsa) {
	var flash = xbb.getConnector();
	if (flash == null) {
		alert("Connection not available to the server.");
	} else {
		flash.getSoundFileLength(Vsa);
	}
}
function $rw_soundFileLengthCallback(p_strLength) {
	alert(p_strLength);
}
SpeechStream.storedVoice = null;
SpeechStream.storedSpeed = null;
SpeechStream.storedCustId = null;
SpeechStream.storedBookId = null;
SpeechStream.storedPageId = null;
function WKb(XKb, cKb, dKb, eKb, fKb) {
	if (SpeechStream.storedVoice == null) {
		SpeechStream.storedVoice = xca;
		SpeechStream.storedSpeed = Hda;
		SpeechStream.storedCustId = yca;
		SpeechStream.storedBookId = zca;
		SpeechStream.storedPageId = Ada;
	}
	if (XKb != undefined) {
		xca = XKb;
	}
	if (cKb != undefined) {
		$rw_setSpeedValue(cKb);
	}
	if (dKb != undefined) {
		yca = dKb;
	}
	if (eKb != undefined) {
		zca = eKb;
	}
	if (fKb != undefined) {
		Ada = fKb;
	}
	var flash = xbb.getConnector();
	if (flash != null) {
		flash.setAltSettings(XKb, cKb, dKb, eKb, fKb);
	}
}
function gKb() {
	if (SpeechStream.storedVoice != null) {
		xca = SpeechStream.storedVoice;
		g_speedValue = SpeechStream.storedSpeed;
		yca = SpeechStream.storedCustId;
		zca = SpeechStream.storedBookId;
		Ada = SpeechStream.storedPageId;
		SpeechStream.storedVoice = null;
		SpeechStream.storedSpeed = null;
		SpeechStream.storedCustId = null;
		SpeechStream.storedBookId = null;
		SpeechStream.storedPageId = null;
	}
	var flash = xbb.getConnector();
	if (flash != null) {
		flash.restoreSettings();
	}
}
/*Code designed and developed by Stuart McWilliams.*/
var hKb = false;
function $rw_ipadFrameScrolling(HWb, p_nStartingOffset) {
	hKb = true;
	jKb();
	lKb();
}
function jKb() {
	var OLb;
	OLb = document.getElementById('rwDrag').style;
	if (OLb == null) {
		return;
	}
	if (!cca) {
		var x;
		var y;
		if (typeof(eba_override_x) == 'number' && typeof(eba_override_y) == 'number') {
			x = eba_override_x;
			y = eba_override_y;
		} else {
			var wd = rw_getDisplayWidth();
			var ht = rw_getDisplayHeight();
			if (Zca) {
				yea = 1;
				zea = 0;
				uea = 0;
			}
			if (efa) {
				wd = rw_getDocumentDisplayWidth();
				ht = rw_getDocumentDisplayHeight();
				x = wd * yea;
				y = ht * zea;
			} else {
				x = wd * yea;
				y = ht * zea;
			}
			if ((x + Bfa + uea) > rw_getDisplayWidthAdjusted()) {
				x = rw_getDisplayWidthAdjusted() - Bfa - uea;
			}
			if (x < uea) {
				x = uea;
			}
			if ((y + Cfa + uea) > rw_getDisplayHeightAdjusted()) {
				y = rw_getDisplayHeightAdjusted() - Cfa - uea;
			}
			if (y < uea) {
				y = uea;
			}
			if (!efa) {
				x = rw_getScreenOffsetLeft() + x;
				y = rw_getScreenOffsetTop() + y;
			}
			if (Zca) {
				y = 0;
			}
		}
		if (rw_getDisplayWidthAdjusted() < Bfa) {
			x = rw_getScreenOffsetLeft() + rw_getDisplayWidthAdjusted() - Bfa - uea;
		}
		if (x < uea) {
			x = uea;
		}
		OLb.left = x + 'px';
		OLb.top = y + 'px';
	}
	if (Kda) {
		return;
	}
	OLb.visibility = 'visible';
	OLb.display = "inline";
	var mBc = document.getElementById("rwMainOutline");
	if (mBc != null) {
		mBc.style.visibility = 'visible';
		mBc.style.display = "block";
	}
	mBc = document.getElementById("rwMainNoOutline");
	if (mBc != null) {
		mBc.style.visibility = 'visible';
		mBc.style.display = "block";
	}
}
function lKb() {
	pKb(0);
	pKb(1);
	pKb(2);
	pKb(3);
	pKb(4);
	pKb(5);
	pKb(6);
	pKb(7);
	pKb(8);
	pKb(9);
	pKb(10);
	pKb(11);
	pKb(12);
	pKb(13);
	pKb(14);
	pKb(15);
	pKb(16);
}
function pKb(JLb) {
	var mKb;
	var OLb;
	var Hpb;
	switch (JLb) {
	case 0:
		Hpb = "rwDisplay";
		break;
	case 1:
		Hpb = "rwTrans";
		break;
	case 2:
		Hpb = "rwFF";
		break;
	case 3:
		Hpb = "rwDict";
		break;
	case 4:
		Hpb = "rwCollect";
		break;
	case 5:
		Hpb = "rwSticky";
		break;
	case 6:
		Hpb = "rwPronCreate";
		break;
	case 7:
		Hpb = "rwPronEdit";
		break;
	case 8:
		Hpb = "rwCal";
		break;
	case 9:
		Hpb = "rwGenerateCache";
		break;
	case 10:
		Hpb = "rwCheckCache";
		break;
	case 11:
		Hpb = "rwPictureDictionary";
		break;
	case 12:
		Hpb = "rwRec";
		break;
	case 13:
		Hpb = "rwPrediction";
		break;
	case 14:
		Hpb = "rwSpelling";
		break;
	case 15:
		Hpb = "rwHomophone";
		break;
	case 16:
		Hpb = "rwGrammar";
		break;
	default:
		Hpb = "rwDisplay";
	}
	mKb = document.getElementById(Hpb);
	if (typeof(mKb) == 'undefined' || mKb == null) {
		return;
	}
	OLb = mKb.style;
	if (OLb == null) {
		return;
	}
	if (Hfa[JLb]) {
		OLb.display = "block";
		if (OLb.visibility == 'visible') {
			var NLb = AFb(Hpb);
			if (NLb != null) {
				var sKb = parseInt(NLb.offsetHeight, 10);
				if (!isNaN(sKb)) {
					Gfa[JLb] = sKb - 4;
				}
			}
		}
		var width = rw_getDocumentDisplayWidth();
		var height = rw_getDocumentDisplayHeight();
		var x,
		y;
		if (efa) {
			x = Dfa[JLb];
			y = Efa[JLb] + top.window.pageYOffset;
		} else {
			x = width * Dfa[JLb];
			y = height * Efa[JLb];
		}
		var tKb = Ffa[JLb];
		if (!efa) {
			if ((x + tKb + uea) > rw_getDisplayWidthAdjusted()) {
				x = rw_getDisplayWidthAdjusted() - tKb - uea;
			}
		} else {
			if ((x - top.window.pageXOffset + tKb + uea) > top.window.innerWidth) {
				x = top.window.innerWidth - tKb - uea + top.window.pageXOffset;
			}
		}
		if (x < uea) {
			x = uea;
		}
		var uKb = Gfa[JLb];
		if (!efa) {
			if ((y + uKb + uea) > rw_getDisplayHeightAdjusted()) {
				y = rw_getDisplayHeightAdjusted() - uKb - uea;
			}
		} else {
			if ((y - top.window.pageYOffset + uKb + uea) > top.window.innerHeight) {
				y = top.window.innerHeight - uKb - uea + top.window.pageYOffset;
			}
		}
		if (y < uea) {
			y = uea;
		}
		if (!efa) {
			x = rw_getScreenOffsetLeft() + x;
			y = rw_getScreenOffsetTop() + y;
		}
		if (rw_getDisplayWidthAdjusted() < Bfa) {
			x = rw_getScreenOffsetLeft() + rw_getDisplayWidthAdjusted() - Bfa - uea;
		}
		if (x < uea) {
			x = uea;
		}
		OLb.left = x + 'px';
		OLb.top = y + 'px';
		OLb.visibility = 'visible';
	} else {
		OLb.display = "none";
		OLb.visibility = 'hidden';
	}
}
function vKb(x, y) {
	if (x < 0) {
		x = 0;
	}
	if (y < 0) {
		y = 0;
	}
	yea = x / rw_getDocumentDisplayWidth();
	zea = y / rw_getDocumentDisplayHeight();
	if (yea > 1) {
		yea = 1;
	}
	if (zea > 1) {
		zea = 1;
	}
}
function xKb(JLb, x, y) {
	if (efa) {
		Dfa[JLb] = x;
		Efa[JLb] = y;
	} else {
		Dfa[JLb] = x / rw_getDocumentDisplayWidth();
		Efa[JLb] = y / rw_getDocumentDisplayHeight();
	}
}
function $rw_divOver(JLb) {
	var Hpb;
	switch (JLb) {
	case Oba:
		Hpb = "displayImg";
		break;
	case Pba:
		Hpb = "transImg";
		break;
	case Qba:
		Hpb = "FFImg";
		break;
	case Rba:
		Hpb = "dictImg";
		break;
	case Sba:
		Hpb = "collectImg";
		break;
	case Uba:
		Hpb = "pronCreateImg";
		break;
	case Vba:
		Hpb = "pronEditImg";
		break;
	case Wba:
		Hpb = "calImg";
		break;
	case Xba:
		Hpb = "generateCacheImg";
		break;
	case Yba:
		Hpb = "checkCacheImg";
		break;
	case Zba:
		Hpb = "pictureDictionaryImg";
		break;
	case aba:
		Hpb = "recImg";
		break;
	case bba:
		Hpb = "predictImg";
		break;
	case cba:
		Hpb = "spellingImg";
		break;
	case dba:
		Hpb = "homophoneImg";
		break;
	case eba:
		Hpb = "grammarImg";
		break;
	default:
		Hpb = "displayImg";
	}
	if (document.images[Hpb] != null) {
		document.images[Hpb].src = aga.paths.strFileLoc + "rwimgs/thepressedx.bmp";
	}
}
function $rw_divOut(JLb) {
	var Hpb;
	switch (JLb) {
	case Oba:
		Hpb = "displayImg";
		break;
	case Pba:
		Hpb = "transImg";
		break;
	case Qba:
		Hpb = "FFImg";
		break;
	case Rba:
		Hpb = "dictImg";
		break;
	case Sba:
		Hpb = "collectImg";
		break;
	case Uba:
		Hpb = "pronCreateImg";
		break;
	case Vba:
		Hpb = "pronEditImg";
		break;
	case Wba:
		Hpb = "calImg";
		break;
	case Xba:
		Hpb = "generateCacheImg";
		break;
	case Yba:
		Hpb = "checkCacheImg";
		break;
	case Zba:
		Hpb = "pictureDictionaryImg";
		break;
	case aba:
		Hpb = "recImg";
		break;
	case bba:
		Hpb = "predictImg";
		break;
	case cba:
		Hpb = "spellingImg";
		break;
	case dba:
		Hpb = "homophoneImg";
		break;
	case eba:
		Hpb = "grammarImg";
		break;
	default:
		Hpb = "displayImg";
	}
	if (document.images[Hpb] != null) {
		document.images[Hpb].src = aga.paths.strFileLoc + "rwimgs/thex.bmp";
	}
}
function $rw_divPress(JLb) {
	$rw_event_stop();
	HLb(false, JLb);
}
function CLb(JLb, exb) {
	var OLb;
	var Hpb;
	switch (JLb) {
	case Oba:
		Hpb = "rwpopupdisplay";
		break;
	case Pba:
		Hpb = "rwpopuptrans";
		break;
	case Qba:
		Hpb = "rwpopupff";
		break;
	case Rba:
		Hpb = "rwpopupdict";
		break;
	case Sba:
		Hpb = "rwpopupcollect";
		break;
	case Uba:
		Hpb = "rwpopupproncreate";
		break;
	case Vba:
		Hpb = "rwpopuppronedit";
		break;
	case Wba:
		Hpb = "rwpopupcal";
		break;
	case Xba:
		Hpb = "rwpopupgeneratecache";
		break;
	case Yba:
		Hpb = "rwpopupcheckcache";
		break;
	case Zba:
		Hpb = "rwpopuppicturedictionary";
		break;
	case aba:
		Hpb = "rwpopuprec";
		break;
	case bba:
		Hpb = "rwPopupPredict";
		break;
	case cba:
		Hpb = "rwPopupSpelling";
		break;
	case dba:
		Hpb = "rwpopuphomophone";
		break;
	case eba:
		Hpb = "rwpopupgrammar";
		break;
	default:
		Hpb = "rwpopupdisplay";
	}
	OLb = document.getElementById(Hpb);
	if (OLb == null) {
		return;
	}
	try {
		OLb.innerHTML = exb;
	} catch (err) {
		SSDOM.eob(OLb, exb, false);
	}
}
function HLb(ILb, JLb) {
	var OLb;
	Hfa[JLb] = ILb;
	var Hpb;
	switch (JLb) {
	case Oba:
		Hpb = "rwDisplay";
		break;
	case Pba:
		Hpb = "rwTrans";
		break;
	case Qba:
		Hpb = "rwFF";
		break;
	case Rba:
		Hpb = "rwDict";
		break;
	case Sba:
		Hpb = "rwCollect";
		break;
	case Tba:
		Hpb = "rwSticky";
		break;
	case Uba:
		Hpb = "rwPronCreate";
		break;
	case Vba:
		Hpb = "rwPronEdit";
		break;
	case Wba:
		Hpb = "rwCal";
		break;
	case Xba:
		Hpb = "rwGenerateCache";
		break;
	case Yba:
		Hpb = "rwCache";
		break;
	case Zba:
		Hpb = "rwPictureDictionary";
		break;
	case aba:
		Hpb = "rwRec";
		break;
	case bba:
		Hpb = "rwPrediction";
		break;
	case cba:
		Hpb = "rwSpelling";
		break;
	case dba:
		Hpb = "rwHomophone";
		break;
	case eba:
		Hpb = "rwGrammar";
		break;
	default:
		Hpb = "rwDisplay";
	}
	var NLb = AFb(Hpb);
	if (NLb != null) {
		OLb = NLb.style;
		if (OLb == null) {
			return;
		}
		if (ILb) {
			MLb();
			OLb.visibility = 'visible';
			OLb.display = 'block';
			OLb.zIndex = 501;
		} else {
			OLb.visibility = 'hidden';
			OLb.display = "none";
			CLb(JLb, "");
		}
	}
	lKb();
}
function MLb() {
	var Hpb;
	Hpb = "rwDisplay";
	var NLb = AFb(Hpb);
	var OLb;
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwTrans";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwFF";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwDict";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwCollect";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwSticky";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwCal";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwGenerateCache";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwCheckCache";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwPictureDictionary";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwRec";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwPrediction";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwSpelling";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwHomophone";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
	Hpb = "rwGrammar";
	NLb = AFb(Hpb);
	if (NLb != null && NLb.style) {
		OLb = NLb.style;
		OLb.zIndex = 500;
	}
}
function $rw_getToolbarX() {
	return yea;
}
function $rw_getToolbarY() {
	return zea;
}
function $rw_setToolbarPosition(p_nX, p_nY) {
	if (typeof(p_nX) == "string") {
		$rw_placeToolbar(p_nX);
	}
	if (typeof(p_nX) != "number" || typeof(p_nY) != "number") {
		return;
	}
	vKb(p_nX, p_nY);
	jKb();
}
function $rw_placeToolbar(p_strSlot) {
	if (typeof(p_strSlot) != "string") {
		return;
	}
	var klb = p_strSlot.toLowerCase().replace(/\s+/g, "");
	if (klb == "topleft" || klb == "lefttop") {
		yea = 0;
		zea = 0;
	} else if (klb == "topright" || klb == "righttop") {
		yea = 1;
		zea = 0;
	} else if (klb == "bottomleft" || klb == "leftbottom") {
		yea = 0;
		zea = 1;
	} else if (klb == "bottomright" || klb == "rightbottom") {
		yea = 1;
		zea = 1;
	} else if (klb == "top") {
		yea = ((rw_getDisplayWidthAdjusted() - Bfa) / 2.0) / rw_getDisplayWidthAdjusted();
		zea = 0;
	} else if (klb == "right") {
		yea = 1;
		zea = 0.5 - (Cfa / rw_getDisplayHeightAdjusted());
	} else if (klb == "bottom") {
		yea = ((rw_getDisplayWidthAdjusted() - Bfa) / 2.0) / rw_getDisplayWidthAdjusted();
		zea = 1;
	} else if (klb == "left") {
		yea = 0;
		zea = 0.5 - (Cfa / rw_getDisplayHeightAdjusted());
	}
	jKb();
}
function $rw_setOverridePosition(p_nX, p_nY) {
	eba_override_x = p_nX;
	eba_override_y = p_nY;
	jKb();
}
function $rw_getOverrideX() {
	return eba_override_x;
}
function $rw_getOverrideY() {
	return eba_override_y;
}
SpeechStream.mathJaxHighlighter = (function () {
	var QLb = {};
	var RLb = {
		"r" : 255,
		"g" : 255,
		"b" : 0
	};
	var SLb = {
		"r" : 0,
		"g" : 0,
		"b" : 0
	};
	var TLb = {
		"r" : 0,
		"g" : 0,
		"b" : 255
	};
	var ULb = {
		"r" : 255,
		"g" : 255,
		"b" : 255
	};
	var VLb = {
		"r" : 255,
		"g" : 255,
		"b" : 0
	};
	var WLb = {
		"r" : 0,
		"g" : 0,
		"b" : 0
	};
	var XLb = {
		"r" : 0,
		"g" : 0,
		"b" : 255
	};
	var YLb = {
		"r" : 255,
		"g" : 255,
		"b" : 255
	};
	var ZLb = "highlight";
	var aLb = "highlight";
	var bLb = {};
	bLb.highlight = function (FWb) {
		try {
			var gCc = document.getElementById(FWb);
			var lLb = bLb.getJaxFor(gCc);
			var h = MathJax.Hub.Highlight.getHighlighter(lLb);
			if (ZLb === "highlight") {
				h.setHighlightStyle("context", "bg", VLb.r, VLb.g, VLb.b);
				h.setHighlightStyle("context", "fg", WLb.r, WLb.g, WLb.b);
			} else if (ZLb === "outline") {
				h.setHighlightStyle("context", "outline", WLb.r, WLb.g, WLb.b);
			} else if (ZLb === "underline") {
				h.setHighlightStyle("context", "underline", WLb.r, WLb.g, WLb.b);
			}
			if (aLb === "highlight") {
				h.setHighlightStyle("word", "bg", XLb.r, XLb.g, XLb.b);
				h.setHighlightStyle("word", "fg", YLb.r, YLb.g, YLb.b);
			} else if (aLb === "outline") {
				h.setHighlightStyle("word", "outline", YLb.r, YLb.g, YLb.b);
			} else if (aLb === "underline") {
				h.setHighlightStyle("word", "underline", YLb.r, YLb.g, YLb.b);
			}
		} catch (err) {
			thLogE(err);
		}
	};
	bLb.highlightRange = function (FWb, p_nStart, gxb) {
		try {
			if (qva) {
				bLb.clearHighlights();
			}
			qva = FWb;
			if (!QLb[FWb]) {
				bLb.highlight(FWb);
				QLb[FWb] = true;
			}
			var gCc = document.getElementById(FWb);
			var lLb = bLb.getJaxFor(gCc);
			var h = MathJax.Hub.Highlight.getHighlighter(lLb);
			h.highlightNodes("context", p_nStart, gxb);
		} catch (err) {
			thLogE(err);
		}
	};
	bLb.highlightWord = function (FWb, p_nStart, gxb) {
		try {
			if (!QLb[FWb]) {
				bLb.highlight(FWb);
				QLb[FWb] = true;
			}
			var gCc = document.getElementById(FWb);
			var lLb = bLb.getJaxFor(gCc);
			var h = MathJax.Hub.Highlight.getHighlighter(lLb);
			h.highlightNodes("word", p_nStart, gxb);
		} catch (err) {
			thLogE(err);
		}
	};
	bLb.clearHighlights = function (FWb) {
		try {
			var gCc = document.getElementById(FWb);
			var lLb = SpeechStream.mathJaxHighlighter.getJaxFor(gCc);
			var h = MathJax.Hub.Highlight.getHighlighter(lLb);
			h.clearHighlights();
		} catch (err) {
			thLogE(err);
		}
	};
	bLb.setHighlightStyle = function (lHb, p_strType, p_rgbFg, p_rgbBg) {
		if (typeof(lHb) === "string") {
			if (typeof(p_strType) == "undefined") {
				p_strType = "word";
			}
			var kob = (typeof(p_rgb) == "undefined");
			if (p_strType === "context") {
				ZLb = lHb;
				if (lHb === "highlight") {
					VLb = (kob ? RLb : p_rgbBg);
					WLb = (kob ? SLb : p_rgbFg);
				} else if (lHb === "outline" || lHb === "underline") {
					WLb = (kob ? RLb : p_rgbFg);
				}
			} else if (p_strType === "word") {
				aLb = lHb;
				if (aLb === "highlight") {
					XLb = (kob ? TLb : p_rgbBg);
					YLb = (kob ? ULb : p_rgbFg);
				} else if (lHb === "outline" || lHb === "underline") {
					YLb = (kob ? TLb : p_rgbFg);
				}
			}
		}
	};
	bLb.getJaxFor = function (ypb) {
		var lLb = null;
		if (!Lfa || Rfa) {
			lLb = MathJax.Hub.getJaxFor(ypb);
		} else {
			if (ypb && ypb.isMathJax) {
				while (ypb && !ypb.jaxID) {
					ypb = ypb.parentNode;
				}
				if (ypb) {
					var mLb = ypb.id;
					if (mLb.indexOf("-Frame") > -1) {
						mLb = mLb.substr(0, mLb.indexOf("-Frame"));
						var Orb = document.getElementById(mLb);
						lLb = MathJax.Hub.getJaxFor(Orb);
					}
				}
			}
		}
		return lLb;
	};
	return bLb;
})();
var oLb = '<bookmark mark="';
var pLb = '"/>';
var qLb = null;
var rLb = null;
var sLb = 0;
var tLb = 0;
var uLb = false;
var vLb = false;
var wLb = false;
if (Yfa) {
	var xLb = navigator.appVersion;
	var yLb = xLb.lastIndexOf("/");
	xLb = xLb.substring(yLb + 1);
	try {
		var zLb = parseFloat(xLb);
		if (zLb < 300 || (zLb > 400 && zLb < 416)) {
			uLb = true;
		} else if (zLb > 500) {
			wLb = true;
		} else {
			vLb = true;
		}
	} catch (err) {
		wLb = true;
	}
}
var AMb = 0;
var BMb = 0;
function EMb(evt, lMb) {
	try {
		if (Vca) {
			var d = new Date();
			var gnb = d.getTime();
			if (gnb < (BMb + 800) || !aga.controlData.bOnloadFinished) {
				return;
			}
		}
		var Fpb = KMb(evt, Vca);
		if (Fpb != null) {
			if (Fpb.node.nodeType == 1 && Fpb.node.tagName.toLowerCase() == "img") {
				if (Fpb.node.className == "mceIcon") {
					return;
				}
			}
			if (Fpb.node.nodeType == 1 && Fpb.node.tagName.toLowerCase() == "input" && Fpb.node.type.toLowerCase() == "button" && Lda && !WIb(evt)) {
				return;
			}
			if (Oga != null) {
				SSAPIP.apipHandler.checkElement(Fpb.node, SSAPIP.apipHandler.Type.DEFAULT);
				return;
			}
			if (Rea) {
				if (Fpb.node.nodeType == 1 && Fpb.node.tagName.toLowerCase() == "input") {
					var alb = Fpb.node.getAttribute("type");
					if (alb != null) {
						alb = alb.toLowerCase();
						if ((alb == "radio" || alb == "checkbox")) {
							Fpb.node = SSDOM.getNextTextNodeNoBlank(Fpb.node, true, null);
							Fpb.offset = 0;
						}
					}
				}
			}
			var vub = SMb(Fpb);
			if (vub != null) {
				try {
					if (vub.equals(qLb)) {
						return;
					}
					if (vub.equals(rLb)) {
						if ((gnb - AMb) < 1000) {
							return;
						}
						AMb = gnb;
					}
					if (typeof(lMb) == "boolean" && lMb) {
						vub.useHighlighting = false;
						jMb(vub, true);
					} else {
						jMb(vub, false);
					}
				} catch (err) {
					thLogE(err);
				}
			} else {
				qLb = null;
			}
		}
	} catch (err) {
		eya("mousehover error: " + err);
	}
}
function KMb(Yvb, RWb) {
	var VMb = null;
	var JMb = 0;
	if (Lfa) {
		VMb = Yvb.srcElement;
		if (VMb.nodeType == 1 && VMb.tagName.toLowerCase() == "textarea") {}
		else {
			var Zub = rw_getTargetNodeAsCaretIE(Yvb, RWb);
			if (Zub != null) {
				VMb = Zub.node;
				JMb = Zub.offset;
			} else {
				var OMb = false;
				if (VMb.tagName.toLowerCase() == "li" || VMb.tagName.toLowerCase() == "a") {
					OMb = true;
				} else if (VMb.parentNode != null && VMb.parentNode.tagName != null && VMb.parentNode.tagName.toLowerCase() == "li") {
					OMb = true;
				}
				if (OMb) {
					var wvb = VMb.firstChild;
					if (wvb == null) {
						return null;
					}
					if (wvb.nodeType != 3) {
						wvb = SSDOM.getNextTextNodeNoBlank(wvb, false, VMb);
						if (wvb == null) {
							return null;
						}
					}
					VMb = wvb;
					JMb = 0;
				}
			}
		}
	} else if (Yfa) {
		VMb = Yvb.target;
		if (VMb != null) {
			if (wLb) {
				if (VMb.firstChild != null && VMb.firstChild.nodeType == 3 && VMb.tagName.toLowerCase() != "textarea") {
					var UWb = VMb.firstChild.nodeValue;
					if (UWb.trimTH().length > 0) {
						VMb = VMb.firstChild;
					}
				}
			} else if (vLb) {
				if (Yvb.fromElement != null) {
					if (VMb.nodeType == 1 && VMb.tagName.toLowerCase() != "textarea") {
						if (Yvb.fromElement.nodeType == 3) {
							VMb = Yvb.fromElement;
						}
					}
				} else {
					if (VMb.firstChild != null && VMb.firstChild.nodeType == 3 && VMb.tagName.toLowerCase() != "textarea") {
						var UWb = VMb.firstChild.nodeValue;
						if (UWb.trimTH().length > 0) {
							VMb = VMb.firstChild;
						}
					}
				}
			}
		}
	} else {
		if (Yvb.explicitOriginalTarget.nodeValue != null) {
			if (Yvb.target.tagName.toLowerCase() == "textarea") {
				VMb = Yvb.target;
			} else {
				VMb = Yvb.explicitOriginalTarget;
				if (Yvb.rangeOffset) {
					JMb = Yvb.rangeOffset;
				}
			}
		} else {
			VMb = Yvb.target;
		}
	}
	if (VMb == null) {
		return null;
	}
	if (SSDOM.isIgnored(VMb)) {
		return null;
	}
	if (VMb.nodeType == 3) {
		if (VMb.parentNode.isMathJax) {
			VMb = VMb.parentNode;
		}
	}
	return new kqa(VMb, JMb, true);
}
function SMb(TMb) {
	if (!TMb || (TMb.node.nodeType == 1 && TMb.node.tagName.toUpperCase() == "HTML")) {
		return null;
	}
	var UMb = false;
	var VMb = TMb.node;
	var vub = null;
	if (oUb(VMb)) {
		if (oca == null) {
			return vub;
		} else {
			UMb = true;
		}
	}
	if (VMb != null && VMb.parentNode != null && VMb.parentNode.getAttribute) {
		var qRb;
		var YMb;
		var ZMb;
		var aMb;
		var bMb;
		if (VMb.nodeType == 1) {
			qRb = VMb.tagName;
			if (Lda && qRb.toUpperCase() == "INPUT") {
				var alb = VMb.getAttribute("type");
				var dMb = VMb.className;
				if (alb != null && alb == "button" && dMb != "rwcalbutton" && dMb != "rwcalEqbutton") {
					return vub;
				}
			}
			YMb = VMb.getAttribute("started");
			ZMb = VMb.getAttribute("ignore");
			aMb = VMb.getAttribute("sp");
			bMb = VMb.getAttribute("csp");
			if (bMb != null || ZMb != null || aMb != null || YMb != null) {
				return vub;
			}
		}
		var npb = VMb.parentNode;
		YMb = npb.getAttribute("started");
		ZMb = npb.getAttribute("ignore");
		aMb = npb.getAttribute("sp");
		bMb = npb.getAttribute("csp");
		if (bMb != null || ZMb != null || aMb != null || YMb != null) {
			vub = null;
		} else {
			var ygb;
			var zgb;
			if (VMb.nodeType == 3) {
				var Zub = TMb;
				try {
					if (!Lfa && Zub.node.nodeValue.length > 0) {
						if (!Eea && Yfa) {
							Zub.offset = 0;
							ygb = SSDOM.getSentenceBreakToLeft(Zub);
							Zub.offset = Zub.node.nodeValue.length - 1;
							zgb = SSDOM.getSentenceBreakToRight(Zub);
						} else {
							ygb = SSDOM.getSentenceBreakToLeft(Zub);
							zgb = SSDOM.getSentenceBreakToRight(Zub);
						}
					} else {
						ygb = SSDOM.getSentenceBreakToLeft(Zub);
						zgb = SSDOM.getSentenceBreakToRight(Zub);
					}
					if (ygb != null && zgb != null) {
						var range = new THRange(SSDOM.getBody(npb), ZUb(ygb.node, ygb.offset), ZUb(zgb.node, zgb.offset));
						vub = new THHoverTarget(null, null, range);
						vub.blockCache = UMb;
					}
				} catch (err) {
					thLogE(err);
				}
			} else if (VMb.nodeType == 1) {
				if (VMb.tagName.toLowerCase() == "img" && VMb.getAttribute("msg") != null) {
					ygb = SSDOM.getSentenceBreakToLeft(TMb);
					zgb = SSDOM.getSentenceBreakToRight(TMb);
					if (ygb != null && zgb != null) {
						var range = new THRange(SSDOM.getBody(npb), ZUb(ygb.node, ygb.offset), ZUb(zgb.node, zgb.offset));
						vub = new THHoverTarget(null, null, range);
					} else {
						vub = new THHoverTarget(SSDOM.getBody(VMb), SSDOM.getPositionInDom(VMb), null);
					}
					vub.blockCache = UMb;
				} else {
					vub = new THHoverTarget(SSDOM.getBody(VMb), SSDOM.getPositionInDom(VMb), null);
					vub.blockCache = UMb;
					vub.allowContinuous = false;
				}
			} else {
				vub = null;
			}
		}
	}
	return vub;
}
function jMb(Ypb, lMb) {
	if (sLb > 0) {
		clearTimeout(sLb);
		sLb = 0;
	}
	if (tLb > 0) {
		clearTimeout(tLb);
		tLb = 0;
	}
	if (Vca && !lMb) {
		if (!rMb(Ypb)) {
			return;
		}
		qLb = Ypb;
		var iMb = fEb;
		$rw_event_stop_limited();
		if (iMb) {
			sLb = setTimeout(pMb, 500);
		} else {
			sLb = setTimeout(pMb, 5);
		}
	} else {
		qLb = Ypb;
		sLb = setTimeout(mMb, 500);
	}
}
function mMb() {
	if (qLb == null) {
		return;
	}
	if ($g_bMouseSpeech || ((jca & calculator_icon) == calculator_icon)) {
		sLb = 0;
		if (qLb != null) {
			if (!rMb(qLb)) {
				return;
			}
		}
		if (sLb > 0) {
			clearTimeout(sLb);
			sLb = 0;
		}
		if (tLb > 0) {
			clearTimeout(tLb);
			tLb = 0;
		}
		if (Qga) {
			window.postMessage({
				type : "1757FROM_PAGERW4G",
				command : "trackEvent",
				settings : {
					'category' : "FromWebReader",
					'action' : "WebReader start hover action",
					'label' : window.location.host
				}
			}, '*');
		}
		$rw_event_stop_limited();
		tLb = setTimeout(pMb, 500);
	}
}
var nMb = false;
function pMb() {
	try {
		nMb = true;
		sLb = 0;
		if (qLb != null) {
			if (rLb != null) {
				if (tLb > 0) {
					clearTimeout(tLb);
					tLb = 0;
				}
				$rw_event_stop_limited();
				tLb = setTimeout(pMb, 500);
			} else {
				var d = new Date();
				BMb = d.getTime();
				if (qLb.range != null) {
					Tga = qLb;
				}
				rw_speakHoverTarget(qLb);
				qLb = null;
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
	nMb = false;
}
function rMb(Ypb) {
	if (Ypb != null) {
		var bub;
		if (Ypb instanceof String) {
			bub = Ypb.toString();
		} else {
			if (Ypb.isRange()) {
				if (wya(Ypb.getCaretRange())) {
					bub = " ";
				} else {
					bub = null;
				}
			} else {
				bub = Ypb.getTextPreparedForSpeech();
			}
		}
		if (bub == null || bub.length == 0) {
			return false;
		} else {
			return true;
		}
	}
}
var tMb = new Array();
var uMb = (new Date).getTime();
var vMb = 0;
var wMb = 500;
function yMb() {
	if ((new Date).getTime() - uMb < wMb) {
		return true;
	}
	return false;
}
function zMb() {
	if (yMb()) {
		vMb = setTimeout(zMb, 100);
	} else {
		vMb = 0;
		if (tMb.length > 1) {
			var Fcb = tMb[tMb.length - 2];
			var target = tMb[tMb.length - 1];
			tMb.length = 0;
			if (Fcb == "rw_speakHoverTarget") {
				rw_speakHoverTarget(target);
			} else if (Fcb == "rw_speechHighlightOnly") {
				rw_speechHighlightOnly(target);
			} else {
				TNb(target, Fcb);
			}
		}
	}
}
function rw_speakHoverTarget(ZNb) {
	try {
		if (Qga) {
			window.postMessage({
				type : "1757FROM_PAGERW4G",
				command : "trackEvent",
				settings : {
					'category' : "FromWebReader",
					'action' : "WebReader speak sentence with hover",
					'label' : window.location.host
				}
			}, '*');
		}
		if (ZNb == null) {
			return;
		}
		if (yMb()) {
			tMb.push("rw_speakHoverTarget");
			tMb.push(ZNb);
			if (vMb == 0) {
				vMb = setTimeout(zMb, 100);
			}
			return;
		}
		uMb = (new Date).getTime();
		if (rLb != null) {
			rLb.unhighlightRange();
		}
		if (ZNb instanceof String) {
			rLb = null;
			var MOb = new SpeechStream.SpeechRequest();
			MOb.setString(ZNb.toString(), SpeechStream.SpeechRequestBookmarks.NONE);
			var oCc = MOb.getText();
			var flash = xbb.getConnector();
			if (flash != null && !Kga) {
				if (SpeechStream.cacheMode.useBackupForLiveRequests()) {
					flash.simpleSpeechFromBackup(oCc, !ofa);
				} else {
					flash.simpleSpeech(oCc, !ofa);
				}
			}
		} else {
			if (ZNb.range && ZNb.range instanceof THRange) {
				var HOb = rQb(ZNb.range);
				if (HOb != null && NNb(HOb.ygb.node)) {
					Tga = null;
					ega = 0;
					$rw_speakFirstSentence();
					return;
				}
				if (HOb != null && oUb(HOb.ygb.node)) {
					if (oca == null) {
						return;
					} else {
						ZNb.blockCache = true;
					}
				}
			}
			if (!Nea && Qea != null && Pea != null) {
				if (ZNb.equalsAprox(Pea)) {
					ZNb = Qea;
					ZNb.allowContinuous = false;
					Pea = null;
					Qea = null;
					if (Oea) {
						CJb.push("rw_pageCompleteCallBack()");
					}
				}
			}
			rLb = ZNb;
			var bub = ZNb.getTextPreparedForSpeech();
			var ibb = bub.indexOf("math:") == 0;
			if (bub != null && bub.length > 0) {
				Mra(ZNb);
				if (!ZNb.isValid()) {
					return;
				}
				$rw_setSentenceFromSelection();
				if (!ZNb.useHighlighting) {
					var flash = xbb.getConnector();
					if (flash != null) {
						if (ZNb.isOverridingGlobal()) {
							WKb(ZNb.voice, null, null, ZNb.bookId, ZNb.pageId);
							if (SpeechStream.cacheMode.useBackupForLiveRequests()) {
								flash.simpleSpeechFromBackup(bub, !ofa);
							} else {
								flash.simpleSpeech(bub, !ofa);
							}
							gKb();
						} else {
							if (SpeechStream.cacheMode.useBackupForLiveRequests()) {
								flash.simpleSpeechFromBackup(bub, !ofa);
							} else {
								flash.simpleSpeech(bub, !ofa);
							}
						}
					}
				} else {
					if (!ibb) {
						ZNb.highlightRange();
					} else {
						var FBc = bub.indexOf(";");
						var MAc = bub.substring(5, FBc);
						SpeechStream.mathJaxHighlighter.highlightRange(MAc, 1, 1);
					}
					var LNb = ZNb.textToSpeakNoChanges;
					if (ZNb.isOverridingGlobal()) {
						WKb(ZNb.voice, null, null, ZNb.bookId, ZNb.pageId);
						Xua(bub, ZNb.blockCache, LNb);
						gKb();
					} else {
						Xua(bub, ZNb.blockCache, LNb);
					}
				}
				if (Eea && ZNb.allowContinuous) {
					if (ZNb.equals(Pea)) {
						Pea = null;
						if (Oea) {
							CJb.push("rw_pageCompleteCallBack()");
						}
					} else {
						JJb(ZNb);
					}
				}
			}
		}
	} catch (err) {
		eya("rw_speakHoverTarget error:" + err.message);
	}
}
function NNb(Bub) {
	try {
		var MNb = SSDOM.getComputedStyle(Bub);
		if (MNb != null && (MNb.display == "none" || MNb.visibility == "hidden")) {
			return true;
		} else {
			var wvb = Bub;
			var ipb = wvb.ownerDocument.body;
			while (wvb != ipb) {
				wvb = wvb.parentNode;
				if (SSDOM.getComputedStyle(wvb).display == "none") {
					return true;
				}
			}
		}
	} catch (e) {}

	return false;
}
function TNb(ZNb, XNb) {
	if (ZNb == null || XNb == null) {
		return;
	}
	if (yMb()) {
		tMb.push(XNb);
		tMb.push(ZNb);
		if (vMb == 0) {
			vMb = setTimeout(zMb, 100);
		}
		return;
	}
	uMb = (new Date).getTime();
	if (rLb != null) {
		rLb.unhighlightRange();
	}
	if (ZNb instanceof String) {
		rLb = null;
		try {
			var flash = xbb.getConnector();
			if (flash != null) {
				pEb(true);
				flash.startSpeechFromFile(ZNb, XNb, !ofa);
				Jxa();
			}
		} catch (err) {
			thLogE(err);
		}
	} else {
		rLb = ZNb;
		var bub = ZNb.getTextPreparedForSpeech();
		if (bub != null && bub.length > 0) {
			Mra(ZNb);
			if (!ZNb.isValid()) {
				return;
			}
			ZNb.highlightRange();
			Cva(bub, XNb);
		}
	}
}
function rw_speechHighlightOnly(ZNb) {
	if (ZNb == null) {
		return;
	}
	if (yMb()) {
		tMb.push("rw_speechHighlightOnly");
		tMb.push(ZNb);
		if (vMb == 0) {
			vMb = setTimeout(zMb, 100);
		}
		return;
	}
	uMb = (new Date).getTime();
	if (rLb != null) {
		rLb.unhighlightRange();
	}
	if (ZNb instanceof String) {
		rLb = null;
	} else {
		rLb = ZNb;
		var bub = ZNb.getTextPreparedForSpeech();
		if (bub != null && bub.length > 0) {
			Mra(ZNb);
			if (!ZNb.isValid()) {
				return;
			}
			ZNb.highlightRange();
			Iva(bub);
		}
	}
}
function $rw_readNextTarget() {
	if (Fea != null) {
		qLb = Fea;
		Tga = Fea;
		Fea = null;
		pMb();
	}
}
function eNb(fNb, mTb) {
	if (fNb == null || mTb == null || fNb == mTb) {
		return false;
	}
	var ghb;
	var aOb = fNb;
	var dNb = null;
	if (Pea != null) {
		dNb = Pea.getCaretRange().zgb.node;
	}
	while (aOb != null && aOb != mTb) {
		if (aOb.nodeType == 1) {
			if (aOb.getAttribute("texthelpStopContinuous") != null) {
				return true;
			}
		}
		if (aOb == dNb && aOb != mTb) {
			return true;
		}
		ghb = !SSDOM.isInvalidNode(aOb);
		if (aOb.firstChild != null && ghb) {
			aOb = aOb.firstChild;
		} else if (aOb.nextSibling != null) {
			aOb = aOb.nextSibling;
		} else {
			while (aOb != null && aOb.nextSibling == null) {
				aOb = aOb.parentNode;
				if (aOb != null && aOb.nodeType == 1) {
					if (aOb.getAttribute("texthelpStopContinuous") != null) {
						return true;
					}
				}
				if (mTb == aOb) {
					return false;
				}
			}
			if (aOb != null && mTb != aOb) {
				aOb = aOb.nextSibling;
			}
		}
	}
	return false;
}
function iNb(zeb, Usb) {
	this.range = zeb;
	this.word = Usb;
}
function oNb(OXb, qNb, p_domRefRight, iOb) {
	try {
		if (qNb == null || p_domRefRight == null) {
			return new YJb();
		}
		var YCc = SSDOM.getCaretPairFromDomPosition(OXb, qNb.path, qNb.offset, p_domRefRight.path, p_domRefRight.offset);
		return BOb(YCc, iOb);
	} catch (err) {
		eya("err rw_getTextOverRangeToSpeak:" + "|" + err.message);
		return new YJb();
	}
}
function BOb(COb, iOb) {
	var tNb = new YJb();
	try {
		if (COb == null) {
			return tNb;
		}
		var ygb = COb.ygb;
		var zgb = COb.zgb;
		if (ygb == null) {
			return tNb;
		}
		if (zgb == null) {
			return tNb;
		}
		if (ygb.node != null) {
			var qlb = SSDOM.checkForSpecialParent(ygb.node);
			if (qlb != null) {
				ygb.node = qlb;
				ygb.offset = 0;
			}
		}
		if (zgb.node != null) {
			var qlb = SSDOM.checkForSpecialParent(zgb.node);
			if (qlb != null) {
				if (zgb.node.nodeType == 3) {
					zgb.offset = zgb.node.nodeValue.length;
				}
				zgb.node = qlb;
			}
		}
		var yNb = false;
		if (!Fda) {
			var wvb = ygb.node;
			var rlb;
			while (wvb != null) {
				if (wvb.nodeType == 1) {
					rlb = wvb.getAttribute("id");
					if (rlb != null && rlb.length > 0) {
						if (rlb == "rwpopuptrans") {
							yNb = true;
							break;
						} else {
							if (rlb.indexOf("rwMeaning") != 0 && rlb.indexOf("rwHeadWord") != 0) {
								break;
							}
						}
					}
				}
				wvb = wvb.parentNode;
			}
		}
		if (Fda || yNb) {
			var DOb = ZJb(ygb.node);
			if (DOb != null) {
				tNb.voice = DOb;
			}
			var EOb = mJb(ygb.node, zgb.node, DOb);
			if (EOb != null) {
				tNb.YCc = new qqa(ygb, EOb);
				var bub = tNb.YCc.toString();
				if (bub.length == 0 || !mya(bub)) {
					var wvb = SSDOM.getNextTextNodeNoBlank(ygb.node, false, zgb.node);
					if (wvb != null && wvb != ygb.node && wvb != COb.ygb.node) {
						var HOb = new qqa(new kqa(wvb, 0, true), COb.zgb);
						return BOb(HOb, iOb);
					}
				}
				zgb = EOb;
			}
		}
		if (Cga) {
			var IOb = Fra(ygb.node);
			var JOb = xJb(ygb.node, zgb.node, IOb);
			if (JOb != null) {
				tNb.YCc = new qqa(COb.ygb, JOb);
				zgb = JOb;
			}
		}
		rw_getTextOverRangeToSpeakImpl(ygb, zgb, iOb);
		var i;
		var UBc = iOb.length;
		var wordList = [];
		for (i = 0; i < UBc; i++) {
			wordList.push(iOb[i].word);
		}
		var MOb = new SpeechStream.SpeechRequest();
		MOb.setWordList(wordList, SpeechStream.SpeechRequestBookmarks.ALL);
		tNb.LNb = MOb.getText();
		tNb.bub = MOb.getFinalText();
		return tNb;
	} catch (err) {
		eya("err rw_getTextOverRangeToSpeak:" + "|" + err.message);
		return tNb;
	}
}
var NOb = 500;
function POb(QOb) {
	var lOb = QOb.length;
	if (lOb > 1 && QOb.substr(lOb - 2, 2) == ". ") {
		return QOb;
	} else if (lOb > 0 && QOb.substr(lOb - 1, 1) == ".") {
		return QOb + " ";
	} else {
		var bub = QOb.trimEndTH();
		var c = bub.charCodeAt(bub.length - 1);
		if (xxa(c) || c > 127) {
			return QOb + ". ";
		} else {
			return QOb;
		}
	}
}
function TOb(Usb) {
	var WBc = "";
	if (Usb.indexOf("<math") > -1) {
		return Usb;
	} else {
		var VOb = (Usb.length > 20);
		var bxb = 0;
		var UBc = Usb.length;
		var i = 0;
		var uvb;
		for (i = 0; i < UBc; i++) {
			uvb = Usb.charCodeAt(i);
			if (uvb > 127) {
				WBc += Usb.charAt(i);
			} else {
				switch (uvb) {
				case 35:
				case 40:
				case 41:
				case 91:
				case 93:
				case 95:
				case 123:
				case 124:
				case 125:
					WBc += " ";
					break;
				case 96:
					WBc += "'";
					break;
				case 38:
					WBc += "&amp;";
					break;
				case 34:
					WBc += "&quot;";
					break;
				case 60:
					WBc += "&lt";
					break;
				case 62:
					WBc += "&gt";
					break;
				default:
					WBc += Usb.charAt(i);
				}
				if (VOb) {
					if ((uvb >= 48 && uvb <= 57) || uvb == 44) {
						++bxb;
						if (bxb > 20) {
							WBc += ' ';
							bxb = 0;
						}
					} else {
						bxb = 0;
					}
				}
			}
		}
		return WBc;
	}
}
function rw_getTextOverRangeToSpeakImpl(gOb, hOb, iOb) {
	try {
		var hUb = gOb.node;
		var aOb = hOb.node;
		var ipb = SSDOM.getBody(hUb);
		var cOb = gOb.offset;
		var dOb = hOb.offset;
		var eOb = "";
		var jOb = hUb;
		var kOb = null;
		var lOb = 0;
		var mOb = true;
		var bxb = 0;
		var sWb = ZUb(jOb, cOb);
		var tWb = null;
		while (jOb != null) {
			if (bxb > NOb && NOb > 0) {
				if (Mda) {
					throw "Full selection will not be spoken due to its length.";
				} else {
					Bza("Full selection will not be spoken due to its length.");
				}
				return;
			}
			if (SSDOM.isSpecialCase(jOb)) {
				if (eOb.length > 0) {
					if (mya(eOb)) {
						iOb[bxb++] = new iNb(new THRange(ipb, sWb, tWb), eOb);
					}
					eOb = "";
				}
				var vOb = SSDOM.getTextFromNode(jOb);
				if (vOb.length > 0 && mya(vOb)) {
					if (SSDOM.isSpecialCaseHighlightable(jOb)) {
						var rOb = SSDOM.getFirstChildTextNode(jOb, false);
						var stb = SSDOM.getLastChildTextNode(jOb, false);
						var tOb = "";
						var uOb = "";
						if (rOb.nodeType == 1) {
							tOb = rOb.tagName.toLowerCase();
						}
						if (stb.nodeType == 1) {
							uOb = stb.tagName.toLowerCase();
						}
						if (rOb != null && rOb.nodeType == 3 && stb != null && stb.nodeType == 3) {
							sWb = ZUb(rOb, 0);
							tWb = ZUb(stb, stb.nodeValue.length);
						}
						if (rOb != null && rOb.nodeType == 1 && (tOb == "math" || rOb.isMathJax) && stb != null && stb.nodeType == 1 && (uOb == "math" || rOb.isMathJax)) {
							sWb = ZUb(rOb, 0);
							tWb = ZUb(stb, vOb.length);
						}
						iOb[bxb++] = new iNb(new THRange(ipb, sWb, tWb), vOb);
					} else {
						sWb = ZUb(jOb, -1);
						iOb[bxb++] = new iNb(new THRange(ipb, sWb, sWb), vOb);
					}
					eOb = "";
				}
				sWb = null;
				tWb = null;
				jOb = SSDOM.getNextNodeIgnoreChildren(jOb, false, aOb);
			} else if (jOb.nodeType == 1) {
				if (SpeechStream.pauseHandler.isPauseElement(jOb)) {
					eOb += SpeechStream.pauseHandler.encodePauseFromNode(jOb);
				}
				if (mOb) {
					kOb = SSDOM.getNextNodeAllowMoveToChild(jOb, true, aOb);
				} else {
					kOb = SSDOM.getNextNode(jOb, true, aOb);
				}
				if (kOb == null) {
					if (eOb.length > 0) {
						if (mya(eOb)) {
							iOb[bxb++] = new iNb(new THRange(ipb, sWb, tWb), POb(eOb));
						}
						eOb = "";
						sWb = null;
						tWb = null;
					}
					if (mOb) {
						jOb = SSDOM.getNextNodeAllowMoveToChild(jOb, false, aOb);
					} else {
						jOb = SSDOM.getNextNode(jOb, false, aOb);
					}
				} else {
					jOb = kOb;
				}
			} else if (jOb.nodeType == 3) {
				var vOb = SSDOM.getTextFromNode(jOb);
				if (vOb == null) {
					vOb = "";
				}
				var rzb = 0;
				if (aOb == jOb && dOb > -1) {
					vOb = vOb.substring(0, dOb);
				}
				if (hUb == jOb && cOb > 0) {
					vOb = vOb.substring(cOb);
					rzb = cOb;
				}
				if (vOb.length == 0 && eOb.length == 0) {
					sWb = null;
				} else {
					if (sWb == null || eOb.length == 0) {
						sWb = ZUb(jOb, rzb);
					}
					var FBc = KPb(vOb);
					while (FBc > -1) {
						if (FBc == 0) {
							if (eOb.length > 0) {
								if (mya(eOb)) {
									if (tWb == null) {
										tWb = ZUb(jOb, rzb);
									}
									var tcb = new THRange(ipb, sWb, tWb);
									if (!sda) {
										eOb = eOb + vOb.substr(0, 1);
									} else {
										var VRb = null;
										var APb = null;
										var EPb = jOb.parentNode;
										if (EPb != null && EPb.nodeType == 1) {
											VRb = EPb.getAttribute("class");
											APb = EPb.getAttribute("className");
										}
										var CPb = false;
										CPb = (VRb != null && (VRb.toLowerCase() == "x2" || VRb.toLowerCase() == "x3")) || (APb != null && (APb.toLowerCase() == "x2" || APb.toLowerCase() == "x3"));
										var DPb = false;
										while (CPb) {
											if (EPb.previousSibling != null && EPb.previousSibling.nodeType == 1) {
												VRb = EPb.previousSibling.getAttribute("class");
												APb = EPb.previousSibling.getAttribute("className");
												if (VRb != null && VRb.length > 3) {
													VRb = VRb.substr(VRb.length - 4).toLowerCase();
													if (VRb == "text") {
														DPb = true;
														break;
													}
												}
												if (APb != null && APb.length > 3) {
													APb = APb.substr(APb.length - 4).toLowerCase();
													if (APb == "text") {
														DPb = true;
														break;
													}
												}
												break;
											}
											EPb = EPb.parentNode;
											if (EPb != null && EPb.nodeType == 1 && EPb.tagName.toLowerCase() == "span") {}
											else {
												break;
											}
										}
										if (CPb) {
											if (!DPb) {
												eOb = eOb + vOb.substr(0, 1);
											}
										} else {
											if (jOb.previousSibling != null) {
												VRb = jOb.previousSibling.getAttribute("class");
												APb = jOb.previousSibling.getAttribute("className");
												if ((VRb != null && (VRb.toLowerCase() == "x2" || VRb.toLowerCase() == "x3")) || (APb != null && (APb.toLowerCase() == "x2" || APb.toLowerCase() == "x3"))) {
													eOb = eOb + vOb.substr(0, 1);
												}
											} else {
												var EPb = jOb;
												var Xxb = false;
												while (EPb.previousSibling == null && EPb.parentNode.tagName.toLowerCase() == "span") {
													EPb = EPb.parentNode;
													if (EPb.previousSibling != null) {
														Xxb = true;
													}
												}
												if (Xxb && EPb.previousSibling != null) {
													VRb = EPb.previousSibling.getAttribute("class");
													APb = EPb.previousSibling.getAttribute("className");
													if ((VRb != null && (VRb.toLowerCase() == "x2" || VRb.toLowerCase() == "x3")) || (APb != null && (APb.toLowerCase() == "x2" || APb.toLowerCase() == "x3"))) {
														eOb = eOb + vOb.substr(0, 1);
													}
												}
											}
										}
									}
									iOb[bxb++] = new iNb(tcb, eOb);
								}
								eOb = "";
								++rzb;
								vOb = vOb.substr(1);
							} else {
								vOb = vOb.substr(1);
								++rzb;
							}
						} else {
							var GPb = eOb + vOb.substring(0, FBc + 1);
							if (GPb.trimTH() == "*") {
								if (!(qfa && ZVb("*"))) {
									GPb = "";
								}
							}
							if (mya(GPb)) {
								tWb = ZUb(jOb, FBc + rzb);
								var tcb = new THRange(ipb, sWb, tWb);
								iOb[bxb++] = new iNb(tcb, GPb);
								if (bxb > NOb && NOb > 0) {
									if (Mda) {
										throw "Full selection will not be spoken due to its length.";
									} else {
										Bza("Full selection will not be spoken due to its length.");
									}
									return;
								}
							}
							eOb = "";
							rzb += FBc + 1;
							vOb = vOb.substring(FBc + 1);
						}
						sWb = ZUb(jOb, rzb);
						tWb = null;
						FBc = KPb(vOb);
					}
					if (vOb.length > 0) {
						eOb += vOb;
						tWb = ZUb(jOb, vOb.length + rzb);
						if (tWb == null) {
							eOb = "";
						}
					}
					if (jOb == aOb) {
						if (eOb.length > 0) {
							var tcb = new THRange(ipb, sWb, tWb);
							if (mya(eOb)) {
								iOb[bxb++] = new iNb(tcb, eOb);
							}
						}
						return;
					}
				}
				kOb = SSDOM.getNextNode(jOb, true, aOb);
				if (kOb == null) {
					if (eOb.length > 0) {
						if (mya(eOb)) {
							iOb[bxb++] = new iNb(new THRange(ipb, sWb, tWb), POb(eOb));
						}
						eOb = "";
						sWb = null;
						tWb = null;
					}
					jOb = SSDOM.getNextNode(jOb, false, aOb);
				} else {
					jOb = kOb;
				}
			} else {
				kOb = SSDOM.getNextNode(jOb, true, aOb);
				if (kOb == null) {
					if (eOb.length > 0) {
						if (mya(eOb)) {
							iOb[bxb++] = new iNb(new THRange(ipb, sWb, tWb), POb(eOb));
						}
						eOb = "";
						sWb = null;
						tWb = null;
					}
					jOb = SSDOM.getNextNode(jOb, false, aOb);
				} else {
					jOb = kOb;
				}
			}
			mOb = false;
		}
	} catch (err) {
		eya("err rw_getTextOverRangeToSpeakImpl:" + err.message);
	}
}
function KPb(LPb) {
	if (LPb == null || LPb.length == 0) {
		return -1;
	}
	var FBc = LPb.search("[\\s\"]");
	return FBc;
}
/* The following code is derived from MD5 hash functions (c) Paul Johnston, http://pajhome.org.uk/crypt/md5/. */
var MPb = 0;
var NPb = "";
var OPb = 8;
function PPb(s) {
	return iPb(QPb(gPb(s), s.length * OPb));
}
function QPb(x, fwa) {
	x[fwa >> 5] |= 0x80 << ((fwa) % 32);
	x[(((fwa + 64) >>> 9) << 4) + 14] = fwa;
	var a = 1732584193;
	var b = -271733879;
	var c = -1732584194;
	var d = 271733878;
	for (var i = 0; i < x.length; i += 16) {
		var RPb = a;
		var SPb = b;
		var TPb = c;
		var UPb = d;
		a = WPb(a, b, c, d, x[i + 0], 7, -680876936);
		d = WPb(d, a, b, c, x[i + 1], 12, -389564586);
		c = WPb(c, d, a, b, x[i + 2], 17, 606105819);
		b = WPb(b, c, d, a, x[i + 3], 22, -1044525330);
		a = WPb(a, b, c, d, x[i + 4], 7, -176418897);
		d = WPb(d, a, b, c, x[i + 5], 12, 1200080426);
		c = WPb(c, d, a, b, x[i + 6], 17, -1473231341);
		b = WPb(b, c, d, a, x[i + 7], 22, -45705983);
		a = WPb(a, b, c, d, x[i + 8], 7, 1770035416);
		d = WPb(d, a, b, c, x[i + 9], 12, -1958414417);
		c = WPb(c, d, a, b, x[i + 10], 17, -42063);
		b = WPb(b, c, d, a, x[i + 11], 22, -1990404162);
		a = WPb(a, b, c, d, x[i + 12], 7, 1804603682);
		d = WPb(d, a, b, c, x[i + 13], 12, -40341101);
		c = WPb(c, d, a, b, x[i + 14], 17, -1502002290);
		b = WPb(b, c, d, a, x[i + 15], 22, 1236535329);
		a = XPb(a, b, c, d, x[i + 1], 5, -165796510);
		d = XPb(d, a, b, c, x[i + 6], 9, -1069501632);
		c = XPb(c, d, a, b, x[i + 11], 14, 643717713);
		b = XPb(b, c, d, a, x[i + 0], 20, -373897302);
		a = XPb(a, b, c, d, x[i + 5], 5, -701558691);
		d = XPb(d, a, b, c, x[i + 10], 9, 38016083);
		c = XPb(c, d, a, b, x[i + 15], 14, -660478335);
		b = XPb(b, c, d, a, x[i + 4], 20, -405537848);
		a = XPb(a, b, c, d, x[i + 9], 5, 568446438);
		d = XPb(d, a, b, c, x[i + 14], 9, -1019803690);
		c = XPb(c, d, a, b, x[i + 3], 14, -187363961);
		b = XPb(b, c, d, a, x[i + 8], 20, 1163531501);
		a = XPb(a, b, c, d, x[i + 13], 5, -1444681467);
		d = XPb(d, a, b, c, x[i + 2], 9, -51403784);
		c = XPb(c, d, a, b, x[i + 7], 14, 1735328473);
		b = XPb(b, c, d, a, x[i + 12], 20, -1926607734);
		a = YPb(a, b, c, d, x[i + 5], 4, -378558);
		d = YPb(d, a, b, c, x[i + 8], 11, -2022574463);
		c = YPb(c, d, a, b, x[i + 11], 16, 1839030562);
		b = YPb(b, c, d, a, x[i + 14], 23, -35309556);
		a = YPb(a, b, c, d, x[i + 1], 4, -1530992060);
		d = YPb(d, a, b, c, x[i + 4], 11, 1272893353);
		c = YPb(c, d, a, b, x[i + 7], 16, -155497632);
		b = YPb(b, c, d, a, x[i + 10], 23, -1094730640);
		a = YPb(a, b, c, d, x[i + 13], 4, 681279174);
		d = YPb(d, a, b, c, x[i + 0], 11, -358537222);
		c = YPb(c, d, a, b, x[i + 3], 16, -722521979);
		b = YPb(b, c, d, a, x[i + 6], 23, 76029189);
		a = YPb(a, b, c, d, x[i + 9], 4, -640364487);
		d = YPb(d, a, b, c, x[i + 12], 11, -421815835);
		c = YPb(c, d, a, b, x[i + 15], 16, 530742520);
		b = YPb(b, c, d, a, x[i + 2], 23, -995338651);
		a = ZPb(a, b, c, d, x[i + 0], 6, -198630844);
		d = ZPb(d, a, b, c, x[i + 7], 10, 1126891415);
		c = ZPb(c, d, a, b, x[i + 14], 15, -1416354905);
		b = ZPb(b, c, d, a, x[i + 5], 21, -57434055);
		a = ZPb(a, b, c, d, x[i + 12], 6, 1700485571);
		d = ZPb(d, a, b, c, x[i + 3], 10, -1894986606);
		c = ZPb(c, d, a, b, x[i + 10], 15, -1051523);
		b = ZPb(b, c, d, a, x[i + 1], 21, -2054922799);
		a = ZPb(a, b, c, d, x[i + 8], 6, 1873313359);
		d = ZPb(d, a, b, c, x[i + 15], 10, -30611744);
		c = ZPb(c, d, a, b, x[i + 6], 15, -1560198380);
		b = ZPb(b, c, d, a, x[i + 13], 21, 1309151649);
		a = ZPb(a, b, c, d, x[i + 4], 6, -145523070);
		d = ZPb(d, a, b, c, x[i + 11], 10, -1120210379);
		c = ZPb(c, d, a, b, x[i + 2], 15, 718787259);
		b = ZPb(b, c, d, a, x[i + 9], 21, -343485551);
		a = cPb(a, RPb);
		b = cPb(b, SPb);
		c = cPb(c, TPb);
		d = cPb(d, UPb);
	}
	return Array(a, b, c, d);
}
function VPb(q, a, b, x, s, t) {
	return cPb(dPb(cPb(cPb(a, q), cPb(x, t)), s), b);
}
function WPb(a, b, c, d, x, s, t) {
	return VPb((b & c) | ((~b) & d), a, b, x, s, t);
}
function XPb(a, b, c, d, x, s, t) {
	return VPb((b & d) | (c & (~d)), a, b, x, s, t);
}
function YPb(a, b, c, d, x, s, t) {
	return VPb(b^c^d, a, b, x, s, t);
}
function ZPb(a, b, c, d, x, s, t) {
	return VPb(c^(b | (~d)), a, b, x, s, t);
}
function cPb(x, y) {
	var aPb = (x & 0xFFFF) + (y & 0xFFFF);
	var bPb = (x >> 16) + (y >> 16) + (aPb >> 16);
	return (bPb << 16) | (aPb & 0xFFFF);
}
function dPb(num, cnt) {
	return (num << cnt) | (num >>> (32 - cnt));
}
function gPb(LCc) {
	var ePb = Array();
	var mask = (1 << OPb) - 1;
	for (var i = 0; i < LCc.length * OPb; i += OPb) {
		ePb[i >> 5] |= (LCc.charCodeAt(i / OPb) & mask) << (i % 32);
	}
	return ePb;
}
function iPb(binarray) {
	var hPb = MPb ? "0123456789ABCDEF" : "0123456789abcdef";
	var LCc = "";
	for (var i = 0; i < binarray.length * 4; i++) {
		LCc += hPb.charAt((binarray[i >> 2] >> ((i % 4) * 8 + 4)) & 0xF) + hPb.charAt((binarray[i >> 2] >> ((i % 4) * 8)) & 0xF);
	}
	return LCc;
}
function $rw_hash(ZAc) {
	return PPb(ZAc);
}
function $rw_cachePage(XKb, cKb, p_strBookName) {
	var nPb = 0;
	try {
		if (Dea) {
			eba_cacheResult = "failure: The embedded speech toolbar cannot be added due to invalid html tag markup in this page.";
			window.external.completed(eba_cacheResult);
			return eba_cacheResult;
		}
		if (Bda == 300) {
			if (typeof(p_strBookName) == "string" && p_strBookName != null && p_strBookName.length > 0) {
				zca = p_strBookName;
			} else {
				zca = "1";
			}
			Ada = "1";
		}
		if (Mda) {
			if (cKb != null) {
				$rw_setSpeedValue(parseInt(cKb), 10);
			}
			if (XKb != null) {
				$rw_setVoice(XKb);
			}
			var lPb = SSDOM.getFirstSentence(document.body);
			$rw_doSelection(-2);
			nPb = sPb(lPb, nPb);
		} else {
			eba_cacheResult = "failure: The generate cache flag was set to false, no processing done for this page.";
			window.external.completed(eba_cacheResult);
			return eba_cacheResult;
		}
	} catch (err) {
		if (err.message != null) {
			eba_cacheResult = "failure:" + err.message;
		} else {
			eba_cacheResult = "failure:" + err;
		}
		window.external.completed(eba_cacheResult);
		return eba_cacheResult;
	}
	eba_cacheResult = "success";
	if (nPb > 0) {
		eba_cacheResult = eba_cacheResult + ":Warning, encountered " + nPb + " zero length speech files.";
	}
	window.external.completed(eba_cacheResult);
	return "success";
}
function sPb(tPb, uPb) {
	var mPb = tPb;
	var nPb = uPb;
	var oPb = 0;
	while (mPb != null) {
		var pPb = BOb(mPb, new Array());
		var bub = pPb.bub;
		if (bub == null || bub.trimTH().length == 0) {
			if (mPb != null) {
				var sentence = SSDOM.getNextSentence(mPb);
				if (sentence != null) {
					mPb = sentence;
				} else {
					if (oPb > 1) {
						throw "Speech engine generating empty files.";
					}
					zPb();
					return nPb;
				}
			} else {
				if (oPb > 1) {
					throw "Speech engine generating empty files.";
				}
				zPb();
				return nPb;
			}
		}
		var vPb = IRb(bub);
		var wPb = GQb();
		var xeb = window.external.Generate(bub, wPb, vPb);
		if (xeb == 1) {
			oPb = 0;
		} else {
			if (xeb == 2) {
				throw "Got a Failure response from the speech engine.";
			} else if (xeb == 3) {
				++nPb;
				++oPb;
				if (oPb > 4) {
					throw "Speech engine generating empty files.";
				}
			}
		}
		mPb = SSDOM.getNextSentence(mPb);
	}
	if (oPb > 1) {
		throw "Speech engine generating empty files.";
	}
	zPb();
	return nPb;
}
function zPb() {
	var Orb = document.getElementById("pageComplete");
	if (Orb != null) {
		Orb.click();
	}
}
function DQb(exb) {
	if (Pda) {
		var AQb = IQb();
		var BQb = IRb(exb);
		var CQb = JQb(BQb);
		return AQb + "/" + CQb + "/" + BQb;
	} else {
		return IQb() + "/" + IRb(exb);
	}
}
function GQb() {
	var LCc = pVb(xca, " ", "_");
	if (Bea) {
		return LQb(yca + "\\" + zca + "\\" + $rw_scholasticHash(Ada) + "\\" + Ada + "\\" + LCc + (Hda));
	} else {
		return LQb(yca + "\\" + zca + "\\" + Ada + "\\" + LCc + Hda);
	}
}
function IQb() {
	var LCc = pVb(xca, " ", "_");
	if (Bea) {
		return LQb(yca + "/" + zca + "/" + $rw_scholasticHash(Ada) + "/" + Ada + "/" + LCc + Hda);
	} else {
		return LQb(yca + "/" + zca + "/" + Ada + "/" + LCc + Hda);
	}
}
function JQb(kxb) {
	if (kxb == null || kxb.length < 2) {
		return "1/1";
	}
	return kxb.substr(0, 1) + "/" + kxb.substr(1, 1);
}
function LQb(ZAc) {
	return ZAc.replace(/[\x26\x3a\x2a\x3f\x22<>\x7c]/g, "");
}
var Kab = "";
function $rw_speechCacheGenErrorHandler(Xja) {
	Kab = Xja;
	var Orb = document.getElementById("pageFailed");
	if (Orb != null) {
		Orb.click();
	}
}
function $rw_getLastError() {
	return Kab;
}
function TQb(WQb) {
	if (WQb == null || WQb.ygb == null || WQb.zgb == null) {
		return null;
	}
	return new THRange(WQb.ygb.node.ownerDocument.body, ZUb(WQb.ygb.node, WQb.ygb.offset), ZUb(WQb.zgb.node, WQb.zgb.offset));
}
function dQb(mQb) {
	if (mQb == null || mQb.Upb == null || mQb.Vpb == null) {
		return null;
	} else {
		return new THRange(mQb.body, mQb.Upb, mQb.Vpb);
	}
}
function lQb(mQb) {
	if (mQb == null || mQb.Upb == null || mQb.Vpb == null) {
		return null;
	} else {
		var ygb = SSDOM.getCaretFromDomPosition(mQb.body, mQb.Upb.path, mQb.Upb.offset, true);
		var zgb = SSDOM.getCaretFromDomPosition(mQb.body, mQb.Vpb.path, mQb.Vpb.offset, false);
		if (ygb != null && zgb != null) {
			return new qqa(ygb, zgb);
		} else {
			return null;
		}
	}
}
function rQb(zeb) {
	if (zeb == null || zeb.Upb == null || zeb.Vpb == null) {
		return null;
	} else {
		var ygb = SSDOM.getCaretFromDomPosition(zeb.body, zeb.Upb.path, zeb.Upb.offset, true);
		var zgb = SSDOM.getCaretFromDomPosition(zeb.body, zeb.Vpb.path, zeb.Vpb.offset, false);
		if (ygb != null && zgb != null) {
			return new qqa(ygb, zgb);
		} else {
			return null;
		}
	}
}
function xQb(Ghb) {
	return ZUb(Ghb.node, Ghb.offset);
}
function DRb(OXb, HRb) {
	return SSDOM.getCaretFromDomPosition(OXb, HRb.path, HRb.offset, true);
}
function IRb(ZAc) {
	if (Bda == 200) {
		ZAc = ZAc.replace(/\s+/g, " ");
	} else {
		ZAc = ZAc.replace(/(\x3cbookmark\x20mark\x3d\x22(\d)+\x22\x2f\x3e)/g, "");
		ZAc = ZAc.replace(/[\s\xA0]+/g, " ");
	}
	return PPb(ZAc);
}
function $rw_scholasticHashShort(p_asset) {
	var bub = p_asset.replace(/^0+|[^0-9]/g, "");
	return "0001".substring(0, 4 - bub.length) + bub.substring(0, 4);
}
function $rw_scholasticHash(p_asset) {
	var bub = p_asset.replace(/^0+|[^0-9]/g, "");
	if (bub.length < 4) {
		bub = "0001".substring(0, 4 - bub.length) + bub;
	} else {
		bub = bub.substring(0, 4);
	}
	return bub;
}
SpeechStream.pauseHandler = (function () {
	var self = {};
	var MRb = {
		"none" : 0,
		"x-weak" : 233,
		"weak" : 466,
		"medium" : 700,
		"strong" : 933,
		"x-string" : 1166
	};
	var NRb = "~!pause:";
	var ORb = "!~";
	var PRb = NRb.length + 4;
	var QRb = "silence";
	var RRb = "break";
	self.getStrengthValue = function (p_strStrength) {
		if (typeof(MRb[p_strStrength]) != "undefined") {
			return MRb[p_strStrength];
		} else {
			return 0;
		}
	};
	self.getPauseTimeFromNode = function (Bub) {
		var xRb = 0;
		var qRb = Bub.tagName.toLowerCase();
		if (qRb == QRb) {
			var VRb = Bub.getAttribute("msec");
			if (VRb != null) {
				xRb = parseInt(VRb, 10);
				if (isNaN(xRb)) {
					xRb = 0;
				}
			}
		} else if (qRb == RRb) {
			var VRb = Bub.getAttribute("time");
			if (VRb != null && VRb.length >= 2) {
				if (VRb.substr(VRb.length - 2) == "ms") {
					xRb = parseInt(VRb, 10);
				} else if (VRb.substr(VRb.length - 1) == "s") {
					xRb = parseInt(VRb, 10) * 1000;
				}
				if (isNaN(xRb)) {
					xRb = 0;
				}
			} else {
				VRb = Bub.getAttribute("strength");
				if (VRb != null) {
					xRb = self.getStrengthValue(VRb.toLowerCase());
				} else {
					xRb = self.getStrengthValue("medium");
				}
			}
		}
		return xRb;
	};
	self.encodedPause = function (p_nMsec) {
		if (!isNaN(p_nMsec) && p_nMsec >= 0 && p_nMsec < 10000) {
			return NRb + p_nMsec + ORb;
		} else {
			return "";
		}
	};
	self.encodePauseFromNode = function (Bub) {
		return self.encodedPause(self.getPauseTimeFromNode(Bub));
	};
	self.encodePauseFromString = function (exb) {
		if (exb.indexOf(RRb) > -1 || exb.indexOf(QRb) > -1) {
			exb = XRb(exb, QRb, "msec");
			exb = XRb(exb, RRb, "time");
			exb = XRb(exb, RRb, "strength");
			exb = XRb(exb, RRb, "");
		}
		return exb;
	};
	function XRb(exb, ZRb, pRb) {
		var WRb = " " + pRb + "=\"";
		var bRb = "</" + ZRb + ">";
		var cRb;
		var dRb;
		var eRb;
		var fRb;
		var gRb;
		var hRb;
		var iRb;
		cRb = exb.indexOf("<" + ZRb);
		while (cRb > -1) {
			eRb = exb.indexOf(">", cRb);
			fRb = exb.indexOf("/>", cRb);
			if (eRb > cRb && (fRb < 0 || eRb < fRb)) {
				dRb = eRb;
				iRb = exb.indexOf(bRb);
			} else if (fRb > cRb && (eRb < 0 || fRb < eRb)) {
				dRb = eRb;
				iRb = -1;
			} else {
				eya("Error in pause handler, appears to be due to unclosed element in html.");
				break;
			}
			var xRb;
			if (pRb == "" && ZRb == RRb) {
				xRb = self.getStrengthValue("medium");
			} else {
				gRb = exb.indexOf(WRb, cRb);
				hRb = exb.indexOf("\"", gRb + WRb.length);
				if (gRb == -1) {
					break;
				} else if (gRb > dRb) {
					xRb = -1;
				} else if (hRb > dRb || hRb == -1) {
					eya("Error in pause handler, appears to be due to unclosed string in html attribute.");
					break;
				} else {
					var kRb = exb.substring(gRb + WRb.length, hRb);
					xRb = nRb(kRb, pRb);
				}
			}
			if (xRb > -1) {
				var mBc = exb.substr(0, cRb);
				mBc += self.encodedPause(xRb);
				if (iRb > -1) {
					mBc += exb.substring(dRb + 1, iRb);
					mBc += exb.substr(iRb + bRb.length);
				} else {
					mBc += exb.substring(dRb + 2);
				}
				exb = mBc;
				cRb = exb.indexOf("<" + ZRb, cRb + 1);
			}
			cRb = exb.indexOf("<" + ZRb, cRb + 1);
		}
		return exb;
	}
	function nRb(oRb, pRb) {
		var xRb = 0;
		if (pRb == "msec") {
			xRb = parseInt(oRb, 10);
			if (isNaN(xRb)) {
				xRb = 0;
			}
		} else if (pRb == "time") {
			if (oRb.substr(oRb.length - 2) == "ms") {
				xRb = parseInt(oRb.substr(0, oRb.length - 2), 10);
				if (isNaN(xRb)) {
					xRb = 0;
				}
			} else {
				xRb = parseInt(oRb.substr(0, oRb.length - 1), 10) * 1000;
				if (isNaN(xRb)) {
					xRb = 0;
				}
			}
		} else if (pRb == "strength") {
			xRb = self.getStrengthValue(oRb);
		}
		return xRb;
	}
	self.isPauseElement = function (Bub) {
		if (Bub.nodeType == 1) {
			var qRb = Bub.tagName.toLowerCase();
			return (qRb == QRb || qRb == RRb);
		} else {
			return false;
		}
	};
	self.hasPauseKey = function (p_strCode) {
		if (typeof(p_strCode) == "string") {
			var Qmb = p_strCode.indexOf(NRb);
			if (Qmb > -1) {
				var Cyb = p_strCode.indexOf(ORb, Qmb);
				if (Cyb > -1 && (Cyb - Qmb) <= PRb) {
					return true;
				}
			}
		} else {
			return false;
		}
	};
	self.decodedPause = function (p_strCode) {
		var tRb = [];
		if (typeof(p_strCode) == "string") {
			var Qmb = p_strCode.indexOf(NRb);
			while (Qmb > -1) {
				var Cyb = p_strCode.indexOf(ORb, Qmb);
				if (Cyb > -1 && (Cyb - Qmb) <= PRb) {
					if (Qmb > 0) {
						tRb.push(p_strCode.substr(0, Qmb));
					}
					var mBc = p_strCode.substring(Qmb + NRb.length, Cyb);
					var xRb = parseInt(mBc, 10);
					if (!isNaN(xRb)) {
						tRb.push("<silence msec=\"" + mBc + "\"/>");
					}
					p_strCode = p_strCode.substr(Cyb + ORb.length);
					Qmb = p_strCode.indexOf(NRb);
				}
			}
			tRb.push(p_strCode);
		}
		return tRb;
	};
	return self;
})();
function rw_getDisplayWidth(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	var nW = (HWb.innerWidth) ? HWb.innerWidth : HWb.document.body.offsetWidth;
	return nW;
}
function rw_getDisplayWidthAdjusted(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	var nW = ((HWb.innerWidth) ? HWb.innerWidth : HWb.document.body.offsetWidth) - rw_getScrollBarWidth(HWb);
	return nW;
}
function rw_getDocumentDisplayWidth(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	var nW = (HWb.innerWidth) ? HWb.innerWidth : HWb.document.documentElement.offsetWidth;
	return nW;
}
function rw_getDocumentDisplayWidthAdjusted(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	var nW = ((HWb.innerWidth) ? HWb.innerWidth : HWb.document.documentElement.offsetWidth) - rw_getScrollBarWidth(HWb);
	return nW;
}
function rw_getDisplayHeight(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (hKb) {
		return top.window.innerHeight;
	}
	if (Jfa) {
		return rw_getDocumentDisplayHeight(HWb);
	} else {
		var nH = (HWb.innerHeight) ? HWb.innerHeight : HWb.document.body.offsetHeight;
		return nH;
	}
}
function rw_getDisplayHeightAdjusted(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (hKb) {
		return top.window.innerHeight - window.pageYOffset;
	}
	if (Jfa) {
		return rw_getDocumentDisplayHeightAdjusted(HWb);
	} else {
		var nH = ((HWb.innerHeight) ? HWb.innerHeight : HWb.document.body.offsetHeight) - rw_getScrollBarHeight(HWb);
		return nH;
	}
}
function rw_getDocumentDisplayHeight(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (hKb) {
		return top.window.innerHeight;
	}
	var nH = (HWb.innerHeight) ? HWb.innerHeight : HWb.document.documentElement.offsetHeight;
	return nH;
}
function rw_getDocumentDisplayHeightAdjusted(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	var nH = ((HWb.innerHeight) ? HWb.innerHeight : HWb.document.documentElement.offsetHeight) - rw_getScrollBarHeight(HWb);
	return nH;
}
function OSb(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (HWb.document.compatMode == "CSS1Compat" && HWb.document.body.parentNode && HWb.document.body.parentNode.scrollLeft) {
		return HWb.document.body.parentNode.scrollLeft;
	}
	var n = (HWb.pageXOffset) ? HWb.pageXOffset : (HWb.scrollX) ? HWb.scrollX : (HWb.document.body.scrollLeft) ? HWb.document.body.scrollLeft : (HWb.document.documentElement.scrollLeft) ? HWb.document.documentElement.scrollLeft : 0;
	return n;
}
function rw_getScreenOffsetLeft(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (HWb.pageXOffset && HWb.pageXOffset > 0) {
		return HWb.pageXOffset;
	} else if (HWb.document.body.scrollLeft && HWb.document.body.scrollLeft > 0) {
		return HWb.document.body.scrollLeft;
	} else if (HWb.document.documentElement.scrollLeft && HWb.document.documentElement.scrollLeft > 0) {
		return HWb.document.documentElement.scrollLeft;
	}
	return 0;
}
function rw_getScreenOffsetTop(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (HWb.pageYOffset && HWb.pageYOffset > 0) {
		return HWb.pageYOffset;
	} else if (HWb.document.body.scrollTop && HWb.document.body.scrollTop > 0) {
		return HWb.document.body.scrollTop;
	} else if (HWb.document.documentElement.scrollTop && HWb.document.documentElement.scrollTop > 0) {
		return HWb.document.documentElement.scrollTop;
	}
	return 0;
}
function USb(Dqb) {
	if (Dqb.scrollLeft && Dqb.scrollLeft > 0) {
		return Dqb.scrollLeft;
	}
	if (Dqb.tagName.toLowerCase() == "body" && Dqb.ownerDocument && Dqb.ownerDocument.documentElement && Dqb.ownerDocument.documentElement.scrollLeft) {
		return Dqb.ownerDocument.documentElement.scrollLeft;
	}
	return 0;
}
function WSb(Dqb) {
	if (Dqb.scrollTop && Dqb.scrollTop > 0) {
		return Dqb.scrollTop;
	}
	if (Dqb.tagName.toLowerCase() == "body" && Dqb.ownerDocument && Dqb.ownerDocument.documentElement && Dqb.ownerDocument.documentElement.scrollTop) {
		return Dqb.ownerDocument.documentElement.scrollTop;
	}
	return 0;
}
function rw_getScrollBarWidth(HWb) {
	if (Lfa) {
		if (Jfa) {
			return 20;
		} else {
			if (typeof(HWb) == "undefined") {
				HWb = window;
			}
			if (HWb.document.compatMode.equalsTH("CSS1Compat")) {
				return (HWb.document.documentElement.offsetWidth - HWb.document.documentElement.clientWidth);
			} else {
				return (HWb.document.body.offsetWidth - HWb.document.body.clientWidth);
			}
		}
	} else {
		if (window.scrollMaxY > 0 || Yfa) {
			return 18;
		} else {
			return 4;
		}
	}
}
function rw_getScrollBarHeight(HWb) {
	if (typeof(HWb) == "undefined") {
		HWb = window;
	}
	if (Lfa) {
		if (Jfa) {
			return 20;
		} else {
			if (HWb.document.compatMode.equalsTH("CSS1Compat")) {
				return (HWb.document.documentElement.offsetHeight - HWb.document.documentElement.clientHeight);
			} else {
				return (HWb.document.body.offsetHeight - HWb.document.body.clientHeight);
			}
		}
	} else {
		if (HWb.scrollMaxX > 0) {
			return 18;
		} else {
			return 4;
		}
	}
}
function eSb() {
	var MWb = null;
	var dSb = null;
	if (Ufa) {
		var range = document.selection.createRange();
		if (range == null || range.text == null || range.text.length == 0) {
			var kSb = SSDOM.getFrameSelectionOldIE(window);
			if (kSb.dSb) {
				MWb = kSb.MWb;
				dSb = kSb.dSb;
			}
		} else {
			MWb = window;
			dSb = range;
		}
		if (dSb != null) {
			var ueb = dSb.duplicate();
			ueb.collapse();
			var qlb = ueb.parentElement();
			if (SSDOM.isIgnored(qlb)) {
				dSb = null;
			}
		}
		if (dSb != null && dSb.parentElement() != null && dSb.parentElement().tagName.toLowerCase() == "input") {
			dSb = new String(dSb.text);
		}
	} else {
		if (Zja != null) {
			aja.getSelection().addRange(Zja);
		}
		var iSb = window.getSelection();
		var jSb = null;
		if (!iSb.isCollapsed && iSb.toString().trimTH().length > 0) {
			MWb = window;
			jSb = iSb;
		} else {
			if (WEb && WEb.selectionStart != WEb.selectionEnd) {
				return {
					frame : window,
					range : new String(WEb.value.substring(WEb.selectionStart, WEb.selectionEnd))
				};
			}
			var kSb = SSDOM.getFrameSelectionSFF(window);
			if (kSb.jSb) {
				MWb = kSb.MWb;
				jSb = kSb.jSb;
			}
		}
		if (jSb == null) {
			return null;
		}
		if (jSb.anchorNode) {
			if (SSDOM.isIgnored(jSb.anchorNode)) {
				return null;
			}
		}
		if (jSb.focusNode && jSb.focusNode.id) {
			if (jSb.focusNode.id == "flashcontent") {
				return null;
			}
		}
		if (jSb.anchorNode != null && jSb.anchorNode == jSb.focusNode && jSb.anchorOffset == jSb.focusOffset) {
			return null;
		}
		var lSb = null;
		if (jSb.getRangeAt) {
			lSb = jSb.getRangeAt(0);
		} else {
			var range = SSDOM.getRangeObject();
			if (range != null) {
				if (jSb.anchorNode == jSb.focusNode && jSb.anchorOffset == jSb.focusOffset) {
					range = SSDOM.getRangeFromSelectionPoint(jSb);
				} else {
					range.setStart(jSb.anchorNode, jSb.anchorOffset);
					range.setEnd(jSb.focusNode, jSb.focusOffset);
					if (range.toString().length == 0) {
						range.setStart(jSb.focusNode, jSb.focusOffset);
						range.setEnd(jSb.anchorNode, jSb.anchorOffset);
					}
				}
				lSb = range;
			}
		}
		if (lSb != null) {
			var mSb = lSb.startContainer;
			var nSb = lSb.startOffset;
			var oSb = lSb.endContainer;
			var pSb = lSb.endOffset;
			if (mSb.nodeType != 3) {
				if (mSb.nodeType != 1) {
					return null;
				} else {
					if (nSb > 0) {
						if (mSb.hasChildNodes() && mSb.childNodes.length > nSb) {
							mSb = mSb.childNodes[nSb];
							if (mSb.nodeType == 3) {
								nSb = 0;
							} else {
								nSb = 0;
								if (mSb.toString() == "[object HTMLEmbedElement]") {
									return null;
								}
							}
						}
					}
				}
			}
			if (oSb.nodeType != 3) {
				if (oSb.nodeType != 1) {
					return null;
				} else {
					if (oSb.hasChildNodes()) {
						if (oSb.childNodes.length > pSb) {
							oSb = oSb.childNodes[pSb];
						} else {
							oSb = oSb.childNodes[pSb - 1];
							if (oSb.nodeType != 3) {
								var wvb = SSDOM.getLastChildTextNode(oSb, true);
								if (wvb != null) {
									oSb = wvb;
								}
							}
						}
					}
					if (oSb.nodeType != 3) {
						var rSb = SSDOM.getPreviousNode(mSb, true, null);
						var wvb = SSDOM.getPreviousTextNode(oSb, true, rSb);
						if (wvb != null) {
							oSb = wvb;
						}
					}
					if (oSb.nodeType == 3) {
						pSb = oSb.nodeValue.length;
					} else {
						pSb = 0;
					}
				}
			}
			dSb = new vra(mSb, nSb, oSb, pSb);
		} else {
			return null;
		}
	}
	if (MWb != null && dSb != null) {
		return {
			frame : MWb,
			range : dSb
		};
	} else {
		return null;
	}
}
function uSb() {
	var wSb = eSb();
	if (wSb != null && wSb.range != null && !(wSb.range instanceof String)) {
		if (Ufa) {
			wSb.range = CXb(wSb.frame.document.body, wSb.range);
		} else if (wSb.range instanceof vra) {
			wSb.range = dQb(wSb.range);
		}
	}
	return wSb;
}
function xSb() {
	var rBc = eSb();
	if (rBc != null) {
		var wSb = rBc.range;
		if (wSb instanceof String) {
			return wSb;
		} else if (wSb instanceof vra) {
			return wSb.toString();
		} else {
			return rBc.range.text;
		}
	}
	return "";
}
function ySb(Bub) {
	if (typeof(eba_no_scroll) == "boolean" && eba_no_scroll) {
		return;
	}
	try {
		var MWb = SSDOM.getWindow(Bub);
		if (MWb == null || Bub == null || Bub.parentNode == null) {
			return;
		}
		var x = 0;
		var y = 0;
		var rBc = Bub;
		if (rBc.nodeType == 3) {
			rBc = rBc.parentNode;
		}
		var CTb = null;
		var DTb = rBc;
		var opb = rBc.ownerDocument.body;
		var FTb = false;
		var ppb = null;
		while (DTb != null && DTb != opb) {
			if (DTb.tagName.toLowerCase() == "div" || DTb.tagName.toLowerCase() == "form") {
				if (QTb(DTb)) {
					FTb = true;
					CTb = dTb(rBc, DTb, CTb);
					ppb = DTb;
					rBc = DTb;
				}
			}
			DTb = DTb.parentNode;
		}
		if (ppb != null) {
			rBc = ppb;
		}
		while (rBc != null) {
			x += rBc.offsetLeft;
			y += rBc.offsetTop;
			rBc = rBc.offsetParent;
		}
		if (CTb != null) {
			y += CTb.y;
			x += CTb.x;
		}
		var HTb;
		var ITb;
		var JTb;
		var KTb;
		var LTb = 30;
		if (Bub.nodeType == 3) {
			LTb = 10 + 5 * Bub.nodeValue.length;
			if (LTb > 60) {
				LTb = 60;
			}
		}
		HTb = rw_getScreenOffsetLeft(MWb);
		ITb = rw_getScreenOffsetTop(MWb);
		if (typeof(MWb.innerWidth) == 'number') {
			JTb = MWb.innerWidth;
			KTb = MWb.innerHeight;
		} else if (MWb.document.documentElement.clientHeight > 0 && MWb.document.documentElement.clientWidth > 0) {
			JTb = MWb.document.documentElement.clientWidth;
			KTb = MWb.document.documentElement.clientHeight;
		} else {
			JTb = MWb.document.body.clientWidth;
			KTb = MWb.document.body.clientHeight;
		}
		JTb = JTb - LTb;
		KTb = KTb - 20;
		var MTb;
		var NTb;
		if (FTb) {}

		MTb = (x < HTb || x > (HTb + JTb));
		NTb = (y < ITb || y > (ITb + KTb));
		if (MTb || NTb && (x != 0 || y != 0)) {
			if (x > (HTb + JTb)) {
				x = (x + HTb) / 2;
			}
			if (y > (ITb + KTb)) {
				y = (y + ITb) / 2;
			}
			var OTb = $g_bMouseSpeech;
			if ($g_bMouseSpeech) {
				$g_bMouseSpeech = false;
			}
			MWb.scrollTo((MTb ? x : HTb), (NTb ? y : ITb));
			if (bfa) {
				jKb();
				lKb();
			}
			if (OTb) {
				var PTb = function () {
					$g_bMouseSpeech = true;
				};
				setTimeout(PTb, 500);
			}
		}
	} catch (ignore) {
		thLogE(ignore);
	}
	g_bDidScroll = false;
}
function QTb(Dqb) {
	var spb = Dqb.clientHeight;
	var tpb = Dqb.clientWidth;
	var upb = SSDOM.getComputedStyle(Dqb);
	var vpb = false;
	if (upb != null && upb.overflow != "visible" && upb.display != "inline") {
		if (Dqb.scrollHeight > spb && upb.overflowY != "visible") {
			vpb = true;
		}
		if (Dqb.scrollWidth > tpb && upb.overflowX != "visible") {
			vpb = true;
		}
	}
	return vpb;
}
function dTb(Bub, Dqb, gTb) {
	var Ppb;
	var Qpb;
	var spb = Dqb.clientHeight;
	var tpb = Dqb.clientWidth;
	var aTb = 0;
	var bTb = 0;
	var OCc = Bub;
	while (OCc != Dqb && OCc != null) {
		aTb += OCc.offsetTop;
		bTb += OCc.offsetLeft;
		OCc = kTb(OCc, Dqb);
	}
	if (OCc == null) {
		bTb -= Dqb.offsetLeft;
		aTb -= Dqb.offsetTop;
	}
	if (gTb != null) {
		aTb += gTb.y;
		bTb += gTb.x;
	}
	if (Dqb.scrollTop > aTb || (Dqb.scrollTop + spb) < (aTb + Bub.offsetHeight)) {
		if (spb > (Bub.offsetHeight * 6)) {
			Dqb.scrollTop = aTb - Bub.offsetHeight;
		} else {
			Dqb.scrollTop = aTb;
		}
	}
	if (Dqb.scrollLeft > bTb || (Dqb.scrollLeft + tpb) < (bTb + Bub.offsetWidth)) {
		Dqb.scrollLeft = bTb;
	}
	Ppb = bTb - Dqb.scrollLeft;
	Qpb = aTb - Dqb.scrollTop;
	return {
		x : Ppb,
		y : Qpb
	};
}
function kTb(DBc, mTb) {
	var iTb = DBc;
	var jTb = iTb.offsetParent;
	if (jTb == null) {
		return null;
	}
	if (mTb == null) {
		return jTb;
	}
	while (iTb != null && iTb != jTb) {
		if (iTb == mTb) {
			return null;
		}
		iTb = iTb.parentNode;
	}
	return jTb;
}
function oTb() {
	var LCc = "" + "rw_getDisplayWidth=" + rw_getDisplayWidth() + "  rw_getDisplayWidthAdjusted=" + rw_getDisplayWidthAdjusted() + "  rw_getDocumentDisplayWidth=" + rw_getDocumentDisplayWidth() + "  rw_getDocumentDisplayWidthAdjusted=" + rw_getDocumentDisplayWidthAdjusted() + "  rw_getDisplayHeight=" + rw_getDisplayHeight() + "  rw_getDisplayHeightAdjusted=" + rw_getDisplayHeightAdjusted() + "  rw_getDocumentDisplayHeight=" + rw_getDocumentDisplayHeight() + "  rw_getDocumentDisplayHeightAdjusted=" + rw_getDocumentDisplayHeightAdjusted() + "  rw_getScreenOffsetLeft=" + rw_getScreenOffsetLeft() + "  rw_getScreenOffsetTop=" + rw_getScreenOffsetTop() + "  rw_getScrollBarWidth=" + rw_getScrollBarWidth() + "  rw_getScrollBarHeight=" + rw_getScrollBarHeight();
	Bza(LCc);
}
function pTb(Brb) {
	ySb(document.getElementById(Brb));
}
if (document.getElementById("testbuildspan")) {
	document.getElementById("testbuildspan").innerHTML = "test build 4";
}
SpeechStream.stringAssist = new function () {
	var rTb = String.fromCharCode(160);
	this.SOFT_HYPHEN = String.fromCharCode(173);
	var self = this;
	var sTb = /[\s\xA0]+/g;
	this.REGEX_WORD_SPLIT = null;
	this.REGEX_NON_WORD = null;
	function uTb() {
		var tTb = "";
		var i;
		var LCc = '\\s\xA0!"%&()*+,-./:;<=>?[]^_{|}����������������\u201d\u201c';
		for (i = 0; i < LCc.length; i++) {
			tTb += '\\' + LCc.charAt(i);
		}
		self.REGEX_WORD_SPLIT = new RegExp('([' + tTb + '])', 'g');
		self.REGEX_NON_WORD = new RegExp('(\\S*[0-9|�$@#]\\S*)', 'g');
	}
	uTb();
	this.trimWordsFromStart = function (exb, p_nWords) {
		if (exb == null) {
			return "";
		}
		var Nub = exb;
		Nub = self.reduceWhiteSpace(Nub);
		if (Nub.length == 0) {
			return "";
		}
		var AUb = 0;
		var FBc = Nub.indexOf(' ');
		while (FBc > -1) {
			if (AUb == p_nWords) {
				Nub = Nub.substr(0, FBc);
				break;
			}
			++AUb;
			FBc = Nub.indexOf(' ', FBc + 1);
		}
		return Nub;
	};
	this.trimWordsFromEnd = function (exb, p_nWords) {
		if (exb == null) {
			return "";
		}
		var Nub = exb;
		Nub = self.reduceWhiteSpace(Nub);
		if (Nub.length == 0) {
			return "";
		}
		var AUb = 0;
		var FBc = Nub.lastIndexOf(' ');
		while (FBc > -1) {
			if (AUb == p_nWords) {
				Nub = Nub.substr(FBc + 1);
				break;
			}
			++AUb;
			FBc = Nub.lastIndexOf(' ', FBc - 1);
		}
		return Nub;
	};
	this.trimWordsFromStartAsArray = function (exb, p_nWords) {
		var words = self.convertTextToArray(exb);
		if (words.length <= p_nWords) {
			return words;
		} else {
			return words.slice(0, p_nWords);
		}
	};
	this.trimWordsFromEndAsArray = function (exb, p_nWords) {
		var words = self.convertTextToArray(exb);
		if (words.length <= p_nWords) {
			return words;
		} else {
			return words.slice(words.length - p_nWords, words.length);
		}
	};
	this.getWordListToBreak = function (exb, p_nWords, p_bForward) {
		var wordList;
		if (p_nWords > -1) {
			wordList = p_bForward ? SpeechStream.stringAssist.trimWordsFromStartAsArray(exb, p_nWords) : SpeechStream.stringAssist.trimWordsFromEndAsArray(exb, p_nWords);
		} else {
			wordList = SpeechStream.stringAssist.convertTextToArray(exb);
		}
		if (!p_bForward) {
			wordList.reverse();
		}
		var FUb = false;
		var UBc = wordList.length;
		var i;
		var HUb,
		bPost;
		var WBc;
		for (i = 0; i < UBc; i++) {
			if (i == p_nWords) {
				FUb = true;
			}
			if (!FUb) {
				var JUb = i;
				WBc = wordList[i];
				HUb = !vxa(WBc.charCodeAt(0));
				bPost = !vxa(WBc.charCodeAt(WBc.length - 1));
				if (HUb || bPost) {
					FUb = true;
					if (!HUb && bPost && p_bForward) {
						i++;
					} else if (HUb && !bPost && !p_bForward) {
						i++;
					}
				}
				WBc = SpeechStream.stringAssist.bra(WBc);
				wordList[JUb] = WBc;
			}
			if (FUb) {
				wordList = wordList.slice(0, i);
				break;
			}
		}
		if (!p_bForward) {
			wordList.reverse();
		}
		return wordList;
	};
	this.convertTextToArray = function (exb) {
		if (exb == null || exb.length == 0) {
			return [];
		}
		var oCc = self.reduceWhiteSpace(exb).trimTH();
		return oCc.split(" ");
	};
	this.reduceWhiteSpace = function (exb) {
		return exb.replace(sTb, " ");
	};
	this.filterWordForPronunciation = function (Kya) {
		if (Kya == null || Kya.length == 0) {
			return Kya;
		}
		var UBc = Kya.length;
		for (var i = 0; i < UBc; i++) {
			var uvb = Kya.charCodeAt(i);
			if (!(uvb == 39 || uvb == 45 || (uvb > 47 && uvb < 58) || (uvb > 64 && uvb < 91) || (uvb > 94 && uvb < 123) || (uvb > 127 && uvb != 160))) {
				Kya = Kya.replace(Kya.charAt(i), ' ');
			}
		}
		return Kya.trimTH();
	};
	this.filterToListOfWords = function (ZAc, p_filterList, p_regExSplit) {
		var oCc;
		var wordList;
		var PUb = [];
		if (typeof(p_regExSplit) == "undefined" || p_regExSplit == null) {
			p_regExSplit = self.REGEX_WORD_SPLIT;
		}
		oCc = ZAc.replace(self.REGEX_NON_WORD, ' ');
		oCc = oCc.replace(p_regExSplit, ' ');
		oCc = tinymce.trim(oCc.replace(/(\s+)/g, ' '));
		wordList = oCc.split(" ");
		tinymce.each(wordList, function (v) {
			if (PUb.indexOf(v) == -1 && p_filterList.indexOf(v) == -1) {
				PUb.push(v);
			}
		});
		return PUb;
	};
	this.bra = function (ZAc) {
		var UBc = ZAc.length;
		for (var i = 0; i < UBc; i++) {
			var uvb = ZAc.charCodeAt(i);
			if (!(uvb == 39 || uvb == 45 || (uvb > 47 && uvb < 58) || (uvb > 64 && uvb < 91) || (uvb > 94 && uvb < 123) || (uvb > 127 && (uvb != 160 && uvb != 163 && uvb != 8364)))) {
				ZAc = ZAc.replace(ZAc.charAt(i), ' ');
			}
		}
		return ZAc.replace(self.REGEX_WORD_SPLIT, ' ').trimTH();
	};
	this.checkIfIncludesBreak = function (Usb) {
		return !(vxa(Usb.charCodeAt(0)) && vxa(Usb.charCodeAt(Usb.length - 1)));
	};
	this.filterNbsp = function (exb) {
		if (typeof(exb) == "string" && exb.indexOf(rTb) > -1) {
			var FBc = exb.indexOf(rTb);
			while (FBc > -1) {
				exb = exb.replace(rTb, " ");
				FBc = exb.indexOf(rTb, FBc + 1);
			}
		}
		return exb;
	};
	this.filter = function (exb, p_strFilter, sVb) {
		if (typeof(exb) == "string" && typeof(p_strFilter) == "string" && typeof(sVb) == "string") {
			if (exb.indexOf(p_strFilter) > -1) {
				var FBc = exb.indexOf(p_strFilter) > -1;
				while (FBc > -1) {
					exb = exb.replace(p_strFilter, sVb);
					FBc = exb.indexOf(p_strFilter, FBc + 1);
				}
			}
		}
		return exb;
	};
	this.compareStrings = function (p_str1, p_str2) {
		var TAc = self.reduceWhiteSpace(p_str1);
		var VAc = self.reduceWhiteSpace(p_str2);
		return TAc == VAc;
	};
};
function ZUb(Bub, Thb) {
	try {
		if (Bub == null) {
			return null;
		}
		if (Bub.nodeType == 1 || Bub.nodeType == 3) {
			var qlb = SSDOM.checkForSpecialParent(Bub);
			if (qlb != null) {
				return new THDomRefPt(SSDOM.getPositionInDom(qlb), Thb);
			}
			var rzb;
			var Ulb;
			if (Bub.nodeType == 1) {
				rzb = 0;
				Ulb = Bub;
			} else {
				rzb = jUb(Bub);
				Ulb = Bub.parentNode;
			}
			var attr = Ulb.getAttribute("rwstate");
			var dUb = Ulb.getAttribute(daa);
			while ((attr != null && attr.length > 0) || dUb != null) {
				rzb += jUb(Ulb);
				Ulb = Ulb.parentNode;
				attr = Ulb.getAttribute("rwstate");
				dUb = Ulb.getAttribute(daa);
			}
			if (Thb == -1) {
				rzb = -1;
			}
			return new THDomRefPt(SSDOM.getPositionInDom(Ulb), rzb + Thb);
		} else {
			return null;
		}
	} catch (ignore) {
		return null;
	}
}
function jUb(Bub) {
	if (Bub == null) {
		return 0;
	}
	var rzb = 0;
	var hUb = Bub.previousSibling;
	if (hUb != null) {
		rzb = lUb(hUb);
	}
	return rzb;
}
function lUb(Bub) {
	var rzb = 0;
	var hUb = Bub;
	var RAc;
	while (hUb != null) {
		if (hUb.nodeType == 3) {
			RAc = hUb.nodeValue;
			rzb += RAc.length;
		} else if (hUb.nodeType == 1) {
			if (!SSDOM.isInvalidNode(hUb)) {
				if (SSDOM.isSpecialCase(hUb)) {
					rzb += 1;
				} else if (hUb.tagName.toLowerCase() != "textarea") {
					rzb += lUb(hUb.lastChild);
				} else {
					rzb += 1;
				}
			}
		}
		hUb = hUb.previousSibling;
	}
	return rzb;
}
function oUb(Bub) {
	if (Nda || Mda) {
		var ipb = SSDOM.getBody(Bub);
		if (ipb.getAttribute(("id")) == "tinymce") {
			return true;
		}
		var wvb = Bub;
		if (wvb.nodeType == 3) {
			wvb = wvb.parentNode;
		}
		if (wvb.nodeType != 1) {
			return true;
		}
		while (wvb != null && wvb != ipb && wvb.getAttribute) {
			if (wvb.getAttribute(caa) != null) {
				return true;
			}
			wvb = wvb.parentNode;
		}
	}
	return false;
}
function sUb(Bub) {
	var bub = "";
	try {
		if (Bub.nodeType == 1) {
			if (Bub.getAttribute("ignore") == null) {
				var tagName = Bub.tagName.toLowerCase();
				if (tagName == "input") {
					var Fcb = Bub.getAttribute("type");
					if (Fcb != null) {
						Fcb = Fcb.toLowerCase();
						if (Fcb.length == 0 || Fcb == "text") {
							bub = Bub.value;
						} else if (Fcb == "password") {
							bub = "";
						} else if (Fcb == "image") {
							var Kpb = Bub.getAttribute("alt");
							if (Kpb != null && Kpb.length > 0) {
								bub = Kpb;
							} else {
								bub = "";
							}
						} else if (Fcb == "button" || Fcb == "submit" || Fcb == "reset") {
							if (Bub.className == "rwcalbutton" || Bub.className == "rwcalEqbutton") {
								bub = Bub.getAttribute("name");
							} else {
								bub = Bub.getAttribute("value");
							}
						}
					} else {
						bub = Bub.value;
					}
				} else if (tagName == "select") {
					var wUb = Bub.selectedIndex;
					var htb = "";
					var UBc = Bub.options.length;
					for (var bxb = 0; bxb < UBc; bxb++) {
						htb += Bub.options[bxb].text + " ";
					}
					if (UBc > 0) {
						if (wUb > -1) {
							bub = Bub.options[wUb].text + " selected from the list " + htb;
						} else {
							bub = "No selection from list " + htb;
						}
					}
				} else if (tagName == "textarea" || tagName == "option") {
					bub = Bub.value;
				} else if (tagName == "img") {
					var Jpb = Bub.getAttribute("title");
					if (Jpb != null && Jpb.length > 0) {
						bub = Jpb;
					} else {
						var Kpb = Bub.getAttribute("alt");
						if (Kpb != null && Kpb.length > 0) {
							bub = Kpb;
						} else {
							var Lpb = Bub.getAttribute("msg");
							if (Lpb != null && Lpb.length > 0) {
								bub = Lpb;
							}
						}
					}
				} else if (tagName == "math") {
					var DVb = SSDOM.getTextFromMathMl(Bub);
					if (DVb.length > 0) {
						bub = Kpb;
					}
				} else if (Bub.isMathJax) {
					bub = SSDOM.getTextFromMathJax(Bub);
				} else {
					var Kpb = Bub.getAttribute("alt");
					if (Kpb != null && Kpb.length > 0) {
						bub = Kpb;
					} else {
						var Lpb = Bub.getAttribute("msg");
						if (Lpb != null && Lpb.length > 0) {
							bub = Lpb;
						}
					}
				}
			}
		}
	} catch (ignore) {
		bub = "";
	}
	if (bub == null) {
		bub = "";
	}
	return bub;
}
function MVb(NVb) {
	var GVb;
	var Qmb;
	var Cyb;
	var jhb;
	var Vnb;
	var FZb;
	Qmb = 0;
	Cyb = NVb.indexOf(":");
	jhb = NVb.indexOf(";", Cyb);
	while (Qmb > -1 && Cyb > -1) {
		Vnb = NVb.substring(Qmb, Cyb);
		if (jhb > -1) {
			FZb = NVb.substring(Cyb + 1, jhb);
		} else {
			FZb = NVb.substring(Cyb + 1);
		}
		if (FZb.length == 0) {
			GVb = rfa.indexOf(Vnb);
			if (GVb > -1) {
				rfa.splice(GVb, 1);
				delete sfa[Vnb];
			}
		} else {
			GVb = rfa.indexOf(Vnb);
			if (!(GVb > -1)) {
				rfa.push(Vnb);
			}
			sfa[Vnb] = FZb;
		}
		if (jhb > -1) {
			Qmb = jhb + 1;
			Cyb = NVb.indexOf(":", Qmb);
			jhb = NVb.indexOf(";", Cyb);
		} else {
			break;
		}
	}
}
function SVb(exb) {
	var UBc = exb.length;
	var oCc = exb;
	var yrb = XVb(exb);
	if (yrb == null) {
		return exb;
	}
	var i;
	var j;
	var c;
	for (i = UBc - 1; i >= 0; i--) {
		c = yrb.charCodeAt(i);
		if (c != 32) {
			j = i;
			while (c != 32 && i >= 0) {
				--i;
				c = yrb.charCodeAt(i);
			}
			var RVb = yrb.substring(i + 1, j + 1);
			if (sfa[RVb] != null) {
				oCc = oCc.substring(0, i + 1) + ' ' + sfa[RVb] + ' ' + oCc.substr(j + 1);
			}
		}
	}
	return oCc;
}
function XVb(exb) {
	var Xxb = false;
	var UBc = exb.length;
	var hVb = "";
	var i;
	var c;
	for (i = 0; i < UBc; i++) {
		c = exb.charCodeAt(i);
		if (c == 32 || Bya(c)) {
			hVb += ' ';
		} else {
			hVb += exb.charAt(i);
			Xxb = true;
		}
	}
	if (Xxb) {
		return hVb;
	} else {
		return null;
	}
}
function ZVb(exb) {
	return (rfa.indexOf(exb.trimTH()) > -1);
}
function eVb(fVb) {
	var mwb;
	var Cdb = false;
	var dVb = fVb.split(" ");
	var UBc = dVb.length;
	var i;
	for (i = 0; i < UBc; i++) {
		mwb = SVb(dVb[i]);
		if (mwb != dVb[i]) {
			dVb[i] = mwb;
			Cdb = true;
		}
	}
	if (Cdb) {
		var hVb = "";
		for (i = 0; i < UBc - 1; i++) {
			hVb += dVb[i];
			hVb += " ";
		}
		hVb += dVb[UBc - 1];
		return hVb;
	}
	return fVb;
}
function lVb(Pyb) {
	var Cdb = false;
	if (qfa && Pyb != null) {
		var UBc = Pyb.length;
		var i;
		for (i = 0; i < UBc; i++) {
			var mwb = SVb(Pyb[i]);
			if (mwb != Pyb[i]) {
				Pyb[i] = mwb;
				Cdb = true;
			}
		}
	}
	return Cdb;
}
MVb(tfa);
function pVb(ZAc, rVb, sVb) {
	if (typeof(ZAc) == "string") {
		var n = ZAc.indexOf(rVb);
		while (n > -1) {
			ZAc = ZAc.replace(rVb, sVb);
			n = ZAc.indexOf(rVb, n + 1);
		}
	}
	return ZAc;
}
function uVb(exb) {
	var tVb = exb.replace(/\x26/g, '&amp;');
	tVb = tVb.replace(/\x3c/g, '&lt;');
	tVb = tVb.replace(/\x3e/g, '&gt;');
	return tVb;
}
function xVb(Brb) {
	var UBc = Brb.length;
	if (UBc > 100) {
		return PPb(Brb);
	} else {
		var i;
		var c;
		var s;
		for (i = 0; i < UBc; i++) {
			c = Brb.charCodeAt(i);
			if ((c > 47 && c < 58) || (c > 63 && c < 92) || (c > 94 && c < 124)) {
				continue;
			}
			if (c > 126 || c == 92 || c == 47 || c == 58 || c == 59 || c == 42 || c == 63 || c == 34 || c == 60 || c == 62 || c == 124 || c == 35 || c == 37 || c == 43 || c == 38 || c == 94) {
				s = "^" + zya(c);
				Brb = Brb.substr(0, i) + s + Brb.substr(i + 1);
				UBc = Brb.length;
				i += s.length - 1;
			}
		}
	}
	return Brb;
}
function EWb(FWb) {
	var gCc = document.getElementById(FWb);
	if (gCc == null) {
		if (!uda) {
			var i;
			var UBc = top.frames.length;
			for (i = 0; i < UBc; i++) {
				try {
					var Orb = top.frames[i].document.getElementById(FWb);
					if (Orb != null) {
						return Orb;
					}
				} catch (e) {}

			}
		}
		return null;
	} else {
		return gCc;
	}
}
var CWb = null;
function GWb(HWb) {
	var LCc = "th_tmp$";
	var bxb = 1;
	var Wjb;
	var zfb = true;
	while (zfb) {
		Wjb = LCc + bxb;
		if (HWb.document.getElementById(Wjb)) {
			++bxb;
		} else {
			zfb = false;
		}
	}
	return Wjb;
}
function rw_getTargetNodeAsCaretIE(evt, RWb) {
	try {
		if ((Ofa || Pfa || Qfa) && RWb && !WIb(evt)) {
			var LWb = evt.srcElement;
			var MWb = SSDOM.getWindow(LWb);
			var NWb = LWb.childNodes.length;
			var i;
			var OWb = false;
			for (i = 0; i < NWb; i++) {
				if (LWb.childNodes[i].nodeType == 3) {
					OWb = true;
					break;
				}
			}
			if (OWb) {
				if (CWb == null) {
					CWb = GWb(MWb);
				}
				var PWb = MWb.document.getElementById(CWb);
				if (PWb != null) {
					PWb.parentNode.removeChild(PWb);
				}
				var range = MWb.document.selection.createRange();
				range.collapse();
				range.pasteHTML("<span id='" + CWb + "'></span>");
				var gCc = MWb.document.getElementById(CWb);
				var TWb = null;
				if (gCc != null) {
					if (gCc.previousSibling != null && gCc.previousSibling.nodeType == 3) {
						SSDOM.mergeTextNodes(gCc.previousSibling);
						TWb = new kqa(gCc.previousSibling, gCc.previousSibling.length, true);
					}
					if (gCc.nextSibling != null && gCc.nextSibling.nodeType == 3) {
						SSDOM.mergeTextNodes(gCc.nextSibling);
						if (TWb == null) {
							TWb = new kqa(gCc.nextSibling, 0, true);
						}
					}
					if (TWb == null && gCc.nextSibling != null && gCc.nextSibling.nodeType == 1) {
						TWb = new kqa(gCc.nextSibling, 0, true);
					} else if (TWb == null && gCc.previousSibling != null && gCc.previousSibling.nodeType == 1) {
						TWb = new kqa(gCc.previousSibling, 0, true);
					}
				}
				if (TWb == null) {
					TWb = new kqa(gCc.parentNode, 0, true);
				}
				if (gCc != null) {
					gCc.parentNode.removeChild(gCc);
				}
				return TWb;
			} else {
				return new kqa(LWb, 0, true);
			}
		}
		if (Vfa) {
			VMb = evt.target;
			if (VMb != null) {
				if (VMb.firstChild != null && VMb.firstChild.nodeType == 3 && VMb.tagName.toLowerCase() != "textarea") {
					var UWb = VMb.firstChild.nodeValue;
					if (UWb.trimTH().length > 0) {
						VMb = VMb.firstChild;
					}
				}
				return new kqa(VMb, 0, true);
			}
		} else {
			var VWb = aEb(evt);
			var WWb = SSDOM.getRangeObject(evt.srcElement.ownerDocument.body);
			try {
				WWb.moveToPoint(VWb.x, VWb.y);
			} catch (skip) {
				return null;
			}
			var dWb = SSDOM.getRangeObject(evt.srcElement.ownerDocument.body);
			var YWb = SSDOM.getRangeObject(evt.srcElement.ownerDocument.body);
			var ZWb = evt.srcElement.firstChild;
			while ((ZWb != null)) {
				if (ZWb.nodeType == 3 && ZWb.nodeValue.trimTH().length > 0) {
					SSDOM.mergeTextNodes(ZWb);
					var fkb = ZWb.previousSibling;
					while (fkb != null && fkb.nodeType != 1) {
						fkb = fkb.previousSibling;
					}
					if (fkb != null) {
						dWb.moveToElementText(fkb);
						dWb.collapse(false);
					} else {
						dWb.moveToElementText(ZWb.parentNode);
					}
					var nextNode = ZWb.nextSibling;
					while (nextNode != null && nextNode.nodeType != 1) {
						nextNode = nextNode.nextSibling;
					}
					if (nextNode != null) {
						YWb.moveToElementText(nextNode);
						dWb.setEndPoint("EndToStart", YWb);
					} else {
						YWb.moveToElementText(ZWb.parentNode);
						dWb.setEndPoint("EndToEnd", YWb);
					}
					if (dWb.inRange(WWb)) {
						var rzb = lWb(ZWb, dWb, WWb);
						return new kqa(ZWb, rzb, true);
					}
				}
				ZWb = ZWb.nextSibling;
			}
		}
	} catch (exc) {
		eya("rw_getTargetNodeAsCaretIE error:" + exc.message);
	}
	return null;
}
function rw_getTextRangeAsRefPtIE(OXb, EXb) {
	try {
		var dWb = SSDOM.getRangeObject(OXb);
		var parentNode = EXb.parentElement();
		dWb.moveToElementText(parentNode);
		var rzb = lWb(parentNode, dWb, EXb);
		var Khb = ZUb(parentNode, rzb);
		return Khb;
	} catch (exc) {
		eya("rw_getTextRangeAsRefPtIE error:" + exc.message);
	}
	return null;
}
function lWb(mWb, zeb, oWb) {
	try {
		var rzb = 0;
		var range = zeb.duplicate();
		range.collapse();
		range.move("character", 1);
		range.move("character", -1);
		var pWb = 0;
		var qWb = 0;
		while (range.compareEndPoints("EndToEnd", oWb) == -1) {
			range.moveEnd("character", 1);
			pWb = range.text.length;
			if (pWb > qWb) {
				++rzb;
				qWb = pWb;
			}
		}
		return rzb;
	} catch (err) {
		return 0;
	}
}
function CXb(OXb, EXb) {
	var ueb = EXb.duplicate();
	ueb.collapse(true);
	var sWb = rw_getTextRangeAsRefPtIE(OXb, ueb);
	ueb = EXb.duplicate();
	ueb.collapse(false);
	var tWb = rw_getTextRangeAsRefPtIE(OXb, ueb);
	return new THRange(OXb, sWb, tWb);
}
function rw_getAsTextRange(OXb, TXb, VXb, UXb, WXb) {
	var range = SSDOM.getRangeObject(OXb);
	var YCc = SSDOM.getCaretPairFromDomPosition(OXb, TXb, -1, UXb, -1);
	var ygb = YCc.ygb;
	var zgb = YCc.zgb;
	if (ygb != null && ygb.node != null && zgb != null && zgb.node != null) {
		var xWb = ygb.node;
		if (xWb.nodeType == 3) {
			var rzb = jUb(xWb);
			xWb = xWb.parentNode;
			VXb += rzb;
		}
		var zWb = zgb.node;
		if (zWb.nodeType == 3) {
			var rzb = jUb(zWb);
			zWb = zWb.parentNode;
			WXb += rzb;
		}
		range.moveToElementText(xWb);
		range.collapse();
		aXb(range, VXb);
		range.collapse(false);
		range.select();
		var BXb = SSDOM.getRangeObject(OXb);
		BXb.moveToElementText(zWb);
		BXb.collapse();
		aXb(BXb, WXb);
		BXb.collapse(false);
		range.setEndPoint("EndToEnd", BXb);
	} else {
		range = null;
		eya("Error with rw_getAsTextRange.");
	}
	return range;
}
function aXb(zeb, hlb) {
	var UBc;
	var kAc;
	var ZXb;
	UBc = zeb.text.length;
	while (hlb > 0) {
		ZXb = zeb.moveEnd("character", hlb);
		if (ZXb == 0) {
			return;
		}
		kAc = zeb.text.length;
		hlb -= (kAc - UBc);
		UBc = kAc;
	}
}
SpeechStream.ipadUtils = new function () {
	this.tinyMceBlurFix = function () {
		if (Dga && cfa) {
			try {
				var nhb = qAb(true);
				if (nhb) {
					if (nhb.ipadTinyMceFocus) {
						nhb.ipadTinyMceFocus = false;
						var eXb = window.pageXOffset;
						var fXb = window.pageYOffset;
						var mBc = document.getElementById(SpeechStream.tinymceipadfix);
						mBc.focus();
						window.scrollTo(eXb, fXb);
						mBc.blur();
					}
				}
			} catch (err) {}

		}
	};
};
var hXb = new SpeechStream.Dictionary();
function kXb(ZAc) {
	var oCc;
	var UBc;
	var i;
	var c;
	oCc = "";
	UBc = ZAc.length;
	for (i = 0; i < UBc; i++) {
		c = ZAc.charCodeAt(i);
		if ((c < 40 && c != 33) || c == 43 || c == 47 || c == 60 || c == 62 || c == 92 || c == 96 || c > 126) {
			if (c == 32 && SpeechStream.pronunciation.mode == SpeechStream.pronunciation.SERVER_PRONUNCIATION) {
				oCc += ZAc.charAt(i);
			} else {
				var xlb = zya(c);
				switch (xlb.length) {
				case 1:
					xlb = "0" + xlb;
				case 2:
					oCc += "/x" + xlb;
					break;
				case 3:
					xlb = "0" + xlb;
				case 4:
					oCc += "/u" + xlb;
					break;
				default:
				}
			}
		} else {
			oCc += ZAc.charAt(i);
		}
	}
	return oCc;
}
function pXb(ZAc) {
	var mwb = decodeURIComponent(ZAc);
	var oCc = "";
	var UBc = mwb.length;
	var i;
	var mBc;
	var tXb;
	for (i = 0; i < UBc; i++) {
		var c = mwb.charAt(i);
		if (c == '/') {
			if (i < UBc - 1) {
				var uXb = mwb.charAt(i + 1);
				switch (uXb) {
				case '/':
					oCc += '/';
					i++;
					break;
				case 'x':
					if (i < UBc - 3) {
						mBc = mwb.substr(i + 2, 2);
						tXb = Aza(mBc);
						oCc += String.fromCharCode(tXb);
						i += 3;
					}
					break;
				case 'u':
					if (i < UBc - 5) {
						mBc = mwb.substr(i + 2, 4);
						tXb = Aza(mBc);
						oCc += String.fromCharCode(tXb);
						i += 5;
					}
					break;
				default:
				}
			} else {}

		} else if (c == '+') {
			oCc += ' ';
		} else if (c == 10 || c == 13) {}
		else if (c == '%') {
			if (i < UBc - 2) {
				mBc = mwb.substr(i, 3);
				if (mBc == "%2f") {
					oCc += '/';
					i += 2;
				} else if (mBc == "%2a") {
					oCc += ':';
					i -= 2;
				} else if (mBc == "%60") {
					oCc += '`';
					i -= 2;
				} else {
					if (i < UBc - 9) {
						mBc = mwb.substr(i, 10);
						if (mBc == "%26quot%3b") {
							oCc += '"';
							i += 9;
						} else {
							if (i < UBc - 6) {
								mBc = mwb.substr(i, 7);
								if (mBc == "%26quot") {
									oCc += '"';
									i += 6;
								} else {
									oCc += c;
								}
							} else {
								oCc += c;
							}
						}
					} else {
						oCc += c;
					}
				}
			} else {
				oCc += c;
			}
		} else if (c == '&') {
			if (i < UBc - 5) {
				mBc = mwb.substr(i, 6);
				if (mBc == "&#x27") {
					oCc += '\'';
				} else if (mBc == "&quot;") {
					oCc += '"';
				}
				oCc += String.fromCharCode(tXb);
				i += 5;
			}
		} else {
			oCc += mwb.charAt(i);
		}
	}
	return oCc;
}
function wXb(ZAc) {
	var oCc = "";
	var UBc = ZAc.length;
	var i;
	for (i = 0; i < UBc; i++) {
		var c = ZAc.charCodeAt(i);
		if ((c < 40 && c != 33) || c == 43 || c == 47 || c == 60 || c == 62 || c == 92 || c == 96 || c > 126) {
			oCc += "&#" + c + ";";
		} else {
			oCc += ZAc.charAt(i);
		}
	}
	return oCc;
}
function AYb(ZAc) {
	var oCc = "";
	var UBc = ZAc.length;
	var i;
	for (i = 0; i < UBc; i++) {
		var c = ZAc.charCodeAt(i);
		if ((c < 40 && c != 33) || c == 43 || c == 47 || c == 60 || c == 62 || c == 92 || c == 96 || c > 126) {
			if (c == 34 || c == 39) {
				oCc += "\\&#" + c + ";";
			} else {
				oCc += "&#" + c + ";";
			}
		} else {
			oCc += ZAc.charAt(i);
		}
	}
	return oCc;
}
var DYb = "\\x82\\x91\\x92";
var EYb = "\\x93\\x94";
var FYb = /^[,.?!;:\x27\x22�$�]+|[,.?!;:\x27\x22�$�]+$/g;
function KYb(Pyb) {
	if (!pfa) {
		return false;
	}
	var Cdb = false;
	var mwb;
	var oCc;
	var UBc = Pyb.length;
	var i;
	for (i = 0; i < UBc; i++) {
		oCc = Pyb[i].trimTH();
		if (oCc.indexOf(" ") > -1) {
			var Wjb = WYb(oCc);
			if (Wjb != oCc) {
				Pyb[i] = Wjb;
				Cdb = true;
			}
			continue;
		}
		var value;
		if (hXb.MultiwordStart$__.indexOf(oCc) > -1) {
			if (i < UBc - 1) {
				mwb = oCc + " " + Pyb[i + 1].trimTH();
				if ((value = hXb.get$__(mwb)) != null) {
					var RYb = value.split(" ");
					var TAc = RYb[0] + " ";
					var VAc = RYb[1] + " ";
					Pyb[i] = TAc;
					Pyb[i + 1] = VAc;
					Cdb = true;
					continue;
				}
			}
			if (i < UBc - 2) {
				mwb = oCc + " " + Pyb[i + 1].trimTH() + " " + Pyb[i + 2].trimTH();
				if ((value = hXb.get$__(mwb)) != null) {
					var RYb = value.split(" ");
					var TAc = RYb[0] + " ";
					var VAc = RYb[1] + " ";
					var UAc = RYb[2] + " ";
					Pyb[i] = TAc;
					Pyb[i + 1] = VAc;
					Pyb[i + 2] = UAc;
					Cdb = true;
					continue;
				}
			}
		}
		if ((value = hXb.get$__(oCc)) != null || (value = hXb.get$__(oCc.toLowerCase())) != null) {
			Pyb[i] = value + " ";
			Cdb = true;
			continue;
		} else {
			mwb = oCc.replace(DYb, '\'');
			mwb = mwb.replace(EYb, '"');
			if (oCc != mwb) {
				if ((value = hXb.get$__(mwb)) != null || (value = hXb.get$__(mwb.toLowerCase())) != null) {
					Pyb[i] = value + " ";
					Cdb = true;
					continue;
				} else {
					oCc = mwb;
					mwb = oCc.replace(FYb, '');
					if (oCc != mwb) {
						if ((value = hXb.get$__(mwb)) != null || (value = hXb.get$__(mwb.toLowerCase())) != null) {
							Pyb[i] = value + " ";
							Cdb = true;
							continue;
						}
					}
				}
			} else {
				mwb = oCc.replace(FYb, '');
				if (oCc != mwb) {
					if ((value = hXb.get$__(mwb)) != null || (value = hXb.get$__(mwb.toLowerCase())) != null) {
						Pyb[i] = value + " ";
						Cdb = true;
						continue;
					}
				}
			}
		}
	}
	return Cdb;
}
function WYb(exb) {
	if (!pfa) {
		return exb;
	}
	var oCc = "";
	var wordList = exb.split(" ");
	if (KYb(wordList)) {
		var UBc = wordList.length;
		var i;
		for (i = 0; i < UBc; i++) {
			oCc += wordList[i];
			if (i < UBc - 1) {
				oCc += " ";
			}
		}
		return oCc;
	} else {
		return exb;
	}
}
function eYb(exb) {
	var n1;
	var n2;
	var aYb;
	var anb;
	var bnb;
	var dYb = exb.split("\r\n");
	var i;
	for (i = 0; i < dYb.length; i++) {
		aYb = dYb[i];
		var gYb = aYb.indexOf("&p_pageID=*&") > -1;
		n1 = aYb.indexOf("&sayThis=");
		n2 = aYb.indexOf("&likeThis=");
		anb = aYb.substring(n1 + 9, n2);
		bnb = aYb.substring(n2 + 10);
		anb = pXb(anb);
		bnb = pXb(bnb);
		hXb.add$__(anb, bnb, gYb);
	}
	if (typeof(Snb) == "function") {
		Snb();
	}
}
function jYb() {}

function kYb() {
	if (yca != null && yca.length > 0 && zca != null && zca.length > 0 && Ada != null && Ada.length > 0) {
		var Hsb;
		if (SpeechStream.cacheMode.getLiveServer() == null) {
			return;
		}
		Hsb = Dza(true) + SpeechStream.cacheMode.getLiveServer();
		var Pib = "&custID=" + yca + "&bookID=" + zca + "&pageID=" + Ada + "&combined=Y";
		var flash = xbb.getConnector();
		if (flash == null) {
			Bza("Connection to the server is not available.");
		} else {
			flash.getPronunciationDataAll(Hsb, Pib);
		}
	}
}
var mYb = 2;
function $rw_loadPronCallback(Xhb) {
	if (Xhb == null) {
		if (mYb > 0) {
			--mYb;
			kYb();
		} else {
			Bza("Failed to load pronunciation data, this may affect the text to speech function.");
		}
	} else {
		Xhb = Xhb.trimTH();
		if (Xhb == "") {
			if (SpeechStream.pronunciation.mode != SpeechStream.pronunciation.SERVER_PRONUNCIATION) {
				ofa = true;
			}
			if (typeof(Snb) == "function") {
				Snb();
			}
		} else if (Xhb == "-1") {
			if (mYb > 0) {
				--mYb;
				kYb();
			} else {
				Bza("Failed to load pronunciation data, this may affect the text to speech function.");
			}
		} else {
			if (SpeechStream.pronunciation.mode != SpeechStream.pronunciation.SERVER_PRONUNCIATION) {
				ofa = true;
			}
			eYb(Xhb);
		}
	}
}
function pYb(KZb, BZb, LZb) {
	if (yca != null && yca.length > 0 && zca != null && zca.length > 0 && Ada != null && Ada.length > 0) {
		var Hsb;
		if (SpeechStream.cacheMode.getLiveServer() == null) {
			return;
		}
		Hsb = Dza(true) + SpeechStream.cacheMode.getLiveServer();
		var Pib = "&custID=" + yca + "&bookID=" + zca + "&pageID=" + (LZb ? "*" : Ada) + "&sayThis=" + kXb(KZb) + "&likeThis=" + kXb(BZb);
		var flash = xbb.getConnector();
		if (flash == null) {
			alert("Connection to the server is not available.");
		} else {
			flash.addPronunciationData(Hsb, Pib);
		}
	}
}
function $rw_addPronCallback(Xhb) {
	if (Xhb == null) {
		document.getElementById("confirmPageMsg").innerHTML = "Failed to insert.";
	} else if (Xhb == "-1") {
		document.getElementById("confirmPageMsg").innerHTML = "Failed to insert.";
	} else {
		var Vnb = document.getElementById('createSayThis').value.trimTH();
		var FZb = document.getElementById('createLikeThis').value.trimTH();
		var Znb = document.getElementById('createAllPages').checked;
		hXb.add$__(Vnb, FZb, Znb);
		document.getElementById('createSayThis').value = '';
		document.getElementById('createLikeThis').value = '';
		document.getElementById("confirmPageMsg").innerHTML = "Pronunciation inserted.";
		Snb();
	}
}
function zYb(KZb, BZb, LZb) {
	if (yca != null && yca.length > 0 && zca != null && zca.length > 0 && Ada != null && Ada.length > 0) {
		var Hsb;
		if (SpeechStream.cacheMode.getLiveServer() == null) {
			return;
		}
		Hsb = Dza(true) + SpeechStream.cacheMode.getLiveServer();
		var Pib = "&custID=" + yca + "&bookID=" + zca + "&pageID=" + (LZb ? "*" : Ada) + "&sayThis=" + kXb(KZb) + "&likeThis=" + kXb(BZb);
		var flash = xbb.getConnector();
		if (flash == null) {
			alert("Connection to the server is not available.");
		} else {
			flash.updatePronunciationData(Hsb, Pib);
		}
	}
}
function $rw_updatePronCallback(Xhb) {
	if (Xhb == null) {
		document.getElementById("editPageMsg").innerHTML = "Failed to updated.";
	} else if (Xhb == "-1") {
		document.getElementById("editPageMsg").innerHTML = "Failed to updated.";
	} else {
		var Vnb = document.getElementById('editSayThis').value.trimTH();
		var FZb = document.getElementById('editLikeThis').value.trimTH();
		var Znb = document.getElementById('editAllPages').checked;
		hXb.add$__(Vnb, FZb, Znb);
		document.getElementById('editSayThis').value = '';
		document.getElementById('editLikeThis').value = '';
		document.getElementById("editPageMsg").innerHTML = "Pronunciation updated.";
		enb();
		Snb();
	}
}
function JZb(KZb, LZb) {
	if (yca != null && yca.length > 0 && zca != null && zca.length > 0 && Ada != null && Ada.length > 0) {
		var Hsb;
		if (SpeechStream.cacheMode.getLiveServer() == null) {
			return;
		}
		Hsb = Dza(true) + SpeechStream.cacheMode.getLiveServer();
		var Pib = "&custID=" + yca + "&bookID=" + zca + "&pageID=" + (LZb ? "*" : Ada) + "&sayThis=" + kXb(KZb);
		var flash = xbb.getConnector();
		if (flash == null) {
			alert("Connection to the server is not available.");
		} else {
			flash.removePronunciationData(Hsb, Pib, KZb);
		}
	}
}
function $rw_removePronCallback(Xhb, ZRb) {
	if (Xhb == null) {
		document.getElementById("editPageMsg").innerHTML = "Failed to delete item.";
	} else if (Xhb == "-1") {
		document.getElementById("editPageMsg").innerHTML = "Failed to delete item.";
	} else {
		hXb.remove$__(ZRb);
		document.getElementById('editSayThis').value = '';
		document.getElementById('editLikeThis').value = '';
		document.getElementById("editPageMsg").innerHTML = "Pronunciation deleted.";
		Snb();
	}
}
var NZb = null;
function PZb() {
	if (NZb != null) {
		return NZb;
	} else {
		if (ufa) {
			var Orb = document.getElementById("rwDrag");
			if (Orb != null) {
				Orb.style.position = "relative";
				ufa = false;
			}
		}
		var flash = null;
		try {
			if (Yfa) {
				flash = window.document.WebToSpeech;
			} else {
				if (window.document.WebToSpeech) {
					flash = window.document.WebToSpeech;
				} else {
					flash = window.WebToSpeech;
				}
			}
			if (flash != null) {
				flash.getVersion();
				NZb = flash;
			}
		} catch (err) {
			flash = null;
			NZb = null;
		}
		if (flash == null) {
			var RZb = document.getElementById("WebToSpeech");
			if (RZb != null) {
				flash = RZb;
				try {
					flash.getVersion();
					NZb = flash;
				} catch (e) {
					flash = null;
					NZb = null;
				}
				if (flash == null && !Lfa && RZb.childNodes && typeof(RZb.childNodes.length) == "number") {
					var UBc = RZb.childNodes.length;
					var i;
					for (i = 0; i < UBc; i++) {
						var wvb = RZb.childNodes[i];
						if (wvb.tagName.toLowerCase() == "embed") {
							flash = wvb;
							try {
								flash.getVersion();
								NZb = flash;
							} catch (e) {
								flash = null;
								NZb = null;
							}
						}
					}
				}
			}
		}
		return flash;
	}
}
var UZb = 0;
SpeechStream.AjaxRequest = function () {
	var VZb = ++UZb;
	var WZb = null;
	var XZb = null;
	var YZb = null;
	var ZZb = false;
	var aZb = null;
	var bZb = null;
	var cZb = 0;
	var dZb = false;
	this.cancel = function () {
		eya("Cancelling request " + VZb);
		dZb = true;
	};
	this.getCount = function () {
		return VZb;
	};
	this.setTimeoutCallBack = function (p_func) {
		bZb = p_func;
	};
	this.setErrorCallBack = function (p_func) {
		aZb = p_func;
	};
	this.setTimeout = function (p_nMs) {
		cZb = p_nMs;
	};
	this.callBack = function () {
		if (dZb) {
			return;
		}
		with (this) {
			if (readyState < 4) {
				return;
			}
			if (status != 200) {
				if (bZb) {
					bZb(VZb);
					return;
				}
				if (aZb) {
					aZb(status);
					return;
				}
			}
			if (ZZb) {
				if (YZb == null) {
					XZb(responseXML);
				} else {
					XZb[YZb](responseXML);
				}
			} else {
				if (YZb == null) {
					XZb(responseText);
				} else {
					XZb[YZb](responseText);
				}
			}
		}
	};
	this.doPost = function (HBb, p_parameters, p_responseObject, p_responseCallback, p_bXml) {
		with (this) {
			XZb = p_responseObject;
			YZb = p_responseCallback;
			ZZb = p_bXml;
			if (Qga) {
				g_ExtBackgroundScriptCommObj.extBGAjaxRequest(p_parameters, HBb, p_responseObject, p_responseCallback, p_bXml);
			} else {
				WZb = new XMLHttpRequest();
				if (afa) {
					WZb.timeout = cZb;
				}
				WZb.open("POST", HBb, true);
				WZb.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				WZb.onreadystatechange = callBack;
				WZb.send(p_parameters);
			}
		}
	};
	this.doGet = function (HBb, p_parameters, p_responseObject, p_responseCallback, p_bXml) {
		with (this) {
			XZb = p_responseObject;
			YZb = p_responseCallback;
			ZZb = p_bXml;
			if (Qga) {
				g_ExtBackgroundScriptCommObj.extBGAjaxRequest(p_parameters, HBb, p_responseObject, p_responseCallback, p_bXml);
			} else {
				WZb = new XMLHttpRequest();
				if (afa) {
					WZb.timeout = cZb;
				}
				WZb.open("GET", HBb, true);
				WZb.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				WZb.onreadystatechange = callBack;
				WZb.send(p_parameters);
			}
		}
	};
};
SpeechStream.HTML5Controller = function () {
	var self = this;
	var eZb = gca;
	var fZb = false;
	var gZb = 0;
	var hZb = false;
	var iZb = 0;
	var jZb = null;
	var kZb;
	var lZb;
	var mZb;
	var nZb;
	var oZb;
	var pZb;
	var qZb = xca;
	var rZb = new Audio();
	rZb.type = "audio/mpeg";
	if (typeof(Ida) == "number" && Ida > -1) {
		rZb.volume = (parseInt(Ida, 10) / 100.0);
	}
	var sZb = null;
	var tZb = null;
	var uZb = null;
	var vZb = 0;
	var wZb = null;
	var sbb = true;
	var yZb = false;
	var zZb = false;
	this.initialise = function (p_flashVars, dCb, eCb, fCb) {
		kZb = dCb;
		lZb = eCb;
		mZb = fCb;
		if (qca != null) {
			nZb = qca;
		} else {
			nZb = lCb(ica, false);
		}
		if (pca != null) {
			oZb = pca;
		} else {
			oZb = lCb(ica, false);
		}
		if (rca != null) {
			pZb = rca;
		} else {
			pZb = lCb(ica, false);
		}
		if (p_flashVars.cacheMode == "true") {
			yZb = true;
		}
		if (p_flashVars.cacheLiveFallover == "true") {
			zZb = true;
		}
		if (p_flashVars.volumeValue > -1) {
			rZb.volume = (p_flashVars.volumeValue / 100.0);
		}
	};
	this.setToClipboard = function (ZAc) {};
	this.setCacheBuster = function (p_bCacheBuster) {
		fZb = p_bCacheBuster;
	};
	this.setCacheMode = function () {};
	this.resetCacheTimer = function () {
		gZb = 0;
		setTimeout(self.resetCacheTimer, Wda * 1000 * 60);
	};
	setTimeout(this.resetCacheTimer, Wda * 1000 * 60);
	this.canPause = function () {
		return rZb && !rZb.ended && !isNaN(rZb.duration) && rZb.duration > 0;
	};
	this.isPaused = function () {
		return hZb;
	};
	this.pause = function () {
		if (rZb != null) {
			rZb.pause();
			hZb = true;
			return true;
		}
		return false;
	};
	this.resume = function () {
		if (rZb != null && hZb) {
			if (Xfa && rZb.currentTime > 0.05 && !rZb.ended) {
				rZb.currentTime -= 0.05;
			}
			rZb.play();
			hZb = false;
			return true;
		}
		return false;
	};
	this.setCustomerId = function () {};
	this.setBookId = function () {};
	this.setPageId = function () {};
	this.setSpeedValue = function () {};
	this.setVoiceName = function (XKb) {
		qZb = XKb;
	};
	this.getVoiceName = function () {
		return qZb;
	};
	this.setVolumeValue = function (p_strVal) {
		if (rZb != null) {
			try {
				rZb.volume = (parseInt(p_strVal, 10) / 100.0);
			} catch (e) {}

		}
	};
	this.checkPath = function (Vsa) {
		if (mda != null && nda != null) {
			var mwb;
			mwb = Vsa;
			if (mwb.indexOf(mda) > -1) {
				mwb = mwb.substr(0, mwb.indexOf(mda)) + nda + mwb.substr(mwb.indexOf(mda) + mda.length);
			}
			return mwb;
		} else {
			return Vsa;
		}
	};
	this.getPronunciationDataAll = function (p_strBase, p_strDetails) {
		var hob = p_strBase + "/SpeechNAServer/pronounce.html?type=get&pronounceClient=" + Cda + p_strDetails;
		var caller = new SpeechStream.AjaxRequest();
		caller.doGet(hob, null, this, "requestPGetAllLoad", false);
	};
	this.requestPGetAllLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_loadPronCallback("-1");
		} else if (exb == "get=false" || exb == "") {
			$rw_loadPronCallback("-1");
		} else if (exb == "empty") {
			$rw_loadPronCallback("");
		} else {
			$rw_loadPronCallback(exb);
		}
	};
	this.addPronunciationData = function (p_strBase, p_strDetails) {
		var hob = p_strBase + "/SpeechNAServer/pronounce.html?type=add&pronounceClient=" + Cda + p_strDetails;
		var caller = new SpeechStream.AjaxRequest();
		caller.doGet(hob, null, this, "requestPAddLoad", false);
	};
	this.requestPAddLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_addPronCallback("-1");
		} else if (exb == "add=false") {
			$rw_addPronCallback("-1");
		} else if (exb == "add=true") {
			$rw_addPronCallback("1");
		} else {
			$rw_addPronCallback("-1");
		}
	};
	this.updatePronunciationData = function (p_strBase, p_strDetails) {
		var hob = p_strBase + "/SpeechNAServer/pronounce.html?type=update&pronounceClient=" + Cda + p_strDetails;
		var caller = new SpeechStream.AjaxRequest();
		caller.doGet(hob, null, this, "requestPUpdateLoad", false);
	};
	this.requestPUpdateLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_updatePronCallback("-1");
		} else if (exb == "update=false") {
			$rw_updatePronCallback("-1");
		} else if (exb == "update=true") {
			$rw_updatePronCallback("1");
		} else {
			$rw_updatePronCallback("-1");
		}
	};
	this.removePronunciationData = function (p_strBase, p_strDetails, ZRb) {
		var hob = p_strBase + "/SpeechNAServer/pronounce.html?type=delete&pronounceClient=" + Cda + p_strDetails;
		var caller = new SpeechStream.AjaxRequest();
		var Jab = function (exb) {
			if (exb == null || exb.length == 0) {
				$rw_removePronCallback("-1");
			} else if (exb == "delete=false") {
				$rw_removePronCallback("-1");
			} else if (exb == "delete=true") {
				$rw_removePronCallback("1", ZRb);
			} else {
				$rw_removePronCallback("-1");
			}
		};
		caller.doGet(hob, null, Jab, null, false);
	};
	var Kab = "";
	this.getLastError = function () {
		return Kab;
	};
	this.getVersion = function () {
		return eZb;
	};
	this.getRevisionNumber = function () {
		return "0";
	};
	function Mab() {
		var oab = (++iZb);
		if (jZb != null) {
			Rab(0);
			delete jZb;
			jZb = null;
		}
		hZb = false;
		if (sbb) {
			rZb.pause();
			rZb.autoplay = true;
			rZb.play();
			sbb = false;
		}
		return oab;
	}
	this.checkRequestStillValid = function (Nab) {
		return Nab == iZb;
	};
	this.setAudio = function (p_strMp3Url) {
		rZb.src = p_strMp3Url;
	};
	this.setTimer = function (p_timerArray) {
		sZb = p_timerArray;
	};
	this.setMathMlTimer = function (p_strMathId, Oab) {
		tZb = p_strMathId;
		uZb = Oab;
	};
	this.requestCompleteStartPlayback = function () {
		$rw_renderingSpeechCallback();
		audioPlaybackTimer();
		rZb.autoplay = true;
		rZb.play();
	};
	this.stopSpeech = function () {
		if (rZb != null) {
			rZb.pause();
			rZb.currentTime = 0;
			this.onSpeechStop(-1);
		}
		jZb = null;
		++iZb;
	};
	this.stopSpeechAlt = function () {
		if (rZb != null) {
			rZb.pause();
			try {
				rZb.currentTime = 0;
			} catch (err) {}

			this.onSpeechStop(-2);
		}
		jZb = null;
		++iZb;
	};
	this.onSpeechStop = function (Sab) {
		Rab(Sab);
	};
	function Rab(Sab) {
		if (Sab < 0) {
			$rw_doSelection(Sab);
		}
		sZb = null;
		uZb = null;
		vZb = 0;
		wZb = null;
	}
	this.startSpeech = function (exb, Yab) {
		var oab = Mab();
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, exb, Yab);
		if (gZb <= Vda || mZb == null) {
			jZb.makeSpeechRequest(lZb, false);
		} else {
			jZb.makeSpeechRequest(mZb, true);
		}
	};
	this.startSpeechFromBackup = function (exb, Yab) {
		var oab = Mab();
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, exb, Yab);
		jZb.makeSpeechRequest(mZb, true);
	};
	this.startSpeechBackup = function (p_prevRequest) {
		var Zab = p_prevRequest.m_mathJaxId;
		var Jgb = p_prevRequest.m_params;
		if (mZb == null) {
			this.onSpeechStop(-3);
			return false;
		} else {
			var oab = Mab();
			jZb = new SpeechStream.Html5Speech();
			jZb.copyParameters(this, oab, Jgb);
			jZb.setMathJaxId(Zab);
			jZb.makeSpeechRequest(mZb, true);
			return true;
		}
	};
	this.startSpeechFromCacheWithGen = function (p_strFilePath, fVb, p_destFolder, p_destFilename, p_bPron) {
		var oab = Mab();
		if (gZb <= Vda || !zZb || mZb == null) {
			var kab;
			var lab;
			if (fZb) {
				kab = lZb + "SpeechCache/" + p_strFilePath + ".xml" + "?cachebuster=" + new Date().getTime() + Math.random();
				lab = lZb + "SpeechCache/" + p_strFilePath + ".mp3" + "?cachebuster=" + new Date().getTime() + Math.random();
			} else {
				kab = lZb + "SpeechCache/" + p_strFilePath + ".xml";
				lab = lZb + "SpeechCache/" + p_strFilePath + ".mp3";
			}
			jZb = new SpeechStream.Html5Speech();
			jZb.setParameters(this, oab, fVb, p_bPron);
			jZb.setStaticParameters(p_destFolder, p_destFilename);
			jZb.loadFiles(kab, lab);
		} else {
			jZb = new SpeechStream.Html5Speech();
			jZb.setParameters(this, oab, fVb, p_bPron);
			jZb.setStaticParameters(p_destFolder, p_destFilename);
			jZb.makeSpeechRequest(mZb, true);
		}
	};
	this.startSpeechGenerateCache = function (p_strFilePath, fVb, p_destFolder, p_destFilename, p_bPron, p_strServerName) {
		var oab = Mab();
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, fVb, p_bPron);
		jZb.setStaticParameters(p_destFolder, p_destFilename);
		jZb.makeSpeechRequest(p_strServerName, true);
	};
	this.startSpeechFromCache = function (p_strFilePath, fVb, p_bPron) {
		var oab = Mab();
		if (gZb <= Vda || !zZb || mZb == null) {
			var kab;
			var lab;
			if (fZb) {
				kab = lZb + "SpeechCache/" + p_strFilePath + ".xml" + "?cachebuster=" + new Date().getTime() + Math.random();
				lab = lZb + "SpeechCache/" + p_strFilePath + ".mp3" + "?cachebuster=" + new Date().getTime() + Math.random();
			} else {
				kab = lZb + "SpeechCache/" + p_strFilePath + ".xml";
				lab = lZb + "SpeechCache/" + p_strFilePath + ".mp3";
			}
			jZb = new SpeechStream.Html5Speech();
			jZb.setParameters(this, oab, fVb, p_bPron);
			jZb.loadFiles(kab, lab);
		} else {
			jZb = new SpeechStream.Html5Speech();
			jZb.setParameters(this, oab, fVb, p_bPron);
			jZb.makeSpeechRequest(mZb, true);
		}
	};
	this.speakFromFile = function (kxb) {
		var oab = Mab();
		var kab = kxb + ".xml";
		var lab = kxb + ".mp3";
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, "n/a", false);
		jZb.loadFiles(kab, lab);
	};
	this.startSpeechFromFile = function (exb, HBb, p_bPron) {};
	this.startHighlighting = function (exb) {};
	this.simpleSpeech = function (exb, Yab) {
		var oab = Mab();
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, exb, Yab);
		jZb.setHighlightable(false);
		if (gZb <= Vda || mZb == null) {
			jZb.makeSpeechRequest(lZb, false);
		} else {
			jZb.makeSpeechRequest(mZb, true);
		}
	};
	this.simpleSpeechFromBackup = function (exb, Yab) {
		var oab = Mab();
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, exb, Yab);
		jZb.setHighlightable(false);
		jZb.makeSpeechRequest(mZb, true);
	};
	this.autogenSpeechFiles = function (fVb, p_destFolder, p_destFilename, p_bPron, p_strServerName) {
		var oab = Mab();
		this.onSpeechStop(-2);
		jZb = new SpeechStream.Html5Speech();
		jZb.setParameters(this, oab, fVb, p_bPron);
		jZb.setStaticParameters(p_destFolder, p_destFilename);
		jZb.makeSpeechRequest(p_strServerName, true);
	};
	this.checkAutogenCachedFiles = function (p_strFilePath) {};
	this.autoGenComplete = function (p_strServerName) {};
	this.getMP3File = function (LPb, p_bPron) {};
	this.getPictureDictionaryPage = function (exb, p_strLocale) {
		var pab = "&userName=" + Cda + "&swf=" + eZb;
		var Nbb = pZb + "ImageServices/imagedict.html";
		var yab = "word=" + exb + pab;
		if (p_strLocale != null) {
			yab += "&locale=" + p_strLocale;
		}
		if (yca != null) {
			yab += "&custID=" + yca;
		}
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(Nbb, yab, this, "imagedictionaryLoad", false);
	};
	this.imagedictionaryLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_picturedictionaryReply("No Image.");
		} else {
			$rw_picturedictionaryReply(exb);
		}
	};
	this.getCustomDictionaryPage = function (fVb, HBb) {
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(HBb, fVb, this, "dictionaryLoad", false);
	};
	this.getDictionaryPage = function (exb) {
		var Nbb = nZb + "rwserver/";
		var yab = "query=dictionaryHtml" + "&text=" + exb + "&locale=" + Gda + "&userName=" + Cda + "&swf=" + eZb;
		if (yca != null) {
			yab += "&custID=" + yca;
		}
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(Nbb, yab, this, "dictionaryLoad", false);
	};
	this.dictionaryLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_dictionaryReply("Error loading content.");
		} else {
			$rw_dictionaryReply(exb);
		}
	};
	this.getPopupDictionaryPage = function (exb) {
		var Nbb = nZb + "rwserver/";
		var yab = "query=dictionaryHtml" + "&text=" + exb + "&locale=" + Gda + "&userName=" + Cda + "&swf=" + eZb;
		if (yca != null) {
			yab += "&custID=" + yca;
		}
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(Nbb, yab, this, "dictionaryPopupLoad", false);
	};
	this.dictionaryPopupLoad = function (exb) {
		if (typeof($rw_popupDictionaryReply) == "function") {
			if (exb == null || exb.length == 0) {
				$rw_popupDictionaryReply("Error loading content.");
			} else {
				$rw_popupDictionaryReply(exb);
			}
		}
	};
	this.getDictionaryPageFl = function (exb) {
		var Nbb = nZb + "rwserver/?query=dictionary&type=result&wordType=15&" + "&text=" + exb + "&locale=" + Gda + "&userName=" + Cda + "&swf=" + eZb + "&dictionaryType=SIMPLE";
		if (yca != null) {
			Nbb += "&custID=" + yca;
		}
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(Nbb, null, this, "dictionaryFlLoad", false);
	};
	this.getDictionaryPageFlHTML = function (exb) {
		var Nbb = nZb + "rwserver/?query=dictionaryHtml" + "&text=" + exb + "&locale=" + Gda + "&userName=" + Cda + "&swf=" + eZb + "&dictionaryType=SIMPLE";
		if (yca != null) {
			Nbb += "&custID=" + yca;
		}
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(Nbb, null, this, "dictionaryFlLoad", false);
	};
	this.dictionaryFlLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_dictionaryFlReply("Error loading content.");
		} else {
			$rw_dictionaryFlReply(exb);
		}
	};
	this.getTranslationPage = function (exb) {
		this.getTranslationGenericPage(exb, "English", "Spanish");
	};
	this.getTranslationGenericPage = function (exb, Qbb, Rbb) {
		Fbb(exb, Qbb, Rbb, false, this);
	};
	this.getTranslationFlGenericPage = function (exb, Qbb, Rbb) {
		Fbb(exb, Qbb, Rbb, true, this);
	};
	this.getTranslationParagraph = function (exb, Qbb, Rbb) {
		Obb(exb, Qbb, Rbb, this);
	};
	function Fbb(exb, Qbb, Rbb, Jbb, Sbb) {
		var Nbb = oZb + "rwtranslateserver/onlinetranslator";
		var Jgb = "type=ultrahtml5&mode=content" + "&value=" + encodeURIComponent(exb) + "&value2=" + exb + "&caller=" + location.protocol + "//" + location.host + location.pathname + "&key=" + PPb(exb + Cda) + "&username=" + Cda + "&source=" + Qbb + "&dest=" + Rbb;
		if (yca != null) {
			Jgb += "&custID=" + yca;
		}
		var caller = new SpeechStream.AjaxRequest();
		if (Jbb) {
			caller.doPost(Nbb, Jgb, Sbb, "translationFlLoad", false);
		} else {
			caller.doPost(Nbb, Jgb, Sbb, "translationLoad", false);
		}
	}
	this.paratranCaller = null;
	function Obb(exb, Qbb, Rbb, Sbb) {
		var Nbb = oZb + "rwtranslateserver/onlinetranslator";
		var Jgb = "type=paragraphtranslate&mode=content" + "&value=" + exb + "&caller=" + location.protocol + "//" + location.host + location.pathname + "&username=" + Cda + "&source=" + Qbb + "&dest=" + Rbb;
		if (yca != null) {
			Jgb += "&custID=" + yca;
		}
		if (self.paratranCaller != null) {
			self.paratranCaller.cancel();
		}
		var caller = new SpeechStream.AjaxRequest();
		caller.doPost(Nbb, Jgb, Sbb, "translationLoad", false);
		self.paratranCaller = caller;
	}
	this.translationLoad = function (exb) {
		self.paratranCaller = null;
		if (exb == null || exb.length == 0) {
			$rw_transReply("Error loading content.");
		} else {
			exb = "<style type=\"text/css\">div.rwTranWordHeader{font-weight: bold;padding: 5px;" + "border-bottom:1px solid #666666;margin-bottom: 10px;}" + "span.rwMeaningNum{pading-left: 10px;padding-right: 10px;font-weight:bold;}" + "span.rwMeaning{padding-right:1-px;}</style>" + exb;
			$rw_transReply(exb);
		}
	};
	this.translationFlLoad = function (exb) {
		if (exb == null || exb.length == 0) {
			$rw_transFlReply("Error loading content.");
		} else {
			exb = "<style type=\"text/css\">div.rwTranWordHeader{font-weight: bold;padding: 5px;" + "border-bottom:1px solid #666666;margin-bottom: 10px;}" + "span.rwMeaningNum{pading-left: 10px;padding-right: 10px;font-weight:bold;}" + "span.rwMeaning{padding-right:1-px;}</style>" + exb;
			$rw_transFlReply(exb);
		}
	};
	this.getSoundFileLength = function (hob) {};
	audioPlaybackTimer = function () {
		if (sZb == null) {
			return;
		}
		if (rZb.ended) {
			$rw_speechCompleteCallback("Complete");
			Rab(-1);
			try {
				rZb.pause();
				rZb.currentTime = 0;
			} catch (err) {}

			return;
		}
		if (sZb.length > 0) {
			if (sZb[vZb] < rZb.currentTime) {
				if (uZb != null) {
					if (uZb[vZb]) {
						var mBc = uZb[vZb];
						eya("highlight " + mBc.start + "  " + mBc.end);
						SpeechStream.mathJaxHighlighter.highlightWord(tZb, mBc.start, mBc.end);
					}
				} else {
					$rw_doSelection(vZb);
				}
				vZb++;
			}
			if (rZb.currentTime > sZb[sZb.length - 1]) {
				$rw_speechCompleteCallback("Complete");
				Rab(-1);
				try {
					rZb.pause();
					rZb.currentTime = 0;
				} catch (err) {}

				return;
			}
		}
		wZb = setTimeout(audioPlaybackTimer, 10);
	};
	this.strStoredVoice = null;
	this.setAltSettings = function (XKb, cKb, dKb, eKb, fKb) {
		if (this.strStoredVoice == null) {
			this.strStoredVoice = qZb;
		}
		if (typeof(XKb) == "string") {
			qZb = XKb;
		}
	};
	this.restoreSettings = function () {
		if (this.strStoredVoice != null) {
			qZb = this.strStoredVoice;
			this.strStoredVoice = null;
		}
	};
};
SpeechStream.Html5Speech = function () {
	var self = this;
	this.m_nRequestNumber = -1;
	this.m_params = null;
	this.m_bBackup = false;
	this.m_bHighlightable = true;
	this.m_mathJaxId = null;
	this.m_controller = null;
	this.makeSpeechRequest = function (p_strSpeechServer, p_bBackup) {
		var server = p_strSpeechServer + "SpeechServices/index.html";
		self.m_bBackup = p_bBackup;
		var Xbb = new SpeechStream.AjaxRequest();
		Xbb.doPost(server, self.m_params, self, "onSpeechRequestResponse", false);
	};
	this.onSpeechRequestResponse = function (p_strResponse) {
		if (!self.m_controller.checkRequestStillValid(self.m_nRequestNumber)) {
			return;
		}
		var Ybb = p_strResponse.indexOf("xml=");
		var Zbb = p_strResponse.indexOf("&mp3");
		var abb = p_strResponse.substring(Ybb + 4, Zbb);
		var bbb = p_strResponse.substring(Zbb + 5, p_strResponse.length);
		if (abb == "error" || bbb == "error" || abb == "" || bbb == "") {
			if (!self.m_bBackup) {
				if (!self.m_controller.startSpeechBackup(self)) {
					Kab = "Error response from server";
				}
				return;
			} else {
				Kab = "Error response from server";
				self.m_controller.onSpeechStop(-3);
				return;
			}
		}
		if (abb == "busy" || bbb == "busy") {
			if (!self.m_bBackup) {
				if (!self.m_controller.startSpeechBackup(self)) {
					Kab = "Busy response from server";
				}
				return;
			} else {
				Kab = "Busy response from server";
				self.m_controller.onSpeechStop(-3);
				return;
			}
		}
		if (Iea) {
			if (abb.substring(0, 5) == "http:") {
				abb = "https:" + abb.substr(5);
			}
			if (bbb.substring(0, 5) == "http:") {
				bbb = "https:" + bbb.substr(5);
			}
		}
		self.loadFiles(abb, bbb);
	};
	this.loadFiles = function (p_strXmlUrl, p_strMp3Url) {
		p_strXmlUrl = self.m_controller.checkPath(p_strXmlUrl);
		p_strMp3Url = self.m_controller.checkPath(p_strMp3Url);
		self.m_controller.setAudio(p_strMp3Url);
		if (self.m_bHighlightable) {
			var cbb = new SpeechStream.AjaxRequest;
			cbb.doGet(p_strXmlUrl, null, self, "onTimingFileResponse", true);
		} else {
			var ebb = new Array();
			self.m_controller.setTimer(ebb);
			self.m_controller.requestCompleteStartPlayback();
		}
	};
	this.onTimingFileResponse = function (p_xmlResponse) {
		if (!self.m_controller.checkRequestStillValid(self.m_nRequestNumber)) {
			return;
		}
		var ebb = new Array();
		var fbb = new Array();
		if (p_xmlResponse && p_xmlResponse.documentElement && p_xmlResponse.documentElement.childNodes && p_xmlResponse.documentElement.nodeName != "Error") {
			var gbb = p_xmlResponse.documentElement.childNodes;
			var hbb = 0;
			var ibb = false;
			for (i = 0; i < gbb.length; i++) {
				if (gbb[i].nodeType == 1) {
					ebb[hbb] = parseFloat(gbb[i].getAttribute("time") / 1000);
					if (hbb > 0 && ebb[hbb] <= ebb[hbb - 1]) {
						ebb[hbb] = ebb[hbb - 1] + 0.010;
					}
					if (gbb[i].getAttribute("mark")) {
						var jbb = parseInt(gbb[i].getAttribute("mark"));
						if (!isNaN(jbb) && jbb > 65536) {
							if (!ibb && hbb > 0) {
								for (var i = 0; i < hbb; i++) {
									fbb[i] = 0;
								}
							}
							ibb = true;
							var itb = jbb & 65535;
							var jtb = ((jbb | 65535)^65535) / 65536;
							fbb[hbb] = {
								"start" : itb,
								"end" : jtb
							};
							eya("" + hbb + "  " + "start:" + itb + "  end:" + jtb);
						} else if (ibb) {
							fbb[hbb] = {
								"start" : 0,
								"end" : 0
							};
							eya("" + hbb + "  start:0  end:0");
						}
					}
					hbb = hbb + 1;
				}
			}
			self.m_controller.setTimer(ebb);
			self.m_controller.setMathMlTimer(self.m_mathJaxId, fbb.length > 0 ? fbb : null);
			self.m_controller.requestCompleteStartPlayback();
		} else {
			if (!self.m_bBackup) {
				if (!self.m_controller.startSpeechBackup(self)) {
					Kab = "Failed to get timing response from server";
				}
			} else {
				Kab = "Failed to get timing response from server";
				self.m_controller.onSpeechStop(-3);
			}
		}
	};
	this.setMathJaxId = function (p_strMathJaxId) {
		self.m_mathJaxId = p_strMathJaxId;
	};
};
SpeechStream.Html5Speech.prototype.setParameters = function (Msb, Nab, exb, Yab) {
	if (exb == null) {
		exb = "";
	}
	this.m_controller = Msb;
	this.m_nRequestNumber = Nab;
	if (exb.indexOf("math:") == 0) {
		var FBc = exb.indexOf(";");
		var MAc = exb.substring(5, FBc);
		exb = exb.substr(FBc + 1);
		this.m_mathJaxId = MAc;
	} else {
		this.m_mathJaxId = null;
	}
	var Jgb = "text=" + encodeURIComponent(exb) + "&userName=" + encodeURIComponent(Cda) + "&voiceName=" + encodeURIComponent(Msb.getVoiceName()) + "&speedValue=" + encodeURIComponent(Hda);
	if (yca != null) {
		Jgb += "&custID=" + encodeURIComponent(yca);
	}
	if (zca != null) {
		Jgb += "&bookID=" + encodeURIComponent(zca);
	}
	if (Ada != null) {
		Jgb += "&pageID=" + encodeURIComponent(Ada);
	}
	if (Yab) {
		Jgb += "&usePron=Y";
	}
	this.m_params = Jgb;
};
SpeechStream.Html5Speech.prototype.copyParameters = function (Msb, Nab, p_params) {
	this.m_controller = Msb;
	this.m_nRequestNumber = Nab;
	this.m_params = p_params;
};
SpeechStream.Html5Speech.prototype.setStaticParameters = function (p_strDestFolder, p_strDestFilename) {
	var Jgb = "&destFolder=" + p_strDestFolder + "&destFilename=" + p_strDestFilename;
	this.m_params = this.m_params + Jgb;
};
SpeechStream.Html5Speech.prototype.setHighlightable = function (p_bHighlightable) {
	this.m_bHighlightable = p_bHighlightable;
};
SpeechStream.Html5Speech.prototype.getHighlightable = function () {
	return this.m_bHighlightable;
};
SpeechStream.TouchScreenManager = function () {
	var qbb = false;
	var rbb = false;
	var sbb = true;
	var tbb = null;
	this.initialise = function () {
		if (hfa >= 6 && Dga) {}
		else {
			YEb(document, "touchstart", onTouchStart);
			YEb(document, "touchmove", onTouchMove);
			YEb(document, "touchend", onTouchEnd);
			if (!uda && window.frames.length > 0) {
				var i;
				for (i = 0; i < window.frames.length; i++) {
					this.initialiseToFrame(window.frames[i]);
				}
			}
		}
	};
	this.initialiseToFrame = function (HWb) {
		if (hfa >= 6 && Dga) {}
		else if (Qga) {}
		else {
			try {
				if (HWb.document) {
					YEb(HWb.document, "touchstart", onTouchStart);
					YEb(HWb.document, "touchmove", onTouchMove);
					YEb(HWb.document, "touchend", onTouchEnd);
				}
			} catch (err) {}

		}
	};
	this.clickAndSpeak = function (enable) {
		qbb = enable;
	};
	var ubb = "onTouchStart onTouchMove onTouchEnd changedTouches";
	onTouchStart = function (event) {
		if (!bfa) {
			bfa = true;
		}
		rbb = false;
		if (qbb || nfa) {
			if (sbb) {
				var changedTouches = event.changedTouches;
				if (changedTouches != null && changedTouches.length > 0) {
					var Fpb = KMb(changedTouches[0], true);
					tbb = SMb(Fpb);
				}
			}
		}
	};
	onTouchMove = function () {
		if (!bfa) {
			bfa = true;
		}
		rbb = true;
	};
	onTouchEnd = function (event) {
		if (qbb || nfa) {
			if (rbb) {
				tbb = null;
				return;
			}
			if (sbb && !nfa) {
				if (tbb == null) {
					return;
				}
				var target = new THHoverTarget(null, null, tbb.range);
				if (target == null) {
					return;
				}
				sbb = false;
				rw_speakHoverTarget(target);
			} else {
				CIb(event);
			}
		}
	};
};
var xbb = null;
SpeechStream.setUpControllerFactory = function () {
	if (xbb == null) {
		xbb = (function () {
			var ybb = null;
			var zbb = null;
			var Acb = false;
			var Bcb = false;
			var Ccb = new SpeechStream.TouchScreenManager();
			Ccb.initialise();
			if (lea) {
				ybb = null;
				Acb = false;
			} else {
				Ecb();
			}
			function Ecb() {
				var Dcb;
				try {
					if ((Lfa && !Qfa && !Rfa) || (Lfa && (Qfa || (Rfa && lea)) && Mcb()) || fca == "hmh") {
						Dcb = null;
						Acb = false;
					} else {
						Dcb = new Audio();
						var Fcb;
						if (yfa == "mp3") {
							Fcb = "audio/mpeg";
						} else {
							Fcb = "audio/" + yfa;
						}
						Dcb.type = Fcb;
						if (Dcb.canPlayType) {
							Acb = ("no" != Dcb.canPlayType(Fcb)) && ("" != Dcb.canPlayType(Fcb));
						}
					}
				} catch (err) {
					Dcb = null;
					Acb = false;
				}
			}
			var instance = {
				enableTouchEvents : function (enable) {
					Ccb.clickAndSpeak(enable);
				},
				getConnector : function () {
					if (ybb != null) {
						return ybb;
					}
					if (Acb) {
						ybb = new SpeechStream.HTML5Controller();
						if (ufa) {
							var Orb = document.getElementById("rwDrag");
							if (Orb != null) {
								Orb.style.position = "relative";
								ufa = false;
							}
						}
					} else {
						Bcb = instance.hasFlashSupport();
						if (Bcb) {
							ybb = PZb();
						}
					}
					return ybb;
				},
				setSwaConntector : function (p_con) {
					zbb = p_con;
				},
				getSwaConnector : function () {
					if (zbb != null) {
						return zbb;
					} else {
						return instance.getConnector();
					}
				},
				hasFlashSupport : function () {
					if (typeof(eba_no_flash) == "boolean" && eba_no_flash == true) {
						Bcb = true;
					} else {
						var flash = PZb();
						if (flash != null) {
							var Jcb = flash.getVersion();
							var Kcb = parseFloat(Jcb);
							if (Kcb < 1.05 || Kcb == NaN) {
								Bcb = false;
							} else {
								Bcb = true;
							}
						}
					}
					return Bcb;
				},
				doesSupportSpeech : function () {
					if (Acb) {
						return true;
					} else {
						return instance.hasFlashSupport();
					}
				},
				doesSupportHtml5 : function () {
					return Acb;
				}
			};
			return instance;
		})();
	}
};
function Mcb() {
	var Lcb = null;
	try {
		Lcb = !!new ActiveXObject("htmlfile");
	} catch (e) {
		Lcb = false;
	}
	return Lcb;
}
if (!SpeechStream.tools) {
	SpeechStream.tools = {};
}
SpeechStream.tools.translator = (function () {
	var self = {};
	self.translate = function (Usb) {
		if (typeof(Usb) == "string" && Usb.length > 0) {
			var flash = xbb.getConnector();
			if (flash != null) {
				flash.getTranslationGenericPage(Usb, SpeechStream.translatorData.iib, SpeechStream.translatorData.qAc);
			}
		} else {
			var Wjb = "<style type=\"text/css\">" + "div.rwTranWordHeader{" + "font-size:150%;" + "font-weight: bold;" + "padding-bottom: 3px;" + "padding-left: 5px;" + "border-bottom: 1px solid #666666;" + "margin-bottom: 10px;}" + "span.rwMeaningNum{padding-left: 10px;" + "padding-right: 10px;" + "font-weight: bold;}" + "span.rwMeaning{padding-right:10px}" + "</style>" + "<div class=\"rwTranWordHeader\">No valid word selected</div>";
			$rw_transReply(Wjb);
		}
	};
	return self;
})();
if (!SpeechStream.tools) {
	SpeechStream.tools = {};
}
SpeechStream.tools.dictionary = (function () {
	var self = {};
	self.lookup = function (Usb) {
		$rw_dictionaryRequest(Usb);
	};
	return self;
})();
if (!SpeechStream.tools) {
	SpeechStream.tools = {};
}
SpeechStream.tools.pictureDictionary = (function () {
	var self = {};
	self.lookup = function (Usb) {
		if (Usb.length > 0) {
			var flash = xbb.getConnector();
			if (flash != null) {
				flash.getPictureDictionaryPage(Usb, Gda);
			}
		} else {
			var Wjb = "<div class=\"rwDictWordHeader\">No valid word selected</div>";
			CLb(Zba, Wjb);
			HLb(true, Zba);
		}
	};
	return self;
})();
if (!SpeechStream.tools) {
	SpeechStream.tools = {};
}
SpeechStream.tools.factfinder = (function () {
	var self = {};
	self.lookup = function (Kya) {
		var yib = Jya(Kya);
		if (yib.length > 0) {
			var bub = "http://www.google.com/search?q=" + yib;
			Wwa(bub, 'popup', 700, 500, 1, 1, 0, 0, 0, 0, 0);
		}
	};
	return self;
})();
function $rw_barCacheInit() {
	if (Lfa) {
		SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + aga.paths.strFileLoc + "rwcache.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
	} else {
		SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + aga.paths.strFileLoc + "rwcacheSFF.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
	}
	var eqb = "";
	eqb += '<div id="rwGenerateCache" rwTHcomp="1" texthelpStopContinuous="1">';
	eqb += '<div class="rwGenerateCachePopupOutline">';
	eqb += '<div id="rwDragMeGenerateCache" class="rwToolbarCaptionGenerateCache" ignore="1">';
	eqb += 'Loading, please wait...';
	eqb += '<img name="displayImg" align="right" src="' + aga.paths.strFileLoc + 'rwimgs/thex.bmp" onMouseOver="$rw_divOver(9)" onMouseOut="$rw_divOut(9)" ' + 'onMouseUp="$rw_divPress(9)"/>';
	eqb += '</div>';
	eqb += '<div class="rwGenerateCachePopupContent">';
	eqb += '<span id="rwGenerateCachedisplay" ignore="1">';
	eqb += '';
	eqb += '</span>';
	eqb += '</div>';
	eqb += '</div>';
	eqb += '</div>';
	SSDOM.eob(aga.pageData.placeholder, eqb, false);
}
function $rw_barRecInit() {
	if (Lfa) {
		SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + aga.paths.strFileLoc + "rwrecorder.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
	} else {
		SSDOM.eob(aga.pageData.placeholder, "<link href=\"" + aga.paths.strFileLoc + "rwrecorderSFF.css\" type=\"text/css\" rel=\"stylesheet\"/>", false);
	}
	var eqb = "";
	eqb += '<div id="rwRec" rwTHcomp="1" style="visibility:hidden" texthelpStopContinuous="1">';
	eqb += ' <div class="rwRecPopupOutline">';
	if (Eda == ENG_UK || Eda == ENG_US) {
		eqb += '  <div id="rwDragMeRec" class="rwToolbarCaptionRec" >';
	} else {
		eqb += '  <div id="rwDragMeRec" class="rwToolbarSpanCaptionRec" >';
	}
	eqb += '<img name="recImg" align="right" src="' + aga.paths.strFileLoc + 'rwimgs/thex.bmp" onmouseover="$rw_divOver(12);" onmouseout="$rw_divOut(12);" onmouseup="$rw_divPress(12);"/></div>';
	eqb += '<div class="rwRecPopupContent">';
	eqb += '<form name="rw_recForm" class="rw_recForm" id="rw_recForm">';
	eqb += '<table class="rw_recTable">';
	eqb += "<tbody>";
	eqb += '<tr><td><div class="rw_micDisplay" id="rw_micDisplay" onclick="recordExercise(START_RECORDING)"></div><div class="rw_micRecordingOn" id="rw_micRecordingOn" onclick="recordExercise(STOP_RECORDING)"></div></td></tr>';
	eqb += '<tr><td><div id="coverUpDiv"></div><div class="rw_playBackButton" id="rw_playBackButton" onclick="recordExercise(REPLAY_RECORDING)"></div></td></tr>';
	eqb += '</tbody></table></form>';
	eqb += '<div id="flashcontent" align="center"><embed type="application/x-shockwave-flash" src="SpeechStream/v194/flashaudiocontrol/textHelpAudioRecord.swf" width="300" height="138" style="undefined" id="audiocontrol" name="audiocontrol" bgcolor="#FF6600" quality="high" wmode="transparent" flashvars="myServer=SpeechStream/v194/flashaudiocontrol/&amp;mySound=3845_2013-12-03_155532191_p&amp;schoolId=235&amp;studentId=20543&amp;signature=acK8In%2Fa6khZxBMNM6XsPQ%3D%3D&amp;soundMode=playOnly&amp;audioStreamFileName=SpeechStream/v194/flashaudiocontrol/3845_2013-12-03_155532191_p&amp;recordType=1&amp;studentRecordModePendingText=Uploading...&amp;studentRecordModeRecordingText=Recording...&amp;audioQualityOGG=0.5&amp;studentRecordModeCSSPath=SpeechStream/v194/flashaudiocontrol/flashstyle.css"></embed></div>';
	eqb += '</div></div></div>';
	SSDOM.eob(aga.pageData.placeholder, eqb, false);
}
var START_RECORDING = 0;
var STOP_RECORDING = 2;
var REPLAY_RECORDING = 3;
var Ycb = false;
var Zcb = 0;
var acb = 0;
var bcb = false;
function recordExercise(i) {
	if (i == START_RECORDING) {
		var gCc = document.getElementById("rw_micDisplay");
		gCc.style.visibility = "hidden";
		var gcb = document.getElementById("rw_micRecordingOn");
		gcb.style.visibility = "visible";
		icb("audiocontrol").stopRecording();
		jcb(false);
		icb("audiocontrol").startRecording();
		Zcb = new Date().getTime();
	} else if (i == STOP_RECORDING) {
		var gCc = document.getElementById("rw_micDisplay");
		gCc.style.visibility = "visible";
		var gcb = document.getElementById("rw_micRecordingOn");
		gcb.style.visibility = "hidden";
		var hcb = document.getElementById("coverUpDiv");
		hcb.style.visibility = "hidden";
		Ycb = false;
		lcb();
		icb("audiocontrol").stopRecording();
		acb = new Date().getTime();
	} else if (i == REPLAY_RECORDING) {
		Ycb = true;
		lcb();
		icb("audiocontrol").stopRecording();
		acb = new Date().getTime();
		icb("audiocontrol").playStreaming();
	}
}
function icb(movieName) {
	if (navigator.appName.indexOf("Microsoft") != -1) {
		if (window[movieName]) {
			return window[movieName];
		} else {
			return document[movieName];
		}
	} else {
		return document[movieName];
	}
}
function jcb() {
	g_timerSeconds = 175;
	lcb();
	StartTheTimer();
}
function StartTheTimer() {
	if (g_timerSeconds == 0) {
		lcb();
		recordExercise(STOP_RECORDING);
	} else {
		self.status = g_timerSeconds;
		g_timerSeconds -= 1;
		bcb = true;
		g_timerID = self.setTimeout("StartTheTimer()", 1000);
	}
}
function lcb() {
	if (bcb) {
		clearTimeout(g_timerID);
	}
	bcb = false;
}
if (typeof($rw_swaParameters) == "function") {
	iea = true;
	$rw_swaParameters();
	if (typeof($rw_userParameters) == "function") {
		$rw_userParameters();
	}
	$rw_barInit();
} else if (typeof($rw_userParameters) == "function") {
	iea = true;
	$rw_userParameters();
	$rw_barInit();
}