﻿
function speechStartedCallback() {
    console.log('Speech has started!');
}


function speechCompletedCallback() {
    console.log('Speech has finished!');
}

function speechRenderingCallback() {
    console.log('Speech rendering has triggered!');
}

var gSpeechCompletedCallback = undefined;
function speechPageCompletedCallback() {
    console.log('Speech page has finished!');
    if (gSpeechCompletedCallback) {
        gSpeechCompletedCallback();
    }
}