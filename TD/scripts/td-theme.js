(function ($) {
	// Delay load of shared resources.
	$('head').append('<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext" rel="stylesheet" type="text/css"/>');
	$('head').append('<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>');
	
	// Load Tenant specific Theme.
	console.log("Detected tenant id: " + $Global$.tenant + " and theme: " + $Global$.theme);
	$('head').append('<link rel="stylesheet" type="text/css" href="' + $Global$.assetUrl + '/styles/td.css' + $Global$.assetExt + '">');
	var themeUrl = $Global$.assetUrl + "/styles/td-purple.css"; //default theme
	if ($Global$.theme) {
		themeUrl = $Global$.assetUrl + "/styles/td-" + $Global$.theme + ".css" + $Global$.assetExt;
	}
	$('head').append('<link rel="stylesheet" type="text/css" href="' + themeUrl + '">');
})(jQuery);
