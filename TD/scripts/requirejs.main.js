﻿/**
 * Based on code by Bertrand Chevrier - OAT
 * @author Bertrand Chevrier <bertrand@taotesting.com>
 * @see {@link https://github.com/oat-sa/parc-itemrunner/wiki}
 */

'use strict';

var version = "0.9.0";

var defaultTheme = 'blackOnWhite';

//configuration of the bundle: libraries paths,
requirejs.config({
    //  baseUrl : '/TD/OAT/js', // TR: Removing baseUrl to streamline minification. - this will default to the value specified in for data-main attribute where require is referenced in index.html or testDriver.phtml.
    // urlArgs: "buster=" + version,      //buster ensure files are not in cache for that version


    //alias for external librairies, paths are relative to the baseUrl (ie. requirejs)
    paths: {
        'ckeditor': '../OAT/dist/assets/ckeditor/ckeditor', // '../dist/assets/ckeditor/ckeditor',
        'mathJax': '../OAT/dist/assets/mathjax/MathJax.min', // '../dist/assets/mathjax/MathJax.min',
        //  'qtiItemRunner': '../OAT/dist/qtiItemRunner.min',
        'qtiItemRunner': '../OAT/dist/qtiItemRunnerFull.min',
        'qtiScorer': '../OAT/dist/qtiScorer.min'
    },

    //configure paths for external libraries, they are relative to the page.  
    //TR: Changed paths below to use require.toUrl() whene generating references so that the paths are relative to OAT/js folder instead of the page. Relative paths are needed for the release build.
    config: {
        'mediaElement': {
            plugins: [],
            pluginPath: require.toUrl('../OAT/dist/assets/mediaelement/') //where mediaElement loads its plugin based players
        },

        'MathJax/MathJax': {
            root: require.toUrl('../OAT/dist/assets/mathjax/')       //where MathJax will look for it's fonts
        },

        'mathJax': {
            menuSettings: {
                inTabOrder: false
            }
        },

        'ui/themes': {
            items: {                          //register the themes for the IR (at least the default is required)
                base: require.toUrl('../OAT/dist/qti-runner.css'),
                default: defaultTheme,
                available: [{
                    id: 'tao',
                    path: require.toUrl('../OAT/dist/themes/tao-default/theme.css')
                }, {
                    id: 'blackOnWhite',
                    path: require.toUrl('../OAT/dist/themes/black-on-white/theme.css')
                }, {
                    id: 'whiteOnBlack',
                    path: require.toUrl('../OAT/dist/themes/white-on-black/theme.css')
                }, {
                    id: 'blackOnCream',
                    path: require.toUrl('../OAT/dist/themes/black-on-cream/theme.css')
                }, {
                    id: 'blackOnLightBlue',
                    path: require.toUrl('../OAT/dist/themes/black-on-light-blue/theme.css')
                }, {
                    id: 'blackOnLightMagenta',
                    path: require.toUrl('../OAT/dist/themes/black-on-light-magenta/theme.css')
                }, {
                    id: 'greyOnGreen',
                    path: require.toUrl('../OAT/dist/themes/grey-on-green/theme.css')
                }, {
                    id: 'lightBlueOnDarkBlue',
                    path: require.toUrl('../OAT/dist/themes/light-blue-on-dark-blue/theme.css')
                }]
            }
        }
    },

    //set timeout for require.js to download
    waitSeconds: 300,

    //CKEditor still exposes a global variable, it needs to be shimed.
    shim: {
        'ckeditor': {
            exports: 'CKEDITOR'
        }
    }
});
