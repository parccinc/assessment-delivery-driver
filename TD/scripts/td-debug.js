var $TDEnvironmentUrl$ = {};
if ($Global$.test) {
    $Global$.token = "Vm0xMFYxVnRVa1pPV0VaWFYwZDRhRmxYZUdGamJGSldWV3M1YVZKdGVEQldNVkpQVjJ4WmVGZHVUbFZOVmtwTVdWZHplRmRHWkZWVGJIQk9Za1pXTmxaR1dtdFNiVlpHVDFaV1dGWkdXbEZXYTJRd1RVWlNXV05HV210TlYzaFdWbXhTVjFkdFNsWmlSRnBZWWtaS1JGbHNXbmRqVms1WlkwVTVhR0pIZERWV01uUlRWbXN4UmsxVlZsTmlXRUpSVm1wQ1ZtUXhVbFpWYTBwc1VsUkdSbFpzVVhkUVVUMDk=";
    $TDEnvironmentUrl$.int = "http://int.tds.adp.parcc-ads.breaktech.org";
    $TDEnvironmentUrl$.stable = "https://stable.tds.adp.parcc-ads.breaktech.org";
    $Global$.apiTime = new Date().getTime() / 1000;
    $Global$.login = {
        "AR": {
            "name": "Arkansas",
            "pid": false,
            "dob": false
        },
        "CO": {
            "name": "Colorado",
            "pid": false,
            "dob": false
        },
        "DC": {
            "name": "District of Columbia",
            "pid": false,
            "dob": false
        },
        "IL": {
            "name": "Illinois",
            "pid": true,
            "dob": true
        },
        "LA": {
            "name": "Louisiana",
            "pid": true,
            "dob": false
        },
        "MA": {
            "name": "Massachusetts",
            "pid": false,
            "dob": true
        },
        "MD": {
            "name": "Maryland",
            "pid": true,
            "dob": false
        },
        "MS": {
            "name": "Mississippi",
            "pid": false,
            "dob": true
        },
        "NJ": {
            "name": "New Jersey",
            "pid": false,
            "dob": true
        },
        "NM": {
            "name": "New Mexico",
            "pid": true,
            "dob": true
        },
        "NY": {
            "name": "New York",
            "pid": true,
            "dob": true
        },
        "OH": {
            "name": "Ohio",
            "pid": true,
            "dob": true
        },
        "PT": {
            "name": "",
            "pid": false,
            "dob": false
        },
        "RI": {
            "name": "Rhode Island",
            "pid": true,
            "dob": true
        }
    };
    $Global$.theme = "purple";
    $Global$.tenant = 1;
    //$Global$.testKey = "PRACTZLDLP";
}
var uri = location.search;
var queryString = {};
uri.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function ($0, $1, $2, $3) {
    queryString[$1] = $3;
});
if (typeof queryString["env"] !== "undefined") {
	$Global$.apiHost = $TDEnvironmentUrl$[queryString["env"]]
}
