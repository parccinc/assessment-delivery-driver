(function ($) {
    $.widget("td.content", {

        options: {
            headerHeight: 78,
            footerHeight: 27
	    },

	    global: {
		    model: {
			    version: "1.2.31"
		    },
		    settings: {
		        toolbar: {
		            fullscreen: true,
		            linereader: false,
		            tts: false
		        }
		    }
	    },

	    _create: function () {

		    console.log("In ContentWidget._create();");

		    var self = this;
		   
		    this.options.container = $("<div class='td-container'/>").appendTo(this.element);

		    this.global.settings.timeOffset = 1000 * $Global$.apiTime - new Date().getTime();
        
		    // Create widgets that use container
		    this.options.login = this.options.container.login({
                headerHeight: this.options.headerHeight,
			    logindone: function () {
			        self.options.login.login("close");
			        self.options.start.starttest("open", self.global.settings);
			    }
		    });
		    this.options.start = this.options.container.starttest({
		        headerHeight: this.options.headerHeight,
		        starttest: function () {
		            self.options.start.starttest("close");
		            self.options.item.item("open", self.global.settings);
		            $('.td-ttstoolbox-container').show();
		        },
		        justexit: function (evt, purp) {
		            if (self.global.settings.config.enablePause) {
		                self.global.settings = {};
		                self.global.data = {};
		                self.global.testInfo = {};
		                window.location.reload();
		                screenfull.exit();
		            }
		            else {
		                self.options.start.starttest("close");
		                var ss = self.options.container.savesubmit({ logoHeight: 128, error: true, purpose: purp });
		                ss.savesubmit("open", self.global.settings);
		            }
		        }
		    });
		    this.options.item = this.options.container.item({
		        headerHeight: this.options.headerHeight,
                saveandexitsucceeded: function () {
                    $('.td-ttstoolbox-container').hide();
                    self.options.item.item("close");                   
		            var ss = self.options.container.savesubmit({ save: true, logoHeight: 128 });
		            ss.savesubmit("open", self.global.settings);
		        },
		        saveandsubmitsucceeded: function () {
		            $('.td-ttstoolbox-container').hide();
		            self.options.item.item("close");
		            var ss = self.options.container.savesubmit({ save: false, logoHeight: 128 });
		            ss.savesubmit("open", self.global.settings);
		        },
		        showscorereport: function(evt, purp){
		            $('.td-ttstoolbox-container').hide();
		            self.options.item.item("close");
		            if (self.global.settings.config.enableSubmit) {
		                var ss = self.options.container.savesubmit({ save: false, logoHeight: 128 });
		            }
		            else {
		                var ss = self.options.container.savesubmit({logoHeight: 128, error: true, purpose: purp });
		            };
		            ss.savesubmit("open", self.global.settings);
		            var sr = self.options.container.scorereport({});
		            sr.scorereport("open", self.global.settings);
		        },
		        saveanderrorsucceeded: function () {
		            $('.td-ttstoolbox-container').hide();
		            self.options.item.item("close");
		            var ss = self.options.container.savesubmit({ logoHeight: 128, error: true });
		            ss.savesubmit("open", self.global.settings);
		        },
		        cancelsucceeded: function (evt, purp) {
		            $('.td-ttstoolbox-container').hide();
		            self.options.item.item("close");
		            var ss = self.options.container.savesubmit({ logoHeight: 128, error: true, purpose: purp });
		            ss.savesubmit("open", self.global.settings);
		        }
		    });

            //create div that is ttsToolbox container
		    this.options.footerHeight = $('.td-sticky-footer-wrapper').outerHeight();
		    $("<div class='td-ttstoolbox-container'/>").appendTo(this.options.container)
                .css({ 'top': this.options.headerHeight + 'px', 'bottom': this.options.footerHeight + 'px' });

	        // Start
		    this._start();
	    },

	    _start: function () {
	        this.options.login.login("open", this.global.settings);
	    }
    });
})(jQuery);
