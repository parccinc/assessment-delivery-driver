(function ($) {

    $.UTILITY = {

        isSystemItem: function (item) {
            return item && item.category && item.category.length && item.category.indexOf("systemItem") > -1;
        },

        isScoreReport: function (item) {
            return item && item.category && item.category.length && item.category.indexOf("systemItem") > -1 && item.category.indexOf("scoreReport") > -1;
        },

        isNotScored: function (item) {
            return item && item.category && item.category.length && item.category.indexOf("notScored") > -1;
        },

        hasAssets: function (item) {
            return typeof item.data !== "undefined";
        },

        algorithmCanGoBack: function (section) {
            return section && section.dynamic && section.dynamic.selectionAlgorithm && section.dynamic.selectionAlgorithm.length && section.dynamic.selectionAlgorithm == "nonlinear";
        },

        algorithmCanGoForward: function (section) {
            return section && section.dynamic && section.dynamic.selectionAlgorithm && section.dynamic.selectionAlgorithm.length ? true : false; // forcing true or false returned
        },

        getItemScore: function (item) {
            var score = NaN;
            if (item && item.dynamic && item.dynamic.score && item.dynamic.score.value && item.dynamic.score.value.SCORE && item.dynamic.score.value.SCORE.base) {
                if (typeof item.dynamic.score.value.SCORE.base.float !== "undefined") {
                    score = item.dynamic.score.value.SCORE.base.float;
                }
                else if (typeof item.dynamic.score.value.SCORE.base.integer !== "undefined") {
                    score = item.dynamic.score.value.SCORE.base.integer;
                }
            }
            return score;
        },

        getItemGrade: function (item) {
            return item && item.itemGrade && $.isNumeric(item.itemGrade) ? item.itemGrade.toString() : "undefined";
        },

        getItemScoringType: function (item) {
            // "MC", "MS", "EBSR", "DEFAULT"
            // This is "work-in-progress" - depending on the item metadata. For now we will assume that, if metadata is missing, that item scoring type is defaut (MAX_SCORE == 1)
            if (typeof item.itemType === "undefined") {
                if (typeof item.maxScorePoints === "undefined") {
                    return "DEFAULT";
                }
                else if (item.maxScorePoints === 2) { // Make sure to use ===
                    return "EBSR";
                }
                return "MX"; // Do not know, might be either MS or MC
            }
            return item.itemType.toString();
        },

        isItemAnswered: function (item) {
            var answered = false;
            if (item && item.dynamic && item.dynamic.response && item.dynamic.response.length > 0) {
                $.each(item.dynamic.response, function (id, resp) {
                    if (resp !== null) {
                        answered = true;
                    }
                });
            };
            return answered;
        },

        isItemAnsweredCorrectly: function (item) {
            var score = this.getItemScore(item);
            var maxScore = 1;
            if (this.getItemScoringType(item) === "EBSR") {
                maxScore = 2;
            }
            return maxScore == score; // Make sure not to use ===
        },

        isLinkingItem: function (item, testGrade) {
            // This is a working version, hence non-optimized in any shape or form. I expect all kind of junk in here.
            console.log("testGrade: " + testGrade + ", itemGrade: " + (typeof item["itemGrade"] !== "undefined" ? item.itemGrade : "undefined"));
            if (testGrade && testGrade.length && typeof item["itemGrade"] !== "undefined" && typeof item["itemGrade"] === "string" && item.itemGrade.length) {
                var strItemGrade = $.isNumeric(item.itemGrade) ? item.itemGrade.toString() : item.itemGrade.toLowerCase();
                var strTestGrade = $.isNumeric(testGrade) ? testGrade.toString() : testGrade.toLowerCase();
                var numItemGrade = parseInt(strItemGrade, 10);
                var numTestGrade = parseInt(testGrade, 10);
                if ($.isNumeric(numItemGrade) && $.isNumeric(numTestGrade)) {
                    return numItemGrade !== numTestGrade;
                }
                return strItemGrade !== strTestGrade;
            }
            return true;
        },

        timeSpentOnItem: function (item) {

            // logic: time chunk begins (always) with "navto" event. It ends in one of three ways: "navfrom", "pause" or "submit". We calculate time for each item, all the time.
            var startIndex = this._findIntervalStart(item, 0);
            var endIndex = 0;
            var pairs = [];

            while (startIndex >= 0) {
                var startTimeStamp = item.dynamic.events[startIndex].time;
                endIndex = this._findIntervalEnd(item, startIndex + 1);
                var endTimeStamp = endIndex >= 0 ? item.dynamic.events[endIndex].time : $.now();
                pairs.push({
                    start: startTimeStamp,
                    end: endTimeStamp
                });

                startIndex = endIndex >= 0 ? this._findIntervalStart(item, endIndex + 1) : -1;
            }

            var totalTime = 0;
            $.each(pairs, function (index, pair) {
                totalTime += pair.end - pair.start;
            });
            // return Math.round(totalTime / 1000);
            return totalTime;
        },

        _findIntervalStart: function(item, startIndex) {
            return this._propertySearch(item.dynamic.events, "type", "navto", startIndex);
        },

        _findIntervalEnd: function (item, startIndex) {
            var navto = this._propertySearch(item.dynamic.events, "type", "navto", startIndex);
            var navfrom = this._propertySearch(item.dynamic.events, "type", "navfrom", startIndex);
            var pause = this._propertySearch(item.dynamic.events, "type", "pause", startIndex);
            var submit = this._propertySearch(item.dynamic.events, "type", "submit", startIndex);
            var good = [];
            if (navto >= 0) {
                if (navfrom >= 0 && navfrom < navto) {
                    good.push(navfrom);
                }
                if (pause >= 0 && pause < navto) {
                    good.push(pause);
                }
                if (submit >= 0 && submit < navto) {
                    good.push(submit);
                }
            }
            else {
                if (navfrom >= 0) {
                    good.push(navfrom);
                }
                if (pause >= 0) {
                    good.push(pause);
                }
                if (submit >= 0) {
                    good.push(submit);
                }
            }
            if (good.length) {
                good.sort(function (val1, val2) {
                    return val1 - val2;
                });
                return good[0];
            }
            return -1;
        },

        _propertySearch: function (obarray, property, value, startIndex) {
            var si = typeof startIndex !== "undefined" ? startIndex : 0;
            if (obarray && obarray.length) {
                for (var i = si; i < obarray.length; i++) {
                    if (obarray[i] && typeof obarray[i][property] !== "undefined" && obarray[i][property] === value) {
                        return i;
                    }
                }
            }
            return -1;
        },

        isItemScored: function (item) {
            return item && item.dynamic && item.dynamic.score && typeof item.dynamic.score.value !== "undefined" && item.dynamic.score.value != null;
        },

        getToolsUsed: function (item) {
            var toolsUsed = "";
            for (var i = 0; i < item.dynamic.events.length; i++) {
                if (item.dynamic.events[i].type === "navto") {
                    console.log(item.dynamic.events[i].value);
                    if (item.dynamic.events[i].value.textToSpeech && toolsUsed.indexOf("Text to Speech") == -1) {
                        toolsUsed += "Text to Speech,";
                    }
                    if (item.dynamic.events[i].value.lineReader && toolsUsed.indexOf("Line Reader") == -1) {
                        toolsUsed += "Line Reader,";
                    }

                }
                else if (item.dynamic.events[i].type === "activated") {
                    if (toolsUsed.indexOf(item.dynamic.events[i].value.name) == -1) {
                        toolsUsed = toolsUsed + item.dynamic.events[i].value.name + ",";
                    }
                }
            }
            if (toolsUsed.length > 0) {
                toolsUsed = toolsUsed.substring(0, (toolsUsed.lastIndexOf(",")));
            }
            return toolsUsed;
        },

        utf8toBase64: function (str) {
                return window.btoa(unescape(encodeURIComponent(str)));
        },

        base64toUtf8: function (str) {
            return decodeURIComponent(escape(window.atob(str)));
        }
    };

})(jQuery);
