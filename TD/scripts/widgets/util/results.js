(function ($) {

    $.RESULTS = {

        options: {
        },

        create: function (data) {
            if (data && data.data && data.data.dynamic) {
                this._setResults(data);
                this._createResults();
            }
            else {
                throw new Error("Cannot create results, invalid result object structure!");
            }
        },

        _setResults: function(data) {
            this.options.data = data;
            this.options.data.data.dynamic.results = {};
        },

        _createResults: function() {
            this._createItemLevelData(); // Items go first, so I can reuse that for the test later.
            this._createTestLevelData();
        },

        _createItemLevelData: function () {

            var self = this;
            $.each(this.options.data.data.dynamic.history, function (packageHistoryIndex, testPartIndex) {
                $.each(self.options.data.data.testPart[testPartIndex].dynamic.history, function (testPartHistoryIndex, assessmentSectionIndex) {
                    $.each(self.options.data.data.testPart[testPartIndex].assessmentSection[assessmentSectionIndex].dynamic.history, function (assessmentSectionHistoryIndex, assessmentItemIndex) {

                        var item = self.options.data.data.testPart[testPartIndex].assessmentSection[assessmentSectionIndex].assessmentItem[assessmentItemIndex];
                        item.dynamic.results = {
                            itemInternalIdentifier: typeof item.itemId !== "undefined" ? item.itemId : "",                                          // This one is uterly useless and has no meaning in our results whatsoever, plus it is painfully redundant
                            //itemTestPart: testPartIndex,                                                                                            // This and the following one are useless without the item index in the pool
                            //ItemSection: assessmentSectionIndex,                                                                                    // Missing the item pool index to have some meaning - this way it is completely useless.
                            //itemResponseFinal: $.UTILITY.isItemScored(item) ? item.dynamic.response[item.dynamic.response.length - 1] : null,       // Last scored response
                            //itemResponseOriginal: $.UTILITY.isItemAnswered(item) ? item.dynamic.response[item.dynamic.response.length - 1] : null,  // And it cannot change regardless
                            timeSpent: $.UTILITY.timeSpentOnItem(item),                                                                             // Calculated
                            //itemScore: $.UTILITY.getItemScore(item),                                                                                // Redundant
                            isItemCorrect: $.UTILITY.isItemAnsweredCorrectly(item),                                                                 // Redundant
                            //isItemAnswered: $.UTILITY.isItemAnswered(item),                                                                         // Redundant
                            //isItemScored: $.UTILITY.isItemScored(item),                                                                             // Idiotic, am I really surprised there!?!
                            toolsUsed: $.UTILITY.getToolsUsed(item)                                                                                 // True dependency
                        };
                    });
                });
            });
        },

        _createTestLevelData: function () {

            var self = this;
            this.options.data.data.dynamic.results = {
                testAssignmentId: this.options.data.settings.credentials.testkey,
                formRevision: this.options.data.settings.test.id,
                testStartDateTime: 0,
                testSubmitDateTime: 0,
                timeSpentOnItems: 0,
                timeSpentOnAllItems: 0,
                testDuration: 0,
                totalItemsPresented: 0,
                totalQuestionsPresented: 0,
                totalItemsAnsweredCorrectly: 0,
                totalItemsAnsweredIncorrectly: 0,
                totalItemsNotAnswered: 0,
                totalItemsNotScored: 0,
                testRawScore: 0,
                testScoreTheta: 0.0,
                testScoreScale: 0.0,
            };

            $.each(this.options.data.data.dynamic.history, function (packageHistoryIndex, testPartIndex) {
                $.each(self.options.data.data.testPart[testPartIndex].dynamic.history, function (testPartHistoryIndex, assessmentSectionIndex) {
                    $.each(self.options.data.data.testPart[testPartIndex].assessmentSection[assessmentSectionIndex].dynamic.history, function (assessmentSectionHistoryIndex, assessmentItemIndex) {

                        var item = self.options.data.data.testPart[testPartIndex].assessmentSection[assessmentSectionIndex].assessmentItem[assessmentItemIndex];

                        // First item presented
                        if (packageHistoryIndex == 0 && testPartHistoryIndex == 0 && assessmentSectionHistoryIndex == 0) {
                            var startIndex = $.UTILITY._findIntervalStart(item, 0);
                            var startTime = item.dynamic.events[startIndex].time;
                            var st = new Date(startTime).toLocaleString();
                            self.options.data.data.dynamic.results.testStartDateTime = startTime;
                            console.log('Start time: ' + st);
                        }
                        // Last item presented
                        if (packageHistoryIndex == self.options.data.data.dynamic.history.length - 1 &&
                            testPartHistoryIndex == self.options.data.data.testPart[testPartIndex].dynamic.history.length - 1 &&
                            assessmentSectionHistoryIndex == self.options.data.data.testPart[testPartIndex].assessmentSection[assessmentSectionIndex].dynamic.history.length -1){
                            var endIndex = $.UTILITY._propertySearch(item.dynamic.events, "type", "submit", startIndex);
                            var endTime = item.dynamic.events[endIndex].time;
                            var et = new Date(endTime).toLocaleString();
                            self.options.data.data.dynamic.results.testSubmitDateTime = endTime;
                            console.log('End time: ' + et);
                        }
                        // time spent on item
                        var timeSpentOnItem = item.dynamic.results.timeSpent;
                        if (!$.UTILITY.isSystemItem(item)) {
                            self.options.data.data.dynamic.results.timeSpentOnItems += timeSpentOnItem;
                            self.options.data.data.dynamic.results.totalQuestionsPresented++;
                            if (item.dynamic.results.isItemCorrect) {
                                self.options.data.data.dynamic.results.totalItemsAnsweredCorrectly++;
                            }
                            else {
                                self.options.data.data.dynamic.results.totalItemsAnsweredIncorrectly++;
                            }
                            if (!$.UTILITY.isItemAnswered(item)) {
                                self.options.data.data.dynamic.results.totalItemsNotAnswered++;
                            }
                            if (!$.UTILITY.isItemScored(item)) {
                                self.options.data.data.dynamic.results.totalItemsNotScored++;
                            }
                            self.options.data.data.dynamic.results.testRawScore += isNaN($.UTILITY.getItemScore(item)) ? 0 : $.UTILITY.getItemScore(item);
                        }
                        self.options.data.data.dynamic.results.timeSpentOnAllItems += timeSpentOnItem;
                        self.options.data.data.dynamic.results.totalItemsPresented++;
                    });
                });
            });
            this.options.data.data.dynamic.results.testDuration = this.options.data.data.dynamic.results.testSubmitDateTime - this.options.data.data.dynamic.results.testStartDateTime;
        }
    };

})(jQuery);