(function ($) {
    $.widget("td.timer", {

        options: {
        },

        _create: function () {
            console.log('In Timer');
        },

        _load: function () {
        },

        _ticker: function () {

            var elapsed = Math.round((new Date().valueOf() - this.options.ticks) / 1000.0);
            var remaining = Math.round(this.options.duration * 60.0 * 60.0 - elapsed);

            var seconds = remaining % 60;
            remaining = Math.floor(remaining / 60);
            var minutes = remaining % 60;
            remaining = Math.floor(remaining / 60);
            var hours = remaining % 24;

            var ticker = this._checkPad(hours) + ':' + this._checkPad(minutes) + ':' + this._checkPad(seconds);

            this._trigger("ping");

            setTimeout($.proxy(this._ticker, this), this.options.period);
        },

        _checkPad: function (val) {
            if (val < 10) {
                val = "0" + val;
            };
            return val;
        },

        _fire: function (name) {
            this._trigger(name);
            if (this.options['_' + name].periodic) {
                this.options['_' + name].id = setTimeout($.proxy(this._fire, this, name), this.options['_' + name].period);
            }
            else {
                this.options['_' + name].id = null;
            }
        },

        create: function (name, period, periodic) {
            if (this.exists(name)) {
                throw new Error('Timer with identifier:' + name + ' already exists');
            }
            else {
                this.options['_' + name] = { id: null, period: period, periodic: periodic };
            }
        },

        exists: function(name) {
            // return this.options['_' + name] !== undefined;
            return this.options.hasOwnProperty('_' + name);
        },

        start: function (name, immediately) {
            if (this.exists(name)) {
                if (typeof immediately === 'undefined' || !immediately) {
                    this.options['_' + name].id = setTimeout($.proxy(this._fire, this, name), this.options['_' + name].period);
                }
                else {
                    this._fire(name);
                }
            }
        },

        stop: function (name) {
            if (this.exists(name)) {
                clearTimeout(this.options['_' + name].id);
                this.options['_' + name].id = null;
            }
        },

        clear: function (name) {
            if (this.exists(name)) {
                delete this.options['_' + name];
            }
        }
    });
})(jQuery);
