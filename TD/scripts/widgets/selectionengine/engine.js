var gInstanceCounter = 0;
(function ($) {
    $.widget("shureco.engine", {

        options: {
            level: 0
        },

        _create: function () {

            var self = this;

            var cls = ".td-" + new Date().valueOf() + "-" + gInstanceCounter++;
            this.options.element = $('<div class="' + cls + '"/>').appendTo(this.element);

            this.options.element
                .on('nextselected previousselected', function (event, param) {
                    if (self.options.engine) {
                        self._destroySubjectEngine();
                    }
                    if (self.options.data.dynamic.current >= 0 && self.options.data.dynamic.current != null) {
                        self._createSubjectEngine();
                    }

                    self.fireselectionchanged();
                });
            // Do async (outside of _create :( (widget "bug"))
            setTimeout(function () {
                self._trigger('ready');
            }, 0);
        },

        _load: function (data) {
            this.options.data = data;
            this._establishCurrent();
        },

        _establishCurrent: function () {
            if (this.options.data.dynamic.current == -1) {
                this._next();
            }
            else {
                this._createSubjectEngine();
                this.fireselectionchanged();
            }
        },

        _destroySubjectEngine: function () {
            if (this.options.engine != null) {
                console.log('destroying engine: ' + this.options.data.dynamic.type + '[' + this.options.data.dynamic.collection + ']')

                this.options.engine.engine("destroy");
                this.options.engine = null;
            }
        },

        _createSubjectEngine: function () {

            var collectionElement = this.options.data[this.options.data.dynamic.collection][this.options.data.dynamic.history[this.options.data.dynamic.current]];
            if (collectionElement.dynamic.collection) {

                try {
                    var self = this;
                    var classSelector = ".td-" + collectionElement.dynamic.collection + "-engine";
                    this.options.engine = $(classSelector).engine({

                        eventSink: this.options.eventSink,

                        ready: function (event, readyParam) {
                            self.options.engine.engine('load', collectionElement);
                        },

                        atstart: function () {
                            console.log('atstart fired from engine: ' + collectionElement.dynamic.type + '[' + collectionElement.dynamic.collection + ']');
                            self._rewind();
                        },

                        atend: function () {
                            console.log('atend fired from engine: ' + collectionElement.dynamic.type + '[' + collectionElement.dynamic.collection + ']');
                            self._advance();
                        },

                        selectionchanged: function (event, selector) {
                            var currentSelection = {};
                            currentSelection[self.options.data.dynamic.collection] = self.options.data.dynamic.history[self.options.data.dynamic.current];
                            self._trigger('selectionchanged', null, $.extend(currentSelection, selector));
                        }

                    });
                }
                catch (error) {
                    alert(error);
                }
            }
        },

        _advance: function () {
            this._destroySubjectEngine();
            this._next();
        },

        _rewind: function () {
            this._destroySubjectEngine();
            this._previous();
        },

        _next: function () {

            // If inside history - not at the end, then just take whatever is at the current position
            if (this.options.data.dynamic.current < this.options.data.dynamic.history.length - 1) {
                this.options.data.dynamic.current++;
                this.options.element.triggerHandler('nextselected');
            }
            else {
                if (this.options.data.dynamic.sealed) {
                    this._atend();
                }
                else {
                    var self = this;
                    this._asyncNext(function (data) {

                        if (data === null || (typeof data === "object" && data.items && data.items.length == 0)) {
                            if (data !== null) {
                                self.options.data.dynamic.state = data.studentState;
                            }
                            self.options.data.dynamic.sealed = true;
                            self._atend();
                        }
                        else {
                            if (typeof data === "object") {
                                if (!(data.items[0].id in self.options.data.map)) {
                                    alert("Bad input file")
                                }
                                else {
                                    self.options.data.dynamic.state = data.studentState;
                                    var id = self.options.data.map[data.items[0].id];
                                    data = id;
                                }
                            }
                            self.options.data.dynamic.current = self.options.data.dynamic.history.length;
                            if (data.length) {
                                $.merge(self.options.data.dynamic.history, data);
                            }
                            else {
                                self.options.data.dynamic.history.push(data);
                            }
                            self.options.data.dynamic.access = new Date().valueOf();
                            self.options.element.triggerHandler('nextselected');
                        }

                    });
                }
            }
        },

        _previous: function () {

            if (this.options.data.dynamic.current > 0) {
                this.options.data.dynamic.current--;
                this.options.element.triggerHandler('previousselected');
            }
            else {
                this._atstart();
            }
        },

        _atstart: function () {
            if (true) { // confirm('You have reached the beginning of the collection: ' + this.options.data.dynamic.collection + '.\n\nIf you continue you might not be able to return to these items.\n\nAre you sure you want to proceed?')) {
                if (this.options.eventSink) {
                    if (this.options.eventSink.triggerHandler('atstart', this)) {
                        this._trigger('atstart');
                    }
                }
            }
        },

        _atend: function () {
            // if (true) { //confirm('You have reached the end of the collection: ' + this.options.data.dynamic.collection + '.\n\nIf you continue you might not be able to return to these items.\n\nAre you sure you want to proceed?')) {
            if (this.options.eventSink) {
                if (this.options.eventSink.triggerHandler('atend', this)) {
                    this._trigger('atend');
                }
            }
        },

        _assert: function (condition, message) {
            if (!condition) {
                message = message || "Assertion failed";
                if (typeof Error !== "undefined") {
                    throw new Error(message);
                }
                throw message; // Fallback
            }
        },

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Pub iface
        load: function (data) {
            this._load(data);
        },

        select: function (criteria) {
            if (this.options.engine) {
                this.options.engine.engine("select", criteria);
            }
            else {
                var fname = "_" + criteria;
                if (this[fname] && typeof this[fname] === "function") {
                    this[fname]();
                }
            }
        },

        fireselectionchanged: function () {
            if (this.options.data) {
                if (this.options.engine) {
                    this.options.engine.engine("fireselectionchanged");
                }
                else {
                    var currentSelection = {};
                    currentSelection[this.options.data.dynamic.collection] = this.options.data.dynamic.history[this.options.data.dynamic.current];
                    this._trigger('selectionchanged', null, currentSelection);
                }
            }
        },

        destroy: function () {
            if ($.Widget) {
                $.Widget.prototype.destroy.apply(this, arguments);
            }
            else {
                console.log('$.Widget is UNDEFINED!!! How is this possible!!!!');
            }
        },

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Async service call
        _asyncNext: function (handler) {

            var self = this;
            var load = { pool: [], history: [] };
            load.state = this.options.data[this.options.data.dynamic.collection].state;
            if (this.options.data.url) {
                load.url = this.options.data.url
            };

            $.each(this.options.data[this.options.data.dynamic.collection], function (index, item) {
                //
                // This is a collection of all entities in the pool, not the entites that haven't been selected. Entities that have been selected are in the history collection, so you can do you own delta.
                // {
                //     id: oatGenerateId, // This is a whatever Id - Id probably used by TAO
                //     pid: pearsonId, // This is pearson generated Id if I have it
                //     index: entityIndex, // index in the pool -  entity's serial number, zero based.
                //     scoring: scoringInfo // If anything
                // }
                //
                load.pool.push({
                    id: null,
                    pid: null,
                    index: index,
                    scoring: {}
                });
            });
            $.each(this.options.data.dynamic.history, function (index, item) {
                //
                // In history we keep the index - matching the index above, being the fastest way to access the entity, and the status (boolean for now) that tells us if we have already uploaded scores.
                // This will work for the linear tests, where you cannot go back and change the answer (thus triggerring scoring again).
                // {
                //     index: entityIndex,
                //     score: scoreIntegerValue,
                //     sent: booleanIndicator
                // }
                //
                var entity = self.options.data[self.options.data.dynamic.collection][item];
                var state = self.options.data.dynamic.state !== null ? self.options.data.dynamic.state : false;
                load.history.push({
                    index: item, // This is correct, take the item (item index from the history) not the index (index to the histry buffer).
                    pid: null, // Since adcm will not use our pool
                    score: $.UTILITY.isItemAnswered(entity) ? $.UTILITY.getItemScore(entity) : 0, //entity.dynamic.score && entity.dynamic.score.value ? 1 : 0,
                    //sent: entity.dynamic.score && typeof entity.dynamic.score.sent !== "undefined" ? entity.dynamic.score.sent : false,
                    entity: entity, // For convenience
                    studentState: state
                });
            });

            ////////////////////////////////////////////////////////////////////////////////////////////
            // All algorithms are implemented with the same footprint:
            // 1. All take one argument object - load
            // 2. All return an array of indices of items. If the selection is done, null is returned.
            //    Typically only one index is returned so I will be sensitive to the cardinality of the result.
            //
            // Synchronous handler invocation - we need a criteria to invoke it asynchronously. We also need to know the url of the service.
            if (this.options.data.dynamic.selectionAlgorithm && this.options.data.dynamic.selectionAlgorithm.length) {
                if (this.options.data.dynamic.async) {
                    var func = window[this.options.data.dynamic.selectionAlgorithm];
                    if (typeof func === "undefined") {
                        func = $.DCM[this.options.data.dynamic.selectionAlgorithm]
                        func(load, handler);
                    }
                    else {
                        func(load, handler);
                    }
                    
                }
                else {
                    handler(window[this.options.data.dynamic.selectionAlgorithm](load));
                }
            }
        }
    });
})(jQuery);
