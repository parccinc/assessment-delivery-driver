(function ($) {
    $.widget("shureco.selectionenginefactory", {

        options: {
            engine: null
        },

        _create: function () {
        },

        _load: function (data) {

            var self = this;
            this.options.data = data; // avoids deep copy through the options, keeps global scope.
            if (typeof this.options.data.dynamic.ase === "undefined") {
                this.options.data.dynamic.ase = {};
            }
            if (typeof this.options.data.dynamic.ase.history === "undefined") {
                this.options.data.dynamic.ase.history = [];
            }
            if (typeof this.options.data.dynamic.ase.unique === "undefined") {
                this.options.data.dynamic.ase.unique = [];
            }
            if (typeof this.options.data.dynamic.ase.atBeginning === "undefined") {
                this.options.data.dynamic.ase.atBeginning = true;
            }
            if (typeof this.options.data.dynamic.ase.endIndex === "undefined") {
                this.options.data.dynamic.ase.endIndex = -1;
            }
            if (typeof this.options.data.dynamic.ase.done === "undefined") {
                this.options.data.dynamic.ase.done = false;
            }

            this.element.on('atend atstart', function (event, param) {
                return self._trigger('boundary', null, { type: event.type, source: param.options.data.dynamic.type, collection: param.options.data.dynamic.collection, selectionAlgorithm: param.options.data.dynamic.selectionAlgorithm });
            });

            try {
                var classSelector = ".td-" + this.options.data.dynamic.collection + "-engine";
                this.options.engine = $(classSelector).engine({

                    eventSink: this.element,

                    ready: function (event, readyParam) {
                        self.options.engine.engine('load', self.options.data); // Loads top level engine
                    },

                    atstart: function () {
                        console.log('Test Package rewound, you must restart to continue!');
                        self.options.data.dynamic.ase.atBeginning = true; // Not necessary, since we prevent it.
                    },

                    atend: function () {
                        console.log('Test Package Completed!');
                        self.options.data.dynamic.ase.done = true;
                        self.options.data.dynamic.ase.endIndex = self.options.data.dynamic.ase.history.length - 1;
                        self._trigger('navupdate', null, { atBeginning: self.options.data.dynamic.ase.atBeginning, done: self.options.data.dynamic.ase.done });
                    },

                    selectionchanged: function (event, selector) {
                        console.log(selector);
                        if (self._valid(selector)) {
                            self.options.data.dynamic.ase.history.push(selector);
                            self._unique(selector);
                            self._highlight(selector);

                            self.options.data.dynamic.ase.atBeginning = self._comparer(selector, self.options.data.dynamic.ase.history[0]) == 0;

                            self._trigger('navupdate', null, { atBeginning: self.options.data.dynamic.ase.atBeginning, done: self.options.data.dynamic.ase.done });
                        }
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        },

        _valid: function (selector) {
            var valid = true;
            $.each(this.options.collectionType, function (index, val) {
                valid = valid && typeof selector[val.name] != "undefined";
            });
            return valid;
        },

        _comparer: function (selector1, selector2) {
            for (var i = 0; i < this.options.collectionType.length; i++) {
                var name = this.options.collectionType[i].name;
                if (selector1[name] < selector2[name]) {
                    return -1;
                }
                else if (selector1[name] > selector2[name]) {
                    return 1;
                }
            }
            return 0;
        },

        _unique: function (selector) {

            for (var i = 0; i < this.options.data.dynamic.ase.unique.length; i++) {
                if (this._comparer(this.options.data.dynamic.ase.unique[i].selector, selector) == 0) {
                    this.options.data.dynamic.ase.unique[i].history.push(this.options.data.dynamic.ase.history.length);
                    return i;
                }
            }
            this.options.data.dynamic.ase.unique.push({ selector: selector, history: [this.options.data.dynamic.ase.history.length] });
            return this.options.data.dynamic.ase.unique.length - 1;
        },

        _findUnique: function (selector) {
            for (var i = 0; i < this.options.data.dynamic.ase.unique.length; i++) {
                if (this._comparer(this.options.data.dynamic.ase.unique[i].selector, selector) == 0) {
                    return i;
                }
            }
            return -1;
        },

        _highlight: function (selector) {
            var nodeId = "0";

            $.each(this.options.collectionType, function (index, val) {
                nodeId += "-" + selector[val.name];
            });

            this.element.treetable("reveal", nodeId);
            // this.element.treetable("expandAll");
            // Highlight revealed row
            this.element.find("tr.current").removeClass("current").addClass("visited");
            var current = this.element.find("tr[data-tt-id='" + nodeId + "']").addClass("current");
            // var orderColumn = current.find("td.order").text(this._findUnique(selector) + 1); // this.options.data.dynamic.ase.unique[this._findUnique(selector)].history.join() 
            var orderColumn = current.find("span.spp-order").text(this._findUnique(selector) + 1);
        },

        /////////////////////////////////////////////////////////////////////////////////////////////
        // Public API

        load: function (data) {
            this._load(data);
        },

        current: function () {
            return this.options.data.dynamic.ase.history.length ? this.options.data.dynamic.ase.history[this.options.data.dynamic.ase.history.length - 1] : null;
        },

        predecessor: function () {
            var c = this.current();
            if (c) {
                var pre = this._findUnique(c);
                if (pre > 0) {
                    return this.options.data.dynamic.ase.unique[pre - 1];
                }
            }
            return null;
        },

        successor: function() {
            var c = this.current();
            if (c) {
                var post = this._findUnique(c);
                if (post < this.options.data.dynamic.ase.unique.length - 1) {
                    return this.options.data.dynamic.ase.unique[post + 1];
                }
            }
            return null;
        },

        atBeginning: function () {
            return this.options.data.dynamic.ase.atBeginning;
        },

        done: function () {
            return this.options.data.dynamic.ase.done;
        },

        getOrder: function (selector) {
            return this._findUnique(selector) + 1;
        },

        next: function () {
            if (this.options.engine) {
                this.options.engine.engine("select", "next");
            }
        },

        previous: function () {
            if (this.options.engine) {
                this.options.engine.engine("select", "previous");
            }
        }
    });
})(jQuery);
