(function ($) {
    $.DCM = {

        state: null,

        adcm: function (load, handler) {
            var state = "null";
            if (load.history.length) {
                state = '{ "slot":' + (load.history.length - 1) + ', "scores":[' + load.history[load.history.length - 1].score + ']' + ', "studentState":' + JSON.stringify(load.history[load.history.length - 1].studentState) + '}';
            }
            var request = $.ajax({
                cache: false,
                data: state,
                contentType: 'application/json',
                dataType: 'json',
                // method: 'POST',
                type: 'POST',
                url: load.url
                //url: 'https://int-dcm-parcc-ads.breaktech.org/items/95'    
            })
            .done(function (data, textStatus, jqXHR) {
                console.log('Success:', data)
                // handler(data);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.log('Failed:', errorThrown);
                // throw new Error(errorThrown);
            });

            request.done(function (data) {
                handler(data);
            });

            request.fail(function (jqXHR, textStatus, errorThrown) {
            });
        }
    };

})(jQuery);
