function dcm(load) {
    if (load.pool && load.pool.length) {
        var start = load.history ? load.history.length : 0;
        if (start < load.pool.length) {
            var size = Math.floor(Math.random() * 4) + 1; // Up to 4 items selected
            var selection = [];
            for (var index = 0; index < size && index + start < load.pool.length; index++) {
                selection.push(index + start);
            }
            return selection;
        }
    }
    return null;
}
