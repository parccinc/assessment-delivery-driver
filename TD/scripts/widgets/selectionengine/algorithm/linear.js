function linear(load) {
    if (load.pool && load.history && load.pool.length > load.history.length) {
        return load.history.length;
    }
    else if (load.pool && !load.history) {
        return 0;
    }
    return null;
}
