function random(load) {
    if (load.pool && load.pool.length) {
        var choices = [];
        for (var i = 0; i < load.pool.length; i++) {
            choices.push(false);
        }
        if (load.history) {
            for (var i = 0; i < load.history.length; i++) {
                choices[load.history[i]] = true;
            }
        }

        var retries = Math.floor(Math.random() * choices.length);
        for (var i = 0; i < retries; i++) {
            var choice = Math.floor(Math.random() * choices.length);
            if (!choices[choice]) {
                return choice;
            }
        }
        // Make sure at least one will be selected
        if (!load.history) {
            while (true) {
                var choice = Math.floor(Math.random() * chices.length);
                if (!choices[choice]) {
                    return choice;
                }
            }
        }
    }
    return null;
}
