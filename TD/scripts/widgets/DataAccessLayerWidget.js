(function ($) {
    $.widget("td.dal", $.td.formbase, {

        _create: function () {

            var self = this;
            var markup = '\
                <div class="td-dal">\
                </div>';

            this.options.container = $(markup).appendTo(this.element);

        },

        getsystemtags: function (selector) {
            try{
                return this._getsystemtags(selector);
            }
            catch (error) {
                alert("dal.getsystemtags() failed with: " + error);
            }
        },

        gettimeonitemsintest: function(){
            try {
                return this._timeoneachitemintest();
            }
            catch (error) {
                return console.log("dal.gettimeonitemsintest() failed with: " + error);
            }
        },

        _getsystemtags: function (selector) {
            return {
                FIRST_NAME: this.global.settings.candidate.firstName,
                LAST_NAME: this.global.settings.candidate.lastName,
                GRADE: this.global.settings.candidate.grade,
                SCHOOL: this.global.settings.candidate.school,
                TEST_NAME: this.global.settings.test.name,
                TEST_PART_NAME: this.global.data.testPart[selector.test].identifier,
                SECTION_NAME: this.global.data.testPart[selector.test].assessmentSection[selector.section].title,
                SECTION_RAW_SCORE: this._sectionrawscore(selector.test, selector.section),
                TEST_PART_RAW_SCORE: this._testrawscore(selector.test),
                SECTION_UNANSWERED_ITEMS: this._unansweredinsection(selector.test, selector.section),
                TEST_PART_UNANSWERED_ITEMS: this._totalunansweredintest(selector.test),
                SECTION_BOOKMARKED_ITEMS: this._bookmarkedinsection(selector.test, selector.section),
                TEST_PART_BOOKMARKED_ITEMS: this._bookmarkedintest(selector.test),
                SECTION_TOTAL_ITEMS: this._totalitemsinsection(selector.test, selector.section),
                TEST_PART_TOTAL_ITEMS: this._totalitemsintest(selector.test),
                timeOnItem: $.UTILITY.timeSpentOnItem(this.global.data.testPart[selector.test].assessmentSection[selector.section].assessmentItem[selector.item])
            }
        },

        _totalitemsintest: function (tp) {
            var testPart = this.global.data.testPart[tp];
            var totalItems = 0;
            if (typeof testPart.navigationMode === "undefined" ||
                (testPart.navigationMode && testPart.navigationMode.indexOf("linear") >= 0)) {
                for (var i = 0; i < testPart.assessmentSection.length; i++) {
                    totalItems += this._totalitemsinsection(tp, i);
                };
            };
            return totalItems;
        },

        _totalitemsinsection: function (tp, sec) {
            var section = this.global.data.testPart[tp].assessmentSection[sec];
            var sectionItems = 0;
            for (var i = 0; i < section.assessmentItem.length; i++) {
                var item = section.assessmentItem[i];
                if (!$.UTILITY.isSystemItem(item)) {
                    sectionItems++;
                };
            };
            return sectionItems;
        },

        _unansweredinsection: function (tp, sec) {
            var section = this.global.data.testPart[tp].assessmentSection[sec];
            var sectionUnanswered = 0;
            for (var i = 0; i < section.dynamic.history.length; i++) {
                var item = section.assessmentItem[section.dynamic.history[i]];
                if (!$.UTILITY.isSystemItem(item) && !$.UTILITY.isItemAnswered(item)) {
                    sectionUnanswered++;
                };
            };
            return sectionUnanswered;
        },

        _totalunansweredintest: function (tp) {
            var testPart = this.global.data.testPart[tp];
            var testPartUnanswered = 0;
            for (var i = 0; i < testPart.dynamic.history.length; i++) {
                testPartUnanswered += this._unansweredinsection(tp, i);
            };
            return testPartUnanswered;
        },

        _sectionrawscore: function (tp, sec) {
            var section = this.global.data.testPart[tp].assessmentSection[sec];
            sectionScore = 0;
            for (var i = 0; i < section.dynamic.history.length; i++) {
                var item = section.assessmentItem[section.dynamic.history[i]];
                if (!$.UTILITY.isSystemItem(item) && $.UTILITY.isItemAnswered(item) && !$.UTILITY.isNotScored(item)) {
                    sectionScore += $.UTILITY.getItemScore(item)
                }
            };
            return sectionScore;
        },

        _testrawscore: function (tp) {
            var testPart = this.global.data.testPart[tp];
            var testPartScore = 0;
            for (var i = 0; i < testPart.dynamic.history.length; i++) {
                testPartScore += this._sectionrawscore(tp, i);
            };
            return testPartScore;
        },

        _bookmarkedinsection: function (tp, sec) {
            var section = this.global.data.testPart[tp].assessmentSection[sec];
            sectionBookmark = 0;
            for (var i = 0; i < section.dynamic.history.length; i++) {
                var item = section.assessmentItem[section.dynamic.history[i]];
                if (!$.UTILITY.isSystemItem(item) && item.dynamic.flagged) {
                    sectionBookmark++;
                }
            };
            return sectionBookmark;
        },

        _bookmarkedintest: function (tp) {
            var testPart = this.global.data.testPart[tp];
            var testBookmark = 0;
            for (var i = 0; i < testPart.dynamic.history.length; i++) {
                testBookmark += this._bookmarkedinsection(tp, i);
            };
            return testBookmark;
        },

        questioncounter: function (cc) {

            var self = this;
            var testLinear = false;
            var currentQuestion = 0;
            var tallyText;
            var testName = this.global.data.testPart[cc.test].assessmentSection[cc.section].title ? this.global.data.testPart[cc.test].assessmentSection[cc.section].title : "title does not exist.";
            $.each(this.global.data.testPart[cc.test].assessmentSection, function (currentSection, section) {
                $.each(section.assessmentItem, function (currentItem, item) {
                    if (!$.UTILITY.isSystemItem(item)) {
                        if (currentSection < self.global.data.testPart[cc.test].dynamic.current ||
                           (currentSection == self.global.data.testPart[cc.test].dynamic.current && currentItem <= section.dynamic.current)) {
                            currentQuestion++;
                        }
                    }
                });
            });
            
            if (this.global.settings.test.itemSelection == "Adaptive") { //ADS-1255
                tallyText = 'Question: ' + currentQuestion;
            }
            else {               
               tallyText = 'Question: ' + currentQuestion + " of " + this._totalitemsintest(cc.test);
            }
            return { name: testName, text:tallyText };
        }
    });
})(jQuery);
