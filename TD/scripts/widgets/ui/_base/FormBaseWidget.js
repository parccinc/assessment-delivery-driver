(function ($) {
    $.widget("td.formbase", {

        options: {
        },

        global: {
        },

        _create: function () {
        },

        _setFocus: function () {
            if (this.options.focusSelector) {
                this.options.container.find(this.options.focusSelector).focus();
            }
        },

        _showForm: function () {
            if (!this.options.container.is(':visible')) {
                if (this._show) {
                    this._show(this.options.container, this.options.show);
                }
                else {
                    this.options.container.show();
                    this._setFocus();
                }
            }
        },

        _hideForm: function () {
            if (this.options.container.is(':visible')) {
                if (this._hide) {
                    this._hide(this.options.container, this.options.hide);
                }
                else {
                    this.options.container.hide();
                }
            }
        },

        // Public methods
        open: function (settings) {
            this.global.settings = settings;
            this._showForm();
            this.options.container.triggerHandler("formopen");
        },

        close: function () {
            this._hideForm();
            this.options.container.triggerHandler("formclose");
        }
    });
})(jQuery);