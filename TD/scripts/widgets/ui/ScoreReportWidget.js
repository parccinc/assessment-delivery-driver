﻿(function ($) {
    $.widget("td.scorereport", $.td.formbase, {

        options: {

        },

        _create: function () {
            console.log("In Score Report Widget Create");

            var self = this;
            var markup = 
               '<div id="srOverlay" class="td-overlay" >' +
			   '<div class="td-scorereport-container">' +
                       '<div class="td-scorereport-header-container">' +
                           '<div class="td-scorereport-titlebar" >' +
                               '<h3 class="td-heading" style="margin-top:0; position:absolute;"> Score Report</h3>' +
                               '<div class="closeBtnFrame" tabindex="1"><div class="closeButton"/></div>' +
                           '</div>' +
                           '<div class="td-scorereport-buttonpane" >' +
                               '<input type="button" class="td-button td-button-small td-button-secondary" id="btnShare" style="display:none;" value="Share" tabindex="-1">' +
                               '<input type="button" class="td-button td-button-small td-button-secondary" id="btnPrint" value="Print" tabindex="1">' +
                           '</div>' +
                       '</div>' +
                       '<div id="divScoreReportContent">' +
                       '</div>' +
		       '</div>' +
               '</div>';

            this.options.container = $(markup).appendTo(this.element)
               .on('formopen', function () {
                   self._createScoreReport();
                   if (self.global.settings.test.scoreReport == 'Math Comprehension' || self.global.settings.test.scoreReport == 'Math Fluency') {
                       $("a").removeAttr('href');
                   }
               });

            this.options.container.find(".closeButton")
              .on('click', function () {
                  self.close();
              });

            this.options.container.find(".closeBtnFrame").on('keydown', function (event) {
                if (event.keyCode == 13 || event.keyCode == 32) {
                    self.close();
                }
            });

            this.options.container.find("#btnPrint").on('click', function (event) {
                window.print();
            });

        },

        _createScoreReport: function () {
            this.options.container.find("#divScoreReportContent").html($.UTILITY.base64toUtf8(this.global.data.dynamic.html));
        }
    });
})(jQuery);
