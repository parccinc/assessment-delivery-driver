(function ($) {
    $.widget("td.logo", $.td.formbase, {

	    options: {
	        src: "images/logo1.png",
	        height: 128, // height in pixels
	        link: false,
            centered: false
	    },

	    _create: function () {

		    console.log("In LogoWidget._create();");

		    // Create container element
		    this.options.container = $("<div class='td-logo unselectable'/>")
                .appendTo(this.element)
                .css({ /*height: this.options.height + "px"*/ });

		    if (this.options.link) {
		        this.options.container
                    .on('click', function () {
                        window.location.href = "http://www.parcconline.org/about-parcc";
                    })
                    .css({ cursor: "pointer" });
		    }
		    if (this.options.centered) {
		        this.options.container
                    .css({ float: "none", clear: "both", margin: "0 auto" });
		    }
	    }
    });
})(jQuery);
