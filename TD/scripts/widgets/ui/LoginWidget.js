(function ($) {
    $.widget("td.login", $.td.formbase, {

	    options: {
	        defaultUrl: '/samples/tests/settingsBad.json', // '/local/nonexistingsettings.json'
	    },

	    global: {
	    },
      
	    _create: function () {

		    console.log("In LoginWidget._create();");

		    var self = this;
		    this.options.container = $("<div class='td-login'/>").appendTo(this.element);

		    if (this._on) {
                // for jQuery >= 1.9
			    //this._on(this.element, {
			    //	"login.td": function (event) {
			    //	}
			    //});
		    }
		    else {
		        this.options.container
                    .on('formopen', function () {
                        self.options.sticky.stickyheader("open", self.global.settings);
                        self._setEnvironment();
                        self._checkConfiguration();
                    })
                    .on('welcomedone.td', function () {
                        self.options.welcome.welcome("close");
                        self.options.confirmStudent.confirmstudent("open", self.global.settings);
                    })
                    .on('confirmstudentdone.td', function (event) {
                        self.options.confirmStudent.confirmstudent("close");
                        self.options.confirmTest.confirmtest("open", self.global.settings);
                    })
                    .on('confirmtestdone.td', function (event) {
                        self.options.confirmTest.confirmtest("close");
                        self._trigger("logindone");
                    });
		    }

		    // Create widgets that use container
		    this.options.sticky = this.options.container.stickyheader({
		        logo: true,
		        height: this.options.headerHeight
		    });
		    
		    this.options.welcome = this.options.container.welcome({
                headerHeight: this.options.headerHeight,
		        credobtained: function (event, data) {
				    self.options.container.trigger("authenticate", { type: "credentials" });
		        },
		        authsuccess: function (event, obj) {
		            self._authorize(obj, true);
		        }
		    });
		    this.options.confirmStudent = this.options.container.confirmstudent({
		        csyes: function () {
		            self.options.container.trigger("confirmstudentdone");
		        }
		    });
		    this.options.confirmTest = this.options.container.confirmtest({
		        ctyes: function () {
		            self.options.container.trigger("confirmtestdone");
		        }
		    });
	    },

	    _setEnvironment: function (settings) {
		    this.global.settings.environment = {
			    seb: null,
			    practice: null,
			    regular: null
		    };
		    this.global.settings.credentials = {
		        username: "",
		        password: "",
                secret: "",
		        testkey: "",
		        dob: "",
		        id: "",
		        state: ""
		    };
	    },

	    _authorize: function (obj, authorization, jsonStrReason) {
	        if (obj.type === "seb") {
	            this.global.settings.environment.seb = authorization;
	        }
	        else if (obj.type === "practice") {
	            this.global.settings.environment.practice = authorization;
	        }
	        else if (obj.type === "credentials") {
	            this.global.settings.environment.regular = authorization;
	        }
	        this._dumpAuthorizationSettings();

	        if (authorization) {
	            // OK, logged in successfully. If obj.type is seb or credentials we need to go and confirm info, otherwise, for parctice, go directly to test download and start test.
	            if (obj.type === "seb" || obj.type === "credentials") {
	                this.options.container.trigger("welcomedone");
	            }
	            else {
	                this._trigger("logindone");
	                $("#cover").css({ "display": "none" });
	            }
	        }
	        else {
	            // Login failed. If obj.type is seb we just ignore the error and continue. If obj.type is practice we display practice error, if credentials - credentials error in the notification area.
	            //if (obj.type != "seb") {
	            //    this._errorMessage(jsonStrReason);
	            //}
	            this._checkConfiguration();
	        }
	    },

	    _dumpAuthorizationSettings: function () {
	        console.log('this.global.settings.environment.seb: ' + this.global.settings.environment.seb);
	        console.log('this.global.settings.environment.practice: ' + this.global.settings.environment.practice);
	        console.log('this.global.settings.environment.regular: ' + this.global.settings.environment.regular);
	        console.log('this.global.settings.credentials.testkey: ' + this.global.settings.credentials.testkey);
	        console.log('this.global.settings.credentials.dob: ' + this.global.settings.credentials.dob);
	        console.log('this.global.settings.credentials.id: ' + this.global.settings.credentials.id);
	        console.log('this.global.settings.credentials.state: ' + this.global.settings.credentials.state);
	    },

	    _checkConfiguration: function() {
		    // Disable SEB check for now.
	        //if (this._checkedInsideSEB()) {
	            if (this._checkedPracticeTest()) {
	                this._getCredentials();
	            }
	        //}
	    },

	    _checkedInsideSEB: function () {
	        if (this.global.settings.environment.seb == null) {
	            this.options.container.trigger("authenticate", { type: "seb" });
	            return false;
		    }
		    return true;
	    },

	    _checkedPracticeTest: function () {
	        var self = this;
	        if (self.global.settings.environment.practice == null) {
	            if (self._parseQueryForPracticeTestKey()) {
	                //self.options.container.trigger("authenticate", { type: "practice" });
	                //return false;
	                return true;
	            }
	            else {
	                self.global.settings.environment.practice = false;
	            }
	        }
	        return true;
	    },

	    _parseQueryForPracticeTestKey: function () {
	        //var uri = location.search;
	        //var queryString = {};
	        //uri.replace(
            //    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
            //    function ($0, $1, $2, $3) { queryString[$1] = $3; }
            //);
	        //if (typeof queryString["key"] === "undefined") {
	        //    this.global.settings.environment.practice = false;
            //}
	        //else {
	        //    this.global.settings.credentials.testkey = queryString["key"];
	        //    this.global.settings.environment.practice = true;
	        //}

	        if ($Global$.testKey) {
	            this.global.settings.credentials.testkey = $Global$.testKey;
	            this.global.settings.credentials.state = Object.keys($Global$.login)[0];
	            this.global.settings.environment.practice = true;
	        }
	        else {
	            this.global.settings.environment.practice = false;
	        }
	        return this.global.settings.environment.practice;
	    },

	    _getCredentials: function () {
		    this.options.welcome.welcome("open", this.global.settings);
	    }
    });
})(jQuery);

