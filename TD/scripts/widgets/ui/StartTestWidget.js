(function ($) {
    $.widget("td.starttest", $.td.formbase, {

	    _create: function () {

		    console.log("In StartTestWidget._create();");

		    var self = this;
		    this.options.container = $('<div class="td-start-test" style="background-color:rgba(255,0,0,0); border:thin none black;"/>').appendTo(this.element);

	        // Create widgets that use container
		    this.options.sticky = this.options.container.stickyheader({
		        name: true,
		        exit: {
		            show: true,
                    save: false
		        },
		        separator: true,
		        height: this.options.headerHeight,
		        justexit: function (e) {
		            if (self.global.settings.config.enablePause) {
		                self._trigger("justexit");
		            }
		            else {
		                self._trigger("justexit", e, "justexit");
		            }
		        }
    	    });
		    this.options.notifications = this.options.container.notifications({
		        yoffset: this.options.headerHeight
		    });

		    this.options.uploader = this.options.container.datacomm({
		        uploadfailed: function (evt, errResponse) {
		            self._processErrorMessage(errResponse, "start");
		        },
		        uploaddone: function (evt, data) {
		            if (data.status !== "InProgress") {
		                self._processErrorMessage({ errorCode: "TD-1001", errorDesc: "", purpose: "start" }, "start");
		            }
		            else {
		                self._trigger("starttest");
		            }
		        }
		    });

		    this.options.dlg = this.options.container.find("#gpd");
		    this.options.dlg.notificationdialog({		        
		        proceed: function () {
		            self.options.info.testinfo("open", self.global.settings);
		        },
		        start: function () {
		            self.options.uploader.datacomm("upload", "start");
		        }
		    });

		    this.options.info = this.options.container.testinfo({
		        starttest: function () {
		            self._trigger('starttest'); // just forward
		        },
		        downloadfailed: function (evt, errResponse) {
		            if (errResponse && errResponse.errorCode) {
		                switch (errResponse.errorCode) {
		                    case "ADP-1306":
		                    case "ADP-1459":
		                    case "ADP-1460":
		                    case "ADP-1001":
		                    case "ADP-1003":
		                    case "ADP-1004":
		                        self._closeErrorMessage(errResponse);
		                        break;
		                    default:
		                        self._processErrorMessage(errResponse, "proceed");
		                        break;
		                }
		            }
		            else {
		                self._processErrorMessage(errResponse, "proceed");
		            }
		        },
		        uploadfailed: function (evt, errResponse) {
		            if (errResponse && errResponse.errorCode) {
		                switch (errResponse.errorCode) {
		                    case "ADP-1306":
		                    case "ADP-1459":
		                    case "ADP-1460":
		                    case "ADP-1001":
		                    case "ADP-1003":
		                    case "ADP-1004":
		                        self._closeErrorMessage(errResponse);
		                        break;
		                    default:
		                        self._processErrorMessage(errResponse, "start");
		                        break;
		                }
		            }
		            else {
		                self._processErrorMessage(errResponse, "start");
		            }
		        }
		    });

		    if (this._on) {
		        // for jQuery >= 1.9
		    }
		    else {
		        this.options.container
                    .on('formopen', function () {
                        self.options.sticky.stickyheader("open", self.global.settings);
                        self.options.info.testinfo("open", self.global.settings);
                    });
            }
	    },

	    _processErrorMessage: function (errResponse, action) {
	        var self = this;

	        var header = "There was a problem getting your test because the server cannot be reached. <br/>Please ask your teacher for help.";
	        var msgbody = "Click Retry to try to get your test again or Log Out to exit your test session.";
	        if (errResponse && errResponse.purpose == "start") {
	            msgbody = "Click Retry to try to start your test again, or Log Out to exit your test session."
	            if (errResponse.errorCode !== 0) {
	                header = "There was a problem starting your test. [" + errResponse.errorCode + "]<br/>Please ask your teacher for help.";
	            }
	            else {
	                header = "There was a problem starting your test because the server cannot be reached. [TD-1000]<br/>Please ask your teacher for help.";
	            }
	        }
	        else {
	            if (errResponse && errResponse.errorCode !== 0) {
	                header = "There was a problem getting your test. [" + errResponse.errorCode + "] <br/>Please ask your teacher for help.";
	            }
	        }
	        self.options.dlg.notificationdialog("dlgmsg", {
	            showClose: false,
	            errorMessage:true,
	            title: "Problem Occurred",
	            dialogWidth: 550,
	            dialogHeight: 255,
	            dlgContent: [
                      {                         
                          heading: header,
                          body: [msgbody],
                          buttons: [
                               {
                                   name: "Log Out",
                                   class: " left",
                                   action: {
                                       type: "notify-close",
                                       value: "justexit"
                                   }
                               },
                              {
                                  name: 'Retry',
                                  class: " right",
                                  action: {
                                      type: "notify-close",
                                      value: action
                                  }
                              }
                          ]
                      }],
	        });
	    },

        _closeErrorMessage:function(errResponse){
            var self = this;
            var dwidth = undefined;
            var dheight = undefined;
            var header = "Your test session has expired. [" + errResponse.errorCode + "]<br/>Please ask your teacher for help.";
            if (errResponse.errorCode == "ADP-1001" || errResponse.errorCode=="ADP-1003" || errResponse.errorCode=="ADP-1004") {
                header = "The system is currently undergoing maintenance. [" + errResponse.errorCode + "]<br/>We are sorry for the inconvenience. Please ask your teacher for help.";
                dwidth = 490;
                dheight = 240;
            }
            self.options.dlg.notificationdialog("dlgmsg", {
                showClose: false,
                errorMessage: true,
                title: "Problem Occurred",
                dialogWidth: dwidth,
                dialogHeight: dheight,
                dlgContent: [
                      {
                          heading: header,
                          body: ["Click Close to close this message."],
                          buttons: [
                               {
                                   name: "Close",
                                   class: " right",
                                   action: {
                                       type: "notify-close",
                                       value: "justexit"
                                   }
                               }
                          ]
                      }]
            });
        }
    });
})(jQuery);
