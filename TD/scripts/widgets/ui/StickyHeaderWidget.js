(function ($) {
    $.widget("td.stickyheader", $.td.formbase, {

	    options: {
	        logo: false,
	        name: false,
	        exit: {
	            show: false,
	            save: false
	        },
	        timer: false,
	        tally: false,
	        review: false,
            flag: false,
            height: 78,
            separator: false,
            dark: true
	    },

	    global: {
	    },

	    _create: function () {

		    console.log("In StickyHeaderWidget._create();");

		    var self = this;
		    var cls = 'td-sticky-header-wrapper unselectable';
		    if (this.options.dark) {
		        cls += ' td-header-dark';
		    }
		    else {
		        cls += ' td-header-light';
		    }
		    this.options.container = $("<div class='" + cls + "'><div class='td-sticky-header'/></div>").appendTo(this.element).height(this.options.height);
            this.options.content = this.options.container.find(".td-sticky-header");

		    // Create widgets that use container
	        this.options.logoWidget = this.options.content.logo({height: this.options.height});
	        this.options.candidateInfoWidget = this.options.content.candidateinfo({});
	        this.options.saveExitWidget = this.options.content.saveexit({
                save: self.options.exit.save,
	            saveandexit: function () {
	                self._trigger("saveandexit");
	            },
	            justexit: function () {
	                self._trigger("justexit");
	            }
	        });
	        this.options.clockWidget = this.options.content.clock({});
	        this.options.tallyWidget = this.options.content.tally({});
	        this.options.reviewLinkWidget = null; // this.options.content.reviewlink({});
	        this.options.flagWidget = null;
            //    this.options.content.flag({
	        //    flagged: function () {
	        //        self._trigger("flagged");
	        //    }
	        //});

            // Let not have this one as a control (widget) yet :)
	        if (this.options.separator) {
	            var rgb = this.options.dark ? "52, 71, 89" : "237, 230, 229";
		        $(this.options.container).css({ "border-bottom": "thin solid rgb(" + rgb + ")" });
	        }

	        this.options.container
                .on('formopen', function () {

                    if (self.options.logo) {
                        self.options.logoWidget.logo("open", self.global.settings);
                    }
                    if (self.options.name) {
                        self.options.candidateInfoWidget.candidateinfo("open", self.global.settings);
                    }
                    if (self.options.exit.show) {
                        self.options.saveExitWidget.saveexit("open", self.global.settings);
                    }
                    if (self.options.timer) {
                        self.options.clockWidget.clock("open", self.global.settings);
                    }
                    if (self.options.tally) {
                        self.options.tallyWidget.tally("open", self.global.settings);
                    }
                    if (false) { // self.options.review) {
                        // self.options.reviewLinkWidget.reviewlink("open", self.global.settings);
                    }
                    if (false) { // self.options.flag) {
                        // self.options.flagWidget.flag("open", self.global.settings);
                    }
                });
	    },

	    tallycurrent: function (current) {
	        this.options.tallyWidget.tally("tallycurrent", current);
	    },

	    updatetime: function (hhmm) {
	        this.options.clockWidget.clock("update", hhmm);
	    },

	    updateflag: function (current) {
	        // this.options.flagWidget.flag("update", current);
	    },

	    togglecontrols: function (options) {
            // TODO: Add all controls here, not only these 3
	        // this.options.clockWidget.toggle(options.clock);
	        this.options.tallyWidget.tally("togglecontrol", options.tally);
	        // this.options.reviewLinkWidget.reviewlink("togglecontrol", options.review);
	    }
    });
})(jQuery);
