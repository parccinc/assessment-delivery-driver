(function ($) {
    $.widget("td.tally", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In TallyWidget._create();");

            var self = this;
            this.options.container = $("<div class='td-tally'><div class='td-tally-header'></div><div class='td-tally-body'></div></div>")
                .appendTo(this.element)
                .on('formopen', function () {
                });
            this.options.header = this.options.container.find(".td-tally-header");
            this.options.body = this.options.container.find(".td-tally-body");
        },

        tallycurrent: function (tallydata) { // current) {
            this.options.header.text(tallydata.text);
            this.options.body.text("in " + tallydata.name);
        },

        togglecontrol: function (val) {
            // jQuery > 1.9
            // val ? this._show(this.element, this.options.show) : this._hide(this.element, this.options.hide);
            this.options.container.toggle(val);
        }
    });
})(jQuery);
