(function ($) {
    $.widget("td.notifications", {

        options: {
            close: true,
            expire: true,
            period: 10000,
            exit: false,
            yoffset: 0, // pixel offset from the top
            prepend: false
	    },

        _create: function () {

            var self = this;

            console.log("In NotificationsWidget._create();");

            // Create container element
            var markup =
                "<div class='td-notifications' >" +
                    "<div class='td-notification-area'>" +
		                "<p title=''></p>" +
                        "<div class='td-notification-error-btn-frame' tabindex=1><div class='td-notification-error-btn'/></div>" +
                    "</div>" +
                "</div>";
            this.options.container = this.options.prepend ? $(markup).prependTo(this.element) : $(markup).appendTo(this.element);
            this.options.container
                .on('keypress keydown', function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    if (event.keyCode == 27) {
                        $(this).trigger('closenotification');
                    }
                })
                .on('closenotification.td', function (event) {
                    $(this).find(".td-notification-area").fadeOut(400, function () {
                        self.options.container.hide();
                        $(".td-welcome-content").delay(400)
                            .queue(function (next) {
                                var step = $(".td-notification-area").height();
                                $(this).css('top', 0);
                                next();
                            });
                    });
                })
                .on('focusout', function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).focus();
                })
                .focus();

            this.options.container.find(".td-notification-error-btn-frame")
                .on('click', function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    self.options.container.trigger('closenotification');
                })
                .on('keypress keydown', function(event){
                    if (event.keyCode == 13) {
                        $(this).trigger('closenotification');
                    }
                })
        },

	    _msg: function (type, msg, title, exit, close, expire, period) {

	        var self = this;

            var e = typeof exit === "undefined" ? this.options.exit : exit;
            var c = typeof close === "undefined" ? this.options.close : close;
            var exp = typeof expire === "undefined" ? this.options.expire : expire;
            var p = typeof period === "undefined" ? this.options.period : period;
            if (isNaN(p) || p <= 0 || p > 2000) {
                p = this.options.period;
            }
            var paragraph = this.options.container.find("p")[0];
            var notificationArea = this.options.container.find(".td-notification-area")[0];
            //var closeBtn = this.options.container.find(".td-notification-close-btn");
            this.options.container.show().focus();
                  
            $(paragraph).html(msg);
            if (title && title.length > 0) {

                var t = "Error information is not available";
                var errObject = JSON.parse(title);
                var errObj = errObject.result ? errObject.result : errObject;
                if (errObj && errObj.errorDescription && errObj.errorCode) {
                    t = errObj.errorDescription + "[" + errObj.errorCode + "]";
                }
                $(paragraph).attr('title', t);
            }
            else {
                $(paragraph).removeAttr('title');
            }
            if (e) {
                $("<span id='idExit' class='td-error-link' tabStop='1'>Exit ></span>").appendTo(paragraph);
                $(paragraph).find('#idExit').on('click', function () {
                    if (self.global) {
                        self.global.settings = {};
                        self.global.data = {};
                        self.global.testInfo = {};
                    }
                    window.location.reload();
                    screenfull.exit();
                });
            }
            if (exp) {
                setTimeout($.proxy(this._close, this), p);
            }
            $(notificationArea)
                .removeClass("td-notification-info td-notification-success td-notification-warning td-notification-error")
                .addClass("td-notification-" + type)
                .fadeIn(400);

            var step = $(".td-notification-area").height();
            $(".td-welcome-content").css("top", step);
        },

        _close: function () {
            this.options.container.trigger('closenotification');
        },

        error: function (msg, title, exit, close, expire, period) {
            this._msg("error", msg, title, exit, close, expire, period);
        },

        success: function (msg, title, exit, close, expire, period) {
            this._msg("success", msg, title, exit, close, expire, period);
        },

        warning: function (msg, title, exit, close, expire, period) {
            this._msg("warning", msg, title, exit, close, expire, period);
        },

        info: function (msg, title, exit, close, expire, period) {
            this._msg("info", msg, title, exit, close, expire, period);
        }
    });
})(jQuery);
