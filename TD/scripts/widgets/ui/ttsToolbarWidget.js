﻿(function ($) {
    $.widget("td.ttsToolbar", {

        options: {
        },

        parts: {
            btnPlay: null,
            btnStop: null
        },

        _states: {
            ready: 0,
            playing: 1,
            paused: 2,
            stopped: 0, // synonym for ready
        },

        _state: {
            current: null
        },

        _create: function () {

            var self = this;

            // TODO: Add to options
            this._state.current = this._states.ready;

            // Catch the completed callback
            try {
                if (!gSpeechCompletedCallback) {
                    gSpeechCompletedCallback = $.proxy(this._speechCompleted, this);
                }
            }
            catch (error) {
                if (console && console.log && typeof console.log == "function") {
                    console.log('Failed to register TTS Speech Completed Callback: ' + error.message)
                }
            }


            this.options.container = $("<div class='td-tts'/>")
            .appendTo(this.element);

            // Media buttons
            this.parts.btnPlay = $('<div id="playBtnId" class="media-btn media-play" style=""></div>')
                .appendTo(this.options.container)
                .on('click', $.proxy(this._play, this));
            this.parts.btnStop = $('<div id="stopBtnId" class="media-btn media-stop media-disabled" style=""></div>')
                .appendTo(this.options.container)
                .on('click', $.proxy(this._stop, this));


            //parent.appendTo(this.element);

            // this._setButtons();
        },

        _play: function () {
            var self = this;
         //   $.getScript('../scripts/tts.js', function () {
                //alert('loaded tts');


                if (self._state.current == self._states.ready) {
                    self._state.current = self._states.playing;

                    self._setButtons();

                    try {
                        $rw_speakById('item');
                    }
                    catch (error) {
                        alert('$rw_speakById() failed: ' + error.message);
                    }
                }
                else if (self._state.current == self._states.playing) {
                    self._state.current = self._states.paused;

                    self._setButtons();

                    try {
                        $rw_event_pause();
                    }
                    catch (error) {
                        alert('$rw_event_pause(): ' + error.message);
                    }
                }
                else if (self._state.current == self._states.paused) {
                    self._state.current = self._states.playing;

                    self._setButtons();

                    try {
                        $rw_event_pause();
                    }
                    catch (error) {
                        alert('$rw_event_pause(): ' + error.message);
                    }
                }

                self._setButtons();
           // });
        },

        _stop: function () {

            if (this._state.current == this._states.ready) {
                this._state.current = this._states.stopped; // same as ready, shouldn't even be possible given the state of buttons
            }
            else if (this._state.current == this._states.playing || this._state.current == this._states.paused) {
                this._state.current = this._states.stopped;

                this._setButtons();

                try {
                    $rw_stopSpeech();
                }
                catch (error) {
                    alert('$rw_stopSpeech() failed: ' + error.message);
                }
            }
        },

        _setButtons: function () {

            switch (this._state.current) {

                case this._states.ready:

                    this.parts.btnPlay.removeClass('media-pause media-disabled').addClass('media-play');
                    this.parts.btnStop.addClass('media-disabled');

                    break;

                case this._states.playing:

                    this.parts.btnPlay.removeClass('media-play media-disabled').addClass('media-pause');
                    this.parts.btnStop.removeClass('media-disabled');

                    break;

                case this._states.paused:

                    this.parts.btnPlay.removeClass('media-pause media-disabled').addClass('media-play');
                    this.parts.btnStop.removeClass('media-disabled');

                    break;
            }
        },

        _speechCompleted: function () {
            this._state.current = this._states.ready;
            this._setButtons();
        },

        stop: function () {
            this._stop();
        }
    });
})(jQuery);
