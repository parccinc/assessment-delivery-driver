(function ($) {
    $.widget("td.savesubmit", $.td.formbase, {

        options: {
            save: true,
            logoHeight: 78,
            error: false
        },

        _create: function () {

            console.log("In SaveSubmitWidget._create();");

            var self = this;
            var markup = this.options.save ?
                "<div class='td-save-submit td-scroll'>" +
                   "<h1 id='td-logout-header' class='td-heading'>Your responses have been saved.</h1>" +
                   "<div style='padding:25px 0 40px;'>" +
                       "<div class='td-save-submit-logo' >" +
                           "<div id='td-logo-area' class='td-save-logo'/>" +
                       "</div>" +
                       "<div class='td-body' style='padding-top:20px;'>" +
                           "<div id='td-logout-info'>You can return to your test later.</div>" +
                           "<div id='td-logout-instructions'>Log in again or ask your teacher for help to resume your test.</div>" +
                       "</div>" +
                   "</div>" +
                   "<div >" +
                       "<input type='button' id='idClose' name='close' value='Return to Log In' class='td-button td-button-large td-button-primary' style='font-size:1em;'>" +
                   "</div>" +
               "</div>"
                :
                "<div class='td-save-submit td-scroll'>" +
                   "<h1 class='td-heading'>Your test has been submitted.</h1>" +
                   "<div style='padding:25px 0 40px;'>" +
                       "<div class='td-save-submit-logo' >" +
                           "<div class='td-submit-logo'/>" +
                       "</div>" +
                       "<div class='td-body' style='padding-top:20px;'>" +
                           "<div id='td-logout-instructions'>Return to the log in page or ask your teacher for help.</div>" +
                       "</div>" +
                   "</div>" +
                   "<div >" +
                       "<input type='button' id='idClose' name='close' value='Return to Log In' class='td-button td-button-large td-button-primary' style='font-size:1em;'>" +
                   "</div>" +
               "</div>";

            this.options.stickyheader = this.element.stickyheader({ logo: true, height: 78 });

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0 || ua.indexOf('Edge/') > 0;

            this.options.container = $(markup)
                .appendTo(this.element)
                .on('formopen', function () {
                    //self.options.logoWidget.logo("open", self.global.settings);
                    self.options.stickyheader.stickyheader("open", self.global.settings);
                    
                    if (self.options.error) {
                        var infoText = "You have been logged out because your test session has expired.";
                        if (self.options.purpose) {
                            if (self.options.purpose == "cancel" || self.options.purpose == "maintenance" || self.options.purpose == "justexit") {
                                var td_header = self.options.purpose == "cancel" ? "You have been logged out and your test is canceled" : "You have exited your session.";
                                self.options.container.find("#td-logout-header").html(td_header);
                                self.options.container.find("#td-logo-area").removeClass("td-save-logo").addClass("td-cancel-logo");
                                self.options.container.find(".td-save-submit-logo").addClass("td-cancel-background-color");
                            }
                            else {
                                self.options.container.find("#td-logout-header").html("You have been logged out");
                            }
                            infoText = "";
                            self.options.container.find("#td-logout-instructions").html("Return to the log in page or ask your teacher<br>for help to return to the log in page.");
                        }
                        self.options.container.find("#td-logout-info").html(infoText);
                        if (!self.global.settings.config.enablePause || !self.global.settings.config.enableSubmit) {
                            self.options.container.find("#td-logout-instructions").html("Please close your browser.")
                            $("#idClose").hide();
                        };
                    }
                    if (msie) {
                        self.options.container.find("#idClose").val("Close");
                        self.options.container.find("#td-logout-instructions").html("Please close your browser.")
                    }
                });

            this.options.container.find("#idClose")
                .on('click', function () {
                    if (msie) {
                        window.close();
                    }
                    else {
                        self.global.settings = {};
                        self.global.data = {};
                        self.global.testInfo = {};
                        window.location.reload();
                    }
                });
            
            this.options.dal = this.options.container.dal({});

            timeSpentOnItemsInTest = this.options.dal.dal("gettimeonitemsintest");                        
        }
    });
})(jQuery);
