(function ($) {
    $.widget("td.spinner", $.td.formbase, {

        _create: function () {

            console.log("In SpinnerWidget._create();");

            var self = this;
            var markup =
                '<div class="td-spinner">' +
                    '<div class="td-spinner-img spinner"></div>' +
                '</div>';
            this.options.container = $(markup).appendTo(this.element);

            if (this._on) {
                // for jQuery >= 1.9
            }
            else {
                this.options.container
                    .on('formopen', function () {
                        // self.options.sticky.stickyheader("open", self.global.settings);
                        // self.options.info.testinfo("open", self.global.settings);
                    });
            }
        },

        enterbusy: function () {
            this._showForm();
        },

        leavebusy: function () {
            this._hideForm();
        }
    });
})(jQuery);
