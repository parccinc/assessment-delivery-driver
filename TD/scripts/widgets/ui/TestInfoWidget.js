(function ($) {
    $.widget("td.testinfo", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In TestInfoWidget._create();");

            var self = this;
            var markup = 
                '<div class="td-test-info td-scroll unselectable">' +
                   '<div class="td-test-info-body">' +
                       '<div style="padding:0 16px;">' +
                           '<h1 id="idStartTestName" class="td-heading"/>' +
                           '<div class="td-test-info-summarybody">' +
                               '<div class="td-test-summary">' +
                                   '<div id="idTotalSections" class="td-summary-middle"></div>' +
                                   '<div class="td-summary-center">TOTAL SECTIONS</div>' +
                               '</div>' +
                               '<div class="td-test-summary">' +
                                   '<div id="idTotalQuestions" class="td-summary-middle"></div>' +
                                   '<div class="td-summary-center">TOTAL QUESTIONS</div>' +
                               '</div>' +
                           '</div>' +
                       '</div>' +
                       '<div style="padding:10px 32px 48px;">' +
                           '<h2 class="td-heading">Instructions</h2>' +
                           '<div id="idStartInstructions" class="td-instructions td-body"></div>' +
                       '</div>' +
                       '<button id="idStartTestNow" class="td-button td-button-large td-button-primary" disabled="disabled">Start Test</button>' +
                   '</div>' +
               '</div>';

            this.options.container = $(markup).appendTo(this.element);
            this.options.downloader = this.options.container.datacomm({
                downloaddone: function () {
                    self._setFormDataFromDownload();
                },
                //PARADS-3155
                downloadfailed: function (evt, errResponse) {
                    self._trigger("downloadfailed", null, errResponse);
                },
                uploadfailed: function (evt, errResponse) {
                    self._trigger("uploadfailed", null, errResponse);
                },
                uploaddone: function (evt, data) {
                    if (data.status !== "InProgress") {
                        self._trigger("uploadfailed", null, { errorCode: "TD-1000", errorDesc: "", purpose: "start" });
                    }
                    else {
                        self._trigger("starttest");
                    }
                }
            });
        
            if (this._on) {
                // for jQuery >= 1.9
            }
            else {
                this.options.container
                    .on('formopen', function () {
                        self.options.downloader.datacomm("open", self.global.settings);
                        self._setFormDataFromSettings();
                        self.options.downloader.datacomm("download");
                    })
                    
                this.options.container.find("#idStartTestNow")
                    .on('click', function () {
                        if ($.browser.safari) {
                            console.log("Detected browser: ", $.browser.safari);
                        }
                        else {
                            if (self.global.settings.toolbar.fullscreen) {
                                if (screenfull.enabled && !screenfull.isFullscreen) {
                                    screenfull.request();
                                }
                            };
                        }
                        if (!self.global.settings.config.enablePause && !self.global.settings.config.enableSubmit) {
                            self._trigger("starttest");
                        }
                        else {
                            self.options.downloader.datacomm("upload", "start");
                        }
                    });
            }
        },

        _setFormDataFromSettings: function () {
            if (this.global.settings && this.global.settings.candidate && this.global.settings.candidate.name && this.global.settings.candidate.name.first && this.global.settings.candidate.name.last) {
                this.options.container.find('#idStartCandidateName').text(this.global.settings.candidate.name.first + ' ' + this.global.settings.candidate.name.last);
            }
            if (this.global.settings && this.global.settings.test && this.global.settings.test.name) {
                this.options.container.find('#idStartTestName').text(this.global.settings.test.name);
            }

            // Show instructions
            if (this.global.settings.test.description) {
                this.options.container.find('#idStartInstructions').html(this.global.settings.test.description);
            }

            //Show/hide summary ADS-1255
            if (this.global.settings.test.itemSelection == "Adaptive") {
                this.options.container.find(".td-test-info-summarybody").hide();
            }

        },

        _setFormDataFromDownload: function () {
            this.options.container.find('#idTotalSections').text(this.global.testInfo.testPartCount);
            this.options.container.find('#idTotalQuestions').text(this.global.testInfo.itemCount);
            this.options.container.find('#idStartTestNow').removeProp('disabled');
            if (this.global.testInfo.restart) {
                this.options.container.find('#idStartTestNow').text('Resume Test');
            }
        }
    });
})(jQuery);
