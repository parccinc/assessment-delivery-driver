(function ($) {
    $.widget("td.confirmtest", $.td.formbase, {

        _create: function () {

            console.log("In ConfirmTestWidget._create();");

            var self = this;
            var markup =
                '<div class="td-confirm-test td-scroll"><div id="gpd"/>' +
                    '<form id="idConfirmTestForm" style="text-align:center;">' +
                        '<h1 class="td-confirmation-header td-heading">Is this your test?</h1>' +
                        '<br/>' +
                        '<div class="td-test-background-box">' +
                           '<div style="padding-top:25px;">' +
                               '<div class="td-test-student-img-frame">' +
                                   '<div class="td-confirm-test-img"/>' +
                               '</div>' +
                               '<h2 class="td-confirmation-title td-heading" id="idAssessmentName"></h2>' +
                           '</div>' +
                        '</div>' +
                        '<br/>' +
                        '<div style="margin-top:16px; margin-bottom:15px;">' +
                           '<input id="idNo" type="button" value="No" class="td-button td-button-large td-button-secondary"/>' +
                            '<input id="idYes" type="button" value="Yes" class="td-button td-button-large td-button-primary"/>' +
                        '</div>' +
                    '</form>' +
                '</div>';
            this.options.container = $(markup).appendTo(this.element)
                .on('formopen', function () {
                    self._setValues();
                });
           

            this.options.dlg = this.options.container.find("#gpd");

            this.options.container.find("#idYes")
                .on('click', function () {
                    self._trigger("ctyes");
                });
            this.options.container.find("#idNo")
                .on('click', function () {
                    self.options.dlg.notificationdialog("dlgmsg", {
                        dialogWidth: 550,
                        dialogHeight: 255,
                        title: "Exit",

                        dlgContent: [
                            {
                                heading: "If you are having trouble starting your test, please ask your teacher for help before moving on.",
                                body: [],
                                buttons: [
                                    {
                                        name: "Go Back",
                                        class: " left",
                                        action: {
                                            type: "cancel"
                                        }
                                    },
                                    {
                                        name: "Exit",
                                        class: " right",
                                        action: {
                                            type: "notify-close",
                                            value: "exitTD"
                                        }
                                    }
                                ]
                            }
                        ]
                    });

                });

            this.options.dlg.notificationdialog({
                exitTD: function () {
                    console.log("In Exit TD functionality");
                    if (self.global) {
                        self.global.settings = {};
                        self.global.data = {};
                        self.global.testInfo = {};
                    }
                    window.location.reload();
                    screenfull.exit();
                }

            });
        },

        _setValues: function () {
            this.options.container.find("#idAssessmentName").text(this.global.settings.test.name);
        }
    });
})(jQuery);
