(function ($) {
    $.widget("td.saveexit", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In SaveExitWidget._create();");

            var self = this;

            var markup = "<div class='td-save-exit'><div id='gpd'/></div>";
            this.options.container = $(markup)
                .appendTo(this.element)
                .on('formopen', function () {
                    if (!self.options.save || !self.global.settings.config.enablePause) {
                        self.options.link.val("Exit");
                    }
                });

            this.options.dlg = this.options.container.find("#gpd");
            this.options.link = $('<input id="idYes" type="button" value="Save & Exit" class="td-button td-button-small td-button-global"/>')
               .appendTo(self.options.container)
               .on('click', function (event) {
                   event.preventDefault();
                   event.stopPropagation();

                   if (self.options.save && self.global.settings.config.enablePause) {     
                       self.options.dlg.notificationdialog("dlgmsg", {

                           title: "Save & Exit",

                           dlgContent: [
                               {
                                   heading: "What would you like to do?",
                                   body: [],
                                   buttons: [
                                       {
                                           name: 'Go Back',
                                           class: " left",
                                           action: {
                                               type: "cancel"
                                           }
                                       },
                                       {
                                           name: 'Save & Exit',
                                           class: " right",
                                           action: {
                                               type: "continue"
                                           }
                                       }
                                   ]
                               },
                               {
                                   heading: "What would you like to do?",
                                   body: [
                                       "Are you sure you want to exit the test and come back later?"
                                   ],
                                   buttons: [
                                       {
                                           name: "Cancel",
                                           class: " left",
                                           action: {
                                               type: "cancel"
                                           }
                                       },
                                       {
                                           name: "Save & Exit",
                                           class: " right",
                                           action: {
                                               type: "notify-close",
                                               value:"saveandexit"
                                           }
                                       }
                                   ]
                               }
                           ]
                       });

                   }
                   else {
                       self.options.dlg.notificationdialog("dlgmsg", {

                           title: "Exit",

                           dlgContent: [
                               {
                                   heading: "Are you sure you want to exit?",
                                   body: [],
                                   buttons: [
                                       {
                                           name: "Cancel",
                                           class: " left",
                                           action: {
                                               type: "cancel"
                                           }
                                       },
                                       {
                                           name: "Exit",
                                           class: " right",
                                           action: {
                                               type: "notify-close",
                                               value: "justexit"
                                           }
                                       }
                                   ]
                               }
                           ]
                       });

                   }
               });

            this.options.dlg.notificationdialog({
                justexit: function () {
                    self._trigger("justexit");
                },
                saveandexit: function () {
                    self._trigger("saveandexit");
                }

            });
        }
    });
})(jQuery);
