(function ($) {
    $.widget("td.confirmstudent", $.td.formbase, {

	    _create: function () {

		    console.log("In ConfirmStudentWidget._create();");

		var self = this;
		var markup =
			'<div class="td-confirm-student td-scroll"><div id="gpd"/>' +
				'<form id="idConfirmStudentForm" style="text-align:center;">' +
					'<h1 class="td-confirmation-header td-heading">Is this you?</h1>' +
					'<br/>' +
                    '<div class="td-test-background-box">' +
					    '<div style="padding:25px;">' +
                            '<div class="td-test-student-img-frame">' +
                                '<div class="td-confirm-student-img"/>' +
                            '</div>' +
						    '<h2 class="td-confirmation-header td-heading" id="idCandidateName"></h2>' +
						    '<div class="td-body"><span id="idGradeLevel"/><span>th Grade at </span>' +
						    '<span id="idSchoolName"/></div>' +
					    '</div>' +
					'</div>' +
					'<br/>' +
					'<div style="margin-top:16px; margin-bottom:15px;">' +
						'<input id="idNo" type="button" value="No" class="td-button td-button-large td-button-secondary" tabindex="1">' +
						'<input id="idYes" type="button" value="Yes" class="td-button td-button-large td-button-primary" tabindex="1">' +
					'</div>' +
				'</form>' +
			'</div>';
		this.options.container = $(markup).appendTo(this.element)
            .on('formopen', function () {
                self._setValues();
            });
		

		this.options.container.find("#idYes")
            .on('click', function (event) {
                self._trigger("csyes");
            });

		this.options.dlg = this.options.container.find("#gpd");

		this.options.container.find("#idNo")
            .on('click', function (event) {

                self.options.dlg.notificationdialog("dlgmsg", {
                    dialogWidth: 550,
                    dialogHeight: 255,
                    title: "Exit",

                    dlgContent: [
                        {
                            heading: "If you are having trouble starting your test, please ask your teacher for help before moving on.",
                            body: [],
                            buttons: [
                                {
                                    name: "Go Back",
                                    class: " left",
                                    action: {
                                        type: "cancel"
                                    }
                                },
                                {
                                    name: "Exit",
                                    class: " right",
                                    action: {
                                        type: "notify-close",
                                        value: "exitTD"
                                    }
                                }
                            ]
                        }
                    ]
                });
             });

		this.options.dlg.notificationdialog({
		    exitTD: function () {
		        console.log("In Exit TD functionality");
		        if (self.global) {
		            self.global.settings = {};
		            self.global.data = {};
		            self.global.testInfo = {};
		        }
		        window.location.reload();
		        screenfull.exit();
		    }

		});

	},

	    _setValues: function () {
	        this.options.container.find("#idCandidateName").text(this.global.settings.candidate.firstName + " " + this.global.settings.candidate.lastName);
	        this.options.container.find("#idGradeLevel").text(this.global.settings.candidate.grade);
	        this.options.container.find("#idSchoolName").text(this.global.settings.candidate.school);
	    }
    });
})(jQuery);
