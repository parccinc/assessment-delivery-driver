(function ($) {
    $.widget("td.item", $.td.formbase, {

        options: {
            time: {
                total: null,
                start: null,
                running: null,
                current: ""
            },
            allowNavigation: {
                testPart: false,
                assessmentSection: false
            },
            generated: false,
            collectionNames: {
                generated: [{
                    name: "battery"
                }, {
                    name: "test"
                }, {
                    name: "testlet"
                },{
                    name: "topic"
                },{
                    name: "part"
                }, {
                    name: "section"
                }, {
                    name: "item"
                }],
                imported: [{
                    name: "testPart"
                }, {
                    name: "assessmentSection"
                }, {
                    name: "assessmentItem"
                }]
            }
        },

        _createEngineElements: function () {
            var collectionType = this.options.collectionNames.imported;
            for (var level = 0; level < collectionType.length; level++) {
                var engineElementClassName = "td-" + collectionType[level].name + "-engine";
                if ($("." + engineElementClassName).length == 0) {
                    $("body").append("<div class='" + engineElementClassName + " td-engine-class'/>");
                }
            }
        },

        _loadPackage: function (obj, pid, cid) {

            this._insertRow(obj, pid, cid);

            var self = this;
            if (obj.dynamic["collection"]) {
                $.each(obj[obj.dynamic.collection], function (index, colel) {
                    self._loadPackage(colel, cid, cid + "-" +index);
                });
            }
        },

        _insertRow: function (obj, pid, cid) {

            //var row = this._createRow(obj, pid, cid);
            //console.log(row);
            //$("#dataTree").append(row);

            $("#dataTree").append(this._createRow(obj, pid, cid));
        },

        _createRow: function (obj, pid, cid) {
            var rowData = this._getRowData(obj, pid, cid);
            return $(
                "<tr data-tt-id='" + rowData.cidattr + "'" + (pid < 0 ? "" : " data-tt-parent-id='" + rowData.pidattr + "'") + ">" +
                    "<td class='sp-icon'>" + rowData.icon + "</td>" +
                    "<td class='sp-order'>" + rowData.order + "</td>" +
                    "<td class='sp-system-item'>" + rowData.systemItem + "</td>" +
                    "<td class='sp-not-scored'>" + rowData.notScored + "</td>" +
                    "<td class='sp-score-report'>" + rowData.scoreReport + "</td>" +
                    "<td class='sp-item-answered'>" + rowData.itemAnswered + "</td>" +
                    "<td class='sp-score'>" + rowData.score + "</td>" +
                    "<td class='sp-item-grade'>" + rowData.itemGrade + "</td>" +
                    "<td class='sp-scoring-type'>" + rowData.scoringType + "</td>" +
                    "<td class='sp-correct'>" + rowData.correct + "</td>" +
                "</tr>"
            );
        },

        _getRowData: function (obj, pid, cid) {
            if (obj.dynamic["collection"]) {
                return {
                    pidattr: pid, //pid < 0 ? "" : " data-tt-parent-id='" + pid + "'",
                    cidattr: cid, //" data-tt-id='" + cid + "'",
                    icon: "<span class='folder'>" + obj.dynamic.type + "</span>",
                    order: "<span title='Selection Algorithm'>" + obj.dynamic.selectionAlgorithm + "</span>",
                    systemItem: "",
                    notScored: "",
                    scoreReport: "",
                    itemAnswered: "",
                    score: "",
                    itemGrade: "",
                    scoringType: "",
                    correct: ""
                };
            }
            else {
                return {
                    pidattr: pid, // pid < 0 ? "" : " data-tt-parent-id='" + pid + "'",
                    cidattr: cid, // " data-tt-id='" + cid + "'",
                    icon: "<span class='file'>" + obj.dynamic.type + "</span>",
                    order: "<span class='spp-order' title='Order'/>",
                    systemItem: "<input class='spp-system-item' type='checkbox' disabled='disabled' title='System Item' " + ($.UTILITY.isSystemItem(obj) ? "checked='checked'" : "") + "/>",
                    notScored: "<input class='spp-not-scored' type='checkbox' disabled='disabled' title='Not Scored' " + ($.UTILITY.isNotScored(obj) ? "checked='checked'" : "") + "/>",
                    scoreReport: "<input class='spp-score-report' type='checkbox' disabled='disabled' title='Score Report' " + ($.UTILITY.isScoreReport(obj) ? "checked='checked'" : "") + "/>",
                    itemAnswered: "<input class='spp-item-answered' type='checkbox' disabled='disabled' title='Answered' " + ($.UTILITY.isItemAnswered(obj) ? "checked='checked'" : "") + "/>",
                    score: "<span class='spp-score' disabled='disabled' title='Raw Score'>" + $.UTILITY.getItemScore(obj).toString() + "</span>",
                    itemGrade: "<span class='spp-item-grade' disabled='disabled' title='Item Grade'>" + $.UTILITY.getItemGrade(obj).toString() + "</span>",
                    scoringType: "<span class='spp-scoring-type' disabled='disabled' title='Scoring Type'>" + $.UTILITY.getItemScoringType(obj).toString() + "</span>",
                    correct: "<span class='spp-correct' disabled='disabled' title='Answered Correctly'>" + $.UTILITY.isItemAnsweredCorrectly(obj).toString() + "</span>"
                };
            }
        },

        _updateRow: function (obj) {
            obj.row.find(".spp-order").text(obj.order);
            obj.row.find(".spp-item-answered").prop("checked", obj.answered);
            obj.row.find(".spp-score").text(obj.score);
            obj.row.find(".spp-correct").text(obj.correct);
        },

        _updateTreeList: function () {

            var self = this;
            $("#dataTree tr").each(function (index, element) {
                var id = $(this).attr("data-tt-id");
                var ids = id.split("-");
                if (ids.length == 4) {
                    var selector = {
                        testPart: ids[1],
                        assessmentSection: ids[2],
                        assessmentItem: ids[3]
                    };

                    var testPart = self.global.data.testPart[selector.testPart];
                    var section = testPart.assessmentSection[selector.assessmentSection];
                    var item = section.assessmentItem[selector.assessmentItem];

                    self._updateRow({
                        row: $(this),
                        order: self.options.asefactory.selectionenginefactory('getOrder', selector),
                        answered: $.UTILITY.isItemAnswered(item),
                        score: $.UTILITY.getItemScore(item).toString(),
                        correct: $.UTILITY.isItemAnsweredCorrectly(item).toString()
                    });
                }
            });
        },

	    _create: function () {

	        console.log("In ItemWidget._create();");

	        this._createEngineElements();

	        var self = this;
		    var markup =
			    '<div class="td-item unselectable">' +
                   '<div id="gpd"/>' +
		       '</div>';
		    var dbgMarkup = 
               '<div id="dbgTreeDiv" class="unselectable" style="display:none; position:absolute; left:0; top:0; right:0; bottom:0; background-color:rgba(0,0,0,.3); z-index:10000;">' +
                   '<div style="position:absolute; overflow:auto; left:64px; right:64px; top:64px; bottom:64px; box-shadow:0 0 50px 10px rgba(0,0,0,.3); background-color:white;">' +
                       '<table id="dataTree" style="width:1024px; margin:64px auto;">' +
                           '<colgroup>' +
                               '<col span="1" style="width:380px"/>' +
                               '<col span="1" style="width:100px"/>' +
                               '<col span="1" style="width:32px"/>' +
                               '<col span="1" style="width:32px"/>' +
                               '<col span="1" style="width:32px"/>' +
                               '<col span="1" style="width:100px"/>' +
                               '<col span="1" style="width:100px"/>' +
                               '<col span="1" style="width:100px"/>' +
                               '<col span="1" style="width:100px"/>' +
                               '<col span="1" style="width:1px"/>' +
                           '</colgroup>' +
                       '</table>' +
                   '</div>' +
               '</div>';

		    this.options.dbgMarkup = $(dbgMarkup).appendTo("body");

		    this.options.container = $(markup).appendTo(this.element);

		    this.options.dlg = this.options.container.find("#gpd");
		    this.options.dlg.notificationdialog({
		        proceed: $.proxy(self._proceedConfirmed, self),
		        submit: $.proxy(self._submitConfirmed, self),
		        saveandexit: $.proxy(self._saveExitConfirmed, self),
		        saveanderror: $.proxy(self._saveErrorConfirmed, self),
		        cancelederror: $.proxy(self._canceledErrorConfirmed, self),
		        maintenanceError: $.proxy(self._maintenanceErrorConfirmed, self),
		        errorLogout: $.proxy(self._errorLogoutConfirmed, self)
		    });

		    $(document).on('keydown', function (event) {
		        if (event.which == 118 && event.altKey && event.ctrlKey && event.shiftKey) {
		            if (self.global.settings.config.debug) {
		                self._updateTreeList();
		                $("#dbgTreeDiv").toggle(); // REMOVE ONCE EVERYTHING IS TESTED; IN PRODUCTION THIS LINE MUST BE COMMENTED OUT!!! TODO!!!}
		            }
		        }
		    });

	        // Create widgets that use container
		    this.options.sticky = this.options.container.stickyheader({
		        logo: false,
		        name: true,
		        exit: {
		            show: true,
		            save: true
		        },
		        separator: true,
		        timer: false,
		        tally: true,
		        review: false, // true,
		        flag: false, // true,
		        height: this.options.headerHeight,
		        saveandexit: function () {
		            self._saveExitConfirmed();
		        },
		        justexit:function(){
		            self._justExitConfirmed();
		        },
		        flagged: function () {
		            var current = self._getCurrent();
		            var item = self.global.data.testPart[current.current.testPart].assessmentSection[current.current.assessmentSection].assessmentItem[current.current.assessmentItem];
		            item.dynamic.flagged = !item.dynamic.flagged;
		            self.options.sticky.stickyheader("updateflag", current);
		        }
		    });
		    this.options.notifications = this.options.container.notifications({
		        yoffset: this.options.headerHeight
		    });
		    this.options.navigation = this.options.container.navigation({
		        nextitem: function () {
		            self._callEngine("next");		  
		        },
		        previous: function () {
		            self._callEngine("previous");
		        }
		    });

		    this.options.dal = this.options.container.dal({});

		    this.options.interaction = this.options.container.iteminteraction({
		        top: this.options.headerHeight,
		        irevent: function (event, ire) {
		            self._itemRunnerEventHandler(event, ire);
		        },
		        toolActivated: function (evt, toolObj) {
		            self._fireToolActivatedEvent(toolObj);
		        }
		    });

		    this.options.spinner = this.options.container.find(".td-item-interaction-container").spinner({});

		    this.options.uploader = this.options.container.datacomm({
		        uploaddone: function (evt, data) {
		            var purpose = data.purpose;
		            if (purpose === "submit") {
		                self._removeWarningMsgOnClose();
		                var current = self._getCurrent();
		                screenfull.exit();
		                if (current.scoreReport) {
		                    self._trigger("showscorereport");
		                }
		                else {
		                    self._trigger("saveandsubmitsucceeded");
		                }
		            }
		            else if (purpose === "pause") {
		                self._removeWarningMsgOnClose();
		                screenfull.exit();
		                self._trigger("saveandexitsucceeded");
		            }
		        },
		        uploadfailed: function (evt, errResponse) {
		            if (errResponse && errResponse.time) {
		                self.global.settings.timeOffset = 1000 * errResponse.time - new Date().getTime();
		            }
		                switch (errResponse.errorCode) {
		                    case "ADP-1467":
		                        if (errResponse.purpose == "pause") {
		                            self._removeWarningMsgOnClose();
		                            screenfull.exit();
		                            self._trigger("saveandexitsucceeded");
		                        }
		                        else {
		                            self._closeErrorMessage(errResponse, "saveanderror")
		                        }
		                        break;
		                    case "ADP-1455":
		                        if (errResponse.purpose == ("pause" || "submit")) {
		                            self._removeWarningMsgOnClose();
		                            screenfull.exit();
		                            self._trigger("cancelsucceeded", evt, "cancel");
		                        }
		                        else {
		                            self._canceledTestErrorMessage(errResponse, "cancelederror")
		                        };
		                        break;
		                    case "ADP-1451":
		                        self._closeErrorMessage(errResponse, "errorLogout");
		                        break;
		                    case "ADP-1001":
		                    case "ADP-1003":
		                    case "ADP-1004":
		                        self._closeErrorMessage(errResponse, "maintenanceError");
		                        break;
		                    default:
		                        if (errResponse && errResponse.purpose == "submit") {
		                            self._processErrorMessage(errResponse, "submit");
		                        }
		                        else {
		                            if (errResponse && errResponse.purpose == "pause") {
		                                self._processErrorMessage(errResponse, "saveandexit");
		                            }
		                        };
		                        break;
		            }
		        }
		    });
		
		    this.options.timer = this.options.container.timer({
		        upload: function () {
		            if (self.global.settings.config.uploadResults.timeInterval > 0) {
		                self.options.uploader.datacomm('upload');
		            }
		        },
		        ticker: function () {
		            var now = new Date().valueOf();
		            if (self.options.time.start == null) {
		                self.options.time.total = self.global.data.dynamic.remainingTime;
		                self.options.time.start = now;
		            }
		            self.options.time.running = now;
		            var cur = self._formatSpan();
		            if (cur !== self.options.time.current) {
		                self.options.time.current = cur;
		                console.log(self.options.time.current);
		                self.options.sticky.stickyheader("updatetime", self.options.time.current);
		            }
		        }
		    });

		    this.options.stickyfooter = this.options.container.stickyfooter({
		        fullScreenClicked: function (event, source) {
		            screenfull.toggle();
		        },
		        textToSpeechClicked: function (event, source) {
		            self.options.ttstoolbox.ttstoolbox("toggle", source);
		        },
		        lineReaderClicked: function (event, source) {
                    self.options.interaction.iteminteraction("toggleLineReader");
		        }
		    });

            // MOVE ALL TOOLS TO THE TOOLBAR - CONTAINMENT!!!! TODO
		    this.options.ttstoolbox = this.options.container.ttstoolbox({
		        toolActivated: function (evt, toolObj) {
		            self._fireToolActivatedEvent(toolObj);
		        }
		    });
        
		    this.options.container.find(".td-tts").hide();
            
		    if (this._on) {
		        // for jQuery >= 1.9
		    }
		    else {
		        this.options.container
                    .on('formopen', function () {                        
                        self.options.spinner.spinner("enterbusy");
                        
                        if (!$.browser.safari) {
                            self.options.container.triggerHandler('gotofc');
                        }

                        $(window).on("beforeunload", function (e) {
                            var confirmationMessage = 'If you leave this page, your test responses will not be saved. ' +
                            'Please use "save & exit" if you want to end your test session and save your responses. ' +
                            'If you are not sure what to do, please ask your teacher for help.';
                            + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        });

                        self.options.uploader.datacomm("open", self.global.settings);
                        self.options.sticky.stickyheader("open", self.global.settings);
                        self.options.navigation.navigation("open", self.global.settings);
                        self.options.interaction.iteminteraction("open", self.global.settings);
                        self.options.timer.timer('create', 'upload', self.global.settings.config.uploadResults.timeInterval * 1000, true); // ADP returns this in seconds, so it has to be converted into milliseconds
                        self.options.timer.timer('create', 'ticker', 500, true);
                        self.options.timer.timer("start", "upload");
                        self.options.timer.timer("start", "ticker", true);
                        self.options.stickyfooter.stickyfooter("open", self.global.settings);
                        self._createSelectionEngineFactory(self.global.data);
                        self.options.ttstoolbox.ttstoolbox("open", self.global.settings);
                    })
                    .on('gotofc', function () {
                        setTimeout($.proxy(function () {
                            self.options.stickyfooter.stickyfooter("fullscreen", screenfull.isFullscreen);
                        }, self), 500);
                    })
                    .on('formclose', function () { //ADS-1241 - call clear on IR when the item form is closed                        
                        self.options.interaction.iteminteraction("cleanup");
                    });
		    }

	        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function () {
	            if (!screenfull.isFullscreen) {
	                self.options.stickyfooter.stickyfooter("fullscreen", screenfull.isFullscreen);
	            }
	        });
	    },

	    _createSelectionEngineFactory: function () {

	        this._loadPackage(this.global.data, -1, 0);
	        var self = this;

	        this.options.asefactory = $("#dataTree")
                .selectionenginefactory({
                    testPackage: this.global.data,

                    collectionType: this.options.collectionNames.imported,

                    navupdate: function (event, state) {
                        self._onNavigationUpdate(state);
                    },

                    boundary: function (event, param) {
                        return true;
                    }
                })
                .treetable({ expandable: true, indent: 48 });

	        this.options.asefactory.selectionenginefactory("load", this.global.data);
	    },

	    _onNavigationUpdate: function() {
	        var ct = this._getCurrentToken();
	        if (ct) {
	            var tools = this._getTools();
	            this._fireLocalEvent("navto", ct, tools);
	        }
	        this.options.navigation.navigation("enable", this._updateNavControls()); // Replace control state with real state returned with the current
	        this._renderCurrent();
	    },

	    _updateNavControls: function () {
	        var current = this._getCurrent();
	        return { previous: current.enablePrevious, next: current.enableNext, review: current.enableReview, systemItem: current.systemItem };
	    },
   
	    _callEngine: function (criteria) {

	        try {
	            var ct = this._getCurrentToken();
	            if (ct) {
	                this._fireLocalEvent("navfrom", ct);
	                this.options.interaction.iteminteraction("cleanup", ct);
	            }
	            this.options.asefactory.selectionenginefactory(criteria);
	        }
	        catch (error) {
	            throw new Error(error);
	        }
	    },

	    _renderCurrent: function () {
	        this.options.spinner.spinner("enterbusy");
	        var currentToken = this._getCurrentToken();

	        var tallydata = this.options.dal.dal("questioncounter", currentToken)
	        this.options.sticky.stickyheader("togglecontrols", { clock: !currentToken.systemItem, tally: !currentToken.systemItem, review: !currentToken.systemItem && currentToken.enableReview }); // only show review if this is NOT a system item and "enableReview" flag is set to true
	        this.options.interaction.iteminteraction("render", currentToken);
	        this.options.sticky.stickyheader("tallycurrent", tallydata);
	        this.options.sticky.stickyheader("updateflag", currentToken);

	        this.options.ttstoolbox.ttstoolbox("stop");

	        //TR: Add Method to footer to update tools based on current item?
	        this.options.stickyfooter.stickyfooter("updateToolsBasedOnAssessmentItem", currentToken);
	    },

	    _formatSpan: function() {

	        var elapsed = Math.round((this.options.time.running - this.options.time.start) / 1000.0);
	        var remaining = Math.round(this.options.time.total - elapsed);
	        ////// this.global.data.dynamic.remainingTime = remaining;

	        var seconds = remaining % 60;
	        remaining = Math.floor(remaining / 60);
	        var minutes = remaining % 60;
	        remaining = Math.floor(remaining / 60);
	        var hours = remaining % 24;

	        // var ticker = this._checkPad(hours) + ':' + this._checkPad(minutes) + ':' + this._checkPad(seconds);
	        var hhmm = this._checkPad(hours) + ':' + this._checkPad(minutes); // + ':' + this._checkPad(seconds);
	        return hhmm;
	    },

	    _checkPad: function (val) {
	        if (val < 10) {
	            val = "0" + val;
	        };
	        return val;
	    },

	    _fireLocalEvent: function (eventType, itemSelector, value) {
	        value = (value === undefined) ? null: value;
	        var event = { type: eventType, value: value, selector: itemSelector };
	        this.options.uploader.datacomm("update", event);
	    },

	    _fireToolActivatedEvent: function (toolActivated) {
	        var ct = this._getCurrentToken();
	        if (ct) {          
	            this._fireLocalEvent("activated", ct, toolActivated);
	        }
	    },

	    _getCurrent: function () {
	        var current = this.options.asefactory.selectionenginefactory('current');

	        var testPart = this.global.data.testPart[current.testPart];
	        var section = testPart.assessmentSection[current.assessmentSection];
	        var item = section.assessmentItem[current.assessmentItem];

	        var predecessor = this.options.asefactory.selectionenginefactory('predecessor');
	        var successor = this.options.asefactory.selectionenginefactory('successor');

	        var atBeginning = this.options.asefactory.selectionenginefactory('atBeginning');
	        var done = this.options.asefactory.selectionenginefactory('done');

	        var systemItem = $.UTILITY.isSystemItem(item);
	        var scoreReport = $.UTILITY.isScoreReport(item);
	        var notScored = $.UTILITY.isNotScored(item);
	        var hasAssets = $.UTILITY.hasAssets(item);
	        var aseBackAllowed = $.UTILITY.algorithmCanGoBack(section);
	        var aseNextAllowed = $.UTILITY.algorithmCanGoForward(section);

	        var testPartChange = predecessor && current && predecessor.selector.testPart != current.testPart;

	        var enableNavigationToPrevious = !atBeginning && predecessor && !testPartChange && !systemItem && aseBackAllowed;
	        var enableNavigationToNext = !systemItem && (successor || (!successor && !done)) && aseNextAllowed;

	        return {
	            current: current,
	            predecessor: predecessor,
	            successor: successor,
	            atBeginning: atBeginning,
	            done: done,
	            systemItem: systemItem,
	            scoreReport: scoreReport,
	            notScored: notScored,
	            hasAssets: hasAssets,
	            aseBackAllowed: aseBackAllowed,
	            aseNextAllowed: aseNextAllowed,
	            testPartChange: testPartChange,
	            enablePrevious: enableNavigationToPrevious,
	            enableNext: enableNavigationToNext,
                enableReview: !systemItem
	        }
	    },

	    _getCurrentToken: function() {
	        var current = this._getCurrent();
	        if (current) {
	            return {
	                test: current.current.testPart,
	                section: current.current.assessmentSection,
	                item: current.current.assessmentItem,
	                enablePrevious: current.enablePrevious,
	                enableNext: current.enableNext,
	                enableReview: current.enableReview,
	                systemItem: current.systemItem,
	                notScored: current.notScored
	            }
	        }
	        else {
	            return null;
	        }
	    },

	    _getTools: function() {
	        return {
	            textToSpeech: false, //This should always be false on a navto event which is the only place this function is currentlty being called from. 
	            lineReader: this.options.container.parent().linereader("isActive")
	        }
	    },

	    _proceedConfirmed: function () {
	        var current = this._getCurrent();
	        if (current.scoreReport) {
	            this._trigger("showscorereport"); // There's something fishy here, I do not know what exacty or if I need to fix it, but it stinks.... TODO - r.s.
	        }
	        else {
	            this._callEngine("next");
	        }
	    },

	    _submitConfirmed: function (e) {
	        var self = this;

	        // Step 0: mark event on item
	        this._markExitEvent("submit");

            // Step 1: stop all timers
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");

            // Step 2: close all tools that are left open
	        this.options.interaction.iteminteraction("closeLineReader"); // TODO - close all tools on submit, not this one only. Make sure that you do that for all by calling some method here! TODO!!!!

	        // Step 3: Calculate all result data
	        $.SCOREREPORT.create(this.global);

	        //Step 3.1: If category:"scoreReport" create html
	        var current = this._getCurrent();
	        if (current.scoreReport) {
	            $.SCOREREPORT.getHtml(this.global);
	        }

	        // Step 4: Calculate all results not included in the score report - might use some calculated for the score report her - since score reports are standalone.
	        $.RESULTS.create(this.global);

	        // Step 5: if enableSubmit=true upload all results  else show score report or log out confiramtion page
	        if (this.global.settings.config.enableSubmit) {
	            this.options.uploader.datacomm("upload", "submit");
	        }
	        else {
	            self._removeWarningMsgOnClose();
	            var current = self._getCurrent();
	            screenfull.exit();
	            if (current.scoreReport) {
	                self._trigger("showscorereport", e, "justexit");
	            }
	            else {
	                self._trigger("cancelsucceeded", e, "justexit");
	            }
	        }
	    },

	    _saveExitConfirmed: function () {
	        // Step 0: mark event on item
	        this._markExitEvent("pause");
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");
	        this.options.interaction.iteminteraction("closeLineReader");
	        this.options.uploader.datacomm("upload", "pause");
	    },

	    _justExitConfirmed: function (e) {
	        this._removeWarningMsgOnClose();
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");
	        this.options.interaction.iteminteraction("closeLineReader");
	        screenfull.exit();
	        this._trigger("cancelsucceeded", e, "justexit");
	    },

	    _saveErrorConfirmed: function () {
	        //Step -1: remove beforeunload handler
	        this._removeWarningMsgOnClose();
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");
	        this.options.interaction.iteminteraction("closeLineReader");
	        screenfull.exit();
	        this._trigger("saveanderrorsucceeded");
	    },

	    _maintenanceErrorConfirmed: function (e) {
	        this._removeWarningMsgOnClose();
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");
	        this.options.interaction.iteminteraction("closeLineReader");
	        screenfull.exit();
	        this._trigger("cancelsucceeded", e, "maintenance");
	    },

	    _canceledErrorConfirmed: function (e) {
	        this._removeWarningMsgOnClose();
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");
	        this.options.interaction.iteminteraction("closeLineReader");
	        screenfull.exit();
	        this._trigger("cancelsucceeded", e, "cancel");
	    },

	    _errorLogoutConfirmed:function(e){
	        this._removeWarningMsgOnClose();
	        this.options.timer.timer("stop", "upload");
	        this.options.timer.timer("stop", "ticker");
	        this.options.interaction.iteminteraction("closeLineReader");
	        screenfull.exit();
	        this._trigger("cancelsucceeded", e, "logout");
	    },

	    _removeWarningMsgOnClose: function () {
	        $(window).off("beforeunload");
	    },

	    _markExitEvent: function(event) {
	        var ct = this._getCurrentToken();
	        if (ct) {
	            this._fireLocalEvent(event, ct);
	        }
	        else {
	            throw new Error("Failed to determine current token for event: " + event);
	        }
	    },

	    _processErrorMessage: function (errResponse, action) {
	        var self = this;

	        var header = "There was a problem saving your test results because the server cannot be reached. Please ask your teacher for help." 
	        var msgbody = "Click Retry to try to save your test results again, or Cancel to close this error message.";
	        if (errResponse && errResponse.errorCode !== 0) {
	            header = "There was a problem saving your test results.<br/>Please ask your teacher for help. [" + errResponse.errorCode + "]";
	        }
	        self.options.dlg.notificationdialog("dlgmsg", {
	            showClose: false,
                errorMessage:true,
	            title: "Problem Occurred",
	            dialogWidth: 558,
	            dialogHeight: 255,
	            dlgContent: [
                      {
                          heading: header,
                          body: [msgbody],
                          buttons: [
                               {
                                   name: "Cancel",
                                   class: " left",
                                   action: {
                                       type: "notify-close",
                                       value: "cancel"
                                   }
                               },
                              {
                                  name: 'Retry',
                                  class: " right",
                                  action: {
                                      type: "notify-close",
                                      value: action
                                  }
                              }
                          ]
                      }]
	        });
	    },

	    _closeErrorMessage:function(errResponse, action){
	        var self = this;
	        var dwidth, dheight;
	        if (action == "maintenanceError"||errResponse.errorCode === "ADP-1451") {
	            dwidth = action == "maintenanceError" ? 490 : 600;
	            dheight = action == "maintenanceError" ? 240 : 275;
	        }
	        var header = "Your test session has expired.[" + errResponse.errorCode + "]<br/>Please ask your teacher for help.";
	        if (errResponse.errorCode === "ADP-1451") {
	            header = "A problem has occurred and you will be logged out of your test session. [" + errResponse.errorCode + "] After you log out, you may try to log back in again to resume your test.<br/>Please ask your teacher for help."
	        }
	        if (action == "maintenanceError") {
	            header = header = "The system is currently undergoing maintenance. [" + errResponse.errorCode + "]<br/>We are sorry for the inconvenience. Please ask your teacher for help."
	        }
	        self.options.dlg.notificationdialog("dlgmsg", {
	            showClose: false,
	            errorMessage: true,
	            title: "Problem Occurred",
	            dialogWidth: dwidth,
	            dialogHeight: dheight,
	            dlgContent: [
                      {
                          heading: header,
                          body: ["Click Close to close this message."],
                          buttons: [
                               {
                                   name: "Close",
                                   class: " right",
                                   action: {
                                       type: "notify-close",
                                       value: action
                                   }
                               }
                          ]
                      }]
	        });
	    },

	    _canceledTestErrorMessage:function(errResponse, action){
	        var self = this;
	        var header = "Your test session has been canceled and you will be logged out of your test session. [" + errResponse.errorCode + "]<br/>Please ask your teacher for help.";
	        self.options.dlg.notificationdialog("dlgmsg", {
	            showClose: false,
	            errorMessage: true,
	            title: "Problem Occurred",
	            dialogWidth: 550,
	            dialogHeight: 275,
	            dlgContent: [
                      {
                          heading: header,
                          body: ["Click Close to close this error message."],
                          buttons: [
                               {
                                   name: "Close",
                                   class: " right",
                                   action: {
                                       type: "notify-close",
                                       value: action
                                   }
                               }
                          ]
                      }]
	        });
	    },

	    _itemRunnerEventHandler: function (event, ire) {
	        var self = this;
	        this.options.uploader.datacomm("update", ire);
	        if (ire.type === "endattempt") {
	            this._endAttemptHandler(ire);
	        };
	        if (ire.type === "setstate") {
	            setTimeout(function () {	                
	                self.options.spinner.spinner("leavebusy");
	            }, 3000);
	            
	        }
	    },

	    _endAttemptHandler: function(ire){
	        if (ire.value.indexOf("CONTINUE") >= 0) {
	            this._continueHandler(ire);
            }
	        else if (ire.value.indexOf("REVIEW") >= 0) {
	            this._reviewHandler(ire);
	        }
	        else if (ire.value.indexOf("SUBMIT") >= 0) {
	            this._submitHandler(ire);
	        }
	    },

	    _continueHandler: function (ire) {
	        var item = this.global.data.testPart[ire.selector.test].assessmentSection[ire.selector.section].assessmentItem[ire.selector.item];
	        if (item.dynamic.systemItemNavProperties && item.dynamic.systemItemNavProperties.lastInTestPart) {

	            this.options.dlg.notificationdialog("dlgmsg", {

	                title: "Complete Section",

	                dlgContent: [
                        {
                            heading: "Are you sure?",
                            body: [
                                "You are about to leave this section and will not be able to return when you leave."
                            ],
                            buttons: [
                                {
                                    name: "No",
                                    class: " left",
                                    action: {
                                        type: "cancel"
                                    }
                                },
                                {
                                    name: "Yes",
                                    class: " right",
                                    action: {
                                        type: "notify-close",
                                        value: "proceed"
                                    }
                                }
                            ]
                        }
	                ]
	            });
	        }
	        else {
	            this._callEngine("next");
	        }
	    },

	    _reviewHandler: function (ire) {
	        this._callEngine("previous");
	    },

	    _submitHandler: function (ire) {
	        this.options.dlg.notificationdialog("dlgmsg", {

	            title: "Submit & Exit",

	            dlgContent: [
                    {
                        heading: "Are you sure?",
                        body: [
                            "You are about to submit your test and will not be able to return when you leave."
                        ],
                        buttons: [
                            {
                                name: "No",
                                class: " left",
                                action: {
                                    type: "cancel"
                                }
                            },
                            {
                                name: "Yes",
                                class: " right",
                                action: {
                                    type: "notify-close",
                                    value: "submit"
                                }
                            }
                        ]
                    }
	            ]
	        });
	    }

    });
})(jQuery);
