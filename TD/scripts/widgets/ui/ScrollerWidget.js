(function ($) {
    $.widget("td.scroller", {

        options: {
            orientation: "vertical",
            target: {
                selector: "",
                element: null,
                height: 0,
                visibleHeight: 0
            },
            bar: {
                element: null,
                height: 0,
                start: 0
            },
            page: {
                element: null,
                height: 0
            },
            thumb: {
                element: null,
                proportional: true,
                height: 32
            },
            arrows: {
                show: false,
                height: 16,
                elements: []
            },
            intervals: []
        },

        destroy: function () {
            console.log("In ScrollerWidget::destroy()");
            $(this.options.bar.element).remove();
            $.Widget.prototype.destroy.apply(this, arguments);
        },

        _create: function() {

            var self = this;

            this.options.target.element = $(this.options.target.selector)[0];
            if (!this.options.target.element) {
                alert('Target element not found!');
                throw new Error("Target element with selector: " + this.options.target.selector + " not found.");
            }

            this.options.bar.element = this.element.find(".td-scroller.td-" + this.options.orientation)[0];
            if (!this.options.bar.element) {
                this.options.bar.element = $("<div class='td-scroller td-" + this.options.orientation + "'/>").appendTo(this.element);
                this.options.arrows.elements.push($("<div class='td-arrow ui-icon ui-icon-triangle-1-n td-up-arrow'/>").appendTo(this.options.bar.element));
                this.options.page.element = $("<div class='td-bar-page'/>").appendTo(this.options.bar.element);
                this.options.arrows.elements.push($("<div class='td-arrow ui-icon ui-icon-triangle-1-s td-down-arrow'/>").appendTo(this.options.bar.element));
                this.options.thumb.element = $("<div class='td-thumb'/>").appendTo(this.options.page.element);
            }

            if (this._on) {
            }
            else {
                $(this.options.thumb.element)
                    .on('mousedown', function (event) {
                        event.stopPropagation();
                    })
                    .draggable({
                        axis: "y",
                        containment: this.options.page.element,
                        scroll: false,
                        cursor: "pointer",
                        start: function (event) {
                            // event.stopPropagation();
                            // dragging = true;
                        },
                        stop: function () {
                            // dragging: false;
                        },
                        drag: function (event) {

                            // event.stopImmediatePropagation();
                            // event.preventDefault();
                            // event.stopPropagation();

                            // Get scroll thumb position
                            var sbpos = $(this).position();

                            var st = $(self.options.target.selector).scrollTop();

                            // scale: tbpos.top : scrollBarHeight = targetScrollTop : targetScrollHeight =>
                            // var targetScrollTop = Math.round(sbpos.top * self.options.target.element.scrollHeight / self.options.bar.height);
                            var targetScrollTop = Math.round(sbpos.top * self.options.target.element.scrollHeight / self.options.page.height);
                            console.log('Old targetScrollTop: ' + st, 'New targetScrollTop set to: ' + targetScrollTop);
                            $(self.options.target.selector).scrollTop(targetScrollTop);
                        }
                    });

                $(this.options.page.element)
                    .on('mousedown', function (event) {
                        var delay = [128, 96, 64, 48, 32, 16, 8];
                        var delayIndex = 0;
                        console.log('In onmousedown!');

                        var interval = setInterval(function () {

                            var scrollRangeMax = $(this).height();
                            var screenY = event.screenY;
                            var pageY = event.pageY;
                            var clientY = event.clientY;
                            var offsetY = event.offsetY;
                            var whichOne = offsetY; // clientY;

                            var scrollThumbTop = $(self.options.thumb.element).position().top;
                            var scrollThumbBottom = scrollThumbTop + self.options.thumb.height;

                            var newThumbPos = 0;

                            if (whichOne < scrollThumbTop) {
                                newThumbPos = scrollThumbTop - self.options.thumb.height;
                                if (newThumbPos < 0) {
                                    newThumbPos = 0;
                                }
                            }
                            else if (whichOne > scrollThumbBottom) {
                                newThumbPos = scrollThumbTop + self.options.thumb.height;
                                if (newThumbPos + self.options.thumb.height > self.options.page.height) {
                                    newThumbPos = self.options.page.height - self.options.thumb.height;
                                }
                            }
                            else {
                                newThumbPos = -1;
                            }

                            if (newThumbPos >= 0) {
                                $(self.options.thumb.element).css({ top: newThumbPos + "px" });
                                var targetScrollTop = Math.round(newThumbPos * self.options.target.element.scrollHeight / self.options.page.height);
                                $(self.options.target.element).scrollTop(targetScrollTop);
                            }

                            // event.stopPropagation();
                        }, 10, event);

                        self.options.intervals.push(interval);

                        // return false;
                    });

                $(this.options.arrows.elements)
                    .on('mousedown', function (event) {

                        var interval = setInterval(function () {
                            console.log('this works, let us see which arrow!');
                        }, 10, event);

                        self.options.intervals.push(interval);
                    });

                $(document).on('mouseup', function () {
                    $.each(self.options.intervals, function (index, interval) {
                        clearInterval(interval);
                    });
                });

                $(this.options.target.element)
                    .on("mousewheel", function (event) {
                        console.log(event.deltaX, event.deltaY, event.deltaFactor);
                        var scrollTop = $(this).scrollTop();
                        var newScrollTop = scrollTop;
                        if (event.deltaY < 0) {
                            newScrollTop = scrollTop - event.deltaY * event.deltaFactor;
                            if (newScrollTop > $(this)[0].scrollHeight) {
                                newScrollTop = event.currentTarget.scrollHeight;
                            }
                        }
                        else if (event.deltaY > 0) {
                            newScrollTop = scrollTop - event.deltaY * event.deltaFactor;
                            if (newScrollTop < 0) {
                                newScrollTop = 0;
                            }
                        }

                        if (scrollTop != newScrollTop) {
                            $(this).scrollTop(newScrollTop);
                            self._setThumbPosition();
                        }
                    })
                    .on("swipedown", function (event) {
                        console.log("swype down up on the target, distY: ", event.distY, ", velocityY: ", event.velocityY, ", calculated distance: ", event.distY * event.velocityY);

                        var scrollTop = $(this).scrollTop();
                        var newScrollTop = scrollTop - event.distY * event.velocityY * 5; // Experimental factor, tbd, add to options
                        if (newScrollTop < 0) {
                            newScrollTop = 0;
                        }
                        if (scrollTop != newScrollTop) {
                            $(this).animate({ scrollTop: newScrollTop }, '100', 'swing', function () {
                                console.log("Finished animating");
                                self._setThumbPosition();
                            });
                        }
                    })
                    .on("swipeup", function (event) {
                        console.log("swype up event on the target, distY: ", event.distY, ", velocityY: ", event.velocityY, ", calculated distance: ", event.distY * event.velocityY);

                        var scrollTop = $(this).scrollTop();
                        var newScrollTop = scrollTop + event.distY * event.velocityY * 5; // Experimental factor, tbd, add to options
                        if (newScrollTop > $(this)[0].scrollHeight) {
                            newScrollTop = event.currentTarget.scrollHeight;
                        }
                        if (scrollTop != newScrollTop) {
                            $(this).animate({ scrollTop: newScrollTop }, '100', 'swing', function () {
                                console.log("Finished animating");
                                self._setThumbPosition();
                            });
                        }
                    })
                    .on("swipeleft", function (event) {
                        console.log("swype left event on the target, distX: ", event.distX, ", velocityX: ", event.velocityX, ", calculated distance: ", event.distX * event.velocityX);
                    })
                    .on("swiperight", function (event) {
                        console.log("swype right event on the target, distX: ", event.distX, ", velocityX: ", event.velocityX, ", calculated distance: ", event.distX * event.velocityX);
                    });

            }

            this._resize();

        },

        _resize: function () {

            try {

                var target = this.options.target.element;
                var container = this.element;

                var scrollTop = this.element.scrollTop();
                var scrollBot = scrollTop + this.element.height();
                var elTop = $(target).offset().top;
                var elBottom = elTop + $(target).outerHeight();
                var visibleTop = elTop < scrollTop ? scrollTop : elTop;
                var visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
                console.log('Visible portion of the target: ' + (visibleBottom - visibleTop), 'Total height of target: ' + this.options.target.element.scrollHeight);

                console.log('Container scrollTop: ' + scrollTop, 'Container scrollBot: ' + scrollBot);
                console.log('Element top: ' + elTop, 'Element bottom: ' + elBottom);
                console.log('Visible top: ' + visibleTop, 'Visible Bottom: ' + visibleBottom);
                console.log('Element scroll height is: ' + this.options.target.element.scrollHeight);


                var showScrollBar2 = visibleBottom - visibleTop < this.options.target.element.scrollHeight;
                console.log('Show Scroll Bar flag is: ' + showScrollBar2);

                this.options.target.visibleHeight = visibleBottom - visibleTop;

                // $(this.options.target).height(this.options.target.visibleHeight);


                this.options.target.height = $(this.options.target.element).innerHeight();
                var showScrollBar = this.options.target.height < this.options.target.element.scrollHeight;
                if (showScrollBar2) {
                    this._setArrows();
                    this._setScrollBar();
                    this._setThumb();
                    this._setThumbPosition();
                }
                $(this.options.bar.element).toggle(showScrollBar2);

            }
            catch (Error) {
                console.log('Known issue, ignore now.');
            }
        },

        _setArrows: function() {
            var self = this;
            if (this.options.arrows.show) {
                $(this.options.arrows.elements).each(function () {
                    $(this).height(self.options.arrows.height).show();
                });
            }
            else {
                $(this.options.arrows.elements).each(function () {
                    $(this).hide();
                });
            }
        },

        _setScrollBar: function () {
            this.options.bar.start = 0;
            var barTotalHeight = $(this.options.bar.element).innerHeight();
            var barAvailableHeight = barTotalHeight;
            if (this.options.arrows.show) {
                barAvailableHeight -= this.options.arrows.height * 2;
                this.options.bar.start = this.options.arrows.height;
            }
            barAvailableHeight -= 2; // 2 pixels for 1 pixel margin at the ends
            this.options.bar.height = barAvailableHeight;
            this.options.bar.start += 1;
            this.options.page.height = barAvailableHeight;
            $(this.options.page.element)
                .height(barAvailableHeight)
                .css({ top: this.options.bar.start + "px" /*, "background-color": "pink"*/ });
        },

        _setThumb: function () {
            /*
            var self = this;
            if (this.options.thumb.proportional) {
                this.options.thumb.height = Math.round(this.options.target.height * this.options.page.height / this.options.target.element.scrollHeight);
                if (this.options.thumb.height < 32) {
                    this.options.thumb.proportional = false;
                    this.options.thumb.height = 32;
                }
            }
            $(this.options.thumb.element).height(this.options.thumb.height).show();
            */
            if (this.options.thumb.proportional) {
                this.options.thumb.height = Math.round(this.options.target.visibleHeight * this.options.page.height / this.options.target.element.scrollHeight);
                if (this.options.thumb.height < 32) {
                    // this.options.thumb.proportional = false;
                    this.options.thumb.height = 32;
                }
            }
            $(this.options.thumb.element).height(this.options.thumb.height).show();
        },

        _setThumbPosition: function() {
            var targetScrollTop = $(this.options.target.element).scrollTop();
            var thumbTopPosition = Math.round(targetScrollTop * this.options.page.height / this.options.target.element.scrollHeight);
            $(this.options.thumb.element).css({ top: thumbTopPosition + "px" });
        },

        resize: function () {
            this._resize();
        }
    });
})(jQuery);
