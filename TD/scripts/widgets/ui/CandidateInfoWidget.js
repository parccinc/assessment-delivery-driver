(function ($) {
    $.widget("td.candidateinfo", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In CandidateInfoWidget._create();");

            var self = this;
            this.options.container = $("<div class='td-candidate-info td-header-heading'/>")
                .appendTo(this.element)
                .on('formopen', function () {
                    $(this).text(self.global.settings.candidate.firstName + " " + self.global.settings.candidate.lastName);
                });
        }
    });
})(jQuery);
