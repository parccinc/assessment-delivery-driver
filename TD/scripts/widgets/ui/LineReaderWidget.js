(function ($) {
    $.widget("td.linereader", $.td.formbase, {

        options: {
            on: true,
            position:0,
            height: 128    // Reader line height in pixels
        },

        _create: function () {
            var self = this;

            this.element.addClass("lr-class");
            $('<div class="lr-top"/><div class="lr-bottom"/>').appendTo(this.element);
            this._setPos(this.options.position);

            if (this._on) {
                this._on(this.element, {
                    //mousemove: function (event) {
                    //    this._setPos(event.offsetY);
                    //}
                });
            }
            else {
                //this.element.on('mousemove', function (event) {
                //    // self._setPos(event.offsetY);
                //});

                // Must set on an element high-up in the hierarchy so it would be delivered to this handler.
                $(document).on('mousemove scroll', function (event) {
                    self._setPos(event.pageY - self.options.height);
                });
            }

            $(window).on('resize', function (event) {
                self._setPos(0);
            });

            this._showInitial();
        },

        _setPos: function (desiredCenter) {
            var topHeight = 0;
            var bottomHeight = 0;
            var offSetTop = this.element.find('.td-sticky-header-wrapper').height();
            var offSetBottom = this.element.find('.td-sticky-footer-wrapper').height();
            var totalHeight = this.element.innerHeight() - offSetTop - offSetBottom;
            if (totalHeight <= this.options.height) {
                // Nothing to do
            }
            else {
                if (desiredCenter < (this.options.height / 2)) {
                    desiredCenter = this.options.height / 2;
                }
                if (desiredCenter > (totalHeight - this.options.height / 2)) {
                    desiredCenter = totalHeight - this.options.height / 2;
                }
                topHeight = desiredCenter - this.options.height / 2;
                bottomHeight = totalHeight - desiredCenter - this.options.height / 2;            
            }

            this.element.find('.lr-top').css({ 'top': offSetTop + 'px', 'height': topHeight + 'px' });
            this.element.find('.lr-bottom').css({'bottom': offSetBottom + 'px', 'height': bottomHeight + 'px'})
        },

        _showInitial: function () {
            if (this.options.on) {
                this.element.find(".lr-top").show();
                this.element.find(".lr-bottom").show();
            }
            else {
                this.element.find(".lr-top").hide();
                this.element.find(".lr-bottom").hide();
            }
        },

        toggle: function () {
            if (this.options.on) {
                this.element.find(".lr-top").hide();
                this.element.find(".lr-bottom").hide();
            }
            else {
                this.element.find(".lr-top").show();
                this.element.find(".lr-bottom").show();
                
            }
            this.options.on = !this.options.on;
            if (this.isActive())
            {
                this._trigger("toolActivated", null, { id: "lineReader", name: "Line Reader" });
            }
            
        },

        close: function () {
            if (this.options.on) {
                this.element.find(".lr-top").hide();
                this.element.find(".lr-bottom").hide();
            }
        },

        isActive: function () {
            return this.options.on;
        }
    });
})(jQuery);
