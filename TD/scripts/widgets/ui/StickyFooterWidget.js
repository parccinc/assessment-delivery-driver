﻿(function ($) {
    $.widget("td.stickyfooter", $.td.formbase, {

        options: {
            tabSeed: 0,
            tools: {
                fullScreen: {
                    id: "fullScreen",
                    description: "Full Screen",
                    spritePosition: "-69px -8px;", // "-74px -8px;",
                    tabIndex: 1
                },
                tts: {
                    id: "textToSpeech",
                    description: "Text to Speech",
                    spritePosition: "-8px -8px;",
                    tabIndex: 2
                },
                lineReader: {
                    id: "lineReader",
                    description: "Line Reader",
                    spritePosition: "-8px -62px;",
                    tabIndex: 3
                }
            }
        },

        _create: function () {
            var self = this;
            console.log('Inside td.stickyfooter _create()');
            var self = this;
            this.options.container = $("<div class='td-sticky-footer-wrapper unselectable'><div class='td-sticky-footer-divider'/><div class='td-sticky-footer'/></div>")
                .appendTo(this.element)/*.height(this.global.footerHeight)*/;

            this.options.content = this.options.container.find(".td-sticky-footer");
        
            if (this._on) {
                // for jQuery >= 1.9
            }
            else {
                this.options.container
                    .on('formopen', function () {
                        if (self.global.settings.tools) {
                            $.each(self.global.settings.tools, function (tool, value) {
                                if (!value) {
                                    $('#' + tool).hide();
                                }
                            });

                            //Add toolbar if tts is enabled for the user.
                            if (self.global.settings.tools["textToSpeech"] == true) {
                                TexthelpSpeechStream.addToolbar(null, null);
                            }
                        }
  
                    })
            }

            this._addTools();
        },

        _addTools: function () {

            var self = this;
            
            $.each(this.options.tools, function (key, tool) {
                    // var toolWidget = $("<div class='td-sticky-footer-item' tabindex='" + (tool.tabIndex + self.options.tabSeed) + "'/>")
                self.options.tools[key].tool = $("<div class='td-sticky-footer-item' tabindex='" + (tool.tabIndex + self.options.tabSeed) + "' id='" + tool.id + "'/>")
                    .appendTo(self.options.content)
                    .tool({
                        settings: tool,
                        clicked: function (event, source) {
                            console.log('Tool Button Clicked!');
                            self._trigger(source.source + "Clicked", null, source);
                        }
                    });
                if ($.browser.safari && key=="fullScreen") {
                    self.options.tools[key].tool
                        .removeClass("td-sticky-footer-item")
                        .addClass("td-sticky-footer-item-disable")
                        .attr("tabindex", -1);
                    var dissablefs = $("<div class='td-sticky-footer-disable-icon'/>")
                        .appendTo(".td-sticky-footer-item-disable");
                }
            });

            //TexthelpSpeechStream.addToolbar(null, null);
           

        },

        fullscreen: function (value) {
            this.options.tools.fullScreen.tool.tool("pressed", value);
        }
    });
})(jQuery);
