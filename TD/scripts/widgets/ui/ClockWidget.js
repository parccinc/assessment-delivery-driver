(function ($) {
    $.widget("td.clock", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In ClockWidget._create();");

            var self = this;
            this.options.container = $("<div class='td-clock centered unselectable'/>")
                .appendTo(this.element)
                .on('formopen', function () {
                    $(this).text("00:00");
                });
        },

        update: function (hhmm) {
            this.options.container.text(hhmm);
        }
    });
})(jQuery);
