(function ($) {
    $.widget("td.flag", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In FlagWidget._create();");

            var self = this;
            this.options.container = $("<div class='td-flag'/>").appendTo(this.element)
                .on('click', function () {
                    self._trigger("flagged");
                });
        },

        update: function (current) {
            var sysItem = this.global.data.testPart[current.test].assessmentSection[current.section].assessmentItem[current.item].category &&
                this.global.data.testPart[current.test].assessmentSection[current.section].assessmentItem[current.item].category.indexOf("systemItem") >= 0;
            if (sysItem || !current.enableReview) {
                this.options.container.hide();
            }
            else {
                var css = {
                    "background-position-x": (this.global.data.testPart[current.test].assessmentSection[current.section].assessmentItem[current.item].dynamic.flagged ? "-44px" : "0")
                }
                this.options.container.show().css(css);
            }
        }
    });
})(jQuery);
