﻿(function ($) {
    $.widget("td.tool", $.td.formbase, {

        options: {
            height: 200,       
            settings: null,
            value: null
        },

        global: {
        },

        _create: function () {

            var self = this;

            console.log('Inside td.tool _create();');      

            $("<div class='td-sticky-footer-item-img' style='background-position:" + this.options.settings.spritePosition + "'></div>").appendTo(this.element);
            $("<div class='td-sticky-footer-item-text'/>").text(this.options.settings.description).appendTo(this.element);

            this.element
                .on('click', function () {
                    if ($.browser.safari && self.options.settings.id == "fullScreen") { }
                    else {
                        self._handleClick();
                        self._trigger("clicked", null, self.options.value);
                    }
                })
                .on('keydown', function (event) {
                    if ($.browser.safari && self.options.settings.id == "fullScreen") { }
                    else {
                        var keyCode = (event.keyCode ? event.keyCode : event.which);
                        if (keyCode == 13 || keyCode == 32) {
                            self._handleClick();
                            self._trigger("clicked", null, self.options.value);
                        }
                    }
                });
        },

        _handleClick: function () {
            this.element.toggleClass("td-tool-active");
            var size = { width: this.element.outerWidth(), height: this.element.outerHeight() };
            var pos = this.element.offset();
            this.options.value = { source: this.options.settings.id, pos: pos, size: size, active: this.element.hasClass("td-tool-active") };
        },

        pressed: function (value) {
            if (value) {
                this.element.addClass("td-tool-active");
            }
            else {
                this.element.removeClass("td-tool-active");
            }
        }

    });
})(jQuery);
