(function ($) {
    $.widget("td.welcome", $.td.formbase, {

        options: {
            focusSelector: "#idStateSelect",
            headerHeight: 0
	    },

        _create: function () {

            var self = this;
            console.log("In WelcomeWidget._create();");

            // Create container element
            var markup = 
			    '<div class="td-welcome td-scroll unselectable">' +
                    '<div class="td-welcome-content" style="position:relative;">' +
                        '<h1 class="td-heading">Login</h1>' +
                        '<form id="idWelcomeForm" style="text-align:center; margin-top:32px;">' +
                            '<div id="idState">' +
                                '<label for="idStateSelect">State</label>' +
                                '<select id="idStateSelect" style="margin-bottom:32px;" tabindex="1">' +
	                                '<option selected="" value="">- Select -</option>' +
                                '</select>' +
                            '</div>' +
                            '<div id="idCredentials" style="display:none; margin-bottom:15px;">' +
                                '<div id="idTestKeyGroup">' +
                                    '<label for="idTestKey">Access Code</label>' +
                                    '<input class="td-input" id="idTestKey" name="TestKey" type="text" style="margin-bottom:32px; text-align:center; text-transform:uppercase; width:120px;" tabindex="2"/>' +
                                '</div>' +
                                '<div id="idStudentIdGroup" style="display:none">' +
                                    '<label for="idstudentId">State Student Identifier</label>' +
                                    '<input class="td-input" id="idstudentId" name="StudentId" type="text" style="margin-bottom:32px; text-align:center; width:420px;" tabindex="3" maxlength="30"/>' +
                                '</div>' +
                                '<div id="idDobGroup" style="margin-bottom:32px;display:none;">' +
                                    '<label for="idMonth">Date of Birth</label>' +
                                    '<select id="idMonth" tabindex="4" style="margin-right:8px">' +
	                                    '<option selected="selected" value="">Month</option>' +
	                                    '<option value="01">January</option>' +
	                                    '<option value="02">February</option>' +
	                                    '<option value="03">March</option>' +
	                                    '<option value="04">April</option>' +
	                                   '<option value="05">May</option>' +
	                                   '<option value="06">June</option>' +
	                                   '<option value="07">July</option>' +
	                                   '<option value="08">August</option>' +
	                                   '<option value="09">September</option>' +
	                                   '<option value="10">October</option>' +
	                                   '<option value="11">November</option>' +
	                                   '<option value="12">December</option>' +
                                   '</select>' +
                                   '<select id="idDay" tabindex="5" style="margin-right:8px">' +
	                                   '<option selected="selected" value="">Day</option>' +
	                                   '<option value="01">01</option>' +
	                                   '<option value="02">02</option>' +
	                                   '<option value="03">03</option>' +
	                                   '<option value="04">04</option>' +
	                                   '<option value="05">05</option>' +
	                                   '<option value="06">06</option>' +
	                                   '<option value="07">07</option>' +
	                                   '<option value="08">08</option>' +
	                                   '<option value="09">09</option>' +
	                                   '<option value="10">10</option>' +
	                                   '<option value="11">11</option>' +
	                                   '<option value="12">12</option>' +
	                                   '<option value="13">13</option>' +
	                                   '<option value="14">14</option>' +
	                                   '<option value="15">15</option>' +
	                                   '<option value="16">16</option>' +
	                                   '<option value="17">17</option>' +
	                                   '<option value="18">18</option>' +
	                                   '<option value="19">19</option>' +
	                                   '<option value="20">20</option>' +
	                                   '<option value="21">21</option>' +
	                                   '<option value="22">22</option>' +
	                                   '<option value="23">23</option>' +
	                                   '<option value="24">24</option>' +
	                                   '<option value="25">25</option>' +
	                                   '<option value="26">26</option>' +
	                                   '<option value="27">27</option>' +
	                                   '<option value="28">28</option>' +
	                                   '<option value="29">29</option>' +
	                                   '<option value="30">30</option>' +
	                                   '<option value="31">31</option>' +
                                   '</select>' +
                                   '<select id="idYear" tabindex="6">' +
	                                   '<option selected="selected" value="">Year</option>' +
                                   '</select>' +
                               '</div>' +
                               '<input type="submit" id="idSubmit" value="Submit" class="td-button td-button-large td-button-primary" style="margin-top:8px"/>' +
                           '</div>' +
                       '</form>' +
                   '</div>' +
			    '</div>';

            
            this.options.container = $(markup).appendTo(this.element);
            
            this.options.notifications = this.options.container.notifications({
                yoffset: 0, // this.options.headerHeight,
                prepend: true
            });

            $.each($Global$.login, function (key, value) {
                var s = $("#idStateSelect");
                $('<option value="' + key + '">' + value.name + ' (' + key + ')</option>').appendTo(s);
            });

            if (Object.keys($Global$.login).length == 1) {
                $("#idState").hide();
                for (var key in $Global$.login) {
                    $("#idStateSelect").val(key);
                    $("#idCredentials").show();
                    if ($Global$.login[key].pid) {
                        $("#idStudentIdGroup").show();
                    }
                    if ($Global$.login[key].dob) {
                        $("#idDobGroup").show();
                    }
                }
            };

            var currentYear = new Date(1000 * $Global$.apiTime).getFullYear();
            for (i = currentYear - 20; i <= currentYear - 5; i++) {
                var y = $("#idYear");
                $('<option value="' + i + '">' + i + '</option>').appendTo(y);
            }

            if ($.browser.safari) {
                this.options.container.find("select").addClass("td-safari-select");
            }
            
            this.options.container.find("#idTestKey").mask("*****-*****", {
                placeholder: "_____-_____", autoclear: true, completed: function () {
                    if ($("#idStudentIdGroup").is(":visible")){
                        self.options.container.find("#idstudentId").focus();
                    }
                    else
                        if ($("#idDobGroup").is(":visible")) {
                            self.options.container.find("#idMonth").focus();
                        }
                        else {
                            self.options.container.find("#idSubmit").focus();
                        }
                }
            });

            if (this._on) {
            }
            else {
                var self = this;
                this.options.container
                    .on('formopen', function () {
                        if (self.global && self.global.settings && self.global.settings.environment && self.global.settings.environment.practice) {
                            $(".td-welcome-content").hide();           
                            self.options.container.trigger("authenticate", { type: "practice" });
                        }
                        else {
                            $("#cover").css({ "display": "none" });
                        }
                    })
                    .on('authenticate', function (event, obj) {
                        self._authenticate(obj);
                    })
                    .on('authfailure', function (event, obj) {
                        self._errorMessage(obj.responseText);
                    });
                this.options.container.find("#idSubmit")
                    .on('click.td', function (event) {
                        event.preventDefault();
                        var state = $("#idStateSelect").val();
                        var testKey = self._validateTestKey();
                        var studentId = self._validateStudentId();
                        var dob = self._validateDob();
                        if ((!testKey.status) || (studentId.on && !studentId.status) || (dob.on && !dob.status)) {
                            var text = "There was a problem logging in. Please enter the following information, or ask the teacher for help before moving on.";
                            if (!testKey.status) {
                                text += "<li>A valid Access Code</li>";
                            }
                            if (studentId.on && !studentId.status) {
                                text += "<li>State Student Identifier</li>";
                            }
                            if (dob.on && !dob.status) {
                                text += "<li>Student Date of Birth</li>";
                            }
                            self.options.notifications.notifications('error', text, null, false, true, false);
                            if ($.browser.safari) {
                                $("#idCredentials").find(".td-error").addClass("td-error-safari").removeClass("td-error");
                            }
                        }
                        else {
                            self._setCredentials({ testKey: testKey.tkey, dob: dob.date, id: studentId.studId, state: state });
                        }
                    });
                this.options.container.find("#idStateSelect")
                    .on('change', function (event) {
                        var state = $(this).val();
                        if (state) {
                            $.cookie('state', state, { expires: 7, path: '/' });
                            $("#idCredentials").hide();
                            self.options.container.find("#idCredentials")
                                .css('opacity', 0)
                                .slideDown(800)
                                .animate({
                                    opacity: 1
                                },
                                {
                                    queue: false,
                                    duration: 1000
                                }, "linear");
                            self._testKeyReset();
                            self._studentIdReset();
                            self._dobReset();
                            if (state in $Global$.login) {
                                if ($Global$.login[state].pid) {
                                    $("#idStudentIdGroup").show();
                                }
                                else {
                                    $("#idStudentIdGroup").hide();
                                }
                                if ($Global$.login[state].dob) {
                                    $("#idDobGroup").show();

                                }
                                else {
                                    $("#idDobGroup").hide();
                                }
                            }
                            else {
                                $("#idStudentIdGroup").show();
                                $("#idDobGroup").hide();
                            }
                        }
                        else {
                            self.options.container.find("#idCredentials").hide();
                            $.removeCookie('state'); // This is just for now, a way to remove cookie
                        }
                    });

            }
            this._checkStateCookie();
        },

	    _checkStateCookie: function () {
	        if ($.cookie("state") === undefined) {
	            console.log('State cookie not found.');
	        }
	        else {
	            this.options.container.find("#idStateSelect").val($.cookie("state")).trigger("change");
	        }
	    },

	    _validateDob: function () {
	        var obj = {date:"", status:false, on:false};
	        if ($("#idDobGroup").is(":visible")) {
	            obj.on = true;
	            var y = $("#idYear").val(), m = $("#idMonth").val(), d = $("#idDay").val();
	            if (y && m && d) {
	                var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	                if ((!(y % 4) && y % 100) || !(y % 400)) {
	                    daysInMonth[1] = 29;
	                }
	                if (d <= daysInMonth[--m]){
	                    obj.status = true;
	                    obj.date = ""+$("#idYear").val() + '-' + $("#idMonth").val() + '-' + $("#idDay").val();
	                    $("#idMonth").removeClass("td-error td-error-safari");
	                    $("#idDay").removeClass("td-error td-error-safari");
	                    $("#idYear").removeClass("td-error td-error-safari");
	                    console.log("Date of birth is: ", obj.date)
	                }
	                else {
	                    obj.status = false;
	                    $("#idMonth").addClass("td-error");
	                    $("#idDay").addClass("td-error");
	                    $("#idYear").addClass("td-error");
	                }
	            }
	            else {
	                obj.status = false;
	                $("#idMonth").addClass("td-error");
	                $("#idDay").addClass("td-error");
	                $("#idYear").addClass("td-error");
	            }
	        }
	        else {
	            obj.on = false;
	        }
	        return obj;
	    },

	    _validateTestKey: function () {
	        var obj = { tkey: "", status: false };
	        obj.tkey = $("#idTestKey").mask().toUpperCase();
	        if (obj.tkey.length == 10) {
	            obj.status = true;
	            $("#idTestKey").removeClass("td-error td-error-safari");
	        }
	        else {
	            obj.status = false;
	            $("#idTestKey").addClass("td-error");
	        }
	        return obj;
	    },

	    _validateStudentId: function () {
	        var obj = { studId: "", status: false, on: false };
	        if ($("#idStudentIdGroup").is(":visible")) {
	            obj.on = true;
	            obj.studId = $("#idstudentId").val();
	            if (obj.studId) {
	                obj.status = true;
	                $("#idstudentId").removeClass("td-error td-error-safari");
	            }
	            else {
	                obj.status = false;
	                $("#idstudentId").addClass("td-error");
	            }
	        };
	        return obj;
	    },

	    _testKeyReset: function () {
	        var self = this;
	        $("#idTestKey").removeClass("td-error td-error-safari");
	        $("#idTestKey").unmask();
	        $("#idTestKey").val("");
	        $("#idTestKey").mask("*****-*****", {
	            placeholder: "_____-_____", autoclear: false, completed: function () {
	                if ($("#idStudentIdGroup").is(":visible")) {
	                    self.options.container.find("#idstudentId").focus();
	                }
	                else
	                    if ($("#idDobGroup").is(":visible")) {
	                        self.options.container.find("#idMonth").focus();
	                    }
	                    else {
	                        self.options.container.find("#idSubmit").focus();
	                    }
	            }
	        });
	    },

	    _studentIdReset: function(){
	        $("#idstudentId").removeClass("td-error td-error-safari");
	        $("#idstudentId").val("");
	    },

	    _dobReset:function(){
	        $("#idMonth").removeClass("td-error td-error-safari");
	        $("#idDay").removeClass("td-error td-error-safari");
	        $("#idYear").removeClass("td-error td-error-safari");
	        $("#idYear").val("");
	        $("#idMonth").val("");
	        $("#idDay").val("");
	    },

	    _authenticate: function (obj) {

	        var self = this;
	        var method = "POST";
	        var xdebug = ''; // '?XDEBUG_SESSION_START=dbgp'

	        var str = $Global$.token;
	        var ar = [];
	        while ((ar = str.split('-')).length < 3) {
	            str = $.UTILITY.base64toUtf8(str);
	        }

	        var username = ar[0]; // $Global$.username;
	        var password = ar[1]; // $Global$.password;
	        var secret = ar[2]; // $Global$.secret;
	        this.global.settings.credentials.username = username;
	        this.global.settings.credentials.password = password;
	        this.global.settings.credentials.secret = secret;
	        var dateOfBirth = this.global.settings.credentials.dob;
	        var studentId = this.global.settings.credentials.id;
	        var state = this.global.settings.credentials.state;
	        var testkey = this.global.settings.credentials.testkey;

	        var apiPath = $Global$.apiPath + "/login";

	        var url = $Global$.apiHost + apiPath + xdebug;

	        var credentials = username + ":" + password;
	        var timestamp = new Date().getTime() + this.global.settings.timeOffset;
	        var nonce = Math.floor(Math.random() * 99999);
	        var hash = apiPath + Math.round(timestamp / 1000) + nonce;
	        var message = $.sha256hmac(secret, hash);
	        var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(message));
	        var payload;
	        if (method !== 'GET' && method !== 'HEAD' && method !== 'OPTIONS') {
	            payload = JSON.stringify({
	                testKey: testkey,
	                dateOfBirth: dateOfBirth,
	                studentId: studentId,
	                state: state
	            });
	        }

	        if (obj.type === "seb") {
	            method = "GET";
	            url = this.options.defaultUrl;
	            crossDomain = false;
	        }

	        $.ajax({
	            url: url,
	            headers: {
	                'Content-Type': 'application/json; charset=UTF-8',
	                'Authorization': 'Basic ' + btoa(credentials),
	                'Authentication': authentication,
	                'X-Requested-With': btoa(testkey)
	            },
	            type: method,
	            data: payload,
	            crossDomain: true
	        })
            .done(function (data, textStatus, jqXHR) {
                self._saveSettings(data);
                self._trigger("authsuccess", null, obj);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                self.options.container.triggerHandler("authfailure", { obj: obj, responseText: jqXHR.responseText });
            });
	    },

	    _errorMessage: function (data) {
	        if (data.length != 0) {
	            var errObject = JSON.parse(data);
	            var errObj = errObject.result ? errObject.result : errObject;
	            if (errObj.time) {
	                this.global.settings.timeOffset = 1000 * errObj.time - new Date().getTime();
	            }
	            if (errObj && errObj.errorCode) {
	                switch (errObj.errorCode) {
	                    case "ADP-1107":
	                    case "ADP-1108":
	                    case "ADP-1109":
	                    case "ADP-1204":
	                    case "ADP-1206":
                            //with implemented SEB error
	                    //case "ADP-1222":
	                    //    this.options.notifications.notifications('error', '<div>The Access Code does not match our records. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                    //    break;
	                    //case "ADP-1231":
	                    //case "ADP-1232":
	                    //    this.options.notifications.notifications('error', '<div>There was a problem logging in because our records show that you are already logged in and your test is locked. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                    //    break;
	                    //case "ADP-1228":
	                    //    this.options.notifications.notifications('error', '<div>Your test has already been submitted. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                    //    break;
	                    //case "ADP-1229":
	                    //case "ADP-1230":
	                    //    this.options.notifications.notifications('error', '<div>Your test has been submitted or canceled. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                    //    break;
	                        //end

                            //without implemented SEB errors
	                    case "ADP-1219":
	                        this.options.notifications.notifications('error', '<div>The Access Code does not match our records. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                        break;
	                    case "ADP-1228":
	                    case "ADP-1229":
	                        this.options.notifications.notifications('error', '<div>There was a problem logging in because our records show that you are already logged in and your test is locked. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                        break;
	                    case "ADP-1225":
	                        this.options.notifications.notifications('error', '<div>Your test has already been submitted. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                        break;
	                    case "ADP-1226":
	                    case "ADP-1227":
	                        this.options.notifications.notifications('error', '<div>Your test has been submitted or canceled. [' + errObj.errorCode + ']</div> <div>Please ask the teacher for help before moving on.</div>', data, false, true, false);
	                        break;
                            //end
	                    case "ADP-1200":
	                    case "ADP-1201":
	                    case "ADP-1202":
	                    case "ADP-1203":
	                    case "ADP-1205":
	                    case "ADP-1207":

	                        //with implemented SEB error
	                    //case "ADP-1213":
	                    //case "ADP-1214":
	                    //case "ADP-1215":
	                    //case "ADP-1216":
	                    //case "ADP-1217":
	                    //case "ADP-1218":
	                    //case "ADP-1220":
                            //end

	                        //without implemented SEB error
	                    case "ADP-1210":
	                    case "ADP-1211":
	                    case "ADP-1212":
	                    case "ADP-1213":
	                    case "ADP-1214":
	                    case "ADP-1215":
	                    case "ADP-1217":
                            //end
	                        this.options.notifications.notifications('error', '<div>There was a problem logging in because one or more of the log in fields do not match our records. [' + errObj.errorCode + ']</div>Please provide valid data and try again.</div>', data, false, true, false);
	                        break;
	                    case "ADP-1001":
	                        this.options.notifications.notifications('error', '<div>The system is currently undergoing maintenance. We are sorry for the inconvenience. [' + errObj.errorCode + ']</div>Please ask your teacher for help.</div>', data, false, true, false);
	                        break;

	                        //with implemented SEB error
	                    //case "ADP-1210":
	                    //    this.options.notifications.notifications('error', '<div>The test you are trying to take requires the use of Secure Browser. [' + errObj.errorCode + ']</div>Please ask your teacher for help.</div>', data, false, true, false);
	                    //    break;
	                    //case "ADP-1211":
	                    //case "ADP-1212":
	                    //    this.options.notifications.notifications('error', '<div>There was a problem logging in because the Secure Browser is not configured properly. [' + errObj.errorCode + ']</div>Please ask your teacher for help.</div>', data, false, true, false);
	                    //    break;
                            //end
	                    default:
	                        this.options.notifications.notifications('error', '<div>There was a problem logging in. [' + errObj.errorCode + '] <div>Please ask your teacher for help.</div>', data, false, true, false);
	                        break;
	                }
	            }
	            else {
	                this.options.notifications.notifications('error', '<div>There was a problem logging in because the server cannot be reached. <p/><div>Please ask your teacher for help.</div>', data, false, true, false);
	            };
	        }
	        else {
	            this.options.notifications.notifications('error', '<div>There was a problem logging in because the server cannot be reached. <p/><div>Please ask your teacher for help.</div>', data, false, true, false);
	        }
	    },

	    _setCredentials: function (credentials) {
	        this.global.settings.credentials.testkey = credentials.testKey.split('-').join('');
	        this.global.settings.credentials.dob = credentials.dob;
	        this.global.settings.credentials.state = credentials.state;
	        this.global.settings.credentials.id = credentials.id;
	        this.options.container.trigger("authenticate", { type: "credentials" });
	    },

	    _saveSettings: function (data) {
	        var settings = data.result ? data.result : data;
	        this.global.settings.api = settings.api;
	        this.global.settings.config = settings.config;
	        this.global.settings.candidate = settings.candidate;
	        this.global.settings.test = settings.test;
	        this.global.settings.tools = settings.tools;
	        $Global$.apiTime = settings.api.time;
	    }
    });
})(jQuery);
