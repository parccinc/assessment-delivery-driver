(function ($) {
    $.widget("td.navigation", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In NavigationWidget._create();");

            var self = this;
            this.options.container = $('<div/>').appendTo(this.element);
            this.options.prev = $("<div class='td-navigation td-navigation-prev' tabindex='0'/>").appendTo(this.element)
                .on('click', function () {
                    self._trigger("previous");
                })
                .on('keydown', function (event) {
                    var keycode = event.keyCode ? event.keyCode : event.which;
                    if (keycode == 13 || keycode == 32) {
                        self._trigger("previous");
                    }
                });
            this.options.next = $("<div class='td-navigation td-navigation-next' tabindex='0'/>").appendTo(this.element)
                .on('click', function () {
                    self._trigger("nextitem");
                })
                .on('keydown', function (event) {
                    var keycode = event.keyCode ? event.keyCode : event.which;
                    if (keycode == 13 || keycode == 32) {
                        self._trigger("nextitem");
                    }
                });

            if (this._on) {
                // for jQuery >= 1.9
            }
            else {
                this.options.container
                    .on('formopen', function () {
                        self.options.prev.show();
                        self.options.next.show();
                    });
            }
        },

        enable: function (state) {
            state.previous ? this.options.prev.show() : this.options.prev.hide();
            state.next ? this.options.next.show() : this.options.next.hide();

            this.options.prev.blur();
            this.options.next.blur();
        }
    });
})(jQuery);
