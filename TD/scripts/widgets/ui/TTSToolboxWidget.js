(function ($) {
    $.widget("td.ttstoolbox", $.td.formbase, {

        options: {
            position: null,
            parts: {
                btnPlay: null,
                btnStop: null
            },
            state: {
                current: null
            },
            voice: {
                tom: {
                    id: "ttsTomId",
                    key: "Tom",
                    value: "Vocalizer Expressive Tom Premium High 22kHz",
                    selected: false
                },
                ava: {
                    id: "ttsEvaId",
                    key: "Eva",
                    value: "Vocalizer Expressive Ava Premium High 22kHz",
                    selected: true
                }
            },
            speed: {
                slow: {
                    id: "ttsSlowId",
                    key: "SLOW_SSPEED",
                    value: 25, // SLOW_SPEED,
                    titleSelector: ".td-tts-speed-slow",
                    selected: false
                },
                medium: {
                    id: "ttsMediumId",
                    key: "MEDIUM_SSPEED",
                    value: 40, // MEDIUM_SPEED,
                    titleSelector: ".td-tts-speed-medium",
                    selected: true
                },
                fast: {
                    id: "ttsFastId",
                    key: "FAST_SSPEED",
                    value: 55, // FAST_SPEED,
                    titleSelector: ".td-tts-speed-fast",
                    selected: false
                }
            }
        },

        _states: {
            ready: 0,
            playing: 1,
            paused: 2,
            stopped: 0, // synonym for ready
        },

        _create: function () {

            console.log("In TTSToolboxWidget._create();");

            var self = this;
            this.options.state.current = this._states.ready;
            // Catch the completed callback
            try {
                if (!gSpeechCompletedCallback) {
                    gSpeechCompletedCallback = $.proxy(this._speechCompleted, this);
                }
            }
            catch (error) {
                if (console && console.log && typeof console.log == "function") {
                    console.log('Failed to register TTS Speech Completed Callback: ' + error.message)
                }
            }

            var markup = "<div class='td-tts-toolbox'>" +
                             "<div class='td-tts-header'>" +
                                 "<div class='td-tts-header-handle'/>" +
                             "</div>" +
                             "<div class='td-tts-controls'>" +
                                 "<div id='ttsPlayId' class='td-tts-control' tabindex='101'>" +
                                     "<div class='td-tts-control-handle'></div>" +
                                 "</div>" +
                                 "<div id='ttsStopId' class='td-tts-control' tabindex='102'>" +
                                     "<div class='td-tts-control-handle td-tts-disabled'></div>" +
                                 "</div>" +
                                 "<div id='ttsSettingsId' class='td-tts-control' tabindex='103'>" +
                                     "<div class='td-tts-control-handle'></div>" +
                                 "</div>" +
                             "</div>" +
                             "<div style='clear:both;height:0;background-color:transparent;overflow:hidden;'/>" +
                             "<div class='td-tts-settings'>" +
                                 "<div class='td-tts-settings-speed'>" +
                                     "<div class='td-tts-settings-speed-heading'>Speed</div>" +
                                     "<div class='td-tts-settings-speed-buttons'>" +
                                         "<div class='td-tts-settings-speed-slider'/>" +
                                         "<div id='ttsSlowId' class='td-tts-settings-speed-radio-container' tabindex='-1'><div class='td-tts-settings-speed-radio'></div></div>  " +
                                         "<div id='ttsMediumId' class='td-tts-settings-speed-radio-container' tabindex='-1'><div class='td-tts-settings-speed-radio'></div></div>  " +
                                         "<div id='ttsFastId' class='td-tts-settings-speed-radio-container' tabindex='-1'><div class='td-tts-settings-speed-radio'></div></div>  " +
                                     "</div>" +
                                     "<div class='td-tts-settings-speed-titles'>" +
                                         "<div class='td-tts-settings-speed-title td-tts-speed-slow'>Slow</div>" +
                                         "<div class='td-tts-settings-speed-title td-tts-speed-medium'>Medium</div>" +
                                         "<div class='td-tts-settings-speed-title td-tts-speed-fast'>Fast</div>" +
                                     "</div>" +
                                 "</div>" +
                                 "<div class='td-tts-settings-voice'>" +
                                     "<div class='td-tts-settings-voice-heading'>Voice</div>" +
                                     "<div class='td-tts-settings-voice-buttons'>" +
                                         "<div id='ttsTomId' class='td-tts-settings-voice-container' tabindex='-1'>Tom</div>  " +
                                         "<div id='ttsEvaId' class='td-tts-settings-voice-container' tabindex='-1'>Eva</div>  " +
                                     "</div>" +
                                 "</div>" +
                             "</div>" +
                         "</div>";

            this.options.container = $(markup)
                .appendTo(this.element)
                .on('formopen', function () {
                    self.options.container.draggable({ handle: '.td-tts-header', containment: ".td-ttstoolbox-container", scroll: false }).hide();
                })
                .on('click', '.td-tts-control', function (event) {
                    if (this.id === "ttsPlayId") {
                        self._play();
                    }
                    else if (this.id === "ttsStopId") {
                        self._stop();
                    }
                    else if (this.id === "ttsSettingsId") {
                        self._settings();
                    }
                })
                .on('keydown', function (event) {
                    var keyCode = (event.keyCode ? event.keyCode : event.which);
                    if (keyCode == 13 || keyCode == 32) {
                        self._play();
                    }
                })
                .on('click', '.td-tts-settings-speed-radio-container', function (event) {
                    var self2 = this;
                    $.each(self.options.speed, function (key, settings) {
                        settings.selected = self2.id === settings.id;
                    });
                    self._setSpeed();
                })
                .on('click', '.td-tts-settings-voice-container', function (event) {
                    var self2 = this;
                    $.each(self.options.voice, function (key, settings) {
                        settings.selected = self2.id === settings.id;
                    });
                    self._setVoice();
                });

            $(window).on('resize', function (event) {
                self._checkVisible();
            });

            this.options.parts.btnPlay = this.options.container.find(".td-tts-control#ttsPlayId .td-tts-control-handle");
            this.options.parts.btnStop = this.options.container.find(".td-tts-control#ttsStopId .td-tts-control-handle");

            this._setVoice();
            this._setSpeed();
        },

        _settings: function () {
            this.options.container.toggleClass("td-tts-expanded");
            this.options.container.find("#ttsSettingsId").toggleClass("td-tts-active");
            this._checkVisible();
            this._setTabbing();
        },

        _center: function (source) {
            var l = source.pos.left + source.size.width / 2 - 77; // 77 is width in pixels
            var t = source.pos.top - 63 - 24; // 63 is non-expanded height
            this.options.position = { left: l, top: t };
            this.options.container.css({ "left": l, "top": t });
        },

        _setTabbing: function() {
            if (this.options.container.hasClass("td-tts-expanded")) {
                this.options.container.find("#ttsSlowId").attr("tabindex","104");
                this.options.container.find("#ttsMediumId").attr("tabindex","105");
                this.options.container.find("#ttsFastId").attr("tabindex","106");
                this.options.container.find("#ttsTomId").attr("tabindex","107");
                this.options.container.find("#ttsEvaId").attr("tabindex","108");
            }
            else {
                this.options.container.find("#ttsSlowId").attr("tabindex","-1");
                this.options.container.find("#ttsMediumId").attr("tabindex","-1");
                this.options.container.find("#ttsFastId").attr("tabindex","-1");
                this.options.container.find("#ttsTomId").attr("tabindex","-1");
                this.options.container.find("#ttsEvaId").attr("tabindex","-1");
            }
        },

        _checkVisible: function () {

            var clip = this._clip();
            while ((clip.horizontal && !clip.atLeft) || (clip.vertical && !clip.atTop)) {
                if (clip.horizontal && !clip.atLeft) {
                    this._tryMoveLeft();
                }
                if (clip.vertical && !clip.atTop) {
                    this._tryMoveUp();
                }
                clip = this._clip();
            }
        },

        _tryMoveLeft: function () {
            var widgetPos = this.options.container.position();
            var newLeft = widgetPos.left > 100 ? Math.floor(widgetPos.left - 100) : 0;
            this.options.position.left = newLeft;
            this.options.container.css({ "left": newLeft + "px" });
        },

        _tryMoveUp: function() {
            var widgetPos = this.options.container.position();
            var newTop = widgetPos.top > 64 ? Math.floor(widgetPos.top - 64) : 0;
            this.options.position.top = newTop;
            this.options.container.css({ "top": newTop + "px" });
        },

        _clip: function () {

            var clip = { horizontal: false, vertical: false, atLeft: false, atTop: false };
            var containment = $(".td-ttstoolbox-container");
            var containmentPos = containment.position();
            var containmentSize = { width: containment.width(), height: containment.height() + containmentPos.top};
            containmentPos.top = 0;

            var widgetPos = this.options.container.position();
            var widgetSize = { width: this.options.container.outerWidth(), height: this.options.container.outerHeight() };

            clip.atLeft = widgetPos.left === 0;
            clip.atTop = widgetPos.top === 0;

            if (widgetPos.left < containmentPos.left || widgetPos.left + widgetSize.width > containmentPos.left + containmentSize.width) {
                console.log('Widget is outside horizontally!');
                console.log('Widget pos:[' + widgetPos.left + "," + widgetPos.top + "], size: [" + widgetSize.width + "," + widgetSize.height + "]");
                console.log('Contnr pos:[' + containmentPos.left + "," + containmentPos.top + "], size: [" + containmentSize.width + "," + containmentSize.height + "]");
                clip.horizontal = true;
            }
            if (widgetPos.top < containmentPos.top || widgetPos.top + widgetSize.height > containmentPos.top + containmentSize.height) {
                console.log('Widget is outside vertically!');
                console.log('Widget pos:[' + widgetPos.left + "," + widgetPos.top + "], size: [" + widgetSize.width + "," + widgetSize.height + "]");
                console.log('Contnr pos:[' + containmentPos.left + "," + containmentPos.top + "], size: [" + containmentSize.width + "," + containmentSize.height + "]");
                clip.vertical = true;
            }
            return clip;
        },

        _play: function () {

            if (this.options.state.current == this._states.ready) {
                this.options.state.current = this._states.playing;

                this._setButtons();

                try {
                    $rw_speakById('item');
                }
                catch (error) {
                    alert('$rw_speakById() failed: ' + error.message);
                }
                
               
                this._trigger("toolActivated", null, { id: "textToSpeech", name: "Text to Speech" });
                
                
            }
            else if (this.options.state.current == this._states.playing) {
                this.options.state.current = this._states.paused;

                this._setButtons();

                try {
                    $rw_event_pause();
                }
                catch (error) {
                    alert('$rw_event_pause(): ' + error.message);
                }
            }
            else if (this.options.state.current == this._states.paused) {
                this.options.state.current = this._states.playing;

                this._setButtons();

                try {
                    $rw_event_pause();
                }
                catch (error) {
                    alert('$rw_event_pause(): ' + error.message);
                }
            }

            this._setButtons();
        },

        _stop: function () {

            if (this.options.state.current == this._states.ready) {
                this.options.state.current = this._states.stopped; // same as ready, shouldn't even be possible given the state of buttons
            }
            else if (this.options.state.current == this._states.playing || this.options.state.current == this._states.paused) {
                this.options.state.current = this._states.stopped;

                this._setButtons();

                try {
                    $rw_stopSpeech();
                }
                catch (error) {
                    alert('$rw_stopSpeech() failed: ' + error.message);
                }
            }
        },

        _setButtons: function () {

            switch (this.options.state.current) {

                case this._states.ready:

                    this.options.parts.btnPlay.removeClass('td-tts-control-paused');
                    this.options.parts.btnStop.addClass("td-tts-disabled");

                    break;

                case this._states.playing:

                    this.options.parts.btnPlay.addClass('td-tts-control-paused');
                    this.options.parts.btnStop.removeClass('td-tts-disabled');

                    break;

                case this._states.paused:

                    this.options.parts.btnPlay.removeClass("td-tts-control-paused");
                    this.options.parts.btnStop.removeClass("td-tts-disabled");

                    break;
            }
        },

        _speechCompleted: function () {
            this.options.state.current = this._states.ready;
            this._setButtons();
        },

        _setSpeed: function () {
            $.each(this.options.speed, function (key, settings) {

                var selector = "#" + settings.id;
                if (settings.selected) {
                    eba_speed_value = settings.value;
                    if (typeof $rw_setSpeedValue === "function") {
                        $rw_setSpeedValue(eba_speed_value);
                    }
                    $(selector).find(".td-tts-settings-speed-radio").addClass("td-tts-active");
                    $(settings.titleSelector).addClass("td-tts-settings-speed-title-visible");
                }
                else {
                    $(selector).find(".td-tts-settings-speed-radio").removeClass("td-tts-active");
                    $(settings.titleSelector).removeClass("td-tts-settings-speed-title-visible");
                }
            });
        },

        _setVoice: function () {
            $.each(this.options.voice, function (key, settings) {

                var selector = "#" + settings.id;
                if (settings.selected) {
                    eba_voice = settings.value;
                    if (typeof $rw_setVoice === "function") {
                        $rw_setVoice(eba_voice);
                    }
                    $(selector).addClass("td-tts-active");
                }
                else {
                    $(selector).removeClass("td-tts-active");
                }
            });
        },

        //////////////////////////////////////////////////////////////////////////////////////////
        // Public interface
        //
        stop: function () {
            this._stop();
        },

        toggle: function (source) {
            if (this.options.position == null) {
                this._center(source);
            }
            this._stop();
            this._checkVisible();
            this.options.container.toggle();

           
        },

        isActive: function() {
            return this.options.state.current == this._states.playing;
        }
    });
})(jQuery);
