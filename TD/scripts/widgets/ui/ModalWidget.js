(function ($) {
    $.widget("td.modal", $.td.formbase, {

        _create: function () {

            console.log("In ModalWidget._create();");

            var self = this;
            var markup = "\
                <div id='idModalDialog' style='width:60%; margin:0 auto; text-align:center;'>\
                    <p id='idAreYou' style='font-size:18px; font-weight:bold;'>Are you sure?</p>\
                    <p id='idIfYouLeave' style='font-size:12px;'>You are about to leave this section and will not be able to return when you leave.</p>\
                </div>"
            this.options.container = $(markup).appendTo(this.element)
                .on('formopen', function () {});

       
            this.options.dialog = this.options.container
                    .dialog({
                        create: function (event, ui) {
                            var titleBar = $(".ui-dialog-titlebar").css({ 'background': 'none', 'border': 'none' });
                        },
                        autoOpen: false,
                        draggable: false,
                        width: 600,
                        height: 340,
                        resizable: false,
                        modal: true,
                        buttons: [
                            {
                                text: "Yes",
                                "class": 'td-dlg-button',
                                click: function () {
                                    $(this).dialog("close");
                                    alert("Closing dialog");
                                }
                            },
                            {
                                text: "No",
                                "class": 'td-dlg-button',
                                click: function () {
                                    $(this).dialog("close");
                                }
                            }
                        ]
                    });

            $(".ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset").css({ "text-align": "center", "float": "none", "background": "none" });

            $(".ui-dialog-buttonpane").css({ "border": "none", "text-align": "center", "display": "block" });

            //var buttons = $(".td-dlg-button");
            //buttons.removeClass("td-dlg-button").addClass("td-dlg-button");
       
        },

        warning: function (type) {
        
            this.options.dialog.dialog("open");

        }
    });
})(jQuery);
