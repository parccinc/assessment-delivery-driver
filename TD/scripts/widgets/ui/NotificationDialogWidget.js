(function ($) {
    $.widget("td.notificationdialog", {

        options: {
            dialogWidth: 400,
            dialogHeight: 235
        },

        _create: function () {

            var self = this;
            console.log('In notificationdialog _create().');
            this.element.css({ "text-align": "center", "overflow": "visible"});
        },

        dlgmsg: function (settings) {

            var self = this;

            // if settings.title set title of the element, otherwise set it to the heading
            // if settings.heading that's the bold face h1 in our html
            this.element.prop('title', typeof settings.title !== "undefined" ? settings.title : "General Purpose Dialog for PARCC");

            if (settings.dialogWidth === undefined) {
                settings.dialogWidth = self.options.dialogWidth;
            }

            if (settings.dialogHeight === undefined) {
                settings.dialogHeight = self.options.dialogHeight;
            }

            if(settings.showClose === undefined){
                settings.showClose = true;
            }

            if (settings.hideTitle === undefined) {
                settings.hideTitle = true;
            }

            if (settings.errorMessage === undefined) {
                settings.errorMessage = false;
            }

            var content = [];
            var buttons = [];
            var html = "";
            $.each(settings.dlgContent, function (contentIndex, content) {
                var btnClass = "notification-" + (contentIndex + 1);
                btnClass += contentIndex > 0 ? " td-hidden-element" : " td-visible-element";
                var ht = "<div class='" + btnClass + "'>";
                if (settings.errorMessage) {
                    ht += "<div  style='color:#882522; background-color:#fedcd8;margin-bottom:25px; padding:3px 0; min-width:350px;'><h4 class='td-heading'>" + content.heading + "</h4></div>";
                }
                else {
                    ht += "<div style='color: #464646; '><h3 class='td-heading' style='padding-bottom:0;'>" + content.heading + "</h3></div>";
                }
                $.each(content.body, function (index, item) {
                    ht += "<div style='font-family:Arial; color:#545454; font-size:13px; font-weight:400; letter-spacing:0.52px; margin:-10px 10px 0;'>" + item + "</div>";
                });
                ht += "</div>";
                html += ht;
            
                $.each(content.buttons, function (buttonsIndex, btn) {
                    buttons.push({
                        text: btn.name,
                        "class": "notification-dlg-btn td-button td-button-large " + btnClass + btn.class,
                        click: function () {
                            if (btn.action.type === "notify-close" || btn.action.type === "notify-continue") {
                                self._trigger(btn.action.value);
                            }
                            if (btn.action.type === "continue" || btn.action.type === "notify-continue") {
                                var currentClass = $(this).find("div.td-visible-element, button.td-visible-element").prop("class");
                                var tokens = currentClass.split(/[ -]+/);
                                var currentIndex = parseInt(tokens[1]);
                                var newIndex = currentIndex + 1;
                                if (newIndex < (settings.dlgContent.length + 1)) { // Because of the 1 based rule I used
                                    var classToHide = ".notification-" + currentIndex;
                                    var classToShow = ".notification-" + newIndex;
                                    $(classToHide).removeClass("td-visible-element").addClass("td-hidden-element");
                                    $(classToShow).removeClass("td-hidden-element").addClass("td-visible-element").focus();
                                }
                                else {
                                    $(this).dialog("close");
                                }
                            }
                            else if (btn.action.type === "cancel" || btn.action.type === "notify-close") {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            });
            this.element.html(html);

            var dlgwidget = this.element.dialog({
                resizable: false,
                autoOpen: false,
                width: settings.dialogWidth,
                height: settings.dialogHeight,
                modal: true,
                buttons: buttons,
                closeOnEscape: settings.showClose,
                zIndex: 9999999 //ADS-1241
            });

            var parent = this.element.parent();

            // remove buttons background and set the html contet
            var btnsleft = $("button.notification-dlg-btn.left")
                .addClass("td-button-secondary")
                .each(function (index, btn) {
                    var txt = $(this).text();
                    $(this).text("").html(txt)
                        .css({
                            "background": "none",
                            "color": "#363636",
                            "font-family": "'Open-Sans', sans-serif",
                            "font-weight": "bold",
                            "background-color": "#ffffff",
                            "font-size": "18px"
                        });
                });

            var btnsright = $("button.notification-dlg-btn.right")
                .addClass("td-button-primary")
                .each(function (index, btn) {
                    var txt = $(this).text();
                    $(this).text("").html(txt)
                        .css({
                            "background": "none",
                            "color": "#ffffff",
                            "border-color": "#70547d",
                            "font-family": "'Open-Sans', sans-serif",
                            "font-weight": "bold",
                            "font-size": "18px"
                        });
                });

             ////Titlebar
            var headerBar = $(parent)
                .find(".ui-dialog-titlebar")
                .css({
                    "background": "none",
                    "background-color": "#344759",
                    "border-width": "0",
                    "height": "55px",
                    "color": "#f3f2f2",
                    "font-size": "26px",
                    "text-align": "left",
                    "font-family": "Open Sans Condensed",
                    "font-weight": "bold",
                    "padding": "0 0 0 20px",
                    "line-height": "55px"
                });

            var titleSpan = $(parent)
                .find("span.ui-dialog-title#ui-dialog-title-gpd")
                .text(settings.title);

            var closeBtn = headerBar.find(".closeBtnFrame");
            if (closeBtn.length==0){
                headerBar.append("<div class='closeBtnFrame' tabindex='1'><div class='closeButton'/></div>");
            }

            if (settings.showClose === false) {
                $(".closeButton").hide();
            }
            else {
                $(".closeButton").on("click", function () {
                    dlgwidget.dialog("close");
                });
            }
           

            // Close Button
            $(parent).find("a.ui-dialog-titlebar-close.ui-corner-all").hide();

            // Remove border on button-set
            var buttonPane = $(parent).find(".ui-dialog-buttonpane").css({ "border-width": "0", "float": "none", "clear": "both", "text-align": "center", "padding-right":"0" });
            var buttonSet = $(buttonPane).find(".ui-dialog-buttonset").css({ "float": "none" });

            if (settings.hideTitle===true) {
                $(".ui-dialog-titlebar").hide();
            }
           

            this.element.dialog("open");
        }
    });
})(jQuery);
