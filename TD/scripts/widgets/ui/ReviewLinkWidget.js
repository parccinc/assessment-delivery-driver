(function ($) {
    $.widget("td.reviewlink", $.td.formbase, {

        options: {
        },

        _create: function () {

            console.log("In ReviewLinkWidget._create();");

            var self = this;
            this.options.container = $("<div class='td-review-link'/>")
                .appendTo(this.element)
                .on('formopen', function () {
                    $('<a href="" target="_self">See All in &lt;Section Name&gt;</a>').appendTo(self.options.container);
                });
        },

        togglecontrol: function (val) {
            // jQuery > 1.9
            // val ? this._show(this.element, this.options.show) : this._hide(this.element, this.options.hide);
            this.options.container.toggle(val);
        }
    });
})(jQuery);
