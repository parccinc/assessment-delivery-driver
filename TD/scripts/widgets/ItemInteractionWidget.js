(function ($) {
    $.widget("td.iteminteraction", $.td.formbase, {

        options: {
            itemRunner: null
        },

        _create: function () {

            console.log("In ItemInteractionWidget._create();");

            var self = this;
            this.options.container = $("<div class='td-item-interaction-container unselectable'/>")
                .appendTo(this.element)
                .css({ top: (this.options.top+2) + "px" });
            this.options.interaction = $("<div id='item' class='td-item-interaction unselectable'/>").appendTo(this.options.container);

            // Create widgets that use container
            this.options.container
                .on('formopen', function () {

                });

            this.options.linereader = $('.td-container').linereader({
                height: 64,
                on: false,
                toolActivated: function (evt, args) {
                    self._trigger("toolActivated", null, args);
                }
            });

            this.options.dal = this.options.container.dal({});

            $(window).on('resize', function (event) {
                if (self.options.targetElement) {
                    // $(".td-item-interaction-container").scroller('resize');
                    self._centerVertically();
                }
            });

            $(this.options.interaction).on('scroll', function () {
                var top = $(this).position().top;
                var offset = $(this).offset().top;
                var scrollTop = $(this).scrollTop();
                var height = $(this).height();
                var innerHeight = $(this).innerHeight();
                var outerHeight = $(this).outerHeight();
                var scrollHeight = $(this)[0].scrollHeight;

                console.log("top:", top, "offset:", offset, "scrollTop:", scrollTop, "height: ", height, "innerHeigt:", innerHeight, "outerHeight:", outerHeight, "scrollHeight: ", scrollHeight, "scrollTop + height:", scrollTop + height);

            });
        },

        render: function (itemSelector) {

            this.options.targetElement = null;

            // $('.td-item-interaction-container').scroller("destroy");
            $('.td-item-interaction#item')
                .empty()
                .css({
                    "visibility": "hidden",
                    "margin-top": 0
                });

            var item = this.global.data.testPart[itemSelector.test].assessmentSection[itemSelector.section].assessmentItem[itemSelector.item];
            if (item) {

                //this._refreshSystemTags(itemSelector);
                this._replaceSystemTags(itemSelector, item);

                try {
                    this._render(item, $('.td-item-interaction#item')[0], $.proxy(this._callback, this), itemSelector);
                }
                catch (error) {
                    alert('Error: ' + error.message);
                }
            }
        },

        cleanup: function (itemSelector) {
            if (this.options.itemRunner) {
                try {
                    this.options.itemRunner.clear();
                    this.options.itemRunner = null;
                }
                catch (error) {
                    console.log(error);
                }
            }

        },

        _replaceSystemTags: function (selector, item) {

            var replaceableItems = this.options.dal.dal("getsystemtags", selector);
            var self = this;
            var substitutionCount = 0;
            if (item && item.assessmentItemContent && item.assessmentItemContent.data && /*item.assessmentItemContent.assets && */item.assessmentItemContent.data.body && item.assessmentItemContent.data.body.originalBody && item.assessmentItemContent.data.body.originalBody.length > 0) {
                var newBody = item.assessmentItemContent.data.body.originalBody.replace(/\[{2}([A-Z_]+)\]{2}/g, function (match, key) {
                    substitutionCount++;
                    return typeof replaceableItems[key] === "undefined" ? "<span class='td-replaceableItems'>" + match + "</span>" : replaceableItems[key];
                });
                console.log('Substituted: ' + substitutionCount + ' system variables.');
                item.assessmentItemContent.data.body.body = newBody;
            }
            else if (item && item.assessmentItemContent && item.assessmentItemContent.body && item.assessmentItemContent.body.originalBody && item.assessmentItemContent.body.originalBody.length > 0) {
                var newBody = item.assessmentItemContent.body.originalBody.replace(/\[{2}([A-Z_]+)\]{2}/g, function (match, key) {
                    substitutionCount++;
                    // return typeof self.global.replaceableItems[key] === "undefined" ? "<span style='font-weight:bold; font-size:2em; color:ivory; background-color:red;'>" + match + "</span>" : self.global.replaceableItems[key];
                    return typeof replaceableItems[key] === "undefined" ? "<span class='td-replaceableItems'>" + match + "</span>" : replaceableItems[key];
                });
                console.log('Substituted: ' + substitutionCount + ' system variables.');
                item.assessmentItemContent.body.body = newBody;
            }
        },

        _render: function (item, ic, fp, is) {

            var self = this;

            require(['qtiItemRunner'//,
                //'../dist/tei.min',
                //'../dist/studenttools.min'
            ], function (itemRunnerFactory) { // Parameters are still not including tei (whatever that is), and the student tools, ask Bertrand about it

                /**
                 * Create asset management strategies, 
                 * each URL is resolved by the handler in the order they are defined 
                 * until the strategy returns something.
                 * Here the middleware is resolves URLs using this stack:
                 *   -  if the URL is absolute URL we return it intact
                 *   -  else if the URL is relative and is a video we use a base URL
                 *   -  otherwise we look into the item package to get the value
                 */
                var strategies = [{
                    name: 'adp',
                    handle: function (url) {
                        if (url.source.indexOf("\[\[ADP\]\]") >= 0) {
                            // alert('Found [[ADP]] in url: ' + url);
                            // return url.source.replace(/\[\[ADP\]\]/g, "img\/");
                            var url2 = url.source.replace(/\[\[ADP\]\]/g, $Global$.apiHost + '\/api\/content\/' + self.global.settings.api.token + '\/' + self.global.settings.test.id + '\/');
                            return url2;
                        }
                    }
                },{
                    name: 'external',
                    handle: function handleExternal(url) {
                        if (url.source !== url.relative) {
                            return url.toString();
                        }
                    }
                },{
                    name: 'video',
                    handle: function handleVideo(url) {
                        //is relative and audio/video 
                        if (url.source === url.relative && /\.(mp4|avi|ogg|mp3)$/.test(url.file)) {
                            return 'samples/resources/' + url.file;
                        }
                    }
                },{
                    name: 'packed',
                    handle: function handlePacked(url) {
                        if (item.assessmentItemContent.assets) {
                            var packKey = url.toString();
                            for (var type in item.assessmentItemContent.assets) {
                                if (item.assessmentItemContent.assets[type][packKey]) {
                                    return item.assessmentItemContent.assets[type][packKey];
                                }
                            }
                        }
                        return url.toString();
                    }
                }];

                if (self.options.itemRunner) {
                    // If you get here, something is wrong :) - Clarification so you would have a chance to understand this comment - itemRunner instance is now always destroyed on the 'navfrom' event, so this should always be null when you come here.
                    try {
                        self.options.itemRunner.clear();
                        self.options.itemRunner = null;
                    }
                    catch (error) {
                        console.log(error);
                    }
                }

                // Order of events is: init, render, ready, setstate
                var itemData = item.assessmentItemContent.data ? item.assessmentItemContent.data : item.assessmentItemContent;
                console.log(itemData);
           
                self.options.itemRunner = itemRunnerFactory(itemData)
                        .on('ready', function (event) {
                            fp({ type: 'ready', value: null, selector: is });

                            // if the value is null then we do not need to assemble a state - instead just grab the state and use it as a template.
                            // this way, we force IR to give us proper structure instead of dreaming one up for each type!
                            if (!item.dynamic.state) {
                                item.dynamic.state = self.options.itemRunner.getState();
                            }
                            self.options.itemRunner.setState(item.dynamic.state);
                            fp({ type: 'setstate', value: item.dynamic.state, selector: is });
                        })
                        .on('render', function (event) {
                            
                            fp({ type: 'render', value: null, selector: is });

                        })
                        .on('init', function (event) {

                            fp({ type: 'init', value: null, selector: is });

                            if (typeof $rw_setBookId === "function") {
                                $rw_setBookId(self.global.settings.test.id.toString());
                            }

                            if (typeof $rw_setPageId === "function") {
                                $rw_setPageId(self.global.data.testPart[is.test].assessmentSection[is.section].assessmentItem[is.item].itemId);
                            }

                            if (typeof $rw_tagSentencesForDynamicSection === "function") {
                                $rw_tagSentencesForDynamicSection(document.getElementById("item"));
                            }

                        })
                        .on('clear', function (event) {
                            // alert('On Clear received!');
                        })
                        .on('statechange', function (event) {
                            fp({ type: 'state', value: self.options.itemRunner.getState(), selector: is });
                        })
                        .on('responsechange', function (event) {
                            fp({ type: 'response', value: self.options.itemRunner.getResponses(), selector: is });
                        })
                        .on('endattempt', function (identifier) {
                            fp({ type: 'endattempt', value: identifier, selector: is });
                        })
                        .on('error', function (error) {
                            fp({ type: 'error', value: error, selector: is });
                        })
                        .assets(strategies)
                        .init()
                        .render(ic);
                });
          
        },

        _callback: function (event) {

            var self = this;
            // this.element.model('update', event);

            // TODO: Shall I move model change notifications into the model, after the model has been changed?
            // this.element.triggerHandler('testevent', event);
            console.log('Event received: ' + event.type);
            if (event.type === "response" && !event.selector.notScored && !event.selector.systemItem) {
                this._score(event);
            }

            // Disconnect from the item runner
            //setTimeout($.proxy(function () {
                self._trigger("irevent", null, event);
            //}, this), 100);


            if (event.type === "render") {
                this.options.targetElement = $(".qti-itemBody")[0]; // $(".td-item-interaction#item")[0];
                if (this.options.targetElement) {
                    /*
                    $(".td-item-interaction-container").scroller({
                        target: {
                            selector: ".qti-itemBody" // ".td-item-interaction#item"
                        },
                        arrows: {
                            show: false
                        }
                    });
                    */
                }

                // TTS must be spawned on their own global - so in the context of this, notice the timeout of 0 (it is not delay that is an issue).
                setTimeout($.proxy(function () {
                    if (typeof $rw_tagSentences === "function") {
                        $rw_tagSentences();
                    }
                }, this), 0);
                // if (typeof $rw_tagSentencesForDynamicSection === "function") {
                //    $rw_tagSentencesForDynamicSection(document.getElementById("item"));
                //}

                //$("#item img").each(function (index, image) {
                //    var altAttribute = $(this).attr("alt");
                //    var msgAttribute = $(this).attr("msg");
                //    if (altAttribute && !msgAttribute) {
                //        $(this).attr("msg", altAttribute);
                //    }
                //});

            }

            if (event.type === "setstate") {
                this._centerVertically();
            }
        },

        _score: function (event) {

            var item = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item];
            var itemData = item.assessmentItemContent.data ? item.assessmentItemContent.data : item.assessmentItemContent;

            try {
        
                require([
                    'qtiScorer'
                ], function (scoringFactory) {

                    //////////// Failure #1 - OAT issue ////
                    /*
                    var scorer = scoringFactory('qti')
                        .on('error', function (er) {
                            item.dynamic.score.value = er;
                            item.dynamic.score.error = true;
                        })
                        .on('outcome', function (val) {
                            item.dynamic.score.value = val;
                            item.dynamic.score.error = false;
                        })
                        .process(event.value.length > 1 ? event.value : event.value[0], itemData);
                        */
                    ///////////////////////////////////////
                    //////////// Failure #2 - OAT issue ////
                    /*
                    item.dynamic.score.value = [];

                    $.each(event.value, function (index, response) {
                        var scorer = scoringFactory('qti')
                            .on('error', function (er) {
                                item.dynamic.score.value.push(er);
                                item.dynamic.score.error = true;
                            })
                            .on('outcome', function (val) {
                                item.dynamic.score.value.push(val);
                                item.dynamic.score.error = false;
                            })
                            .process(response, itemData);
                    });
                    */
                    ///////////////////////////////////////

                    var scorer = scoringFactory('qti')
                        .on('error', function (er) {
                            item.dynamic.score.value = er;
                            item.dynamic.score.error = true;
                        })
                        .on('outcome', function(val){
                            item.dynamic.score.value = val;
                            item.dynamic.score.error = false;
                        })
                        .process(event.value, itemData);
                        // .process(event.value[0], itemData);
                });
            }
            catch (error) {
                alert('Scoring Library has thrown: ' + error);
                item.dynamic.score.value = null;
                item.dynamic.score.error = true;
            }
        },

        toggleLineReader: function () {
            this.options.linereader.linereader("toggle");
        },

        closeLineReader: function(){
            this.options.linereader.linereader("close");
        },

        _centerVertically: function () {

            var containerSelector = ".td-item-interaction-container";
            var itemSelector = ".td-item-interaction#item";

            var container = $(containerSelector)[0];
            var item = $(itemSelector)[0];

            var containerHeight = $(container).height();
            var itemHeight = $(item).height();

            var marginTop = 1;
            if ((containerHeight - 95 - 128 ) > itemHeight) { // 95 is stickyBottomHeader height, 65 is additional "pretty" offset
                marginTop = Math.ceil((containerHeight - 95 - 128 - itemHeight) / 2);
                marginTop += "px";
            }

            console.log('Container height is: ' + containerHeight, 'Item height is: ' + itemHeight);
            $(".td-item-interaction#item").css({
                "margin-top": marginTop,
                "visibility": "visible"
            });
        }
    });
})(jQuery);
