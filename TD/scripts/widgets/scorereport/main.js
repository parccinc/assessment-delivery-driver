(function ($) {

    $.SCOREREPORT = {

        namespaceMap: {
            "Math Comprehension": "mathComprehension",
            "Math Fluency": "mathFluency",
            "ELA Reader Comprehension": "readingComprehension",
            "ELA Vocabulary": "readingComprehension",
            "ELA Reader Motivation Survey":"readerMotivation"
        },

        create: function (data) {
            if (data && data.settings && data.settings.test && data.settings.test.scoreReport && typeof data.settings.test.scoreReport !== "undefined" && data.settings.test.scoreReport.length) {
                if (typeof $[this.namespaceMap[data.settings.test.scoreReport]] !== "undefined" && typeof $[this.namespaceMap[data.settings.test.scoreReport]].create === "function") {
                    $[this.namespaceMap[data.settings.test.scoreReport]].create(data);
                }
                else {
                    $.generic.create(data);
                }
            }
        },

        getHtml: function (data) {
            if (data && data.settings && data.settings.test && data.settings.test.scoreReport && typeof data.settings.test.scoreReport !== "undefined" && data.settings.test.scoreReport.length) {
                if (typeof $[this.namespaceMap[data.settings.test.scoreReport]] !== "undefined" && typeof $[this.namespaceMap[data.settings.test.scoreReport]].getHtml === "function") {
                    $[this.namespaceMap[data.settings.test.scoreReport]].getHtml(data);
                }
                else {
                    $.generic.getHtml(data);
                }
            }
        }
    };

})(jQuery);