(function ($) {

    $.mathComprehension = {

        create: function (data) {
            // Note: All score reports are done on the test part level. Currently we can only have one score report type since it is passed in the login configuration
            //       for the battery (test). Theoretically we could and should have multile tests of different types but we needed smarter BA's or other "leaders" to understand that.
            this.data = data;
            this._createResults();
        },

        getHtml: function (data) {
            this._getMarkup();
        },

        _createResults: function () {

            var self = this;
            $.each(this.data.data.testPart, function (index, testPart) {
                self._create(index);
            });
            this._aggregate();
        },

        _create: function (testPartIndex) {

            var self = this;
            var report = {};

            // Please note that this loop is not entirely accurate - I should have gone through the testPart history and through the assessmentSection history - not only item history to generate report.
            $.each(this.data.data.testPart[testPartIndex].assessmentSection, function (sectionIndex, assessmentSection) {
                $.each(assessmentSection.dynamic.history, function (itemHistoryIndex, itemPoolIndex) {
                    console.log('Checking item in test part: ' + testPartIndex + ', section: ' + sectionIndex + ', history index: ' + itemHistoryIndex + ', pool index: ' + itemPoolIndex);
                    var item = assessmentSection.assessmentItem[itemPoolIndex];

                    // Stupidity begins NOW
                    if (!$.UTILITY.isSystemItem(item)) {
                        // So I excluded all system items. Theoretically I should exclude all "nonScored" items but nobody said so :P
                        if (!$.UTILITY.isLinkingItem(item, self.data.settings.test.grade)) {

                            // I am grouping by the domain + cluster. If those do not exist on the item, I will use the string "UNKNOWN" so the nice report element will pop up and be rather obvious :P
                            var domain = typeof item["domain"] !== "undefined" && typeof item.domain === "string" && item.domain.length > 0 ? item.domain : "UNKNOWN";
                            var cluster = typeof item["cluster"] !== "undefined" && typeof item.cluster === "string" && item.cluster.length > 0 ? item.cluster : "UNKNOWN";
                            var evidenceStatement = typeof item["evidenceStatement"] !== "undefined" && typeof item.evidenceStatement === "string" && item.evidenceStatement.length > 0 ? item.evidenceStatement : "UNKNOWN";
                            var answered = $.UTILITY.isItemAnswered(item);
                            var answeredCorrectly = $.UTILITY.isItemAnsweredCorrectly(item);
                            var key = domain + "-" + cluster;

                            if (typeof report[key] === "undefined") {
                                report[key] = {
                                    totalItemsScored: 0,
                                    totalItemsAnsweredCorrectly: 0,
                                    evidenceStatements: {}
                                }
                            }
                            report[key].totalItemsScored++;
                            if (typeof report[key].evidenceStatements[evidenceStatement] === "undefined") {
                                        report[key].evidenceStatements[evidenceStatement] = {
                                            count: 0,
                                            total: 0,
                                            itemPoolIndices: []
                                        }
                            }
                            report[key].evidenceStatements[evidenceStatement].total++;
                            if (answeredCorrectly) {
                                report[key].totalItemsAnsweredCorrectly++;
                            }
                            else {
                                report[key].evidenceStatements[evidenceStatement].count++;
                                report[key].evidenceStatements[evidenceStatement].itemPoolIndices.push(itemPoolIndex);
                            }
                            
                            //if (answeredCorrectly) {
                            //    report[key].totalItemsAnsweredCorrectly++;
                            //}
                            //else {
                            //    if (typeof report[key].evidenceStatements[evidenceStatement] === "undefined") {
                            //        report[key].evidenceStatements[evidenceStatement] = {
                            //            count: 0,
                            //            total: 0,
                            //            itemPoolIndices: []
                            //        }
                            //    }
                            //    report[key].evidenceStatements[evidenceStatement].count++;
                            //    report[key].evidenceStatements[evidenceStatement].itemPoolIndices.push(itemPoolIndex);
                            //}
                        }
                    }
                });
            });

            console.log(report);
            this.data.data.testPart[testPartIndex].dynamic.scoreReport = report;
        },

        _aggregate: function () {

            var self = this;
            this.data.data.dynamic.scoreReport = {}; // This will overwrite existing score report - check it - TODO!!!
            // Aggregate all test part reports on the package level since this is what our loving and lovable POs do not understand - idiots :)
            $.each(this.data.data.testPart, function (index, testPart) {
                $.each(testPart.dynamic.scoreReport, function (key, report) {
                    if (typeof self.data.data.dynamic.scoreReport[key] === "undefined") {
                        self.data.data.dynamic.scoreReport[key] = report;
                    }
                    else {
                        self.data.data.dynamic.scoreReport[key].totalItemsScored += report.totalItemsScored;
                        self.data.data.dynamic.scoreReport[key].totalItemsAnsweredCorrectly += report.totalItemsAnsweredCorrectly;
                        $.each(report.evidenceStatements, function (evidence, statement) {
                            if (typeof self.data.data.dynamic.scoreReport[key].evidenceStatements[evidence] === "undefined") {
                                self.data.data.dynamic.scoreReport[key].evidenceStatements[evidence] = statement;
                            }
                            else {
                                self.data.data.dynamic.scoreReport[key].evidenceStatements[evidence].count += statement.count;
                                $.merge(self.data.data.dynamic.scoreReport[key].evidenceStatements[evidence].itemPoolIndices, statement.itemPoolIndices);
                            }
                        });
                    }
                });
            });
        },

        _getMarkup: function () {

            var self = this;
            var data = this.data.data.dynamic.scoreReport;
            var html = this._preamble();

            html += this._header();

            $.each(data, function (key, report) {
                html += self._reportDetail(key, report);
            });

            html += this._epilogue();
            
            this.data.data.dynamic.html = $.UTILITY.utf8toBase64(html);
        },

        _preamble: function () {
            var html = 
            "<!DOCTYPE html>" +
            "<html  lang='en' xmlns='http://www.w3.org/1999/xhtml'>" +
                "<head>" +
	                "<meta charset='utf-8'/>" +
	                "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>" +
	                "<title>Math Comprehension</title>" +
                    "<style type='text/css'>" +
                        'div.sr-header,div.sr-bodytext,div.sr-table {font-family:"Open Sans", Arial, Helvetica, sans-serif;max-width:900px;min-width:580px;}' +
                        '.sr-header{text-align:left;margin:25px;border-bottom:1px solid #d8d8d8;font-size:32px;font-weight:700;}' +
                        '.sr-testheading{font-size:24px;font-weight:700;}' +
                        '.sr-date{padding-top:5px;padding-bottom:15px;font-size:16px;font-weight:400;font-family:Verdana;}' +
                        '.sr-studentinfo{padding-bottom:5px;}' +
                        '.sr-summary{background-color:#f3f2f2;width:100%;height:150px;color:#454545;font-weight:700;text-align:center;font-size:14px;width:480px;word-wrap:break-word;display:inline-block;margin:25px auto;}' +
                        '.sr-passage{padding-bottom:30px;}' +
                        '.sr-passage-title{text-align:center;font-size:24px;font-weight:700;}' +
                        '.sr-summary-primary{background-color:#72547d;height:150px;width:33%;float:left;color:#fff;}' +
                        '.sr-summary-secondary{height:150px;border-right:1px solid #e2e2e2;width:33%;float:left;}' +
                        '.sr-summary-middle{font-size:58px;padding-top:10px;font-weight:400;}' +
                        '.sr-summary-center{width:85px;margin:10px auto;font-family:"Open Sans Condensed", Arial, Helvetica, sans-serif;font-weight:700;}' +
                        '.sr-detailed-score{padding-top:15px;padding-bottom:20px;padding-left:55px;}' +
                        '.sr-table table,.sr-table tr {border:thin solid rgba(111,126,149,0.25);}' +
                        'table.sr-table{background-color:#fff;border-collapse:collapse;text-align:center;word-wrap:break-word;width:90%;}' +
                        '.sr-table tr{height:42px;}' +
                        '.sr-table th{background-color:rgb(111,126,149);text-align:center;font-weight:bold;padding:0 20px;color:#fff;}' +
                        '.sr-table td{background-color:#fff;text-align:center;padding:0 20px;}' +
                        '.sr-table td.sr-left{background-color:#f3f2f2;width:100px;}' +
                        '.sr-statement{padding-top:5px;font-size:16px;}' +
                    '</style>' +
                "</head>" +
                "<body class='sr-body'>";
            return html;
        },
       
        _header: function () {
            return this._headerTestInfo() + this._headerDate() + this._headerCandidateInfo();
        },

        _headerTestInfo: function () {
            return "<div class='sr-header'><div>" + this.data.settings.test.name + "</div>";
        },

        _headerDate: function () {
            return "<div class='sr-date'>Test Date: " + new Date().toLocaleDateString() + "</div>";
        },

        _headerCandidateInfo: function () {
            return "<div class='sr-studentinfo'>" + this.data.settings.candidate.firstName + " " + this.data.settings.candidate.lastName + ", Grade " + this.data.settings.candidate.grade + "</div></div><div class='sr-bodytext'>";
        },

        _reportDetail: function (key, report) {
            return this._reportDetailKey(key) + this._reportDetailTable(report) + this._reportDetailEvidenceStatements(report);
        },

        _reportDetailKey: function (key) {
            // return "<div style='margin:25px;padding-bottom:30px;'><div class='sr-testheading'>" + key + "</div>";
            return "<div style='margin:25px;padding-bottom:30px;'><div class='sr-testheading'>" + "<a href='standard:" + key + "'>" + key + "</a>" + "</div>";
        },

        _reportDetailTable: function (report) {
            var html = 
                        '<div class="sr-summary">' +
                            '<div class="sr-answeredCorrect sr-summary-secondary">' +
                                '<div class="sr-summary-middle">' + report.totalItemsAnsweredCorrectly + '</div>' +
                                    '<div class="sr-summary-center">POINTS EARNED</div>' +
                            '</div>' +
                            '<div class="sr-totalQuestions sr-summary-secondary">' +
                                '<div class="sr-summary-middle">' + report.totalItemsScored + '</div>' +
                                    '<div class="sr-summary-center">POINTS POSSIBLE</div>' +
                            '</div>' +
                            '<div class="sr-percentCorrect sr-summary-primary">' +
                                '<div class="sr-summary-middle">' + Math.round(100 * report.totalItemsAnsweredCorrectly / report.totalItemsScored) + '%</div>' +
                                    '<div class="sr-summary-center">PERCENT CORRECT</div>' +
                            '</div>' +
                        '</div>';
            return html;
        },

        _reportDetailEvidenceStatements: function (report) {
            var self = this;
            var html = "";
            if (Object.keys(report.evidenceStatements).length) {
                html += this._reportDetailEvidenceStatementHeader(report);
                $.each(report.evidenceStatements, function (evidence, statement) {
                    if (statement.count > 0) {
                        html += self._reportDetailEvidenceStatementDetail(evidence, statement);
                    }
                });
            }
            html+="</div>"
            return html;
        },

        _reportDetailEvidenceStatementHeader: function (report) {
            return "<div class='sr-statement'>" + "One or more questions were answered incorrectly for the following:" + "</div>";
        },

        _reportDetailEvidenceStatementDetail: function (evidence, statement) {
            return "<div class='sr-statement'>" + "<a href='standard:" + evidence + "'>" + evidence + "</a>" + " - " + statement.count + " of " + statement.total + (statement.total === 1 ? " question" : " questions") + " incorrect</div>";
        },

        _epilogue: function () {
            return "</div></body></html>";
        }
    };

})(jQuery);