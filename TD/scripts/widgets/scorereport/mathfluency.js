(function ($) {

    $.mathFluency = {
        create: function (data) {
            this.data = data;
            this._createResults();
            console.log(this.data.data.dynamic.scoreReport);
        },

        _createResults: function () {
            var self = this;
            $.each(this.data.data.testPart, function (index, testPart) {
                self._create(index);
                console.log(testPart.dynamic.scoreReport);
            });
            this._aggregate();
        },

        _create: function (index) {
            this.data.data.testPart[index].dynamic.scoreReport = {};
            this._getData(this.data.data.testPart[index].assessmentSection, this.data.data.testPart[index].dynamic.scoreReport);
        },

        getHtml: function (data) {
            this._createHtml();
        },

        _getData: function (obj, test) {
            var self = this;
            $.each(obj, function (index, items) {
                $.each(items.assessmentItem, function (key, item) {
                    if (!$.UTILITY.isSystemItem(item)) {
                        if (test.subscores) {
                            if (item.itemStrand in test.subscores) {
                                self.addItemStrandData(test.subscores[item.itemStrand], item);
                            }
                            else {
                                self.addItemStrand(test.subscores, item.itemStrand);
                                self.addItemStrandData(test.subscores[item.itemStrand], item);
                            }
                        }
                        else {
                            self.addSubscores(test);
                            self.addItemStrand(test.subscores, item.itemStrand);
                            self.addItemStrandData(test.subscores[item.itemStrand], item);
                        }
                    }
                })
            });
            if (test.subscores) {
                $.each(test.subscores, function (strand, strandDetails) {
                    if ($.type(strandDetails) === "object") {
                        test.subscores.answeredCorrectly += strandDetails.answeredCorrectly;
                        test.subscores.totalQuestions += strandDetails.totalQuestions;
                        test.subscores.timeSpent += strandDetails.timeSpent;
                    }
                });
                test.subscores.percentCorrect = Number(Math.round(test.subscores.answeredCorrectly * 100 / test.subscores.totalQuestions));
            }
        },

        addSubscores: function (obj) {
            obj.subscores = {
                answeredCorrectly: 0,
                totalQuestions: 0,
                timeSpent: 0
            };
        },

        addItemStrand: function(obj, item){
            obj[item] = {
                answeredCorrectly: 0,
                totalQuestions: 0,
                timeSpent: 0
            }
        },

        addItemStrandData: function (obj, item) {
            obj.totalQuestions++;
            obj.answeredCorrectly += $.UTILITY.isItemAnsweredCorrectly(item);
            obj.timeSpent += $.UTILITY.timeSpentOnItem(item);
        },
        
        _aggregate: function () {
            var self = this;
            this.data.data.dynamic.scoreReport = {};
            $.each(this.data.data.testPart, function (index, testPart) {
                $.each(testPart.dynamic.scoreReport, function (key, report) {
                    if (typeof self.data.data.dynamic.scoreReport[key] === "undefined") {
                        self.data.data.dynamic.scoreReport[key] = report;
                    }
                    else {
                        $.each(report, function (strand, strandDetails) {
                            if (strandDetails) {
                                if (typeof self.data.data.dynamic.scoreReport[key][strand] === "undefined") {
                                    self.data.data.dynamic.scoreReport[key][strand] = strandDetails;
                                }
                                else {
                                    if ($.type(strandDetails) === "object") {
                                        self.data.data.dynamic.scoreReport[key][strand].totalQuestions += strandDetails.totalQuestions;
                                        self.data.data.dynamic.scoreReport[key][strand].answeredCorrectly += strandDetails.answeredCorrectly;
                                        self.data.data.dynamic.scoreReport[key][strand].timeSpent += strandDetails.timeSpent;
                                    }
                                    else {
                                        self.data.data.dynamic.scoreReport[key][strand] += strandDetails;
                                    }
                                }
                            }
                        })
                    };
                })
            });
            $.each(this.data.data.dynamic.scoreReport, function (key, report) {
                report.percentCorrect = Number(Math.round(report.answeredCorrectly * 100 / report.totalQuestions));
                
            })
        },

        _createHtml: function () {
            var self = this;
            var html = '';
            var obj = this.data.data.dynamic.scoreReport;
            var examDate = new Date();
            var examDateFormatted = ("0" + (examDate.getMonth() + 1)).slice(-2) + '/' + ("0" + examDate.getDate()).slice(-2) + '/' + examDate.getFullYear();
            html =
                '<html lang="en" xmlns="http://www.w3.org/1999/xhtml">' +
                '<head>' +
                    '<meta charset="utf-8"/>' +
                    '<title>Score Report</title>' +
	                '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>' +
                    '<style type="text/css">' +
                        'div.sr-header,div.sr-bodytext,div.sr-table {font-family:"Open Sans", Arial, Helvetica, sans-serif;max-width:900px;min-width:580px;}' +
                        '.sr-header{text-align:left;margin:25px;border-bottom:1px solid #d8d8d8;font-size:32px;font-weight:700;}' +
                        '.sr-testheading{font-size:24px;font-weight:700;}' +
                        '.sr-date{padding-top:5px;padding-bottom:15px;font-size:16px;font-weight:400;font-family:Verdana;}' +
                        '.sr-studentinfo{padding-bottom:5px;}' +
                        '.sr-summary{background-color:#f3f2f2;width:100%;height:150px;color:#454545;font-weight:700;text-align:center;font-size:14px;width:590px;word-wrap:break-word;display:inline-block;margin:25px auto;}' +
                        '.sr-passage{padding-bottom:30px;}' +
                        '.sr-passage-title{text-align:center;font-size:24px;font-weight:700;}' +
                        '.sr-summary-primary{background-color:#72547d;height:150px;width:25.5%;float:left;color:#fff;border-right:1px solid #e2e2e2;}' +
                        '.sr-summary-secondary{height:150px;border-right:1px solid #e2e2e2;width:24.5%;float:left;}' +
                        '.sr-summary-middle{font-size:42px;padding-top:18px;font-weight:400;}' +
                        '.sr-summary-center{width:85px;margin:10px auto;font-family:"Open Sans Condensed", Arial, Helvetica, sans-serif;font-weight:700;}' +
                        '.sr-detailed-score{padding-top:15px;padding-bottom:20px;padding-left:55px;}' +
                        '.sr-table table,.sr-table tr {border:thin solid rgba(111,126,149,0.25);}' +
                        'table.sr-table{background-color:#fff;border-collapse:collapse;text-align:center;word-wrap:break-word;width:90%;}' +
                        '.sr-table tr{height:42px;}' +
                        '.sr-table th{background-color:rgb(111,126,149);text-align:center;font-weight:bold;padding:0 20px;color:#fff;}' +
                        '.sr-table td{background-color:#fff;text-align:center;padding:0 20px;}' +
                        '.sr-table td.sr-left{background-color:#f3f2f2;width:50%;text-align:left;}' +
                        '.sr-statement{padding-top:5px;font-size:16px;}' +
                    '</style>' +
                '</head>' +
                '<body class="sr-body">' +
                    '<div class="sr-header">';
            html += '<div class="sr-testinfo">' + this.data.settings.test.name + '</div>';
            html += '<div class="sr-date">Test Date: ' + examDateFormatted + '</div>';
            html += '<div class="sr-studentinfo">' + this.data.settings.candidate.firstName + " " + this.data.settings.candidate.lastName + ', Grade ' + this.data.settings.candidate.grade + '</div></div><div class="sr-bodytext">';
            $.each(obj, function (key, value) {
                html +=
                    '<div class="sr-passage">' +
                        '<div class="sr-passage-title">' +
                            '<div class="sr-summary">' +
                                '<div class="sr-summary-secondary">' +
                                    '<div class="sr-summary-middle">' + value.answeredCorrectly + '</div>' +
                                    '<div class="sr-summary-center">ANSWERED CORRECTLY</div>' +
                                '</div>' +
                                '<div class="sr-summary-secondary">' +
                                    '<div class="sr-summary-middle">' + value.totalQuestions + '</div>' +
                                    '<div class="sr-summary-center">TOTAL QUESTIONS</div>' +
                                '</div>' +
                                '<div class="sr-summary-primary">' +
                                    '<div class="sr-summary-middle">' + value.percentCorrect + '%</div>' +
                                    '<div class="sr-summary-center">PERCENT CORRECT</div>' +
                                '</div>' +
                                '<div class="sr-summary-secondary">' +
                                    '<div class="sr-summary-middle">' + self._minSecFormat(value.timeSpent) + '</div>' +
                                    '<div class="sr-summary-center">TIME SPENT (Mins:Secs)</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="sr-detailed-score">' +
                            '<table class="sr-table">' +
                                '<tr><th style="text-align:left">Subskill</th><th>Answered Correctly</th><th>Time Spent (Mins:Secs)</th></tr>';
                $.each(value, function (keyStrand, strand) {
                    if ($.type(strand) === "object" && 'answeredCorrectly' in strand && 'totalQuestions' in strand && 'timeSpent' in strand) {
                        html += '<tr><td class="sr-left"><a href="standard:' + keyStrand + '">' + keyStrand + '</a></td><td>' + strand.answeredCorrectly + ' / ' + strand.totalQuestions + '</td><td>' + self._minSecFormat(strand.timeSpent) + '</td></tr>';
                    }
                });
                html += '</table></div></div>';
            });
            html += '</div></body></html>';
            this.data.data.dynamic.html = $.UTILITY.utf8toBase64(html);
        },

        _minSecFormat: function (time) {
            var min = (time / 1000 / 60) << 0;
            var sec = Math.ceil(time / 1000 - min * 60);
            min = min < 10 ? "0" + min : min;
            sec = sec < 10 ? "0" + sec : sec;
            return min + ":" + sec;
        }
    }

})(jQuery);
