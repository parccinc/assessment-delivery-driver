(function ($) {

    $.readingComprehension = {

        create:function(data){
            this.data = data;
            this._createResults();
            console.log(this.data.data.dynamic.scoreReport);
        },

        _createResults: function(){
            var self = this;
            $.each(this.data.data.testPart, function (index, testPart) {
                self._create(index);
                console.log(testPart.dynamic.scoreReport);
            });
            this._aggregate();
        },
            
        _create: function (index) {
            this.data.data.testPart[index].dynamic.scoreReport = {};
            this._getData(this.data.data.testPart[index].assessmentSection, this.data.data.testPart[index].dynamic.scoreReport);
        },

        getHtml: function (data) {
            this._createHtml();
        },

        _getData: function (obj, test) {
            var self = this;
            $.each(obj, function (index, items) {
                $.each(items.assessmentItem, function (key, item) {
                    if (!$.UTILITY.isSystemItem(item)) {
                        var passage = item.passageType; /*(item.passageType === "Informational" || item.passageType === "Literary") ? item.passageType : "Unknown";*/
                        if (test) {
                            if (passage in test) {
                                self.addGradeDataToPassageType(test, item);
                                self.addSkillDataToPassageType(test, item);
                                self.itemCount(test[passage], item);
                            }
                            else {
                                self.addPassageType(test, passage);
                                self.generateGradeData(test[passage].detailsByGrade, item);
                                self.addItemToSkill(test[passage].detailsBySkill, item);
                                self.itemCount(test[passage], item);
                            }
                        }

                        else {
                            self.addPassageType(test, passage);
                            self.generateGradeData(test[passage].detailsByGrade, item);
                            self.addItemToSkill(test[passage].detailsBySkill, item);
                            self.itemCount(test[passage], item);
                        }
                    };
                });
            });
        },

        generateGradeData: function (obj, item) {
            obj[item.itemGrade] = {
                totalQuestions: item.maxScorePoints,
                correctlyAnsweredQuestions: $.UTILITY.getItemScore(item) ? $.UTILITY.getItemScore(item) : 0
            };
        },

        addPassageType: function (obj, item) {
            obj[item] = {
                answeredCorrectly: 0,
                testTotalQuestions: 0,
                detailsBySkill: {},
                detailsByGrade: {}
            }
        },

        addGradeDataToPassageType: function (obj, item) {
            var self = this;
            var passage = item.passageType; /*(item.passageType === "Informational" || item.passageType === "Literary") ? item.passageType : "Unknown";*/
            if (obj[passage].detailsByGrade) {
                if (item.itemGrade in obj[passage].detailsByGrade) {
                    self.addItemCalculation(obj[passage].detailsByGrade[item.itemGrade], item);
                }
                else {
                    self.generateGradeData(obj[passage].detailsByGrade, item);
                }
            }
            else {
                self.generateGrade(obj[passage], item);
            }
        },

        addItemCalculation: function (obj, item) {
            obj.totalQuestions += item.maxScorePoints;
            obj.correctlyAnsweredQuestions += $.UTILITY.getItemScore(item) ? $.UTILITY.getItemScore(item) : 0;
        },

        addSkillDataToPassageType:function(obj, item){
            var self = this;
            var passage = item.passageType; /*(item.passageType === "Informational" || item.passageType === "Literary") ? item.passageType : "Unknown";*/
            if (obj[passage].detailsBySkill) {
                self.addItemToSkill(obj[passage].detailsBySkill, item);
            }
            else{
                self.addPassageType(obj, passage);
                self.addItemToSkill(obj[passage].detailsBySkill)
            };

        },

        addItemToSkill:function(obj, item){
            if ($.UTILITY.getItemScoringType(item) === "EBSR") {
                this.addEbsrItem(obj, item);
            }
            else {
                this.addMcMsItem(obj, item);
            }
        },

        addEbsrItem: function (obj, item) {
            if (!obj.Accuracy) {
                this.addAccuracy(obj);
            }
            if (!obj.Evidence) {
                this.addEvidence(obj);
            }
            this.addEbsrToSkillCount(obj, item);
        },

        addMcMsItem: function (obj, item) {
            if (item.subclaim === "accuracy") {
                this.addAccuracyToSkillCount(obj, item);
            }
            else {
                this.addEvidenceToSkillCount(obj, item);
            }
        },

        addEbsrToSkillCount: function (obj, item) {
            var score = $.UTILITY.getItemScore(item);
            obj.Accuracy.totalQuestions++;
            obj.Accuracy.correctlyAnsweredQuestions += (score === 1 || score === 2) ? 1 : 0;
            obj.Evidence.totalQuestions++;
            obj.Evidence.correctlyAnsweredQuestions += (score === 2) ? 1 : 0;
        },

        addAccuracyToSkillCount: function (obj, item) {
            if (!obj.Accuracy) {
                this.addAccuracy(obj);
            };
            obj.Accuracy.totalQuestions++;
            obj.Accuracy.correctlyAnsweredQuestions += $.UTILITY.getItemScore(item) ? $.UTILITY.getItemScore(item) : 0;
        },

        addEvidenceToSkillCount: function (obj, item) {
            if (!obj.Evidence) {
                this.addEvidence(obj);
            };
            obj.Evidence.totalQuestions++;
            obj.Evidence.correctlyAnsweredQuestions += $.UTILITY.getItemScore(item) ? $.UTILITY.getItemScore(item) : 0;
        },

        addAccuracy: function (obj) {
            obj.Accuracy = {
                totalQuestions: 0,
                correctlyAnsweredQuestions: 0
            };
        },

        addEvidence: function (obj) {
            obj.Evidence = {
                totalQuestions: 0,
                correctlyAnsweredQuestions: 0
            };
        },

        itemCount: function (obj, item) {
            obj.testTotalQuestions += $.UTILITY.getItemScoringType(item) === "EBSR" ? 2 : 1;
            obj.answeredCorrectly += $.UTILITY.getItemScore(item) ? $.UTILITY.getItemScore(item) : 0;
        },

        _aggregate: function () {
            var self = this;
            this.data.data.dynamic.scoreReport = {};
            $.each(this.data.data.testPart, function (index, testPart) {
                $.each(testPart.dynamic.scoreReport, function (key, report) {
                    if (typeof self.data.data.dynamic.scoreReport[key] === "undefined") {
                        self.data.data.dynamic.scoreReport[key] = report;
                    }
                    else {
                        self.data.data.dynamic.scoreReport[key].answeredCorrectly += report.answeredCorrectly;
                        self.data.data.dynamic.scoreReport[key].testTotalQuestions += report.testTotalQuestions;
                        $.each(report.detailsBySkill, function (skill, skillDetails) {
                            if (skillDetails) {
                                if (typeof self.data.data.dynamic.scoreReport[key].detailsBySkill[skill] === "undefined") {
                                    self.data.data.dynamic.scoreReport[key].detailsBySkill[skill] = skillDetails;
                                }
                                else {
                                    self.data.data.dynamic.scoreReport[key].detailsBySkill[skill].totalQuestions += skillDetails.totalQuestions;
                                    self.data.data.dynamic.scoreReport[key].detailsBySkill[skill].correctlyAnsweredQuestions += skillDetails.correctlyAnsweredQuestions;
                                }
                            }
                            
                        });
                        $.each(report.detailsByGrade, function (grade, gradeDetails) {
                            if (typeof self.data.data.dynamic.scoreReport[key].detailsByGrade[grade] === "undefined") {
                                self.data.data.dynamic.scoreReport[key].detailsByGrade[grade] = gradeDetails;
                            }
                            else {
                                self.data.data.dynamic.scoreReport[key].detailsByGrade[grade].totalQuestions += gradeDetails.totalQuestions;
                                self.data.data.dynamic.scoreReport[key].detailsByGrade[grade].correctlyAnsweredQuestions += gradeDetails.totalQuestions;
                            }
                        });
                    }
                });
            });
        },

        _createHtml: function () {
            var html = "";
            var obj = this.data.data.dynamic.scoreReport;
            var examDate = new Date();
            var examDateFormatted = ("0" + (examDate.getMonth() + 1)).slice(-2) + '/' + ("0" + examDate.getDate()).slice(-2) + '/' + examDate.getFullYear();
            html =
                '<html lang="en" xmlns="http://www.w3.org/1999/xhtml">' +
                '<head>' +
                    '<meta charset="utf-8"/>' +
                    '<title>Score Report</title>' +
	                '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>' +
                    '<style type="text/css">' +
                        'div.sr-header,div.sr-bodytext,div.sr-table {font-family:"Open Sans", Arial, Helvetica, sans-serif;max-width:900px;min-width:580px;}' +
                        '.sr-header{text-align:left;margin:25px;border-bottom:1px solid #d8d8d8;font-size:32px;font-weight:700;}' +
                        '.sr-testheading{font-size:24px;font-weight:700;}' +
                        '.sr-date{padding-top:5px;padding-bottom:15px;font-size:16px;font-weight:400;font-family:Verdana;}' +
                        '.sr-studentinfo{padding-bottom:5px;}' +
                        '.sr-summary{background-color:#f3f2f2;width:100%;height:150px;color:#454545;font-weight:700;text-align:center;font-size:14px;width:480px;word-wrap:break-word;display:inline-block;margin:25px auto;}' +
                        '.sr-passage{padding-bottom:30px;}' +
                        '.sr-passage-title{text-align:center;font-size:24px;font-weight:700;}' +
                        '.sr-summary-primary{background-color:#72547d;height:150px;width:33%;float:left;color:#fff;}' +
                        '.sr-summary-secondary{height:150px;border-right:1px solid #e2e2e2;width:33%;float:left;}' +
                        '.sr-summary-middle{font-size:58px;padding-top:10px;font-weight:400;}' +
                        '.sr-summary-center{width:85px;margin:10px auto;font-family:"Open Sans Condensed", Arial, Helvetica, sans-serif;font-weight:700;}' +
                        '.sr-detailed-score{padding-top:15px;padding-bottom:20px;padding-left:55px;}' +
                        '.sr-table table,.sr-table tr {border:thin solid rgba(111,126,149,0.25);}' +
                        'table.sr-table{background-color:#fff;border-collapse:collapse;text-align:center;word-wrap:break-word;width:90%;}' +
                        '.sr-table tr{height:42px;}' +
                        '.sr-table th{background-color:rgb(111,126,149);text-align:center;font-weight:bold;padding:0 20px;color:#fff;}' +
                        '.sr-table td{background-color:#fff;text-align:center;padding:0 20px;}' +
                        '.sr-table td.sr-left{background-color:#f3f2f2;width:100px;}' +
                        '.sr-statement{padding-top:5px;font-size:16px;}' +
                    '</style>' +
                '</head>' +
                '<body class="sr-body">' +
                    '<div class="sr-header">';
            html += '<div class="sr-testinfo">' + this.data.settings.test.name + '</div>';
            html += '<div class="sr-date">Test Date: ' + examDateFormatted + '</div>';
            html += '<div class="sr-studentinfo">' + this.data.settings.candidate.firstName + " " + this.data.settings.candidate.lastName + ', Grade ' + this.data.settings.candidate.grade + '</div></div><div class="sr-bodytext">';
            $.each(obj, function (key, value) {
                html += 
                    '<div class="sr-passage">' +
                        '<div class="sr-passage-title">' +
                            '<div>' +
                                '<span>Passage Type: </span>' +
                                '<span>' + key + '</span>' +
                            '</div>' +
                            '<div class="sr-summary">' +
                                '<div class="sr-answeredCorrect sr-summary-secondary">' +
                                    '<div class="sr-summary-middle">' + value.answeredCorrectly + '</div>' +
                                    '<div class="sr-summary-center">POINTS EARNED</div>' +
                                '</div>' +
                                '<div class="sr-totalQuestions sr-summary-secondary">' +
                                    '<div class="sr-summary-middle">' + value.testTotalQuestions + '</div>' +
                                    '<div class="sr-summary-center">POINTS POSSIBLE</div>' +
                                '</div>' +
                                '<div class="sr-percentCorrect sr-summary-primary">' +
                                    '<div class="sr-summary-middle">' + (value.answeredCorrectly * 100 / value.testTotalQuestions).toFixed(0) + '%</div>' +
                                    '<div class="sr-summary-center">PERCENT CORRECT</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="sr-detailed-score">' +
                            '<div style="padding-bottom: 5px;">Detail by Skill</div>' +
                            '<table class="sr-table">' +
                                '<tr><th>Skill</th><th>Points Earned</th><th>Points Possible</th><th>Percent Correct</th></tr>';
                var skill = [];
                if (!$.isEmptyObject(value.detailsBySkill)) {
                    $.each(value.detailsBySkill, function (keySkill, skillData) {
                        skill.push(keySkill)
                    });
                    skill.sort();
                    $.each(skill, function (keySkill, valueSkill) {
                        if ($.type(value.detailsBySkill[valueSkill])==="object" && 'correctlyAnsweredQuestions' in value.detailsBySkill[valueSkill] && 'totalQuestions' in value.detailsBySkill[valueSkill]) {
                            html += '<tr><td class="sr-left">' + valueSkill + '</td><td>' + value.detailsBySkill[valueSkill].correctlyAnsweredQuestions + ' / ' + value.detailsBySkill[valueSkill].totalQuestions + '</td><td>' + value.detailsBySkill[valueSkill].totalQuestions + '</td><td>' + (value.detailsBySkill[valueSkill].correctlyAnsweredQuestions * 100 / value.detailsBySkill[valueSkill].totalQuestions).toFixed(2) + '%</td></tr>';
                        }
                    })
                };

                    i
                
                html +=
                    '</table>' +
                '</div>' +
                '<div class="sr-detailed-score">' +
                    '<div style="padding-bottom: 5px;">Detail by Grade</div>' +
                    '<table class="sr-table">' +
                        '<tr><th>Grade</th><th>Points Earned</th><th>Points Possible</th><th>Percent Correct</th></tr>';
                $.each(value.detailsByGrade, function (keyGrade, valueGrade) {
                    html += '<tr><td class="sr-left">Grade ' + keyGrade + '</td><td>' + valueGrade.correctlyAnsweredQuestions + ' / ' + valueGrade.totalQuestions + '</td><td>' + valueGrade.totalQuestions + '</td><td>' + (valueGrade.correctlyAnsweredQuestions * 100 / valueGrade.totalQuestions).toFixed(2) + '%</td></tr>';
                });
                html += '</table></div></div>';
            });
            html += '</div></body></html>';
            this.data.data.dynamic.html = $.UTILITY.utf8toBase64(html);
        }
    };

})(jQuery);