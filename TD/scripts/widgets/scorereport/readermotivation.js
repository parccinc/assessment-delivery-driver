(function ($) {

    $.readerMotivation = {

        create: function (data) {
            this.data = data;
            this._createResults();
            console.log(this.data.data.dynamic.scoreReport);
        },

        _createResults: function () {
            this.data.data.dynamic.scoreReport = {};
            this._getData(this.data.data.testPart[0].assessmentSection, this.data.data.dynamic.scoreReport);
        },

        getHtml: function () {
            this._createHtml();
        },

        _getData: function (obj, test) {
            var self = this;
            $.each(obj, function(index, items){
                $.each(items.assessmentItem, function (key, item) {
                    if (!$.UTILITY.isSystemItem(item)) {
                        if (test.category) {
                            if (item.reportingCategory in test.category) {
                                self._addItemToCategory(test.category[item.reportingCategory], item)
                            }
                            else {
                                self._addItemCategory(test.category, item.reportingCategory);
                                self._addItemToCategory(test.category[item.reportingCategory], item)
                            }
                        }
                        else {
                            self._addCategory(test);
                            self._addItemCategory(test.category, item.reportingCategory);
                            self._addItemToCategory(test.category[item.reportingCategory], item);
                        }
                    }
                })
            })
        },

        _addCategory:function(obj){
            obj.category = {}
        },

        _addItemCategory: function (obj, item) {
            obj[item] = [];
        },
        
        _addItemToCategory: function (obj, item) {
            var self = this;
            obj.push({
                "QuestionNo":item.questionNo,
                "StudentsResponse":$.UTILITY.isItemAnswered(item)?self._getResponse(item):"Not Answered"
            });
        },

        _getResponse: function (item) {
            if (typeof item.dynamic.response !== "undefined" && item.dynamic.response.length) {
                var index = 0;
                $.each(item.dynamic.response, function (key, value) {
                    if (value !== null) {
                        index = key;
                    }
                });
                if (item.dynamic.response[index].RESPONSE && item.dynamic.response[index].RESPONSE.base && typeof item.dynamic.response[index].RESPONSE.base !== "undefined" && item.dynamic.response[index].RESPONSE.base != null) {
                    if (item.dynamic.response[index].RESPONSE.base.identifier) {
                        return { text: false, content: item.dynamic.response[index].RESPONSE.base.identifier };
                    }
                    else {
                        if (item.dynamic.response[index].RESPONSE.base.string) {
                            return { text: true, content: item.dynamic.response[index].RESPONSE.base.string };
                        }
                        else {
                            return { text: true, content: "" }
                        }
                    }
                }
                else {
                    if (item.dynamic.response[index].RESPONSE && item.dynamic.response[index].RESPONSE.list && typeof item.dynamic.response[index].RESPONSE.list !== "undefined" && item.dynamic.response[index].RESPONSE.list != null) {
                        if (item.dynamic.response[index].RESPONSE.list.identifier.length > 0) {
                            return { text: false, content: item.dynamic.response[index].RESPONSE.list.identifier };
                        }
                        else {
                            return { text: true, content: "" }
                        }
                    }
                    else {
                        return { text: true, content: "" }
                    }
                }
            }
            
        },

        _createHtml: function () {
            var html = '';
            var obj = this.data.data.dynamic.scoreReport;
            var examDate = new Date();
            var examDateFormatted = ('0' + (examDate.getMonth() + 1)).slice(-2) + '/' + ('0' + examDate.getDate()).slice(-2) + '/' + examDate.getFullYear();
            html =
                '<html lang="en" xmlns="http://www.w3.org/1999/xhtml">' +
                '<head>' +
                    '<meta charset="utf-8"/>' +
                    '<title>Score Report</title>' +
	                '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>' +
                    '<style type="text/css">' +
                        'div.sr-header,div.sr-bodytext,div.sr-table,div.sr-link {font-family:"Open Sans", Arial, Helvetica, sans-serif;max-width:900px;min-width:580px;}' +
                        '.sr-header{text-align:left;margin:25px;border-bottom:1px solid #d8d8d8;font-size:32px;font-weight:700;}' +
                        '.sr-testheading{font-size:24px;font-weight:700;}' +
                        '.sr-date{padding-top:5px;padding-bottom:15px;font-size:16px;font-weight:400;font-family:Verdana;}' +
                        '.sr-studentinfo{padding-bottom:5px;}' +
                        '.sr-bodytext{padding-bottom:20px;}' +
                        '.sr-link{margin:30px;}' +
                        '.sr-detailed-score{padding-top:15px;padding-bottom:20px;}' +
                        '.sr-table table,.sr-table tr {border:thin solid rgba(111,126,149,0.25);}' +
                        'table.sr-table{background-color:#fff;border-collapse:collapse;text-align:center;word-wrap:break-word;width:90%;min-width: 500px;}' +
                        '.sr-table tr{height:42px;}' +
                        '.sr-table th{background-color:rgb(111,126,149);text-align:center;font-weight:bold;padding:0 20px;color:#fff;}' +
                        '.sr-table td{background-color:#fff;padding:0 20px;text-align:center;}' +
                        '.sr-table td.sr-left{background-color:#f3f2f2;width:70%;text-align:left;}' +
                        '.sr-table ul{padding-left:10px;}' +
                    '</style>' +
                '</head>' +
                '<body class="sr-body">' +
                    '<div class="sr-header">';
            html += '<div class="sr-testinfo">' + this.data.settings.test.name + '</div>';
            html += '<div class="sr-date">Test Date: ' + examDateFormatted + '</div>';
            html += '<div class="sr-studentinfo">' + this.data.settings.candidate.firstName + " " + this.data.settings.candidate.lastName + ', Grade ' + this.data.settings.candidate.grade + '</div></div><div class="sr-bodytext">';
            html += '<div class="sr-link">View the PDF version of the ELA Reader Motivation Survey: <a href="https://prc.parcconline.org/library/reader-motivation-survey" target="_blank"> https://prc.parcconline.org/library/reader-motivation-survey </a></div>';
            var category = [];
            if (!$.isEmptyObject(obj.category)) {
                $.each(obj.category, function (key, report) {
                    category.push(key)
                });
                category.sort();
                $.each(category, function (key, value) {
                    html += "<div style='margin:20px 30px;'><div class='sr-testheading'>" + value + "</div>" + 
                                '<div class="sr-detailed-score">' +
                                    '<table class="sr-table">' +
                                        "<tr><th>Question</th><th>Student's Response</th></tr>";
                    $.each(obj.category[value], function (index, response) {
                        html += '<tr><td class="sr-left">' + response.QuestionNo + '</td><td>';
                        if (typeof response.StudentsResponse === "string") {
                            if (response.StudentsResponse === "Not Answered") {
                                html += "<ul>" + response.StudentsResponse + "<ul>";
                            }
                        }
                        else {
                            if (response.StudentsResponse.text) {
                                html += "<ul>" + response.StudentsResponse.content + "<ul>";
                            }
                            else {
                                if (typeof response.StudentsResponse.content === "string") {
                                    html += "<ul>" + response.StudentsResponse.content[0] + "<ul>";
                                }
                                else {
                                    var i = 0;
                                    for (; i < response.StudentsResponse.content.length - 1 ; i++) {
                                        html += "<ul>" + response.StudentsResponse.content[i][0] + ",</ul>";
                                    };
                                    html += "<ul>" + response.StudentsResponse.content[i][0] + "</ul>"
                                }
                            }
                        };
                        html += '</td></tr>';
                    });
                    html += '</table></div></div>';
                })
            };
            html += '<div class="sr-link">View the PDF version of the ELA Reader Motivation Survey: <a href="https://prc.parcconline.org/library/reader-motivation-survey" target="_blank"> https://prc.parcconline.org/library/reader-motivation-survey </a></div>';
            html += '</div></body></html>';
            this.data.data.dynamic.html = $.UTILITY.utf8toBase64(html);
        }

    }

})(jQuery);
