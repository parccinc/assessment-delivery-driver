(function ($) {

    $.generic = {

        create: function (data) {
            this.data = data;
            this._createResults();
            console.log(this.data.data.dynamic.scoreReport);
        },

        _createResults: function () {
            var self = this;
            $.each(this.data.data.testPart, function (index, testPart) {
                self._create(index);
                console.log(testPart.dynamic.scoreReport);
            });
            this._aggregate();
        },

        _create: function (index) {
            this.data.data.testPart[index].dynamic.scoreReport = {};
            this.getData(this.data.data.testPart[index].assessmentSection, this.data.data.testPart[index].dynamic.scoreReport);
        },

        getHtml:function(data){
            this.createHtml();
        },

        getData: function (obj, test) {
            var self = this;
            $.each(obj, function (index, items) {
                $.each(items.dynamic.history, function(key, itemId){
                    var item =items.assessmentItem[itemId];
                    if (!$.UTILITY.isSystemItem(item)) {
                        if ($.isEmptyObject(test)) {
                            test.totalQuestions = 0;
                            test.totalPoints = 0;
                            test.questionsNotAnswered = 0;
                            test.questionsNotScored = 0;
                            test.assessmentItems = []
                        }
                        self.addItemData(item, test);
                    }
                });
            });
        },

        addItemData: function (item, test) {
            var self = this;
            var itemData = {
                isAnswered: $.UTILITY.isItemAnswered(item) ? true : false,
                notScored: $.UTILITY.isNotScored(item) ? true : false,
                responseData: '',
                score: $.UTILITY.getItemScore(item)
            };
            test.totalQuestions++;
            if (itemData.notScored) {
                test.questionsNotScored++;
                if (itemData.isAnswered) {
                    var index=0;
                    $.each(item.dynamic.response, function (key, value) {
                        if (value !== null) {
                            index = key;
                        }
                    });
                    if (item.dynamic.response[index].RESPONSE && item.dynamic.response[index].RESPONSE.base && item.dynamic.response[index].RESPONSE.base.string) {
                        itemData.responseData = item.dynamic.response[index].RESPONSE.base.string;
                    }
                    else {
                        itemData.responseData = "";
                    }
                }
                else {
                    itemData.responseData = 'Not Answered';
                }
            }
            else {
                if (!itemData.isAnswered) {
                    test.questionsNotAnswered++;
                }
                else {
                    if ($.isNumeric(itemData.score)) {
                        test.totalPoints += itemData.score;
                    }
                }
            }
            test.assessmentItems.push(itemData);
        },

        _aggregate: function () {
            var self = this;
            this.data.data.dynamic.scoreReport = {};
            $.each(this.data.data.testPart, function (index, testPart) {
                if (!self.data.data.dynamic.scoreReport.length) {
                    self.data.data.dynamic.scoreReport = testPart.dynamic.scoreReport;
                }
                else {
                    self.data.data.dynamic.scoreReport.totalQuestions += testPart.dynamic.scoreReport.totalQuestions;
                    self.data.data.dynamic.scoreReport.totalPoints += testPart.dynamic.scoreReport.totalPoints;
                    self.data.data.dynamic.scoreReport.questionsNotAnswered += testPart.dynamic.scoreReport.questionsNotAnswered;
                    self.data.data.dynamic.scoreReport.questionsNotScored += testPart.dynamic.scoreReport.questionsNotScored;
                    $.each(testPart.dynamic.scoreReport.assessmentItems, function (index, item) {
                        self.data.data.dynamic.scoreReport.assessmentItems.push(item)
                    });
                }
            })
        },

        createHtml: function () {
            var html = '';
            var obj = this.data.data.dynamic.scoreReport;
            var examDate = new Date();
            var examDateFormatted = ("0" + (examDate.getMonth() + 1)).slice(-2) + '/' + ("0" + examDate.getDate()).slice(-2) + '/' + examDate.getFullYear();
            html =
                '<html lang="en" xmlns="http://www.w3.org/1999/xhtml">' +
                '<head>' +
                    '<meta charset="utf-8"/>' +
                    '<title>Score Report</title>' +
	                '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>' +
                    '<style type="text/css">' +
                        'div.sr-header,div.sr-scorereport-body,div sr-table{font-family:"Open Sans",Arial,Helvetica,sans-serif;max-width:900px;min-width:580px;}' +
                        'h3.sr-heading{font-family:"Open Sans Condensed",Arial,Helvetica,sans-serif;font-weight:bold}' +
                        '.sr-header{text-align:left;margin:25px;border-bottom:1px solid #d8d8d8;font-size:32px;font-weight:700}' +
                        '.sr-date{padding-top:5px;padding-bottom:15px;font-size:16px;font-weight:400;font-family:Verdana}' +
                        '.sr-studentinfo{padding-bottom:5px}' +
                        '.sr-scorereport-body{padding:0 25px 25px 25px;margin:0 25px 25px}' +
                        '.sr-scorereport-section{margin-top:25px}' +
                        '.sr-scorereport-summarybody{background-color:#f3f2f2;width:100%;height:190px;color:#454545;font-weight:700;text-align:center;font-size:14px;min-width:480px;word-wrap:break-word}' +
                        '.sr-summary-primary{background-color:#72547d;height:190px;width:24.75%;float:left;font-size:16px;color:#ffffff}' +
                        '.sr-summary-big{font-size:78px;padding-top:15px;font-weight:400}' +
                        '.sr-summary-secondary{height:160px;margin:15px 0 15px 0;border-left:1px solid;border-color:#e2e2e2;width:24.75%;float:left}' +
                        '.sr-summary-middle{font-size:58px;padding-top:15px;font-weight:400}' +
                        '.sr-summary-small{font-size:35px;padding-top:56.5px;padding-bottom:5px;font-weight:400}' +
                        '.sr-summary-center{width:80px;margin:auto;font-family:"Open Sans Condensed",Arial,Helvetica,sans-serif;font-weight:700}' +
                        '.sr-table table,.sr-table tr{border:thin solid rgba(111,126,149,0.25)}' +
                        'table.sr-table{background-color:#fff;border-collapse:collapse;text-align:center;word-wrap:break-word}' +
                        '.sr-table tr{height:42px}' +
                        '.sr-table th{background-color:rgb(111,126,149);text-align:center;font-weight:bold;padding:0 20px;color:#fff}' +
                        '.sr-table th.sr-left{text-align:left}' +
                        '.sr-table td{background-color:#fff;text-align:center;font-weight:normal;padding:0 20px}' +
                        '.sr-table td.sr-left{text-align:left}' +
                        '.sr-sc-section{margin:0;margin-bottom:10px}' +
                        '.sr-sc-paragraf{margin:0}' +
                    '</style>' +
                '</head>' +
                '<body class="sr-body">' +
                    '<div class="sr-header">';
            html += '<div class="sr-testinfo">' + this.data.settings.test.name + ' - Grade ' + this.data.settings.test.grade + '</div>';
            html += '<div class="sr-date">Test Date: ' + examDateFormatted + '</div>';
            html += '<div class="sr-studentinfo"';
            if (this.data.settings.environment.practice) {
                html += ' style="display:none"';
            }
            html+= '>' + this.data.settings.candidate.firstName + " " + this.data.settings.candidate.lastName + ', Grade ' + this.data.settings.candidate.grade + '</div></div><div class="sr-bodytext">';
            html += 
             '<div class="sr-scorereport-body">' +
                 '<div class="sr-scorereport-section">' +
                    '<h2 class="sr-heading sr-sc-section">Summary</h2>' +
        			'<div class="sr-scorereport-summarybody">' +
        				'<div class="sr-summary-primary"><div class="sr-summary-big">' + obj.totalPoints + '</div><div>TOTAL POINTS</div></div>' +
                        '<div class="sr-summary-secondary"><div class="sr-summary-middle">' + obj.questionsNotAnswered + '</div><div class="sr-summary-center">UNANSWERED QUESTIONS</div></div>' +
                        '<div class="sr-summary-secondary"><div class="sr-summary-middle">' + obj.questionsNotScored + '</div><div class="sr-summary-center">QUESTIONS NOT SCORED</div></div>' +
                        '<div class="sr-summary-secondary"><div class="sr-summary-middle">' + obj.totalQuestions + '</div><div class="sr-summary-center">TOTAL QUESTIONS</div></div>' +
        			'</div>' +
        		'</div>' +
                '<div class="sr-scorereport-section">' +
                    '<h2 class="sr-heading sr-sc-section">Question Responses</h2>' +
                    '<table class="sr-table">' +
                        '<tr><th>Question Number</th><th class="sr-left">Student\'s Score</th></tr>';
            for (var i = 0; i < obj.assessmentItems.length; i++) {
                if (obj.assessmentItems[i].notScored == true) {
                    html += '<tr><td>' + (i + 1) + '</td><td class="sr-left">Not Scored; see Teacher To Score<sup>*</sup></td></tr>';
                }
                else if (obj.assessmentItems[i].isAnswered == false) {
                    html += '<tr><td>' + (i + 1) + '</td><td class="sr-left">Not Answered</td></tr>';
                }
                else {
                    html += '<tr><td>' + (i + 1) + '</td><td class="sr-left"> ' + obj.assessmentItems[i].score.toFixed(1) + '</td></tr>';
                }
            };
            html += '</table>\
                </div>\
                <div class="sr-scorereport-section">\
                    <h2 class="sr-heading sr-sc-section"><sup>*</sup>Teacher To Score</h2>\
                    <div class="sr-sc-section">Teacher To Score indicates an un-scored question, but the student\'s responses are displayed below to allow for review and the option of teacher/test administrator scoring.</div>';
            for (var i = 0; i < obj.assessmentItems.length; i++) {
                if (obj.assessmentItems[i].notScored == true) {
                    html += '<div class="sr-sc-section"><h3 class="sr-heading sr-sc-paragraf"> Question ' + (i + 1) + '</h3><div> ' + obj.assessmentItems[i].responseData + '</div></div>';
                }
            }
            html += '</div></div></div></body></html>';
            this.data.data.dynamic.html = $.UTILITY.utf8toBase64(html);
        }
    };

})(jQuery);