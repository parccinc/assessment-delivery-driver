(function ($) {
    $.widget("td.datacomm", $.td.formbase, {

        _create: function () {

            console.log("InDataCommunicationWidget._create();");

            var self = this;
            var markup = '\
                <div class="td-data-comm">\
                </div>';
            this.options.container = $(markup).appendTo(this.element);
            this.options.spinner = this.options.container.spinner({});
        },

        download: function () {

            this.options.spinner.spinner("enterbusy");

            var self = this;

            var method = "GET";
            var username = this.global.settings.credentials.username, // $Global$.username,
                password = this.global.settings.credentials.password, // $Global$.password,
                secret = this.global.settings.credentials.secret, // $Global$.secret,
                dob = this.global.settings.credentials.dob,
                sid = this.global.settings.credentials.id,
                state = this.global.settings.credentials.state,
                testkey = this.global.settings.credentials.testkey;

            var apiPath = $Global$.apiPath + "/content";
            var apiURL = apiPath + '/' + this.global.settings.api.token + '/' + this.global.settings.test.id + '/test.json';
           
            var url = $Global$.apiHost + apiURL;

            var credentials = username + ":" + password;
            //var timeOffset = 1000 * $Global$.apiTime - new Date().getTime();
            var timestamp = new Date().getTime() + this.global.settings.timeOffset;
            var nonce = Math.floor(Math.random() * 99999);
            var hash = apiURL + Math.round(timestamp / 1000) + nonce;
            var message = $.sha256hmac(secret, hash);
            var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(message));

            var request = $.ajax({
                url: url,
                type: method,
                crossDomain: true
            })
            .done(function (data, textStatus, jqXHR) {
                console.log('test package downloaded successfully!');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.log('test package failed to download, killing spinner!');
                self.options.spinner.spinner("leavebusy");
            });

            request.done(function (data) {
                // self._urlReplace(data);
                self._setTestData(data);
                if (self.global.settings.test.status === "InProgress" || self.global.settings.test.status === "Paused") {
                    self._downloadState();
                }
                else if (self.global.settings.test.status === "Scheduled") {
                    self._initState();
                    self._setTestData2(false);
                    setTimeout(function () {
                        self.options.spinner.spinner("leavebusy");
                        self._trigger('downloaddone');
                        console.log('Download Done triggered!');
                    }, 10);
                }
                else {
                    alert("Exception!"); // This should never happen!
                }
            });

            request.fail(function (jqxhr, textStatus, errorThrown) {
                var errorDesc = jqxhr.getResponseHeader("X-Error-Description") != null ? jqxhr.getResponseHeader("X-Error-Description") : "";
                var errorCode = jqxhr.getResponseHeader("X-Error-Code") != null ? jqxhr.getResponseHeader("X-Error-Code") : "TD-1000";
                var time = jqxhr.getResponseHeader("X-Error-Code") != null ? jqxhr.getResponseHeader("X-Time") : 0;
                if (time) {
                    self.global.settings.timeOffset = 1000 * time - new Date().getTime();
                }
                self._trigger("downloadfailed", null, { errorCode: errorCode, errorDesc: errorDesc, time: time, status: jqxhr.status });
            });
        },

        _downloadTestPackage: function() {
        },

        _urlReplace: function(data) {

            var replacer = $Global$.apiHost + '\/api\/content\/' + this.global.settings.api.token + '\/' + this.global.settings.test.id + '\/';
            var dataString = JSON.stringify(data);
            var dataString2 = dataString.replace(/\[\[ADP\]\]/g, replacer);
            var data2 = JSON.parse(dataString2);

            this._setTestData(data2);
        },

        _downloadState: function () {

            var self = this;

            var method = "GET";
            var username = this.global.settings.credentials.username, //$Global$.username,
                password = this.global.settings.credentials.password, //$Global$.password,
                secret = this.global.settings.credentials.secret, // $Global$.secret,
                dob = this.global.settings.credentials.dob,
                sid = this.global.settings.credentials.id,
                state = this.global.settings.credentials.state,
                testkey = this.global.settings.credentials.testkey;

            var apiPath = $Global$.apiPath + "/results";
            var apiURL = apiPath + '/' + this.global.settings.api.token + '/' + this.global.settings.test.id;
           
            var url = $Global$.apiHost + apiURL;

            var credentials = username + ":" + password;
            //var timeOffset = 1000 * $Global$.apiTime - new Date().getTime();
            var timestamp = new Date().getTime() + this.global.settings.timeOffset;
            var nonce = Math.floor(Math.random() * 99999);
            var hash = apiURL + Math.round(timestamp / 1000) + nonce;
            var message = $.sha256hmac(secret, hash);
            var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(message));

            var xhr = $.ajax({
                type: method,
                url: url,
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Basic ' + btoa(credentials),
                    'Authentication': authentication,
                    'X-Requested-With': btoa(testkey)
                },
                crossDomain: true
            })
            .done(function (data, textStatus, jqXHR) {
                // Eliminate bad data and check for envelope
                var realData = data.result ? data.result : data;
                if (realData && realData.dynamic) {
                    self._setState(realData);
                    self._setTestData2(true);
                    if ($.type(realData.dynamic.ase) === "array" && realData.dynamic.ase.length === 0) {
                        realData.dynamic.ase = {}
                    };
                    if ($.type(realData.dynamic.settings) === "array" && realData.dynamic.settings.length === 0) {
                        realData.dynamic.settings = {}
                    };
                    console.log('test state downloaded successfully!');
                }
                else {
                    // Same as fail
                    self._initState();
                    self._setTestData2(false);
                    console.log('test state downloaded successfully but data is bad!');
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                self._initState();
                self._setTestData2(false);
                console.log('test state failed to download!');
                var errorDesc = "";
                var errorCode = "TD-1000";
				var time = 0;
                if (jqXHR.responseText.length > 0) {
                    var errObject = JSON.parse(jqXHR.responseText);
                    var errObj = errObject.result ? errObject.result : errObject;
                    if (errObj && errObj.errorCode && errObj.errorDescription) {
                        errorCode = errObj.errorCode;
                        errorDesc = errObj.errorDescription;
                        
                        if (errObj.time) {
                            time = errObj.time;
                            self.global.settings.timeOffset = 1000 * time - new Date().getTime();
                        }
                    }
                }
                
                self._trigger("downloadfailed", null, { errorCode: errorCode, errorDesc: errorDesc, time: time, status: jqXHR.status });
            })
            .always(function (dataorjqXHR, textStatus, jqXHRorerrorThrown) {
                console.log('test state always!');
                // Now we are ready to continue with the test
                setTimeout(function () {
                    self.options.spinner.spinner("leavebusy");
                    self._trigger('downloaddone');
                    console.log('Download Done triggered!');
                }, 10);
            });
        },

        _uploadState: function (purpose) {
            this.options.spinner.spinner("enterbusy");

            var data = this._saveState();
            var datastr = JSON.stringify(data);

            var self = this;

            var method = "POST";
            var username = this.global.settings.credentials.username, // $Global$.username,
                password = this.global.settings.credentials.password, // $Global$.password,
                secret = this.global.settings.credentials.secret, // $Global$.secret,
                dob = this.global.settings.credentials.dob,
                sid = this.global.settings.credentials.id,
                state = this.global.settings.credentials.state,
                testkey = this.global.settings.credentials.testkey;

            var apiPath = $Global$.apiPath + "/results";
            var apiURL = apiPath + '/' + this.global.settings.api.token + '/' + this.global.settings.test.id;
            if (purpose && (purpose === "pause" || purpose === "submit")) {
                apiURL += "/" + purpose;
            }

            var url = $Global$.apiHost + apiURL;

            var credentials = username + ":" + password;
            //var timeOffset = 1000 * $Global$.apiTime - new Date().getTime();
            var timestamp = new Date().getTime() + this.global.settings.timeOffset;
            var nonce = Math.floor(Math.random() * 99999);
            var hash = apiURL + Math.round(timestamp / 1000) + nonce;
            var message = $.sha256hmac(secret, hash);
            var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(message));

            // Get the state (if any)
            var xhr = $.ajax({
                type: method,
                url: url,
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Basic ' + btoa(credentials),
                    'Authentication': authentication,
                    'X-Requested-With': btoa(testkey)
                },
                data: datastr, // data
                crossDomain: true
            })
            .done(function (data, textStatus, jqXHR) {
                var realData = data.result ? data.result : data;
                $Global$.apiTime = realData.api.time;
                    self._trigger("uploaddone", null, { purpose: purpose, status: realData.test.status });
                
                console.log('DataCommWidget: _uploadState() successful!');
                // There's a status and who knows what else returned here.
                // TODO - Implement Proctor actions if possible.
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                var errorDesc = "";
                var errorCode = "TD-1000";
				var time = 0;
                if (jqXHR.responseText.length > 0) {
                    try {
                        var errObject = JSON.parse(jqXHR.responseText);
                        var errObj = errObject.result ? errObject.result : errObject;
                        if (errObj && errObj.errorCode && errObj.errorDescription) {
                            errorCode = errObj.errorCode;
                            errorDesc = errObj.errorDescription;
                            if (errObj.time) {
                                time = errObj.time;
                                self.global.settings.timeOffset = 1000 * time - new Date().getTime();
                            }
                        }
                    }
                    catch (error) {
                        errorDesc = "JSON.parse failed, probably invalid responseText.";
                    }
                }

                self._trigger("uploadfailed", null, { errorCode: errorCode, errorDesc: errorDesc, time: time, status: jqXHR.status, purpose:purpose });

                console.log('DataCommWidget: _uploadState() failed with: ', textStatus)
            })
            .always(function (dataorjqXHR, textStatus, jqXHRorerrorThrown) {
                self.options.spinner.spinner("leavebusy");
            });
        },

        _setTestData: function (data) {

            this.global.data = data;
            var testPartCount = this.global.data.testPart.length;
            var sectionCount = 0;
            var itemCount = 0;

            for (var i = 0; i < this.global.data.testPart.length; i++) {
                sectionCount += this.global.data.testPart[i].assessmentSection.length;
                for (var j = 0; j < this.global.data.testPart[i].assessmentSection.length; j++) {
                    for (var l = 0; l < this.global.data.testPart[i].assessmentSection[j].assessmentItem.length; l++) {
                        if (!this.global.data.testPart[i].assessmentSection[j].assessmentItem[l].category || !this.global.data.testPart[i].assessmentSection[j].assessmentItem[l].category.length || this.global.data.testPart[i].assessmentSection[j].assessmentItem[l].category.indexOf("systemItem") == -1 ) {
                            itemCount++;
                        }
                    }
                    //itemCount += this.global.data.testPart[i].assessmentSection[j].assessmentItem.length;
                    for (var k = 0; k < this.global.data.testPart[i].assessmentSection[j].assessmentItem.length; k++) {
                        if (this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].assessmentItemContent.data /*&& this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].assessmentItemContent.assets*/ ){
                            this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].assessmentItemContent.data.body.originalBody = this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].assessmentItemContent.data.body.body;
                        }
                        else {
                            this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].assessmentItemContent.body.originalBody = this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].assessmentItemContent.body.body;
                        }
                    }
                }
            }

            this.global.testInfo = {
                testPartCount: testPartCount,
                sectionCount: sectionCount,
                itemCount: itemCount
            };
        },

        _setTestData2: function (restart) {
            this.global.testInfo.restart = restart;
        },

        _initState: function () {

            var engine = this._getEngineSelection();

            this.global.data.dynamic = {
                type: "package",
                collection: "testPart",
                selectionAlgorithm: "linear",
                history: [],
                current: -1,
                sealed: false,
                access: 0,
                id: this.global.settings.test.id,
                tenantId: $Global$.tenant,
                state:  null,
                events: [],
                remainingTime: this.global.data.timeLimits && this.global.data.timeLimits.maxTime && Number.isSafeInteger(this.global.data.timeLimits.maxTime) ? Number.parseInt(this.global.data.timeLimits.maxTime) : 0,
                ase: {},
                settings: {}
            };

            for (var i = 0; i < this.global.data.testPart.length; i++) {

                this.global.data.testPart[i].dynamic = {
                    type: "testPart",
                    collection: "assessmentSection",
                    selectionAlgorithm: (this.global.data.testPart[i].navigationMode && this.global.data.testPart[i].navigationMode.length ? this.global.data.testPart[i].navigationMode : "nonlinear"),
                    history: [],
                    current: -1,
                    sealed: false,
                    access: 0,
                    state: null,
                    events: []
                };

                for (var j = 0; j < this.global.data.testPart[i].assessmentSection.length; j++) {

                    this.global.data.testPart[i].assessmentSection[j].dynamic = {
                        type: "assessmentSection",
                        collection: "assessmentItem",
                        selectionAlgorithm: this._getSectionSelectionAlgorithm(this.global.data.testPart[i].dynamic.selectionAlgorithm, engine, this.global.data.testPart[i].assessmentSection[j]),
                        async: engine.asynchronous,
                        history: [],
                        current: -1,
                        sealed: false,
                        access: 0,
                        state: null,
                        events: []
                    };
                    for (var k = 0; k < this.global.data.testPart[i].assessmentSection[j].assessmentItem.length; k++) {

                        this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic = {
                            type: "assessmentItem",
                            state: null,
                            response: [],
                            score: {
                                value: null,
                                error: false,
                                sent: false
                            },
                            events: [],
                            flagged: false,
                            systemItemNavProperties: this._getSystemItemNavProperties(i, j, k)
                        };
                        this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].response = null;
                    }

                    if (this.global.data.testPart[i].assessmentSection[j].dynamic.selectionAlgorithm === "adcm") {
                        this.global.data.testPart[i].assessmentSection[j].map = this._mapItems(this.global.data.testPart[i].assessmentSection[j].assessmentItem);
                        this.global.data.testPart[i].assessmentSection[j].url = engine.dcm ? engine.url + "/items/" + engine.id : null;
                    }
                }
            }
        },

        _getEngineSelection: function() {
            var engine = {
                dcm: false,
                asynchronous: false,
                id: 0,
                url: ""
            }
            var itemSelection = this.global.settings.test.itemSelection;
            if (itemSelection === "Adaptive") {
                engine.dcm = true;
                engine.asynchronous = true;
                engine.id = this.global.settings.test.id;
                engine.url = this.global.settings.api.catUrl;
            }
            return engine;
        },

        _getSectionSelectionAlgorithm: function(testPartSelectionAlgorithm, engine, section) {

            // selectionAlgorithm: engine.dcm ? "adcm" : this.global.data.testPart[i].dynamic.selectionAlgorithm,
            var systemItemCount = 0;
            for (var i = 0; i < section.assessmentItem.length; i++) {
                if ($.UTILITY.isSystemItem(section.assessmentItem[i])) {
                    systemItemCount++;
                }
            }
            // I can tolerate mix of systemItems and regular items for all fixed form tests, but cannot for DCM Adaptive tests - those I will force to be linear.
            if (engine.dcm) {
                if (systemItemCount == 0 && section.assessmentItem.length > 0) {
                    return "adcm";
                }
                else {
                    if (systemItemCount > 0 && systemItemCount < section.assessmentItem.length) {
                        // Mix - alert and force back to linear.
                        alert('DCM section cannot have system items, bad test! Forcing linear selection algorithm.');
                        engine.dcm = false;
                        engine.asynchronous = false;
                        engine.id = 0;
                        engine.url = "";
                        return "linear";
                    }
                    else {
                        if (systemItemCount == section.assessmentItem.length && section.assessmentItem.length > 0) {
                            engine.dcm = false;
                            engine.asynchronous = false;
                            return "linear"; // Section comprised of system items only - that's acceptable
                        }
                        else {
                            alert('Bad Test, found an empty section with DCM Adaptive algorithm in force.');
                            throw new Error('Bad Test, found an empty section with DCM Adaptive algorithm in force.');
                        }
                    }
                }
            }
            else {
                return testPartSelectionAlgorithm;
            }
        },

        _getSystemItemNavProperties: function (part, section, item) {
            if ($.UTILITY.isSystemItem(this.global.data.testPart[part].assessmentSection[section].assessmentItem[item])) {
                return {
                    firstInSection: item == 0,
                    firstInTestPart: section == 0 && item == 0,
                    lastInSection: item == this.global.data.testPart[part].assessmentSection[section].assessmentItem.length - 1,
                    lastInTestPart: section == this.global.data.testPart[part].assessmentSection.length - 1 && item == this.global.data.testPart[part].assessmentSection[section].assessmentItem.length - 1
                };
            }
            return null;
        },

        _setState: function (data) {

            var self = this;
            var engine = this._getEngineSelection();
            
            this.global.data.dynamic = data.dynamic;

            if (this.global.data.dynamic.state !== null && "value" in this.global.data.dynamic.state) {
                this.global.data.dynamic.state = this.global.data.dynamic.state.value;
            }

            for (var i = 0; i < this.global.data.testPart.length; i++) {
                this.global.data.testPart[i].dynamic = data.testPart[i].dynamic;
                if (this.global.data.testPart[i].dynamic.state !== null && "value" in this.global.data.testPart[i].dynamic.state) {
                    this.global.data.testPart[i].dynamic.state = this.global.data.testPart[i].dynamic.state.value;
                }

                for (var j = 0; j < this.global.data.testPart[i].assessmentSection.length; j++) {
                    this.global.data.testPart[i].assessmentSection[j].dynamic = data.testPart[i].assessmentSection[j].dynamic;
                    if (this.global.data.testPart[i].assessmentSection[j].dynamic.state !== null && "value" in this.global.data.testPart[i].assessmentSection[j].dynamic.state) {
                        this.global.data.testPart[i].assessmentSection[j].dynamic.state = this.global.data.testPart[i].assessmentSection[j].dynamic.state.value;
                    }

                    for (var k = 0; k < this.global.data.testPart[i].assessmentSection[j].assessmentItem.length; k++) {
                        var map = this._mapHistory(this.global.data.testPart[i].assessmentSection[j].dynamic.history);
                        //this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic = data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic;
                        if (k in map) {
                            self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic = data.testPart[i].assessmentSection[j].assessmentItem[map[k]].dynamic;
                            if (self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response !== null && "value" in self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response) {
                                var trackList = ["pause", "submit", "activated"];
                                $.each(self.global.settings.config.trackEvents, function (name, value) {
                                    if (value) {
                                        trackList.push(name)
                                    }
                                });
                                var eventList = self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.events;
                                self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.events = [];
                                self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response = [];
                                var resp = null;
                                $.each(eventList, function (id, event) {
                                    if (event.type === "navto") {
                                        self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response.push(null);
                                    }
                                    if (event.type === "response") {
                                        var len = self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response.length - 1;
                                        self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response[len] = event.value;
                                    }
                                    if ($.inArray(event.type, trackList) > -1) {
                                        var len = self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response.length - 1;
                                        if (resp !== null && len > -1) {
                                            if (JSON.stringify(self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response[len]) != JSON.stringify(resp)) {
                                                resp = self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response[len];
                                            }
                                            else {
                                                self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response[len] = null;
                                            }
                                        }
                                        self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.events.push(event)
                                    }
                                })
                            }
                            if (self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.state !== null && "value" in self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.state) {
                                self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.state = self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.state.value;
                            }
                        }
                        else {
                            self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic = {
                                type: "assessmentItem",
                                state:  null,
                                response: [],
                                score: {
                                    value: null,
                                    error: false,
                                    sent: false
                                },
                                events: [],
                                flagged: false,
                                systemItemNavProperties: this._getSystemItemNavProperties(i, j, k)
                            }
                        };
                        self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].response = self._lastResponseInArray(self.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic.response);
                    }

                    if (this.global.data.testPart[i].assessmentSection[j].dynamic.selectionAlgorithm === "adcm") {
                        this.global.data.testPart[i].assessmentSection[j].map = this._mapItems(this.global.data.testPart[i].assessmentSection[j].assessmentItem);
                        this.global.data.testPart[i].assessmentSection[j].url = engine.dcm ? engine.url + "/items/" + engine.id : null;
                    }
                }
            }
        },

        _saveState: function () {

            var self = this;
            var state = {};
            state.dynamic = this.global.data.dynamic;
            state.testPart = [];

            for (var i = 0; i < this.global.data.testPart.length; i++) {
                state.testPart.push({});
                state.testPart[i].dynamic = this.global.data.testPart[i].dynamic;
                state.testPart[i].assessmentSection = [];

                for (var j = 0; j < this.global.data.testPart[i].assessmentSection.length; j++) {
                    state.testPart[i].assessmentSection.push({});
                    state.testPart[i].assessmentSection[j].dynamic = this.global.data.testPart[i].assessmentSection[j].dynamic;
                    state.testPart[i].assessmentSection[j].assessmentItem = [];

                    $.each(state.testPart[i].assessmentSection[j].dynamic.history, function (index, value) {
                        if ($.inArray(value, state.testPart[i].assessmentSection[j].assessmentItem) == -1) {
                            state.testPart[i].assessmentSection[j].assessmentItem[index] = {};
                            state.testPart[i].assessmentSection[j].assessmentItem[index].dynamic = self.global.data.testPart[i].assessmentSection[j].assessmentItem[value].dynamic;
                        }
                    })

                    //for (var k = 0; k < this.global.data.testPart[i].assessmentSection[j].assessmentItem.length; k++) {
                    //    state.testPart[i].assessmentSection[j].assessmentItem.push({});
                    //    state.testPart[i].assessmentSection[j].assessmentItem[k].dynamic = this.global.data.testPart[i].assessmentSection[j].assessmentItem[k].dynamic;
                    //}
                }
            }
            return state;
        },

        update: function (event) {
            var navto = event.type === "navto" && this.global.settings.config.trackEvents.navto;
            var init = event.type === "init" && this.global.settings.config.trackEvents.init;
            var setstate = event.type === "setstate" && this.global.settings.config.trackEvents.setstate;
            var render = event.type === "render" && this.global.settings.config.trackEvents.render;
            var ready = event.type === "ready" && this.global.settings.config.trackEvents.ready;
            var state = event.type === "state" && this.global.settings.config.trackEvents.state;
            var response = event.type === "response" && this.global.settings.config.trackEvents.response;
            var navfrom = event.type === "navfrom" && this.global.settings.config.trackEvents.navfrom;

            if (navto || init || setstate || render || ready || state || response || navfrom || event.type === "pause" || event.type === "submit" || event.type === "activated") {
                event.time = new Date().valueOf();
                this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.events.push(event);
            }

            if (navto) {
                this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response.push(null);
            }

            if (navfrom || event.type === "pause" || event.type === "submit") {
                var len = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response.length - 1;
                var prev = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].response;
                var resp = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response[len];
                if (this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response[len] !== null) {
                    if (prev !== null) {
                        if (JSON.stringify(prev) != JSON.stringify(resp)) {
                            this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].response = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response[len];
                        }
                        else {
                            this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response[len] = null;
                        }
                    }
                    else {
                        this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].response = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response[len];
                    }
                }
            }
            
            if (event.type === 'response') {
                var len = this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response.length - 1;
                this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.response[len] = event.value;
            }
            else if (event.type === 'state') {
                this.global.data.testPart[event.selector.test].assessmentSection[event.selector.section].assessmentItem[event.selector.item].dynamic.state = event.value;
            }

            // I can change this to "go-up" on state changes, but that might be too intense. Let me try to keep it down to response changes.
            if (event.type === 'response') {
                // Let's capture the thumbnail too
                //if (this.options.settings.config.review.enabled && this.options.settings.config[0].review.thumbnails) {
                //    this._captureThumbnail();
                //}

                // TODO: move out to asynchronous processing
                //if (this.options.settings.config.updateon.response) {
                //    this._uploadState(this.options.settings.host.post.state);
                //}
            }

            if ((event.type === "navto" && this.global.settings.config.uploadResults.navto) ||
                (event.type === "navfrom" && this.global.settings.config.uploadResults.navfrom) ||
                (event.type === "state" && this.global.settings.config.uploadResults.state) ||
                (event.type === "response" && this.global.settings.config.uploadResults.response)) {
                this.upload();
            }
        },

        upload: function (purpose) {
            this._uploadState(purpose);
        },

        _mapItems: function (data) {
            var map = {};
            var mapVal=[];
            $.each(data, function (index, value) {
                var id = value.uin;
                map[id] = index;
            });
            return map;
        },

        _mapHistory: function (data) {
            var map = {};
            $.each(data, function (index, value) {
                map[value] = index;
            });
            return map;
        },

        _lastResponseInArray: function (array) {
            var len = array.length > 0 ? array.length - 1 : 0;
            var response = null;
            while (len > 0 && array[len] === null) {
                len--;
            }
            if (array[len] !== null) {
                response = array[len];
            }
            return response;
        }
    });
})(jQuery);
