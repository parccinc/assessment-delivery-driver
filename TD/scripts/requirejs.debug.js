﻿/**
 * Based on code by Bertrand Chevrier - OAT
 * @author Bertrand Chevrier <bertrand@taotesting.com>
 * @see {@link https://github.com/oat-sa/parc-itemrunner/wiki}
 */

'use strict';

var version = "0.9.0";

var defaultTheme = 'blackOnWhite';

//configuration of the bundle: libraries paths,
requirejs.config({
    //  baseUrl : '/TD/OAT/js', // TR: Removing baseUrl to streamline minification. - this will default to the value specified in for data-main attribute where require is referenced in index.html or testDriver.phtml.
    // urlArgs: "buster=" + version,      //buster ensure files are not in cache for that version


    //alias for external librairies, paths are relative to the baseUrl (ie. requirejs)
    paths: {
        //If new paths need to be added use src/tao/views/build/requirejs.build.js as a reference.
        'text': '../OAT/src/tao/views/js/lib/text/text',
        'json': '../OAT/src/tao/views/js/lib/text/json',
        'css': '../OAT/src/tao/views/js/lib/require-css/css.min',
        'tpl': '../OAT/src/tao/views/js/tpl',

        'jquery': '../OAT/src/tao/views/js/lib/jqueryamd-1.8.3',
        'select2': '../OAT/src/tao/views/js/lib/select2/select2.min',

        'ckeditor': '../OAT/src/tao/views/js/lib/ckeditor/ckeditor', // '../dist/assets/ckeditor/ckeditor',
        'mathJax': '/TD/OAT/dist/assets/mathjax/MathJax.min', // '../dist/assets/mathjax/MathJax.min'
        'core': '../OAT/src/tao/views/js/core',
        'class': '../OAT/src/tao/views/js/lib/class',
        'lib': '../OAT/src/tao/views/js/lib',
        'lodash': '../OAT/src/tao/views/js/lib/lodash.min',
       
        'context': '../OAT/src/tao/views/js/context',
        'util': '../OAT/src/tao/views/js/util',
        'async': '../OAT/src/tao/views/js/lib/async',
        'ui': '../OAT/src/tao/views/js/ui',
        'handlebars': '../OAT/src/tao/views/js/lib/handlebars',
       
        'i18n': '../OAT/src/tao/views/js/i18n',
        'polyfill': '../OAT/src/tao/views/js/lib/polyfill',
        'filereader': '../OAT/src/tao/views/js/lib/polyfill/jquery.FileReader.min',

        'raphael': '../OAT/src/tao/views/js/lib/raphael/raphael',
        'scale.raphael': '../OAT/src/tao/views/js/lib/raphael/scale.raphael',


        'tooltipster': '../OAT/src/tao/views/js/lib/tooltipster/jquery.tooltipster',
        'nouislider': '../OAT/src/tao/views/js/lib/sliders/jquery.nouislider',

      

        'mediaElement': '../OAT/src/tao/views/js/lib/mediaelement/mediaelement-and-player',

        'taoQtiItem': '../OAT/src/taoQtiItem/views/js',
        'taoQtiItemCss': '../OAT/src/taoQtiItem/views/css',
        'taoItems': '../OAT/src/taoItems/views/js',
        'taoCss': '../OAT/src/tao/views/js/views/css',
        'taoQtiItemCss/qti': '../OAT/src/taoQtiItem/views/css/qti-runner',
        'qtiCustomInteractionContext': '../OAT/src/taoQtiItem/views/js/runtime/qtiCustomInteractionContext',
        'qtiInfoControlContext': '../OAT/src/taoQtiItem/views/js/runtime/qtiInfoControlContext',

        'i18ntr': '../OAT/src/tao/views/locales/en-US',

        'IMSGlobal': '../OAT/src/taoQtiItem/views/js/portableSharedLibraries/IMSGlobal',
        'OAT': '../OAT/src/taoQtiItem/views/js/portableSharedLibraries/OAT',
        'PARCC': '../OAT/src/taoQtiItem/views/js/portableSharedLibraries/PARCC',
        'studentToolbar': '../OAT/src/qtiItemPic/views/js/picCreator/dev/studentToolbar',
        'qti.customOperators.text.StringToNumber': '../OAT/src/taoParcc/views/js/scoring/processor/expressions/operators/custom/stringToNumber',
        'qti.customOperators.math.graph.CountPointsThatSatisfyEquation' : '../OAT/src/taoParcc/views/js/scoring/processor/expressions/operators/custom/countPointsThatSatisfyEquation',
        'qtiItemRunner': '../OAT/src/taoQtiItem/views/js/runner/qtiItemRunner',
        'qtiScorer': '../OAT/src/taoQtiItem/views/js/scoring/qtiScorer',

        //teis
        'fractionModelInteraction': '../OAT/src/parccTei/views/js/pciCreator/dev/fractionModelInteraction',
        'graphFunctionInteraction': '../OAT/src/parccTei/views/js/pciCreator/dev/graphFunctionInteraction',
        'graphLineAndPointInteraction': '../OAT/src/parccTei/views/js/pciCreator/dev/graphLineAndPointInteraction',
        'graphNumberLineInteraction': '../OAT/src/parccTei/views/js/pciCreator/dev/graphNumberLineInteraction',
        'graphZoomNumberLineInteraction': '../OAT/src/parccTei/views/js/pciCreator/dev/graphZoomNumberLineInteraction',
        'histogramInteraction': '..OAT/src/parccTei/views/js/pciCreator/dev/histogramInteraction',
        'interactivePassageInteraction': '../OAT/src/parccTei/views/js/pciCreator/dev/interactivePassageInteraction',
        'multiTabbedExhibit': '../OAT/src/parccTei/views/js/pciCreator/dev/multiTabbedExhibit',

        //student tools? - as of 3/30/16 these are the only ones included in the bundle file....Assume at some point references will need to be added for everything inside of parccStudentTools\views\js\picCreator\dev?
        'parccAnswerMasking': '../OAT/src/parccStudentTools/views/js/picCreator/dev/parccAnswerMasking',
        'parccColorContrast': '../OAT/src/parccStudentTools/views/js/picCreator/dev/parccColorContrast',
        'parccScientificCalculator': '../OAT/src/parccStudentTools/views/js/picCreator/dev/parccScientificCalculator',
        //'qtiScorer': '../OAT/dist/qtiscorer.min'

    },

    //configure paths for external libraries, they are relative to the page.  
    //TR: Changed paths below to use require.toUrl() whene generating references so that the paths are relative to OAT/js folder instead of the page. Relative paths are needed for the release build.
    config: {
        'mediaElement': {
            plugins: [],
            pluginPath: require.toUrl('../OAT/src/tao/views/js/lib/mediaelement/flashmediaelement.swf/') //where mediaElement loads its plugin based players
        },

        'MathJax/MathJax': {
            root: require.toUrl('../OAT/dist/assets/mathjax/')       //where MathJax will look for it's fonts
        },

        'mathJax': {
            menuSettings: {
                inTabOrder: false
            }
        },

        'ui/themes': {
            items: {                          //register thmessagese themes for the IR (at least the default is required)
                base: require.toUrl('../OAT/src/taoQtiItem/views/css/qti-runner.css'),
                default: defaultTheme,
                available: [
                //{
                //    id: 'tao',
                //    path: require.toUrl('/TD/OAT/src/taoParcc/themes/tao-default/theme.css')
                //}, 

                {
                    id: 'blackOnWhite',
                    path: require.toUrl('../OAT/src/taoParcc/themes/black-on-white/theme.css')
                }, {
                    id: 'whiteOnBlack',
                    path: require.toUrl('../OAT/src/taoParcc/themes/white-on-black/theme.css')
                }, {
                    id: 'blackOnCream',
                    path: require.toUrl('../OAT/src/taoParcc/themes/black-on-cream/theme.css')
                }, {
                    id: 'blackOnLightBlue',
                    path: require.toUrl('../OAT/src/taoParcc/themes/black-on-light-blue/theme.css')
                }, {
                    id: 'blackOnLightMagenta',
                    path: require.toUrl('../OAT/src/taoParcc/themes/black-on-light-magenta/theme.css')
                }, {
                    id: 'greyOnGreen',
                    path: require.toUrl('../OAT/src/taoParcc/themes/grey-on-green/theme.css')
                }, {
                    id: 'lightBlueOnDarkBlue',
                    path: require.toUrl('../OAT/src/taoParcc/themes/light-blue-on-dark-blue/theme.css')
                }]
            }
        }
    },

    //set timeout for require.js to download
    waitSeconds: 300,

    //CKEditor still exposes a global variable, it needs to be shimed.
    shim: {
        'ckeditor': {
            exports: 'CKEDITOR'
        },
        'class': {

            exports: 'Class'
        }
    }
});



