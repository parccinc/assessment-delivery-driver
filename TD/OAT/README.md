parc-itemrunner
===============

Standalone QTI ItemRunner for parcc ADP


## Demo

Clone the repository and open `demo.html` in a web browser.

## Documentation

Each component is documented into the [repository wiki](https://github.com/oat-sa/parc-itemrunner/wiki).
But the demo itself is the best resource to look at and to test the Item Runner.

## Basic Usage

```javascript
require(['qtiItemRunner.min'], function(itemRunner) {

    //for each item rendering

    var runner =
       itemRunner(item)
        .on('error', function(err){
            //err message
        })
        .on('init', function(){
            //item is loaded
        })
        .on('render', function(){
            //item is ready
        }).on('listpic', function(picList){
            //got the list of provided PIC inside a collection
            //  use the communication API to interact
        })
        .on('statechange', function(state){

        })
        .on('endattempt', function(responseIdentifier){

        })
        .init()
        .setState(initialState)
        .render(domElement);

    //...

    runner.getState();
    runner.getResponses();

    runner.clear(); //needed before calling the runner again
});
```

## Sources

The sources are available under the `develop` branch of [extenion-tao-item](oat-sa/extension-tao-item) and [extension-tao-itemqti](oat-sa/extension-tao-itemqti)

 - API : [taoItems/views/js/runner/api/itemRunner.js](https://github.com/oat-sa/extension-tao-item/blob/develop/views/js/runner/api/itemRunner.js)
 - API Test : [taoItems/views/js/test/runner/api](https://github.com/oat-sa/extension-tao-item/tree/develop/views/js/test/runner/api)
 - QTI Provider : [taoQtiItem/views/js/runner/provider/qti.js](https://github.com/oat-sa/extension-tao-itemqti/blob/develop/views/js/runner/provider/qti.js)
 - QTI Provider PIC Manager : [taoQtiItem/views/js/runner/provider/manager/picManager.js](https://github.com/oat-sa/extension-tao-itemqti/blob/develop/views/js/runner/provider/manager/picManager.js)
 - QTI Provider Test : [taoQtiItem/views/js/test/runner/provider](https://github.com/oat-sa/extension-tao-itemqti/blob/develop/views/js/test/runner/provider)
 - Asset Manager [taoItems/views/js/assets](https://github.com/oat-sa/extension-tao-item/blob/develop/views/js/assets)


## Release
 - _0.7.1_ :
  - Fix tooltip not appearing on pattern missmatch using a text entry.
 - _0.7.0_ :
   - update themes and items from nightly524
   - add the userScripts to enable tabbed passages
 - _0.6.0_ :
   - update themes and items from nightly523
   - update MathJax to 2.6.1
 - _0.5.7_ :
    - update : MathJax timeout is connected to requirejs waitSeconds config
 - _0.5.6_ :
    - update : update themes
    - add : OTF fonts for MathJax
 - _0.5.5_ :
    - update : update themes and base styles
 - _0.5.4_ :
    - update : update themes
 - _0.5.3_ :
    - update : update themes
 - _0.5.2_ :
    - update : themes and new MathJax font for OSX
 - _0.5.1_ :
    - update : themes, interactions renderers and TEIs
 - _0.5.0_ :
    - feature : TEIs support (fraction, iPassage, number line, zoom number line, line and point, graph functions)
    - fix : ckEditor initialization and destroy.
    - fix : MathJax exported exported to the golbal scope (used by TTS).
    - update : updated parcc themes (Black on white replaces PARCC and is the default).
 - _0.4.3_ :
    - fix : wait for all interactions to be ready before trigerring the ready state.
 - _0.4.2_ :
    - fix : end attempt interaction destroys correctly and send a  `responsechange` event for consistency across interactions.
 - _0.4.1_ :
    - fix : detach item stylesheet on clear.
 - _0.4.0_ :
    - done : MathJax 2.5 + HighlighAssist. PIC integration (color contrast and answer masking). Better documentation.
    - changes : Only bundles and configuration. Doc is now in the wiki. MathJax key in config. All themes content. New bundle to load : `studenttools.min.js`.
    - incomplete : PCI (TEI). Bug on CkEditor Ready. Still an issue with youtube video.
 - _0.3.0_ :
    - done : Theme support.
    - experimental : PIC control API.
    - changes : ExtendedText interaction restores state correctly. `getResponses` returns now objects (to be sent directly to the scoring engine). Config must contain theme configuration and CSS are now part of the `dist`.
    - incomplete : PCI (TEI) and PIC (Student tools). Still an issue with youtube video. Need to consolidate documentation
 - _0.2.1_ :
    - done : Math and extended text interaction. Flash fallback for videos.
    - changes : MathJax and ckedtior, and flash player fallback are now included in the form of external assets. They need to be configured.
    - incomplete : PCI (TEI) and PIC (Student tools). Stil an issue with youtube video.
 - _0.2.0_ :
    - done : Media support
    - changes : need to define assets
    - incomplete : ckeditor (Extended Text in html mode), MathJax (math support) are not yet part of the runner.
 - _0.1.6-beta_
    - done : Scopped CSS and added endattempt interaction  support
    - changes : `tao-main-style.css` is not needed anymore, but fonts are stil separated
    - incomplete : ckeditor (Extended Text in html mode), MathJax (math support) and MediaElement (used by object and media interactions) are not yet part of the runner.
 - _0.1.5-beta_
    - done : revert fix in 0.1.4 and move shuffling order in the state. Fix some shared variables for stateless interactions.
    - changes : state != response, the state contains now a `response` property under the `responseIdentifier`
 - _0.1.4-beta_
    - done : fixed shuffling by sharing the renderer
 - _0.1.3-beta_
    -  done : scoped libraries included
 - _0.1.2-beta_
    - done : state fixed and scoped libs
 - _0.1.1-beta_
    - done : state fixed and better packaging
 - _0.1.0-beta_
     - done : API, QTI Rendering, state management
     - incomplete : package management and optimisation
     - to do : asset and media management, generated jsdoc

