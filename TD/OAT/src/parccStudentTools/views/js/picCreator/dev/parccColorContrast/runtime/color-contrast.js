define([
    'IMSGlobal/jquery_2_1_1',
    'OAT/sts/common',
    'qtiInfoControlContext'
],
function ($, common, qtiInfoControlContext) {
    'use strict';

    var parccColorContrast = {
        /**
         * @event themechange
         * @param {Event} event The Event instance
         * @param {String} theme The identifier of the theme
         */

        /**
         * Keep track of the current selected theme
         * @type {String}
         */
        appliedTheme: null,

        /**
         * The component identifier
         * @type {String|Number}
         */
        id: -1,

        /**
         * Gets the type of this component
         * @returns {string}
         */
        getTypeIdentifier: function () {
            return 'parccColorContrast';
        },

        /**
         * Initialize the PIC
         *
         * @param {String} id
         * @param {Node} dom
         * @param {Object} config - json
         */
        initialize: function (id, dom, config) {
            var that = this;
            var $container = $(dom);
            var $content  = $container.find('.sts-content');
            var $launcher = $container.find('.sts-launch-button');
            var $menu = $container.find('.sts-menu-container');
            var $page = $('body');

            this.id = id;
            this.dom = dom;
            this.config = config || {};

            common.init($container, this.config);

            // close the menu when the user click outside
            $page.off('.color-contrast').on('click.color-contrast', function(event) {
                if (!$(event.target).closest('#sts-parccColorContrast').length) {
                    $menu.addClass('sts-hidden-container');
                }
            });

            // show/hide the menu when the user click on the button
            $launcher.off().on('click.color-contrast', function() {
                var cWidth;
                var offset;

                // first run only
                if(!$menu.width()) {
                    // having a defined width fixes chrome bug 'container scales instead of resizing'
                    cWidth = $container.width();
                    $container.width(2000);
                    $menu.removeClass('sts-hidden-container');
                    $menu.width($content.width());
                    $container.width(cWidth);
                    $menu.addClass('sts-hidden-container');
                }

                // show/hide the menu
                $menu.toggleClass('sts-hidden-container');

                // set the menu offset when display it
                if (!$menu.hasClass('sts-hidden-container')) {
                    offset = $launcher.offset();
                    offset.top += $launcher.outerHeight();
                    $menu.offset(offset);
                }
            });

            // apply a theme when the user select a menu entry
            $menu.off().on('click.color-contrast', 'li', function() {
                that.selectItem(this);
            });
        },

        /**
         * Reverse operation performed by render()
         * After this function is executed, only the initial naked markup remains
         * Event listeners are removed and the state and the response are reset
         */
        destroy: function () {
            var $container = $(this.dom);
            var $launcher = $container.find('.sts-launch-button');
            var $menu = $container.find('.sts-menu-container');
            var $page = $('body');

            // reset the theme
            this.applyTheme();

            // remove the elements
            $page.off('.color-contrast');
            $launcher.off('.color-contrast');
            $menu.off('.color-contrast');
            $container.remove();
        },

        /**
         * Restore the state of the interaction from the serializedState.
         *
         * @param {Object} serializedState - json format
         */
        setSerializedState: function (serializedState) {
            var $container = $(this.dom);
            var $menu = $container.find('.sts-menu-container');
            var theme = serializedState && serializedState.appliedTheme || 'default';
            var itemToSelect = $menu.find('li[data-theme="' + theme + '"]');
            this.appliedTheme = theme;
            this.selectItem(itemToSelect);

            //console.log('state set to', state);
        },

        /**
         * Get the current state of the interaction as a string.
         * It enables saving the state for later usage.
         * @returns {Object} json format
         */
        getSerializedState: function () {
            return {
                appliedTheme : this.appliedTheme
            };
        },

        /**
         * Selects an item in the menu
         * @param {jQuery|HTMLElement|String} item The menu item
         * @param {Boolean} [preventApply] Avoids the theme to be applied at the same time
         */
        selectItem : function(item, preventApply) {
            var $item = $(item);
            var $menu = $item.closest('.sts-menu-container');
            var theme = null;
            var selectedClass = 'sts-menu-item-selected';
            var isSelected = $item.hasClass(selectedClass);

            // select the active item in the list
            $menu.find('.' + selectedClass).removeClass(selectedClass);
            if (!isSelected) {
                theme = $item.data('theme');
                $item.addClass(selectedClass);
            } else {
                $menu.find('.sts-menu-item-default').addClass(selectedClass);
            }

            // apply or remove the theme
            if (!preventApply) {
                this.applyTheme(theme);
            }

            // always hide the menu after selection
            $menu.addClass('sts-hidden-container');
        },

        /**
         * Applies a particular theme
         * @param {String} [theme]
         */
        applyTheme: function(theme) {
            var $container = $(this.dom);
            var $launcher = $container.find('.sts-launch-button');

            theme = theme || 'default';

            // update the menu
            if ('default' !== theme) {
                $launcher.addClass('sts-activated');
            } else {
                $launcher.removeClass('sts-activated');
            }

            // theme change event will go through parents in DOM tree thanks to events bubbling
            this.appliedTheme = theme;
            if (!triggerCustomEvent($container.get(0), 'themechange', theme)) {
                $container.trigger('themechange', [theme]);
            }
        }
    };

    /**
     * Triggers an event
     * @param {HTMLElement} element
     * @param {String} eventName
     * @param {*} data
     * @returns {Boolean} Returns true if the event has been successfully triggered
     */
    var triggerCustomEvent = function(element, eventName, data) {
        var event;
        var result = false;

        if (window.CustomEvent) {
            event = new CustomEvent(eventName, {
                detail: data,
                bubbles: true,
                cancelable: true
            });
        } else if (document.createEvent) {
            event = document.createEvent('Event');
            event.initEvent(eventName, true, true);
            event.detail = data;
        } else if ( document.createEventObject ) {
            event = document.createEventObject();
            event.detail = data;
        }

        if (event) {
            if (element.dispatchEvent) {
                element.dispatchEvent(event);
                result = true;
            } else if (element.fireEvent) {
                element.fireEvent("on" + eventName, event);
                result = true;
            }
        }

        return result;
    }

    qtiInfoControlContext.register(parccColorContrast);
});
