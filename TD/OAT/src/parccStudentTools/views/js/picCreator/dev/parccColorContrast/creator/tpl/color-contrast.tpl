<div id="sts-{{typeIdentifier}}" class="sts-toolcontainer sts-{{typeIdentifier}}" data-position="{{position}}">
    <span class="sts-button sts-launch-button" data-typeIdentifier="{{typeIdentifier}}" title="{{title}}">
        <img src="{{icon}}" alt="{{alt}}" />
    </span>
    <div class="sts-menu-container sts-hidden-container sts-{{typeIdentifier}}-menu-container">
        <ul class="sts-menu">
            <li class="sts-menu-item sts-menu-item-default sts-menu-item-selected" data-theme="default">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "Black on White (default)" }}</span>
            </li>
            <li class="sts-menu-item sts-menu-item-black-on-cream" data-theme="blackOnCream">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "Black on Cream" }}</span>
            </li>
            <li class="sts-menu-item sts-menu-item-black-on-lightblue" data-theme="blackOnLightBlue">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "Black on Light Blue" }}</span>
            </li>
            <li class="sts-menu-item sts-menu-item-black-on-lightmagenta" data-theme="blackOnLightMagenta">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "Black on Light Magenta" }}</span>
            </li>
            <li class="sts-menu-item sts-menu-item-grey-on-green" data-theme="greyOnGreen">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "Grey on Green" }}</span>
            </li>
            <li class="sts-menu-item sts-menu-item-lightblue-on-darkblue" data-theme="lightBlueOnDarkBlue">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "Light Blue on Dark Blue" }}</span>
            </li>
            <li class="sts-menu-item sts-menu-item-white-on-black" data-theme="whiteOnBlack">
                <span class="sts-menu-item-color icon-style"></span>
                <span class="sts-menu-item-label">{{__ "White on Black" }}</span>
            </li>
        </ul>
    </div>
    <div class="sts-container sts-hidden-container sts-{{typeIdentifier}}-container"></div>
</div>
