define([
    'lodash',
    'taoQtiItem/qtiCreator/editor/infoControlRegistry',
    'parccGraphingCalculator/creator/widget/Widget',
    'tpl!parccGraphingCalculator/creator/tpl/markup'
], function(_, registry, Widget, markupTpl){

    var _typeIdentifier = 'parccGraphingCalculator',
        position = 11;

    return {
        /**
         * (required) Get the typeIdentifier of the custom interaction
         * 
         * @returns {String}
         */
        getTypeIdentifier : function(){
            return _typeIdentifier;
        },
        /**
         * (required) Get the widget prototype
         * Used in the renderer
         * 
         * @returns {Object} Widget
         */
        getWidget : function(){
            return Widget;
        },
        /**
         * (optional) Get the default properties values of the pic.
         * Used on new pic instance creation
         * 
         * @returns {Object}
         */
        getDefaultProperties : function(pic){
            return {
                movable : false,
                theme : 'tao-light',
                position : position
            };
        },
        /**
         * (optional) Callback to execute on the 
         * Used on new pic instance creation
         * 
         * @returns {Object}
         */
        afterCreate : function(pic){
            //do some stuff
        },
        /**
         * (required) Gives the qti pic xml template 
         * 
         * @returns {function} handlebar template
         */
        getMarkupTemplate : function(){
            return markupTpl;
        },
        /**
         * (optional) Allows passing additional data to xml template
         * 
         * @returns {function} handlebar template
         */
        getMarkupData : function(pic, defaultData){
            
            var manifest = registry.get(_typeIdentifier).manifest;

            defaultData = _.defaults(defaultData, {
                typeIdentifier : _typeIdentifier,
                title : manifest.description,
                position : position,
                icon : manifest.icon,
                alt : manifest.short
            });
            
            return defaultData;
        }
    };
});