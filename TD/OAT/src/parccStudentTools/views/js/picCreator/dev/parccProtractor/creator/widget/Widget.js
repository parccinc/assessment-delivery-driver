define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccProtractor/creator/widget/states/states'
], function(Widget, states){

    var ProtractorWidget = Widget.clone();

    ProtractorWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    ProtractorWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return ProtractorWidget;
});