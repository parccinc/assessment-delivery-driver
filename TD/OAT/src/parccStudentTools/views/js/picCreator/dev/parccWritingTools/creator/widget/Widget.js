define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccWritingTools/creator/widget/states/states'
], function(Widget, states){

    var WritingToolsWidget = Widget.clone();

    WritingToolsWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    WritingToolsWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return WritingToolsWidget;
});