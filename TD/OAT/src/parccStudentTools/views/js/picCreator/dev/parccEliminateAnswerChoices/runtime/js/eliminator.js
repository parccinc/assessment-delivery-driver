/*global define, sessionStorage*/
define(
    [
        'IMSGlobal/jquery_2_1_1',
        'OAT/lodash',
        'OAT/sts/stsEventManager'
    ],
    function ($, _, eventManager) {
        'use strict';
        return function (options) {
            var that = this,
                defaultOptions = {
                    choiceContainerSelector : '.qti-choiceInteraction', //available only for choice interaction
                    choiceSelector : '.qti-choice',
                    eventNS : 'parccEliminateAnswerChoices'
                };

            /**
             * Container of choices
             * 
             * @type {jQueryElement} 
             */
            this.$choiceContainer = null;

            /**
             * Whether the elimination mode is enabled (button pressed)
             * 
             * @type {jQueryElement}
             */
            this.active = false;
            
            /**
             * List of tools id that can't be used with current tool.
             * 
             * @type {array}
             */
            this.mutuallyExclusiveTools = ['parccAnswerMasking'];

            /**
             * Tool button on the student tools panel
             * 
             * @type {jQueryElement}
             */
            this.$toolButton;
            
            /**
             * Init function
             * @returns {undefined}
             */
            this.init = function () {
                options = _.merge(defaultOptions, options);
                
                that.$choiceContainer = $(options.choiceContainerSelector);
                that.$toolButton = options.$container.find('.sts-button');
                
                if (!(that.$choiceContainer instanceof $)) {
                    throw new TypeError("options.choiceContainerSelector option should be selector of existing element.");
                }
                
                var $choices = that.$choiceContainer.find(options.choiceSelector);
                
                //if less than one choice or authoring mode then disable the tool
                if (options.scenario) {
                    if (($choices.length <= 1 && options.scenario === 'runtime')
                        || options.scenario === 'authoring'
                    ) {
                        that.$toolButton.addClass('disabled');
                        return;
                    } 
                }
                
                
                options.$container.on('click', '.sts-button', function () {
                    if (that.active) {
                        that.disable();
                    } else {
                        that.enable();
                    }
                });

                var state = that.getInteractionState();
                if (state && state.length) {
                    $.each(that.getInteractionState(), function (key, val) {
                        var $choice = $(options.choiceSelector + '[data-serial="' + val + '"]');
                        that.toggleState($choice);
                    });
                }
                
                eventManager.on('stsEnabled', function (data) {
                    if (data && data.stsName && that.mutuallyExclusiveTools.indexOf(data.stsName) >= 0) {
                        that.destroy();
                    }
                });
            };

            /**
             * Enable elimination mode. Clicking on an choice will cross out it and disable the input.
             * Click again to restore the original state.
             * 
             * @returns {undefined}
             */
            this.enable = function () {
                var $mask;
                if (this.active) {
                    return;
                }
                
                eventManager.trigger('stsEnabled', [{stsName : options.id}]);
                
                options.$container.find('.sts-button').addClass('active');
                that.active = true;
                that.$choiceContainer.addClass('js-choice-eliminating choice-eliminating');
                
                that.$choiceContainer.find(options.choiceSelector).each(function () {
                    $mask = $('<div class="eliminated-choice-wrap"></div>');
                    $mask.on('click', function () {
                        var $choice = $(this).closest(options.choiceSelector);
                        that.toggleState($choice);
                        return false;
                    });
                    $(this).prepend($mask);
                });
            };

            /**
             * Toggle choice state
             * @param {jQueryElement} $choice
             * @returns {undefined}
             */
            this.toggleState = function ($choice) {
                if ($choice.hasClass('eliminated-choice')) {
                    $choice.removeClass('eliminated-choice');
                    $choice.find('input:visible').removeAttr('disabled');
                } else {
                    $choice.addClass('eliminated-choice');
                    $choice.find('input:visible')
                           .attr('disabled', 'disabled')
                           .prop('checked', false)
                           .trigger('change');
                }
                that.saveInteractionState();
            };

            /**
             * Save eliminated choices in sessionStorage
             * @todo move this method to setSerializedState() mentod of interaction
             */
            this.saveInteractionState = function () {
                var interactionSerial = that.$choiceContainer.find('.qti-interaction').data('serial'),
                    choices = [];
                that.$choiceContainer.find(options.choiceSelector + '.eliminated-choice').each(function () {
                    var choiceSerial = $(this).data('serial');
                    choices.push(choiceSerial);
                });
                sessionStorage.setItem(options.eventNS + interactionSerial, JSON.stringify(choices));
            };

            /**
             * Get array of eliminated choices from sessionStorage
             * @todo move this method to getSerializedState() mentod of interaction
             */
            this.getInteractionState = function () {
                var interactionSerial = that.$choiceContainer.find('.qti-interaction').data('serial'),
                    state = sessionStorage.getItem(options.eventNS + interactionSerial);
                return JSON.parse(state);
            };

            /**
             * Disable elimination mode.
             * @returns {undefined}
             */
            this.disable = function () {
                if (!this.active) {
                    return;
                }
                that.active = false;
                options.$container.find('.sts-button').removeClass('active');

                $('.eliminated-choice-wrap').remove();
                
                that.$choiceContainer.removeClass('js-choice-eliminating choice-eliminating');
            };

            /**
             * Destroy and disable all changes
             * @returns {undefined}
             */
            this.destroy = function () {
                that.disable();
                that.$choiceContainer.find('.eliminated-choice input:visible').removeAttr('disabled');
                that.$choiceContainer.find('.eliminated-choice').removeClass('eliminated-choice');
            };
            this.init();
        };
    }
);