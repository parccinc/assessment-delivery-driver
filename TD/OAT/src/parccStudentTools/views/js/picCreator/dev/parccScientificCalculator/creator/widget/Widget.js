define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccScientificCalculator/creator/widget/states/states'
], function(Widget, states){

    var ScientificCalculatorWidget = Widget.clone();

    ScientificCalculatorWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    ScientificCalculatorWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return ScientificCalculatorWidget;
});