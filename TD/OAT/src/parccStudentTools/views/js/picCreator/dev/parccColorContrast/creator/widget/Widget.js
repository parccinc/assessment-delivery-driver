define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccColorContrast/creator/widget/states/states'
], function (Widget, states) {
    'use strict';

    var ColorContrastWidget = Widget.clone();

    ColorContrastWidget.initCreator = function () {

        this.registerStates(states);
        Widget.initCreator.call(this);
    };

    ColorContrastWidget.buildContainer = function () {

        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };

    return ColorContrastWidget;
});
