<div id="sts-{{typeIdentifier}}" class="sts-toolcontainer" data-position="{{position}}">
    <span class="sts-button sts-launch-button" data-typeIdentifier="{{typeIdentifier}}" title="{{title}}">
        <img src="{{icon}}" alt="{{alt}}" />
    </span>
    <div class="sts-container sts-hidden-container sts-{{typeIdentifier}}-container">
        <!-- Tool content here -->
    </div>
</div>