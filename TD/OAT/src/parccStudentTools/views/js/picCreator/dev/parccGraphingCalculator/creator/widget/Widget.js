define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccGraphingCalculator/creator/widget/states/states'
], function(Widget, states){

    var GraphingCalculatorWidget = Widget.clone();

    GraphingCalculatorWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    GraphingCalculatorWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return GraphingCalculatorWidget;
});