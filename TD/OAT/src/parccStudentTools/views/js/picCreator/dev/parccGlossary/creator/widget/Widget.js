define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccGlossary/creator/widget/states/states'
], function(Widget, states){

    var GlossaryWidget = Widget.clone();

    GlossaryWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    GlossaryWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return GlossaryWidget;
});