define([
    'IMSGlobal/jquery_2_1_1',
    'OAT/sts/common',
    'qtiInfoControlContext',
    'parccAnswerMasking/runtime/js/answer-masking'
], function ($, common, qtiInfoControlContext, AnswerMasking) {
    'use strict';


    var parccAnswerMasking = {

        id : -1,

        /**
         * Get the type of this tool
         * @returns {String} the type
         */
        getTypeIdentifier : function getTypeIdentifier () {
            return 'parccAnswerMasking';
        },

        /**
         * Initialize the PIC
         *
         * @param {String} id
         * @param {Node} dom
         * @param {Object} config - json
         */
        initialize : function initialize (id, dom, config) {
            var $container,
                scenario;
            this.id = id;
            this.dom = dom;
            this.config = config || {};

            $container = $(dom);
            scenario = this._taoInfoControl.metaData.scenario || 'runtime';

            this.renderer = new AnswerMasking({
                $container : $container,
                id : this.getTypeIdentifier(),
                scenario : scenario
            });

            common.init($container, this.config);
        },

        /**
         * Reverse operation performed by render()
         * After this function is executed, only the initial naked markup remains
         * Event listeners are removed and the state and the response are reset
         */
        destroy : function destroy () {
            this.renderer.destroy();
            $(this.dom).remove();
        },

        /**
         * Restore the state of the tool from the state
         *
         * @param {Object} state - the state data
         * @param {Boolean} [state.active] - is the tool active
         * @param {String[]} state.choices - serials of visible choices
         */
        setSerializedState : function setSerializedState (state) {
             if(this.renderer && state && state.choices){
                if(state.active){
                    this.renderer.enable();
                    this.renderer.setInteractionState(state.choices);
                } else {
                    this.renderer.disable();
                }
            }
        },

        /**
         * Get the current state of the tool.
         * It enables saving the state for later usage.
         *
         * @returns {Object} The state data (see setSerializedState for the format)
         */
        getSerializedState : function getSerializedState () {
            var state = {};
            if(this.renderer){
                state.active = this.renderer.active;
                state.choices = this.renderer.getInteractionState();
            }
            return state;
        }
    };

    qtiInfoControlContext.register(parccAnswerMasking);
});
