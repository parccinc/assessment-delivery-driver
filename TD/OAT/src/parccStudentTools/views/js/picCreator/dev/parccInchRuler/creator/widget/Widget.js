define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccInchRuler/creator/widget/states/states'
], function(Widget, states){

    var InchRulerWidget = Widget.clone();

    InchRulerWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    InchRulerWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return InchRulerWidget;
});