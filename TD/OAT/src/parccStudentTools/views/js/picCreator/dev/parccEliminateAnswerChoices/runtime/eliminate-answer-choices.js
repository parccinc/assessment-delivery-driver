/*global define*/
define(
    [
        'IMSGlobal/jquery_2_1_1',
        'OAT/sts/common',
        'qtiInfoControlContext',
        'parccEliminateAnswerChoices/runtime/js/eliminator'
    ],
    function ($, common, qtiInfoControlContext, Eliminator) {
        'use strict';
        
        var parccEliminateAnswerChoices = {
            id : -1,
            getTypeIdentifier : function () {
                return 'parccEliminateAnswerChoices';
            },
            /**
             * Initialize the PIC
             *
             * @param {String} id
             * @param {Node} dom
             * @param {Object} config - json
             */
            initialize : function (id, dom, config) {
                this.id = id;
                this.dom = dom;
                this.config = config || {};
                var $container = $(dom),
                    scenario = this._taoInfoControl.metaData.scenario || 'runtime';

                new Eliminator({
                    $container : $container,
                    id : this.getTypeIdentifier(),
                    scenario : scenario
                });
                
                common.init($container, this.config);
            },
            /**
             * Reverse operation performed by render()
             * After this function is executed, only the initial naked markup remains
             * Event listeners are removed and the state and the response are reset
             *
             * @param {Object} interaction
             */
            destroy : function () {
                $(this.dom).remove();
            },
            /**
             * Restore the state of the interaction from the serializedState.
             *
             * @param {Object} interaction
             * @param {Object} serializedState - json format
             */
            setSerializedState : function (state) {
                //console.log('state set to', state);
            },
            /**
             * Get the current state of the interaction as a string.
             * It enables saving the state for later usage.
             *
             * @param {Object} interaction
             * @returns {Object} json format
             */
            getSerializedState : function () {
                return {};
            }
        };

        qtiInfoControlContext.register(parccEliminateAnswerChoices);
    }
);