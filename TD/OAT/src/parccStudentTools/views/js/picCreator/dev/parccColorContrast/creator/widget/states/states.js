define([
    'taoQtiItem/qtiCreator/widgets/states/factory',
    'taoQtiItem/qtiCreator/widgets/static/portableInfoControl/states/states'
], function (factory, states) {
    'use strict';

    return factory.createBundle(states, arguments);
});
