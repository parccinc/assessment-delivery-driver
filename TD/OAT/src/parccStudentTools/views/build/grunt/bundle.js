module.exports = function(grunt) {

    var requirejs   = grunt.config('requirejs') || {};

    var root        = grunt.option('root');
    var out         = 'output';

    var tools = [
        'studentToolbar/runtime/studentToolbar',
        'parccAnswerMasking/runtime/answer-masking',
        'parccColorContrast/runtime/color-contrast'
    ];

    requirejs.parccstudenttools = {
        options: {
            baseUrl : root + '/parccStudentTools/views/js/picCreator/dev/',
            findNestedDependencies : true,
            uglify2: {
                mangle : false,
                output: {
                    'max_line_len': 400
                }
            },
            paths : {
                'IMSGlobal' : root + '/taoQtiItem/views/js/portableSharedLibraries/IMSGlobal',
                'OAT' : root + '/taoQtiItem/views/js/portableSharedLibraries/OAT',
                'PARCC' : root + '/taoQtiItem/views/js/portableSharedLibraries/PARCC',
                'studentToolbar' : root + '/qtiItemPic/views/js/picCreator/dev/studentToolbar',
                'qtiInfoControlContext' : root + '/taoQtiItem/views/js/runtime/qtiInfoControlContext',
            },
            wrapShim: true,
            inlineCss : true,
            include : tools,
            exclude : ['qtiInfoControlContext'],
            out: out + "/studenttools.min.js"
        }
    };

    grunt.config('requirejs', requirejs);
};
