/**
 * Copyright (c) 2016 Open Assessment Technologies, S.A.
 *
 * @author A.Zagovorichev, <zagovorichev@1pt.com>
 */
define([
    'lodash',
    'taoParcc/lib/khan/kas'
], function(_, KAS) {
    'use strict';

    /**
     * Process operands and returns the count of the points
     * 
     * operands: [
     *      0: [point, string],
     *          `[`y x`, '0 0', '3 4']`
     *      1: [string] with formula, that include x,y operands
     *          correct `y=x^2`, `x+2=y-x-2`
     *          incorrect `Y=x^2` camel case sensitive
     * ]
     * 
     * if base type = Point, than x and y can be set only as integer variables
     * if base type = String, we can work with string, spec. symbols like #pi
     * 
     * @exports taoParcc/scoring/processor/expressions/operators/custom/countPointsThatSatisfyEquation
     * @return int or null
     */
    return {
        constraints : {
            minOperand: 2,
            maxOperand: 2,
            cardinality: ['multiple', 'ordered', 'single'],
            baseType: ['point', 'string']
        },
        
        operands: [],
        
        process: function process() {
            var points, equation, expr, baseType;
            var x,y, strs='';
            var result = {
                cardinality : 'single',
                baseType : 'integer',
                value: 0
            };

            //if at least one operand is null, then break and return null
            if (!this.operands || _.some(this.operands, _.isNull) === true || _.some(this.operands[0], _.isNull) === true || !this.operands[1]) {
                return null;
            }
            
            points = this.preProcessor.parseVariable(this.operands[0]).value;
            if (_.isString(points)) {
                points = [points];
            }
            
            baseType = this.operands[0].baseType;
            equation = this.preProcessor.parseVariable(this.operands[1]).value;
            
            if (equation === '') {
                return null;
            }

            equation = equation.replace('#', '\\');

            expr = KAS.parse(equation, {}).expr;
            
            /*
            * incorrect work with equation  y=4x^2+xy => y+-4*x^(2)+-1 But should be y+-4x^(2)+-1x*y
            if (_.isFunction(expr.asExpr)) {
                expr = expr.asExpr(true);
            }*/
            
            if (expr === undefined) {
                return null;
            }

            _.each(points, function (v) {
                
                if (baseType === 'string') {
                    strs = v.split("\x20");
                    if (strs.length !== 2) {
                        return null;
                    }
                    
                    x = strs[0];
                    y = strs[1];
                } else {
                    // point
                    x = v[0];
                    y = v[1];
                }
                
                if (_.isString(x)) {
                    x = x.replace(/#/g, '');
                    // if in operands pi, or other formulas
                    x = KAS.parse(x).expr.eval();
                }
                if (_.isString(y)) {
                    y = y.replace(/#/g, '');
                    y = KAS.parse(y).expr.eval();
                }
                
                var compare = expr.left.eval({x: x, y: y}) - expr.right.eval({x: x, y: y});

                // Math.sin((Math.PI)) === 1.2246467991473532e-16
                if (compare >=-1e-10 && compare <= 1e-10) {
                    result.value++;
                }
            });

            return result;
        }
    };
});
