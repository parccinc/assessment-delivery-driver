/**
 * @author Bertrand Chevrier <bertrand@taotesting.com>
 * @see {@link https://github.com/oat-sa/parc-itemrunner/wiki}
 */

'use strict';

var version = "0.7.1";

var defaultTheme = 'blackOnWhite';

//configuration of the bundle: libraries paths,
requirejs.config({
    baseUrl : 'js',
    urlArgs : "buster=" + version ,      //buster ensure files are not in cache for that version


    //alias for external librairies, paths are relative to the baseUrl (ie. requirejs)
    paths : {
        'ckeditor' : '../dist/assets/ckeditor/ckeditor',
        'mathJax'  : '../dist/assets/mathjax/MathJax.min'
    },

    //configure paths for external libraries, they are relative to the page
    config : {
        'mediaElement' : {
            pluginPath : 'dist/assets/mediaelement/' //where mediaElement loads it's plugin based players
        },

        'MathJax/MathJax' : {
            root : 'dist/assets/mathjax/'       //where MathJax will look for it's fonts
        },

        'ui/themes' : {
            items  : {                          //register the themes for the IR (at least the default is required)
                base    : 'dist/qti-runner.css',
                default : defaultTheme,
                available : [{
                    id:   'tao',
                    path: 'dist/themes/tao-default/theme.css'
                }, {
                    id:   'blackOnWhite',
                    path: 'dist/themes/black-on-white/theme.css'
                }, {
                    id:   'whiteOnBlack',
                    path: 'dist/themes/white-on-black/theme.css'
                }, {
                    id:   'blackOnCream',
                    path: 'dist/themes/black-on-cream/theme.css'
                }, {
                    id:   'blackOnLightBlue',
                    path: 'dist/themes/black-on-light-blue/theme.css'
                }, {
                    id:   'blackOnLightMagenta',
                    path: 'dist/themes/black-on-light-magenta/theme.css'
                }, {
                    id:   'greyOnGreen',
                    path: 'dist/themes/grey-on-green/theme.css'
                }, {
                    id:   'lightBlueOnDarkBlue',
                    path: 'dist/themes/light-blue-on-dark-blue/theme.css'
                }]
            }
        }
    },

    //CKEditor still exposes a global variable, it needs to be shimed.
    shim : {
        'ckeditor' : {
            exports  : 'CKEDITOR'
        }
    }
});

require(['../dist/qtiItemRunner.min', '../dist/tei.min', '../dist/studenttools.min'], function(itemRunner) {

    var runner;

    var itemContainer   = document.querySelector('#item-container');

    var output          = document.querySelector('#output');
    var selectItem      = document.querySelector('.item-selection');
    var itemContent     = document.querySelector('.item-content');
    var loadBtn         = document.querySelector('.load');
    var clearBtn        = document.querySelector('.destroy');
    var getStateBtn     = document.querySelector('.getstate');
    var getResponsesBtn = document.querySelector('.getresponses');
    var setStateBtn     = document.querySelector('.setstate');
    var manStateBtn     = document.querySelector('.manstate');
    var manStateContent = document.querySelector('.manual-state-content');
    var stateOnLoad     = document.querySelector('.state-on-load');
    var clearOutputLink = document.querySelector('.clear-output a');
    var themeSwitcher   = document.querySelector('.theme-switcher');
    var themeSelected   = document.querySelector('.theme-switcher option[value="' + defaultTheme + '"]');

    if (themeSelected) {
        themeSelected.childNodes[0].nodeValue += ' (default)';
    }
    themeSwitcher.value = defaultTheme;

    var runItem = function runtItem(itemPath, itemPack) {

                /**
                 * Create asset management strategies,
                 * each URL is resolved by the handler in the order they are defined
                 * until the strategy returns something.
                 * Here the middleware is resolves URLs using this stack:
                 *   -  if the  URL contains the pattern [[OAT]] then do an replacement
                 *   -  else if the URL is absolute URL we return it intact
                 *   -  else if the URL is relative and is a video we use a base URL
                 *   -  otherwise we look into the item package to get the value
                 */
                var strategies = [{

                    name : 'hosted',
                    handle : function handleHosted(url){
                        if(url.source.indexOf('[[OAT]]') !== -1){
                            return  (url.toString()).replace(/\[\[OAT\]\]/gi, 'http://www.taotesting.com/wp-content/uploads/');
                        }
                    }

                },{
                    name : 'external',
                    handle : function handleExternal(url){
                        if(url.source !== url.relative){
                            return  url.toString();
                        }
                    }
                }, {
                    name : 'video',
                    handle : function handleVideo(url){
                        //is relative and audio/video
                        if(url.source === url.relative && /\.(mp4|avi|ogg|mp3)$/.test(url.file) ){
                            return 'samples/resources/' + url.file;
                        }
                    }
                }, {
                    name : 'packed',
                    handle : function handlePacked(url){
                        var packKey = url.toString();
                        for(var type in itemPack.assets){
                            if(itemPack.assets[type][packKey]){

                                //preven loading of items CSS files
                                //if(/\.css$/.test(url)){
                                    //return false;
                                //}

                                return itemPack.assets[type][packKey];
                            }
                        }
                        return url.toString();
                    }
                }];

                //require a custom user script "passagify" to render tabbed passages correctly
                require(['../dist/userScripts/passagify.min'], function(passagify){

                    /**
                    * Creates a new item runner instance
                    */
                   runner = itemRunner(itemPack.data)      //set data
                       .on('error', function(err) {        //listen for events
                           out(err, 'Error');
                       })
                       .on('init', function() {
                           out('init', 'Event');
                       })
                       .on('render', function() {
                           out('ready', 'Event');

                           //execute the passagify script to render the tabbed passages correctly
                           passagify.exec();

                           if (stateOnLoad.checked && window.localStorage.getItem(itemPath)) {
                               var state = JSON.parse(window.localStorage.getItem(itemPath));
                               this.setState(state);
                               out(state, 'setState');
                           }
                       })
                       .on('statechange', function(state) {
                           out(state, 'Event');
                           window.localStorage.setItem(itemPath, JSON.stringify(state));
                       })
                       .on('clear', function() {
                           out('clear', 'Event');
                       })
                       .on('endattempt', function(responseIdentifier) {
                           out('endattempt from ' + responseIdentifier , 'Event');
                       })
                       .assets(strategies)             //define the asset strategies (before init)
                       .init()                         //load data
                       .render(itemContainer);         //renders
                });

    };

    loadBtn.addEventListener('click', function() {

        var itemPath,
            itemData;
        if (selectItem.value) {
            itemPath = selectItem.value;
            out('Loading sample ' + itemPath, '');

            return loadItemData('samples/' + itemPath, function(itemPack){
                runItem(itemPath, itemPack);
            });
        }
        if (itemContent.value) {
            out('Loading content ', '');
            try {
                itemData = JSON.parse(itemContent.value);
            } catch(err){
                out(err, 'Parsing error');
            }
            if(itemData && !itemData.data && itemData.identifier){
                itemData = {
                    type : "qti",
                    data : itemData,
                    assets : {}
                };
            }
            if(itemData && itemData.data && itemData.data.identifier){
                return runItem('__paste/' + itemData.data.identifier, itemData);
            }
            out("The given data is not an item pack", 'Paste error');
        }
    });

    getStateBtn.addEventListener('click', function() {
        if (runner) {
            var state = runner.getState();
            out(state, 'getState');
        }
    });

    getResponsesBtn.addEventListener('click', function() {
        if (runner) {
            var responses = runner.getResponses();
            out(responses, 'getResponses');
        }
    });

    setStateBtn.addEventListener('click', function() {
        if (runner) {
            var itemPath = selectItem.value;
            if (itemPath && window.localStorage.getItem(itemPath)) {
                var state = JSON.parse(window.localStorage.getItem(itemPath));
                runner.setState(state);
                out(state, 'setState');
            }
        }
    });

    clearBtn.addEventListener('click', function() {
        if(runner){
            runner.clear();

            themeSwitcher.value = defaultTheme;
        }
    });

    manStateBtn.addEventListener('click', function() {
        var state;
        var itemPath;
        if (manStateContent.value) {
            try {
                state = JSON.parse(manStateContent.value);
            } catch (e) {
                console.error(e);
            }
            if (state) {
                runner.setState(state);
                itemPath = selectItem.value;
                if (itemPath) {
                    window.localStorage.setItem(itemPath, manStateContent.value);
                }

                out(state, 'setState');
            }
        }
    });

    clearOutputLink.addEventListener('click', function(e) {
        e.preventDefault();
        output.innerHTML = '';
    });

    themeSwitcher.addEventListener('change', function(e) {
        if(!itemContainer.hasChildNodes()){
            this.value = defaultTheme;
            return false;
        }
        itemContainer.dispatchEvent(new CustomEvent('themechange',  { bubbles: true, detail : this.value } ));
    });

    function out(content, title) {

        if (typeof content === 'object') {
            content = '<pre>' + JSON.stringify(content, null, '  ') + '</pre>';
        }
        output.innerHTML = output.innerHTML +
            '<p>[' + new Date().toLocaleTimeString() + '] <strong>' + title + '</strong> ' + content + '</p>';
        output.scrollTop = output.scrollHeight;
    }

    function loadItemData(url, cb) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                cb(JSON.parse(xhr.responseText));
            }
        };
        xhr.open("GET", url, true);
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.send();
    }
});
