module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({

        stagingFolder: "staging",
        releaseFolder: "release",


        pkg: grunt.file.readJSON('package.json'),

        // Delete the contents of the "release" folders
        clean: {
            options: {
                force: true,
            },
            releaseDebug: ["stagingDebug/**", "releaseDebug/**"],
            release: ["staging/**", "release/**"],
            staging: ["staging/**"],
            stagingDebug: ["stagingDebug/**"],

        },

        replace: {
            releaseBuild: {
                options: {
                    force: true,
                    preserveOrder: true,
                    patterns: [
                        {
                            match: /(\.css)/ig,
                            replacement: '-min.css'
                        }
                    ]
                },
                files: [
                    {
                        expand: true, flatten: false, src: ['scripts/td-theme.js'], dest: '<%= stagingFolder %>/'
                    }
                ]
                
            },
            releaseIndex: {
                options: {
                    force: true,
                    preserveOrder: true,
                    patterns: [
                        {
                            match: 'version',
                            replacement: '<%= releaseFolder %>'
                        }
                    ]
                },
                files: [
                    {
                        expand: true, flatten: false, src: ['index-build.html'], dest: '../',
                        rename: function () {
                            return 'index-<%= releaseFolder %>.html';
                        }
                    }
                ]
            }
        },

        // Minify and obfuscate the javascript.  
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                //  nameCache: 'tmp/grunt-uglify-cache.json',
                //   mangleProperties: true,
                //   reserveDOMProperties: true,
                //  reserveDOMCache: true,
                //  exceptionsFiles: ['domprops.json'],
                mangle: {
                    toplevel: true,
                    except: [ // mangle is true for all else besides the specified exceptions
                      'scripts/distribution/jquery-1.8.0.min.js',
                      'scripts/distribution/jquery-ui-1.8.23.custom.min.js',
                      'scripts/distribution/jquery.cookie-1.4.1.min.js',
                      'scripts/distribution/screenfull.min.js',
                      'scripts/distribution/jquery.mousewheel.min.js',
                      'scripts/distribution/jquery.sha256.min.js',
                    ]
                },
                sourceMap: '<%= includeSourceMap %>',
                compress: {
                    drop_console: true,
                    drop_debugger: true
                }
            },           

            releaseBuild: {
                files: {
                    // TODO:  Add any NEW files that should be compressed/obfuscated here!
                    // this will put all of the files listed in the array below into the file named release/scripts/td-min.js
                    '<%= releaseFolder %>/scripts/td-min.js': [                        
                      //'scripts/td-debug.js', // This is for DEBUGGING only and should never be in the Release versions!
                      'scripts/distribution/jquery-1.8.0.min.js',
                      'scripts/distribution/jquery-ui-1.8.23.custom.min.js',
                      'scripts/distribution/jquery.cookie-1.4.1.min.js',
                      'scripts/distribution/screenfull.min.js',
                      'scripts/distribution/jquery.event.move.js',
                      'scripts/distribution/jquery.event.swipe.js',
                      'scripts/distribution/jquery.mousewheel.min.js',
                      'scripts/distribution/jquery.sha256.min.js',
                      'scripts/distribution/jquery.maskedinput.js',
                      'scripts/distribution/jquery.treetable.js',

                     'scripts/widgets/util/utility.js',
                     'scripts/widgets/util/results.js',
                     'scripts/widgets/selectionengine/engine.js',
                     'scripts/widgets/selectionengine/factory.js',
                     'scripts/widgets/selectionengine/algorithm/linear.js',
                     'scripts/widgets/selectionengine/algorithm/nonlinear.js',
                     'scripts/widgets/selectionengine/algorithm/random.js',
                     'scripts/widgets/selectionengine/algorithm/dcm.js',
                     'scripts/widgets/selectionengine/algorithm/asynchronousdcm.js',
                     'scripts/widgets/scorereport/main.js',
                     'scripts/widgets/scorereport/readingcomp.js',
                     'scripts/widgets/scorereport/readermotivation.js',
                     'scripts/widgets/scorereport/mathcomp.js',
                     'scripts/widgets/scorereport/mathfluency.js',
                     'scripts/widgets/scorereport/general.js',
                     'scripts/distribution/ttsCallbacks.js',

                       '<%= stagingFolder %>/scripts/td-theme.js',
                     'scripts/widgets/ui/_base/FormBaseWidget.js',
                     'scripts/widgets/ContentWidget.js',
                     'scripts/widgets/ui/LoginWidget.js',
                     'scripts/widgets/ui/ItemWidget.js',
                     'scripts/widgets/DataAccessLayerWidget.js',
                     'scripts/widgets/ui/StickyHeaderWidget.js',
                     'scripts/widgets/ui/LogoWidget.js',
                     'scripts/widgets/ui/NotificationsWidget.js',
                     'scripts/widgets/ui/WelcomeWidget.js',
                     'scripts/widgets/ui/ConfirmStudentWidget.js',
                     'scripts/widgets/ui/ConfirmTestWidget.js',
                     'scripts/widgets/ui/StartTestWidget.js',
                     'scripts/widgets/ui/TestInfoWidget.js',
                     'scripts/widgets/ui/FlagWidget.js',
                     'scripts/widgets/ui/ClockWidget.js',
                     'scripts/widgets/ui/ReviewLinkWidget.js',
                     'scripts/widgets/ui/SaveExitWidget.js',
                     'scripts/widgets/ui/CandidateInfoWidget.js',
                     'scripts/widgets/ui/TallyWidget.js',
                     'scripts/widgets/ui/NavigationWidget.js',
                     'scripts/widgets/ui/SaveSubmitWidget.js',
                     'scripts/widgets/ui/SpinnerWidget.js',
                     'scripts/widgets/DataCommWidget.js',
                     'scripts/widgets/ItemInteractionWidget.js',
                     'scripts/widgets/ui/ScrollerWidget.js',
                     'scripts/widgets/ui/LineReaderWidget.js',
                     'scripts/widgets/ui/StickyFooterWidget.js',
                     'scripts/widgets/ui/ToolWidget.js',
                     'scripts/widgets/ui/NotificationDialogWidget.js',
                     'scripts/widgets/ui/TTSToolboxWidget.js',
                     'scripts/widgets/ui/ScoreReportWidget.js',
                     'scripts/widgets/TimerWidget.js',
                     'scripts/widgets/util/utility.js',
                     'scripts/widgets/util/results.js',
                     'scripts/widgets/selectionengine/engine.js',
                     'scripts/widgets/selectionengine/factory.js',
                     'scripts/widgets/selectionengine/algorithm/linear.js',
                     'scripts/widgets/selectionengine/algorithm/nonlinear.js',
                     'scripts/widgets/selectionengine/algorithm/random.js',
                     'scripts/widgets/selectionengine/algorithm/dcm.js',
                     'scripts/widgets/selectionengine/algorithm/asynchronousdcm.js',
                     'scripts/widgets/scorereport/main.js',
                     'scripts/widgets/scorereport/readingcomp.js', 
                     'scripts/widgets/scorereport/readermotivation.js',
                     'scripts/widgets/scorereport/mathcomp.js',
                     'scripts/widgets/scorereport/mathfluency.js',
                     'scripts/widgets/scorereport/general.js',
                     'scripts/distribution/ttsCallbacks.js',
                    ],
                    '<%= releaseFolder %>/OAT/js/main.js': [
                        'OAT/js/main.js'
                    ],
                    '<%= releaseFolder %>/OAT/vendor/require.js': [
                        'OAT/vendor/require.js'
                    ],
                }
            },
        },
        // Minifies the css files.
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            // TODO:  Add any NEW css files that should be compressed here!            
            releaseBuild: {
                files: {
                    //'<%= releaseFolder %>/styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom-min.css': [
                    //    'styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom.css'
                    //],
                    '<%= releaseFolder %>/styles/td-min.css': [
                        'styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom.css',
                        'styles/ScrollerWidget.css',
                        'styles/LineReaderWidget.css',
                        'styles/jquery.treetable.css',
                        'styles/jquery.treetable.theme.default.css',
						'styles/td-dbg.css',                     
                        'styles/td.css',
                    ],
                    '<%= releaseFolder %>/styles/td-purple-min.css': [
                        'styles/td-purple.css',
                    ],
                    '<%= releaseFolder %>/styles/td-green-min.css': [
                        'styles/td-green.css',
                    ],
                    '<%= releaseFolder %>/OAT/dist/assets/ckeditor/contents.css': [
                        'OAT/dist/assets/ckeditor/contents.css',
                    ],
                    '<%= releaseFolder %>/OAT/dist/assets/ckeditor/skins/tao/scss/dialog.css':[
                        'OAT/dist/assets/ckeditor/skins/tao/scss/dialog.css',
                    ],
                    '<%= releaseFolder %>/OAT/dist/assets/ckeditor/skins/tao/scss/editor.css': [
                        'OAT/dist/assets/ckeditor/skins/tao/scss/editor.css',
                    ]
                }
            },
        },
        // Copies folders/files that were untouched by the processes above but still need to exist inside of the release folder.
        copy: {
            release: {
              
                files: [

                  // includes files within path and its sub-directories
                  { expand: true, src: ['OAT/dist/**', '!OAT/dist/themes/tao-default/**', '!**/*.map'], dest: '<%= releaseFolder %>/' },
                  { expand: true, src: ['images/**'], dest: '<%= releaseFolder %>/' },
                  { expand: true, flatten:true, src: ['styles/jquery-ui-1.8.22.custom/images/*'], dest: '<%= releaseFolder %>/styles/images/' },
                  //{ expand: true, src: ['styles/jquery-ui-1.8.22.custom/jquery-ui-1.8.22.custom-min.css'], dest: '<%= releaseFolder %>/' },
                 // { expand: true, src: ['favicon.ico'], dest: '<%= releaseFolder %>/' },
                ],
            },
            releaseDebug: {
                files: [
                    { expand: true, src: ['OAT/dist/**', '!OAT/dist/themes/tao-default/**'], dest: '<%= releaseFolder %>/' },
                    { expand: true, src: ['images/**'], dest: '<%= releaseFolder %>/' },
                    { expand: true, flatten: true, src: ['styles/jquery-ui-1.8.22.custom/images/*'], dest: '<%= releaseFolder %>/styles/images/' },

                ],
            }
        },

    });

    // Load the plugins that provide each task.
    grunt.loadNpmTasks('grunt-replace');  // https://github.com/outatime/grunt-replace
    grunt.loadNpmTasks('grunt-contrib-uglify'); // https://github.com/gruntjs/grunt-contrib-uglify 
    grunt.loadNpmTasks('grunt-contrib-cssmin'); // https://github.com/gruntjs/grunt-contrib-cssmin 
    grunt.loadNpmTasks('grunt-contrib-copy'); // https://github.com/gruntjs/grunt-contrib-copy
    grunt.loadNpmTasks('grunt-contrib-clean'); // https://github.com/gruntjs/grunt-contrib-clean


    grunt.registerTask('build_TD', 'Set a global variable.', function (val) {
        //Initialize the parameter name for the release folder based on "val".  For the "debug" task below "build_TD" is run with "Debug" param which creates releaseFolderDebug.
        grunt.log.writeln("Inside set_global");
        grunt.log.writeln(val);


        grunt.config.set('stagingFolder', 'staging' + val);
        grunt.config.set('releaseFolder', 'release' + val);

        grunt.config.set('includeSourceMap', false);

        if (val == 'Debug'){
            grunt.config.set('includeSourceMap', true);
        }
      
 
        var releaseParam = 'release' + val;
        var stagingParam = 'staging' + val
        

        var stagingFolder = grunt.config.get('stagingFolder');
        var releaseFolder = grunt.config.get('releaseFolder')


        grunt.log.writeln(stagingFolder);
        grunt.log.writeln(releaseFolder);
        grunt.log.writeln(grunt.config.get('includeSourceMap'));
       
        // First copy OAT folders with existing files, and then uglify js and min css and overwrite files.
        grunt.task.run('clean:' + releaseParam, 'replace:releaseBuild', 'replace:releaseIndex', 'copy:' + releaseParam, 'uglify:releaseBuild', 'cssmin:releaseBuild', 'clean:' + stagingParam);


        //grunt.config.set('releaseFolder', 'release' + val);

         //var releaseParam = 'release' + val;

         //grunt.task.run('clean:' + releaseParam, 'uglify:releaseBuild', 'cssmin:releaseBuild', 'copy:releaseBuild');
    });


    // Default task(s).
    grunt.registerTask('default', ['debug']);
    grunt.registerTask('debug', ['build_TD:Debug']); //Pass in Debug parameter. This will cause folder to named "releaseDebug" 
    grunt.registerTask('release', ['build_TD:']); // Do not pass in a parameter.  This will cause folders to remain "release"
    grunt.registerTask('all', ['debug', 'release']);

};