#################################################################################################
PREREQUISITES:
# 1.  NodeJS: https://nodejs.org/
# 2.  If this is your first time working with npm or grunt, follow the steps outlined in "Getting started" and "Installing the CLI" to make sure grunt is installed.
      To install Grunt on Windows, run the following:
      npm install -g grunt-cli
      If after that you get "'grunt' is not recognized as an internal or external command", simply add "%APPDATA%\npm" to Path Environment Variable.

#################################################################################################
NOTES:
The following packages make up the "grunt" task (Gruntfile.js) that can be used to build a release version of the Test Driver Application.  

The end result of this process is a single "release" or "releaseDebug" folder which is a self contained version of the test driver application. 
The contents of the "release" folder have all image references prepended with [[TD]].  This version will be further modified outside of this process to have the [[TD]] key replaced with an actual URL. 
The contents of the "releaseDebug" folder have all image references that are absolute paths (ie: /TD/images) changed to relative (ie: ../images/).  This version is meant to be deployed in a Local environment or on INT.

The grunt task utilizes the packages listed below as well as a file named "index-build.html" and "testDriver-build.phtml".  
"index-build.html" contains updated references to the minified javascript and css files.  It is renamed to "index.html" when copied to the "release" folder.
"testDriver-build.html" contains updated references to the minified javascript and css files.  It is renamed to "testDriver.phtml" when copied to the release folder.

This Grunt task utilizes the following modules:

Module Name:	"grunt-contrib-clean"
URL:			https://github.com/gruntjs/grunt-contrib-clean 
Version:		v0.6.0 
Purpose:		Deletes the contents of the "release", "releaseDebug", "staging" and "stagingDebug" folders.


Module Name:	"grunt-contrib-uglify" 
URL:			https://github.com/gruntjs/grunt-contrib-uglify 
Version:		v0.9.1
Purpose:		Minifies and obfuscates top level variables of javascript files.  Also configured to remove comments and console.log statements.  
				Javascript files are placed inside of the "scripts" folder (ie: release\scripts or releaseDebug\scripts).***


Module Name:	"grunt-contrib-cssmin"
URL:			https://github.com/gruntjs/grunt-contrib-cssmin 
Version:		v0.12.3
Purpose:		Compresses css stylesheets. Css files are placed in side of the "styles" folder (ie: release\styles or releaseDebug\styles)


Module Name:	"grunt-contrib-copy"
URL:			https://github.com/gruntjs/grunt-contrib-copy
Version:		v0.7.0
Purpose:		Copies additional files that are not minified/compressed but still need to exist in the release or releaseDebug folder (Ex: OAT, fonts, images, etc.). 
				All folders/files are copied into the "release" or "releaseDebug" folder and maintain their original structure. 
				This task also copies/renames index-build.html and testDriver-build.phtml.  These files contain references to the minified javascript/css files.  

Module Name:	"grunt-replace" -- NO LONGER BEING USED!
URL:			https://github.com/outatime/grunt-replace
Version:		v0.11.0
Purpose:		Uses variable @@key or RexEx pattern matching to replace strings.  This is currently being used to alter the image references in TD.
				For the "release" version of TD, all references to /images/ are replaced with [[TD]]/images.  For the "releaseDebug" version of TD
				all references to images that are done through absolute paths (ex: "/TD/images" are made relative ("../images/"))

*** UglifyJs only renames top level variables.  The mangleProperties option should provide more aggressive renaming but it was not working correctly with JQuery UI.


#################################################################################################
INSTRUCTIONS:

In the root directory of this web site are four files:
1. package.json - lists all of the packages that are being used by grunt to "minify/copy" the javascript and css for the application.  If new packages are added, use the command "npm install <package name> --save-dev" to ensure that this file stays up to date.  
2. Gruntfile.js - contains the grunt configuration for building the minified version of the application.  Any NEW javascript or css files created by TD developers should be added to this script.
3. index-build.html - This file is used to create the "index.html" file that will get put in the "release" or "releaseDebug" folder.  It has updated references to the compressed javascript and css files.
4. testDriver-build.phtml - This file is used to create the "testDriver.phtml" file that will get put in the "release" or "releaesDebug" folder.  It has updated references to the compressed javascript and css files.

To build the application (once you done everything in the "PREREQUISITES" section above):
1. Open up a command prompt.
2. Navigate to the root directory of the application. (this is where the package.json and Gruntfile.js is).
3. If this is your first time running the grunt task, or new packages have been added to the package.json or grunt configuration, Run the command "npm install", otherwise go to step 4.
4. Run one of the following commands:
	a. "grunt debug" // This will build a minified version of TD that can be run locally or in INT.  The output will be put inside of "releaseDebug"
	b. "grunt release" // This will build a minfied version of TD with image references prepended with [[TD]].  This is used for final production deployment. The output will be put inside of "release"
	c. "grunt all" // This will build both a debug and a release version.
